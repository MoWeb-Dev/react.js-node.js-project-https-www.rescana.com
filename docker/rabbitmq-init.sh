#!/bin/sh
docker exec rabbitmq bash -l -c "rabbitmqctl add_user dark $RABBITMQ"
docker exec rabbitmq bash -l -c 'rabbitmqctl set_user_tags dark administrator'
docker exec rabbitmq bash -l -c 'rabbitmqctl set_permissions -p / dark  ".*" ".*" ".*"'
docker exec server bash -l -c "node neo4j-schema-script.js"
