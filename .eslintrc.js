module.exports = {
    'env': {
        'browser': true,
        'es6': true,
        'node': true
    },
    'extends': 'google',
    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly'
    },
    'parser': 'babel-eslint',
    'parserOptions': {
        'ecmaFeatures': {
            'jsx': true
        },
        'ecmaVersion': 2018,
        'sourceType': 'module'
    },
    'plugins': [
        'react'
    ],
    'rules': {
        'indent': ['error', 4],
        'max-len': [2, 150, 6],
        'comma-dangle': ['error', 'never'],
        'require-jsdoc': ['error', {
            'require': {
                'MethodDefinition': false
            }
        }],
        'prefer-const': 0
    }
};
