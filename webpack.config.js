const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = function(env, argv) {
    return {
        entry: {
            main: './src/modules/app/app.jsx',
            surveyLink: './src/modules/survey/run/survey-continue.jsx',
        },
        devtool: "inline-source-map",
        output: {
            filename: '[name].bundle.js',
            path: path.resolve(__dirname, 'dist/js'),
            chunkFilename: '[name].bundle.js',
            publicPath: '/'
        },
        plugins: argv.mode === 'production' ? [
            new HtmlWebpackPlugin({
                template: 'public/index.html'
            }),
            new CleanWebpackPlugin(['dist']),
            new CopyWebpackPlugin([{from: 'public/**', to: '..'}]),
            new UglifyJsPlugin()
        ] : [
            new HtmlWebpackPlugin({
                template: 'public/index.html'
            }),
            new BundleAnalyzerPlugin(),
        ],
        devServer: {
            contentBase: [path.resolve(__dirname, 'public')],
            compress: true,
            hot: true,
            overlay: true,
            port: 3003,
            proxy: {
                "/api": "http://localhost:3002",
                "/aut": "http://localhost:3002",
                "/admin": "http://localhost:3002"
            }
        },

        node: {
            __dirname: true
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    commons: {
                        test: /[\\/]node_modules[\\/]/,
                        name: "vendor",
                        chunks: "all",
                    },
                },
            },
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        'css-loader'
                    ]
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    use: [
                        'file-loader'
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        'file-loader'
                    ]
                },
                {
                    test: /\.jsx$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                }
            ]
        }
    };
};


