module.exports = [
    {
        prefix: '/api',
        pin: 'role:api,cmd:*', // Make sure there are no spaces in pin.
        map: {
            setupTwoFactAuth: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getProjectsByUserId: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getOrganizationsByUserId: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            createDefaultSurvey: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getProjectsByOrgId: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            enableTwoFactor: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            verifyTwoFactor: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getVulnsByIP: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            sendSurveyAnswersEmail: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            addRevokedTokenToList: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            createSurveyLink: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            downloadSurveyWithAnswers: {
                GET: true,
                secure: {
                    fail: '/login'
                }
            },
            saveSurvey: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getSurvey: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getSurveyWithAnswers: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getAllSurveysWithAnswers: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            saveSurveyEditedData: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            saveSurveyWithAnswers: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getPortsByIP: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getBuckets: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            updateFindingStatus: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getASNByIP: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getProductsByIP: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getVPNByIP: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            addProject: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            addCompany: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getEmailBreachesData: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getEmailBreachInfo: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getNodeNearestInfo: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getAllIPs: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getIsp: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            setAppName: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getAppNames: {
                GET: true,
                secure: {
                    fail: '/login'
                }
            },
            enrich: {
                GET: true,
                secure: {
                    fail: '/login'
                }
            },
            watch: {
                GET: true,
                secure: {
                    fail: '/login'
                }
            },
            feed: {
                GET: true,
                secure: {
                    fail: '/login'
                }
            },
            removeCustomerApp: {
                GET: true,
                secure: {
                    fail: '/login'
                }
            },
            removePublicApp: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getMaxCVSScore: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getCVECountPerHostname: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getLocations: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getCVESeverity: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getSpf: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getDmarc: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getByDomain: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            deleteSurvey: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            deleteSurveyAnswers: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getDataleaks: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getBotnets: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getSSLCerts: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getWifiNetworks: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getGDPRData: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getBlacklists: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            deletePasteFromUser: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            updateSurveyLanguage: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            exportReport: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            exportSurveyReport: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getAllProjectsById: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllCompaniesById: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllCompaniesByOrgProjects: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getAllCompaniesPartialById: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getOrgById: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getCompanyPartialById: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getCompaniesByProjectName: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            updateCompanySurveyScore: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            createNewProjectByUser: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            editCompanyByUser: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            saveVendorAssessmentData: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            updateAssessmentNotificationArea: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            updateCompanyInfoStatus: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            deleteProject: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getAllVendors: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getAllProductsByVendors: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            exportCVEReport: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getSurveyTemplateFromExcel: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            isAuthUser: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            }
        }
    },
    {
        prefix: '/aut',
        pin: 'role:aut,cmd:*',
        map: {
            login: {
                POST: true,
                auth: {
                    strategy: 'local',
                    pass: '/aut/afterLogin',
                    fail: '/login'
                }
            },
            changePassword: {
                POST: true,
                auth: {
                    secure: {
                        fail: '/login'
                    }
                }
            },
            login2f: {POST: true},
            register: {POST: true},
            afterLogin: {
                GET: true,
                secure: {
                    fail: '/login'
                }
            },
            update_user: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            update_user_dataleaksConfig: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            update_user_emailBreachesConfig: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            update_user_blacklistedDomainsConfig: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            update_user_cveReportsConfig: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            get_user_cveReportsConfig: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            get_user_dataleaksConfig: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            get_user_emailBreachesConfig: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            get_user_blacklistedDomainsConfig: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            }
        }
    },
    {
        prefix: '/admin',
        pin: 'role:admin,cmd:*',
        map: {
            getDiscoveredDomains: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            validateDomain: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            runManHibp: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getAllUsers: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getOrganizationByProject: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            updateSurveyTemplateAllowedOrg: {
                POST: true,
                secure: {
                    fail: '/login'
                }
            },
            getAllEmailsToBeNotifiedOnVendorCreation: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getOrganizationsByUserId: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            getAllCompanies: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllCompaniesWithPagination: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllOrganizations: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllOrganizationsWithPagination: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllOrganizationsForDD: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllProjectsForDD: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllCompaniesForDD: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllSurveyTemplatesForDD: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllProjects: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllProjectsWithPagination: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllTokens: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            addAllCompToNeo4j: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            getAllApiKeys: {
                GET: true,
                secure: {
                    fail: '/'
                }
            },
            addProject: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            updateEmailsToBeNotifiedOnVendorCreationOnMongo: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            deleteOrganization: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            addOrganization: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            addApiKey: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            editApiKey: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            editApiKeyStatus: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            deleteApiKey: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            addAllowedUsersToOrgProjects: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            addAllowedUsersToProjCompanies: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            removeAllowedUsersFromOrgProjects: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            removeAllowedUsersFromProjCompanies: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            addCompany: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            runCompanyQuery: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            runDiscoveryQuery: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            runProjectQuery: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            deleteProject: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            deleteUser: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            deleteCompany: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            deleteDiscoveredDomain: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            harvestEmails: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            searchBlacklists: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            searchBotnets: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            searchSSLCertificates: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            searchGDPRCompliance: {
                POST: true,
                secure: {
                    fail: '/'
                }
            },
            collectWifiNetworks: {
                POST: true,
                secure: {
                    fail: '/'
                }
            }
        }
    }
];

