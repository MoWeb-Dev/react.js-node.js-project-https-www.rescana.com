const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const authCfg = require('./auth-config');

const jwtUrlParams = {
    secretOrKey: authCfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromExtractors([ExtractJwt.fromUrlQueryParameter('token'), ExtractJwt.fromBodyField('token')])
};

module.exports = function() {
    const strategy = new JwtStrategy(jwtUrlParams, function(payload, done) {
        return done(null, {
            said: payload.said
        });
    });
    passport.use(strategy);
    return {
        initialize: function() {
            return passport.initialize();
        },
        authenticate: function() {
            return passport.authenticate('jwt', authCfg.jwtSession);
        }
    };
};
