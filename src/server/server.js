'use strict';
const config = require('app-config');
const apiHelpers = require('./api/api-helpers');
const Promise = require('bluebird');
const MongoClient = require('mongodb').MongoClient;
const mongoUrl = config.addresses.mongoConnectionString;

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const jwt = require('jsonwebtoken');
const authCfg = require('./middleware/auth-config');
const path = require('path');
const speakeasy = require('speakeasy');
const compression = require('compression');
const helmet = require('helmet');
const serverHelpers = require('./api/api-helpers');
const fixOfficeLink = require('fix-office-link');
const RateLimit = require('express-rate-limit');
const moment = require('moment');
const mcache = require('memory-cache');
const intelHelpers = require('../server_modules/utils/intel-helpers');

const cache = (duration) => {
    return (req, res, next) => {
        let key = '__express__' + JSON.stringify(req.body) + req.originalUrl || req.url;
        let cachedBody = mcache.get(key);
        if (cachedBody) {
            res.send(cachedBody);
            return;
        } else {
            res.sendResponse = res.send;
            res.send = (body) => {
                mcache.put(key, body, duration * 1000);
                res.sendResponse(body);
            };
            next();
        }
    };
};

const seneca = require('seneca')({
    timeout: 99999999999
})
    .use('basic')
    .use('entity')
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client({port: 22228, host: 'localhost', pin: 'role:intelQuery', timeout: 999999})
    .client({port: 11112, host: 'localhost', pin: 'role:mailer', timeout: 999999})
    .client({port: 10202, host: 'localhost', pin: 'role:whois', timeout: 999999})
    .client({port: 10388, host: 'localhost', pin: 'role:reportCreator', timeout: 999999})
    .client({port: 10701, host: '0.0.0.0', pin: 'role:survey', timeout: 999999})
    .client({port: 13889, host: config.addresses.cveImporterServiceAddress, pin: 'role:cveImporter', timeout: 999999})
    .client({port: 10587, host: 'localhost', pin: 'role:rssFeeds', timeout: 999999})
    .client({port: 10890, host: 'localhost', pin: 'role:emailHunter', timeout: 999999})
    .client({port: 10589, host: 'localhost', pin: 'role:sslCertificatesCollect', timeout: 999999})
    .client({port: 10590, host: 'localhost', pin: 'role:sslCertificatesQuery', timeout: 999999})
    .client({port: 10598, host: 'localhost', pin: 'role:gdprComplianceCollect', timeout: 999999})
    .client({port: 10599, host: 'localhost', pin: 'role:gdprComplianceQuery', timeout: 999999})
    .client({port: 10588, host: 'localhost', pin: 'role:blacklistsSearcher', timeout: 999999})
    .client({port: 10399, host: 'localhost', pin: 'role:wifiNetworksCollector', timeout: 999999})
    .client({port: 10398, host: 'localhost', pin: 'role:wifiNetworksQuery', timeout: 999999})
    .client({port: 12121, host: 'localhost', pin: 'role:domain-finder', timeout: 999999})
    .client({port: 12122, host: 'localhost', pin: 'role:domain-validator', timeout: 999999});


const web = require('seneca-web');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const passport = require('passport');
const Strategy = require('passport-local').Strategy;
const jwtAuth = require('./middleware/jwt-auth')();
const multer = require('multer');
const routes = require('./routes');
const intelApi = require('./api/intel-api')(seneca);
const phishApi = require('./api/phishing-api')(seneca);
const surveyApi = require('./api/survey-api')(seneca);
const userApi = require('./api/user-api')(seneca);
const entityApi = require('./api/entities-api')(seneca);

const store = new MongoDBStore(
    {
        uri: config.addresses.mongoConnectionString,
        collection: 'sessions'
    });

// Catch errors
store.on('error', function(error) {
    console.log(error);
});
const plugin = () => {
    seneca.add('role:api,cmd:setAppName', phishApi.setAppName);
    seneca.add('role:api,cmd:sendSurveyAnswersEmail', surveyApi.sendSurveyAnswersEmail);
    seneca.add('role:api,cmd:addRevokedTokenToList', surveyApi.addRevokedTokenToList);
    seneca.add('role:api,cmd:createSurveyLink', surveyApi.createSurveyLink);
    seneca.add('role:api,cmd:removeCustomerApp', phishApi.removeCustomerApp);
    seneca.add('role:api,cmd:removePublicApp', phishApi.removePublicApp);
    seneca.add('role:api,cmd:getAppNames', phishApi.getAppNames);
    seneca.add('role:api,cmd:enrich', phishApi.saveAppAndenrich);
    seneca.add('role:api,cmd:feed', phishApi.getFeed);
    seneca.add('role:api,cmd:watch', phishApi.createWatchedPublicApp);
    seneca.add('role:api,cmd:updateFindingStatus', intelApi.updateFindingStatus);
    seneca.add('role:api,cmd:getVulnsByIP', intelApi.getVulnsByIP);
    seneca.add('role:api,cmd:getBuckets', intelApi.getBuckets);
    seneca.add('role:api,cmd:getPortsByIP', intelApi.getPortsByIP);
    seneca.add('role:api,cmd:getASNByIP', intelApi.getASNByIP);
    seneca.add('role:api,cmd:getVPNByIP', intelApi.getVPNByIP);
    seneca.add('role:api,cmd:getIsp', intelApi.getIsp);
    seneca.add('role:api,cmd:getBlacklists', intelApi.getBlacklistsByDomains);
    seneca.add('role:api,cmd:getEmailBreachesData', intelApi.getEmailBreachesData);
    seneca.add('role:api,cmd:getEmailBreachInfo', intelApi.getEmailBreachInfo);
    seneca.add('role:api,cmd:getNodeNearestInfo', intelApi.getNodeNearestInfo);
    seneca.add('role:api,cmd:getAllIPs', intelApi.getAllIPs);
    seneca.add('role:api,cmd:getSpf', intelApi.getSpf);
    seneca.add('role:api,cmd:getByDomain', intelApi.getByDomain);
    seneca.add('role:api,cmd:getDmarc', intelApi.getDmarc);
    seneca.add('role:api,cmd:getLocations', intelApi.getLocations);
    seneca.add('role:api,cmd:getProductsByIP', intelApi.getProductsByIP);
    seneca.add('role:api,cmd:getMaxCVSScore', intelApi.getMaxCVSScore);
    seneca.add('role:api,cmd:getCVECountPerHostname', intelApi.getCVECountPerHostname);
    seneca.add('role:api,cmd:getCVESeverity', intelApi.getCVESeverity);
    seneca.add('role:api,cmd:saveSurvey', surveyApi.saveSurvey);
    seneca.add('role:api,cmd:saveSurveyWithAnswers', surveyApi.saveSurveyWithAnswers);
    seneca.add('role:api,cmd:getSurvey', surveyApi.getSurvey);
    seneca.add('role:api,cmd:getSurveyWithAnswers', surveyApi.getSurveyWithAnswers);
    seneca.add('role:api,cmd:getAllSurveysWithAnswers', surveyApi.getAllSurveysWithAnswers);
    seneca.add('role:api,cmd:saveSurveyEditedData', surveyApi.saveSurveyEditedData);
    seneca.add('role:api,cmd:downloadSurveyWithAnswers', surveyApi.downloadSurveyWithAnswers);
    seneca.add('role:api,cmd:exportSurveyReport', surveyApi.exportSingleSurveyReport);
    seneca.add('role:api,cmd:deleteSurvey', surveyApi.deleteSurvey);
    seneca.add('role:api,cmd:deleteSurveyAnswers', surveyApi.deleteSurveyAnswers);
    seneca.add('role:api,cmd:updateSurveyLanguage', surveyApi.updateSurveyLangBySid);
    seneca.add('role:api,cmd:exportReport', userApi.exportReport);
    seneca.add('role:aut,cmd:register', userApi.registerUser);
    seneca.add('role:aut,cmd:afterLogin', userApi.afterLogin);
    seneca.add('role:aut,cmd:update_user', userApi.updateUser);
    seneca.add('role:aut,cmd:update_user_dataleaksConfig', userApi.updateUserDataleaksConfig);
    seneca.add('role:aut,cmd:update_user_emailBreachesConfig', userApi.updateUserEmailBreachesConfig);
    seneca.add('role:aut,cmd:update_user_blacklistedDomainsConfig', userApi.updateUserBlacklistedDomainsConfig);
    seneca.add('role:aut,cmd:update_user_cveReportsConfig', userApi.updateUserCVEReportsConfig);
    seneca.add('role:aut,cmd:get_user_cveReportsConfig', userApi.getUserVendorsConfig);
    seneca.add('role:aut,cmd:get_user_dataleaksConfig', userApi.getUserCompaniesConfig);
    seneca.add('role:aut,cmd:get_user_emailBreachesConfig', userApi.getUserCompaniesConfig);
    seneca.add('role:aut,cmd:get_user_blacklistedDomainsConfig', userApi.getUserCompaniesConfig);
    seneca.add('role:aut,cmd:changePassword', userApi.changePassword);
    seneca.add('role:api,cmd:getDataleaks', entityApi.getDataleaksByKeywords);
    seneca.add('role:api,cmd:getBotnets', entityApi.getBotnets);
    seneca.add('role:api,cmd:getSSLCerts', entityApi.getSSLCerts);
    seneca.add('role:api,cmd:getWifiNetworks', entityApi.getWifiNetworks);
    seneca.add('role:api,cmd:getGDPRData', entityApi.getGDPRData);
    seneca.add('role:api,cmd:deletePasteFromUser', entityApi.deletePasteFromUser);
    seneca.add('role:api,cmd:getAllProjectsById', entityApi.getAllProjectsById);
    seneca.add('role:api,cmd:getAllCompaniesById', entityApi.getAllCompaniesById);
    seneca.add('role:api,cmd:getAllCompaniesByOrgProjects', entityApi.getAllCompaniesByOrgProjects);
    seneca.add('role:api,cmd:getAllCompaniesPartialById', entityApi.getAllCompaniesPartialById);
    seneca.add('role:api,cmd:getOrgById', entityApi.getOrgById);
    seneca.add('role:api,cmd:getCompanyPartialById', entityApi.getCompanyPartialById);
    seneca.add('role:api,cmd:getCompaniesByProjectName', entityApi.getCompaniesByProjectName);
    seneca.add('role:api,cmd:updateCompanySurveyScore', entityApi.updateCompanySurveyScore);
    seneca.add('role:api,cmd:createNewProjectByUser', entityApi.createNewProjectByUser);
    seneca.add('role:api,cmd:editCompanyByUser', entityApi.editCompanyByUser);
    seneca.add('role:api,cmd:saveVendorAssessmentData', entityApi.saveVendorAssessmentData);
    seneca.add('role:api,cmd:updateAssessmentNotificationArea', entityApi.updateAssessmentNotificationArea);
    seneca.add('role:api,cmd:updateCompanyInfoStatus', entityApi.updateCompanyInfoStatus);
    seneca.add('role:api,cmd:deleteProject', entityApi.deleteProject);
    seneca.add('role:api,cmd:getAllVendors', entityApi.cveImporterGetAllVendors);
    seneca.add('role:api,cmd:getAllProductsByVendors', entityApi.cveImporterGetAllProductsByVendors);
    seneca.add('role:api,cmd:exportCVEReport', entityApi.exportCVEReport);
    seneca.add('role:api,cmd:getSurveyTemplateFromExcel', surveyApi.getSurveyTemplateFromExcel);
    seneca.add('role:api,cmd:isAuthUser', userApi.isAuthUser);
    seneca.add('role:api,cmd:setupTwoFactAuth', userApi.setupTwoFactAuth);
    seneca.add('role:api,cmd:verifyTwoFactor', userApi.verifyTwoFactor);
    seneca.add('role:api,cmd:enableTwoFactor', userApi.enableTwoFactor);
    seneca.add('role:api,cmd:addCompany', entityApi.addCompany);
    seneca.add('role:api,cmd:addProject', entityApi.addProject);
    seneca.add('role:api,cmd:getOrganizationsByUserId', userApi.getOrganizationsByUserId);
    seneca.add('role:api,cmd:getProjectsByOrgId', userApi.getProjectsByOrgId);
    seneca.add('role:api,cmd:getProjectsByUserId', userApi.getProjectsByUserId);
    seneca.add('role:api,cmd:createDefaultSurvey', surveyApi.createDefaultSurvey);
    seneca.add('role:admin,cmd:getOrganizationByProject', userApi.getOrganizationByProject);
    seneca.add('role:admin,cmd:getAllUsers', userApi.getAllUsers);
    seneca.add('role:admin,cmd:updateSurveyTemplateAllowedOrg', surveyApi.updateSurveyTemplateAllowedOrg);
    seneca.add('role:admin,cmd:getAllEmailsToBeNotifiedOnVendorCreation', userApi.getAllEmailsToBeNotifiedOnVendorCreation);
    seneca.add('role:admin,cmd:updateEmailsToBeNotifiedOnVendorCreationOnMongo', userApi.updateEmailsToBeNotifiedOnVendorCreationOnMongo);
    seneca.add('role:admin,cmd:getOrganizationsByUserId', userApi.getOrganizationsByUserId);
    seneca.add('role:admin,cmd:deleteUser', userApi.deleteUser);
    seneca.add('role:admin,cmd:getAllCompanies', entityApi.getAllCompanies);
    seneca.add('role:admin,cmd:getAllCompaniesWithPagination', entityApi.getAllCompaniesWithPagination);
    seneca.add('role:admin,cmd:getDiscoveredDomains', entityApi.getDiscoveredDomains);
    seneca.add('role:admin,cmd:validateDomain', entityApi.validateDomain);
    seneca.add('role:admin,cmd:getAllProjects', entityApi.getAllProjects);
    seneca.add('role:admin,cmd:getAllProjectsWithPagination', entityApi.getAllProjectsWithPagination);
    seneca.add('role:admin,cmd:getAllOrganizations', entityApi.getAllOrganizations);
    seneca.add('role:admin,cmd:getAllOrganizationsWithPagination', entityApi.getAllOrganizationsWithPagination);
    seneca.add('role:admin,cmd:getAllOrganizationsForDD', entityApi.getAllOrganizationsForDD);
    seneca.add('role:admin,cmd:getAllSurveyTemplatesForDD', entityApi.getAllSurveyTemplatesForDD);
    seneca.add('role:admin,cmd:getAllProjectsForDD', entityApi.getAllProjectsForDD);
    seneca.add('role:admin,cmd:getAllCompaniesForDD', entityApi.getAllCompaniesForDD);
    seneca.add('role:admin,cmd:getAllTokens', entityApi.getAllTokens);
    seneca.add('role:admin,cmd:getAllApiKeys', entityApi.getAllApiKeys);
    seneca.add('role:admin,cmd:addProject', entityApi.addProject);
    seneca.add('role:admin,cmd:addOrganization', entityApi.addOrganization);
    seneca.add('role:admin,cmd:addApiKey', entityApi.addApiKey);
    seneca.add('role:admin,cmd:editApiKey', entityApi.editApiKey);
    seneca.add('role:admin,cmd:editApiKeyStatus', entityApi.editApiKeyStatus);
    seneca.add('role:admin,cmd:deleteApiKey', entityApi.deleteApiKey);
    seneca.add('role:admin,cmd:addAllCompToNeo4j', entityApi.addAllCompToNeo4j);
    seneca.add('role:admin,cmd:deleteOrganization', entityApi.deleteOrganization);
    seneca.add('role:admin,cmd:addAllowedUsersToOrgProjects', entityApi.addAllowedUsersToOrgProjects);
    seneca.add('role:admin,cmd:addAllowedUsersToProjCompanies', entityApi.addAllowedUsersToProjCompanies);
    seneca.add('role:admin,cmd:removeAllowedUsersFromOrgProjects', entityApi.removeAllowedUsersFromOrgProjects);
    seneca.add('role:admin,cmd:removeAllowedUsersFromProjCompanies', entityApi.removeAllowedUsersFromProjCompanies);
    seneca.add('role:admin,cmd:runCompanyQuery', entityApi.runCompanyQuery);
    seneca.add('role:admin,cmd:runDiscoveryQuery', entityApi.runDiscoveryQuery);
    seneca.add('role:admin,cmd:runProjectQuery', entityApi.runProjectQuery);
    seneca.add('role:admin,cmd:addCompany', entityApi.addCompany);
    seneca.add('role:admin,cmd:deleteCompany', entityApi.deleteCompany);
    seneca.add('role:admin,cmd:deleteProject', entityApi.deleteProject);
    seneca.add('role:admin,cmd:deleteDiscoveredDomain', entityApi.deleteDiscoveredDomain);
    seneca.add('role:admin,cmd:harvestEmails', entityApi.harvestEmails);
    seneca.add('role:admin,cmd:searchBlacklists', entityApi.manualSearchBlacklists);
    seneca.add('role:admin,cmd:searchBotnets', entityApi.manualSearchBotnets);
    seneca.add('role:admin,cmd:searchSSLCertificates', entityApi.manualSearchSSLCertificates);
    seneca.add('role:admin,cmd:searchGDPRCompliance', entityApi.manualSearchGDPRCompliance);
    seneca.add('role:admin,cmd:collectWifiNetworks', entityApi.manualSearchWifiNetworks);
    seneca.add('role:admin,cmd:runManHibp', entityApi.runManHibp);
};

const listToArray = (list) => {
    return new Promise(async (resolve) => {
        try {
            const arr = await list.toArray();
            if (arr) {
                resolve(arr);
            } else {
                resolve();
            }
        } catch (e) {
            console.error('Failed In listToArray() func');
            resolve();
        }
    });
};

const getNeoFlag = () => {
    return new Promise((resolve) => {
        try {
            return MongoClient.connect(mongoUrl, async (err, client) => {
                const db = client.db('darkvision');
                let collection = await db.collection('dataFlagForNeoMerge');
                const find = Promise.promisify(collection.find, {context: collection});

                const myDocument = await find({});
                if (myDocument) {
                    const arr = await listToArray(myDocument);
                    if (arr && Array.isArray(arr) && arr.length > 0 && arr[0].dataFlagForNeoMerge) {
                        console.log('dataFlagForNeoMerge found ' + arr[0].dataFlagForNeoMerge);
                        client.close();
                        resolve(arr[0].dataFlagForNeoMerge);
                    } else {
                        client.close();
                        resolve();
                    }
                }
            });
        } catch (e) {
            console.log('dataFlagForNeoMerge Not Found: ' + e);
            resolve();
        }
    });
};

const createNeoFlag = () => {
    return new Promise(async (resolve) => {
        try {
            await MongoClient.connect(mongoUrl, async (err, client) => {
                const db = client.db('darkvision');
                let collection = await db.collection('dataFlagForNeoMerge');
                const flagData = {dataFlagForNeoMerge: true};
                await collection.insert(flagData, {upsert: true}, () => {
                    console.log('dataFlagForNeoMerge inserted!');
                    client.close();
                    resolve();
                });
            });
        } catch (e) {
            console.log('Failed To Save dataFlagForNeoMerge To DB: ' + e);
            resolve();
        }
    });
};

const getAllCompanies = () => {
    return new Promise(async (resolve) => {
        try {
            const allCompanies = await promiseAct('role:companies, list:company', {query: {}});
            if (allCompanies) {
                resolve(allCompanies);
            } else {
                console.error('error', 'Error: failed to get companies.');
                resolve();
            }
        } catch (e) {
            console.error('error', 'Error: failed to get companies.');
            resolve();
        }
    });
};

//one time func use - when server is loaded - merge all companies and related domains on neo
const runCheckOnCompAndDomainOnNeo = async () => {
    try {
        let hasAddedAllCompAndDomainsRelationshipFlag = await getNeoFlag();
        // if flag was not created(when server is up for the first time) we get flag is undefined
        // then we create the flag in mongo and this func will not called again
        if (typeof hasAddedAllCompAndDomainsRelationshipFlag === 'undefined') {
            await createNeoFlag();
            const allCompanies = await getAllCompanies();
            allCompanies.map(async (curCompany) => {
                await apiHelpers.mergeCompanyAndRelatedDomainsOnNeo(curCompany);
            });
            console.log('In runCheckOnCompAndDomainOnNeo() - Merged All DataBase Companies With Domains.\'');
        }
    } catch (e) {
        console.error('error', 'Error In runCheckOnCompAndDomainOnNeo() - Failed To Merge All DataBase Companies With Domains.');
    }
};

// This runs when login route is activated.
passport.use(new Strategy((username, password, cb) => {
    const getCurrentTime = () => {
        return '[' + moment().format() + '] ';
    };
    console.log(getCurrentTime() + 'Login attempt by user\'s email: ', username);
    // check user's last timeStamp and changed it after 5 minutes.
    // The number of tries before lockout is set while defining the plugin and called failedLoginCount.
    seneca.act({role: 'user', cmd: 'updateUserLastLoginTry', data: username}, (err, data) => {
        if (err) {
            console.log(getCurrentTime() + 'Login failed for userEmail: [' + username + '] with error: ', err);
            cb(err);
        } else if (data.ok) {
            seneca.act({role: 'user', cmd: 'login', email: username, password: password}, (err, data) => {
                if (err) {
                    console.log(getCurrentTime() + 'Login failed for userEmail: [' + username + '] with error: ', err);
                    cb(err);
                } else if (!data) {
                    console.log(getCurrentTime() + 'Login failed for userEmail: [' + username + '].');
                    cb(null, false);
                } else if (!data.ok) {
                    if (data.why || data.err) {
                        console.log(getCurrentTime() + 'Login failed for userEmail: [' + username + '] with reason: ', data.why || data.err);
                    } else {
                        console.log(getCurrentTime() + 'Login failed for userEmail: [' + username + '].');
                    }
                    cb(null, data);
                } else {
                    console.log(getCurrentTime() + 'Login succeeded. userEmail: ' + username);
                    cb(null, data.user);
                }
            });
        } else {
            cb(null, false);
        }
    });
}));

passport.serializeUser((obj, cb) => {
    if (!obj.ok) {
        cb(null, obj);
    } else {
        cb(null, {id: obj.id, mfa: obj.mfaEnabled, firstLogin: obj.firstLogin});
    }
});

passport.deserializeUser((obj, cb) => {
    if (!obj.ok && obj.why) {
        cb(null, obj);
    } else {
        const user_entity = seneca.make('sys', 'user');
        user_entity.load$(obj.id, (err, user) => {
            if (err) {
                return cb(err);
            } else {
                const filteredUser = {};
                filteredUser.email = user && user.email;
                filteredUser.mfaEnabled = user && user.mfaEnabled;
                filteredUser.firstLogin = user && user.firstLogin;
                filteredUser.name = user && user.name;
                filteredUser.eyPartner = user && user.eyPartner;
                filteredUser.nick = user && user.nick;
                filteredUser.userRole = user && user.userRole;
                filteredUser.canExportCVEReports = user && user.canExportCVEReports;
                filteredUser.logo = user && user.logo;
                filteredUser.isUserAllowedToAddVendors = user && user.isUserAllowedToAddVendors;
                filteredUser.isInDemoMode = user && user.isInDemoMode;
                filteredUser.isVendorAssessmentEnabled = user && user.isVendorAssessmentEnabled;
                filteredUser.id = user && user.id;
                filteredUser.alertSystemConfig = user && user.alertSystemConfig;
                filteredUser.userPreferencesConfig = user && user.userPreferencesConfig;
                cb(null, filteredUser);
            }
        });
    }
});

const promiseAct = Promise.promisify(seneca.act, {context: seneca});
const app = express();

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, config.paths.evidence);
    },
    filename: function(req, file, cb) {
        const ext = file.originalname.substring(file.originalname.indexOf('.') + 1, file.originalname.length);
        const name = file.originalname.substring(0, file.originalname.indexOf('.'));
        cb(null, name + '-' + Date.now() + '.' + ext);
    }
});

const storageLogo = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, config.paths.orgLogo);
    },
    filename: function(req, file, cb) {
        const ext = file.originalname.substring(file.originalname.indexOf('.') + 1, file.originalname.length);
        const name = file.originalname.substring(0, file.originalname.indexOf('.'));
        cb(null, name + '-' + Date.now() + '.' + ext);
    }
});

const vendorAssessmentFilesStorage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, config.paths.vendorAssessmentFiles);
    },
    filename: function(req, file, cb) {
        const ext = file.originalname.substring(file.originalname.indexOf('.') + 1, file.originalname.length);
        const name = file.originalname.substring(0, file.originalname.indexOf('.'));
        cb(null, name + '-' + Date.now() + '.' + ext);
    }
});

const storageExcel = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, config.paths.excelTemplates);
    },
    filename: function(req, file, cb) {
        const ext = file.originalname.substring(file.originalname.indexOf('.') + 1, file.originalname.length);
        const name = file.originalname.substring(0, file.originalname.indexOf('.'));
        cb(null, name + '-' + Date.now() + '.' + ext);
    }
});

const storageExcelWithAnswers = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, config.paths.excelTemplatesWithAnswers);
    },
    filename: function(req, file, cb) {
        let ext, name;
        const extentionIndex = file.originalname.lastIndexOf('.');
        if (extentionIndex > -1) {
            ext = file.originalname.substring(extentionIndex + 1, file.originalname.length);
            name = file.originalname.substring(0, extentionIndex);
        } else {
            ext = 'xlsx';
            name = file.originalname;
        }
        cb(null, name + '-' + Date.now() + '.' + ext);
    }
});

const storageIntelManual = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, config.paths.intelligenceManualFindings);
    },
    filename: function(req, file, cb) {
        const ext = file.originalname.substring(file.originalname.indexOf('.') + 1, file.originalname.length);
        const name = file.originalname.substring(0, file.originalname.indexOf('.'));
        cb(null, name + '-' + Date.now() + '.' + ext);
    }
});

function isImage(mime) {
    if (mime === 'image/png' || mime === 'image/jpeg' || mime === 'image/bmp') {
        return true;
    }
}

function isDocument(mime) {
    if (mime === 'application/vnd.ms-excel' || mime === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        || mime === 'application/vnd.ms-powerpoint' || mime === 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        || mime === 'application/msword' || mime === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || mime === 'application/visio' || mime === 'application/x-visio'
        || mime === 'application/vnd.visio' || mime === 'application/visio.drawing' || mime === 'application/vsd' || mime === 'image/x-vsd' || mime === 'zz-application/zz-winassoc-vsd'
        || mime === 'application/vnd.ms-visio.viewer' || mime === 'application/pdf' /* || mime === 'application/zip'*/ || mime === 'application/octet-stream' /* || mime === 'application/x-rar-compressed'*/
    /* || mime === 'application/x-zip-compressed'*/) {
        return true;
    }
}

function fileFilter(ignore, file, cb) {
    const fileParts = file.originalname.split('.');

    // To reject this file pass `false`, like so:
    if (file.originalname.length > 80 || !serverHelpers.allowExtensions(file.originalname) || fileParts.length > 4) {
        cb(null, false);
    } else if (isImage(file.mimetype) || isDocument(file.mimetype)) {
        cb(null, true);
    }
}

app.enable('trust proxy');
app.use(compression());
app.use(helmet());
app.use(helmet.referrerPolicy({policy: 'same-origin'}));
app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({extended: false, limit: '10mb'}));
app.use(fixOfficeLink);
app.use((req, res, next) => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.options('*', (req, res) => {
    res.set('Access-Control-Allow-Methods', 'OPTIONS,GET,HEAD,POST,PUT,PATCH,DELETE');
    res.send();
});


app.use(express.static(path.join(__dirname, '../../dist/js')));
app.use('/', express.static(path.join(__dirname, '../../dist/images')));
app.use(express.static(path.join(__dirname, '../../scr')));
app.use(express.static(path.join(__dirname, '../../dist/public')));
app.use(express.static(path.join(__dirname, '../../dist/public/templates')));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true, limit: '10mb'}));
app.use(session({
    secret: 'magic dog barks at rabbits',
    cookie: {
        maxAge: config.consts.sessionMaxAge // default is a week - vcportal is set to null making it a session cookie.
    },
    store: store,
    resave: true,
    rolling: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());


app.options('*', (req) => {
    // Check each request if it has a mfa lock and if it's not the registration page.
    if (req.session.mfaLock && req.path !== `/login2fa`) {
        req.user = null; // act as if user isn't authenticated
    }
});

// app.use('/api/getCompaniesByProjectName', cache(3600), (req, res, next) => {
//     next();
// });

app.use('/api/getCVESeverity', cache(3600), (req, res, next) => {
    next();
});

app.use('/api/getCVECountPerHostname', cache(3600), (req, res, next) => {
    next();
});

app.use('/api/getLocations', cache(3600), (req, res, next) => {
    next();
});

app.use('/api/createNewProjectByUser', (req, res, next) => {
    mcache.clear();
    next();
});

// app.use('/api/getSSLCerts', cache(3600), (req, res, next) => {
//     mcache.clear();
//     next();
// });


// This runs after login - catches the request as opposed to the seneca route with the same function.
app.use('/aut/afterLogin', (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.status(200);
    if (!req.user.mfaEnabled) {
        res.json({ok: true, user: req.user});
    } else {
        req.session.mfaLock = true;
        res.json({ok: false, user: req.user, mfaEnabled: req.user.mfaEnabled, firstLogin: req.user.firstLogin});
    }
    next();
});

// Login with 2f token
app.use('/aut/login2fa', (req, res) => {
    const user_entity = seneca.make$('sys', 'user');
    user_entity.list$({id: req.session.passport.user.id}, (err, user) => {
        const verified = speakeasy.totp.verify({
            secret: user[0].mfaSecret,
            encoding: 'base32',
            token: req.body.mfaCode,
            window: 6
        });

        res.setHeader('Content-Type', 'application/json');
        res.status(200);

        if (verified) {
            delete req.session.mfaLock;
            res.json({ok: true, user: req.user});
        } else {
            res.json({ok: false});
        }
    });
});

app.post('/api/surveyImageUpload', multer({
    storage: storage,
    limits: {fileSize: 3000000},
    fileFilter: fileFilter
}).array('surveyEvidence', 3), (req, res) => {
    console.log('In /api/surveyImageUpload');
    if (req.session.passport.user) {
        let path = '../../../evidence/';
        serverHelpers.uploadImageOrFileToSurveyAnswers(req, res, path, promiseAct);
    } else {
        res.redirect('/login');
    }
});

app.post('/api/saveLogoForOrg', multer({
    storage: storageLogo,
    limits: {fileSize: 3000000},
    fileFilter: fileFilter
}).array('logoForOrg', 3), (req, res) => {
    console.log('In /api/saveLogoForOrg');
    if (req.session.passport.user) {
        let path = '../../orgLogo/';
        serverHelpers.uploadImageOrFileToSurveyAnswers(req, res, path, promiseAct);
    } else {
        res.redirect('/login');
    }
});

app.post('/api/saveVendorAssessmentFile', multer({
    storage: vendorAssessmentFilesStorage,
    limits: {fileSize: 3000000},
    fileFilter: fileFilter
}).array('vendorAssessmentFile', 3), (req, res) => {
    console.log('In /api/saveVendorAssessmentFile');
    if (req.session.passport.user) {
        let path = '../../vendorAssessmentFiles/';
        serverHelpers.uploadImageOrFileToSurveyAnswers(req, res, path, promiseAct);
    } else {
        res.redirect('/login');
    }
});


app.post('/api/surveyExcelUpload', multer({
    storage: storageExcel,
    limits: {fileSize: 3000000},
    fileFilter: fileFilter
}).array('surveyExcelTemplate', 1), (req, res) => {
    console.log('In /api/surveyExcelUpload');
    if (req.session.passport.user) {
        if (req.files && req.files[0] && req.files[0].filename) {
            let tempName, tempLang;
            const firstDot = req.files[0].filename.indexOf('.');
            if (firstDot && firstDot > 0) {
                tempName = req.files[0].filename.substring(0, firstDot);
                tempLang = req.files[0].filename.substring(firstDot + 1, req.files[0].filename.indexOf('.', firstDot + 1));

                if (tempName) {
                    tempName = tempName.substring(0, tempName.lastIndexOf('-'));
                }
            }

            seneca.client({
                host: 'localhost',
                port: 10701,
                pin: 'role:survey'
            });

            promiseAct('role:survey, get:getSurveyTemplateFromExcel', {
                query: {
                    user: req.user,
                    templateName: tempName,
                    lang: tempLang,
                    filePath: req.files[0].name || req.files[0].path
                }
            }).then((sidData) => {
                console.log('Excel template loaded.');
                if (sidData) {
                    // return the new generated sid to client.
                    res.send({result: sidData});
                } else {
                    console.log('No Sid returned.');
                    res.send({result: ''});
                }
            }).catch((e) => {
                console.log('getSurveyTemplateFromExcel : ', e);
                res.send({result: ''});
            });
        } else {
            console.log('No file path available at /api/surveyExcelUpload');
            res.send({result: ''});
        }
    } else {
        res.redirect('/login');
    }
});

app.post('/api/surveyExcelWithAnswersUpload', multer({
    storage: storageExcelWithAnswers,
    limits: {fileSize: 3000000},
    fileFilter: fileFilter
}).array('surveyExcelWithAnswersTemplate', 1), (req, res) => {
    console.log('In /api/surveyExcelWithAnswersUpload');
    if (req.session.passport.user) {
        if (req.files && req.files[0] && req.files[0].filename && req.body) {
            const surveyName = req.body.surveyName;
            const tempLang = req.body.surveyLang;
            const surveyRespondent = req.body.surveyRespondent;
            const surveyCompanyId = req.body.surveyCompanyId;

            seneca.client({
                host: 'localhost',
                port: 10701,
                pin: 'role:survey'
            });

            promiseAct('role:survey, get:getSurveyWithAnswersTemplateFromExcel', {
                query: {
                    user: req.user,
                    surveyName: surveyName,
                    lang: tempLang,
                    surveyRespondent: surveyRespondent,
                    surveyCompanyId: surveyCompanyId,
                    filePath: req.files[0].name || req.files[0].path
                }
            }).then((sidData) => {
                console.log('Excel template loaded.');
                if (sidData) {
                    // return the new generated sid to client.
                    res.send({result: sidData});
                } else {
                    console.log('No Sid returned.');
                    res.send({result: ''});
                }
            }).catch((e) => {
                console.log('getSurveyTemplateFromExcel : ', e);
                res.send({result: ''});
            });
        } else {
            console.log('No file path available at /api/surveyExcelWithAnswersUpload');
            res.send({result: ''});
        }
    } else {
        res.redirect('/login');
    }
});

app.post('/api/intelImageUpload', multer({
    storage: storageIntelManual,
    limits: {fileSize: 3000000},
    fileFilter: fileFilter
}).array('intelManualFinding', 20), (req, res) => {
    console.log('In /api/intelImageUpload');
    if (req.session.passport.user) {
        const filePaths = [];
        for (let i = 0; i < req.files.length; i++) {
            const fileName = {path: '/' + req.files[i].filename};
            filePaths.push(fileName);
        }
        console.log(filePaths.length + ' images saved.');
        res.send({result: filePaths});
    } else {
        res.redirect('/login');
    }
});

// Authenticate static requests for evidence pictures in survey page.
app.get('/intelImage/*', (req, res, next) => {
    console.log('In /intelManuals/*');
    if (!req.user && req.path.indexOf('/intelImage') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

const adminRequestLimiter = new RateLimit({
    windowMs: 60 * 60 * 1000, // 1 hour window
    delayAfter: 200, // begin slowing down responses after the first request
    delayMs: 3 * 1000, // slow down subsequent responses by 3 seconds per request
    max: 300, // start blocking after 20 requests
    message: 'Too many requests from this IP, please try again later'
});

// Authenticate static requests for evidence pictures in survey page.
app.post('/admin/*', adminRequestLimiter, (req, res, next) => {
    next();
});


// Authenticate static requests for survey excel files in survey page.
app.get('/surveyExcel/*', (req, res, next) => {
    console.log('In /surveyExcel/*');
    if (!req.user && req.path.indexOf('/surveyExcel') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

// Authenticate static requests for Evidence files in survey answers reports.
app.get('/evidence/*', (req, res, next) => {
    console.log('In /evidence/*');
    if (!req.user && req.path.indexOf('/evidence') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

// Authenticate static requests for Logo files in in orgLogo.
app.get('/orgLogo/*', (req, res, next) => {
    console.log('In /orgLogo/*');
    if (!req.user && req.path.indexOf('/orgLogo') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

// Authenticate static requests for Logo files in in vendorAssessmentFiles.
app.get('/vendorAssessmentFiles/*', (req, res, next) => {
    console.log('In /vendorAssessmentFiles/*');
    if (!req.user && req.path.indexOf('/vendorAssessmentFiles') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

// Authenticate static requests for intelligence Reports files.
app.get('/intelReport/*', (req, res, next) => {
    console.log('In /intelReports/*');
    if (!req.user && req.path.indexOf('/intelReport') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

// Authenticate static requests for survey Reports files.
app.get('/surveyReport/*', (req, res, next) => {
    console.log('In /surveyReports/*');
    if (!req.user && req.path.indexOf('/surveyReport') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

// Authenticate static requests for intelligence Reports files.
app.get('/cveReport/*', (req, res, next) => {
    console.log('In /cveReports/*');
    if (!req.user && req.path.indexOf('/cveReport') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

// Authenticate static requests for Excel Templates in survey creation page.
app.get('/excelTemplates/*', (req, res, next) => {
    console.log('In /excelTemplates/*');
    if (!req.user && req.path.indexOf('/excelTemplates') === 0) {
        res.status(403);
        res.send();
    } else {
        res.locals.authenticated = true;
        next();
    }
});

// Authenticate static requests for evidence pictures in survey-continue page (using web token).
app.get('/evdnc/*', jwtAuth.authenticate());
app.use('/evidence', express.static(path.join(__dirname, '../../evidence')));
app.use('/orgLogo', express.static(path.join(__dirname, '../../orgLogo')));
app.use('/vendorAssessmentFiles', express.static(path.join(__dirname, '../../vendorAssessmentFiles')));
app.use('/evdnc', express.static(path.join(__dirname, '../../evidence')));
app.use('/cveReport', express.static(path.join(__dirname, '../../cveReports')));
app.use('/intelReport', express.static(path.join(__dirname, '../../intelReports')));
app.use('/surveyReport', express.static(path.join(__dirname, '../../surveyReports')));
app.use('/intelImage', express.static(path.join(__dirname, '../../intelManuals')));
app.use('/surveyExcel', express.static(path.join(__dirname, '../../surveyExports')));

app.get('/survey-continue', jwtAuth.authenticate(), async (req, res) => {
    console.log('In survey-continue');
    let isFound = false;

    try {
        const results = await intelHelpers.getAllRevokedTokens();
        if (results) {
            isFound = results.some((el) => el.token === req.query.token);
        }
    } catch (e) {
        console.log('getAllRevokedTokens: ', e);
    }

    if (!isFound) {
        const options = {
            maxAge: 1000 * 60 * 60 * 24 * 3, // would expire after 3 days
            httpOnly: false // The cookie accessible by browser and not only web server
        };
        jwt.verify(req.query.token, authCfg.jwtSecret, function(err, decoded) {
            res.cookie('uid', decoded.uid.toString(), options); // options is optional
            res.sendFile(path.join(__dirname, '../../dist/public/index-survey.html'));
        });
    } else {
        res.sendStatus(401);
    }
});

app.post('/api/getSurveyContinueWithAnswers', jwtAuth.authenticate(), function(req, res) {
    jwt.verify(req.body.token, authCfg.jwtSecret, function(err, decoded) {
        console.log('In getSurveyContinueWithAnswers');
        seneca.client({
            host: 'localhost',
            port: 10701
        });

        promiseAct('role:survey, get:surveyWithAnswers', {
            query: {
                uid: decoded.uid,
                said: req.body.said
            }
        }).then((data) => {
            console.log('Getting survey Continue Answers');
            if (data) {
                data.senderEmail = decoded.senderEmail;
            }

            // Get all surveyAnswers' files and then check in fileScans in DB for findings.
            const allSurveyFiles = serverHelpers.getFilesOfSurveyWithAnswers(data);

            console.log('Getting survey Continue Answers\' files scans status');

            // Fetch surveyAnswers' files and check if any fileNames exist as not SAFE files.
            return [promiseAct('role:fileScans, getScanUploadedFiles:fileScan', {
                files: allSurveyFiles
            }), data];
        }).catch((e) => {
            console.log('Error in getSurveyContinueWithAnswers: ', e);
        }).spread((filesResult, data) => {
            if (filesResult && filesResult.ok && filesResult.data && Array.isArray(filesResult.data) && filesResult.data.length > 0) {
                const files = filesResult.data;
                // Save the updated filesFindings in the questionnaire (data).
                data = serverHelpers.updateFilesFindingsOfSurveyWithAnswers(files, data);
            }

            // data$ Turns seneca object to normal JS object.
            res.send(data.data$(false));
        }).catch((e) => {
            console.log('Error in getSurveyContinueWithAnswers-getScanUploadedFiles: ', e);
            done();
        });
    });
});

app.post('/api/saveSurveyContinueWithAnswers', jwtAuth.authenticate(), function(req, res) {
    jwt.verify(req.body.token, authCfg.jwtSecret, (err, decoded) => {
        console.log('In saveSurveyContinueWithAnswers');
        seneca.client({
            host: 'localhost',
            port: 10701
        });
        req.body.questionnaire.progress = serverHelpers.getProgress(req.body.questionnaire);
        promiseAct('role:surveystore, updateAnswers:surveyAnswers', {
                said: decoded.said,
                data: req.body.questionnaire
            }
        ).then((data) => {
            console.log('Saving survey Answers');
            res.send(data);
        }).catch((e) => {
            console.log(e);
        });
    });
});

app.post('/api/getMaxCVSScoreFromServer', jwtAuth.authenticate(), function(req, res) {
    jwt.verify(req.body.token, authCfg.jwtSecret, (err, decoded) => {
        console.log('In getMaxCVSScoreFromServer');
        seneca.client({
            host: 'localhost',
            port: 10701
        });
        promiseAct('role:intelQuery,cmd:getMaxCVSScore', {
            companyID: req.body.companyID,
            saveScoresOnDB: req.body.saveScoresOnDB,
        }).then((data) => {
            console.log('CVSS Score has been calculated!');
            res.send(data);
        }).catch((e) => {
            console.log(e);
        });
    });
});

app.post('/api/updateCompanySurveyContinueScore', jwtAuth.authenticate(), function(req, res) {
    jwt.verify(req.body.token, authCfg.jwtSecret, (err, decoded) => {
        console.log('In updateCompanySurveyContinueScore');

        seneca.client({
            host: config.addresses.storeServiceAddress,
            port: 10904
        });

        promiseAct('role:companies, update:company', {
            data: {
                id: req.body.id,
                surveyScore: req.body.surveyScore,
                isUnsafeCompany: req.body.isUnsafeCompany || false
            }
        })
            .then(() => {
                console.log('Updated Company Score');
                res.send({ok: true});
            })
            .catch((e) => {
                console.log('getAllCompaniesById: ', e);
            });
    });
});

app.post('/api/surveyImageContinueUpload', jwtAuth.authenticate(), multer({
    storage: storage,
    limits: {fileSize: 3000000},
    fileFilter: fileFilter
}).array('surveyEvidence', 3), function(req, res) {
    jwt.verify(req.body.token, authCfg.jwtSecret, () => {
        console.log('In surveyImageContinueUpload');
        let path = '../../../evidence/';
        // Same as in surveyImageUpload.
        serverHelpers.uploadImageOrFileToSurveyAnswers(req, res, path, promiseAct);
    });
});

app.post('/api/sendDoneNotification', jwtAuth.authenticate(), (req, res) => {
    jwt.verify(req.body.token, authCfg.jwtSecret, function(err, decoded) {
        const url = config.addresses.website + '/survey?said=' + decoded.said;
        let text = '<!DOCTYPE html><html><head></head><body><div>';
        text += '<h2>' + decoded.surveyName + '</h2>';
        text += '<p>The survey has been answered.</p>';
        text += '<p>You can visit it here: </p>';
        text += '<a href="' + url + '" target="_blank">' + url + '</a><br/>';

        text += '<br/><b>Rescana</b>';
        text += '</div></body></html>';
        const msgObj = {
            to: decoded.senderEmail,
            subject: 'Security Survey - ' + decoded.surveyName,
            text: text
        };

        return promiseAct('role:mailer, cmd:send', msgObj)
            .then(() => {
                console.log('Survey Email has been sent');
                res.send({sent: 'ok'});
            })
            .catch((e) => {
                console.log('role:mailer, cmd:send', e);
            });
    });
});

const senecaWebConfig = {
    adapter: require('seneca-web-adapter-express'),
    context: app,
    options: {parseBody: false}, // so we can use body-parser
    routes: routes,
    auth: passport
};

seneca
    .use(plugin)
    .use(web, senecaWebConfig)
    .use('user', {confirm: true, failedLoginCount: 5})
    .use('mongo-store', {
        name: 'darkvision',
        host: config.db.hostname,
        port: config.db.port,
        username: config.db.username,
        password: config.db.password
    });

seneca.ready(async () => {
    const server = seneca.export('web/context')();

    // send all requests to index.html so browserHistory in React Router works
    server.get('*', function(req, res) {
        res.sendFile(path.join(__dirname, '../../dist/js/index.html'));
    });

    server.listen(3002, function() {
        console.log('Rescana listening on port 3002');
    });
    await runCheckOnCompAndDomainOnNeo();
});


/**
 *  This is the API configuration
 *  */
// [START setup]
const api_app = express();
const {getSurveyData, getScore, getAllScores, addCompany} = require('../api_server/api-helpers.js');

// Create RateLimit.
const apiRequestLimiter = new RateLimit({
    windowMs: 60 * 60 * 1000, // 1 hour window
    delayAfter: 20, // begin slowing down responses after the first 20 requests
    delayMs: 3 * 1000, // slow down subsequent responses by 3 seconds per request
    max: 50, // start blocking after 50 requests
    message: 'Too many requests from this IP, please try again later'
});

api_app.set('case sensitive routing', true);
api_app.use(bodyParser.json({limit: '10mb'}));
// Limit all API get requests as configured.
api_app.get('/*', apiRequestLimiter, (req, res, next) => {
    next();
});
api_app.post('/*', apiRequestLimiter, (req, res, next) => {
    next();
});
// [END setup]

// [Routes setup]
api_app.get('/v1/company/survey/:companyID', getSurveyData);
api_app.get('/v1/company/score/:companyID', getScore);
api_app.get('/v1/company/score', getAllScores);
api_app.post('/v1/company', addCompany);

// This if determine when a file is run directly from Node.js [and not by require('...')]
if (module === require.main) {
    // [START listen]
    const PORT = process.env.PORT || 8080;
    api_app.listen(PORT, () => {
        console.log(`API App listening on port ${PORT}`);
    });
    // [END listen]
}
// [END app]
