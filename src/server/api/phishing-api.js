const Promise = require('bluebird');
const shortid = require('shortid');
const config = require('app-config');
const helpers = require('./api-helpers');

module.exports = function(seneca) {
    const promiseAct = Promise.promisify(seneca.act, {context: seneca});

    return {
        getFeed: (args, done) => {
            seneca.client({
                host: config.addresses.storeServiceAddress,
                port: 10904
            });
            const aid = args.aid || args.request$.query.aid;
            promiseAct('role:publicstore, list:app', {query: {aid: aid}})
                .then((results) => {
                    buildFeedItems(results, done);
                });
        },

        // Start Brand Protection Query - set app name to DB and get all the app names.
        setAppName: (arg, done) => {
            seneca.client({
                host: config.addresses.storeServiceAddress,
                port: 10904
            });

            promiseAct('role:customerstore, set:app', {data: {url: arg.request$.query.url}})
                .then(() => {
                    promiseAct('role:customerstore, list:app', {query: {}}).then((apps) => {
                        prepereDataForMenu(apps, arg.uid, done);
                    });
                });
        },
        getAppNames: (args, done) => {
            seneca.client({
                host: config.addresses.storeServiceAddress,
                port: 10904
            });

            const id = args.request$.session.passport.user.id;
            const projectName = args.request$.query.pName;
            helpers.isUserRoleAllowed(id, ['user', 'basic'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:projects, listProjectsById:project', {
                            query: {uid: id, projectName: projectName}
                        });
                    }
                })
                .then((projectId) => {
                    // Get all protected apps for this uid.
                    return promiseAct('role:customerstore, listByUidAndPid:app', {
                        query: {
                            uid: id,
                            pid: projectId[0]._id
                        }
                    });
                })
                .then((apps) => {
                    prepereDataForMenu(apps, args.request$.query.uid, done);
                });
        },

        // Enrich user with App
        saveAppAndenrich: (arg, done) => {
            seneca.client({
                host: 'localhost',
                port: 11104
            });
            const url = arg.request$.query.url;
            let date;
            const projectName = arg.request$.query.pName;
            const id = arg.request$.session.passport.user.id;

            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:customerstore, list:app', {query: {url: url}});
                    }
                })
                .then((customerAppsArr) => {
                    return [promiseAct('role:projects, listProjectsById:project', {
                        query: {uid: id, projectName: projectName}
                    }), customerAppsArr];
                })
                .spread((projectId, customerAppsArr) => {
                    if (customerAppsArr.length === 0) {
                        date = new Date().getTime();
                        return promiseAct('role:customerstore, set:app',
                            {
                                data: {
                                    url: url,
                                    uid: [id],
                                    pid: [projectId[0]._id],
                                    timestamp: date,
                                    dnsTwistTimeStamp: date,
                                    aid: shortid.generate()
                                }
                            }
                        ).then(() => {
                            promiseAct('role:customerenrich, enrich:app', {'url': url});
                            return promiseAct('role:customerstore, list:app', {query: {}});
                        }).catch((e) => {
                            console.log(e);
                        }).then((apps) => {
                            prepereDataForMenu(apps, id, done);
                        });
                    } else {
                        for (const customerApp of customerAppsArr) {
                            if (customerApp.uid && customerApp.uid.indexOf(id) === -1) {
                                customerApp.uid.push(id);

                                if (customerApp.pid && customerApp.pid.indexOf(projectId[0]._id) === -1) {
                                    customerApp.pid.push(projectId[0]._id);
                                }

                                promiseAct('role:customerstore, update:app', {
                                    data: {
                                        id: customerApp.id,
                                        uid: customerApp.uid,
                                        pid: customerApp.pid
                                    }
                                }).then(() => {
                                    promiseAct('role:customerstore, list:app', {query: {}}).then((apps) => {
                                        prepereDataForMenu(apps, id, done);
                                    });
                                });
                            } else {
                                // This is ok because we have only one result in results.
                                done();
                                break;
                            }
                        }
                    }
                });
        },
        // Create a new public app
        createWatchedPublicApp: (arg, done) => {
            seneca.client({
                host: 'localhost',
                pin: 'role:dnstwist',
                port: 11111
            });

            const data = {
                aid: arg.request$.query.aid,
                url: arg.request$.query.url,
                isWatchedPublicApp: true
            };

            promiseAct('role:dnstwist, twist:publicApp', {data: data})
                .then(() => {
                    this.getFeed({aid: data.aid}, done);
                })
                .catch((e) => {
                    throw e;
                });
        },

        removeCustomerApp: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:projects, listProjectsById:project', {
                            query: {uid: id, projectName: arg.request$.query.pName}
                        });
                    }
                })
                .then((projectId) => {
                    return promiseAct('role:customerstore, softRemove:app', {
                        aid: arg.request$.query.aid,
                        uid: id,
                        pid: projectId[0]._id
                    });
                })
                .then(() => {
                    done(null, {id: arg.request$.query.uid});
                });
        },

        removePublicApp: (arg, done) => {
            const aid = arg.args.body.aid;

            promiseAct('role:publicstore, remove:app', {id: arg.request$.body.id, aid: aid})
                .then(() => {
                    this.getFeed({aid: aid}, done);
                })
                .catch((e) => {
                    throw e;
                });
        }
    };
};

function buildFeedItems(results, cb) {
    const feedItems = [];
    let i = 0;

    for (const result of results) {
        feedItems[i] = {};
        feedItems[i]['id'] = result.data$().id;
        feedItems[i]['aid'] = result.data$().aid;
        feedItems[i]['url'] = result.data$().url;
        feedItems[i]['icon'] = result.data$().screenshot;
        feedItems[i]['screenShotScore'] = parseInt(result.data$().screenShotCompareScore);
        feedItems[i]['contentScore'] = parseInt(result.data$().contentCompareScore * 100);
        feedItems[i]['domainScore'] = parseInt(result.data$().domainCompareScore * 100);
        feedItems[i]['geoIPCountry'] = result.data$().dnstwistDGeoIpCountry;
        feedItems[i]['dnsARecord'] = result.data$().dnstwistDnsARecord;
        feedItems[i]['dnsMx'] = result.data$()['dnstwistDns-mx'];
        feedItems[i]['dnsNs'] = result.data$()['dnstwistDns-ns'];
        feedItems[i]['reason'] = result.data$().reason;
        feedItems[i]['isWatched'] = result.data$().isWatched;
        i++;
    }
    cb(null, feedItems);
}

// Turn app name object to array.
function prepereDataForMenu(apps, uid, cb) {
    const appNames = {};
    for (const app of apps) {
        if (app.uid) {
            for (let i = 0; i < app.uid.length; i++) {
                // If customerapp is related to this uid, show it
                if (app.uid[i] && app.uid[i].indexOf(uid) !== -1) {
                    if (app && app.data$) {
                        appNames[app.aid] = app.data$().url;
                    } else {
                        appNames[app.aid] = app.url;
                    }
                }
            }
        }
    }
    cb(null, appNames);
}
