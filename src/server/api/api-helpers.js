const PromiseB = require('bluebird');
const config = require('app-config');
const path = require('path');
const fs = require('fs');
const moment = require('moment');
const shortid = require('shortid');
const pn = require('port-numbers');
const Jimp = require('jimp');
const mongo = require('mongodb');
const ObjectId = mongo.ObjectId;
const neoHelpers = require('helpers');
const {WILDFIRE_URL} = require('../../../config/consts.js');
const {DEFAULT_INTEL_SCORE_RATIOS, API_KEY_STATUS} = require('../../../config/server-consts.js');
const createWildfireClient = require('wildfire-client').create;
const wildfireClient = createWildfireClient({
    apikey: config.consts.wildFireAPI, // config.consts.WF_API,
    baseUrl: WILDFIRE_URL, // https://wildfire.paloaltonetworks.com/publicapi/
    json: true // api is xml only, client library converts the xml to json using xml2js
});
const neo4jDriver = require('neo4j-driver').v1;
const neo4jRetried = require('@ambassify/neo4j-retried');
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass), {maxTransactionRetryTime: 30000}), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});
const MongoClient = mongo.MongoClient;
const mongoUrl = config.addresses.mongoConnectionString;
const extraHelpers = require('./helpers.js');
const intelHelpers = require('../../server_modules/utils/intel-helpers.js');
const sha1 = require('sha1');
const cryptoRandomString = require('crypto-random-string');
const RANDOM_SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';


module.exports.WILDFIRE_STATUS_TYPES = {
    SAFE: 'safe',
    SCANNING: 'scanning',
    MALICIOUS: 'malicious'
};
const neoAndMongoHelpers = require('helpers');

const API_KEYS_COLLECTION = 'apiKeys_map';


module.exports.DEFAULT_INTEL_SCORE_RATIOS = DEFAULT_INTEL_SCORE_RATIOS;


module.exports.saveTokenInfoToDB = (data, curToken) => {

    const tokenData = {
        uid: data.request$.session.passport.user.id,
        userEmail: data.request$.session.passport.user.email,
        tokenCreationTime: data.request$.body.tokenCreationTime,
        tokenExpirationTime: data.request$.body.tokenExpirationTime,
        said: data.request$.body.said,
        senderEmail: data.request$.body.senderEmail,
        surveyName: data.request$.body.surveyName,
        companyName: data.request$.body.companyName,
        companyId: data.request$.body.companyId,
        isSentWithEmail: data.request$.body.isSentWithEmail,
        receiverEmail: data.request$.body.recipient ? data.request$.body.recipient : 'N/A',
        token: curToken,
        isTokenRevoked: false
    };

    let {said, senderEmail, tokenCreationTime, tokenExpirationTime, userEmail, uid, companyId, companyName, token} = tokenData;

    return new Promise((resolve) => {
        MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let Qcollection = db.collection('tokens_map');

            if (uid && userEmail && tokenCreationTime && tokenExpirationTime && said && senderEmail && companyId && companyName && token) {
                Qcollection.insert(tokenData, {upsert: true}, () => {
                    client.close();
                    resolve(true);
                });
            } else {
                client.close();
                resolve(false);
            }
        });
    });
};
module.exports.addNewUserCreatedCompanyToProject = (projectId, newCompany) => {
    if (projectId && newCompany.companyName && newCompany.id) {
        const companyData = {name: newCompany.companyName, id: newCompany.id};
        const projId = ObjectId(projectId.toString());
        try {
            return MongoClient.connect(mongoUrl, (err, client) => {
                const db = client.db('darkvision');
                let orgCollection = db.collection('project_map');

                orgCollection.update({_id: projId}, {$addToSet: {'selectedCompanies': companyData}}, {upsert: true}, () => {
                    console.log('Project updated!');
                    client.close();
                });
                client.close();
            });
        } catch (e) {
            console.log('Failed To Update Project On DB: ' + e);
        }
    } else {
        console.log('Failed To Update Project On DB: ' + e);
    }
};


const generateNewApiKey = () => {
    // Generates 2 random strings.
    const prefix = cryptoRandomString({length: 8, characters: RANDOM_SYMBOLS});
    const key = cryptoRandomString({length: 35, characters: RANDOM_SYMBOLS});

    // Create the api key. (for user)
    const apiKey = prefix + '.' + key;
    // Hash the api key. (for DB)
    const hashedKey = prefix + '.' + sha1(apiKey);

    return {
        apiKey: apiKey,
        hashedKey: hashedKey
    };
};

module.exports.saveApiKeyToDB = (data, isEditMode = false) => {
    return new Promise(async (resolve) => {

        if (data && data.organizations && Array.isArray(data.organizations)) {
            try {
                const timeNow = moment().format('YYYY-MM-DD, HH:mm:ss');

                let objectToSave;

                let originalApiKey;

                if (isEditMode && data.key) {
                    objectToSave = {
                        key: data.key,
                        organizations: data.organizations,
                        lastUpdated: timeNow,
                        isScoreBackwardsEnabled: data.isScoreBackwardsEnabled || false
                    };
                } else {
                    const keyResult = generateNewApiKey();

                    if (keyResult && keyResult.apiKey && keyResult.hashedKey) {
                        objectToSave = {
                            key: keyResult.hashedKey,
                            organizations: data.organizations,
                            status: API_KEY_STATUS.ACTIVE,
                            dateCreated: timeNow,
                            lastUpdated: timeNow,
                            isScoreBackwardsEnabled: data.isScoreBackwardsEnabled || false
                        };

                        originalApiKey = keyResult.apiKey;
                    }
                }

                if (objectToSave) {
                    const client = await MongoClient.connect(mongoUrl);

                    const db = client.db('darkvision');

                    const apiKeysCollection = db.collection(API_KEYS_COLLECTION);

                    apiKeysCollection.update({key: objectToSave.key}, {$set: objectToSave}, {upsert: true}, () => {
                        console.log('API Key successfully updated.');
                        client.close();

                        resolve({ok: true, data: objectToSave, originalApiKey: originalApiKey});
                    });
                } else {
                    console.error('Failed to generate new API Key');
                    resolve({ok: false, error: 'Failed to generate new API Key'});
                }
            } catch (e) {
                console.error('Error in saveApiKeyToDB: ', e);
                resolve({ok: false, error: e});
            }
        } else {
            resolve({ok: false, error: 'Wrong parameters inserted'});
        }
    });
};

module.exports.editApiKeyStatusInDB = (data) => {
    return new Promise(async (resolve) => {

        if (data && data.key && data.newStatus && Object.values(API_KEY_STATUS).includes(data.newStatus)) {
            try {
                const objectIdentifier = {
                    key: data.key
                };

                const objectToUpdate = {
                    status: data.newStatus,
                    lastUpdated: moment().format('YYYY-MM-DD, HH:mm:ss')
                };

                const client = await MongoClient.connect(mongoUrl);

                const db = client.db('darkvision');

                const apiKeysCollection = db.collection(API_KEYS_COLLECTION);

                apiKeysCollection.update(objectIdentifier, {$set: objectToUpdate}, {upsert: true}, () => {
                    console.log('API Key successfully updated.');
                    client.close();

                    resolve({ok: true});
                });
            } catch (e) {
                console.error('Error in editApiKeyStatusInDB: ', e);
                resolve({ok: false, error: e});
            }
        } else {
            resolve({ok: false, error: 'Wrong parameters inserted'});
        }
    });
};

module.exports.deleteApiKeyFromDB = (data) => {
    return new Promise(async (resolve) => {

        if (data && data.keyToDelete) {
            try {
                const objectToDelete = {
                    key: data.keyToDelete
                };

                const client = await MongoClient.connect(mongoUrl);

                const db = client.db('darkvision');

                const apiKeysCollection = db.collection(API_KEYS_COLLECTION);

                apiKeysCollection.deleteOne(objectToDelete, () => {
                    console.log('API Key Deleted!');
                    client.close();
                    resolve({ok: true});
                });
            } catch (e) {
                console.error('Error in deleteApiKeyFromDB: ', e);
                resolve({ok: false, error: e});
            }
        } else {
            resolve({ok: false, error: 'Wrong parameters inserted'});
        }
    });
};

module.exports.saveOrganizationToDB = (orgData) => {

    const organizationData = {
        'allowedUsers': orgData.allowedUsers,
        'organizationName': orgData.organizationName,
        'selectedProjects': orgData.selectedProjects,
        'createDate': orgData.createDate
    };

    if (orgData.apiAuditorEmail) {
        organizationData.apiAuditorEmail = orgData.apiAuditorEmail;
    }
    if (orgData.apiSelectedTemplate && Array.isArray(orgData.apiSelectedTemplate) && orgData.apiSelectedTemplate.length > 0) {
        organizationData.apiSelectedTemplate = orgData.apiSelectedTemplate;
    }
    if (orgData.hasOwnProperty('isSendAutoSurveyFromApi') && typeof orgData.isSendAutoSurveyFromApi === 'boolean') {
        organizationData.isSendAutoSurveyFromApi = orgData.isSendAutoSurveyFromApi;
    }

    if (orgData.hasOwnProperty('showScanDate') && typeof orgData.isSendAutoSurveyFromApi === 'boolean') {
        organizationData.showScanDate = orgData.showScanDate;
    }

    let orgValidObjId = '';

    if (orgData._id && orgData._id !== '' && ObjectId.isValid(orgData._id)) {
        orgValidObjId = ObjectId(orgData._id.toString());
    }

    let {organizationName, selectedProjects, createDate} = organizationData;

    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let orgCollection = db.collection('organizations_map');

            if (organizationName && selectedProjects && createDate) {
                if (orgValidObjId !== '') {
                    orgCollection.update({_id: orgValidObjId}, {$set: organizationData}, {upsert: true}, () => {
                        console.log('Organization updated!');
                        client.close();
                        resolve(orgData);
                    });
                } else {
                    orgCollection.insert(organizationData, {upsert: true}, (err, re) => {
                        console.log('New Organization added to DB!');
                        client.close();
                        resolve(re.ops[0]);
                    });
                }
            } else {
                console.log('Failed To Save Organization To DB: ' + e);
                client.close();
                reject();
            }
        });
    });
};

module.exports.updateEmailsToBeNotifiedOnVendorCreationOnMongo = (data) => {
    try {
        return MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let orgCollection = db.collection('notify_emails_map');

            if (data.emailsToInclude && data.emailsToInclude.length > 0) {
                let emailsObjArr = [];

                data.emailsToInclude.map((email) => {
                    emailsObjArr.push({email: email.toString()});
                });

                orgCollection.insertMany(emailsObjArr, {upsert: true}, () => {
                    console.log('In updateEmailsToBeNotifiedOnVendorCreationOnMongo - done update admin users emails that would be notified.');
                });
            }

            if (data.emailsToRemove && data.emailsToRemove.length > 0) {
                orgCollection.remove({email: {$in: data.emailsToRemove}}, {upsert: true}, () => {
                    console.log('In updateEmailsToBeNotifiedOnVendorCreationOnMongo - done update admin users emails that would not be notified.');
                });
            }
            console.log('In updateEmailsToBeNotifiedOnVendorCreationOnMongo - success.');
            client.close();
        });
    } catch (e) {
        console.log('In updateEmailsToBeNotifiedOnVendorCreationOnMongo - Error: ' + e);
    }
};


module.exports.deleteOrganizationFromDB = (orgId) => {
    let orgValidObjId = '';

    if (orgId && orgId !== '') {
        orgValidObjId = ObjectId(orgId.toString());
    }
    try {
        return MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let orgCollection = db.collection('organizations_map');
            if (orgValidObjId !== '') {
                orgCollection.deleteOne({_id: orgValidObjId}, () => {
                    console.log('Organization Deleted!');
                    client.close();
                });
            } else {
                console.log('Failed To Delete Organization!');
                client.close();
            }

        });
    } catch (e) {
        console.log('Failed To Delete Organization: ' + e);
    }
};

const addProjectsToOrgOnMongo = (org, projects) => {
    let projArr = [];

    if (projects && !Array.isArray(projects)) {
        projects = [projects];
    }

    if (projects && projects.length > 0) {
        projects.map((project) => {
            projArr.push({name: project.projectName || project.name, id: project._id || project.id});
        });
    }
    let orgId = '';
    if (org.id && org.id !== '') {
        orgId = org.id;
    } else if (org._id && org._id !== '') {
        orgId = org._id;
    }

    try {
        return MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let orgCollection = db.collection('organizations_map');
            orgCollection.update({_id: ObjectId(orgId.toString())}, {$addToSet: {selectedProjects: {$each: projArr}}}, () => {
                console.log('projects to organization added!');
                client.close();
            });
        });
    } catch (e) {
        console.log('In addProjectsToOrgOnMongo - Failed To add project to organization: ' + e);
    }
};

module.exports.addProjectsToOrgOnMongo = addProjectsToOrgOnMongo;

module.exports.removeAllowedUsersFromOrgProjects = (orgData, allProjects, allCompanies, userIdToRemove) => {
    return MongoClient.connect(mongoUrl, async (err, client) => {
        let companiesToUpdate = [];
        try {
            const db = client.db('darkvision');
            let projectCollection = db.collection('project_map');
            const projectsArray = extraHelpers.getFullSelectedObjectsInfoById(allProjects, orgData.selectedProjects);

            await extraHelpers.asyncForEach(projectsArray, async (curProject) => {
                companiesToUpdate = companiesToUpdate.concat(curProject.selectedCompanies);
                projectCollection.update(
                    {_id: ObjectId(curProject.id.toString())},
                    {$pull: {'allowedUsers': {'id': userIdToRemove}}},
                    {upsert: true}, () => {
                        console.log('Added allowed users to Organization projects');
                    });
            });
            client.close();
            await removeAllowedUsersFromCompanies(companiesToUpdate, userIdToRemove);
        } catch (e) {
            console.log(e);
            client.close();
        }
    });
};

const removeAllowedUsersFromCompanies = (companiesToUpdate, userIdToRemove) => {
    return MongoClient.connect(mongoUrl, async (err, client) => {
        try {
            const db = client.db('darkvision');
            let companiesArray = companiesToUpdate;
            let companyCollection = db.collection('company_map');

            await extraHelpers.asyncForEach(companiesArray, async (curCompany) => {
                companyCollection.update(
                    {_id: ObjectId(curCompany.id.toString())},
                    {$pull: {'allowedUsers': {'id': userIdToRemove}, 'allowedEditUsers': {'id': userIdToRemove}}},
                    {upsert: true}, () => {
                        console.log('removed allowed users from companies');
                    });
            });
            client.close();
        } catch (e) {
            console.log(e);
            client.close();
        }
    });
};
module.exports.removeAllowedUsersFromCompanies = removeAllowedUsersFromCompanies;

module.exports.removeProjectFromOrganizationsOnMongo = (organizationsToUpdate, projectIdToRemove) => {
    return MongoClient.connect(mongoUrl, (err, client) => {
        try {
            const db = client.db('darkvision');
            let collection = db.collection('organizations_map');
            organizationsToUpdate.map((org) => {
                collection.update(
                    {_id: org._id},
                    {$pull: {'selectedProjects': {'id': projectIdToRemove}}},
                    {upsert: true}, () => {
                        console.log('removed project from organizations');
                        client.close();
                    });
            });
        } catch (e) {
            console.log(e);
            client.close();
        }
    });
};


module.exports.addAllowedUsersToOrgProjects = (orgData, allProjects, allCompanies, userToAdd) => {
    return MongoClient.connect(mongoUrl, async (err, client) => {
        let companiesToUpdate = [];
        try {
            const db = client.db('darkvision');
            let projectCollection = db.collection('project_map');
            let projectsArray = extraHelpers.getFullSelectedObjectsInfoById(allProjects, orgData.selectedProjects);

            await extraHelpers.asyncForEach(projectsArray, async (curProject) => {
                companiesToUpdate = companiesToUpdate.concat(curProject.selectedCompanies);

                let exists = extraHelpers.checkIfExistsInArrById(curProject.allowedUsers, userToAdd);
                if (!exists) {
                    projectCollection.update(
                        {_id: ObjectId(curProject.id.toString())},
                        {$push: {'allowedUsers': userToAdd}},
                        {upsert: true}, () => {
                            console.log('Added allowed users to Organization projects');
                        });
                } else {
                    console.log('Added allowed users to Organization projects');
                }
            });
            client.close();
            let companiesFullDataArray = extraHelpers.getFullSelectedObjectsInfoById(allCompanies, companiesToUpdate);
            await addAllowedUsersToCompanies(companiesFullDataArray, userToAdd);
        } catch (e) {
            console.log(e);
            client.close();
        }
    });
};

const addAllowedUsersToCompanies = (companiesToUpdate, userToAdd) => {
    return MongoClient.connect(mongoUrl, async (err, client) => {
        try {
            const db = client.db('darkvision');
            let companiesArray = companiesToUpdate;
            let companyCollection = db.collection('company_map');

            await extraHelpers.asyncForEach(companiesArray, async (curCompany) => {
                let exists = extraHelpers.checkIfExistsInArrById(curCompany.allowedUsers, userToAdd);
                if (!exists) {
                    companyCollection.update(
                        {_id: ObjectId(curCompany.id.toString())},
                        {$push: {'allowedUsers': userToAdd, 'allowedEditUsers': userToAdd}},
                        {upsert: true}, () => {
                            console.log('Added allowed users to companies');
                        });
                } else {
                    console.log('Added allowed users to companies');
                }
            });
            client.close();
        } catch (e) {
            console.log(e);
            client.close();
        }
    });
};
module.exports.addAllowedUsersToCompanies = addAllowedUsersToCompanies;

module.exports.getOrgById = (orgId) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                const db = client.db('darkvision');
                const collection = db.collection('organizations_map');
                const find = PromiseB.promisify(collection.find, {context: collection});
                const list = await find({_id: ObjectId(orgId.toString())});
                if (list) {
                    const result = await listToArray(list);
                    if (result) {
                        client.close();
                        resolve(result);
                    }
                }
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};

module.exports.getOrganizationsByUserId = (userId) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                const db = client.db('darkvision');
                const collection = db.collection('organizations_map');
                const find = PromiseB.promisify(collection.find, {context: collection});

                const list = await find({'allowedUsers': {$elemMatch: {id: {$eq: userId}}}});
                if (list) {
                    const result = await listToArray(list);
                    if (result) {
                        client.close();
                        resolve(result);
                    }
                }
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};

module.exports.getOrganizationByProject = (projectId) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                const db = client.db('darkvision');
                const collection = db.collection('organizations_map');
                const find = PromiseB.promisify(collection.find, {context: collection});

                const list = await find({'selectedProjects': {$elemMatch: {id: {$eq: projectId}}}});
                if (list) {
                    const result = await listToArray(list);
                    if (result && Array.isArray(result)) {
                        client.close();
                        if (result[0] && result.length === 1) {
                            resolve(result[0]);
                        } else if (result.length > 1) {
                            resolve({message: 'Found more then one organization'});
                        } else {
                            resolve({message: 'No organization found.'});
                        }
                    } else {
                        client.close();
                        reject(e);
                    }
                }
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};

module.exports.getCompaniesInfoByCompaniesIdsArrFromMongo = (compObjIdsArr, userId) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                const db = client.db('darkvision');
                const collection = db.collection('company_map');
                const find = PromiseB.promisify(collection.find, {context: collection});

                const list = await find({_id: {$in: compObjIdsArr}, 'allowedUsers': {$elemMatch: {id: {$eq: userId}}}});
                if (list) {
                    const result = await listToArray(list);
                    if (result) {
                        client.close();
                        resolve(result);
                    }
                }
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};

module.exports.getDBDataWithPagination = (collectionName, sliceStartIndex, sliceMaxLength, searchQuery = null, sortMethod = null) => {
    return new Promise((resolve, reject) => {
        try {
            MongoClient.connect(mongoUrl, async (err, client) => {
                if (client) {
                    const db = client.db('darkvision');
                    const collection = db.collection(collectionName);

                    const findQuery = {};

                    if (searchQuery) {
                        if (searchQuery.companyName) {
                            // Search with regex any companyName containing searchQuery with case insensitive.
                            findQuery.companyName = {$regex: new RegExp(searchQuery.companyName, 'i')};
                        } else if (searchQuery.projectName) {
                            // Search with regex any projectName containing searchQuery with case insensitive.
                            findQuery.projectName = {$regex: new RegExp(searchQuery.projectName, 'i')};
                        } else if (searchQuery.organizationName) {
                            // Search with regex any organizationName containing searchQuery with case insensitive.
                            findQuery.organizationName = {$regex: new RegExp(searchQuery.organizationName, 'i')};
                        }
                    }

                    let sortQuery = null;

                    if (sortMethod) {
                        if (sortMethod === 'companyNameAsc') {
                            sortQuery = {companyName: 1};
                        } else if (sortMethod === 'companyNameDesc') {
                            sortQuery = {companyName: -1};
                        } else if (sortMethod === 'createDateAsc') {
                            sortQuery = {createDate: 1};
                        } else if (sortMethod === 'createDateDesc') {
                            sortQuery = {createDate: -1};
                        } else if (sortMethod === 'latestScanAsc') {
                            sortQuery = {lastScanDate: 1};
                        } else if (sortMethod === 'latestScanDesc') {
                            sortQuery = {lastScanDate: -1};
                        } else if (sortMethod === 'projectNameAsc') {
                            sortQuery = {projectName: 1};
                        } else if (sortMethod === 'projectNameDesc') {
                            sortQuery = {projectName: -1};
                        } else if (sortMethod === 'organizationNameAsc') {
                            sortQuery = {organizationName: 1};
                        } else if (sortMethod === 'organizationNameDesc') {
                            sortQuery = {organizationName: -1};
                        }
                    }

                    let dataCursor;

                    if (sortQuery) {
                        dataCursor = await collection.find(findQuery).sort(sortQuery).skip(((sliceStartIndex - 1) * sliceMaxLength) || 0).limit(sliceMaxLength);
                    } else {
                        dataCursor = await collection.find(findQuery).skip(((sliceStartIndex - 1) * sliceMaxLength) || 0).limit(sliceMaxLength);
                    }

                    const data = await listToArray(dataCursor);

                    const totalDataCount = await collection.find(findQuery).count();

                    resolve({
                        data: data,
                        totalDataCount: totalDataCount
                    });
                } else {
                    console.error('Error in getCompaniesWithPagination-MongoClient.connect: ', err);
                    reject(err);
                }
            });
        } catch (e) {
            console.error('Error in getCompaniesWithPagination: ', e);
            reject(e);
        }
    });
};

module.exports.getDBDataWithSpecificFields = (collectionName, fieldNamesToReturn = null) => {
    return new Promise((resolve) => {
        try {
            MongoClient.connect(mongoUrl, async (err, client) => {
                if (client) {
                    const db = client.db('darkvision');
                    const collection = db.collection(collectionName);

                    const projectionFields = {};

                    if (fieldNamesToReturn && Array.isArray(fieldNamesToReturn) && fieldNamesToReturn.length > 0) {
                        fieldNamesToReturn.map((currField) => {
                            if (currField && typeof currField === 'string') {
                                projectionFields[currField] = 1;
                            }
                        });
                    }

                    let dataCursor = await collection.find({}, projectionFields);

                    const data = await listToArray(dataCursor);

                    resolve({
                        data: data
                    });
                } else {
                    console.error('Error in getDBDataWithSpecificFields-MongoClient.connect: ', err);
                    resolve({error: err});
                }
            });
        } catch (e) {
            console.error('Error in getDBDataWithSpecificFields: ', e);
            resolve({error: e});
        }
    });
};

const getAdminUsers = () => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                const db = client.db('darkvision');
                const collection = db.collection('sys_user');
                const find = PromiseB.promisify(collection.find, {context: collection});
                const list = await find({'userRole': {$eq: 'admin'}});
                if (list) {
                    const result = await listToArray(list);
                    if (result) {
                        client.close();
                        resolve(result);
                    }
                }
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};

module.exports.getAdminUsers = getAdminUsers;

module.exports.eraseAdminUsersFromOrgAllowedUsers = async (orgArr) => {
    try {
        const adminUsers = await getAdminUsers();
        if (adminUsers && orgArr && orgArr.length > 0) {
            let adminIdArr = [];
            adminUsers.map((adminUser) => {
                if (adminUser._id) {
                    adminIdArr.push(adminUser._id.valueOf().toString());
                }
            });
            orgArr.map((org) => {
                for (let i = 0; i < org.allowedUsers.length; i++) {
                    if (adminIdArr.includes(org.allowedUsers[i].id)) {
                        org.allowedUsers.splice(i, 1);
                    }
                }
            });
        } else {
            return orgArr;
        }
        return orgArr;
    } catch (e) {
        console.error('In eraseAdminUsersFromOrgAllowedUsers Error: ' + e);
        return orgArr;
    }
};

module.exports.getAllApiKeysFromDB = () => {
    return new Promise(async (resolve) => {
        let client;
        try {
            client = await MongoClient.connect(mongoUrl);

            const db = client.db('darkvision');

            const apiKeysCollection = db.collection(API_KEYS_COLLECTION);

            const find = PromiseB.promisify(apiKeysCollection.find, {context: apiKeysCollection});
            const list = await find({});
            if (list) {
                const result = await listToArray(list);
                client.close();
                if (result) {
                    resolve(result);
                } else {
                    resolve([]);
                }
            } else {
                client.close();
                resolve([]);
            }
        } catch (e) {
            if (client) {
                client.close();
            }
            resolve([]);
        }
    });
};

module.exports.getProjectsByUserId = (userId) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                const db = client.db('darkvision');
                const collection = db.collection('project_map');
                const find = PromiseB.promisify(collection.find, {context: collection});

                const list = await find({'allowedUsers': {$elemMatch: {id: {$eq: userId}}}});
                if (list) {
                    const result = await listToArray(list);
                    if (result) {
                        client.close();
                        resolve(result);
                    }
                }
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};

module.exports.getProjectsByOrgId = (orgId) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                const db = client.db('darkvision');
                const collection = db.collection('organizations_map');
                const find = PromiseB.promisify(collection.find, {context: collection});

                const list = await find({_id: ObjectId(orgId.toString())});
                if (list) {
                    const result = await listToArray(list);
                    if (result && result.length === 1) {
                        client.close();
                        resolve(result[0].selectedProjects);
                    }
                }
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};


const getAllFromMongoByCollectionName = (collectionName) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                const db = client.db('darkvision');
                if (collectionName && typeof collectionName === 'string') {
                    const collection = db.collection(collectionName);
                    const find = PromiseB.promisify(collection.find, {context: collection});
                    const list = await find({});
                    if (list) {
                        const result = await listToArray(list);
                        if (result) {
                            client.close();
                            resolve(result);
                        }
                    }
                } else {
                    console.error('In getAllFromMongoByCollectionName() - collection Name is not valid or exists');
                    client.close();
                    reject();
                }
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};
module.exports.getAllFromMongoByCollectionName = getAllFromMongoByCollectionName;

const listToArray = (list) => {
    return new Promise((resolve) => {
        list.toArray((err, docs) => {
            resolve(docs);
        });
    });
};

module.exports.getFilesOfSurveyWithAnswers = (data) => {
    // Get all surveyAnswers' files.
    let allSurveyFiles = [];
    if (data && data.pages && Array.isArray(data.pages)) {
        data.pages.map((currPage) => {
            if (currPage && currPage.elements && Array.isArray(currPage.elements)) {
                currPage.elements.map((currCategory) => {
                    if (currCategory && currCategory.elements && Array.isArray(currCategory.elements)) {
                        currCategory.elements.map((currQuestion) => {
                            if (currQuestion && currQuestion.files && Array.isArray(currQuestion.files) && currQuestion.files.length > 0) {
                                allSurveyFiles = allSurveyFiles.concat(currQuestion.files);
                            }
                        });
                    }
                });
            }
        });
    }
    return allSurveyFiles;
};

module.exports.updateFilesFindingsOfSurveyWithAnswers = (filesFindings, data) => {
    data.pages.map((currPage) => {
        currPage.elements.map((currCategory) => {
            currCategory.elements.map((currQuestion) => {
                if (currQuestion && currQuestion.files && Array.isArray(currQuestion.files) && currQuestion.files.length > 0) {
                    currQuestion.files.map((currFile) => {
                        if (currFile && currFile.path) {
                            const fileFinding = filesFindings.find((f) => {
                                return f.path === currFile.path;
                            });
                            if (fileFinding && fileFinding.scanStatus) {
                                currFile.scanStatus = fileFinding.scanStatus;
                            }
                        }
                    });
                }
            });
        });
    });
    return data;
};

module.exports.uploadImageOrFileToSurveyAnswers = (req, res, pathFolder, promiseAct) => {
    const filePaths = [];
    for (let i = 0; i < req.files.length; i++) {
        const fileName = {
            path: '/' + req.files[i].filename,
            scanStatus: this.WILDFIRE_STATUS_TYPES.SCANNING
        };
        let destinationFolder = config.paths.evidence;
        if (pathFolder === '../../orgLogo/') {
            destinationFolder = config.paths.orgLogo;
        } else if (pathFolder === '../../vendorAssessmentFiles/') {
            destinationFolder = config.paths.vendorAssessmentFiles;
        }
        this.convertFiles(req.files[i].filename, destinationFolder);
        filePaths.push(fileName);
    }

    PromiseB.map(req.files, (file) => {
        const fullFilePath = path.join(__dirname, pathFolder + file.filename);
        if (config.consts.wildFireAPI) {
            return wildfireClient.submitFile({file: fs.createReadStream(fullFilePath)});
        } else {
            console.log('No wildfire API defined!');
            filePaths.map((fileName) => {
                fileName.scanStatus = this.WILDFIRE_STATUS_TYPES.SAFE;
            });
            return PromiseB.resolve();
        }
    }).then((result) => {
        const scanningFilesToSave = [];
        if (result && Array.isArray(result) && result.length > 0) {
            result.map((currFileResult, index) => {
                if (currFileResult && currFileResult.wildfire && currFileResult.wildfire['upload-file-info']) {
                    const currDataToSave = currFileResult.wildfire['upload-file-info'];
                    currDataToSave.wildfireStatus = this.WILDFIRE_STATUS_TYPES.SCANNING;
                    currDataToSave.createDate = moment().format();
                    console.log(currDataToSave);
                    scanningFilesToSave.push(currDataToSave);
                    console.log('entity status is scanning!');
                } else {
                    // Otherwise, Mark this file as OK (because API can't check its type).
                    filePaths[index].scanStatus = this.WILDFIRE_STATUS_TYPES.SAFE;
                    console.log('the file is OK (because API can\'t check its type!');
                }
            });
        }
        if (scanningFilesToSave.length > 0) {
            return promiseAct('role:fileScans, saveScanUploadedFiles:fileScan', {
                files: scanningFilesToSave
            }).then((done) => {
                if (done && done.ok) {
                    console.log('Successfully saved uploaded fileScans data in DB.');

                    promiseAct('role:fileScans, updateScanUploadedFiles:fileScan', {
                        files: scanningFilesToSave
                    }).then((done) => {
                        if (done && done.ok && done.data) {
                            done.data.map((currFile) => {
                                if (currFile && currFile.filename && currFile.wildfireStatus) {
                                    // Search data in filePaths and update its status result.
                                    const currFilePath = filePaths.find((f) => {
                                        return f.path === '/' + currFile.filename;
                                    });
                                    if (currFilePath) {
                                        currFilePath.scanStatus = currFile.wildfireStatus;
                                    }
                                }
                            });
                        }
                        // Continue here with filePaths and block UI by scanStatus.
                        res.send({result: filePaths});
                    }).catch((err) => {
                        console.error('Failed to updateScanUploadedFiles: ', err);
                        res.send({result: filePaths});
                    });
                } else {
                    console.error('Error saving uploaded fileScans data in DB.');
                    res.send({result: filePaths});
                }
            }).catch((e) => {
                console.error('Failed to saveScanUploadedFiles: ', e);
                res.send({result: filePaths});
            });
        } else {
            res.send({result: filePaths});
        }
    }).catch((err) => {
        console.error(' Error In /api/surveyImageUpload: ' + err);
        res.send({result: filePaths});
    });
};

module.exports.isUserRoleAllowed = (id, roles, context) => {
    const user_entity = context.make$('sys', 'user');
    const list = PromiseB.promisify(user_entity.list$, {context: user_entity});
    return list({id: id}).then((user) => {
        if (user && user[0] && user[0].userRole) {
            for (let i = 0; i < roles.length; i++) {
                if (roles[i] === user[0].userRole || user[i].userRole === 'admin') {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }).catch((e) => {
        console.log('Error in isUserRoleAllowed: ', e);
    });
};

module.exports.checkWritePermissions = (uid, cid, data, context) => {
    const mongo = require('mongodb');
    const company_entity = context.make('companies', 'company', 'map');
    const native$ = PromiseB.promisify(company_entity.native$, {context: company_entity});
    if (data.keywords.length === 0 && data.allowedUsers.length === 0) {
        return native$()
            .then((db) => {
                const collection = db.collection('company_map');
                const find = PromiseB.promisify(collection.find, {context: collection});
                return find({
                    '_id': new mongo.ObjectID(cid),
                    'allowedEditUsers': {$elemMatch: {id: {$eq: uid}}}
                });
            })
            .then((list) => {
                return this.listToArray(list);
            })
            .then((docs) => {
                const result = [];
                docs.map((entity) => {
                    result.push(entity);
                });

                return result.length > 0;
            })
            .catch((e) => {
                console.log('Error getDataleaks: ', e);
            });
    }
};

module.exports.updateCompanyNameOnNeo = (companyID, companyName) => {
    console.log('In updateCompanyNameOnNeo');
    return new Promise((resolve) => {
        if (companyName) {
            if (companyID) {
                const session = neo4j.session();

                const readTxResultPromise = session.readTransaction((transaction) => {
                    return transaction.run(queryCypherUpdateCompanyNameByID(companyID, companyName));
                });

                return readTxResultPromise
                    .then((result) => {
                        session.close();
                        const data = convertToObject(result);
                        return resolve(data);
                    })
                    .catch((error) => {
                        console.error('Error in updateCompanyNameOnNeo: ', error);
                        return resolve({ok: false, error: error});
                    });
            } else {
                // This means that this is a new company (that has no id yet)
                return resolve({ok: false});
            }
        } else {
            console.error('Error in updateCompanyNameOnNeo: Wrong parameters inserted');
            return resolve({ok: false});
        }
    });
};

const queryCypherUpdateCompanyNameByID = (companyID, companyName) => {
    console.log('Starting Query');

    const query = 'MATCH (n:COMPANY) ';

    const filter = ' WHERE n.cid="' + companyID + '" ';

    const update = ' SET n.companyName="' + companyName + '" ';

    const ret = ' RETURN COUNT(*) as updatedNodes ';

    // Not using queryMaker here since scanTime is irrelevant when it comes to company's extra data.
    const fullQuery = query + filter + update + ret;

    console.log('Running query UpdateCompanyNameByID : ', fullQuery);
    return fullQuery;
};

const mergeOrgAndRelatedCompaniesOnNeo = (orgData, allProjects, Companies, isInProjectUpdate) => {
    console.log('In mergeOrgAndRelatedCompaniesOnNeo');
    return new Promise(async (resolve) => {
        if (orgData && orgData.organizationName && allProjects && Companies) {
            let relArr = [];
            let companiesArr = [];
            let orgNode;
            const orgNodeName = 'organization';

            if (isInProjectUpdate) {
                companiesArr = Companies;
                orgNode = {organizationName: orgData.organizationName, oid: orgData._id || orgData.id};
            } else {
                let projectsArray = extraHelpers.getFullSelectedObjectsInfoById(allProjects, orgData.selectedProjects);
                orgNode = {organizationName: orgData.organizationName, oid: orgData._id || orgData.id};
                projectsArray.map((curProject) => {
                    companiesArr = companiesArr.concat(curProject.selectedCompanies);
                });
            }

            if (companiesArr.length > 0) {
                for (let i = 0; i < companiesArr.length; i++) {
                    const relData = {
                        fromNodeLabelName: orgNodeName,
                        fromNodeProps: orgNode,
                        newNodeLabelName: 'COMPANY',
                        relationshipLabel: 'owner',
                        toNodeLabel: 'COMPANY',
                        toNodeType: 'COMPANY',
                        toNodeProps: {
                            companyName: companiesArr[i].companyName || companiesArr[i].name,
                            cid: companiesArr[i].id || companiesArr[i]._id
                        }
                    };
                    relArr.push(relData);
                }
                try {
                    await neoAndMongoHelpers.createNodeAndRelationship(relArr);
                } catch (e) {
                    console.error('Error in mergeOrgAndRelatedCompaniesOnNeo-createNodeAndRelationship: ', e);
                    resolve({error: e});
                }
            } else {
                try {
                    await intelHelpers.createNode(orgNodeName, orgNodeName, orgNode);
                } catch (e) {
                    console.error('Error in mergeOrgAndRelatedCompaniesOnNeo- createNode: ', e);
                    resolve({error: e});
                }
            }
            resolve({ok: true});
        } else {
            console.error('Error in mergeOrgAndRelatedCompaniesOnNeo');
            resolve({ok: false});
        }
    });
};
module.exports.mergeOrgAndRelatedCompaniesOnNeo = mergeOrgAndRelatedCompaniesOnNeo;


module.exports.OnUserCompanyCreateMergeOrgAndCompOnNeo = async (orgId, orgName, companyName, cid) => {
    console.log('In OnUserCompanyCreateMergeOrgAndCompOnNeo');
    let orgNode;
    const orgNodeName = 'organization';

    orgNode = {
        organizationName: orgName,
        oid: orgId
    };
    const relData = {
        fromNodeLabelName: orgNodeName,
        fromNodeProps: orgNode,
        newNodeLabelName: 'COMPANY',
        relationshipLabel: 'owner',
        toNodeLabel: 'COMPANY',
        toNodeType: 'COMPANY',
        toNodeProps: {companyName: companyName, cid: cid}
    };

    try {
        await neoAndMongoHelpers.createNodeAndRelationship(relData);
    } catch (e) {
        console.error('Error in OnUserCompanyCreateMergeOrgAndCompOnNeo-createNodeAndRelationship: ', e);
        resolve({error: e});
    }
};

module.exports.mergeCompanyAndRelatedDomainsOnNeo = (companyData) => {
    console.log('In mergeCompanyAndRelatedDomainsOnNeo');
    return new Promise(async (resolve) => {

        if (companyData && companyData.id && companyData.companyName && companyData.selectedDomains) {
            let relArr = [];
            let domainsArr = [];
            if (companyData.selectedDomains.length > 0) {
                domainsArr = companyData.selectedDomains;
            }
            const companyNodeName = 'COMPANY';
            const companyNode = {
                companyName: companyData.companyName,
                cid: companyData.id
            };

            if (domainsArr.length > 0) {
                for (let i = 0; i < domainsArr.length; i++) {
                    const relData = {
                        fromNodeLabelName: companyNodeName,
                        fromNodeProps: companyNode,
                        newNodeLabelName: 'domain',
                        relationshipLabel: 'owner',
                        toNodeLabel: 'domain',
                        toNodeType: 'domain',
                        toNodeProps: {hostname: domainsArr[i]}
                    };
                    relArr.push(relData);
                }
                try {
                    await neoAndMongoHelpers.createNodeAndRelationship(relArr);
                } catch (e) {
                    console.error('Error in mergeCompanyAndRelatedDomainsOnNeo-createNodeAndRelationship: ', e);
                    resolve({error: e});
                }
            }
            resolve({ok: true});
        } else {
            console.error('Error in mergeCompanyAndRelatedDomainsOnNeo');
            resolve({ok: false});
        }
    });
};

module.exports.handleIpMergeAndDetach = (data) => {
    console.log('In handleIpMergeAndDetach');
    return new Promise(async (resolve, reject) => {
        if (data.id && data.companyName && data.detachArr && Array.isArray(data.detachArr) && data.addArr && Array.isArray(data.addArr)) {

            const companyNodeName = 'COMPANY';
            const companyNode = {
                companyName: data.companyName,
                cid: data.id
            };

            const relData = {
                fromNodeLabelName: companyNodeName,
                fromNodeProps: companyNode,
                newNodeLabelName: 'domain',
                relationshipLabel: 'owner',
                toNodeLabel: 'domain',
                toNodeType: 'domain',
                toNodeProps: {hostname: 'no_domain', companyName: data.companyName}
            };

            try {
                await neoAndMongoHelpers.createNodeAndRelationship([relData]);
            } catch (e) {
                console.error('Error in handleIpMergeAndDetach-createNodeAndRelationship: ', e);
                reject({error: e});
            }

            try {

                if (data.detachArr.length > 0) {
                    await detachIpsFromNoDomainCompany(data.companyName, now, metaData, data.detachArr);
                }

                if (data.addArr.length > 0) {
                    await addIpsFromNoDomainCompany(data.companyName, now, metaData, data.addArr);
                }

            } catch (e) {
                console.error('Error in handleIpMergeAndDetach-createNodeAndRelationship: ', e);
                reject({error: e});
            }

            resolve({ok: true});
        } else {
            console.error('Error in handleIpMergeAndDetach');
            reject({ok: false});
        }
    });
};

const detachIpsFromNoDomainCompany = async (companyName, now, data, addArr) => {
    return new Promise(async (resolve, reject) => {
        const data = {
            label: 'domain',
            relationship: 'IP',
            toLabel: 'IP',
            prop: 'hostname',
            propStr: 'no_domain',
            secondProp: 'address',
            thirdProp: companyName,
            arrPropStr: addArr
        };

        try {
            await intelHelpers.detachNodes(data);
            resolve({ok: true});
        } catch (e) {
            console.error('Error in addIpsFromNoDomainCompany - createNodeAndRelationship: ', e);
            reject({error: e});
        }
    });
};

const addIpsFromNoDomainCompany = (companyName, now, data, addArr) => {
    return new Promise((resolve, reject) => {
        addArr.map(async (curIp) => {
            const relData = {
                fromNodeLabelName: 'domain',
                fromNodeProps: {hostname: 'no_domain', companyName: companyName},
                newNodeLabelName: 'IP',
                relationshipLabel: 'IP',
                data: data,
                toNodeLabel: 'IP',
                toNodeType: 'IP',
                toNodeProps: {address: curIp}
            };

            try {
                await neoAndMongoHelpers.createNodeAndRelationship([relData]);
                resolve({ok: true});
            } catch (e) {
                console.error('Error in addIpsFromNoDomainCompany - createNodeAndRelationship: ', e);
                reject({error: e});
            }

        });
    });
};


module.exports.updateOrgAndCompaniesOnProjUpdateOnNeo = (curProject, allOrg, allCompanies, companiesToDetach, companiesToMerge) => {
    return new Promise(async (resolve) => {
        if (companiesToDetach && companiesToMerge && (companiesToDetach.length > 0 || companiesToMerge.length > 0)) {

            console.log('In updateOrgAndCompaniesOnProjUpdateOnNeo');
            try {
                let orgArr = [];
                let detachCompaniesIdsArr = [];
                let mergeCompaniesIdsArr = [];
                if (companiesToDetach) {
                    detachCompaniesIdsArr = companiesToDetach;
                }
                if (companiesToMerge) {
                    mergeCompaniesIdsArr = companiesToMerge;
                }
                allOrg.map((org) => {
                    org.selectedProjects.map((project) => {
                        if (project.id && curProject && curProject.id && project.id === curProject.id) {
                            orgArr.push(org);
                        }
                    });
                });

                if (orgArr.length > 0) {
                    if (detachCompaniesIdsArr.length > 0) {
                        orgArr.map(async (org) => {
                            let projectsObjIdsArr = [];
                            org.selectedProjects.map((proj) => {
                                if (proj.id) {
                                    projectsObjIdsArr.push(ObjectId(proj.id.toString()));
                                }
                            });
                            const finalDetachCompaniesArr = await checkIfCompaniesOfArrFoundOnOtherProjects(detachCompaniesIdsArr, projectsObjIdsArr);

                            if (finalDetachCompaniesArr && finalDetachCompaniesArr.length > 0) {
                                const data = {
                                    label: 'organization',
                                    relationship: 'owner',
                                    toLabel: 'COMPANY',
                                    prop: 'oid',
                                    secondProp: 'cid',
                                    arrPropStr: [org._id],
                                    arrSecondPropStr: finalDetachCompaniesArr
                                };
                                await intelHelpers.detachNodes(data);
                            }
                        });
                    }

                    if (mergeCompaniesIdsArr.length > 0) {
                        let companiesArr = extraHelpers.getFullSelectedObjectsInfoById(allCompanies, mergeCompaniesIdsArr);
                        orgArr.map((org) => {
                            mergeOrgAndRelatedCompaniesOnNeo(org, [], companiesArr, true);
                        });
                    }
                }
                resolve({ok: true, orgArr: orgArr});
            } catch (e) {
                console.error('On updateOrgAndCompaniesOnProjUpdateOnNeo: ' + e);
                resolve({ok: false, orgArr: []});
            }
        } else {
            resolve({ok: false, orgArr: []});
        }
    });
};

const checkIfCompaniesOfArrFoundOnOtherProjects = (companiesIdsArr, projectsObjIdsArr) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                let finalDetachCompaniesArr = [];
                const db = client.db('darkvision');
                const collection = db.collection('project_map');
                const find = PromiseB.promisify(collection.find, {context: collection});

                for (let i = 0; i < companiesIdsArr.length; i++) {
                    const list = await find({
                        _id: {$in: projectsObjIdsArr},
                        'selectedCompanies': {$elemMatch: {id: {$eq: companiesIdsArr[i]}}}
                    });
                    if (list) {
                        const result = await listToArray(list);
                        if (result) {
                            if (result.length === 0) {
                                finalDetachCompaniesArr.push(companiesIdsArr[i]);
                            }
                        }
                    }
                }
                client.close();
                resolve(finalDetachCompaniesArr);
            } catch (e) {
                client.close();
                reject(e);
            }
        });
    });
};

module.exports.detachOrgFromCompaniesOnNeo = async (projectsIds, allOrg, allPro, allComp, onUpdateOrg) => {
    console.log('In detachOrgFromCompaniesOnNeo');
    let orgArr = [];
    let companiesArr = [];
    let projectsIdsArr = [];

    if (onUpdateOrg) {
        orgArr.push(allOrg);
        projectsIdsArr = projectsIds;

        //on delete project
    } else {
        allOrg.map((org) => {
            org.selectedProjects.map((proj) => {
                if (proj.id && projectsIds && proj.id === projectsIds) {
                    orgArr.push(org);
                }
            });
        });
        projectsIdsArr.push(projectsIds);
    }

    let projArray = extraHelpers.getFullSelectedObjectsInfoById(allPro, projectsIdsArr);
    projArray.map((curProject) => {
        companiesArr = companiesArr.concat(curProject.selectedCompanies);
    });

    if (companiesArr.length > 0) {

        const companiesIdsArr = extraHelpers.createIdsArray(companiesArr);
        const orgIdsArr = extraHelpers.createIdsArray(orgArr);

        if (companiesIdsArr.length > 0) {
            orgArr.map(async (org) => {
                let projectsObjIdsArr = [];
                org.selectedProjects.map((proj) => {
                    if (proj.id) {
                        projectsObjIdsArr.push(ObjectId(proj.id.toString()));
                    }
                });
                const finalDetachCompaniesArr = await checkIfCompaniesOfArrFoundOnOtherProjects(companiesIdsArr, projectsObjIdsArr);

                if (finalDetachCompaniesArr && finalDetachCompaniesArr.length > 0) {
                    const data = {
                        label: 'organization',
                        relationship: 'owner',
                        toLabel: 'COMPANY',
                        prop: 'oid',
                        secondProp: 'cid',
                        arrPropStr: orgIdsArr,
                        arrSecondPropStr: finalDetachCompaniesArr
                    };
                    await intelHelpers.detachNodes(data);
                }
            });
        }
    }
};


module.exports.saveCompanyInfoOnNeo = (companyData) => {
    console.log('In saveCompanyInfoOnNeo');
    return new Promise((resolve) => {
        if (companyData && companyData.id && companyData.companyName && companyData.companyInfo) {
            const relData = [];

            const companyNode = {
                companyName: companyData.companyName,
                cid: companyData.id
            };
            const companyNodeName = 'COMPANY';

            const addNodeArrayRelData = (propertyName, nodeLabel) => {
                if (propertyName && nodeLabel && companyData.companyInfo.hasOwnProperty(propertyName)) {
                    if (Array.isArray(companyData.companyInfo[propertyName])) {
                        companyData.companyInfo[propertyName].map((currEntity) => {
                            if (currEntity) {
                                const currEntityObj = (typeof currEntity === 'string') ? {label: currEntity} : currEntity;
                                // Remove old nodeIDs.
                                if (currEntityObj.hasOwnProperty('nid')) {
                                    delete currEntityObj.nid;
                                }
                                relData.push({
                                    fromNodeLabelName: companyNodeName,
                                    fromNodeProps: companyNode,
                                    toNodeType: nodeLabel.toString().toUpperCase(),
                                    toNodeProps: currEntityObj,
                                    relationshipLabel: nodeLabel.toString().toLowerCase()
                                });
                            }
                        });
                    } else if (companyData.companyInfo[propertyName]) {
                        let toNodePropsObj;

                        if (typeof companyData.companyInfo[propertyName] === 'string') {
                            toNodePropsObj = {
                                label: companyData.companyInfo[propertyName]
                            };
                        } else {
                            toNodePropsObj = companyData.companyInfo[propertyName];
                        }

                        relData.push({
                            fromNodeLabelName: companyNodeName,
                            fromNodeProps: companyNode,
                            toNodeType: nodeLabel.toString().toUpperCase(),
                            toNodeProps: toNodePropsObj,
                            relationshipLabel: nodeLabel.toString().toLowerCase()
                        });
                    }
                }
            };

            addNodeArrayRelData('companyResponsibles', 'responsible');

            addNodeArrayRelData('companyContacts', 'contact');

            addNodeArrayRelData('companyInformation', 'information');

            addNodeArrayRelData('companyClassifications', 'classification');

            addNodeArrayRelData('companyDescription', 'description');

            addNodeArrayRelData('companySectors', 'sector');

            addNodeArrayRelData('companyDateOfContact', 'date_of_contact');

            addNodeArrayRelData('companyDateOfSurvey', 'date_of_survey');

            console.log('In saveCompanyInfoOnNeo - Starting to remove previous companyInfo related to this company');

            // Before adding new data, delete previous companyInfo related to this company.
            return this.removePreviousCompanyInfo(companyNodeName, companyNode, ['responsible', 'contact', 'information', 'classification', 'description', 'sector', 'date_of_contact', 'date_of_survey'])
                .then((res) => {
                    if (res && Array.isArray(res) && res.length > 0 && res[0].hasOwnProperty('deletedNodes') && res[0].deletedNodes.hasOwnProperty('low')) {
                        console.log('In saveCompanyInfoOnNeo - Successfully removed previous ', res[0].deletedNodes.low, ' companyInfo records');
                    } else {
                        console.log('In saveCompanyInfoOnNeo - some error happened while removing previous companyInfo records');
                    }
                    if (relData.length > 0) {
                        console.log('In saveCompanyInfoOnNeo - Starting to save new companyInfo data');
                        return neoHelpers.createNodeAndRelationship(relData);
                    } else if (relData.length === 0) {
                        console.log('In saveCompanyInfoOnNeo - company node created with no extra data');
                        return intelHelpers.createNode(companyNodeName, companyNodeName, companyNode);
                    } else {
                        console.log('In saveCompanyInfoOnNeo - No companyInfo data to save was found');
                        return PromiseB.resolve({ok: true});
                    }
                })
                .catch((e) => {
                    console.error('Error in saveCompanyInfoOnNeo-removePreviousCompanyInfo: ', e);
                    return PromiseB.resolve({ok: false});
                })
                .then(() => {
                    console.log('In saveCompanyInfoOnNeo - Successfully saved companyInfo on GraphDB');
                    resolve({ok: true});
                }).catch((e) => {
                    console.error('Error in saveCompanyInfoOnNeo-createNodeAndRelationship: ', e);
                    resolve({error: e});
                });
        } else {
            console.log('In saveCompanyInfoOnNeo - No company data was inserted');
            resolve({error: 'No company data was inserted'});
        }
    });
};

module.exports.removePreviousCompanyInfo = (fromNodeLabelName, fromNodeProps, toRelationshipTypes) => {
    console.log('In removePreviousCompanyInfo - starting to remove old companyInfo of ', fromNodeProps);
    return new Promise((resolve) => {
        const session = neo4j.session();

        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(queryCypherDeleteOldCompanyExtraDataByID(fromNodeLabelName, fromNodeProps, toRelationshipTypes));
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('Error in removePreviousCompanyInfo: ', error);
                return reject();
            });
    });
};

const convertToObject = (result) => {
    return result.records.map((record) => {
        const dataObj = {};

        for (let i = 0; i < record._fields.length; i++) {
            dataObj[record.keys[i]] = record._fields[i];
        }

        return dataObj;
    });
};

/* queryCypherDeleteOldCompanyExtraDataByID will result with query like:
                MATCH (n:COMPANY) WHERE n.companyName="etravel texas aaa" AND n.cid="5b67232399e78f05589b96ca"
                OPTIONAL MATCH (n)-[r:responsible|:contact]->(e)
                DETACH DELETE e
                RETURN COUNT(e) AS deletedNodes*/
const queryCypherDeleteOldCompanyExtraDataByID = (fromNodeLabelName, fromNodeProps, toRelationshipTypes) => {
    let fullQuery = '';
    if (fromNodeLabelName && fromNodeProps && toRelationshipTypes && Array.isArray(toRelationshipTypes)) {
        fullQuery += 'MATCH (n:' + fromNodeLabelName + ') ';
        const fromNodePropsKeys = Object.keys(fromNodeProps);
        if (fromNodePropsKeys && fromNodePropsKeys.length > 0) {
            fullQuery += ' WHERE ';
            fromNodePropsKeys.map((currFromNodeProp, keyIndex) => {
                if (currFromNodeProp && fromNodeProps[currFromNodeProp]) {
                    if (keyIndex > 0) {
                        fullQuery += ' AND ';
                    }
                    fullQuery += ' n.' + currFromNodeProp + '="' + fromNodeProps[currFromNodeProp] + '" ';
                }
            });
        }
        fullQuery += ' OPTIONAL MATCH (n)-[r';
        toRelationshipTypes.map((currType, index) => {
            if (currType) {
                if (index > 0) {
                    fullQuery += '|';
                }
                fullQuery += ':' + currType;
            }
        });
        fullQuery += ']->(e) ' +
            ' DETACH DELETE e ' +
            ' RETURN COUNT(*) AS deletedNodes ';
    }
    console.log('Running DeleteOldCompanyInfo query: ', fullQuery);
    return fullQuery;
};

module.exports.listToArray = (list) => {
    return new Promise((resolve, reject) => {
        list.toArray((err, docs) => {
            if (err) {
                return reject(err);
            }
            return resolve(docs);
        });
    });
};

module.exports.isAdminUserByID = (uid, context) => {
    const user_entity = context.make$('sys', 'user');
    const list = PromiseB.promisify(user_entity.list$, {context: user_entity});
    return list({id: uid}).then((user) => {
        // return true only is userRole is admin.
        return !(!user || user.length < 1 || !user[0] || !user[0].userRole || user[0].userRole !== 'admin');
    });
};

module.exports.isUserAllowedToExportCVEByID = (uid, context) => {
    const user_entity = context.make$('sys', 'user');
    const list = PromiseB.promisify(user_entity.list$, {context: user_entity});
    return list({id: uid}).then((user) => {
        // Only admin or allowed users.
        return !!(user && user.length > 0 && user[0].userRole && (user[0].userRole === 'admin' || user[0].canExportCVEReports));
    });
};

// Only Admin is allowed to export intel reports as word. All user types are allowed to export PDF and Excel.
module.exports.isUserAllowedToExportIntelByID = (uid, exportType, context) => {
    return new Promise((resolve) => {
        if (uid && exportType) {
            if (exportType.toLowerCase() === 'word') {
                const user_entity = context.make$('sys', 'user');
                const list = PromiseB.promisify(user_entity.list$, {context: user_entity});
                return list({id: uid}).then((user) => {
                    // Only admin is allowed to export intel reports as word.
                    resolve(!!(user && user.length > 0 && user[0].userRole && user[0].userRole === 'admin'));
                });
            } else {
                // All user types are allowed to export PDF and Excel.
                resolve(true);
            }
        } else {
            console.log('ERROR: Wrong input in isUserAllowedToExportIntelByID');
            resolve(false);
        }
    });
};

module.exports.getProgress = (survey) => {
    const pages = survey.pages;
    let answerCounter = 0;
    let questionCounter = 0;
    if (pages) {
        for (let i = 0; i < pages.length; i++) {
            const category = pages[i].elements;
            for (let j = 0; j < category.length; j++) {
                const question = category[j].elements;
                for (let k = 0; k < question.length; k++) {
                    questionCounter++;
                    if (question[k].answer) {
                        answerCounter++;
                    }
                }
            }
        }

        return (answerCounter / questionCounter) * 100;
    } else {
        return 0;
    }
};

module.exports.asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

module.exports.generateCurReport = (reportKind, data, promiseAct) => {
    return new Promise(async (resolve, reject) => {
        let res;
        try {
            if (reportKind === 'intelReport') {
                res = await promiseAct('role:reportCreator, cmd:getReport', {data});
            } else {
                res = await promiseAct('role:reportCreator, cmd:getSurveyReport', {data});
            }

            if (res && res.ok && res.data) {
                resolve({ok: true, data: res.data});
            } else {
                console.error('Error in generateCurReport()');
                reject({ok: false});
            }
        } catch (e) {
            console.error('Error in generateCurReport(): ', e);
            reject({ok: false, error: e});
        }
    });
};


module.exports.whoIsRelatedByDomain = (companies, domain) => {
    const relatedCompanies = [];
    if (companies && domain) {
        companies.map((company) => {
            if (company.selectedDomains && company.selectedDomains.indexOf(domain) !== -1) {
                relatedCompanies.push(company.companyName);
            }
        });
    }
    return (relatedCompanies);
};

module.exports.getKnownPortInfo = (port) => {
    const result = [];

    if (port && isNumeric(port)) {
        port = Number(port);

        // Using pn (port-numbers) library.
        const knownInfo = pn.getService(port);

        if (knownInfo) {
            if (knownInfo.name) {
                result.push({name: 'name', finding: knownInfo.name});
            }
            if (knownInfo.description) {
                result.push({name: 'description', finding: knownInfo.description});
            }
        }
    }
    return result;
};

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

// This function add the unique create_time from DB in a correct format, if no such registry exists - generates a new unique ID.
function stringifyDate(date) {
    let result = '';
    if (date) {
        const checkDate = moment(date);
        if (checkDate && checkDate.isValid()) {
            result = checkDate.format('YY-MM-DD--HH-mm-ss');
        }
    }
    if (!result) {
        result = shortid.generate();
    }
    return result;
}


module.exports.allowExtensions = (fileName) => {
    const fileExtension = fileName.split('.');
    let suffix = fileExtension[fileExtension.length - 1];
    suffix = suffix.toLowerCase();
    return suffix === 'xlsx' || suffix === 'xls' || suffix === 'ppt' || suffix === 'pptx' || suffix === 'ppt' || suffix === 'pdf'
        || suffix === 'docx' || suffix === 'doc' || suffix === 'vdx' || suffix === 'vsd' || suffix === 'vsdx' || suffix === 'zip' || suffix === 'rar'
        || suffix === 'tgz' || suffix === 'tar' || suffix === 'txt' || suffix === 'png' || suffix === 'jpeg' || suffix === 'jpg' || suffix === 'bmp';
};

module.exports.convertFiles = (fileName, destinationFolder) => {
    const file = fileName.split('.');
    let suffix = file[file.length - 1];
    suffix = suffix.toLowerCase();
    if (suffix === 'jpg' || suffix === 'jpeg' || suffix === 'bmp') {
        Jimp.read(destinationFolder + '/' + fileName)
            .then((pic) => {
                return pic.write(destinationFolder + '/' + file[0] + '.png'); // save
            })
            .catch((err) => {
                console.error(err);
            })
            .then((pic) => {
                return pic.write(destinationFolder + '/' + file[0] + '.' + suffix); // save
            })
            .catch((err) => {
                console.error(err);
            });
    } else if (suffix === 'png') {
        Jimp.read(destinationFolder + '/' + fileName)
            .then((pic) => {
                return pic.write(destinationFolder + '/' + file[0] + '.jpg'); // save
            })
            .catch((err) => {
                console.error(err);
            })
            .then((pic) => {
                return pic.write(destinationFolder + '/' + file[0] + '.' + suffix); // save
            })
            .catch((err) => {
                console.error(err);
            });
    }
};

module.exports.addNewCreatedProjectsToOrg = async (createdProjects, chosenProj, chosenOrg) => {
    console.log('In addProjectsToOrg');
    try {
        let companies = [];
        await addProjectsToOrgOnMongo(
            chosenOrg,
            createdProjects);
        if (chosenProj && chosenProj.selectedCompanies) {
            companies = chosenProj.selectedCompanies;
        }
        if (createdProjects && createdProjects.length > 0) {
            createdProjects.map((currProj) => {
                if (currProj.selectedCompanies && currProj.selectedCompanies.length > 0) {
                    companies = companies.concat(currProj.selectedCompanies);
                }
            });
        }

        await mergeOrgAndRelatedCompaniesOnNeo(
            chosenOrg,
            [],
            companies,
            true);
        console.log('In addProjectsToOrg - Successfully merge organization and related companies on GraphDB');
        return ({ok: true});
    } catch (e) {
        console.error('Error in addProjectsToOrg: ', e);
        return ({ok: false});
    }
};


module.exports.sendNotificationToAdmin = async (data, promiseAct) => {

    if (data && data.userWhoCreateNewCompany && data.newCompanyData) {
        const companyData = data.newCompanyData;
        const newProjectsCreatedByUser = data.newProjectsCreatedByUser;
        const userData = data.userWhoCreateNewCompany;
        const chosenOrg = data.chosenOrg;
        const chosenProj = data.chosenProj;

        if (typeof userData.email === 'object') {
            // This is a fix to display API Key instead of user if needed.
            userData.email = JSON.stringify(userData.email);
        }

        let text = '<!DOCTYPE html><html><head></head><body><div>';
        text += '<p>New Company: ' + companyData.companyName + '.</p>';
        text += '<p>Created By: ' + userData.email + '.</p>';
        text += '<p>Creation Time: ' + companyData.createDate + '.</p>';
        text += '<p>Environment: ' + config.addresses.website + '.</p>';
        text += '<p>Organization: ' + chosenOrg.organizationName + '.</p>';
        text += '<p>Project: ' + chosenProj.projectName + '.</p>';

        if (newProjectsCreatedByUser.length > 0) {
            newProjectsCreatedByUser.map((project) => {
                text += '<br/><p>New Project Was Created</p>';
                text += '<p>    Project Name: ' + project.projectName + '.</p>';
                text += '<p>    Project Companies: ' +
                    project.selectedCompanies.map((comp) => {
                        return (' ' + comp.name + ', ');
                    });
                +'</p><br/>';
            });
        }

        text += '<br/><br/><b>Rescana</b>';
        text += '</div></body></html>';
        try {
            const adminUsersEmailsArr = await getAllFromMongoByCollectionName('notify_emails_map');
            if (adminUsersEmailsArr && Array.isArray(adminUsersEmailsArr) && adminUsersEmailsArr.length > 0) {
                adminUsersEmailsArr.map(async (emailToSendTo) => {
                    const msgObj = {
                        to: emailToSendTo.email,
                        subject: 'New Company ' + companyData.companyName + ' was added by ' + userData.email,
                        text: text
                    };

                    await promiseAct('role:mailer, cmd:send', msgObj);
                    console.log('In sendNotificationToAdmin() - Notification To: ' + emailToSendTo.email + ' has beenSent!');
                });
            } else {
                const msgObj = {
                    to: 'tzach@rescana.com',
                    subject: 'New Company ' + companyData.companyName + ' was added by ' + userData.email,
                    text: text
                };

                await promiseAct('role:mailer, cmd:send', msgObj);
                console.log('In sendNotificationToAdmin() - Notification To: ' + 'tzach@rescana.com' + ' has beenSent!');
            }
            return ({ok: true});
        } catch (e) {
            console.error('In sendNotificationToAdmin() - Error:' + e);
            return ({ok: false});
        }
    } else {
        return ({ok: false});
    }
};

module.exports.updateVendorAssessmentFieldsAndScore = (compId, fieldData, assessmentScore) => {
    let compObjId = '';

    if (typeof compId === 'string') {
        compObjId = ObjectId(compId.toString());
    }
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let collection = db.collection('company_map');
            if (compObjId && fieldData && (assessmentScore || assessmentScore === 0)) {
                collection.update({_id: compObjId}, {
                    $set: {
                        assessmentFields: fieldData,
                        assessmentScore: assessmentScore
                    }
                }, {upsert: true}, (err, res) => {
                    client.close();
                    if (err) {
                        console.error('In updateVendorAssessmentField() - Failed To Save vendor assessment field To DB : ' + err);
                        reject();
                    } else {
                        console.log('In updateVendorAssessmentField() - All vendor assessment fields was updated!');
                        resolve(compId);
                    }
                });
            } else {
                console.error('In updateVendorAssessmentField() - Failed To Save vendor assessment field To DB');
                client.close();
                reject();
            }
        });
    });
};


module.exports.updateAssessmentNotificationArea = (compId, notificationAreaComments) => {
    let compObjId = '';

    if (typeof compId === 'string') {
        compObjId = ObjectId(compId.toString());
    }
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let collection = db.collection('company_map');
            if (compObjId && notificationAreaComments) {
                collection.update({_id: compObjId}, {
                    $set: {notificationAreaComments: notificationAreaComments}
                }, {upsert: true}, (err) => {
                    client.close();
                    if (err) {
                        console.error('In updateAssessmentNotificationArea() - Failed To Save vendor assessment Notification Area To DB : ' + err);
                        reject();
                    } else {
                        console.log('In updateAssessmentNotificationArea() - vendor assessment notification area was updated!');
                        resolve(compId);
                    }
                });
            } else {
                console.error('In updateAssessmentNotificationArea() - Failed To Save vendor assessment Notification Area To DB');
                client.close();
                reject();
            }
        });
    });
};


module.exports.updateSurveyEditedData = (surveyId, surveyName) => {
    let surveyObjId = '';

    if (typeof surveyId === 'string') {
        surveyObjId = ObjectId(surveyId.toString());
    }
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let collection = db.collection('surveyAnswers_map');
            if (surveyObjId && surveyName) {
                collection.update({_id: surveyObjId}, {
                    $set: {name: surveyName}
                }, {upsert: true}, (err) => {
                    client.close();
                    if (err) {
                        console.error('In updateSurveyEditedData() - Failed To Save Survey Data To DB : ' + err);
                        reject({ok: false});
                    } else {
                        console.log('In updateSurveyEditedData() - Survey Data updated!');
                        resolve({ok: true});
                    }
                });
            } else {
                console.error('In updateSurveyEditedData() - Failed To Save Survey Data To D');
                client.close();
                reject({ok: false});
            }
        });
    });
};

module.exports.updateSurveyTemplateAllowedOrgOnMongo = (surveyId, uid, allowedOrganizations) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            let collection = db.collection('survey_map');
            if (surveyId && allowedOrganizations && Array.isArray(allowedOrganizations)) {
                collection.update({sid: surveyId}, {
                    $set: {uid: uid, allowedOrganizations: allowedOrganizations}
                }, {upsert: true}, (err) => {
                    client.close();
                    if (err) {
                        console.error('In updateSurveyTemplateAllowedOrgOnMongo() - Failed To Save Survey Template allowed users Data To DB : ' + err);
                        reject({ok: false});
                    } else {
                        console.log('In updateSurveyTemplateAllowedOrgOnMongo() - Survey Template allowed users Data updated!');
                        resolve({ok: true});
                    }
                });
            } else {
                console.error('In updateSurveyTemplateAllowedOrgOnMongo() - Failed To Save Survey Template allowed users Data To D');
                client.close();
                reject({ok: false});
            }
        });
    });
};

module.exports.updateCompanySurveyDataOnMongo = async (data) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, (err, client) => {

            if (data && data.id && (data.surveyScore || data.surveyScore === 0) && data.hasOwnProperty('isUnsafeCompany') && data.orgId && data.orgId !== 'No Id') {

                const db = client.db('darkvision');
                let collection = db.collection('company_map');

                let compValidObjId = '';

                if (data.id && data.id !== '') {
                    compValidObjId = ObjectId(data.id.toString());
                }

                let destination = 'dataByOrg.' + data.orgId;

                let find = {_id: compValidObjId};

                collection.update(find, {'$set': {[destination + '.orgId']: data.orgId, [destination + '.isUnsafeCompany']: data.isUnsafeCompany,
                        [destination + '.surveyScore']: data.surveyScore, surveyScore: data.surveyScore}
                        }, (err) => {
                    client.close();
                    if (err) {
                        console.error('In updateCompanySurveyDataOnMongo() - Failed To update company survey data to mongoDB : ' + err);
                        reject({ok: false});
                    } else {
                        console.log('In updateCompanySurveyDataOnMongo() - Company survey data updated!');
                        resolve({ok: true});
                    }
                });
            } else {
                console.error('In updateCompanySurveyDataOnMongo() - Failed To update company survey data to mongoDB - one or more props are missing');
                client.close();
                reject({ok: false});
            }
        });
    });
};
