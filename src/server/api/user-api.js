const async = require('async');
const helpers = require('./api-helpers');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const Promise = require('bluebird');
const serverHelpers = require('./api-helpers');
const extraHelpers = require('./helpers');
const owasp = require('owasp-password-strength-test');
// Pass settings to the `config` method.
owasp.config({
    allowPassphrases: true,
    maxLength: 128,
    minLength: 8,
    minPhraseLength: 20
});


module.exports = function (seneca) {
    const promiseAct = Promise.promisify(seneca.act, {context: seneca});
    const Unauthorized_User_Error = 'Unauthorized user.';

    return {
        registerUser: (args, cb) => {
            const alertSystemConfig = {
                sendIntelAlerts: true,
                sendPhishingAlerts: true
            };
            const userPreferencesConfig = {
                displayScoreBackwards: false,
                displayIntelTableViewFirst: false,
                gaugeDisplay: args.request$.body.userPreferencesConfig.gaugeDisplay || false
            };
            seneca.act({
                    role: 'user',
                    cmd: 'register',
                    email: args.request$.body.username,
                    password: args.request$.body.password,
                    userRole: args.request$.body.userRole,
                    name: {firstName: '', lastName: ''},
                    twoFactorOnLogin: args.request$.body.twoFactorOnLogin,
                    canExportCVEReports: args.request$.body.canExportCVEReports,
                    firstLogin: true,
                    mfaEnabled: args.request$.body.mfaEnabled,
                    alertSystemConfig: alertSystemConfig,
                    userPreferencesConfig: userPreferencesConfig,
                    isUserAllowedToAddVendors: args.request$.body.isUserAllowedToAddVendors,
                    isInDemoMode: args.request$.body.isInDemoMode,
                    isVendorAssessmentEnabled: args.request$.body.isVendorAssessmentEnabled,
                    logo: args.request$.body.logo,
                    fatal$: false
                },
                (err, out) => {
                    if (err) {
                        console.log(err);

                        cb(null, {ok: false, error: err});
                    }
                    if (!out.ok) {
                        console.log('Register user failed with reason:', out.why);

                        cb(null, {ok: false, error: out.why});
                    }

                    seneca.act('role:userdata, get:user', {id: out.user.id}, (err, user) => {
                        if (user.name !== null && typeof user.name === 'object') {
                            user.name = user.name.firstName + ' ' + user.name.lastName;
                        } else if (!user.name) {
                            user.name = '';
                        }
                        let mfaEnabled = false;

                        if (user.mfaEnabled === true || user.mfaEnabled === false) {
                            mfaEnabled = user.mfaEnabled;
                        }

                        const formatedUser = {
                            id: user.id ? user.id.toString() : '',
                            name: user.name ? user.name.toString() : '',
                            nick: user.nick ? user.nick.toString() : '',
                            email: user.email ? user.email.toString() : '',
                            mfaEnabled: mfaEnabled || user.twoFactorOnLogin, // If two factor on login is enabled than mfa should be enabled.
                            twoFactorOnLogin: user.twoFactorOnLogin,
                            canExportCVEReports: user.canExportCVEReports,
                            firstLogin: true,
                            isUserAllowedToAddVendors: user.isUserAllowedToAddVendors,
                            isInDemoMode: user.isInDemoMode,
                            isVendorAssessmentEnabled: user.isVendorAssessmentEnabled,
                            userPreferencesConfig: user.userPreferencesConfig,
                            logo: user.logo,
                            active: user.active ? user.active.toString() : '',
                            failedLoginCount: user.failedLoginCount ? user.failedLoginCount.toString() : '',
                            created: user.when ? user.when.toString() : '',
                            confirmed: user.confirmed ? user.confirmed.toString() : ''
                        };


                        cb(null, {
                            ok: out.ok,
                            user: formatedUser
                        });
                    });
                });
        },

        afterLogin: (msg, cb) => {
            if (msg.args.user && msg.args.user.mfaEnabled) {
                cb(null, {ok: false, user: msg.args.user, mfaEnabled: msg.args.user.mfaEnabled});
            } else {
                cb(null, {ok: true, user: msg.args.user});
            }
        },

        changePassword: (msg, cb) => {
            const sessionUser = msg.request$.session.passport.user;
            console.log('in change password process for :', sessionUser.email);

            // Get the user's updated data.
            promiseAct('role:userdata, get:user', {
                id: sessionUser.id
            }).then((user) => {
                const pass = user.pass;
                const salt = user.salt;

                return promiseAct('role:user, cmd:verify_password', {
                    salt: salt,
                    pass: pass,
                    proposed: msg.args.body.userData.oldPassword
                });
            }).catch((e) => {
                console.log('Error getting user data with email:', sessionUser.email, ' ,uid: ', sessionUser.id, ':', e);
            }).then((res) => {
                if (res.ok) {
                    console.log('Verified password.');
                    return promiseAct('role:user, cmd:create_reset', {email: msg.args.user.email});
                } else {
                    console.log('Failed to verify password.');
                    cb(null, {ok: false, error: 'Wrong password'});
                }
            }).then((data) => {
                if (data) {
                    console.log('Resetting password.');
                    const password = msg.args.body.userData.newPassword;
                    const repeatPassword = msg.args.body.userData.repeatPassword;
                    if (password !== repeatPassword) {
                        console.log('Passwords do not match.');
                        return cb(null, {ok: false, error: 'Passwords do not match'});
                    } else {
                        // invoke test() to test the strength of the new password.
                        const result = owasp.test(password);

                        if (result && result.errors && Array.isArray(result.errors) && result.errors.length > 0) {
                            result.errors = result.errors.map((currError) => {
                                // Fix API errors text (if needed).
                                if (currError && currError.startsWith('The password may not contain')) {
                                    currError = currError.replace('The password may not contain', 'The password must not contain');
                                }
                                return currError;
                            });
                            console.log('Password is not strong enough: ', result.errors);
                            cb(null, {ok: false, error: result.errors});
                        } else {
                            return promiseAct('role:user, cmd:execute_reset', {
                                token: data.reset.token,
                                password: password,
                                repeat: repeatPassword
                            });
                        }
                    }
                }
            }).then((data) => {
                if (data) {
                    if (data.ok) {
                        console.log('Done resetting password.');
                        cb(null, {ok: true});
                    } else {
                        console.log('Failed resetting password.');
                        cb(null, {ok: false, error: 'Failed to reset password'});
                    }
                }
            }).catch((e) => {
                console.log('Error - Failed resetting password: ', e);
                cb(null, {ok: false, error: 'Failed to reset password'});
            });
        },

        updateUser: (args, cb) => {
            const req = args.request$.body;
            const email = req.email || req.username;

            console.log("In updateUser() - update started");
            console.log("userName: " + args.request$.body.username);
            console.log("userRole: " + args.request$.body.userRole);
            console.log("twoFactorOnLogin: " + args.request$.body.twoFactorOnLogin);
            console.log("canExportCVEReports: " + args.request$.body.canExportCVEReports);
            console.log("isUserAllowedToAddVendors: " + args.request$.body.isUserAllowedToAddVendors);
            console.log("isInDemoMode: " + args.request$.body.isInDemoMode);
            console.log("isVendorAssessmentEnabled: " + args.request$.body.isVendorAssessmentEnabled);

            let data = {};

            if (req.mfaEnabled) {
                data.mfaEnabled = req.mfaEnabled;
            }

            // If twoFactorOnLogin is set to false (and not undefined) set mfaEnabled to false.
            // We don't want the twofactor to be activated if the user hasn't set it up yet.
            if (req.twoFactorOnLogin === false) {
                data.mfaEnabled = false;
            }
            if (req.hasOwnProperty('twoFactorOnLogin')) {
                data.twoFactorOnLogin = req.twoFactorOnLogin;
            }
            if (req.hasOwnProperty('canExportCVEReports')) {
                data.canExportCVEReports = req.canExportCVEReports;
            }
            if (req.hasOwnProperty('isUserAllowedToAddVendors')) {
                data.isUserAllowedToAddVendors = req.isUserAllowedToAddVendors;
            }
            if (req.hasOwnProperty('isInDemoMode')) {
                data.isInDemoMode = req.isInDemoMode;
            }
            if (req.hasOwnProperty('isVendorAssessmentEnabled')) {
                data.isVendorAssessmentEnabled = req.isVendorAssessmentEnabled;
            }
            if (req.hasOwnProperty('logo')) {
                data.logo = req.logo;
            }

            if (req.userRole) {
                data.userRole = req.userRole;
            }

            if (req.username) {
                data.email = req.username;
                data.nick = req.username;
            }

            if (req.alertSystemConfig && req.alertSystemConfig.sendIntelAlerts !== null && req.alertSystemConfig.sendPhishingAlerts !== null) {
                data.alertSystemConfig = {
                    sendIntelAlerts: req.alertSystemConfig.sendIntelAlerts,
                    sendPhishingAlerts: req.alertSystemConfig.sendPhishingAlerts
                };
                if (req.alertSystemConfig.excludedCompaniesIntelAlert) {
                    data.alertSystemConfig.excludedCompaniesIntelAlert = req.alertSystemConfig.excludedCompaniesIntelAlert;
                }
            } else {
                data.alertSystemConfig = {
                    sendIntelAlerts: true,
                    sendPhishingAlerts: true,
                    excludedCompaniesIntelAlert: []
                };
            }

            if (req.userPreferencesConfig) {
                if (req.userPreferencesConfig.displayScoreBackwards != null) {
                    data.userPreferencesConfig = {
                        displayScoreBackwards: req.userPreferencesConfig.displayScoreBackwards
                    };
                }
                if (req.userPreferencesConfig.displayIntelTableViewFirst != null) {
                    data.userPreferencesConfig.displayIntelTableViewFirst = req.userPreferencesConfig.displayIntelTableViewFirst;
                }
                // else {
                //     data.userPreferencesConfig.displayIntelTableViewFirst = false;
                // }
                if (req.userPreferencesConfig.gaugeDisplay != null) {
                    if(data.userPreferencesConfig === null || data.userPreferencesConfig === undefined){
                        data.userPreferencesConfig = {};
                    }
                    data.userPreferencesConfig.gaugeDisplay = req.userPreferencesConfig.gaugeDisplay;
                }
            }

            if (!req.id) {
                if (args.request$.body.name) {
                    data.name = extraHelpers.validateUserName(args.request$.body)
                }
                saveUserByEmail(data, email, seneca, cb);
            } else {
                saveUserById(data, req.id, seneca, cb);
            }
        },

        getUserVendorsConfig: (args, cb) => {
            getUserVendorsConfigByEmail(args.request$.body.email, seneca, cb);
        },

        getUserCompaniesConfig: (args, cb) => {
            getUserCompaniesByEmail(args.request$.body.email, seneca, cb);
        },

        updateUserDataleaksConfig: (args, cb) => {
            const data = {};
            data.dataleaksConfig = {
                selectedCompanies: args.request$.body.selectedCompanies
            };
            saveUserByEmail(data, args.request$.body.email, seneca, cb);
        },

        updateUserEmailBreachesConfig: (args, cb) => {
            const data = {};
            data.emailBreachesConfig = {
                selectedCompanies: args.request$.body.selectedCompanies
            };
            saveUserByEmail(data, args.request$.body.email, seneca, cb);
        },

        updateUserBlacklistedDomainsConfig: (args, cb) => {
            const data = {};
            data.blacklistedDomainsConfig = {
                selectedCompanies: args.request$.body.selectedCompanies
            };
            saveUserByEmail(data, args.request$.body.email, seneca, cb);
        },

        updateUserCVEReportsConfig: (args, cb) => {
            const data = {};
            data.cveReportsConfig = {
                selectedVendors: args.request$.body.selectedVendors
            };
            saveUserByEmail(data, args.request$.body.email, seneca, cb);
        },

        getAllEmailsToBeNotifiedOnVendorCreation: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        const emailsToBeNotified = await serverHelpers.getAllFromMongoByCollectionName('notify_emails_map');
                        if (emailsToBeNotified) {
                            cb({ok: true, emailsToBeNotified: emailsToBeNotified});
                        } else {
                            console.error('In getAllEmailsToBeNotifiedOnVendorCreation - Error could not get all admin users!');
                            cb({ok: false});
                        }
                    } else {
                        console.error('In getAllEmailsToBeNotifiedOnVendorCreation Error: Unauthorized User');
                        cb({ok: false});
                    }
                } else {
                    console.error('In getAllEmailsToBeNotifiedOnVendorCreation Error: Unauthorized User');
                    cb({ok: false});
                }
            } catch (e) {
                console.error('In getAllEmailsToBeNotifiedOnVendorCreation Error: ' + e);
                cb({ok: false});
            }
        },

        updateEmailsToBeNotifiedOnVendorCreationOnMongo: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        await serverHelpers.updateEmailsToBeNotifiedOnVendorCreationOnMongo(args.request$.body.data);
                        cb({ok: true});
                    } else {
                        console.error('In updateEmailsToBeNotifiedOnVendorCreationOnMongo Error: Unauthorized User');
                        cb({ok: false});
                    }
                } else {
                    console.error('In updateEmailsToBeNotifiedOnVendorCreationOnMongo Error: Unauthorized User');
                    cb({ok: false});
                }
            } catch (e) {
                console.error('In updateEmailsToBeNotifiedOnVendorCreationOnMongo Error: ' + e);
                cb({ok: false});
            }
        },

        getAllUsers: (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                helpers.isAdminUserByID(authUser, seneca)
                    .then((isAdmin) => {
                        if (isAdmin) {
                            seneca.act('role:userdata, list:user', {query: {}}, (err, users) => {
                                const formatedUsers = [];
                                async.each(users, (user, done) => {
                                    if (user.name !== null && typeof user.name === 'object') {
                                        user.name = user.name.firstName + ' ' + user.name.lastName;
                                    } else if (!user.name) {
                                        user.name = '';
                                    }
                                    const userPreferencesConfig = {
                                        displayScoreBackwards: false,
                                        displayIntelTableViewFirst: false,
                                        gaugeDisplay: false
                                    };

                                    formatedUsers.push({
                                        id: user.id ? user.id.toString() : '',
                                        name: user.name ? user.name.toString() : '',
                                        nick: user.nick ? user.nick.toString() : '',
                                        email: user.email ? user.email.toString() : '',
                                        userRole: user.userRole ? user.userRole.toString() : '',
                                        active: user.active ? user.active.toString() : '',
                                        failedLoginCount: user.failedLoginCount ? user.failedLoginCount.toString() : '',
                                        twoFactorOnLogin: user.twoFactorOnLogin ? user.twoFactorOnLogin : false,
                                        canExportCVEReports: user.canExportCVEReports ? user.canExportCVEReports : false,
                                        logo: user.logo ? user.logo : {},
                                        isUserAllowedToAddVendors: user.isUserAllowedToAddVendors ? user.isUserAllowedToAddVendors : false,
                                        isInDemoMode: user.isInDemoMode ? user.isInDemoMode : false,
                                        isVendorAssessmentEnabled: user.isVendorAssessmentEnabled ? user.isVendorAssessmentEnabled : false,
                                        userPreferencesConfig: user.userPreferencesConfig ? user.userPreferencesConfig : userPreferencesConfig,
                                        created: user.when ? user.when.toString() : '',
                                        confirmed: user.confirmed ? user.confirmed.toString() : '',
                                        admin: user.userRole ? user.userRole === 'admin' : false
                                    });
                                    done();
                                }, () => {
                                    cb(null, {ok: true, users: formatedUsers});
                                });
                            });
                        } else {
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        }
                    })
                    .catch(() => {
                        cb();
                    });
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        getOrganizationsByUserId: async (args, cb) => {
            let authUser = args.request$.session.passport.user.id;
            let needToCheckAdmin = args.request$.body.checkAdmin;
            let adminCheck = true;

            if (needToCheckAdmin) {
                try {
                    adminCheck = await helpers.isAdminUserByID(authUser, seneca);
                } catch (e) {
                    console.log("In getOrganizationsByUserId - Failed to get all organization users without admin users: " + e);
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            }

            if (authUser && adminCheck && typeof adminCheck === 'boolean') {
                try {
                    if (args.request$.body.userId) {
                        authUser = args.request$.body.userId;
                    }
                    const res = await serverHelpers.getOrganizationsByUserId(authUser);
                    if (res) {
                        const orgArrForUserInputWithoutAdminUsers = await serverHelpers.eraseAdminUsersFromOrgAllowedUsers(res);

                        console.log("In getOrganizationsByUserId - got all organization users without admin users");
                        cb({ok: true, orgArrForUserInput: orgArrForUserInputWithoutAdminUsers});
                    } else {
                        console.log("In getOrganizationsByUserId - Failed to get all organization users without admin users");
                        cb({ok: false});
                    }
                } catch (e) {
                    console.log("In getOrganizationsByUserId - Failed to get all organization users without admin users: " + e);
                    cb({ok: false});
                }

            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }

        },
        getOrganizationByProject: async (args, cb) => {
            let authUser = args.request$.session.passport.user.id;
            let projectId = args.request$.body.projectId;

            try {
                const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                if (isAdmin && projectId) {
                    try {
                        const res = await serverHelpers.getOrganizationByProject(projectId);
                        let curOrg = {};
                        if (res) {
                            if(res._id && typeof res._id === 'object' && res.organizationName){
                                curOrg.id = res._id.valueOf().toString();
                                curOrg.name = res.organizationName;
                                console.log("In getOrganizationByProject - success! current organization: " + res.organizationName );
                                cb({ok: true, curOrg: curOrg});
                            } else if(res.message){
                                cb({ok: true, message: res.message});
                            } else {
                                console.error("In getOrganizationByProject - Failed to get organization");
                                cb({ok: false});
                            }
                        } else {
                            console.error("In getOrganizationByProject - Failed to get organization");
                            cb({ok: false});
                        }
                    } catch (e) {
                        console.error("In getOrganizationByProject - Failed to get organization");
                        cb({ok: false});
                    }
                } else {
                    console.error("In getOrganizationByProject - Unauthorized User or props are missing.");
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } catch (e) {
                console.error("In getOrganizationByProject - Failed to get organization Error: " + e);
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        getProjectsByOrgId: async (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                try {
                    const selectedProjects = await serverHelpers.getProjectsByOrgId(args.request$.body.orgId);
                    if (selectedProjects) {
                        const allPro = await serverHelpers.getAllFromMongoByCollectionName('project_map');
                        if (allPro) {
                            const projectsWithFullInfo = await extraHelpers.getFullSelectedObjectsInfoById(allPro, selectedProjects);
                            if (projectsWithFullInfo) {
                                console.log("In getProjectsByOrgId - got all projects.");
                                cb(null, projectsWithFullInfo);
                            }
                        } else {
                            console.log("In getProjectsByOrgId - Failed to get all projects.");
                            cb(null);
                        }
                    } else {
                        console.log("In getProjectsByOrgId - Failed to get all projects.");
                        cb(null);
                    }
                } catch (e) {
                    console.log("In getProjectsByOrgId - Failed to get all projects: " + e);
                    cb(null);
                }

            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        getProjectsByUserId: async (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                try {
                    const res = await serverHelpers.getProjectsByUserId(authUser);
                    if (res) {
                        console.log("In getProjectsByUserId - got all projects users");
                        cb(null, res);
                    } else {
                        console.log("In getProjectsByUserId - Failed to get all projects users");
                        cb(null);
                    }
                } catch (e) {
                    console.log("In getProjectsByUserId - Failed to get all projects users: " + e);
                    cb(null);
                }

            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        deleteUser: (args, cb) => {
            const authUser = args.request$.body.uid;
            if (authUser) {
                helpers.isAdminUserByID(authUser, seneca)
                    .then((isAdmin) => {
                        if (isAdmin) {
                            seneca.act('role:userdata, remove:user', {id: args.request$.body.id}, (err) => {
                                if (err) {
                                    cb(null, {ok: false, error: err});
                                }
                                cb(null, {ok: true});
                            });
                        } else {
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        }
                    })
                    .catch(() => {
                        cb();
                    });
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        isAuthUser: (args, cb) => {
            const authUser = args.request$.body.uid;
            if (authUser) {
                helpers.isAdminUserByID(authUser, seneca)
                    .then((isAdmin) => {
                        if (isAdmin) {
                            cb(null, {ok: true});
                        } else {
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        }
                    })
                    .catch(() => {
                        cb();
                    });
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        exportReport: (args, cb) => {
            const user = args.request$.session.passport.user;
            const userEmail = args.request$.session.passport.user.email;
            const authUser = args.request$.session.passport.user.id;
            let isInDemoMode = user && user.isInDemoMode;
            if (authUser) {
                console.log('User id [', authUser, '] requested to export an intel report');

                return helpers.isUserAllowedToExportIntelByID(authUser, args.request$.body.type, seneca)
                    .then(async (isAllowed) => {
                        if (isAllowed) {
                            console.log('User id [', authUser, '] is allowed to export an intel report as ', args.request$.body.type);
                            if(args.request$.body.companies && args.request$.body.companies[0] && args.request$.body.companies[0].id){
                                try {
                                    let data = {
                                        uid: authUser,
                                        orgId: args.request$.body.orgId,
                                        companyID: args.request$.body.companies[0].id,
                                        saveScoresOnDB: true
                                    };
                                    await promiseAct('role:intelQuery,cmd:getMaxCVSScore', data);
                                    console.log('in exportReport - company has been recalculated!');

                                } catch (e) {
                                    console.error('in exportReport - Error - Failed to recaluclate company!');
                                }
                            } else {
                                console.log('in exportReport - Did not recalculate company - no company id found');
                            }

                            let companies = args.request$.body.companies;
                            let projects = args.request$.body.projects;
                            const data = {
                                uid: authUser,
                                userEmail: userEmail,
                                windowLocation: args.request$.body.windowLocation,
                                isInDemoMode: isInDemoMode,
                                displayScoreBackwards: args.request$.body.displayScoreBackwards || false,
                                orgId: args.request$.body.orgId,
                                type: args.request$.body.type,
                                projects: args.request$.body.projects,
                                companies: args.request$.body.companies,
                                manualData: args.request$.body.manualData,
                                manualDataID: args.request$.body.filesID,
                                reportKind: args.request$.body.reportKind
                            };

                            if (user && user.logo) {
                                data.logo = user.logo;
                            }

                            let dataArr = [];
                            if(companies && companies.length > 0){
                                await helpers.asyncForEach(companies, async (curComp) => {
                                    data.companies = [curComp];
                                    let res = await helpers.generateCurReport(args.request$.body.reportKind , data, promiseAct, seneca);
                                    if(res && res.ok === false){
                                        cb(null, {ok: false, error: e});
                                    }
                                    dataArr.push(res.data);
                                });
                                cb(null, {ok: true, dataArr: dataArr});
                            }
                            else if(projects && projects.length > 0){
                                await helpers.asyncForEach(projects, async (curProject) => {
                                    data.projects = [curProject];
                                    let res = await helpers.generateCurReport(args.request$.body.reportKind , data, promiseAct, seneca);
                                    if(res && res.ok === false){
                                        cb(null, {ok: false, error: e});
                                    }
                                    dataArr.push(res.data);
                                });
                                cb(null, {ok: true, dataArr: dataArr});
                            }
                        } else {
                            console.log('User id [', authUser, '] is not allowed to export an intel report as ', args.request$.body.type);

                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        }
                    })
                    .catch((e) => {
                        console.error('Error while checking if user [', authUser, '] is allowed to export an intel report: ', e);

                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    });
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        setupTwoFactAuth: (msg, cb) => {
            const id = msg.args.user.id;
            serverHelpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed || msg.args.user.firstLogin) {
                        const secret = speakeasy.generateSecret({length: 20});
                        msg.args.user.tempMfaSec = secret.base32;
                        const user_entity = seneca.make$('sys', 'user');
                        user_entity.list$({email: msg.args.user.email}, (err, user) => {
                            user[0].save$(msg.args.user, () => {
                                console.log('User Saved');
                                QRCode.toDataURL(secret.otpauth_url, (err, data_url) => {
                                    cb(null, {ok: true, qr: data_url});
                                });
                            });
                        });
                    } else {
                        cb(null, {
                            ok: false,
                            mfaError: 'User Not Allowed to enable/disable Two Factor Authentication.'
                        });
                    }
                });
        },

        enableTwoFactor: (msg, cb) => {
            const id = msg.args.user.id;
            const user_entity = seneca.make$('sys', 'user');
            serverHelpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        user_entity.list$({email: msg.args.user.email}, (err, user) => {
                            if (!user.twoFactorOnLogin) {
                                msg.args.user.mfaEnabled = msg.args.body.enableTwoFactor;
                            }
                            user[0].save$(msg.args.user, () => {
                                console.log('User: ', msg.args.user.id, ' updated two factor to : ', msg.args.user.mfaEnabled);
                                cb(null, {ok: true, enableTwoFactor: msg.args.user.mfaEnabled});
                            });
                        });
                    } else {
                        cb(null, {
                            ok: false,
                            mfaError: 'User Not Allowed to enable/disable Two Factor Authentication.'
                        });
                    }
                });
        },

        verifyTwoFactor: (msg, cb) => {
            seneca.act('role:userdata, get:user', {id: msg.args.user.id}, (err, user) => {
                if (user.firstLogin === true) {
                    user.firstLogin = false;
                }
                saveUserById({firstLogin: user.firstLogin}, msg.args.user.id, seneca);
            });

            const user_entity = seneca.make$('sys', 'user');
            user_entity.list$({email: msg.args.user.email}, (err, user) => {
                const verified = speakeasy.totp.verify({
                    secret: user[0].tempMfaSec,
                    encoding: 'base32',
                    token: msg.args.body.code,
                    window: 6
                });

                if (verified) {
                    console.log('Verified mfa');
                    msg.args.user.mfaSecret = user[0].tempMfaSec;
                    msg.args.user.mfaEnabled = true;
                    delete msg.args.user.tempMfaSec;
                    user[0].save$(msg.args.user, () => {
                        console.log('User Saved');
                        cb(null, {ok: true});
                    });
                } else {
                    console.log('mfa not verified');
                    cb(null, {ok: false});
                }
            });
        }
    };
};

function getUserCompaniesByEmail(email, context, cb) {
    const user_entity = context.make$('sys', 'user');
    user_entity.list$({email: email}, (err, user) => {
        cb(null, {
            ok: true,
            dataleaksConfig: user[0].dataleaksConfig,
            emailBreachesConfig: user[0].emailBreachesConfig,
            blacklistedDomainsConfig: user[0].blacklistedDomainsConfig
        });
    });
}

function getUserVendorsConfigByEmail(email, context, cb) {
    const user_entity = context.make$('sys', 'user');
    user_entity.list$({email: email}, (err, user) => {
        cb(null, {ok: true, cveReportsConfig: user[0].cveReportsConfig});
    });
}

function saveUserByEmail(data, email, context, cb) {
    const user_entity = context.make$('sys', 'user');
    user_entity.list$({email: email}, (err, user) => {
        user[0].save$(data, (err) => {
            if (err) {
                console.error('Error In saveUserByEmail: ', err)
            }
            console.log('User Saved');
            cb(null, {ok: true, user: data});
        });
    });
}

function saveUserById(data, id, context, cb) {
    const user_entity = context.make$('sys', 'user');
    user_entity.list$({id: id}, (err, user) => {
        data.name = extraHelpers.validateUserName(user[0]);

        user[0].save$(data, (err) => {
            if (err) {
                console.error("Error saveUserById: ", err);
            }

            if (user[0].name !== null && typeof user[0].name === 'object') {
                user[0].name = user[0].name.firstName + ' ' + user[0].name.lastName;
            } else if (!user[0].name) {
                user[0].name = '';
            }
            const userPreferencesConfig = {
                displayScoreBackwards: false,
                displayIntelTableViewFirst: false,
                gaugeDisplay: false
            };

            const formatedUser = {
                id: user[0].id ? user[0].id.toString() : '',
                name: user[0].name ? user[0].name.toString() : '',
                nick: user[0].nick ? user[0].nick.toString() : '',
                email: user[0].email ? user[0].email.toString() : '',
                active: user[0].active ? user[0].active.toString() : '',
                userRole: user[0].userRole ? user[0].userRole.toString() : '',
                failedLoginCount: user[0].failedLoginCount ? user[0].failedLoginCount.toString() : '',
                twoFactorOnLogin: user[0].twoFactorOnLogin ? user[0].twoFactorOnLogin : false,
                isUserAllowedToAddVendors: user[0].isUserAllowedToAddVendors ? user[0].isUserAllowedToAddVendors : false,
                isInDemoMode: user[0].isInDemoMode ? user[0].isInDemoMode : false,
                isVendorAssessmentEnabled: user[0].isVendorAssessmentEnabled ? user[0].isVendorAssessmentEnabled : false,
                userPreferencesConfig: user[0].userPreferencesConfig ? user[0].userPreferencesConfig : userPreferencesConfig,
                canExportCVEReports: user[0].canExportCVEReports ? user[0].canExportCVEReports : false,
                logo: user[0].logo ? user[0].logo : {},
                created: user[0].when ? user[0].when.toString() : '',
                confirmed: user[0].confirmed ? user[0].confirmed.toString() : '',
                admin: user[0].userRole ? user[0].userRole === 'admin' : false
            };
            console.log('User Saved');
            if (cb) {
                cb(null, {ok: true, user: formatedUser});
            }
        });
    });
}

