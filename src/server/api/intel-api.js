const Promise = require('bluebird');
const helpers = require('./api-helpers');
const intelHelpers = require('../../server_modules/utils/intel-helpers.js');
const {BLACKLISTS_TYPE, EMAIL_BREACHES_TYPE} = require('../../../config/consts.js');


module.exports = function(seneca) {
    const promiseAct = Promise.promisify(seneca.act, {context: seneca});

    return {
        // Call getIntel from the Intel microservice.
        getVulnsByIP: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getVulnsByIP', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get CVEs.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getPortsByIP: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getPortsByIP', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get ports.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getASNByIP: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getASNByIP', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get ASNs.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getLocations: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user', 'basic'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        let data;

                        if (arg.args.body.companyID) {
                            data = {
                                uid: id,
                                companyID: arg.args.body.companyID
                            };
                        } else {
                            data = {
                                uid: id,
                                project: arg.args.body.project
                            };
                        }
                        return promiseAct('role:intelQuery,cmd:getLocations', data);
                    } else {
                        console.log('User ', id, ' role does not allow to get locations.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getVPNByIP: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getVPNByIP', {
                            uid: arg.args.body.uid,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get VPNs.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getBlacklistsByDomains: (args, cb) => {
            const id = args.request$.session.passport.user.id;
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:blacklists, cmd:getDomainsFindings', {
                            uid: id,
                            domains: args.request$.body.domains
                        });
                    } else {
                        console.log('User ', id, ' role is not allowed to search blacklists');
                        return Promise.resolve([]);
                    }
                })
                .then(async (res) => {
                    const resultsMerge = {};
                    const findingsRes = [];
                    if (res && res.ok && Array.isArray(res.findings)) {
                        console.log('Blacklists Findings data returned ', res.findings.length + ' rows.');
                        const companies = args.request$.body.companies;
                        res.findings.map((entity) => {
                            entity.relatedCompanies = helpers.whoIsRelatedByDomain(companies, entity.domain);
                            findingsRes.push(entity);
                        });
                    }
                    let fmRes = [];
                    if (args.request$.body.orgId && args.request$.body.orgId !== 'No Id') {
                        try {
                            fmRes = await intelHelpers.getAssetUserInputAndMitigatedNodesByType(BLACKLISTS_TYPE, args.request$.body.orgId, true);
                        } catch (e) {
                            console.log('In getAssetUserInputAndMitigatedNodesByType - ' + BLACKLISTS_TYPE + ' - no data returned');
                        }
                        if (fmRes && fmRes.length > 0) {
                            resultsMerge.assetUserInputAndMitigatedData = fmRes;
                        }
                    }
                    resultsMerge.blacklistsData = findingsRes;
                    cb(null, resultsMerge);
                })
                .catch((e) => {
                    console.log('Error in getBlacklistsByDomains: ', e);
                    cb();
                });
        },
        getEmailBreachesData: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log('User: ', id, ' requested Email Breaches data.');
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getEmailBreachesData', {
                            uid: id,
                            domains: arg.args.body.domains
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get Email Breaches data.');
                        return Promise.resolve([]);
                    }
                })
                .then(async (res) => {
                    const resultsMerge = {};
                    const findingsRes = [];

                    if (res && Array.isArray(res)) {
                        console.log('Email Breaches data returned ', res.length + ' rows.');
                        const companies = arg.args.body.companies;
                        res.map((entity) => {
                            entity.relatedCompanies = helpers.whoIsRelatedByDomain(companies, entity.domain);
                            findingsRes.push(entity);
                        });
                    }

                    let fmRes = [];
                    if (arg.args.body.orgId && arg.args.body.orgId !== 'No Id') {
                        try {
                            fmRes = await intelHelpers.getAssetUserInputAndMitigatedNodesByType(EMAIL_BREACHES_TYPE, arg.args.body.orgId, true);
                        } catch (e) {
                            console.log('In getAssetUserInputAndMitigatedNodesByType - ' + EMAIL_BREACHES_TYPE + ' - no data returned');
                        }
                        if (fmRes && fmRes.length > 0) {
                            resultsMerge.assetUserInputAndMitigatedData = fmRes;
                        }
                    }
                    resultsMerge.emailBreachesData = findingsRes;
                    done(null, resultsMerge);
                });
        },
        getEmailBreachInfo: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log('User: ', id, ' requested Email Breach nodeID: ', arg.args.body.nodeID, ' info.');
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getEmailBreachInfo', {
                            uid: id,
                            nodeID: arg.args.body.nodeID
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get Email Breach Info.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    if (res && Array.isArray(res)) {
                        console.log('Email Breach nodeID: ', arg.args.body.nodeID, ' returned ', res.length + ' nodes info.');
                    }
                    done(null, res);
                });
        },
        getNodeNearestInfo: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log('User: ', id, ' requested intel nodeID: ', arg.args.body.nodeID, ' info.');
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getNodeNearestInfo', {
                            uid: id,
                            project: arg.args.body.project,
                            nodeID: arg.args.body.nodeID,
                            nodeType: arg.args.body.nodeType,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get Node Nearest Info.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    if (res && Array.isArray(res)) {
                        console.log('NodeID: ', arg.args.body.nodeID, ' returned ', res.length + ' nearest nodes info.');
                        if (arg.args.body.nodeType === 'port' && arg.args.body.portNumber) {
                            res = res.concat(helpers.getKnownPortInfo(arg.args.body.portNumber));
                        }
                    }
                    done(null, res);
                });
        },
        getAllIPs: async (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);

            let allowed = await helpers.isUserRoleAllowed(id, ['user'], seneca);
            if (allowed) {
                let res = await promiseAct('role:intelQuery,cmd:getAllIPs', {
                    uid: id,
                    project: arg.args.body.project,
                    orgId: arg.args.body.orgId,
                    companiesFilter: arg.args.body.graphFilter,
                    startDate: arg.args.body.startDate,
                    endDate: arg.args.body.endDate
                });
                done(null, res);
            } else {
                console.log('User ', id, ' role does not allow to get All IPs.');
                return Promise.resolve([]);
            }
        },
        getByDomain: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getByDomain', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            domain: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get CVE for domain.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getSpf: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getSpf', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get spf.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getDmarc: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getDmarc', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get dmarc.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getProductsByIP: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getProductsByIP', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get products.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getIsp: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getIsp', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get ISP.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getBuckets: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:intelQuery,cmd:getBuckets', {
                            uid: id,
                            project: arg.args.body.project,
                            orgId: arg.args.body.orgId,
                            companiesFilter: arg.args.body.graphFilter,
                            startDate: arg.args.body.startDate,
                            endDate: arg.args.body.endDate
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get ISP.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    done(null, res);
                });
        },
        getCVESeverity: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['basic', 'user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        let data;

                        if (arg.args.body.companyID) {
                            data = {
                                uid: id,
                                companyID: arg.args.body.companyID
                            };
                        } else {
                            data = {
                                uid: id,
                                project: arg.args.body.project
                            };
                        }
                        return promiseAct('role:intelQuery,cmd:getCVESeverity', data);
                    } else {
                        console.log('User ', id, ' role does not allow to get CVE severity.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    const newObjArr = {};
                    if (res) {
                        let criticalCount = 0, highCount = 0, mediumCount = 0, lowCount = 0;
                        for (let i = 0; i < res.length; i++) {
                            if (res[i] >= 9) {
                                criticalCount++;
                            } else if (7.5 <= res[i] && res[i] < 9) {
                                highCount++;
                            } else if (3 <= res[i] && res[i] < 7.5) {
                                mediumCount++;
                            } else if (3 > res[i]) {
                                lowCount++;
                            }
                        }
                        newObjArr['critical'] = criticalCount;
                        newObjArr['high'] = highCount;
                        newObjArr['medium'] = mediumCount;
                        newObjArr['low'] = lowCount;
                    }
                    done(null, newObjArr);
                })
                .catch((e) => {
                    console.log('Error in getCVESeverity ', e);
                    return Promise.reject(e);
                });
        },
        getCVECountPerHostname: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            console.log(id);
            helpers.isUserRoleAllowed(id, ['basic', 'user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        let data;

                        if (arg.args.body.companyID) {
                            data = {
                                uid: id,
                                companyID: arg.args.body.companyID
                            };
                        } else {
                            data = {
                                uid: id,
                                project: arg.args.body.project
                            };
                        }
                        return promiseAct('role:intelQuery,cmd:getCVECountPerHostname', data);
                    } else {
                        console.log('User ', id, ' role does not allow to get CVE count pre hostname.');
                        return Promise.resolve([]);
                    }
                })
                .then((res) => {
                    const newObjArr = [];
                    if (res) {
                        for (let i = 0; i < res.length; i++) {
                            newObjArr.push({
                                'hostname': res[i].hostname.properties.hostname,
                                'cve': res[i].CVECount.low
                            });
                        }
                    }
                    done(null, newObjArr);
                })
                .catch((e) => {
                    console.log('Error in getCVECountPerHostname ', e);
                    return Promise.reject(e);
                });
        },
        getMaxCVSScore: async (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            let res;
            try {
                let allowed = await helpers.isUserRoleAllowed(id, ['basic', 'user'], seneca);
                if (allowed) {
                    let data = {};


                    if (arg.args.body.companyID) {
                        data = {
                            uid: id,
                            companyID: arg.args.body.companyID
                        };
                    } else if (arg.args.body.companyIDs) {
                        data = {
                            uid: id,
                            companyIDs: arg.args.body.companyIDs
                        };
                    } else {
                        data = {
                            uid: id,
                            project: arg.args.body.project
                        };

                        // Prevent too much data on too many companies in Dashboard.
                        if (arg.args.body.sliceStartIndex != null && arg.args.body.sliceMaxLength) {
                            data.sliceStartIndex = arg.args.body.sliceStartIndex;
                            data.sliceMaxLength = arg.args.body.sliceMaxLength;
                        }

                        // Return partial result by selected display sort option.
                        if (arg.args.body.selectedDisplaySort) {
                            data.selectedDisplaySort = arg.args.body.selectedDisplaySort;
                        }
                    }
                    if (arg.args.body.saveScoresOnDB) {
                        data.saveScoresOnDB = true;
                    }
                    if (arg.args.body.orgId) {
                        data.orgId = arg.args.body.orgId;
                    }
                    if(arg.args.body.hasOwnProperty("getExtraInfo")){
                        data.getExtraInfo = arg.args.body.getExtraInfo
                    }
                    res = await promiseAct('role:intelQuery,cmd:getMaxCVSScore', data);
                } else {
                    console.log('User ', id, ' role does not allow to get MAX CVE score.');
                    res = [];
                }
            } catch (err) {
                console.error(err);
                done(null, {});
            }

            console.log(res);
            done(null, res);
        },
        updateFindingStatus: async (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            const userEmail = arg.request$.session.passport.user.email;
            try {
                let isAllowed = await helpers.isUserRoleAllowed(id, ['user'], seneca);
                if (isAllowed) {
                    if (arg.args.body.findingStatusData) {
                        arg.args.body.findingStatusData.uid = id;
                        arg.args.body.findingStatusData.userEmail = userEmail;
                        let res = await promiseAct('role:intelQuery,cmd:updateFindingStatus', {
                            findingStatusData: arg.args.body.findingStatusData
                        });
                        if (res) {
                            done(null, res);
                        } else {
                            console.error('In updateFindingStatus() - failed to update status.');
                            return Promise.resolve([]);
                        }
                    } else {
                        console.error('In updateFindingStatus() - failed to update status.');
                        return Promise.resolve([]);
                    }
                } else {
                    console.error('In updateFindingStatus() - User ', id, ' role does not allow to get CVEs.');
                    return Promise.resolve([]);
                }
            } catch (e) {
                console.error('In updateFindingStatus() - failed to update status.');
                return Promise.resolve([]);
            }
        }
    };
};
