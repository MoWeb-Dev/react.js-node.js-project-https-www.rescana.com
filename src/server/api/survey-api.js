const moment = require('moment');
const Promise = require('bluebird');
const jwt = require('jsonwebtoken');
const authCfg = require('../middleware/auth-config');
const config = require('app-config');
const helpers = require('../../server_modules/utils/utils-helpers');
const serverHelpers = require('./api-helpers');
const extraHelpers = require('./helpers.js');
const fs = require('fs');
const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const mongoUrl = config.addresses.mongoConnectionString;

module.exports = function (seneca) {
    const promiseAct = Promise.promisify(seneca.act, {context: seneca});

    const saveSurvey = async (arg, done) => {
        seneca.client({
            host: config.addresses.storeServiceAddress,
            port: 10904
        });

        try{
            const id = arg.request$.session.passport.user.id;
            const allowed = await serverHelpers.isUserRoleAllowed(id, ['user'], seneca);
            if (allowed) {
                if(arg.request$.body.selectedOrgId){
                    delete arg.request$.body.selectedOrgId;
                }
                let data = await promiseAct('role:surveystore, updateSurvey:survey', {
                    sid: arg.request$.body.sid,
                    data: arg.request$.body
                });
                console.log('Saving survey');
                done(null, data);
            } else {
                console.log('User ', id, ' role does not allow to delete survey.');
                return Promise.resolve([]);
            }
        } catch (e) {
            console.log('In saveSurvey - Error: ', e);
            done();
        }
    };
    return {
        addRevokedTokenToList: (args, cb) => {
            const data = {
                token: args.request$.body.token
            };
            return new Promise((resolve) => {
                return MongoClient.connect(mongoUrl, (err, client) => {
                    const db = client.db('darkvision');
                    let collection = db.collection('revoked_tokens_map');

                    if (data.token) {
                        collection.insert(data, {upsert: true}, () => {
                            let collectionForUpdate = db.collection('tokens_map');
                            collectionForUpdate.updateOne({token: data.token}, {$set: {isTokenRevoked: true}});
                            client.close();
                            resolve(true);
                            cb(null, {ok: true});
                        });
                    } else {
                        client.close();
                        resolve(false);
                        cb(null, {ok: false});
                    }
                });
            });
        },
        sendSurveyAnswersEmail: (args, cb) => {
            if (!isNaN(args.request$.body.senderEmailExpiry) &&
                extraHelpers.isStringValidAndNotEmpty(args.request$.body.said) &&
                extraHelpers.isStringValidAndNotEmpty(args.request$.body.surveyName) &&
                extraHelpers.isStringValidAndNotEmpty(args.request$.body.senderEmail) &&
                extraHelpers.isStringValidAndNotEmpty(args.request$.body.recipient) &&
                extraHelpers.isStringValidAndNotEmpty(args.request$.body.sender)) {
                const id = args.request$.session.passport.user.id;
                const orgName = extraHelpers.isStringValidAndNotEmpty(args.request$.body.organizationName)? args.request$.body.organizationName : "";
                const companyName = extraHelpers.isStringValidAndNotEmpty(args.request$.body.companyName)? args.request$.body.companyName : "this company";
                const expiry = args.request$.body.senderEmailExpiry + 'd' || '3d';
                const lastDateToCompleteSurvey = moment().add(args.request$.body.senderEmailExpiry - 1 || 2, 'days' ).format("LL");
                const payload = {
                    iat: Math.floor(Date.now() / 1000),
                    uid: id,
                    senderEmail: args.request$.body.senderEmail,
                    surveyName: args.request$.body.surveyName,
                    said: args.request$.body.said
                };
                const token = jwt.sign(payload, authCfg.jwtSecret, {expiresIn: expiry});

                const url = config.addresses.websiteForEmail + '/survey-continue?said=' + args.request$.body.said + '&token=' + token;

                const text = extraHelpers.getSurveyAnswersEmailText(url, lastDateToCompleteSurvey, companyName, orgName, args.request$.body.message);

                const msgObj = {
                    to: args.request$.body.recipient,
                    subject: 'Security survey from ' + args.request$.body.sender,
                    text: text
                };

                serverHelpers.isUserRoleAllowed(id, ['basic', 'user'], seneca)
                    .then((allowed) => {
                        if (allowed) {

                            serverHelpers.saveTokenInfoToDB(args, token).then((res) => {
                                if (res) {
                                    console.log('Email Token has been saved to db');
                                } else {
                                    console.log('Failed to save Email Token to db');
                                }
                            });

                            return promiseAct('role:mailer, cmd:send', msgObj)
                                .then(() => {
                                    cb(null, {ok: true});
                                    console.log('Survey Email has been sent');
                                });
                        } else {
                            console.log('User ', id, ' role does not allow to send emails.');
                            return Promise.resolve([]);
                        }
                    })
                    .catch((e) => {
                        console.log('mailer error ', e);
                    });
            } else {
                cb(null, {ok: false});
            }
        },
        createSurveyLink: (args, cb) => {
            const id = args.request$.session.passport.user.id;
            if (!isNaN(args.request$.body.senderEmailExpiry)) {
                const expiry = args.request$.body.senderEmailExpiry + 'd' || '3d';

                const payload = {
                    iat: Math.floor(Date.now() / 1000),
                    uid: id,
                    senderEmail: args.request$.body.senderEmail,
                    surveyName: args.request$.body.surveyName,
                };

                const token = jwt.sign(payload, authCfg.jwtSecret, {expiresIn: expiry});

                const url = config.addresses.website + '/survey-continue?said=' + args.request$.body.said + '&token=' + token;

                serverHelpers.isUserRoleAllowed(id, ['basic', 'user'], seneca)
                    .then((allowed) => {
                        if (allowed) {

                            serverHelpers.saveTokenInfoToDB(args, token).then((res) => {
                                if (res) {
                                    console.log('Link Token has been saved to db');
                                } else {
                                    console.log('Failed to save Link Token to db');
                                }
                            });

                            console.log('Survey Link has been created for user: ', id);
                            return Promise.resolve(cb(null, {ok: true, surveyLink: url}));
                        } else {
                            console.log('User ', id, ' role does not allow to send emails.');
                            return Promise.resolve([]);
                        }
                    })
                    .catch((e) => {
                        console.log('mailer error ', e);
                    });
            } else {
                cb(null, {ok: false});
            }
        },
        // Save surveys to DB
        saveSurvey: saveSurvey,
        downloadSurveyWithAnswers: (arg, done) => {
            seneca.client({
                host: 'localhost',
                port: 10701
            });

            const id = arg.request$.session.passport.user.id;
            return serverHelpers.isUserRoleAllowed(id, ['user', 'basic'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:survey, export:surveyToExcel', {
                            query: {
                                uid: id,
                                said: arg.request$.query.said,
                                excelFormat: arg.request$.query.excelFormat,
                                orgId: arg.request$.query.orgId
                            }
                        });
                    }
                })
                .then((result) => {
                    const app_entity = seneca.make('surveyFiles', 'surveyFile', 'map');
                    const list$ = Promise.promisify(app_entity.list$, {context: app_entity});

                    return list$({id: result.id});
                })
                .then((survey) => {
                    return new Promise((resolve, reject) => {
                        if (survey) {
                            fs.writeFile(path.join(__dirname, '../../../surveyExports', survey[0].fileName), survey[0].buffer.buffer, 'binary', (err) => {
                                if (err) {
                                    console.log('Error writing file to disk.');
                                    return reject();
                                }
                                console.log('Done Saving survey excel file');
                                return resolve(survey[0].fileName);
                            });
                        } else {
                            console.log('No survey found');
                            return reject();
                        }
                    });
                })
                .catch((e) => {
                    console.log('Error in downloadSurveyWithAnswers: ', e);
                })
                .then((fileName) => {
                    return done(null, {fileName: fileName});
                });
        },
        exportSingleSurveyReport: async (args, cb) => {
            const authUser = args.request$.session.passport.user.id;

            if (authUser) {
                try {
                    let surveyWithAnswer = await promiseAct('role:survey, get:surveyWithAnswers', {
                        query: {
                            uid: authUser,
                            said: args.request$.body.said
                        }
                    });

                    let res = await promiseAct('role:reportCreator, cmd:getSingleSurveyReport', {
                        data: {
                            uid: authUser,
                            survey: surveyWithAnswer,
                            orgId: args.request$.body.orgId
                        }
                    });

                    if (res && res.ok && res.data) {
                        console.log('Survey report exported successfully.');
                        cb(null, {ok: true, data: res.data});
                    } else {
                        cb(null, {ok: false});
                    }
                } catch (e) {
                    console.log('exportSingleSurveyReport: ', e);
                    cb(null, {ok: false, error: e});
                }
            } else {
                cb(null, {ok: false, error: 'Unauthorized user.'});
            }
        },
        createDefaultSurvey: async (arg, done)=>{
            try{
                const authUser = arg.request$.session.passport.user;
                const isAdmin = await serverHelpers.isAdminUserByID(authUser.id, seneca);

                //In case ADMIN created the template we dont change the properties we get from the ui
                //and save the template with the permissions to the organizations users that the admin choose on the ui.
                if(isAdmin){
                    await saveSurvey(arg, done);
                    console.log("In createDefaultSurvey - Done creating default survey template.");
                    done();

                //In case its NOT ADMIN and the user is part of an organization we get the organization info
                //and save the template with permissions to all the users in the organization
                } else if(arg.request$.body.selectedOrgId) {
                    const orgData = await serverHelpers.getOrgById(arg.request$.body.selectedOrgId);
                    if (orgData && orgData[0]) {
                        let orgId = '';
                        if(orgData[0]._id && typeof orgData[0]._id === 'object'){
                            orgId = orgData[0]._id.valueOf().toString();
                        } else if(orgData[0].id && typeof orgData[0].id === 'string'){
                            orgId = orgData[0].id;
                        }
                        if(extraHelpers.isOrgDataValidForSavingSurveyTemplate(orgData[0], orgId)) {
                            let orgObj = {id: orgId, orgUsers: orgData[0].allowedUsers, organizationName: orgData[0].organizationName};
                            arg.request$.body.uid = orgData[0].allowedUsers;
                            arg.request$.body.allowedOrganizations = [orgObj];
                            await saveSurvey(arg, done);
                            console.log("In createDefaultSurvey - Done creating default survey template.");
                            done();
                        } else{
                            console.error("In createDefaultSurvey - Failed to get organization info.");
                            done();
                        }
                    } else {
                        console.error("In createDefaultSurvey - Failed to get organization");
                        done();
                    }

                //In case its NOT ADMIN and the user is NOT part of an organization the permissions is only to the user
                //who created the template.
                } else {
                    await saveSurvey(arg, done);
                    console.log("In createDefaultSurvey - Done creating default survey template.");
                    done();
                }
            } catch (e) {
                console.error('In createDefaultSurvey() - Error: ', e);
                done();
            }
        },

        getSurvey: (arg, done) => {
            seneca.client({
                host: config.addresses.storeServiceAddress,
                port: 10904
            });

            const id = arg.request$.session.passport.user.id;
            try {
            serverHelpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        if (arg.request$.body.sid) {
                            return promiseAct('role:surveystore, listById:survey', {
                                query: {
                                    uid: id,
                                    sid: arg.request$.body.sid
                                }
                            }).then(async (data) => {
                                console.log('Getting survey Answers');
                                data = await helpers.cleanSurveyObject(data, id, seneca);
                                done(null, data);
                            }).catch((e) => {
                                console.log('getSurvey', e);
                                done();
                            });
                        } else {
                            return promiseAct('role:surveystore, listById:survey', {
                                query: {
                                    uid: id
                                }
                            }).then(async (data) => {
                                console.log('Getting surveys');
                                data = await helpers.cleanSurveyObject(data, id, seneca);
                                done(null, data);
                            }).catch((e) => {
                                console.log('getSurvey: ', e);
                                done();
                            });
                        }
                    } else {
                        console.log('User ', id, ' role does not allow to get survey.');
                        done();
                        return Promise.resolve([]);
                    }
                });
            } catch (e) {
                console.error('In getSurvey() - Error : ' + e);
            }
        },

        deleteSurvey: (args, cb) => {
            seneca.client({
                host: config.addresses.storeServiceAddress,
                port: 10904
            });

            const id = args.request$.session.passport.user.id;
            serverHelpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:surveystore, deleteBySid:survey', {
                            query: {
                                sid: args.request$.body.sid
                            }
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to delete survey.');
                        return Promise.resolve([]);
                    }
                })
                .then(() => {
                    console.log('Deleted survey successfully');
                    cb(null, {});
                })
                .catch((e) => {
                    console.log('deleteSurveyAnswers: ', e);
                    cb();
                });
        },

        deleteSurveyAnswers: (args, cb) => {
            seneca.client({
                host: config.addresses.storeServiceAddress,
                port: 10904
            });

            const id = args.request$.session.passport.user.id;
            serverHelpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:surveystore, deleteBySaid:surveyAnswers', {
                            query: {
                                said: args.request$.body.said
                            }
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to delete survey with answers.');
                        return Promise.resolve([]);
                    }
                })
                .then(() => {
                    console.log('Deleted survey answer successfully');
                    cb(null, {});
                })
                .catch((e) => {
                    console.log('deleteSurveyAnswers: ', e);
                    cb();
                });
        },
        updateSurveyLangBySid: (arg, done) => {
            seneca.client({
                host: 'localhost',
                port: 10701
            });

            console.log(arg.request$.body.sid + ' --- ' + arg.request$.body.lang);

            const id = arg.request$.session.passport.user.id;
            serverHelpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:surveystore, UpdateSurveyLang:survey', {
                            sid: arg.request$.body.sid,
                            lang: arg.request$.body.lang
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to update survey language.');
                        return Promise.resolve([]);
                    }
                })
                .then(() => {
                    console.log('Updating survey language');
                    done(null, {ok: true});
                })
                .catch((e) => {
                    console.log('updateSurveyLangBySid: ', e);
                    done();
                });
        },
        getSurveyTemplateFromExcel: (arg, done) => {
            seneca.client({
                host: 'localhost',
                port: 10701
            });

            const id = arg.request$.session.passport.user.id;
            serverHelpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:survey, get:getSurveyTemplateFromExcel', {
                            query: {
                                user: arg.request$.body.user,
                                templateName: arg.request$.body.templateName,
                                filePath: arg.request$.body.filePath
                            }
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get all survey templates from excel.');
                        return Promise.resolve([]);
                    }
                })
                .then((dataArr) => {
                    done(null, dataArr);
                })
                .catch((e) => {
                    console.log('getSurveyTemplateFromExcel: ', e);
                    done();
                });
        },
        getSurveyWithAnswers: (arg, done) => {
            seneca.client({
                host: 'localhost',
                port: 10701
            });
            const id = arg.request$.session.passport.user.id;
            console.log('Querying uid: ', id);
            console.log('Querying said: ', arg.request$.body.said);
            serverHelpers.isUserRoleAllowed(id, ['basic', 'user'], seneca).then((allowed) => {
                if (allowed) {
                    return promiseAct('role:survey, get:surveyWithAnswers', {
                        query: {
                            said: arg.request$.body.said
                        }
                    });
                } else {
                    console.log('User ', id, ' role does not allow to get survey with answers.');
                    return Promise.resolve([]);
                }
            }).then((data) => {
                console.log('Getting survey Answers');

                // Get all surveyAnswers' files and then check in fileScans in DB for findings.
                const allSurveyFiles = serverHelpers.getFilesOfSurveyWithAnswers(data);

                console.log('Getting survey Answers\' files scans status');

                // Fetch surveyAnswers' files and check if any fileNames exist as not SAFE files.
                return [promiseAct('role:fileScans, getScanUploadedFiles:fileScan', {
                    files: allSurveyFiles
                }), data];
            }).catch((e) => {
                console.log('Error in getSurveyWithAnswers: ', e);
                done();
            }).spread((filesResult, data) => {
                if (filesResult && filesResult.ok && filesResult.data && Array.isArray(filesResult.data) && filesResult.data.length > 0) {
                    const files = filesResult.data;
                    // Save the updated filesFindings in the questionnaire (data).
                    data = serverHelpers.updateFilesFindingsOfSurveyWithAnswers(files, data);
                }

                // data$ Turns seneca object to normal JS object.
                done(null, data.data$(false));
            }).catch((e) => {
                console.log('getSurveyWithAnswers: ', e);
                done();
            });
        },

        getAllSurveysWithAnswers: (args, done) => {
            const id = args.request$.session.passport.user.id;
            serverHelpers.isUserRoleAllowed(id, ['basic', 'user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        return promiseAct('role:surveystore, listAnswersById:surveyAnswers', {
                            query: {uid: id}
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get all surveys with answers.');
                        return Promise.resolve([]);
                    }
                })
                .then((dataArr) => {
                    return [promiseAct('role:companies, listCompaniesById:company', {
                        query: {uid: id}
                    }), dataArr];
                })
                .spread((companies, dataArr) => {
                    const companyIdArr = [];

                    for (let i = 0; i < companies.length; i++) {
                        companyIdArr.push(companies[i]._id);
                    }

                    // We want to get all the surveys that are available on the users companies.
                    return [promiseAct('role:surveystore, listAnswersByCompanyIdArr:surveyAnswers', {
                        query: {companyIdArr: companyIdArr}
                    }), dataArr];
                })
                .spread((newDataArr, dataArr) => {
                    let combinedDataArr = newDataArr.concat(dataArr);

                    // Remove duplicates
                    combinedDataArr = combinedDataArr.filter((survey, index, self) =>
                        index === self.findIndex((t) => (
                            t.name === survey.name && t.selectedCompanyId === survey.selectedCompanyId
                        ))
                    );

                    done(null, combinedDataArr);
                })
                .catch((e) => {
                    console.log('getAllSurveysWithAnswers: ', e);
                    done();
                });
        },
        saveSurveyWithAnswers: (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            serverHelpers.isUserRoleAllowed(id, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        arg.request$.body.questionnaire.progress = serverHelpers.getProgress(arg.request$.body.questionnaire);

                        promiseAct('role:companies, get:company', {id: arg.request$.body.questionnaire.selectedCompanyId})
                            .then((company) => {
                                console.log("Working on: ", company);
                                if (company && company.ratios && company.ratios.hasOwnProperty('surveyWeight') && company.ratios.hasOwnProperty('intelWeight') &&
                                    company.ratios.surveyWeight === 0 && company.ratios.intelWeight === 100) {
                                    let data = {
                                        id: arg.request$.body.questionnaire.selectedCompanyId,
                                        ratios: {
                                            surveyWeight: 50,
                                            intelWeight: 50
                                        }
                                    };
                                    promiseAct('role:companies, update:company', {
                                            id: arg.request$.body.questionnaire.selectedCompanyId, data: data
                                        }
                                    );
                                }
                            })
                            .catch((e) => {
                                console.error(e);
                            });

                        return promiseAct('role:surveystore, updateAnswers:surveyAnswers', {
                            said: arg.request$.body.questionnaire.said,
                            data: arg.request$.body.questionnaire
                        });
                    } else {
                        console.log('User ', id, ' role does not allow to get all surveys with answers.');
                        return Promise.resolve([]);
                    }
                })
                .then((data) => {
                    console.log('Getting survey Answers');
                    done(null, {said: data.said});
                })
                .catch((e) => {
                    console.log('saveSurveyWithAnswers: ', e);
                    done();
                });
        },

        saveSurveyEditedData: async (arg, done) => {
            const id = arg.request$.session.passport.user.id;
            try {
                const allowed = await serverHelpers.isUserRoleAllowed(id, ['user'], seneca);
                if (allowed) {
                    const res = await serverHelpers.updateSurveyEditedData(arg.args.body.surveyId, arg.args.body.surveyName);
                    if (res && res.ok) {
                        done(null, {ok: true});
                    } else {
                        console.error('In saveSurveyEditedData() - Failed to update survey edited data');
                    }
                } else {
                    console.error('In saveSurveyEditedData() - Error: user is not allowed');
                }
            } catch (e) {
                console.error('In saveSurveyEditedData() - Error: ', e);
            }
        },
        updateSurveyTemplateAllowedOrg: async (arg, done) => {
            try{
                const authUserId = arg.request$.session.passport.user.id;
                const userEmail = arg.request$.session.passport.user.email;
                let curUserObj = {};
                if(authUserId && userEmail){
                    curUserObj = {id: authUserId, email: userEmail};
                }
                const isAdmin = await serverHelpers.isAdminUserByID(authUserId, seneca);
                const surveyId = arg.args.body.surveyId;
                const allowedOrganizations = arg.args.body.allowedOrganizations;

                if(isAdmin && surveyId && allowedOrganizations && curUserObj && curUserObj.id && curUserObj.email){
                    const uid =  extraHelpers.getUsersFromOrgArrForSurveyTemplate(curUserObj, allowedOrganizations);
                    const res = await serverHelpers.updateSurveyTemplateAllowedOrgOnMongo(surveyId, uid, allowedOrganizations);
                    if (res && res.ok) {
                        done(null, {ok: true});
                    } else {
                        console.error('In updateSurveyTemplateAllowedOrg() - Failed to update survey template allowed users data');
                        done(null, {ok: false});
                    }
                } else {
                    console.error('In updateSurveyTemplateAllowedOrg() - Error: user is not allowed');
                    done(null, {ok: false});
                }
            } catch (e) {
                console.error('In updateSurveyTemplateAllowedOrg() - Error: ', e);
                done(null, {ok: false});
            }
        }
    };
};
