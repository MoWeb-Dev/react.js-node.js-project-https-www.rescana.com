const moment = require('moment');
const Promise = require('bluebird');
const helpers = require('./api-helpers');
const config = require('app-config');
const intelHelpers = require('../../server_modules/utils/intel-helpers');
const neoAndMongoHelpers = require('helpers');
const extraHelpers = require('./helpers.js');
const {isOnlyDigits} = require('../../api_server/api-helpers');
const {DATALEAKS_TYPE, BOTNETS_TYPE, SSL_CERTS_TYPE, GDPR_NOT_COMPLY_TYPE} = require('../../../config/consts.js');

// Imports the Google Cloud client library
const {PubSub} = require('@google-cloud/pubsub');

// Creates a client
const pubsub = new PubSub();
const companyCollectTopic = process.env['NODE_ENV'] + '-company-collect-topic';
process.env.GOOGLE_APPLICATION_CREDENTIALS = config.paths.googlePubSubCredentials;

module.exports = function (seneca) {
    const promiseAct = Promise.promisify(seneca.act, {context: seneca});
    const Unauthorized_User_Error = 'Unauthorized user.';

    const getAllCompaniesFromDB = async (id) => {
        return new Promise((resolve) => {

            const authUser = id;
            if (authUser) {
                helpers.isAdminUserByID(authUser, seneca)
                    .then((isAdmin) => {
                        if (isAdmin) {
                            promiseAct('role:companies, list:company', {
                                query: {}
                            }).then((results) => {
                                return normalizeData(results);
                            }).then((normalizedData) => {
                                // Add companies extraData from Neo4j.
                                console.log("In getAllCompaniesFromDB");
                                return intelHelpers.getCompanyExtraDataFromNeo(normalizedData);
                            }).then(async (companies) => {
                                // This part will add discoveredDomains to all companies result.
                                if (companies && Array.isArray(companies)) {
                                    const allDomains = [];
                                    companies.map((currComp) => {
                                        if (currComp && currComp.selectedDomains && Array.isArray(currComp.selectedDomains)) {
                                            currComp.selectedDomains.map((currDomain) => {
                                                if (currDomain && !allDomains.includes(currDomain)) {
                                                    allDomains.push(currDomain);
                                                }
                                            });
                                        }
                                    });
                                    if (allDomains.length > 0) {
                                        try {
                                            const discoveredDomainsRes = await neoAndMongoHelpers.getDiscoveredDomainsFindings(allDomains);
                                            if (discoveredDomainsRes && discoveredDomainsRes.data && Array.isArray(discoveredDomainsRes.data)) {
                                                discoveredDomainsRes.data.map((currDiscoveredDomain) => {
                                                    if (currDiscoveredDomain && currDiscoveredDomain.domain && currDiscoveredDomain.discoveredDomains) {
                                                        let matchingCompanies = companies.filter((c) => {
                                                            return (c && c.selectedDomains && c.selectedDomains.includes(currDiscoveredDomain.domain));
                                                        });
                                                        if (matchingCompanies && matchingCompanies.length > 0) {
                                                            matchingCompanies.map((currComp) => {
                                                                // Duplicate currDiscoveredDomain so hiding findings from one company won't hide from different company.
                                                                const currDiscoveredDomain_dup = JSON.parse(JSON.stringify(currDiscoveredDomain));
                                                                // This is a fix for deleted discoveredDomains.
                                                                if (currDiscoveredDomain_dup.hasOwnProperty('hideFrom') && currComp && currComp.id) {
                                                                    currDiscoveredDomain_dup.hideFrom.map((currFindingToHide) => {
                                                                        if (currFindingToHide && currFindingToHide.companyID && currFindingToHide.companyID === currComp.id && currFindingToHide.discoveredDomainToHide) {
                                                                            let matchingDomainToHideIndex = currDiscoveredDomain_dup.discoveredDomains.indexOf(currFindingToHide.discoveredDomainToHide);
                                                                            if (matchingDomainToHideIndex > -1) {
                                                                                currDiscoveredDomain_dup.discoveredDomains.splice(matchingDomainToHideIndex, 1);
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                                if (currComp.hasOwnProperty('discoveredDomainsData')) {
                                                                    currComp.discoveredDomainsData.push(currDiscoveredDomain_dup);
                                                                } else {
                                                                    currComp.discoveredDomainsData = [currDiscoveredDomain_dup];
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        } catch (err) {
                                            console.error('Error in getAllCompanies-getDiscoveredDomainsFindings: ', err);
                                        }
                                    }
                                }
                                resolve(companies);
                            }).catch((e) => {
                                console.log('getAllCompanies: ', e);
                                resolve(null);
                            });
                        } else {
                            resolve(null);
                        }
                    })
                    .catch(() => {
                        resolve(null);
                    });
            } else {
                resolve(null);
            }
        });
    };
    const addCompanyToDB = async (companyFields, id, user, compIdx) => {
        return new Promise(async (resolve) => {

            let data = companyFields;
            const extraData = {};

            data.createDate = moment().format();

            if (data.ratios) {
                if(!data.ratios.surveyWeight){
                    data.ratios.surveyWeight = 0;
                }
                if (!data.ratios.intelWeight) {
                    data.ratios.intelWeight = 0;
                }
            } else {
                data.ratios = {intelWeight: 100, surveyWeight:0};
            }

            console.log('In addCompany - admin mode');
            if (user.isUserAllowedToAddVendors || user.userRole === "admin") {
                console.log('In addCompany Starting to save companyData');
                try {
                    //if user edit company we dont want him to chang the current allowed users
                    if (extraData.isInUserEditMode) {
                        data.allowedUsers = undefined;
                        data.allowedEditUsers = undefined;
                    } else if (user.userRole !== "admin") {
                        //on user vendor creation we want to add admin users to be allowed users
                        const adminUsers = await helpers.getAdminUsers();
                        if (adminUsers && Array.isArray(adminUsers)) {
                            const adminUsersIdAndEmailArr = await extraHelpers.getAdminUsersPartialInfo(adminUsers);
                            let allowedUsersArr = data.allowedUsers.concat(adminUsersIdAndEmailArr);
                            data.allowedUsers = allowedUsersArr;
                            data.allowedEditUsers = allowedUsersArr;
                        }
                    }
                    //on user vendor creation we want all created projects to be added to the current organization
                    //and the companies of the projects to connect to the organization on neo
                    if (user.userRole !== "admin" && extraData.createdProjects &&
                        data.chosenProj &&
                        data.chosenOrg &&
                        data.chosenOrg.id !== "" &&
                        data.chosenOrg.organizationName !== "") {
                        let isAdded = await helpers.addNewCreatedProjectsToOrg(extraData.createdProjects, data.chosenProj, data.chosenOrg);
                        if (isAdded && isAdded.ok) {
                            console.log('In addCompany - addProjectsToOrg() all created projects was added to current organization');
                        } else {
                            console.log('In addCompany - addProjectsToOrg() failed to add created projects to current organization');
                        }
                    }
                    let res = await promiseAct('role:intelQuery,add:company', {data: data}, extraData);
                    if (res && res.ok && res.newCompany) {
                        if (data.chosenOrg && data.chosenOrg.id !== '' && data.chosenProj) {
                            try {
                                await helpers.OnUserCompanyCreateMergeOrgAndCompOnNeo(
                                    data.chosenOrg._id,
                                    data.chosenOrg.organizationName,
                                    data.companyName,
                                    res.newCompany.id);
                                console.log('In addCompany - Successfully merge organization and related companies on GraphDB');
                            } catch (e) {
                                console.error('Error in addCompany: ', e);
                            }
                        }
                        if (data.chosenProj && data.chosenProj._id) {
                            await helpers.addNewUserCreatedCompanyToProject(data.chosenProj._id, res.newCompany);
                            console.log('In addCompany - Successfully added new company to project on mongo');
                        }
                        // send notification to admins when user add new vendor
                        if (res.newCompany && extraData.createdProjects &&
                            extraData.isInUserEditMode !== null && !extraData.isInUserEditMode &&
                            data && data.chosenOrg && data.chosenProj) {
                            let sendEmailData = {};
                            sendEmailData.newProjectsCreatedByUser = extraData.createdProjects;
                            sendEmailData.userWhoCreateNewCompany = user;
                            sendEmailData.chosenOrg = data.chosenOrg;
                            sendEmailData.chosenProj = data.chosenProj;
                            sendEmailData.newCompanyData = res.newCompany;
                            const result = await helpers.sendNotificationToAdmin(sendEmailData, promiseAct);
                            if (result && result.ok) {
                                console.log('In addCompany - sendNotificationToAdmin() - Notification sent to admins.');
                            } else {
                                console.log('In addCompany - sendNotificationToAdmin() - Failed  to  sent Notification to admins.');
                            }
                        }
                        console.log('company Added, name: ' + res.newCompany.companyName + ' - ' + compIdx);
                        resolve(res.newCompany);
                    } else {
                        console.error('Error in addCompany - No result has returned');
                        resolve(null);
                    }
                } catch (e) {
                    console.error('Error in addCompany with user ', id, ': ', e);
                    resolve(null);
                }
            } else {
                console.error('User unauthorized to add company!');
                resolve(null);
            }
        });
    };


    return {
        getAllOrganizations: async (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                try {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        const res = await helpers.getAllFromMongoByCollectionName('organizations_map');
                        cb(null, {ok: true, organizations: res});
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    cb(null, {ok: false, error: e});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        getAllOrganizationsWithPagination: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        if (args && args.request$ && args.request$.query && args.request$.query.sliceStartIndex && args.request$.query.sliceMaxLength
                            && isOnlyDigits(args.request$.query.sliceStartIndex) && isOnlyDigits(args.request$.query.sliceMaxLength)) {

                            const searchQuery = (args.request$.query.searchQuery != null && typeof args.request$.query.searchQuery === 'string') ? {organizationName: args.request$.query.searchQuery} : null;

                            const sortMethod = (args.request$.query.sortMethod && typeof args.request$.query.sortMethod === 'string') ? args.request$.query.sortMethod : null;

                            const organizationsData = await helpers.getDBDataWithPagination('organizations_map', Number(args.request$.query.sliceStartIndex), Number(args.request$.query.sliceMaxLength), searchQuery, sortMethod);

                            if (organizationsData && organizationsData.data && Array.isArray(organizationsData.data) && organizationsData.totalDataCount != null) {

                                cb(null, {
                                    ok: true,
                                    organizations: organizationsData.data,
                                    totalOrganizationsCount: organizationsData.totalDataCount
                                });
                            } else {
                                cb(null, {ok: false, error: 'No Organizations were found'});
                            }
                        } else {
                            cb(null, {
                                ok: false,
                                error: 'Some parameters are missing in getAllOrganizationsWithPagination'
                            });
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } catch (e) {
                console.error('Error in getAllOrganizationsWithPagination: ', e);
                cb(null, {ok: false, error: e});
            }
        },
        getAllOrganizationsForDD: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        const organizationFieldsToReturn = ['organizationName'];

                        const organizationsData = await helpers.getDBDataWithSpecificFields('organizations_map', organizationFieldsToReturn);

                        if (organizationsData && organizationsData.data && Array.isArray(organizationsData.data)) {

                            cb(null, {ok: true, organizationsForDD: organizationsData.data});
                        } else {
                            cb(null, {ok: false, error: 'No Organizations were found'});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } catch (e) {
                console.error('Error in getAllOrganizationsForDD: ', e);
                cb(null, {ok: false, error: e});
            }
        },
        getAllProjectsForDD: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        const projectFieldsToReturn = ['projectName'];

                        const projectsData = await helpers.getDBDataWithSpecificFields('project_map', projectFieldsToReturn);

                        if (projectsData && projectsData.data && Array.isArray(projectsData.data)) {

                            // This fixes the id property.
                            projectsData.data.map((currProj) => {
                                if (currProj) {
                                    if (currProj._id) {
                                        currProj.id = currProj._id;
                                        delete currProj._id;
                                    }
                                }
                            });

                            cb(null, {ok: true, projectsForDD: projectsData.data});
                        } else {
                            cb(null, {ok: false, error: 'No Projects were found'});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } catch (e) {
                console.error('Error in getAllProjectsForDD: ', e);
                cb(null, {ok: false, error: e});
            }
        },
        getAllCompaniesForDD: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        const companyFieldsToReturn = ['companyName'];

                        const companiesData = await helpers.getDBDataWithSpecificFields('company_map', companyFieldsToReturn);

                        if (companiesData && companiesData.data && Array.isArray(companiesData.data)) {

                            // This fixes the id property.
                            companiesData.data.map((currComp) => {
                                if (currComp) {
                                    if (currComp._id) {
                                        currComp.id = currComp._id;
                                        delete currComp._id;
                                    }
                                }
                            });

                            cb(null, {ok: true, companiesForDD: companiesData.data});
                        } else {
                            cb(null, {ok: false, error: 'No Companies were found'});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } catch (e) {
                console.error('Error in getAllCompaniesForDD: ', e);
                cb(null, {ok: false, error: e});
            }
        },
        getAllSurveyTemplatesForDD: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        const surveyTemplatesFieldsToReturn = ['name', 'sid', 'createDate'];

                        const surveyTemplatesData = await helpers.getDBDataWithSpecificFields('survey_map', surveyTemplatesFieldsToReturn);

                        if (surveyTemplatesData && surveyTemplatesData.data && Array.isArray(surveyTemplatesData.data)) {

                            cb(null, {ok: true, surveyTemplatesForDD: surveyTemplatesData.data});
                        } else {
                            cb(null, {ok: false, error: 'No Survey Templates were found'});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } catch (e) {
                console.error('Error in getAllSurveyTemplatesForDD: ', e);
                cb(null, {ok: false, error: e});
            }
        },
        getAllApiKeys: async (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                try {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        const res = await helpers.getAllApiKeysFromDB();
                        cb(null, {ok: true, apiKeys: res});
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    cb(null, {ok: false, error: e});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        getOrgById: async (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                try {
                    const res = await helpers.getOrgById(args.request$.body.orgId);
                    if (res) {
                        const orgArrForUserInputWithoutAdminUsers = await helpers.eraseAdminUsersFromOrgAllowedUsers(res);
                        console.log("In getOrgById - got organization users without admin users");
                        cb(null, {ok: true, org: orgArrForUserInputWithoutAdminUsers});
                    } else {
                        console.log("In getOrgById - Failed to get organization users without admin users");
                        cb(null, {ok: false});
                    }
                } catch (e) {
                    console.log("In getOrgById - Error: " + e);
                    cb(null, {ok: false});
                }
            } else {
                console.log("In getOrgById - Error: Unauthorized User");
                cb(null, {ok: false});
            }
        },
        getAllCompanies: (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                helpers.isAdminUserByID(authUser, seneca)
                    .then((isAdmin) => {
                        if (isAdmin) {
                            promiseAct('role:companies, list:company', {
                                query: {}
                            }).then((results) => {
                                return normalizeData(results);
                            }).then((normalizedData) => {
                                // Add companies extraData from Neo4j.
                                console.log("In getAllCompanies");
                                return intelHelpers.getCompanyExtraDataFromNeo(normalizedData);
                            }).then(async (companies) => {
                                // This part will add discoveredDomains to all companies result.
                                if (companies && Array.isArray(companies)) {
                                    const allDomains = [];
                                    companies.map((currComp) => {
                                        if (currComp && currComp.selectedDomains && Array.isArray(currComp.selectedDomains)) {
                                            currComp.selectedDomains.map((currDomain) => {
                                                if (currDomain && !allDomains.includes(currDomain)) {
                                                    allDomains.push(currDomain);
                                                }
                                            });
                                        }
                                    });
                                    if (allDomains.length > 0) {
                                        try {
                                            const discoveredDomainsRes = await neoAndMongoHelpers.getDiscoveredDomainsFindings(allDomains);
                                            if (discoveredDomainsRes && discoveredDomainsRes.data && Array.isArray(discoveredDomainsRes.data)) {
                                                discoveredDomainsRes.data.map((currDiscoveredDomain) => {
                                                    if (currDiscoveredDomain && currDiscoveredDomain.domain && currDiscoveredDomain.discoveredDomains) {
                                                        let matchingCompanies = companies.filter((c) => {
                                                            return (c && c.selectedDomains && c.selectedDomains.includes(currDiscoveredDomain.domain));
                                                        });
                                                        if (matchingCompanies && matchingCompanies.length > 0) {
                                                            matchingCompanies.map((currComp) => {
                                                                // Duplicate currDiscoveredDomain so hiding findings from one company won't hide from different company.
                                                                const currDiscoveredDomain_dup = JSON.parse(JSON.stringify(currDiscoveredDomain));
                                                                // This is a fix for deleted discoveredDomains.
                                                                if (currDiscoveredDomain_dup.hasOwnProperty('hideFrom') && currComp && currComp.id) {
                                                                    currDiscoveredDomain_dup.hideFrom.map((currFindingToHide) => {
                                                                        if (currFindingToHide && currFindingToHide.companyID && currFindingToHide.companyID === currComp.id && currFindingToHide.discoveredDomainToHide) {
                                                                            let matchingDomainToHideIndex = currDiscoveredDomain_dup.discoveredDomains.indexOf(currFindingToHide.discoveredDomainToHide);
                                                                            if (matchingDomainToHideIndex > -1) {
                                                                                currDiscoveredDomain_dup.discoveredDomains.splice(matchingDomainToHideIndex, 1);
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                                if (currComp.hasOwnProperty('discoveredDomainsData')) {
                                                                    currComp.discoveredDomainsData.push(currDiscoveredDomain_dup);
                                                                } else {
                                                                    currComp.discoveredDomainsData = [currDiscoveredDomain_dup];
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        } catch (err) {
                                            console.error('Error in getAllCompanies-getDiscoveredDomainsFindings: ', err);
                                        }
                                    }
                                }

                                cb(null, {ok: true, companies: companies});
                            }).catch((e) => {
                                console.log('getAllCompanies: ', e);
                                cb();
                            });
                        } else {
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        }
                    })
                    .catch(() => {
                        cb();
                    });
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        getAllCompaniesWithPagination: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        if (args && args.request$ && args.request$.query && args.request$.query.sliceStartIndex && args.request$.query.sliceMaxLength
                            && isOnlyDigits(args.request$.query.sliceStartIndex) && isOnlyDigits(args.request$.query.sliceMaxLength)) {

                            const searchQuery = (args.request$.query.searchQuery != null && typeof args.request$.query.searchQuery === 'string') ? {companyName: args.request$.query.searchQuery} : null;

                            const sortMethod = (args.request$.query.sortMethod && typeof args.request$.query.sortMethod === 'string') ? args.request$.query.sortMethod : null;

                            const companiesData = await helpers.getDBDataWithPagination('company_map', Number(args.request$.query.sliceStartIndex), Number(args.request$.query.sliceMaxLength), searchQuery, sortMethod);

                            if (companiesData && companiesData.data && Array.isArray(companiesData.data) && companiesData.totalDataCount != null) {

                                const normalizedData = await normalizeData(companiesData.data);

                                console.log("In getAllCompaniesWithPagination");
                                // Add companies extraData from Neo4j.
                                const companies = await intelHelpers.getCompanyExtraDataFromNeo(normalizedData);

                                // This part will add discoveredDomains to all companies result.
                                if (companies && Array.isArray(companies)) {
                                    const allDomains = [];
                                    companies.map((currComp) => {
                                        if (currComp && currComp.selectedDomains && Array.isArray(currComp.selectedDomains)) {
                                            currComp.selectedDomains.map((currDomain) => {
                                                if (currDomain && !allDomains.includes(currDomain)) {
                                                    allDomains.push(currDomain);
                                                }
                                            });
                                        }
                                    });
                                    if (allDomains.length > 0) {
                                        const discoveredDomainsRes = await neoAndMongoHelpers.getDiscoveredDomainsFindings(allDomains);
                                        if (discoveredDomainsRes && discoveredDomainsRes.data && Array.isArray(discoveredDomainsRes.data)) {
                                            discoveredDomainsRes.data.map((currDiscoveredDomain) => {
                                                if (currDiscoveredDomain && currDiscoveredDomain.domain && currDiscoveredDomain.discoveredDomains) {
                                                    let matchingCompanies = companies.filter((c) => {
                                                        return (c && c.selectedDomains && c.selectedDomains.includes(currDiscoveredDomain.domain));
                                                    });
                                                    if (matchingCompanies && matchingCompanies.length > 0) {
                                                        matchingCompanies.map((currComp) => {
                                                            // Duplicate currDiscoveredDomain so hiding findings from one company won't hide from different company.
                                                            const currDiscoveredDomain_dup = JSON.parse(JSON.stringify(currDiscoveredDomain));
                                                            // This is a fix for deleted discoveredDomains.
                                                            if (currDiscoveredDomain_dup.hasOwnProperty('hideFrom') && currComp && currComp.id) {
                                                                currDiscoveredDomain_dup.hideFrom.map((currFindingToHide) => {
                                                                    if (currFindingToHide && currFindingToHide.companyID && currFindingToHide.companyID === currComp.id && currFindingToHide.discoveredDomainToHide) {
                                                                        let matchingDomainToHideIndex = currDiscoveredDomain_dup.discoveredDomains.indexOf(currFindingToHide.discoveredDomainToHide);
                                                                        if (matchingDomainToHideIndex > -1) {
                                                                            currDiscoveredDomain_dup.discoveredDomains.splice(matchingDomainToHideIndex, 1);
                                                                        }
                                                                    }
                                                                });
                                                            }

                                                            if (currComp.hasOwnProperty('discoveredDomainsData')) {
                                                                currComp.discoveredDomainsData.push(currDiscoveredDomain_dup);
                                                            } else {
                                                                currComp.discoveredDomainsData = [currDiscoveredDomain_dup];
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }

                                cb(null, {
                                    ok: true,
                                    companies: companies,
                                    totalCompaniesCount: companiesData.totalDataCount
                                });
                            }
                        } else {
                            cb(null, {
                                ok: false,
                                error: 'Some parameters are missing in getAllCompaniesWithPagination'
                            });
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } catch (e) {
                console.error('Error in getAllCompaniesWithPagination: ', e);
                cb(null, {ok: false, error: e});
            }
        },

        getAllProjects: (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                helpers.isAdminUserByID(authUser, seneca)
                    .then((isAdmin) => {
                        if (isAdmin) {
                            promiseAct('role:projects, list:project', {
                                query: {}
                            }).then((results) => {
                                return normalizeData(results);
                            }).then((normalizedData) => {
                                cb(null, {ok: true, projects: normalizedData});
                            }).catch((e) => {
                                console.log('getAllProjects: ', e);
                                cb();
                            });
                        } else {
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        }
                    })
                    .catch(() => {
                        cb();
                    });
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },
        getDiscoveredDomains : async (args, cb) => {
            try{
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        console.log("Getting all discovered domains related to company.");
                        let discoveredDomains = await intelHelpers.getDiscoveredDomains(args.request$.query.cid);
                        cb(null, {ok: true, discoveredDomains: discoveredDomains});
                    }
                }
            }catch(e){
                console.error('Error in getDiscoveredDomains: ', e);
                cb(null, {ok: false, error: e});
            }
        },
        validateDomain : async (args, cb) => {
            try{
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        console.log("Validating domains related to company.");
                        let discoveredDomains = await intelHelpers.updateDomainConnectionStrengthAndValidationDate(args.request$.body.domain, args.request$.body.strength);
                        cb(null, {ok: true, discoveredDomains: discoveredDomains});
                    }
                }
            }catch(e){
                console.error('Error in getDiscoveredDomains: ', e);
                cb(null, {ok: false, error: e});
            }
        },
        getAllProjectsWithPagination: async (args, cb) => {
            try {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        if (args && args.request$ && args.request$.query && args.request$.query.sliceStartIndex && args.request$.query.sliceMaxLength
                            && isOnlyDigits(args.request$.query.sliceStartIndex) && isOnlyDigits(args.request$.query.sliceMaxLength)) {

                            const searchQuery = (args.request$.query.searchQuery != null && typeof args.request$.query.searchQuery === 'string') ? {projectName: args.request$.query.searchQuery} : null;

                            const sortMethod = (args.request$.query.sortMethod && typeof args.request$.query.sortMethod === 'string') ? args.request$.query.sortMethod : null;

                            const projectsData = await helpers.getDBDataWithPagination('project_map', Number(args.request$.query.sliceStartIndex), Number(args.request$.query.sliceMaxLength), searchQuery, sortMethod);

                            if (projectsData && projectsData.data && Array.isArray(projectsData.data) && projectsData.totalDataCount != null) {

                                const normalizedData = await normalizeData(projectsData.data);

                                cb(null, {
                                    ok: true,
                                    projects: normalizedData,
                                    totalProjectsCount: projectsData.totalDataCount
                                });
                            }
                        } else {
                            cb(null, {
                                ok: false,
                                error: 'Some parameters are missing in getAllProjectsWithPagination'
                            });
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } catch (e) {
                console.error('Error in getAllProjectsWithPagination: ', e);
                cb(null, {ok: false, error: e});
            }
        },

        getAllTokens: (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                helpers.isAdminUserByID(authUser, seneca)
                    .then((isAdmin) => {
                        if (isAdmin) {
                            intelHelpers.getAllTokens()
                                .then((results) => {
                                    cb(null, {ok: true, tokens: results});
                                }).catch((e) => {
                                console.log('getAllTokens: ', e);
                                cb();
                            });
                        } else {
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        }
                    })
                    .catch(() => {
                        cb();
                    });
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        runCompanyQuery: async (args, cb) => {
            let authUser = args.request$.session.passport.user.id;
            if (authUser) {
                try {
                    let isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        const userData = {
                            uEmail: args.request$.session.passport.user.email,
                            uName: args.request$.session.passport.user.name,
                            id: args.request$.body.id
                        };
                        console.log('In runCompanyQuery');
                        try {
                            const dataBuffer = Buffer.from(JSON.stringify(userData));
                            const messageId = await pubsub.topic(companyCollectTopic).publish(dataBuffer);
                            console.log(`Message ${messageId} published.`);
                            console.log('Company was added to manual scan queue - will email when finished');
                            cb(null, {ok: true});
                        } catch (e) {
                            console.log('Error in runCompanyQuery: ', e);
                            cb(null, {ok: false, error: e});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    console.log('runCompanyQuery failed to check authorization: ', e);
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        runDiscoveryQuery: async (args, cb) => {
            let authUser = args.request$.session.passport.user.id;
            if (authUser) {
                try {
                    let isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        console.log('In runDiscoveryQuery');
                        try {
                            await promiseAct('role:domain-finder,cmd:find-domains', {
                                uEmail: args.request$.session.passport.user.email,
                                uName: args.request$.session.passport.user.name,
                                id: args.request$.body.id
                            });
                            await promiseAct('role:domain-validator,cmd:validate-domains', {
                                uEmail: args.request$.session.passport.user.email,
                                uName: args.request$.session.passport.user.name,
                                id: args.request$.body.id
                            });

                            console.log('Running discovery');
                            cb(null, {ok: true});
                        } catch (e) {
                            console.log('Error in runDiscoveryQuery: ', e);
                            cb(null, {ok: false, error: e});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    console.log('runCompanyQuery failed to check authorization: ', e);
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        runProjectQuery: async (args, cb) => {
            const authUser = args.request$.session.passport.user.id;
            let isAdmin;
            let project;

            if (authUser) {
                try {
                    isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                } catch (e) {
                    console.log('runProjectQuery failed to check authorization: ', e);

                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }

                if (isAdmin) {
                    console.log('In runProjectQuery');
                    try {
                        project = await promiseAct('role:projects, get:project', {id: args.request$.body.id});

                        const companies = project.selectedCompanies;
                        let userData = {
                            uEmail: args.request$.session.passport.user.email,
                            uName: args.request$.session.passport.user.name
                        };

                        //Get all domains of all companies in project.
                        companies.forEach((company) => {
                            userData.id = company.id;
                            const dataBuffer = Buffer.from(JSON.stringify(userData));
                            pubsub.topic(companyCollectTopic).publish(dataBuffer);
                        });

                        console.log('Project was added to manual scan queue - will email when finished');
                        cb(null, {ok: true});

                    } catch (e) {
                        console.log('Error in runProjectQuery: ', e);
                        cb(null, {ok: false, error: e});
                    }
                    cb(null, {ok: true});
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        addProject: (args, cb) => {
            const authUser = args.request$.body.uid;
            if (authUser) {
                const user = args.request$.session.passport.user;
                if (user && (user.isUserAllowedToAddVendors || user.userRole === "admin")) {
                    seneca.act('role:projects, update:project', {data: args.request$.body.data}, (err, res) => {
                        if (err) {
                            cb(null, {ok: false, error: err});
                        }
                        // Need to return the added query so we can add it to the table.
                        seneca.act('role:projects, get:project', {id: res.id}, async (err, project) => {
                            let passedNeoMerge;
                            if (err) {
                                cb(null, {ok: false, error: err});
                            }
                            try {
                                const allOrg = await helpers.getAllFromMongoByCollectionName('organizations_map');
                                if (allOrg) {
                                    const allComp = await promiseAct('role:companies, list:company', {query: {}});
                                    if (allComp) {
                                        try {
                                            if (!args.request$.body.extraData.detachArrayToRemoveFromNeo) {
                                                args.request$.body.extraData.detachArrayToRemoveFromNeo = [];
                                            }
                                            if (!args.request$.body.extraData.detachArrayToAddToNeo) {
                                                args.request$.body.extraData.detachArrayToAddToNeo = [];
                                            }
                                            passedNeoMerge = await helpers.updateOrgAndCompaniesOnProjUpdateOnNeo(
                                                project,
                                                allOrg,
                                                allComp,
                                                args.request$.body.extraData.detachArrayToRemoveFromNeo,
                                                args.request$.body.extraData.detachArrayToAddToNeo
                                                , this);
                                        } catch (e) {
                                            console.error('Error in addProject - admin mode: ', e);
                                        }

                                        if (passedNeoMerge) {
                                            if (passedNeoMerge.ok) {
                                                console.log('In addProject - admin mode - Successfully merge organization and related companies on GraphDB');
                                            }
                                            // There is a third case of empty result where there is no companyInfo to save, so there is no need to write any additional log.
                                            else if (passedNeoMerge.hasOwnProperty('error')) {
                                                console.log('In addProject - admin mode - Failed to merge organization and related companies on GraphDB');
                                            }
                                        }
                                    } else {
                                        console.error('In addProject - didint manage to get all companies');
                                        cb(null, {ok: false});

                                    }
                                } else {
                                    console.error('In addProject - didnt manage to get all organizations');
                                    cb(null, {ok: false});

                                }
                            } catch (e) {
                                console.error('In addProject - error:' + e);
                                cb(null, {ok: false, error: e});
                            }
                            cb(null, {ok: true, newProject: project.data$(false)});
                        });
                    });
                } else {
                    console.error('Error In addProject: unauthorized user');
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },


        addApiKey: async (args, cb) => {
            console.log('In addApiKey');
            const authUser = args.request$.session.passport.user.id;
            let isAdmin;
            if (authUser) {
                try {
                    isAdmin = await helpers.isAdminUserByID(authUser, seneca);

                    if (isAdmin) {
                        const result = await helpers.saveApiKeyToDB(args.request$.body);

                        if (result && result.ok && result.data) {
                            const res = {ok: true, data: result.data};
                            if (result.originalApiKey) {
                                res.originalApiKey = result.originalApiKey;
                            }
                            cb(null, res);
                        } else {
                            cb(null, {ok: false});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    console.error('Error in addApiKey: ', e);
                    cb(null, {ok: false, error: e});
                }

            } else {
                console.log('In addApiKey - user ', args.request$.session.passport.user, ' is not allowed to proceed');

                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },


        editApiKey: async (args, cb) => {
            console.log('In editApiKey');
            const authUser = args.request$.session.passport.user.id;
            let isAdmin;
            if (authUser) {
                try {
                    isAdmin = await helpers.isAdminUserByID(authUser, seneca);

                    if (isAdmin) {
                        // isEditMode parameter is set to true.
                        const result = await helpers.saveApiKeyToDB(args.request$.body, true);

                        if (result && result.ok && result.data) {
                            cb(null, {ok: true, data: result.data});
                        } else {
                            cb(null, {ok: false});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    console.error('Error in editApiKey: ', e);
                    cb(null, {ok: false, error: e});
                }

            } else {
                console.log('In editApiKey - user ', args.request$.session.passport.user, ' is not allowed to proceed');

                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },


        editApiKeyStatus: async (args, cb) => {
            console.log('In editApiKeyStatus');
            const authUser = args.request$.session.passport.user.id;
            let isAdmin;
            if (authUser) {
                try {
                    isAdmin = await helpers.isAdminUserByID(authUser, seneca);

                    if (isAdmin) {
                        const result = await helpers.editApiKeyStatusInDB(args.request$.body);

                        if (result && result.ok) {
                            cb(null, {ok: true});
                        } else {
                            cb(null, {ok: false});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    console.error('Error in editApiKey: ', e);
                    cb(null, {ok: false, error: e});
                }

            } else {
                console.log('In editApiKey - user ', args.request$.session.passport.user, ' is not allowed to proceed');

                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },


        deleteApiKey: async (args, cb) => {
            console.log('In deleteApiKey');
            const authUser = args.request$.session.passport.user.id;
            let isAdmin;
            if (authUser) {
                try {
                    isAdmin = await helpers.isAdminUserByID(authUser, seneca);

                    if (isAdmin) {
                        const result = await helpers.deleteApiKeyFromDB(args.request$.body);

                        if (result && result.ok) {
                            cb(null, {ok: true});
                        } else {
                            cb(null, {ok: false});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    console.error('Error in deleteApiKey: ', e);
                    cb(null, {ok: false, error: e});
                }

            } else {
                console.log('In deleteApiKey - user ', args.request$.session.passport.user, ' is not allowed to proceed');

                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        addOrganization: async (args, cb) => {
            console.log('In addOrganization');
            const authUser = args.request$.body.uid;
            let isAdmin;
            if (authUser) {
                try {
                    isAdmin = await helpers.isAdminUserByID(authUser, seneca);

                } catch (e) {
                    cb(null, {ok: false, error: e});
                }

                if (isAdmin) {
                    try {
                        const lastModifiedOrg = await helpers.saveOrganizationToDB(args.request$.body.orgData);
                        let allPro = [];
                        let allComp = [];
                        try {
                            allPro = await helpers.getAllFromMongoByCollectionName('project_map');
                            if (allPro) {
                                allComp = await promiseAct('role:companies, list:company', {query: {}});
                                if (allComp) {
                                    const passedNeoMerge = await helpers.mergeOrgAndRelatedCompaniesOnNeo(
                                        {
                                            id: lastModifiedOrg._id,
                                            organizationName: lastModifiedOrg.organizationName,
                                            selectedProjects: lastModifiedOrg.selectedProjects
                                        },
                                        allPro,
                                        allComp,
                                        false
                                        , this);

                                    if (passedNeoMerge) {
                                        if (passedNeoMerge.ok) {
                                            console.log('In addOrganization - admin mode - Successfully merge organization and related companies on GraphDB');
                                        }
                                        // There is a third case of empty result where there is no companyInfo to save, so there is no need to write any additional log.
                                        else if (passedNeoMerge.hasOwnProperty('error')) {
                                            console.log('In addOrganization - admin mode - Failed to merge organization and related companies on GraphDB');
                                        }
                                    }
                                } else {
                                    console.error('Error in addOrganization - didnt manage to fetch all companies');
                                }
                            } else {
                                console.error('Error in addOrganization - didnt manage to fetch all projects');
                            }
                        } catch (e) {
                            console.error('Error in addOrganization - admin mode: ', e);
                        }

                        try {
                            if (args.request$.body.orgData.projToDeleteFromNeo && args.request$.body.orgData.projToDeleteFromNeo.length > 0) {
                                await helpers.detachOrgFromCompaniesOnNeo(
                                    args.request$.body.orgData.projToDeleteFromNeo,
                                    lastModifiedOrg,
                                    allPro,
                                    allComp,
                                    true);
                            }
                        } catch (e) {
                            console.error('Error in addOrganization - admin mode - Detach Nodes on Neo Graph: ', e);
                        }

                        cb(null, {ok: true, newOrganization: lastModifiedOrg});

                    } catch (e) {
                        cb(null, {ok: false, error: e});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }

            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        deleteOrganization: async (args, cb) => {
            const user = args.request$.session.passport.user;
            if (user && (user.isUserAllowedToAddVendors || user.userRole === "admin")) {
                try {
                    await helpers.deleteOrganizationFromDB(args.request$.body.id);
                } catch (e) {
                    cb(null, {ok: false});
                }
                try {
                    const data = {
                        label: 'organization',
                        relationship: 'owner',
                        prop: 'oid',
                        propStr: args.request$.body.id
                    };
                    await intelHelpers.detachNodes(data);
                } catch (e) {
                    cb(null, {ok: false});
                }
                cb(null, {ok: true});
            } else {
                console.error('Error In deleteOrganization: unauthorized user');
                cb(null, {ok: false});
            }
        },

        removeAllowedUsersFromOrgProjects: async (args, cb) => {
            const authUser = args.request$.body.uid;
            if (authUser) {
                try {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        try {
                            const allPro = await helpers.getAllFromMongoByCollectionName('project_map');
                            if (allPro) {
                                const allComp = await promiseAct('role:companies, list:company', {query: {}});
                                if (allComp) {
                                    await helpers.removeAllowedUsersFromOrgProjects(
                                        args.request$.body.orgData,
                                        allPro,
                                        allComp,
                                        args.request$.body.userIdToRemove);
                                } else {
                                    console.error('In removeAllowedUsersFromOrgProjects - failed to get all companies ');
                                    cb(null, {ok: false});
                                }
                            } else {
                                console.error('In removeAllowedUsersFromOrgProjects - failed to get all projects');
                                cb(null, {ok: false});
                            }
                        } catch (e) {
                            cb(null, {ok: false, error: e});
                        }
                        cb(null, {ok: true});
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    cb(null, {ok: false, error: e});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        addAllowedUsersToOrgProjects: async (args, cb) => {
            const authUser = args.request$.body.uid;
            if (authUser) {
                try {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        try {
                            const allPro = await helpers.getAllFromMongoByCollectionName('project_map');
                            if (allPro) {
                                const allComp = await promiseAct('role:companies, list:company', {query: {}});
                                if (allComp) {
                                    await helpers.addAllowedUsersToOrgProjects(
                                        args.request$.body.orgData,
                                        allPro,
                                        allComp,
                                        args.request$.body.userToAdd);
                                } else {
                                    console.error('In addAllowedUsersToOrgProjects - failed to get all companies ');
                                    cb(null, {ok: false});
                                }
                            } else {
                                console.error('In addAllowedUsersToOrgProjects - failed to get all projects');
                                cb(null, {ok: false});
                            }
                        } catch (e) {
                            cb(null, {ok: false, error: e});
                        }
                        cb(null, {ok: true});
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    cb(null, {ok: false, error: e});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        addAllowedUsersToProjCompanies: async (args, cb) => {
            const authUser = args.request$.body.uid;
            if (authUser) {
                try {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        try {
                            const allComp = await promiseAct('role:companies, list:company', {query: {}});
                            if (allComp) {
                                let companiesToUpdateFullDataArray = extraHelpers.getFullSelectedObjectsInfoById(allComp, args.request$.body.projData.selectedCompanies);
                                await helpers.addAllowedUsersToCompanies(
                                    companiesToUpdateFullDataArray,
                                    args.request$.body.userToAdd);
                            } else {
                                cb(null, {ok: false, error: "Could not get companies data"});
                            }
                        } catch (e) {
                            cb(null, {ok: false, error: e});
                        }
                        cb(null, {ok: true});
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    cb(null, {ok: false, error: e});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        removeAllowedUsersFromProjCompanies: async (args, cb) => {
            const authUser = args.request$.body.uid;
            if (authUser) {
                try {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        try {
                            await helpers.removeAllowedUsersFromCompanies(
                                args.request$.body.projData.selectedCompanies,
                                args.request$.body.userToRemove);
                        } catch (e) {
                            cb(null, {ok: false, error: e});
                        }
                        cb(null, {ok: true});
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } catch (e) {
                    cb(null, {ok: false, error: e});
                }
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        createNewProjectByUser: async (args, cb) => {
            const user = args.request$.session.passport.user;
            if (user && (user.isUserAllowedToAddVendors || user.userRole === "admin")) {
                try {
                    const adminUsers = await helpers.getAdminUsers();
                    if (adminUsers && Array.isArray(adminUsers)) {
                        const adminUsersIdAndEmailArr = await extraHelpers.getAdminUsersPartialInfo(adminUsers);
                        args.request$.body.data.allowedUsers = args.request$.body.data.allowedUsers.concat(adminUsersIdAndEmailArr);
                    }
                } catch (e) {
                    console.error('In createNewProjectByUser() - Error: ' + e)
                }
                seneca.act('role:projects, update:project', {data: args.request$.body.data}, (err, res) => {
                    if (err) {
                        cb(null, {ok: false});
                    }
                    // Need to return the added query so we can add it to the table.
                    seneca.act('role:projects, get:project', {id: res.id}, async (err, project) => {
                        if (err) {
                            cb(null, {ok: false});

                        }
                        try {
                            if (args.request$.body.curOrg) {
                                const org = args.request$.body.curOrg;
                                if (org && org[0] && (org[0].id || org[0]._id)) {
                                    if (args.request$.body.data.selectedCompanies && args.request$.body.data.selectedCompanies.length > 0) {
                                        const passedNeoMerge = await helpers.mergeOrgAndRelatedCompaniesOnNeo(
                                            org[0],
                                            [],
                                            args.request$.body.data.selectedCompanies,
                                            true);
                                        if (!passedNeoMerge) {
                                            console.error('Error In createNewProjectByUser: failed to merge org and related companies on neo');
                                            cb(null, {ok: false});
                                        }
                                    }
                                    await helpers.addProjectsToOrgOnMongo(
                                        org[0],
                                        project.data$(false));
                                }
                            }
                        } catch (e) {
                            console.error('Error In createNewProjectByUser: ', e);
                            cb(null, {ok: false});
                        }

                        cb(null, {newProject: project.data$(false)});
                    });
                });
            } else {
                console.error('Error In createNewProjectByUser: unauthorized user');
                cb(null, {ok: false});
            }
        },

        editCompanyByUser: (args, cb) => {
            console.log('In editCompanyByUser');
            const uid = args.request$.session.passport.user.id;
            return helpers.isUserRoleAllowed(uid, ['user'], seneca)
                .then((allowed) => {
                    if (allowed) {
                        console.log('In editCompanyByUser - User ', uid, ' is allowed to proceed');
                        return helpers.checkWritePermissions(uid, args.request$.body.cid, args.request$.body.data, seneca);
                    } else {
                        console.log('In editCompanyByUser - User ', uid, ' not allowed to proceed');
                        return Promise.resolve();
                    }
                })
                .catch((e) => {
                    console.error('Error in editCompanyByUser-isUserRoleAllowed for user ', uid, ': ', e);
                    cb(null, {ok: false, data: {error: 'Failed to edit company'}});
                })
                .then((permitted) => {
                    if (permitted) {
                        console.log('In editCompanyByUser - User ', uid, ' is permitted to edit this company');

                        return helpers.updateCompanyNameOnNeo(args.request$.body.cid, args.request$.body.data.companyName)
                            .then((passedNeoCompanyName) => {
                                if (passedNeoCompanyName) {
                                    if (Array.isArray(passedNeoCompanyName) && passedNeoCompanyName[0] && passedNeoCompanyName[0].updatedNodes) {
                                        // This is just a notification that Neo query ran ok. (updatedNodes.low could be 0 if there is no such company yet)
                                        console.log('In editCompanyByUser-updateCompanyNameOnNeo: companyName updated Succesfully');
                                    } else if (passedNeoCompanyName.hasOwnProperty('error')) {
                                        console.error('In editCompanyByUser-updateCompanyNameOnNeo: Failed to update companyName');
                                    }
                                }
                                delete args.request$.body.data.allowedUsers;
                                delete args.request$.body.data.keywords;
                                delete args.request$.body.data.createDate;
                                delete args.request$.body.data.selectedDomains;
                                // This is a fix to remove redundant UI data from company.
                                if (args.request$.body.data.hasOwnProperty('allowedEditUsers')) {
                                    delete args.request$.body.data.allowedEditUsers;
                                }
                                if (args.request$.body.data.hasOwnProperty('companyResponsibles')) {
                                    delete args.request$.body.data.companyResponsibles;
                                }
                                if (args.request$.body.data.hasOwnProperty('companyContacts')) {
                                    delete args.request$.body.data.companyContacts;
                                }
                                if (args.request$.body.data.hasOwnProperty('companyInformation')) {
                                    delete args.request$.body.data.companyInformation;
                                }
                                if (args.request$.body.data.hasOwnProperty('companyDescription')) {
                                    delete args.request$.body.data.companyDescription;
                                }
                                if (args.request$.body.data.hasOwnProperty('companyClassifications')) {
                                    delete args.request$.body.data.companyClassifications;
                                }
                                if (args.request$.body.data.hasOwnProperty('companySectors')) {
                                    delete args.request$.body.data.companySectors;
                                }
                                if (args.request$.body.data.hasOwnProperty('companyDateOfContact')) {
                                    delete args.request$.body.data.companyDateOfContact;
                                }
                                if (args.request$.body.data.hasOwnProperty('companyDateOfSurvey')) {
                                    delete args.request$.body.data.companyDateOfSurvey;
                                }
                                if (args.request$.body.data.hasOwnProperty('scoresData')) {
                                    delete args.request$.body.data.scoresData;
                                }
                                if (args.request$.body.data.hasOwnProperty('isSurveyNA')) {
                                    delete args.request$.body.data.isSurveyNA;
                                }
                                if (args.request$.body.data.hasOwnProperty('maxIntelScore')) {
                                    delete args.request$.body.data.maxIntelScore;
                                }
                                if (args.request$.body.data.hasOwnProperty('edit')) {
                                    delete args.request$.body.data.edit;
                                }

                                let saveCompanyInfoPromise;
                                if (args.request$.body.data.companyInfo) {
                                    // Save companyInfo on neo4j.
                                    saveCompanyInfoPromise = helpers.saveCompanyInfoOnNeo(args.request$.body.data, seneca);
                                    delete args.request$.body.data.companyInfo;
                                } else {
                                    saveCompanyInfoPromise = Promise.resolve();
                                }

                                console.log('In editCompanyByUser - Starting to update company data');

                                return [promiseAct('role:companies, update:company', {
                                    id: args.request$.body.cid,
                                    data: args.request$.body.data
                                }), saveCompanyInfoPromise];
                            })
                            .catch((e) => {
                                console.log('Error in editCompanyByUser-updateCompanyNameOnNeo: ', e);
                                return Promise.resolve(false, false);
                            });
                    } else {
                        console.log('In editCompanyByUser - User ', uid, ' not permitted to edit this company');
                        return Promise.resolve(false, false);
                    }
                })
                .catch((e) => {
                    console.error('Error in editCompanyByUser-checkWritePermissions: ', e);
                    cb(null, {ok: false, data: {error: 'Failed to edit company'}});
                })
                .spread((passedMongo, passedNeo) => {
                    if (passedNeo) {
                        if (passedNeo.ok) {
                            console.log('In editCompanyByUser - Successfully saved companyInfo on GraphDB');
                        }
                        // There is a third case of empty result where there is no companyInfo to save, so there is no need to write any additional log.
                        else if (passedNeo.hasOwnProperty('error')) {
                            console.log('In editCompanyByUser - Failed to save companyInfo on GraphDB');
                        }
                    }
                    if (passedMongo) {
                        console.log('In editCompanyByUser - Successfully saved companyData on MongoDB');
                        // Update companyName also in all projects' instances.
                        if (args.request$.body.data.hasOwnProperty('companyName')) {
                            console.log('In editCompanyByUser - Starting to update companyName in all projects');

                            // Call getMaxCVSScore with saveScoresOnDB true to recalculate scoresData. (update is required when company data is changed - scores rates / domains)
                            return [promiseAct('role:companies, updateCompanyNameInProjects:company', {
                                id: args.request$.body.cid,
                                companyName: args.request$.body.data.companyName
                            }), promiseAct('role:intelQuery,cmd:getMaxCVSScore', {
                                companyID: args.request$.body.cid,
                                saveScoresOnDB: true,
                                orgId: args.request$.body.orgId
                            })];
                        } else {
                            return Promise.resolve([{ok: true, newCompany: args.request$.body.cid}, false]);
                        }
                    } else {
                        console.log('In editCompanyByUser - Failed to save companyData on MongoDB');
                        return Promise.resolve([{
                            ok: false,
                            err: 'You do not have Permissions to edit company.'
                        }, false]);
                    }
                })
                .catch((e) => {
                    console.error('Error in editCompanyByUser-updateCompany: ', e);
                    cb(null, {ok: false, data: {error: 'Failed to update companyName'}});
                })
                .spread((passedMongoCompanyName, passedMongoScoresData) => {
                    let scoresDataStatus = (passedMongoScoresData && passedMongoScoresData.id) ? 'Successfully updated' : 'Failed to update';
                    console.log('In editCompanyByUser - ' + scoresDataStatus + ' scoresData on company ', args.request$.body.cid);

                    if (passedMongoCompanyName && passedMongoCompanyName.ok) {
                        console.log('In editCompanyByUser - Successfully updated companyName in all projects');
                        return cb(null, {ok: true, newCompany: args.request$.body.cid});
                    } else {
                        console.log('In editCompanyByUser - Failed to update companyName in all projects');
                        return cb(null, {
                            ok: false,
                            error: (passedMongoCompanyName && passedMongoCompanyName.err) ? passedMongoCompanyName.err : 'Error has occurred while updating companyName.'
                        });
                    }
                })
                .catch((e) => {
                    console.error('Error in editCompanyByUser-updateCompanyNameInProjects: ', e);
                    cb(null, {ok: false, data: {error: 'Failed to update companyName'}});
                });
        },

        updateCompanyInfoStatus: (args, cb) => {
            console.log('In updateCompanyInfoStatus');
            if (args.request$.body && args.request$.body.companyID && args.request$.body.nid && args.request$.body.hasOwnProperty('checked')
                && typeof args.request$.body.checked === 'boolean') {
                const uid = args.request$.session.passport.user.id;
                return helpers.isUserRoleAllowed(uid, ['user'], seneca)
                    .then((allowed) => {
                        if (allowed) {
                            console.log('In updateCompanyInfoStatus - User ', uid, ' is allowed to proceed');
                            // This is a fix for checkWritePermissions parameters.
                            const permissionsData = {
                                keywords: [],
                                allowedUsers: []
                            };
                            return helpers.checkWritePermissions(uid, args.request$.body.companyID, permissionsData, seneca);
                        } else {
                            console.log('In updateCompanyInfoStatus - User ', uid, ' not allowed to proceed');
                            return Promise.resolve();
                        }
                    })
                    .catch((e) => {
                        console.error('Error in updateCompanyInfoStatus-isUserRoleAllowed for user ', uid, ': ', e);
                        cb(null, {ok: false, data: {error: 'Failed to edit company'}});
                    })
                    .then((permitted) => {
                        if (permitted) {
                            console.log('In updateCompanyInfoStatus - User ', uid, ' is permitted to edit this company');

                            return intelHelpers.UpdateCompanyInfoStatusByID(args.request$.body.companyID, args.request$.body.nid, args.request$.body.checked);
                        } else {
                            console.log('In editCompanyByUser - User ', uid, ' not permitted to edit this company');
                            return Promise.resolve(false);
                        }
                    })
                    .catch((e) => {
                        console.error('Error in updateCompanyInfoStatus-checkWritePermissions: ', e);
                        cb(null, {ok: false, data: {error: 'Failed to edit company'}});
                    })
                    .then((passedNeo) => {
                        if (passedNeo && Array.isArray(passedNeo) && passedNeo[0] && passedNeo[0].updatedNodes && passedNeo[0].updatedNodes.low) {
                            console.log('In updateCompanyInfoStatus - Successfully updated companyInfo on GraphDB');
                            return cb(null, {ok: true, updatedCompany: args.request$.body.companyID});
                        } else {
                            console.error('In updateCompanyInfoStatus - Failed to update companyInfo on GraphDB');
                            return cb(null, {ok: false, error: 'Failed to update companyInfo.'});
                        }
                    })
                    .catch((e) => {
                        console.error('Error in updateCompanyInfoStatus-UpdateCompanyInfoStatusByID: ', e);
                        cb(null, {ok: false, data: {error: 'Failed to update companyInfo'}});
                    });
            } else {
                console.error('Error in updateCompanyInfoStatus: Wrong parameters inserted');
                cb(null, {ok: false, data: {error: 'Wrong parameters inserted'}});
            }
        },
        addAllCompToNeo4j: async (args, cb) => {
            const id = args.request$.session.passport.user.id;
            const user = args.request$.session.passport.user;
            let allComp = await getAllCompaniesFromDB(id);
            if (allComp && Array.isArray(allComp) && allComp.length > 0) {

                await extraHelpers.asyncForEach(allComp, async (comp, i) => {
                    if (comp && comp.id) {
                            let companyFields = {};
                            function matchesEl(el) {
                                return el.id === comp.id;
                            }

                            let company = allComp.filter((companyId) => {
                                return matchesEl(companyId);
                            });

                            companyFields.companyName = company[0].companyName;
                            companyFields.allowedUsers = company[0].allowedUsers;
                            companyFields.allowedEditUsers = company[0].allowedEditUsers;
                            companyFields.selectedDomains = company[0].selectedDomains;
                            companyFields.IPList = company[0].IPList;
                            companyFields.sensitivity = company[0].sensitivity;
                            companyFields.ratios = company[0].ratios;
                            companyFields.intelScoreRatios = company[0].intelScoreRatios;
                            companyFields.keywords = company[0].keywords;
                            companyFields.id = company[0].id;
                            companyFields.companyInfo = company[0].extraData || {};

                            await addCompanyToDB(companyFields, id, user, i);

                        }
                });
            }
            console.log("Done!");
            cb({ok:true});
        },
        addCompany: async (args, cb) => {
            console.log('In addCompany - admin mode');
            const data = args.request$.body.data || {};
            const extraData = args.request$.body.extraData || {};
            const id = args.request$.session.passport.user.id;
            const user = args.request$.session.passport.user;
            if (user.isUserAllowedToAddVendors || user.userRole === "admin") {
                console.log('In addCompany Starting to save companyData');
                try {
                    //if user edit company we dont want him to chang the current allowed users
                    if (extraData.isInUserEditMode) {
                        data.allowedUsers = undefined;
                        data.allowedEditUsers = undefined;
                    } else if (user.userRole !== "admin") {
                        //on user vendor creation we want to add admin users to be allowed users
                        const adminUsers = await helpers.getAdminUsers();
                        if (adminUsers && Array.isArray(adminUsers)) {
                            const adminUsersIdAndEmailArr = await extraHelpers.getAdminUsersPartialInfo(adminUsers);
                            let allowedUsersArr = data.allowedUsers.concat(adminUsersIdAndEmailArr);
                            data.allowedUsers = allowedUsersArr;
                            data.allowedEditUsers = allowedUsersArr;
                        }
                    }
                    //on user vendor creation we want all created projects to be added to the current organization
                    //and the companies of the projects to connect to the organization on neo
                    if (user.userRole !== "admin" && extraData.createdProjects &&
                        data.chosenProj &&
                        data.chosenOrg &&
                        data.chosenOrg.id !== "" &&
                        data.chosenOrg.organizationName !== "") {
                        let isAdded = await helpers.addNewCreatedProjectsToOrg(extraData.createdProjects, data.chosenProj, data.chosenOrg);
                        if (isAdded && isAdded.ok) {
                            console.log('In addCompany - addProjectsToOrg() all created projects was added to current organization');
                        } else {
                            console.log('In addCompany - addProjectsToOrg() failed to add created projects to current organization');
                        }
                    }
                    let res = await promiseAct('role:intelQuery,add:company', {data: data}, extraData);
                    if (res && res.ok && res.newCompany) {
                        if (data.chosenOrg && data.chosenOrg.id !== '' && data.chosenProj) {
                            try {
                                await helpers.OnUserCompanyCreateMergeOrgAndCompOnNeo(
                                    data.chosenOrg._id,
                                    data.chosenOrg.organizationName,
                                    data.companyName,
                                    res.newCompany.id);
                                console.log('In addCompany - Successfully merge organization and related companies on GraphDB');
                            } catch (e) {
                                console.error('Error in addCompany: ', e);
                            }
                        }
                        if (data.chosenProj && data.chosenProj._id) {
                            await helpers.addNewUserCreatedCompanyToProject(data.chosenProj._id, res.newCompany);
                            console.log('In addCompany - Successfully added new company to project on mongo');
                        }
                        // send notification to admins when user add new vendor
                        if (res.newCompany && extraData.createdProjects &&
                            extraData.isInUserEditMode !== null && !extraData.isInUserEditMode &&
                            data && data.chosenOrg && data.chosenProj) {
                            let sendEmailData = {};
                            sendEmailData.newProjectsCreatedByUser = extraData.createdProjects;
                            sendEmailData.userWhoCreateNewCompany = user;
                            sendEmailData.chosenOrg = data.chosenOrg;
                            sendEmailData.chosenProj = data.chosenProj;
                            sendEmailData.newCompanyData = res.newCompany;
                            const result = await helpers.sendNotificationToAdmin(sendEmailData, promiseAct);
                            if (result && result.ok) {
                                console.log('In addCompany - sendNotificationToAdmin() - Notification sent to admins.');
                            } else {
                                console.log('In addCompany - sendNotificationToAdmin() - Failed  to  sent Notification to admins.');
                            }
                        }
                        cb(null, {ok: true, newCompany: res.newCompany});
                    } else {
                        cb(null, {ok: false, error: 'Error in addCompany - No result has returned'});
                    }
                } catch (e) {
                    console.error('Error in addCompany with user ', id, ': ', e);
                    cb(null, {ok: false});
                }
            } else {
                cb(null, {ok: false, error: 'User unauthorized to add company!'});
            }
        },

        deleteProject: async (args, cb) => {
            const user = args.request$.session.passport.user;
            if (user && (user.isUserAllowedToAddVendors || user.userRole === "admin")) {
                try {
                    let allOrg = [];
                    let allPro = [];
                    let allComp = [];
                    allOrg = await helpers.getAllFromMongoByCollectionName('organizations_map');
                    if (allOrg) {
                        allPro = await helpers.getAllFromMongoByCollectionName('project_map');
                        if (allPro) {
                            allComp = await promiseAct('role:companies, list:company', {query: {}});
                            if (allComp) {
                                seneca.act('role:projects, remove:project', {id: args.request$.body.id}, async (err) => {
                                    if (err) {
                                        cb(null, {ok: false});
                                    }
                                    try {
                                        await helpers.detachOrgFromCompaniesOnNeo(
                                            args.request$.body.id,
                                            allOrg,
                                            allPro,
                                            allComp,
                                            false);

                                        await helpers.removeProjectFromOrganizationsOnMongo(allOrg, args.request$.body.id);

                                        cb(null, {ok: true});
                                    } catch (e) {
                                        console.error('In deleteProject Error: ' + e);
                                        cb(null, {ok: false});
                                    }
                                });

                            } else {
                                console.error('In deleteProject - failed to get all companies for detach on neo');
                                cb(null, {ok: false});
                            }
                        } else {
                            console.error('In deleteProject - failed to get all projects for detach on neo');
                            cb(null, {ok: false});
                        }
                    } else {
                        console.error('In deleteProject - failed to get all organizations for detach on neo');
                        cb(null, {ok: false});
                    }
                } catch (e) {
                    console.error('In deleteProject: ' + e);
                    cb(null, {ok: false});
                }
            } else {
                console.error('Error In deleteProject: unauthorized user');
                cb(null, {ok: false});
            }
        },

        manualSearchBlacklists:
            (args, cb) => {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    console.log('Manual Search for Blacklists is starting');

                    const searchFailedMsg = 'Failed to start the blacklists\' search';

                    helpers.isAdminUserByID(authUser, seneca)
                        .then(async (isAdmin) => {
                            if (isAdmin) {
                                console.log('Collecting all domains for Manual Search for Blacklists');

                                // Get all companies' domains.
                                try {
                                    const res = await promiseAct('role:companies, list:company', {query: {}});
                                    if (res) {
                                        let allDomains = [];

                                        res.map((currCompany) => {
                                            if (currCompany && currCompany.selectedDomains) {
                                                currCompany.selectedDomains.map((currDomain) => {
                                                    if (currDomain && !allDomains.includes(currDomain)) {
                                                        allDomains.push(currDomain);
                                                    }
                                                });
                                            }
                                        });

                                        console.log('Manual Search for Blacklists on ', allDomains.length, ' domains is starting on blacklists-service');

                                        const res = await promiseAct('role:blacklistsSearcher, cmd:collectAllBlacklists', {
                                            id: authUser,
                                            domains: allDomains
                                        });
                                        if (res && res.ok) {
                                            cb(null, {ok: true});
                                        } else {
                                            console.log('Error in manualSearchBlacklists: ', searchFailedMsg);
                                            cb(null, {ok: false, error: searchFailedMsg});
                                        }
                                    } else {
                                        cb(null, {ok: false, error: searchFailedMsg});
                                    }
                                } catch (e) {
                                    cb(null, {ok: false, error: searchFailedMsg});
                                }

                            } else {
                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        })
                        .catch(() => {
                            cb();
                        });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        manualSearchBotnets:
            (args, cb) => {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    console.log('In Manual Search for Botnets');

                    const searchFailedMsg = 'Failed to start the Botnets\' search';

                    helpers.isAdminUserByID(authUser, seneca)
                        .then((isAdmin) => {
                            if (isAdmin) {
                                console.log('Manual Search for Botnets is starting');

                                let params = {};
                                if (args.request$.body.companyIDs && Array.isArray(args.request$.body.companyIDs)) {
                                    params.companyIDs = args.request$.body.companyIDs;
                                }

                                // Get all companies' domains.
                                seneca.act('role:rssFeeds, cmd:collectFeeds', params, (err, res) => {
                                    if (res && res.ok) {
                                        cb(null, {ok: true});
                                    } else {
                                        cb(null, {ok: false, error: searchFailedMsg});
                                    }
                                });
                            } else {
                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        })
                        .catch(() => {
                            cb();
                        });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        manualSearchSSLCertificates:
            (args, cb) => {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    console.log('In Manual Search for SSL Certificates');

                    const searchFailedMsg = 'Failed to start the SSL Certificates\' search';

                    helpers.isAdminUserByID(authUser, seneca)
                        .then((isAdmin) => {
                            if (isAdmin) {
                                console.log('Manual Search for SSL Certificates is starting');

                                let params = {};
                                if (args.request$.body.companyIDs && Array.isArray(args.request$.body.companyIDs)) {
                                    params.companyIDs = args.request$.body.companyIDs;
                                }

                                // Get all companies' domains.
                                seneca.act('role:sslCertificatesCollect, cmd:collectSSLCerts', params, (err, res) => {
                                    if (res && res.ok) {
                                        cb(null, {ok: true});
                                    } else {
                                        cb(null, {ok: false, error: searchFailedMsg});
                                    }
                                });
                            } else {
                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        })
                        .catch(() => {
                            cb();
                        });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        manualSearchWifiNetworks:
            (args, cb) => {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    console.log('In Manual Search for Wifi Networks');

                    const searchFailedMsg = 'Failed to start the Wifi Networks search';

                    helpers.isAdminUserByID(authUser, seneca)
                        .then((isAdmin) => {
                            if (isAdmin) {
                                console.log('Manual Search for Wifi Networks is starting');

                                if (args.request$.body.companyID && (args.request$.body.address || args.request$.body.ssid)) {
                                    const params = {
                                        companyID: args.request$.body.companyID
                                    };

                                    let senecaActCmd;

                                    if (args.request$.body.address) {
                                        params.address = args.request$.body.address;

                                        senecaActCmd = 'role:wifiNetworksCollector, cmd:collectWifiNetworksByAddress';
                                    } else if (args.request$.body.ssid) {
                                        params.ssid = args.request$.body.ssid;

                                        senecaActCmd = 'role:wifiNetworksCollector, cmd:collectWifiNetworksBySsid';
                                    }

                                    if (senecaActCmd) {
                                        // Get all companies' domains.
                                        seneca.act(senecaActCmd, params, (err, res) => {
                                            if (res && res.ok) {
                                                cb(null, {ok: true});
                                            } else {
                                                cb(null, {ok: false, error: searchFailedMsg});
                                            }
                                        });
                                    } else {
                                        cb(null, {ok: false, error: searchFailedMsg});
                                    }
                                } else {
                                    cb(null, {ok: false, error: 'Some parameters are missing'});
                                }

                            } else {
                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        })
                        .catch(() => {
                            cb();
                        });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        manualSearchGDPRCompliance:
            (args, cb) => {
                const authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    console.log('In Manual Search for Web Privacy Policy');

                    const searchFailedMsg = 'Failed to start the Web Privacy Policy\' search';

                    helpers.isAdminUserByID(authUser, seneca)
                        .then((isAdmin) => {
                            if (isAdmin) {
                                console.log('Manual Search for Web Privacy Policy is starting');

                                const params = {};
                                if (args.request$.body.companyIDs && Array.isArray(args.request$.body.companyIDs)) {
                                    params.companyIDs = args.request$.body.companyIDs;
                                }

                                // Get all companies' domains.
                                seneca.act('role:gdprComplianceCollect, cmd:collectGDPR', params, (err, res) => {
                                    if (res && res.ok) {
                                        cb(null, {ok: true});
                                    } else {
                                        cb(null, {ok: false, error: searchFailedMsg});
                                    }
                                });
                            } else {
                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        })
                        .catch(() => {
                            cb();
                        });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        deleteCompany:
            async (args, cb) => {
                const authUser = args.request$.body.uid;
                if (authUser) {
                    const isAdmin = await helpers.isAdminUserByID(authUser, seneca);
                    if (isAdmin) {
                        try {
                            await promiseAct('role:companies, remove:company', {id: args.request$.body.id});
                            await promiseAct('role:companies, deleteCascadeCompanyInProjectsByID:company', {id: args.request$.body.id});
                            if (args.request$.body.selectedDomains && args.request$.body.selectedDomains.length > 0) {
                                const data = {
                                    label: 'COMPANY',
                                    relationship: 'owner',
                                    toLabel: 'domain',
                                    prop: 'cid',
                                    secondProp: 'hostname',
                                    propStr: args.request$.body.id,
                                    arrPropStr: args.request$.body.selectedDomains
                                };

                                await intelHelpers.detachNodes(data);
                            }

                            const data = {
                                label: 'organization',
                                relationship: 'owner',
                                prop: 'cid',
                                propStr: args.request$.body.id
                            };
                            await intelHelpers.detachNodes(data);

                            cb(null, {ok: true});
                        } catch (e) {
                            cb(null, {ok: false, error: e});
                        }
                    } else {
                        cb(null, {ok: false, error: Unauthorized_User_Error});
                    }
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        deleteDiscoveredDomain: (args, cb) => {
            console.log('In deleteDiscoveredDomain');
            const authUser = args.request$.session.passport.user.id;
            if (authUser) {
                helpers.isAdminUserByID(authUser, seneca)
                    .then((isAdmin) => {
                        if (isAdmin) {
                            console.log('In deleteDiscoveredDomain - user is allowed');
                            seneca.act('role:discoveredDomains, DeleteDiscoveredDomainFromCompany:discoveredDomain', args.request$.body, (err, res) => {
                                if (res && res.ok) {
                                    console.log('In deleteDiscoveredDomain - Successfully deleted discoveredDomain from company');
                                    cb(null, {ok: true});
                                } else {
                                    let logMsg = 'failed to delete discoveredDomain from company';
                                    if (err) {
                                        logMsg += ': ' + err;
                                    }
                                    console.error('Error in deleteDiscoveredDomain - ', logMsg);
                                    cb(null, {ok: false, error: logMsg});
                                }
                            });
                        } else {
                            console.log('In deleteDiscoveredDomain - user is not allowed');
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        }
                    })
                    .catch((e) => {
                        console.error('Error in deleteDiscoveredDomain-isAdminUser: ', e);
                        cb(null, {ok: false});
                    });
            } else {
                cb(null, {ok: false, error: Unauthorized_User_Error});
            }
        },

        getDataleaksByKeywords:
            async (args, cb) => {
                try {
                    seneca.client({
                        host: config.addresses.storeServiceAddress,
                        port: 10904
                    });
                    const id = args.request$.session.passport.user.id;
                    const allowed = await helpers.isUserRoleAllowed(id, ['user'], seneca);
                    if (allowed) {
                        const results = await promiseAct('role:pastes, getDataleaks:paste', {
                            query: {
                                uid: id,
                                companiesKeywords: args.request$.body.companiesKeywords,
                                companies: args.request$.body.companies
                            }
                        });
                        if (results) {
                            const resultsMerge = {dataleaksData: results};
                            let fmRes = [];
                            if(args.request$.body.orgId && args.request$.body.orgId !== 'No Id') {
                                try {
                                    fmRes = await intelHelpers.getAssetUserInputAndMitigatedNodesByType(DATALEAKS_TYPE, args.request$.body.orgId, true);
                                } catch (e) {
                                    console.log('In getAssetUserInputAndMitigatedNodesByType - ' + DATALEAKS_TYPE + ' - no data returned');
                                }
                                if(fmRes && fmRes.length > 0 ){
                                    resultsMerge.assetUserInputAndMitigatedData = fmRes;
                                }
                            }
                            cb(null, resultsMerge);
                        } else {
                            console.log('getDataleaks: ', e);
                            cb();
                        }
                    } else {
                        console.log('User ', id, ' role is not allowed to get dataleaks');
                        return Promise.resolve([]);
                    }
                } catch (e) {
                    console.log('getDataleaks: ', e);
                    cb();
                }
            },
        getBotnets:
            (args, cb) => {
                console.log('In getBotnets');
                const id = args.request$.session.passport.user.id;
                helpers.isUserRoleAllowed(id, ['user'], seneca)
                    .then((allowed) => {
                        if (allowed) {
                            console.log('User ', id, ' requested to get Botnets');

                            let params = {};
                            if (args.request$.body.companyIDs && Array.isArray(args.request$.body.companyIDs)) {
                                params.companyIDs = args.request$.body.companyIDs;
                            }
                            if (args.request$.body.startDate && args.request$.body.endDate) {
                                params.startDate = args.request$.body.startDate;
                                params.endDate = args.request$.body.endDate;
                            }
                            return promiseAct('role:rssFeeds, cmd:getFeedsData', params);
                        } else {
                            console.log('User ', id, ' role is not allowed to get Botnets');
                            return Promise.resolve([]);
                        }
                    })
                    .then(async (results) => {
                        console.log('Botnets result: ', results);
                        let fmRes = [];
                        if(args.request$.body.orgId && args.request$.body.orgId !== 'No Id') {
                            try {
                                fmRes = await intelHelpers.getAssetUserInputAndMitigatedNodesByType(BOTNETS_TYPE, args.request$.body.orgId, true);
                            } catch (e) {
                                console.log('In getAssetUserInputAndMitigatedNodesByType - '+ BOTNETS_TYPE + '  - no data returned');
                            }
                            if(fmRes && fmRes.length > 0 ){
                                results.assetUserInputAndMitigatedData = fmRes;
                            }
                        }
                        cb(null, results);
                    })
                    .catch((e) => {
                        console.log('Error in getBotnets: ', e);
                        cb();
                    });
            },

        getSSLCerts:
            async (args, cb) => {
                console.log('In getSSLCerts');
                const id = args.request$.session.passport.user.id;
                try {
                    const allowed = await helpers.isUserRoleAllowed(id, ['user'], seneca);
                    if (allowed) {
                        console.log('User ', id, ' requested to get SSLCerts');

                        const params = {};
                        if (args.request$.body.companyIDs && Array.isArray(args.request$.body.companyIDs)) {
                            params.companyIDs = args.request$.body.companyIDs;
                        }
                        const results = await promiseAct('role:sslCertificatesQuery, cmd:getSSLCerts', params);
                        if (results) {
                            console.log('SSLCerts result: ', results);
                            let fmRes = [];
                            if(args.request$.body.orgId && args.request$.body.orgId !== 'No Id') {
                                try {
                                    fmRes = await intelHelpers.getAssetUserInputAndMitigatedNodesByType(SSL_CERTS_TYPE, args.request$.body.orgId, true);
                                } catch (e) {
                                    console.log('In getAssetUserInputAndMitigatedNodesByType - '+ SSL_CERTS_TYPE + ' - no data returned');
                                }
                                if(fmRes && fmRes.length > 0 ){
                                    results.assetUserInputAndMitigatedData = fmRes;
                                }
                            }
                            cb(null, results);
                        } else {
                            console.log('Error in getSSLCerts: ', e);
                            cb();
                        }
                    } else {
                        console.log('User ', id, ' role is not allowed to get SSLCerts');
                        return Promise.resolve([]);
                    }
                } catch (e) {
                    console.log('Error in getSSLCerts: ', e);
                    cb();
                }
            },

        getWifiNetworks:
            async (args, cb) => {
                console.log('In getWifiNetworks');
                const id = args.request$.session.passport.user.id;
                try {
                    const allowed = await helpers.isUserRoleAllowed(id, ['user'], seneca);
                    if (allowed) {
                        console.log('User ', id, ' requested to get WifiNetworks');

                        const params = {};
                        if (args.request$.body.companyIDs && Array.isArray(args.request$.body.companyIDs)) {
                            params.companyIDs = args.request$.body.companyIDs;
                        }
                        const results = await promiseAct('role:wifiNetworksQuery, cmd:getWifiNetworks', params);
                        if (results) {
                            console.log('getWifiNetworks result: ', results);
                            cb(null, results);
                        } else {
                            console.log('Error in getWifiNetworks: ', e);
                            cb();
                        }
                    } else {
                        console.log('User ', id, ' role is not allowed to get WifiNetworks');
                        return Promise.resolve([]);
                    }
                } catch (e) {
                    console.log('Error in getWifiNetworks: ', e);
                    cb();
                }
            },

        getGDPRData:
            (args, cb) => {
                console.log('In getGDPRData');
                const id = args.request$.session.passport.user.id;
                helpers.isUserRoleAllowed(id, ['user'], seneca)
                    .then((allowed) => {
                        if (allowed) {
                            console.log('User ', id, ' requested to get Web Privacy Policy results');

                            let params = {};
                            if (args.request$.body.companyIDs && Array.isArray(args.request$.body.companyIDs)) {
                                params.companyIDs = args.request$.body.companyIDs;
                            }
                            return promiseAct('role:gdprComplianceQuery, cmd:getGDPRCompliance', params);
                        } else {
                            console.log('User ', id, ' role is not allowed to get GDPR results');
                            return Promise.resolve([]);
                        }
                    })
                    .then(async(results) => {
                        console.log('Web Privacy Policy results: ', results);
                        let fmRes = [];
                        if(args.request$.body.orgId && args.request$.body.orgId !== 'No Id') {
                            try {
                                fmRes = await intelHelpers.getAssetUserInputAndMitigatedNodesByType(GDPR_NOT_COMPLY_TYPE, args.request$.body.orgId, true);
                            } catch (e) {
                                console.log('In getAssetUserInputAndMitigatedNodesByType - '+ GDPR_NOT_COMPLY_TYPE + ' - no data returned');
                            }
                            if(fmRes && fmRes.length > 0 ){
                                results.assetUserInputAndMitigatedData = fmRes;
                            }
                        }
                        cb(null, results);
                    })
                    .catch((e) => {
                        console.log('Error in getGDPRData: ', e);
                        cb();
                    });
            },
        getRelatedDomains:
            (args, cb) => {
                return promiseAct('role:whois, cmd:reverseWhois', {
                    query: {
                        domains: args.request$.body.domains
                    }
                }).then((result) => {
                    if (result.ok && result.filterdResults.length > 0) {
                        cb(null, {ok: true, results: result.filterdResults});
                    } else if (!result.ok) {
                        cb(null, {ok: false, error: result.error});
                    } else {
                        cb(null, {ok: false, error: 'No Results Found'});
                    }
                }).catch((e) => {
                    console.log('getRelatedDomains: ', e);
                    cb(null, {ok: false, error: 'An Error Occurred'});
                });
            },

        harvestEmails:
            (args, cb) => {
                return promiseAct('role:emailHunter, cmd:findEmails', {
                    query: {
                        domain: args.request$.body.domainForEmail
                    }
                }).then((result) => {
                    cb(null, result);
                }).catch((e) => {
                    console.log('harvestEmails: ', e);
                    cb(null, {ok: false, error: 'An Error Occurred'});
                });
            },

        cveImporterGetAllVendors:
            (args, cb) => {
                const id = args.request$.session.passport.user.id;
                if (id) {
                    // Check if the user is allowed to use cveImporter.
                    helpers.isUserAllowedToExportCVEByID(id, seneca)
                        .then((isAllowed) => {
                            if (isAllowed) {
                                return promiseAct('role:cveImporter, cmd:getAllVendors', {}).then((result) => {
                                    if (result.data) {
                                        result = result.data;
                                    }
                                    cb(null, {ok: true, data: result});
                                }).catch((e) => {
                                    console.log('cveImporterGetAllVendors: ', e);
                                    cb(null, {ok: false, error: 'An Error Occurred'});
                                });
                            } else {
                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        })
                        .catch(() => {
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        cveImporterGetAllProductsByVendors:
            (args, cb) => {
                const id = args.request$.session.passport.user.id;
                if (id) {
                    // Check if the user is allowed to use cveImporter.
                    helpers.isUserAllowedToExportCVEByID(id, seneca)
                        .then((isAllowed) => {
                            if (isAllowed) {
                                return promiseAct('role:cveImporter, cmd:getAllVendorProducts', {
                                    vendor: args.request$.body.vendor
                                }).then((result) => {
                                    if (result.data) {
                                        result = result.data;
                                    }
                                    cb(null, {ok: true, data: result});
                                }).catch((e) => {
                                    console.log('cveImporterGetAllProductsByVendors: ', e);
                                    cb(null, {ok: false, error: 'An Error Occurred'});
                                });
                            } else {
                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        })
                        .catch(() => {
                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        exportCVEReport:
            (args, cb) => {
                const id = args.request$.session.passport.user.id;
                if (id) {
                    console.log('User id [', id, '] requested to export a CVE report');
                    // Check if the user is allowed to use cveImporter.
                    let checkUserFunc;
                    // Only admin allowed to export to WORD.
                    if (args.request$.body.outputType && args.request$.body.outputType.toString().toUpperCase() === 'WORD') {
                        checkUserFunc = helpers.isAdminUserByID;
                    } else {
                        checkUserFunc = helpers.isUserAllowedToExportCVEByID;
                    }

                    checkUserFunc(id, seneca)
                        .then((isAllowed) => {
                            if (isAllowed) {
                                console.log('User id [', id, '] is allowed to export a CVE report');
                                if (args.request$.body.data && Array.isArray(args.request$.body.data) && args.request$.body.outputType) {
                                    const data = args.request$.body.data;
                                    // Fix the input to the format that the service can handle.
                                    const input = [];
                                    data.map((currData) => {
                                        if (currData && currData.vendor) {
                                            const currObj = {
                                                vendor: currData.vendor
                                            };

                                            if (currData.products && currData.products.length) {
                                                currData.products.map((currProduct) => {
                                                    if (currProduct) {
                                                        // Duplicate currObj since there may be several products.
                                                        const currObjWithProduct = JSON.parse(JSON.stringify(currObj));
                                                        currObjWithProduct.product = currProduct;
                                                        input.push(currObjWithProduct);
                                                    }
                                                });
                                            } else {
                                                input.push(currObj);
                                            }
                                        }
                                    });

                                    const argsObj = {
                                        creator_uid: id,
                                        userEmail: args.request$.body.userEmail,
                                        input: input,
                                        outputType: args.request$.body.outputType
                                    };

                                    if (args.request$.body.hasOwnProperty('rangeStart') && args.request$.body.hasOwnProperty('rangeEnd')) {
                                        argsObj.rangeStart = args.request$.body.rangeStart;
                                        argsObj.rangeEnd = args.request$.body.rangeEnd;
                                    }

                                    console.log('starting to fetch data from cve-importer-service');

                                    promiseAct('role:cveImporter, cmd:exportByVendorWithOrWithoutProduct', argsObj);
                                    cb(null, {ok: true});
                                } else {
                                    console.log('User id [', id, '] didn\'t send params as expected to exportCVEReport');

                                    cb(null, {ok: false, error: Unauthorized_User_Error});
                                }
                            } else {
                                console.log('User id [', id, '] is not allowed to export a CVE report');

                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        })
                        .catch((e) => {
                            console.error('Error while checking if user is allowed to export a CVE report: ', e);

                            cb(null, {ok: false, error: Unauthorized_User_Error});
                        });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        deletePasteFromUser:
            (args, done) => {
                seneca.client({
                    host: config.addresses.storeServiceAddress,
                    port: 10904
                });

                const id = args.request$.session.passport.user.id;

                helpers.isUserRoleAllowed(id, ['user'], seneca)
                    .then((allowed) => {
                        if (allowed) {
                            return promiseAct('role:pastes, DeletePastebinFromUser:paste', {
                                    lid: args.request$.body.lid,
                                    uid: id
                                }
                            );
                        } else {
                            console.log('User ', id, ' role is not allowed to get dataleaks');
                            return Promise.resolve([]);
                        }
                    })
                    .then(() => {
                        console.log('Updating paste hide list');
                        done(null, {ok: true});
                    })
                    .catch((e) => {
                        console.log(e);
                        done();
                    });
            },

        getAllProjectsById:
            (args, cb) => {
                const id = args.request$.session.passport.user.id;
                seneca.client({
                    host: config.addresses.storeServiceAddress,
                    port: 10904
                });

                helpers.isUserRoleAllowed(id, ['user', 'basic'], seneca)
                    .then((allowed) => {
                        if (allowed) {
                            return promiseAct('role:projects, listProjectsById:project', {
                                query: {uid: id}
                            });
                        } else {
                            console.log('User ', id, ' role is not allowed to get projects by id.');
                            return Promise.resolve([]);
                        }
                    })
                    .then((results) => {
                        return normalizeData(results);
                    })
                    .then((normalizedData) => {
                        cb(null, normalizedData);
                    })
                    .catch((e) => {
                        console.log('Error in getAllProjectsById: ', e);
                        cb();
                    });
            },

        getAllCompaniesById:
            (args, cb) => {
                const id = args.request$.session.passport.user.id;

                seneca.client({
                    host: config.addresses.storeServiceAddress,
                    port: 10904
                });
                helpers.isUserRoleAllowed(id, ['user', 'basic'], seneca)
                    .then((allowed) => {
                        if (allowed) {
                            return promiseAct('role:companies, listCompaniesById:company', {
                                query: {uid: id}
                            });
                        }
                    })
                    .then((results) => {
                        if (results) {
                            return normalizeData(results);
                        }
                    })
                    .then((normalizedData) => {
                        if (normalizedData) {
                            cb(null, normalizedData);
                        } else {
                            cb();
                        }
                    })
                    .catch((e) => {
                        console.log('Error in getAllCompaniesById: ', e);
                        cb();
                    });
            },

        getAllCompaniesByOrgProjects:
            async (args, cb) => {
                const id = args.request$.session.passport.user.id;
                const projects = args.request$.body.projects;
                try {
                    const allowed = await helpers.isUserRoleAllowed(id, ['user', 'basic'], seneca);
                    if (allowed) {
                        const compObjIdsArr = extraHelpers.getCompaniesObjIdsArrFromProjectsArr(projects);
                        if (compObjIdsArr) {
                            const results = await helpers.getCompaniesInfoByCompaniesIdsArrFromMongo(compObjIdsArr, id);
                            if (results) {
                                const normalizedData = await normalizeData(results, true, id, true);
                                if (normalizedData) {
                                    console.log("In getAllCompaniesByOrgProjects");
                                    // Add companies extraData from Neo4j.
                                    const companies = await intelHelpers.getCompanyExtraDataFromNeo(normalizedData);
                                    if (companies) {
                                        console.log('in getAllCompaniesByOrgProjects - got companies by organization projects');
                                        cb(null, companies);
                                    } else {
                                        console.log('Error in getAllCompaniesByOrgProjects - failed to get companies');
                                        cb();
                                    }
                                } else {
                                    console.log('Error in getAllCompaniesByOrgProjects - failed to normalize data');
                                    cb();
                                }
                            } else {
                                console.log('Error in getAllCompaniesByOrgProjects - failed to get info from mongo');
                                cb();
                            }
                        } else {
                            console.log('Error in getAllCompaniesByOrgProjects - failed to get companies object ids array');
                            cb();
                        }
                    } else {
                        console.log('Error in getAllCompaniesByOrgProjects - user not allowed');
                        cb();
                    }
                } catch (e) {
                    console.log('Error in getAllCompaniesByOrgProjects: ', e);
                    cb();
                }
            },

        getAllCompaniesPartialById:
            (args, cb) => {
                const id = args.request$.session.passport.user.id;

                seneca.client({
                    host: config.addresses.storeServiceAddress,
                    port: 10904
                });
                helpers.isUserRoleAllowed(id, ['user', 'basic'], seneca)
                    .then((allowed) => {
                        if (allowed) {
                            return promiseAct('role:companies, listCompaniesById:company', {
                                query: {uid: id}
                            });
                        }
                    })
                    .then((results) => {
                        if (results) {
                            return normalizeData(results, true, id, args.request$.query.isInUserCompanyManagement);
                        }
                    })
                    .then((normalizedData) => {
                        // Add companies extraData from Neo4j.
                        return intelHelpers.getCompanyExtraDataFromNeo(normalizedData);
                    })
                    .then((companies) => {
                        if (companies) {
                            cb(null, companies);
                        } else {
                            cb();
                        }
                    })
                    .catch((e) => {
                        console.log('Error in getAllCompaniesPartialById: ', e);
                        cb();
                    });
            },

        getCompanyPartialById:
            (args, cb) => {
                const id = args.request$.session.passport.user.id;

                if (id && args.request$.body.companyID) {
                    seneca.client({
                        host: config.addresses.storeServiceAddress,
                        port: 10904
                    });
                    helpers.isUserRoleAllowed(id, ['user', 'basic'], seneca)
                        .then((allowed) => {
                            if (allowed) {
                                return promiseAct('role:companies, getCompanyByID:company', {
                                    uid: id,
                                    companyID: args.request$.body.companyID
                                });
                            }
                        })
                        .then((results) => {
                            if (results) {
                                return normalizeData(results, true, id);
                            }
                        })
                        .then((normalizedData) => {
                            if (normalizedData) {
                                cb(null, normalizedData);
                            } else {
                                cb();
                            }
                        })
                        .catch((e) => {
                            console.log('Error in getCompanyPartialById: ', e);
                            cb();
                        });
                } else {
                    console.log('Invalid params for getCompanyPartialById.');
                    cb();
                }
            },

        runManHibp:
            async (args, cb) => {
                let authUser = args.request$.session.passport.user.id;
                if (authUser) {
                    console.log('In Manual Search for Breaches');

                    seneca.client({
                        host: config.addresses.hibpServiceAddress,
                        port: 10777,
                        pin: 'role:hibp'
                    }).client({
                        host: config.addresses.storeServiceAddress,
                        port: 10904
                    });

                    seneca.ready(async () => {
                        const searchFailedMsg = 'Failed to start the Breaches search';
                        try {
                            let isAdmin = await helpers.isAdminUserByID(authUser, seneca);

                            if (isAdmin) {
                                console.log('Manual Search for Breaches is starting');
                                let params = {};
                                if (args.request$.body.companies && Array.isArray(args.request$.body.companies) && args.request$.body.companies.length > 0) {
                                    let companyIdArr = [];
                                    args.request$.body.companies.map((comp) => {
                                        if (comp.id) {
                                            companyIdArr.push(comp.id);
                                        }
                                    });
                                    let companiesWithData = await promiseAct('role:companies, listByMultipleIds:company', {query: {ids: companyIdArr}});
                                    if (companiesWithData) {
                                        params.companies = companiesWithData;
                                    }

                                    let res = await promiseAct('role:hibp, cmd:runCompanyMultiHibp', params);
                                    if (res && res.ok) {
                                        cb(null, {ok: true});
                                    } else {
                                        cb(null, {ok: false, error: searchFailedMsg});
                                    }
                                } else {
                                    cb(null, {ok: false, error: searchFailedMsg});
                                }
                            } else {
                                cb(null, {ok: false, error: Unauthorized_User_Error});
                            }
                        } catch (e) {
                            console.error('In runManHibp() - Error: ' + e)
                        }
                    });
                } else {
                    cb(null, {ok: false, error: Unauthorized_User_Error});
                }
            },

        getCompaniesByProjectName:
            (args, cb) => {
                const id = args.request$.session.passport.user.id;

                seneca.client({
                    host: config.addresses.storeServiceAddress,
                    port: 10904
                });

                helpers.isUserRoleAllowed(id, ['user', 'basic'], seneca)
                    .then((allowed) => {
                        if (allowed) {
                            return promiseAct('role:projects, listProjectsById:project', {
                                query: {uid: id, projectName: args.request$.body.project}
                            });
                        } else {
                            console.log('User ', id, ' role is not allowed to get companies by projectName');
                            return Promise.resolve([]);
                        }
                    })
                    .then((results) => {
                        if (results && results.length > 0) {
                            const companyIdArr = results[0].selectedCompanies.map((company) => {
                                return company.id;
                            });
                            return promiseAct('role:companies, listByMultipleIds:company', {query: {ids: companyIdArr}});
                        }
                    })
                    .then((results) => {
                        if (args.request$.body.sliceStartIndex != null && args.request$.body.sliceMaxLength) {
                            results = {
                                totalCompanies: results.length,
                                partialResult: results.slice(args.request$.body.sliceStartIndex, args.request$.body.sliceStartIndex + args.request$.body.sliceMaxLength)
                            };
                        }
                        cb(null, results);
                    })
                    .catch((e) => {
                        console.log('Error in getCompaniesByProjectName: ', e);
                        cb();
                    });
            },

        updateCompanySurveyScore:
            async (args, cb) => {
                seneca.client({
                    host: config.addresses.storeServiceAddress,
                    port: 10904
                });
                const uid = args.request$.session.passport.user.id;
                try {
                    let allowed = await helpers.isUserRoleAllowed(uid, ['user'], seneca);
                    if (allowed) {
                        let data =  {
                            id: args.request$.body.id,
                            orgId: args.request$.body.orgId,
                            surveyScore: args.request$.body.surveyScore,
                            isUnsafeCompany: args.request$.body.isUnsafeCompany,
                        };
                        let res = await helpers.updateCompanySurveyDataOnMongo(data);
                        if (res) {
                            console.log('Updated Company Score');
                            cb(null, {ok: true});
                        } else {
                            console.log('In updateCompanySurveyScore() - Failed to update company score');
                            cb();
                        }
                    } else {
                        console.log('User ', uid, ' role does not allow to update ');
                        cb();
                    }
                } catch (e){
                    console.log('Error in updateCompanySurveyScore: ', e);
                    cb();
                }
            },

        saveVendorAssessmentData: async (args, cb) => {
            seneca.client({
                host: config.addresses.storeServiceAddress,
                port: 10904
            });
            const user = args.request$.session.passport.user;
            const isVendorAssessmentEnabledToUser = args.request$.session.passport.user.isVendorAssessmentEnabled;
            if (isVendorAssessmentEnabledToUser || user.userRole === "admin") {
                try {
                    const allowed = await helpers.isUserRoleAllowed(user.id, ['user'], seneca);
                    if (allowed) {
                        let data = {};
                        //in case we need to update all assessment fields
                        if (args.request$.body.assessmentFields) {
                            data = await helpers.updateVendorAssessmentFieldsAndScore(
                                args.request$.body.compId,
                                args.request$.body.assessmentFields,
                                args.request$.body.assessmentScore);
                            //in case we need to update only one assessment fields
                        } else {
                            console.error('In saveVendorAssessmentData() - no fields to update was found!');
                            cb(null, {ok: false});
                        }
                        if (data) {
                            console.log('In saveVendorAssessmentData() - Vendor Assessment Updated');
                            cb(null, {ok: true, data: data});
                        } else {
                            console.error('In saveVendorAssessmentData() - Vendor Assessment failed to update');
                            cb(null, {ok: false});
                        }
                    } else {
                        console.error('User ', user.id, 'Role does not allow to update ');
                        cb(null, {ok: false});
                    }
                } catch (e) {
                    console.error('Error in saveVendorAssessmentData() - ', e);
                    cb(null, {ok: false});
                }
            } else {
                console.error('Error in saveVendorAssessmentData() - user is not allowed to use vendor assessment');
                cb(null, {ok: false});
            }
        },


        updateAssessmentNotificationArea: async (args, cb) => {
            seneca.client({
                host: config.addresses.storeServiceAddress,
                port: 10904
            });
            const user = args.request$.session.passport.user;
            const isVendorAssessmentEnabledToUser = args.request$.session.passport.user.isVendorAssessmentEnabled;
            if (isVendorAssessmentEnabledToUser || user.userRole === "admin") {
                try {
                    const allowed = await helpers.isUserRoleAllowed(user.id, ['user'], seneca);
                    if (allowed) {
                        let data = {};
                        //in case we need to update all assessment fields
                        if (args.request$.body.notificationAreaComments) {
                            data = await helpers.updateAssessmentNotificationArea(
                                args.request$.body.compId,
                                args.request$.body.notificationAreaComments);
                        } else {
                            console.error('In updateAssessmentNotificationArea() - no fields to update was found!');
                            cb(null, {ok: false});
                        }

                        if (data) {
                            console.log('In updateAssessmentNotificationArea() - Vendor Assessment Notification Area Updated');
                            cb(null, {ok: true, data: data});
                        } else {
                            console.error('In updateAssessmentNotificationArea() - Vendor Assessment Notification Area failed to update');
                            cb(null, {ok: false});
                        }
                    } else {
                        console.error('User ', user.id, 'Role does not allow to update ');
                        cb(null, {ok: false});
                    }
                } catch (e) {
                    console.error('Error in updateAssessmentNotificationArea() - ', e);
                    cb(null, {ok: false});
                }
            } else {
                console.error('Error in updateAssessmentNotificationArea() - user is not allowed to use vendor assessment notification area');
                cb(null, {ok: false});
            }
        }
    }
};

function normalizeData(data, partial, id, isInUserCompanyManagement) {
    return new Promise((resolve) => {
        const newData = data.map((item) => {
            const newData = {};
            if (item.projectName) {
                newData.projectName = item.projectName;
            }

            if (item.hasOwnProperty("isSelfAssessmentProject")) {
                newData.isSelfAssessmentProject = item.isSelfAssessmentProject;
            } else {
                newData.isSelfAssessmentProject = false;
            }

            if (item.companyName) {
                newData.companyName = item.companyName;
            }

            if (item.dataByOrg && typeof item.dataByOrg === 'object') {
                newData.dataByOrg = item.dataByOrg;
            }

            if (!partial || isInUserCompanyManagement) {
                newData.allowedUsers = item.allowedUsers;
                newData.allowedEditUsers = item.allowedEditUsers;

                if (item.keywords) {
                    newData.keywords = item.keywords;
                }
            }

            // Check if user is allowed to edit company
            if (item.allowedEditUsers && item.allowedEditUsers.length > 0 && id) {
                for (let i = 0; i < item.allowedEditUsers.length; i++) {
                    if (item.allowedEditUsers[i].id === id) {
                        newData.edit = true;
                    }
                }
            }

            newData.intelScoreRatios = item.intelScoreRatios;
            if (newData.intelScoreRatios) {
                // Iterate on all supposed-to-exist intelScoreRatios to verify there is a value for each one.
                Object.keys(helpers.DEFAULT_INTEL_SCORE_RATIOS).map((currKey) => {
                    if (currKey && newData.intelScoreRatios[currKey] == null) {
                        newData.intelScoreRatios[currKey] = 0;
                    }
                });
            }
            if (item.IPList) {
                newData.IPList = item.IPList;
            }
            newData.selectedCompanies = item.selectedCompanies;
            newData.selectedDomains = item.selectedDomains;
            newData.sensitivity = item.sensitivity;
            newData.lastScanDate = item.currentScanDate;
            newData.percentageDone = item.percentageDone;
            newData.ratios = item.ratios;
            newData.createDate = item.createDate;
            newData.id = item.id || item._id;
            return newData;
        });
        resolve(newData);
    });
}
