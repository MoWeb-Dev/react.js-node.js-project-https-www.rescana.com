const ObjectId = require('mongodb').ObjectId;
const PromiseB = require('bluebird');
const neo4jDriver = require('neo4j-driver').v1;
const config = require('app-config');
const neo4jRetried = require('@ambassify/neo4j-retried');
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass),
    {
        maxConnectionLifetime: 20 * 60 * 1000,
        connectionTimeout: 1000 * 45,
        connectionAcquisitionTimeout: 600000,
        maxTransactionRetryTime: 10000,
        connectionPoolSize: 1000
    }), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});

const {DATALEAKS_TYPE, BLACKLISTS_TYPE, BOTNETS_TYPE, SSL_CERTS_TYPE, GDPR_NOT_COMPLY_TYPE, EMAIL_BREACHES_TYPE} = require('../../../config/consts.js');


module.exports.getFullSelectedObjectsInfoById = (fullData, selectedObjects) => {
    let resArray = [];
    if (Array.isArray(fullData) && fullData && selectedObjects) {
        if (!Array.isArray(selectedObjects)) {
            selectedObjects = [selectedObjects];
        }
        for (let i = 0; i < fullData.length; i++) {
            if (fullData[i]._id) {
                fullData[i].id = fullData[i]._id.valueOf().toString();
            }
            for (let j = 0; j < selectedObjects.length; j++) {
                if (fullData[i] && selectedObjects[j]) {
                    if (fullData[i].id) {
                        if ((selectedObjects[j].id && (fullData[i].id === selectedObjects[j].id)) ||
                            (selectedObjects[j] && (fullData[i].id === selectedObjects[j]))) {
                            let exists = resArray.some(user => fullData[i].id === user.id);
                            if (!exists) {
                                resArray.push(fullData[i]);
                            }
                        }
                    }
                }
            }
        }
    }
    return resArray;
};

module.exports.checkIfExistsInArrById = (objArray, userToFind) => {
    return Array.isArray(objArray) && objArray ? objArray.some(user => userToFind.id && user.id && userToFind.id === user.id) : false;
};

module.exports.validateUserName = (user) => {
    let name = {firstName: '', lastName: ''};
    if (user && user.name) {
        if (user.name.firstName) {
            name.firstName = user.name.firstName;
        }
        if (user.name.lastName) {
            name.lastName = user.name.lastName;
        }
    }
    return name;
};

module.exports.getCompaniesObjIdsArrFromProjectsArr = (projects) => {
    let compArr = [];
    let compIdArr = [];
    if (projects) {
        for (let i = 0; i < projects.length; i++) {
            if (projects[i].selectedCompanies && Array.isArray(projects[i].selectedCompanies)) {
                compArr = compArr.concat(projects[i].selectedCompanies);
            }
        }
        for (let i = 0; i < compArr.length; i++) {
            if (compArr[i].id) {
                compIdArr.push(ObjectId(compArr[i].id.toString()));
            }
        }
    }
    return compIdArr;
};

module.exports.getAdminUsersPartialInfo = (fullUserInfo) => {
    let partialUserInfo = [];
    let idVal = "";
    if (fullUserInfo) {
        partialUserInfo = fullUserInfo.map((item) => {
            idVal = item.id || item._id;
            if (item.email && idVal) {
                if (idVal && (typeof idVal !== "string")) {
                    idVal = idVal.valueOf().toString();
                }
                return {email: item.email, id: idVal}
            }
        });
    }
    return partialUserInfo;
};

module.exports.createIdsArray = (arr) => {
    let idsArray = [];
    if (arr) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].id || arr[i]._id) {
                idsArray.push(arr[i].id || arr[i]._id)
            }
        }
    }
    return idsArray;
};

module.exports.isOrgDataValidForSavingSurveyTemplate = (orgData, orgId) => {
    return !!(orgId &&
        typeof orgId === 'string' &&
        orgId !== '' &&
        orgData &&
        typeof orgData === 'object' &&
        orgData.allowedUsers &&
        Array.isArray(orgData.allowedUsers) &&
        orgData.organizationName &&
        typeof orgData.organizationName === 'string');
};

const removeDuplicates = (myArr, prop) => {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
};
module.exports.removeDuplicates = removeDuplicates;

module.exports.getUsersFromOrgArrForSurveyTemplate = (curUser, allowedOrganizations) => {
    let allowedUsers = [];
    if (curUser && typeof curUser === 'object' && curUser.id && curUser.email && allowedOrganizations && Array.isArray(allowedOrganizations)) {
        allowedOrganizations.map((currOrg) => {
            if (currOrg.orgUsers && Array.isArray(currOrg.orgUsers)) {
                allowedUsers = allowedUsers.concat(currOrg.orgUsers);
            }
        });
        allowedUsers.push(curUser);
    } else if (curUser && typeof curUser === 'object' && curUser.id && curUser.email) {
        return [curUser];
    }

    return removeDuplicates(allowedUsers, 'id');
};

module.exports.isStringValidAndNotEmpty = (value) => {
    return !!(value && typeof value === 'string' && value !== '' && value !== ' ');
};

module.exports.asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

module.exports.getSurveyAnswersEmailText = (url, emailExpiryDate, companyName, orgName, message) => {
    //we use fontColor for each one of the rows because the html text that sends with the
    //nodemailer npm sometimes change the color of the font on its own
    const fontColor = '#7e7e7e';

    let text = '<!DOCTYPE html><html><head></head><body style="color: '+ fontColor+' ">' +
        '<div style="color: '+ fontColor +'; border-radius: 6px; border-bottom: none; background-color: #f7f7f7;">' +
        '<div style="color: '+ fontColor +'; margin-left: 20px; ">';
    text += '<br/><p style="color: '+ fontColor +'">Dear Sir or Madam,</p><br/>';
    if(orgName && orgName !== ""){
        text += '<p style="color: '+ fontColor +'">' + orgName + ' requires to perform a third-party assessment for '
            + companyName + ' platform in order to identify and analyze potential cyber threats to the company.</p>';
    } else {
        text += '<p style="color: '+ fontColor +'">A third-party assessment for '
            + companyName + ' is needed in order to identify and analyze potential cyber threats.</p>';
    }
    text += '<p style="color: '+ fontColor +'">The assessment is carried out using the Rescana 3rd party vendor assessment system and includes:</p>';
    text += '<p style="color: '+ fontColor +'">1. Open source intelligence (<a style="color: #42afff" href="https://en.wikipedia.org/wiki/Open-source_intelligence" target="_blank">OSINT</a>) of public information related to the vendor.</p>';
    text += '<p style="color: '+ fontColor +'">2. A self assessment security questionnaire.</p>';
    text += '<p style="color: '+ fontColor +'">Please complete the below questionnaire:</p>';
    text += '<a style="color: #42afff" href="https\://' + url + '" target="_blank">Self Assessment Security Questionnaire</a><br/><br/>';
    text += '<p style="color: '+ fontColor +'">The questionnaire will be available until ' + emailExpiryDate + ' (end of day) - feel free to contact us if you require more time to complete the questionnaire.</p>';

    if(module.exports.isStringValidAndNotEmpty(message)){
        text += '<br/><p style="color: #7e7e7e">Sender Comments:</p>';
        text += '<p style="color: '+ fontColor +'">' + message + '</p>';
    }

    text += '<br/><p style="color: '+ fontColor +'">Thank you very much,</p>';
    if(orgName && orgName !== ""){
        text += '<b style="color: '+ fontColor +'">Rescana (on behalf of ' + orgName + ').</b>';
    } else {
        text += '<b style="color: '+ fontColor +'">Rescana.</b>';
    }
    text += '<br/><br/></div><div style="border-radius: 0 0 6px 6px; background-color: #424242">' +
        '<img style="padding: 3px 0 3px 0; margin-left: 88%;width: 110px; " alt="" ' +
        'src="https://static.wixstatic.com/media/d64002_6495ce3b5ba747029c99b94aa741195e~mv2.png/v1/fill/w_412,h_236,al_c,lg_1/logo.png"/></div>';
    text += '</div></body></html>';

    return text;
};

module.exports.getFromNeoByQuery = (query) => {
    return new PromiseB(async (resolve, reject) => {
        if (query && typeof query === "string" && query !== '' && query !== ' ') {
            try {
                const session = neo4j.session();
                let result = await session.readTransaction((transaction) => {return transaction.run(query);});
                if(result){
                    console.log('In getFromNeoByQuery -  Success');
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                } else {
                    console.error('In getFromNeoByQuery - Error - failed to get data ');
                    return reject();
                }
            } catch (error) {
                console.error('In getFromNeoByQuery - Error: ', error);
                return reject();
            }
        } else {
            console.error('In getFromNeoByQuery - Error - no query ');
            return reject();
        }
    });
};

const convertToObject = (result) => {
    if(result && result.records && Array.isArray(result.records) && result.records.length > 0){
        return result.records.map((record) => {
            const dataObj = {};

            for (let i = 0; i < record._fields.length; i++) {
                dataObj[record.keys[i]] = record._fields[i];
            }

            return dataObj;
        });
    } else {
        console.log('In convertToObject() - no data found');
        return [];
    }
};


module.exports.removeUnnecessaryProps = (curArr, type) => {
    if (curArr && Array.isArray(curArr) && curArr.length > 0) {
        let keyArr = ['importance', 'userEmail', 'type'];
        curArr.map((curData)=>{
            if (type === BOTNETS_TYPE) {
            } else if (type === SSL_CERTS_TYPE) {
                keyArr.push('domain', 'days_remaining', 'statusSSL', 'valid','Issued_To', 'Issued_By');
            } else if (type === DATALEAKS_TYPE) {
                keyArr.push('source', 'title', 'url');
            } else if (type === EMAIL_BREACHES_TYPE) {
                keyArr.push('email', 'domain','breach','breach_date');
            } else if (type === BLACKLISTS_TYPE) {
                keyArr.push('domain');
            }

            Object.keys(curData).map((key) => {
                if(key === 'issuer' && type === SSL_CERTS_TYPE){
                    curData.Issued_By = curData[key].CN;
                }
                if(key === 'subject' && type === SSL_CERTS_TYPE) {
                    curData.Issued_To = curData[key].CN;
                }
                if(key === 'breach_classes' && type === EMAIL_BREACHES_TYPE){
                    curData.Compromised_Data = curData[key];
                }
                if(key === 'data' && type === BLACKLISTS_TYPE){
                    curData.ip = curData[key].blacklist_ip && curData[key].blacklist_ip.address? curData[key].blacklist_ip.address : '-';
                    curData.blacklist = curData[key].blacklist_ip && curData[key].blacklist_ip.blacklist &&
                    curData[key].blacklist_ip.blacklist[0]? curData[key].blacklist_ip.blacklist[0] : '-';
                }
                if(!keyArr.includes(key)){
                    delete curData[key];
                }
            });
        });
    }

    return curArr;
};
