module.exports.surveyObj1 =
    [{
        'name': 'First',
        'sid': 'a3f5gh6',
        'said': 'abcd'
    }];

module.exports.surveyObj3 = [
    {
        'lastChangedDate': '2017-08-23T19:32:36+03:00',
        'sid': 'a3f5gh6',
        'said': 'abcdee',
        'selectedCompanyId': '5991ed92550b83316835feaa',
        'respondent': 'אני',
        'name': 'דוגמא',
        'uid': [
            '589710c3df466b238870ce14'
        ],
        'lang': 'he',
        'score': 0.298427400546411,
        'counts': {
            'NACount': 21,
            'complyCount': 4,
            'nonComplyCount': 10,
            'questionsCount': 35
        }
    }
];

module.exports.surveyObj2 =
    [{
        'lastChangedDate': '2017-08-23T19:32:36+03:00',
        'sid': 'a3f5gh6',
        'said': 'abcdee',
        'selectedCompanyId': '5991ed92550b83316835feaa',
        'respondent': 'אני',
        'name': 'דוגמא',
        'uid': [
            '589710c3df466b238870ce14'
        ],
        'pages': [
            {
                'name': 'אבטחת מידע פיזית',
                'elements': [
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'יש פוליסות אבטחה בארגון?',
                                'choices': [
                                    'קיים',
                                    'לא קיים',
                                    'לא רלוונטי'
                                ],
                                'withText': {
                                    'name': 'הערות'
                                },
                                'weight': 3,
                                'answer': 'לא קיים'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do plans exist for implementing security policies?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 4,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do new employees receive training regarding policies?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 3,
                                'answer': 'Non Comply'
                            }
                        ],
                        'name': 'Policies'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Are background checks performed on new employees?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does training cover ISO27001?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do new employees receive specific roles?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Are some employees noted as critical?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Comply'
                            }
                        ],
                        'name': 'Human Resources'
                    }
                ]
            },
            {
                'elements': [
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Is guarding applied 24/7?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do all employees wear an ID tag?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do cameras exist in the facility?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Are alarms sent to a control center?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does the server room require a PIN code?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            }
                        ],
                        'name': 'Access Control'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'When guests arrive is someone notified?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do guests require escort?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does a list of permanent guests exist?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Guests'
                    }
                ],
                'name': 'פיזי'
            },
            {
                'elements': [
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Is the network config under the company control?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Are all the company interfaces under the company control?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is there a segment for LAN?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is the network monitored?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Network Configuration'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Do all access point have firewalls?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is the last rule DROP?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does the firewall drop all UDP packets?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is the traffic logged?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is the access to the firewall in SSH?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Firewall'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Is WLAN allowed in the organization?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does 128bit  WPA encryption exist?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Are VPN tunnels used?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'WLAN'
                    }
                ],
                'name': 'רשתות'
            },
            {
                'elements': [
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Are all the systems monitored?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is an alert system used?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does the company use IDS?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Who checks the IDS?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Monitoring'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'What is the incident response procedure?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does an IR log exist?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does the IT know what to do in case of a fire?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'What is the procedure of notifying clients?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Response'
                    }
                ],
                'name': 'תגובות'
            }
        ],
        'lang': 'he',
        'score': 0.298427400546411,
        'counts': {
            'NACount': 21,
            'complyCount': 4,
            'nonComplyCount': 10,
            'questionsCount': 35
        }
    }, {
        'lastChangedDate': '2017-08-23T19:32:36+03:00',
        'sid': 'a3f5gh6',
        'said': 'abcd',
        'selectedCompanyId': '5991ed92550b83316835feaa',
        'respondent': 'Me',
        'name': 'Kiwinet 123',
        'uid': [
            '589710c3df466b238870ce14'
        ],
        'pages': [
            {
                'name': 'ORGANIZATION SECURITy',
                'elements': [
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Is there a security policy in the organization?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 3,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do plans exist for implementing security policies?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 4,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do new employees receive training regarding policies?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 3,
                                'answer': 'Non Comply'
                            }
                        ],
                        'name': 'Policies'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Are background checks performed on new employees?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does training cover ISO27001?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do new employees receive specific roles?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Are some employees noted as critical?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Comply'
                            }
                        ],
                        'name': 'Human Resources'
                    }
                ]
            },
            {
                'elements': [
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Is guarding applied 24/7?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do all employees wear an ID tag?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do cameras exist in the facility?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Are alarms sent to a control center?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does the server room require a PIN code?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            }
                        ],
                        'name': 'Access Control'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'When guests arrive is someone notified?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Do guests require escort?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984,
                                'answer': 'Non Comply'
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does a list of permanent guests exist?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Guests'
                    }
                ],
                'name': 'PHYSICAl'
            },
            {
                'elements': [
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Is the network config under the company control?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Are all the company interfaces under the company control?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is there a segment for LAN?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is the network monitored?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Network Configuration'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Do all access point have firewalls?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is the last rule DROP?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does the firewall drop all UDP packets?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is the traffic logged?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is the access to the firewall in SSH?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Firewall'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Is WLAN allowed in the organization?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does 128bit  WPA encryption exist?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Are VPN tunnels used?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'WLAN'
                    }
                ],
                'name': 'NETWORk'
            },
            {
                'elements': [
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'Are all the systems monitored?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Is an alert system used?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does the company use IDS?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Who checks the IDS?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Monitoring'
                    },
                    {
                        'type': 'panel',
                        'elements': [
                            {
                                'type': 'radiogroup',
                                'name': 'What is the incident response procedure?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does an IR log exist?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'Does the IT know what to do in case of a fire?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            },
                            {
                                'type': 'radiogroup',
                                'name': 'What is the procedure of notifying clients?',
                                'choices': [
                                    'Non Comply',
                                    'Comply',
                                    'Other'
                                ],
                                'withText': {
                                    'name': 'Comments'
                                },
                                'weight': 2.59133126934984
                            }
                        ],
                        'name': 'Response'
                    }
                ],
                'name': 'INCIDENT RESPONSe'
            }
        ],
        'lang': 'en',
        'score': 0.298427400546411,
        'counts': {
            'NACount': 21,
            'complyCount': 4,
            'nonComplyCount': 10,
            'questionsCount': 35
        }
    }];

module.exports.elCamino = {
    'companyName': 'El Camino',
    'allowedUsers': [
        {
            'email': 'admin@admin.com',
            'id': '58b9407143b63223f93b487b'
        }
    ],
    'selectedDomains': [
        'ectusa.net',
        'elcaminosoft.com',
        'elcaminotech.com',
        'elcamsoft.com',
        'ectsoftware.com'
    ],
    'createDate': '2017-09-05T18:55:44+03:00',
    'id': '59aec904e101ce072aa31daa'
};
