const chai = require('chai');
const mocha = require('mocha');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const extraHelpers = require('../helpers');
const {DATALEAKS_TYPE, BLACKLISTS_TYPE, BOTNETS_TYPE, SSL_CERTS_TYPE, GDPR_NOT_COMPLY_TYPE, EMAIL_BREACHES_TYPE} = require('../../../../config/consts.js');
// const apiHelpers = require('../api-helpers.js');
const surveyObj = require('./survey-object.js');

describe('Server APIs', function () {
    // describe('getProgress() - Should return survey progress from survey', function (fin) {
    //     it('Should return number', function (fin) {
    //         const result = apiHelpers.getProgress(surveyObj.surveyObj2[0]);
    //         fin();
    //         return expect(result).to.be.a('number');
    //     });
    //
    //     it('Should handle survey without pages', function (fin) {
    //         const result = apiHelpers.getProgress(surveyObj.surveyObj3[0]);
    //         fin();
    //         return expect(result).to.be.a('number');
    //     });
    // });
    //
    // describe('getKnownPortInfo() - Should return array of known info for the port', function (fin) {
    //     it('Should return array with port as number', function (fin) {
    //         const result = apiHelpers.getKnownPortInfo(6379);
    //         fin();
    //         return expect(result).to.be.a('Array');
    //     });
    //     it('Should return array with port as string', function (fin) {
    //         const result = apiHelpers.getKnownPortInfo('3306');
    //         fin();
    //         return expect(result).to.be.a('Array');
    //     });
    //     it('Should return name and description for known ports', function (fin) {
    //         const result = apiHelpers.getKnownPortInfo(80);
    //         fin();
    //         expect(result[0].name).to.equal('name');
    //         expect(result[0].finding).to.equal('http');
    //         expect(result[1].name).to.equal('description');
    //         return expect(result[1].finding).to.equal('World Wide Web HTTP');
    //     });
    // });
    //
    // describe('allowExtensions() - Should return true if extension is allowed', function () {
    //     it('Should allow xlsx extension', function () {
    //         const result = apiHelpers.allowExtensions('moshe.xlsx');
    //         return expect(result).to.be.true;
    //     });
    //
    //     it('Should allow capital letters extension', function () {
    //         const result = apiHelpers.allowExtensions('moshe.XlsX');
    //         return expect(result).to.be.true;
    //     });
    //
    //     it('Should not allow a bbb extension', function () {
    //         const result = apiHelpers.allowExtensions('moshe.bbb');
    //         return expect(result).to.be.false;
    //     });
    // });


    describe('getFullInfoSelectedObjectsById() - Should return array of objects', function () {

        const infoObj = [
            {"name": "Rescana", "id": "5c6539ea5296c4495980c38c"},
            {"name": "tzPro", "id": "5c80048253820e34720bf683"},
            {"name": "me", "id": "5c6eb9185296c449598195c5"},
            {"name": "sda", "id": "5c8004ae53820e34720bf684"},
            {"name": "now", "id": "5c7243055296c4495981c040"},
            {"name": "asdf", "id": "5c87ab5773de841660da532b"}
        ];

        const objectsToFind = [
            {"name": "Rescana", "id": "5c6539ea5296c4495980c38c"},
            {"name": "tzPro", "id": "5c80048253820e34720bf683"}
        ];

        it('Should be array with 0 objects (we send false properties to func', function () {
            const result = extraHelpers.getFullSelectedObjectsInfoById('rescana', 'rescana');
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });

        it('Should be array with 2 objects', function () {
            const result = extraHelpers.getFullSelectedObjectsInfoById(infoObj, objectsToFind);
            expect(result.length).to.equals(2);
            return expect(result).to.be.an('array');
        });

        it('Should be empty array', function () {
            const result = extraHelpers.getFullSelectedObjectsInfoById([], objectsToFind);
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });

        it('Should be empty array(send nothing)', function () {
            const result = extraHelpers.getFullSelectedObjectsInfoById();
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });

        it('Should be empty array(send numbers)', function () {
            const result = extraHelpers.getFullSelectedObjectsInfoById(2, 2);
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });
    });

    describe('checkIfExistsInArrById() - Should get array and object with id property and return true if obj exists in array else false ', function () {
        const infoObj = {
            "_id": "5cbc0a4259882c795fdd5120",
            "projectName": "test 1",
            "allowedUsers": [{"email": "admin@admin.com", "id": "5c6935e09ee0db203a64db46"}, {
                "email": "dsad@dsa.s",
                "id": "5ca46415df07f815a3149f8d"
            }, {"email": "tzach@rescana.com", "id": "5c65389426fe11fa170bc164"}],
            "selectedCompanies": [{"name": "tzPro", "id": "5c80048253820e34720bf683"}],
            "createDate": "2019-04-21T10:56:55+03:00", "id": ""
        };

        const objectsToFind = {"email": "tzach@rescana.com", "id": "5c65389426fe11fa170bc164"};
        const objectsToFind2 = {"email": "tom@rescana.com", "id": "5c65389426fe11f2314a2341"};

        it('Should be true - user exists in infoObj', function () {
            const result = extraHelpers.checkIfExistsInArrById(infoObj.allowedUsers, objectsToFind);
            return expect(result).to.be.true;
        });

        it('Should be true - send string instead of array', function () {
            const result = extraHelpers.checkIfExistsInArrById(infoObj.projectName, objectsToFind);
            return expect(result).to.be.false;
        });

        it('Should be false - user doesnt exists in infoObj', function () {
            const result = extraHelpers.checkIfExistsInArrById(infoObj.allowedUsers, objectsToFind2);
            return expect(result).to.be.false;
        });

        it('Should be false - infoObj.createDate is not array', function () {
            const result = extraHelpers.checkIfExistsInArrById(infoObj.createDate, objectsToFind2);
            return expect(result).to.be.false;
        });

        it('Should be false - send nothing', function () {
            const result = extraHelpers.checkIfExistsInArrById();
            return expect(result).to.be.false;
        });

        it('Should be false - send nothing', function () {
            const result = extraHelpers.checkIfExistsInArrById(3, 1);
            return expect(result).to.be.false;
        });
        it('Should be false - send nothing', function () {
            const result = extraHelpers.checkIfExistsInArrById([], {});
            return expect(result).to.be.false;
        });
    });

    describe('validateUserName() - Should return object with first name and last name properties(they could be empty)', function () {

        const infoObj = {
            "_id": "5cc995359fbb5b074a7667bb",
            "nick": "momo@sa.com",
            "email": "momo@sa.com",
            "name": {},
        };

        const infoObj2 = {
            "_id": "5cc995359fbb5b074a7667bb",
            "nick": "momo@sa.com",
            "email": "momo@sa.com",
        };

        const infoObj3 = {
            "_id": "5cc995359fbb5b074a7667bb",
            "nick": "momo@sa.com",
            "email": "momo@sa.com",
            "name": [],
        };

        it('Should return object', function () {
            const result = extraHelpers.validateUserName(infoObj);
            expect(typeof result.firstName).to.be.an('string');
            expect(typeof result.lastName).to.be.an('string');
            return expect(result).to.be.an('object');
        });

        it('Should return object', function () {
            const result = extraHelpers.validateUserName(infoObj2);
            expect(typeof result.firstName).to.be.an('string');
            expect(typeof result.lastName).to.be.an('string');
            return expect(result).to.be.an('object');
        });

        it('Should return object', function () {
            const result = extraHelpers.validateUserName(infoObj3);
            expect(typeof result.firstName).to.be.an('string');
            expect(typeof result.lastName).to.be.an('string');
            return expect(result).to.be.an('object');
        });

        it('Should return object', function () {
            const result = extraHelpers.validateUserName();
            expect(typeof result.firstName).to.be.an('string');
            expect(typeof result.lastName).to.be.an('string');
            return expect(result).to.be.an('object');
        });

        it('Should return object', function () {
            const result = extraHelpers.validateUserName(4);
            expect(typeof result.firstName).to.be.an('string');
            expect(typeof result.lastName).to.be.an('string');
            return expect(result).to.be.an('object');
        });

        it('Should return object', function () {
            const result = extraHelpers.validateUserName({});
            expect(typeof result.firstName).to.be.an('string');
            expect(typeof result.lastName).to.be.an('string');
            return expect(result).to.be.an('object');
        });

    });


    describe('getCompaniesObjIdsArrFromProjectsArr() - Should return array of Object Ids', function () {

        const project = {
            "_id": "5cdsabb5b074ad231dsadd7667bb",
            "projectName": 'test',
        };

        const projectsArr = [{
            "_id": "5cdsabb5b074ad231dsadd7667bb",
            "projectName": 'test',
        },
            {
                "_id": "dsadsadwwddas2",
                "projectName": 'test2',
                "selectedCompanies": [
                    {
                        "name": "test comp",
                        "id": "5cc9857f33b47d35f2d45041"
                    }
                ]
            }
        ];

        it('Should return object ids array with 0 object ids(send object instead of array properties)', function () {
            const result = extraHelpers.getCompaniesObjIdsArrFromProjectsArr(project);
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });

        it('Should return object ids array with 0 object ids(send nothing)', function () {
            const result = extraHelpers.getCompaniesObjIdsArrFromProjectsArr();
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });

        it('Should return object ids array with 1 object ids', function () {
            const result = extraHelpers.getCompaniesObjIdsArrFromProjectsArr(projectsArr);
            expect(result.length).to.equals(1);
            expect(result[0]).to.be.an('object');
            return expect(result).to.be.an('array');
        });
    });


    describe('createIdsArray() - gets array of objects and retrieve from the id or _id property array of ids ', function () {

        const arrOfObjects = [
            {"_id": "5cdsabb5b0dsadw74ad231dsadd7667bb", "projectName": 'test'},
            {"_id": "dsad231dsadd7667bb", "projectName": 'test'},
            {"id": "5cdsaads31dsadd7667bb", "projectName": 'test'},
            {"_id": "5cdsabb5b07asd31dsadd7667bb", "projectName": 'test'},
            {"id": "asxzcad231dsadd7667bb", "projectName": 'test'}
        ];

        const arrOfObjects2 = [
            {"date": "25/5/2100", "projectName": 'test'},
            {"_id": "dsad231dsadd7667bb", "projectName": 'test'},
            {"id": "5cdsaads31dsadd7667bb", "projectName": 'test'},
            {"_id": "5cdsabb5b07asd31dsadd7667bb", "projectName": 'test'},
            {"id": "asxzcad231dsadd7667bb", "projectName": 'test'}
        ];

        it('Should return array with length of 5 ids', function () {
            const result = extraHelpers.createIdsArray(arrOfObjects);
            expect(result.length).to.equals(5);
            return expect(result).to.be.an('array');
        });

        it('Should return array with length of 4 ids(one of the object in the array doesnt have id or _id property)', function () {
            const result = extraHelpers.createIdsArray(arrOfObjects2);
            expect(result.length).to.equals(4);
            return expect(result).to.be.an('array');
        });

        it('Should return empty array(we send object instead of array)', function () {
            const result = extraHelpers.createIdsArray({});
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });
        it('Should return empty array(we send number instead of array)', function () {
            const result = extraHelpers.createIdsArray(213);
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });

        it('Should return empty array(we send nothing)', function () {
            const result = extraHelpers.createIdsArray();
            expect(result.length).to.equals(0);
            return expect(result).to.be.an('array');
        });
    });

    describe('isOrgDataValidForSavingSurveyTemplate() - organization data validation for saving survey template (return true or false)', function () {

        const orgData = {
            "_id": "5d11d1fb31816e3ba59385d0",
            "allowedUsers": [
                {"email": "admin@admin.com", "id": "5c6935e09ee0db203a64db46"},
                {"email": "tzach@gmail.com", "id": "5cd7e19b1036d2bb12de1b0b"}],
            "organizationName": "Global Organization",
        };
        const orgDataWithWrongFieldsType = {
            "_id": "5d11d1fb31816e3ba59385d0",
            "allowedUsers": {},
            "organizationName": ['name'],
        };
        const orgDataWithWrongFields = {
            "_id": "5d11d1fb31816e3ba59385d0",
            "allowed": [
                {"email": "admin@admin.com", "id": "5c6935e09ee0db203a64db46"},
                {"email": "tzach@gmail.com", "id": "5cd7e19b1036d2bb12de1b0b"}],
            "name": "Global Organization",
        };
        const orgId = '5d11d1fb31816e3ba59385d0';

        it('Should return true', function () {
            const result = extraHelpers.isOrgDataValidForSavingSurveyTemplate(orgData, orgId);
            return expect(result).to.equal(true);
        });
        it('Should return false - orgId is empty string', function () {
            const result = extraHelpers.isOrgDataValidForSavingSurveyTemplate(orgData, '');
            return expect(result).to.equal(false);
        });
        it('Should return false - not sending orgId', function () {
            const result = extraHelpers.isOrgDataValidForSavingSurveyTemplate(orgData);
            return expect(result).to.equal(false);
        });
        it('Should return false - sending with wrong fields type', function () {
            const result = extraHelpers.isOrgDataValidForSavingSurveyTemplate(orgDataWithWrongFieldsType, orgId);
            return expect(result).to.equal(false);
        });

        it('Should return false - sending with wrong fields ', function () {
            const result = extraHelpers.isOrgDataValidForSavingSurveyTemplate(orgDataWithWrongFields, orgId);
            return expect(result).to.equal(false);
        });
    });

    describe('getUsersFromOrgArrForSurveyTemplate() - extract from organization data unique ' +
        'allowed users for saving in survey template object on mongo', function () {

        const allowedOrganizations = [
            {
                "organizationName" : "Global Organization",
                "id" : "5d11d1fb31816e3ba59385d0",
                "orgUsers" : [
                    {"email" : "admin@admin.com", "id" : "5c6935e09ee0db203a64db46"},
                    {"email" : "tzach@gmail.com", "id" : "5cd7e19b1036d2bb12de1b0b"}
                ]
            },
            {
                "organizationName" : "Second Org",
                "id" : "5d1b7bddf96b764657d9cf0a",
                "orgUsers" : [
                    {"id" : "5c6935e09ee0db203a64db46", "email" : "admin@admin.com",},
                    {"email" : "tzach@gmail.com", "id" : "5cd7e19b1036d2bb12de1b0b"}
                ]
            }
        ];

        const curUser = {
            "email" : "tzach@gmail.com",
            "id" : "5cd7e19b1036d2bb12de1b0b"
        };

        it('Should return array of 2 users', function () {
            const result = extraHelpers.getUsersFromOrgArrForSurveyTemplate(curUser, allowedOrganizations);
            expect(result[1].email).to.equals("tzach@gmail.com");
            expect(result[1].id).to.equals("5cd7e19b1036d2bb12de1b0b");
            return expect(result.length).to.equals(2);
        });

        it('Should return empty array', function () {
            const result = extraHelpers.getUsersFromOrgArrForSurveyTemplate(null, allowedOrganizations);
            return expect(result.length).to.equals(0);
        });

        it('Should return array with the curUser', function () {
            const result = extraHelpers.getUsersFromOrgArrForSurveyTemplate(curUser, []);
            expect(result[0].email).to.equals("tzach@gmail.com");
            expect(result[0].id).to.equals("5cd7e19b1036d2bb12de1b0b");
            return expect(result.length).to.equals(1);
        });

        it('Should return array with the curUser ', function () {
            const result = extraHelpers.getUsersFromOrgArrForSurveyTemplate(curUser);
            expect(result[0].email).to.equals("tzach@gmail.com");
            expect(result[0].id).to.equals("5cd7e19b1036d2bb12de1b0b");
            return expect(result.length).to.equals(1);
        });

    });


    describe('isStringValidAndNotEmpty() - check for name validation ', function () {

        const value = "Some name";
        const valueInsideArray = ["Some name"];
        const valueInsideObj = {name: "Some name"};
        const number = 0;
        const emptyValue = "";


        it('Should return true', function () {
            const result = extraHelpers.isStringValidAndNotEmpty(value);
            return expect(result).to.be.true;
        });

        it('Should return false - nameInsideArray', function () {
            const result = extraHelpers.isStringValidAndNotEmpty(valueInsideArray);
            return expect(result).to.be.false;
        });

        it('Should return false - nameInsideObj', function () {
            const result = extraHelpers.isStringValidAndNotEmpty(valueInsideObj);
            return expect(result).to.be.false;
        });

        it('Should return false - emptyName', function () {
            const result = extraHelpers.isStringValidAndNotEmpty(emptyValue);
            return expect(result).to.be.false;
        });

        it('Should return false - emptyName2', function () {
            const result = extraHelpers.isStringValidAndNotEmpty(" ");
            return expect(result).to.be.false;
        });

        it('Should return false - nothing send', function () {
            const result = extraHelpers.isStringValidAndNotEmpty();
            return expect(result).to.be.false;
        });

        it('Should return false - number', function () {
            const result = extraHelpers.isStringValidAndNotEmpty(number);
            return expect(result).to.be.false;
        });

        it('Should return false - undefined', function () {
            const result = extraHelpers.isStringValidAndNotEmpty(undefined);
            return expect(result).to.be.false;
        });
    });

    describe('removeUnnecessaryProps() - gets objects array and a object type, and by the type we choose which' +
        ' props will stay and which will remove,- returnd the array with the wanted props', function () {

        it('Should return arr without "notNeededProp" prop', function () {
            const arr = [{importance: 4, userEmail: 'tzach@fsad.com', type: 'ssl', notNeededProp: 'nothing', domain: 'mydomain.com'}];
            const result = extraHelpers.removeUnnecessaryProps(arr, SSL_CERTS_TYPE);
            expect(result[0].importance).to.equals(4);
            expect(result[0].userEmail).to.equals('tzach@fsad.com');
            expect(result[0].type).to.equals('ssl');
            expect(result[0].domain).to.equals('mydomain.com');
            return expect(result[0].notNeededProp).to.equals(undefined);
        });

        it('Should return arr without "notNeededProp" prop and domain prop', function () {
            const arr = [{importance: 4, userEmail: 'tzach@fsad.com', type: 'ssl', notNeededProp: 'nothing', domain: 'mydomain.com'}];
            const result = extraHelpers.removeUnnecessaryProps(arr, DATALEAKS_TYPE);
            expect(result[0].importance).to.equals(4);
            expect(result[0].userEmail).to.equals('tzach@fsad.com');
            expect(result[0].type).to.equals('ssl');
            expect(result[0].domain).to.equals(undefined);
            return expect(result[0].notNeededProp).to.equals(undefined);
        });

        it('Should return arr without "notNeededProp" prop 2', function () {
            const arr = [{importance: 4, userEmail: 'tzach@fsad.com', type: 'ssl', notNeededProp: 'nothing', domain: 'mydomain.com'}];
            const result = extraHelpers.removeUnnecessaryProps(arr, EMAIL_BREACHES_TYPE);
            expect(result[0].importance).to.equals(4);
            expect(result[0].userEmail).to.equals('tzach@fsad.com');
            expect(result[0].type).to.equals('ssl');
            expect(result[0].domain).to.equals('mydomain.com');
            return expect(result[0].notNeededProp).to.equals(undefined);
        });
    });
});
