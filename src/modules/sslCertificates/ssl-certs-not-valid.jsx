import React from 'react';
import SSLCertificates from './ssl-certs-main.jsx';


export default class SSLCertificatesNotValid extends React.Component {
    constructor() {
        super();
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    render() {
        return (
            <SSLCertificates
                displayOnlyValid={false}
            />
        );
    }
}
