import React from 'react';
import SSLCertificates from './ssl-certs-main.jsx';


export default class SSLCertificatesValid extends React.Component {
    constructor() {
        super();
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    render() {
        return (
            <SSLCertificates
                displayOnlyValid={true}
            />
        );
    }
}
