import React from 'react';
import {
    SSL_CERTS_TYPE,
    PROJECT_NAME,
    SESSION_COMPANIES,
    GRAPH_COMPANIES_FILTER_LIMIT,
    ORGANIZATION_ID
} from '../../../config/consts';
import classNames from 'classnames';
import {getLoadedCompaniesObject, getDefaultCompanies} from '../common/datatableTemplate/datatable-template-helpers.js';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import {getKeyNestedChildsIfExists} from '../common/CommonHelpers.js';
import '../../../public/css/survey.css';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SettingsFilterTemplate from '../common/datatableTemplate/settings-filter-template-main.jsx';
import Loader from 'react-loader-advanced';
import DataTableViewTemplate from '../common/datatableTemplate/datatable-template.jsx';
const shortid = require('shortid');
import themeForFont from '../app/themes/themeForFont';
import generalFunctions from "../generalComponents/general-functions";
import textForTitleExpl from "../appGeneralFiles/text-for-title-explanations";
import TitleExplanationComponent from '../appGeneralFiles/title-explanation-component.jsx';


const DAYS_REMAINING = 'days_remaining',
    DAYS_EXPIRED = 'days_expired',
    RELATED_COMPANIES = 'relatedCompanies',
    ISSUED_TO = 'subject.CN',
    NO_SUBDOMAIN = 'None';


export default class SSLCertificates extends React.Component {
    constructor() {
        super();

        const wrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word'
        };
        const clickableWrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word',
            'cursor': 'pointer'
        };
        const smallClickableWrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word',
            'cursor': 'pointer',
            'width': '9%'
        };

        this.state = {
            showLoader: false,
            displayOnlyValid: true,
            companies: [],
            companiesInputVal: [],
            tableData: [],
            tableDataToDownload: [],
            data: [],
            fullData: [],
            assetUserInputAndMitigatedData: [],
            /* Create table column headers */
            tableColumns: [
                {
                    sortable: true,
                    label: 'Domain',
                    key: 'domain',
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Subdomain',
                    key: 'subdomain',
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Status',
                    key: 'statusSSL',
                    style: smallClickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Days Remaining',
                    key: DAYS_REMAINING,
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Days Expired',
                    key: DAYS_EXPIRED,
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Issued To',
                    key: ISSUED_TO,
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Issued By',
                    key: 'issuer.CN',
                    style: clickableWrappableStyle
                }, {
                    label: 'Related Companies',
                    key: RELATED_COMPANIES,
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: true,
                    label: 'Importance',
                    key: 'Importance',
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: false,
                    label: 'Comments',
                    key: 'Comments',
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: true,
                    label: 'Mitigated',
                    key: 'Mitigated',
                    style: wrappableStyle,
                    noneSearchable: true
                }
            ],
            tableColumnsToDownload: [
                {label: 'Domain', key: 'domain'},
                {label: 'Subdomain', key: 'subdomain'},
                {label: 'Status', key: 'statusSSL'},
                {label: 'Days Remaining', key: DAYS_REMAINING},
                {label: 'Days Expired', key: DAYS_EXPIRED},
                {label: 'Issued To', key: ISSUED_TO},
                {label: 'Issued By', key: 'issuer.CN'},
                {label: 'Related Companies', key: RELATED_COMPANIES},
                {label: 'Importance', key: 'Importance'},
                {label: 'Comments', key: 'Comments'},
                {label: 'Mitigated', key: 'Mitigated'},
            ],
            tableExtraDataColumns: [
                {label: 'id', key: 'id'},
                {label: 'Domain', key: 'domain'},
                {label: 'Domain', key: 'subdomain'},
                {label: 'Status', key: 'statusSSL'},
                {label: 'Last Updated', key: 'lastUpdated'},
                {label: 'Valid From', key: 'valid_from'},
                {label: 'Valid To', key: 'valid_to'},
                {label: 'Days Remaining', key: DAYS_REMAINING},
                {label: 'Days Expired', key: DAYS_EXPIRED},
                {label: 'Subject', key: 'subject'},
                {label: 'Issuer', key: 'issuer'},
                {label: 'subjectAltName', key: 'subjectaltname'},
                {label: 'Info Access', key: 'infoAccess'},
                {label: 'Exponent', key: 'exponent'},
                {label: 'Fingerprint', key: 'fingerprint'},
                {label: 'Ext Key Usage', key: 'ext_key_usage'},
                {label: 'Serial Number', key: 'serialNumber'}
            ],
            openInfoBox: false
        };

        this.setResults = this.setResults.bind(this);
        this.updateState = this.updateState.bind(this);
        this.setDefaultCompanies = this.setDefaultCompanies.bind(this);
        this.saveLoadedCompanies = this.saveLoadedCompanies.bind(this);
        this.handleUpdateFpAndMitigatedData = this.handleUpdateFpAndMitigatedData.bind(this);
        this.recalculateScore = this.recalculateScore.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);

        // This is a fix for sslCerts to display data from the first time loading this component.
        if (this.props && this.props.hasOwnProperty('displayOnlyValid')) {
            this.state.displayOnlyValid = this.props.displayOnlyValid;

            let redundantPropToHide;
            if (this.props.displayOnlyValid) {
                // Remove days_remaining
                redundantPropToHide = DAYS_EXPIRED;
            } else {
                redundantPropToHide = DAYS_REMAINING;
            }
            this.state.tableColumns = this.state.tableColumns.filter((c) => {
                return c.key !== redundantPropToHide;
            });
            this.state.tableColumnsToDownload = this.state.tableColumnsToDownload.filter((c) => {
                return c.key !== redundantPropToHide;
            });
        }
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            this.setState({user: user});
            // In case it is desired to save user preferences, use: this.getAllCompaniesByID(user);
            this.getAllowedCompanies(true);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            // Default displayOnlyValid is true, if given 'false' - save it.
            let displayOnlyValid = !(nextProps.hasOwnProperty('displayOnlyValid') && !nextProps.displayOnlyValid);
            this.setState({displayOnlyValid: displayOnlyValid});
        }
        if (nextProps && nextProps.assetUserInputAndMitigatedData) {
            this.setState({assetUserInputAndMitigatedData: nextProps.assetUserInputAndMitigatedData});
        }
    }


    //AMData = assetUserInput and mitigated Data
    updateStateForAMData(assetUserInputAndMitigatedData) {
        this.setState({assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
    }

    recalculateScore() {
        let data = {};

        if (this.state.companiesInputVal && this.state.companiesInputVal.length > 0) {
            data.companyIDs = [];
            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        data.orgId = localStorage.getItem(ORGANIZATION_ID);
        data.saveScoresOnDB = true;
        $.ajax({
            type: 'POST',
            url: '/api/getMaxCVSScore',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'CalculateScores');
            } else {
                app.addAlert('Error', 'Error calculating scores');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to calculate scores');
        });
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    getAllowedCompanies(isDefault, selectedCompanies) {
        $.ajax({
            type: 'GET',
            url: '/api/getAllCompaniesById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((companies) => {
            if (isDefault) {
                this.saveDefaultCompanies(companies);
            } else {
                this.saveLoadedCompanies(companies, selectedCompanies);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load companies.');
        });
    }

    saveDefaultCompanies(companies) {
        if (companies) {
            let pn = localStorage.getItem(PROJECT_NAME);
            if (pn) {
                $.ajax({
                    type: 'POST',
                    url: '/api/getCompaniesByProjectName',
                    async: true,
                    data: JSON.stringify({project: pn}),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((projectCompanies) => {
                    this.setDefaultCompaniesToState(companies, projectCompanies);
                }).fail(() => {
                    app.addAlert('error', 'Error: failed to load project\'s companies.');
                });
            } else {
                this.setDefaultCompaniesToState(companies, null);
            }
        }
    }

    updateState(stateObj) {
        if (stateObj) {
            this.setState(stateObj, this.getSSLCertsFindings);
        }
    }

    setDefaultCompaniesToState(companies, projectCompanies) {
        let result = getDefaultCompanies(companies, projectCompanies, 'selectedDomains');
        if (result && result.companies && result.companiesInputVal && result.companiesPropArray) {
            if (result.companiesInputVal.length > 1) {
                // If the Session has a selection saved - load it.
                let sessionCompanies = sessionStorage.getItem(SESSION_COMPANIES);
                if (sessionCompanies) {
                    let selectedNames = sessionCompanies.split(', ');
                    let relatedCompanies = result.companiesInputVal.filter((c) => {
                        return selectedNames.includes(c.companyName);
                    });
                    if (relatedCompanies && relatedCompanies.length > 0) {
                        result.companiesInputVal = relatedCompanies;
                    }
                } else {
                    // Select only the first company as a default selection.
                    result.companiesInputVal.splice(1, result.companiesInputVal.length);
                }
            }
            this.setDefaultCompanies(result);
        }
    }

    setDefaultCompanies(result) {
        if (result && result.companies && result.companiesInputVal) {
            this.setState({
                companies: result.companies,
                companiesInputVal: result.companiesInputVal
            }, this.getSSLCertsFindings);
        }
    }

    saveLoadedCompanies(companies, selectedCompanies) {
        let result = getLoadedCompaniesObject(companies, selectedCompanies, 'selectedDomains');

        if (result && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: companies,
                companiesInputVal: result.companiesInputVal,
                companiesDomains: result.companiesPropArray
            }, this.getSSLCertsFindings);
        }
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    getSSLCertsFindings() {
        if (this.state.companiesInputVal && Array.isArray(this.state.companiesInputVal) && this.state.companiesInputVal.length > 0) {
            let companyIDs = [];
            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !companyIDs.includes(currComp.id)) {
                    companyIDs.push(currComp.id);
                }
            });
            if (companyIDs.length > 0) {
                this.startLoader();
                let orgId = localStorage.getItem(ORGANIZATION_ID);

                const data = {
                    companyIDs: companyIDs,
                    orgId: orgId
                };
                $.ajax({
                    type: 'POST',
                    url: '/api/getSSLCerts',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((res) => {
                    this.stopLoader();
                    let assetUserInputAndMitigatedData = [];

                    if (res && res.ok) {
                        if(res.data && Array.isArray(res.data)){
                            if(res.assetUserInputAndMitigatedData){
                                assetUserInputAndMitigatedData = res.assetUserInputAndMitigatedData;
                            }
                            this.setState({fullData: res.data, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData})

                        }
                    }

                    this.setResults();
                }).fail(() => {
                    this.stopLoader();
                    app.addAlert('error', 'Error: failed to fetch data');
                });
            }
        }
    }

    add3Dots(str, limit) {
        const dots = '...';
        if (str && limit && str.length > limit) {
            str = str.substring(0, limit) + dots;
        }

        return str;
    }

    handleUpdateFpAndMitigatedData(assetUserInputAndMitigatedData) {
        this.setState({assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
    }

    setResults() {
        const CHARS_LIMIT = 20;
        let data = this.state.fullData || [];
        let tableData = [];
        let tableDataToDownload = [];
        let assetUserInputAndMitigatedData = this.state.assetUserInputAndMitigatedData || [];

        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;

        if (data && Array.isArray(data)) {
            const disabledChar = '-';
            let demoIdx = 0;
            data.map((currData) => {
                if (currData && currData.domain
                    && currData.statusSSL
                    && currData.hasOwnProperty('valid')
                    && currData[RELATED_COMPANIES]
                    && Array.isArray(currData[RELATED_COMPANIES])) {
                    // This check is to separate valid domains from not-valid subdomains (for correct display).
                    if (this.state.displayOnlyValid === currData.valid) {

                        let tableEntityObj = {
                            id: currData.id,
                            surveyIdentifier: shortid.generate()
                        };

                        let tableEntityObjToDownload = {};

                        this.state.tableColumns.map((currColumn) => {
                            let currKey = currColumn.key;
                            if (currKey !== 'Importance' && currKey !== 'Comments' && currKey !== 'Mitigated') {
                                let currDataValue = getKeyNestedChildsIfExists(currData, currKey);
                                if (currDataValue != null) {
                                    if (currKey === RELATED_COMPANIES) {
                                        if(isInDemoMode){
                                            currDataValue = ['Company Name'];
                                        }
                                        tableEntityObj[currKey] = showExpandedDataChip(currDataValue, 3, 5);
                                        tableEntityObjToDownload[currKey] = currDataValue.join(', ');
                                    } else {
                                        // Apply data as string so FuzzySearch will be able to use this registry properly.
                                        if (currKey === 'domain' && currData.url) {
                                            tableEntityObjToDownload[currKey] = currDataValue;
                                            currDataValue = <a href={currData.url} target="_blank"
                                                               style={{'textDecoration': 'none'}}>{currDataValue}</a>;
                                            if(isInDemoMode){
                                                currDataValue = 'domain-example-'+ demoIdx +'.com';
                                            }
                                            tableEntityObj[currKey] = (currKey === ISSUED_TO) ? this.add3Dots(isInDemoMode? 'domain-example-'+ demoIdx +'.com': currDataValue, CHARS_LIMIT) : currDataValue;
                                        } else {
                                            tableEntityObj[currKey] = (currKey === ISSUED_TO) ? this.add3Dots(isInDemoMode? 'domain-example-'+ demoIdx +'.com': currDataValue, CHARS_LIMIT) : currDataValue;
                                            tableEntityObjToDownload[currKey] = currDataValue;
                                        }
                                    }
                                } else {
                                    tableEntityObj[currKey] = '';
                                    tableEntityObjToDownload[currKey] = '';
                                }
                            }
                        });
                        demoIdx++;

                        // Fix text so only one of the following two properties will be displayed.
                        if (tableEntityObj[DAYS_REMAINING]) {
                            tableEntityObj[DAYS_EXPIRED] = disabledChar;
                            tableEntityObjToDownload[DAYS_EXPIRED] = disabledChar;
                        } else if (tableEntityObj[DAYS_EXPIRED]) {
                            tableEntityObj[DAYS_REMAINING] = disabledChar;
                            tableEntityObjToDownload[DAYS_REMAINING] = disabledChar;
                        }
                        let importance = 3;
                        let mitigatedBackground = 'black';
                        let comments = [];
                        let assetUserInputFlag = false;
                        let mitigatedBackgroundFlag = false;
                        let commentsFlag = false;

                        for(let j=0; j<assetUserInputAndMitigatedData.length; j++){
                            if(this.refs.dataTableViewTemplate.isCurRowNeedToBeUpdateCurNode(tableEntityObj, assetUserInputAndMitigatedData[j])){
                                if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'assetUserInput'){
                                    importance = assetUserInputAndMitigatedData[j].importance? Number(assetUserInputAndMitigatedData[j].importance) : 3;
                                    assetUserInputFlag = true;
                                }
                                if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'mitigated'){
                                    mitigatedBackground = '#5bd25e';
                                    mitigatedBackgroundFlag = true;
                                }
                                if(assetUserInputAndMitigatedData[j].comments){
                                    comments = assetUserInputAndMitigatedData[j].comments? assetUserInputAndMitigatedData[j].comments : [];
                                    commentsFlag = true;
                                }
                                if(mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag){
                                    break;
                                }
                            }
                        }
                        let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';
                        tableEntityObj.Importance = this.refs.dataTableViewTemplate.getImportanceComponent(tableEntityObj, importance);
                        tableEntityObj.Comments = this.refs.dataTableViewTemplate.getCommentsComponent(tableEntityObj, comments.length, comments, badgeColor);
                        tableEntityObj.Mitigated = this.refs.dataTableViewTemplate.getMitigatedComponent(tableEntityObj, mitigatedBackground);

                        tableEntityObjToDownload.Importance = generalFunctions.getImportanceStr(importance);
                        tableEntityObjToDownload.Comments = generalFunctions.allCommentsToOneStr(comments);
                        tableEntityObjToDownload.Mitigated = mitigatedBackgroundFlag ? 'Yes' : 'No';
                        // Initialize main domain' subdomain value to None.
                        tableEntityObj.subdomain = NO_SUBDOMAIN;
                        tableEntityObjToDownload.subdomain = NO_SUBDOMAIN;

                        // Add extra data to tableEntityObj for future click event.
                        tableEntityObj.extraData = [];
                        this.state.tableExtraDataColumns.map((currColumn) => {
                            if (currColumn && currColumn.key && currColumn.label) {
                                let finding = getKeyNestedChildsIfExists(currData, currColumn.key);
                                if (finding != null && finding !== '') {
                                    tableEntityObj.extraData.push({
                                        name: currColumn.label,
                                        finding: finding
                                    });
                                }
                            }
                        });

                        tableData.push(tableEntityObj);
                        tableDataToDownload.push(tableEntityObjToDownload);
                    }

                    // Add results also for subdomains.
                    if (currData.subdomains && Array.isArray(currData.subdomains)) {
                        currData.subdomains.map((currSubdomain) => {
                            if (currSubdomain
                                && currSubdomain.domain
                                && currSubdomain.statusSSL
                                && currSubdomain.hasOwnProperty('valid')
                                && this.state.displayOnlyValid === currSubdomain.valid) {
                                let subDomain = this.add3Dots(currSubdomain.domain, CHARS_LIMIT);
                                if (currSubdomain.url) {
                                    subDomain = <a href={currSubdomain.url} target="_blank"
                                                   style={{'textDecoration': 'none'}}>{this.add3Dots(currSubdomain.domain, CHARS_LIMIT)}</a>;
                                }
                                if(isInDemoMode){
                                    subDomain = 'subdomain-example-'+ demoIdx +'.com';
                                }

                                let subdomainTableEntityObj = {
                                    surveyIdentifier: shortid.generate(),
                                    domain: isInDemoMode? 'domain-example-'+ demoIdx +'.com': currData.domain,
                                    subdomain: subDomain
                                };

                                let subdomainTableEntityObjToDownload = {
                                    domain: isInDemoMode? 'domain-example-'+ demoIdx +'.com': currData.domain,
                                    subdomain: isInDemoMode? 'subdomain-example-'+ demoIdx +'.com':  currSubdomain.domain
                                };

                                // Add relatedCompanies manually from parent object.
                                subdomainTableEntityObj[RELATED_COMPANIES] = showExpandedDataChip(isInDemoMode? ['Company Name'] : currData[RELATED_COMPANIES], 3, 5);
                                subdomainTableEntityObjToDownload[RELATED_COMPANIES] = isInDemoMode? ['Company Name'] : currData[RELATED_COMPANIES].join(', ');

                                this.state.tableColumns.map((currColumn) => {
                                    let currKey = currColumn.key;
                                    if (currKey !== 'Importance' && currKey !== 'Comments' && currKey !== 'Mitigated') {
                                        // Add only keys that weren't added manually before.
                                        if (!subdomainTableEntityObj.hasOwnProperty(currKey)) {
                                            let currDataValue = getKeyNestedChildsIfExists(currSubdomain, currKey);
                                            if (currDataValue != null) {
                                                // Apply data as string so FuzzySearch will be able to use this registry properly.
                                                subdomainTableEntityObj[currKey] = (currKey === ISSUED_TO) ?
                                                    isInDemoMode? 'subdomain-example-'+ demoIdx +'.com': this.add3Dots('' + currDataValue, CHARS_LIMIT) : '' + currDataValue;
                                                subdomainTableEntityObjToDownload[currKey] = currDataValue;
                                            } else {
                                                subdomainTableEntityObj[currKey] = '';
                                                subdomainTableEntityObjToDownload[currKey] = '';
                                            }
                                        }
                                    }
                                });

                                // Fix text so only one of the following two properties will be displayed.
                                if (subdomainTableEntityObj[DAYS_REMAINING]) {
                                    subdomainTableEntityObj[DAYS_EXPIRED] = disabledChar;
                                    subdomainTableEntityObjToDownload[DAYS_EXPIRED] = disabledChar;
                                } else if (subdomainTableEntityObj[DAYS_EXPIRED]) {
                                    subdomainTableEntityObj[DAYS_REMAINING] = disabledChar;
                                    subdomainTableEntityObjToDownload[DAYS_REMAINING] = disabledChar;
                                }

                                // This is a fix for extra data to display related domain of this subdomain.
                                currSubdomain.subdomain = currData.domain;

                                // Add extra data to tableEntityObj for future click event.
                                subdomainTableEntityObj.extraData = [];
                                this.state.tableExtraDataColumns.map((currColumn) => {
                                    if (currColumn && currColumn.key && currColumn.label) {
                                        let finding;
                                        if(currColumn.key === 'id'){
                                            finding = getKeyNestedChildsIfExists(currData, currColumn.key);
                                        } else {
                                            finding = getKeyNestedChildsIfExists(currSubdomain, currColumn.key);
                                        }

                                        if (finding != null && finding !== '') {
                                            let currName = (currColumn.key === 'domain') ? 'Subdomain' : currColumn.label;
                                            subdomainTableEntityObj.extraData.push({
                                                name: currName,
                                                finding: finding
                                            });
                                        }
                                    }
                                });
                                demoIdx++;

                                let importance = 3;
                                let mitigatedBackground = 'black';
                                let comments = [];
                                let assetUserInputFlag = false;
                                let mitigatedBackgroundFlag = false;
                                let commentsFlag = false;

                                for(let j=0; j<assetUserInputAndMitigatedData.length; j++){
                                    if(this.refs.dataTableViewTemplate.isCurRowNeedToBeUpdateCurNode(subdomainTableEntityObj, assetUserInputAndMitigatedData[j])){
                                        if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'assetUserInput'){
                                            importance = assetUserInputAndMitigatedData[j].importance? Number(assetUserInputAndMitigatedData[j].importance) : 3;
                                            assetUserInputFlag = true;
                                        }
                                        if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'mitigated'){
                                            mitigatedBackground = '#5bd25e';
                                            mitigatedBackgroundFlag = true;
                                        }
                                        if(assetUserInputAndMitigatedData[j].comments){
                                            comments = assetUserInputAndMitigatedData[j].comments? assetUserInputAndMitigatedData[j].comments : [];
                                            commentsFlag = true;
                                        }
                                        if(mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag){
                                            break;
                                        }
                                    }
                                }
                                let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';
                                subdomainTableEntityObj.Importance = this.refs.dataTableViewTemplate.getImportanceComponent(subdomainTableEntityObj, importance);
                                subdomainTableEntityObj.Comments = this.refs.dataTableViewTemplate.getCommentsComponent(subdomainTableEntityObj, comments.length, comments, badgeColor);
                                subdomainTableEntityObj.Mitigated = this.refs.dataTableViewTemplate.getMitigatedComponent(subdomainTableEntityObj, mitigatedBackground);

                                subdomainTableEntityObjToDownload.Importance = generalFunctions.getImportanceStr(importance);
                                subdomainTableEntityObjToDownload.Comments = generalFunctions.allCommentsToOneStr(comments);
                                subdomainTableEntityObjToDownload.Mitigated = mitigatedBackgroundFlag ? 'Yes' : 'No';

                                tableData.push(subdomainTableEntityObj);
                                tableDataToDownload.push(subdomainTableEntityObjToDownload);
                            }
                        });
                    }
                }
            });
        }
        this.setState({
            tableData: tableData,
            tableDataToDownload: tableDataToDownload
        });

        if(this.refs && this.refs.dataTableViewTemplate){
            this.refs.dataTableViewTemplate.doAfterSetResult();
        }
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        let resultClassName = classNames('search-results-container');

        const styles = {
            cardStyle: {
                margin: '20px 10px 20px 10px'
            },
            headline: {
                fontSize: 24,
                paddingTop: 16,
                marginBottom: 12,
                fontWeight: 400
            },
            slide: {
                padding: 10
            },
            inkBar: {
                backgroundColor: '#ffffff'
            },
            tabItemContainer: {
                backgroundColor: 'rgb(48, 48, 48)'
            },
            tabItemLabel: {
                color: 'white'
            },
            emptyResultStyle: {
                backgroundColor: '#FFDD00',
                display: 'inline-block',
                padding: '1% 5%',
                marginTop: '240px',
                fontSize: 'large',
                textAlign: 'center'
            },
            wrappableStyle: {
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-word'
            },
            clickableWrappableStyle: {
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-word',
                'cursor': 'pointer'
            }
        };

        let infoText = textForTitleExpl.ssl.expl;
        let shorterInfoText = textForTitleExpl.ssl.explShort;


        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div className={pageStyleClass}>
                    <Loader show={this.state.showLoader} message={spinner}>
                        <div>
                            <div style={{margin: '40px 0px 40px 10px'}}>
                                <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                                <span style={{position: 'relative', top: '-6px', fontSize: 22}}>SSL CERTIFICATES</span>
                            </div>
                            <TitleExplanationComponent
                                openInfoBox={this.state.openInfoBox}
                                anchorEl={this.state.anchorEl}
                                shorterInfoText={shorterInfoText}
                                infoText={infoText}
                                handleCloseInfoBox={this.handleCloseInfoBox.bind(this)}
                                handleOpenInfoBox={this.handleOpenInfoBox.bind(this)}
                            />
                            <SettingsFilterTemplate
                                myDataType={SSL_CERTS_TYPE}
                                companies={this.state.companies}
                                companiesInputVal={this.state.companiesInputVal}
                                updateParentState={this.updateState}
                                setDefaultCompanies={this.setDefaultCompanies}
                                saveLoadedCompanies={this.saveLoadedCompanies}
                            />
                            <DataTableViewTemplate
                                ref="dataTableViewTemplate"
                                myDataType={SSL_CERTS_TYPE}
                                tableColumns={this.state.tableColumns}
                                tableData={this.state.tableData}
                                tableColumnsToDownload={this.state.tableColumnsToDownload}
                                tableDataToDownload={this.state.tableDataToDownload}
                                displayOnlyValid={this.state.displayOnlyValid}
                                assetUserInputAndMitigatedData={this.state.assetUserInputAndMitigatedData}
                                updateFpAndMitigatedData={this.handleUpdateFpAndMitigatedData.bind(this)}
                                recalculateScore={this.recalculateScore.bind(this)}
                                setResults={this.setResults.bind(this)}
                                updateStateForAMData={this.updateStateForAMData.bind(this)}
                            />
                        </div>
                    </Loader>
                </div>
            </MuiThemeProvider>
        );
    }
}
