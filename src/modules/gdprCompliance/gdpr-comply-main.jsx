import React from 'react';
import {
    GDPR_NOT_COMPLY_TYPE,
    GRAPH_COMPANIES_FILTER_LIMIT, ORGANIZATION_ID,
    PROJECT_NAME,
    SESSION_COMPANIES
} from '../../../config/consts';
import classNames from 'classnames';
import {getLoadedCompaniesObject, getDefaultCompanies} from '../common/datatableTemplate/datatable-template-helpers.js';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import {getKeyNestedChildsIfExists} from '../common/CommonHelpers.js';
import '../../../public/css/survey.css';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SettingsFilterTemplate from '../common/datatableTemplate/settings-filter-template-main.jsx';
import Loader from 'react-loader-advanced';
import DataTableViewTemplate from '../common/datatableTemplate/datatable-template.jsx';
import generalFunctions from "../generalComponents/general-functions";

const shortid = require('shortid');
const moment = require('moment');

const RESULT_GDPR = 'resultGDPR',
    LAST_UPDATED = 'lastUpdated',
    RELATED_COMPANIES = 'relatedCompanies',
    NO_SUBDOMAIN = 'None';
import textForTitleExpl from "../appGeneralFiles/text-for-title-explanations.js";
import TitleExplanationComponent from '../appGeneralFiles/title-explanation-component.jsx';


export default class GDPRCompliance extends React.Component {
    constructor() {
        super();

        const wrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word'
        };

        this.state = {
            showLoader: false,
            companies: [],
            companiesInputVal: [],
            tableData: [],
            tableDataToDownload: [],
            data: [],
            fullData: [],
            assetUserInputAndMitigatedData: [],
            /* Create table column headers */
            tableColumns: [
                {
                    sortable: true,
                    label: 'Domain',
                    key: 'domain',
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Subdomain',
                    key: 'subdomain',
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Cookie/Privacy Banner',
                    key: RESULT_GDPR,
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Last Updated',
                    key: LAST_UPDATED,
                    style: wrappableStyle
                }, {
                    label: 'Related Companies',
                    key: RELATED_COMPANIES,
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: true,
                    label: 'Importance',
                    key: 'Importance',
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: false,
                    label: 'Comments',
                    key: 'Comments',
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: true,
                    label: 'Mitigated',
                    key: 'Mitigated',
                    style: wrappableStyle,
                    noneSearchable: true
                }
            ],
            tableColumnsToDownload: [
                {label: 'Domain', key: 'domain'},
                {label: 'Subdomain', key: 'subdomain'},
                {label: 'Cookie/Privacy Banner', key: RESULT_GDPR},
                {label: 'Last Updated', key: LAST_UPDATED},
                {label: 'Related Companies', key: RELATED_COMPANIES},
                {label: 'Importance', key: 'Importance'},
                {label: 'Comments', key: 'Comments'},
                {label: 'Mitigated', key: 'Mitigated'},
            ],
            openInfoBox: false
        };

        this.setResults = this.setResults.bind(this);
        this.updateState = this.updateState.bind(this);
        this.setDefaultCompanies = this.setDefaultCompanies.bind(this);
        this.saveLoadedCompanies = this.saveLoadedCompanies.bind(this);
        this.recalculateScore = this.recalculateScore.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            // In case it is desired to save user preferences, use: this.getAllCompaniesByID(user);
            this.setState({user: user});
            this.getAllowedCompanies(true);
        }
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    //AMData = assetUserInput and mitigated Data
    updateStateForAMData(assetUserInputAndMitigatedData) {
        this.setState({assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
    }

    recalculateScore() {
        let data = {};

        if (this.state.companiesInputVal && this.state.companiesInputVal.length > 0) {
            data.companyIDs = [];
            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        data.orgId = localStorage.getItem(ORGANIZATION_ID);
        data.saveScoresOnDB = true;
        $.ajax({
            type: 'POST',
            url: '/api/getMaxCVSScore',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'CalculateScores');
            } else {
                app.addAlert('Error', 'Error calculating scores');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to calculate scores');
        });
    }

    getAllowedCompanies(isDefault, selectedCompanies) {
        $.ajax({
            type: 'GET',
            url: '/api/getAllCompaniesById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((companies) => {
            if (isDefault) {
                this.saveDefaultCompanies(companies);
            } else {
                this.saveLoadedCompanies(companies, selectedCompanies);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load companies.');
        });
    }

    saveDefaultCompanies(companies) {
        if (companies) {
            let pn = localStorage.getItem(PROJECT_NAME);
            if (pn) {
                $.ajax({
                    type: 'POST',
                    url: '/api/getCompaniesByProjectName',
                    async: true,
                    data: JSON.stringify({project: pn}),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((projectCompanies) => {
                    this.setDefaultCompaniesToState(companies, projectCompanies);
                }).fail(() => {
                    app.addAlert('error', 'Error: failed to load project\'s companies.');
                });
            } else {
                this.setDefaultCompaniesToState(companies, null);
            }
        }
    }

    updateState(stateObj) {
        if (stateObj) {
            this.setState(stateObj, this.getGDPRDataFindings);
        }
    }

    setDefaultCompaniesToState(companies, projectCompanies) {
        let result = getDefaultCompanies(companies, projectCompanies, 'selectedDomains');
        if (result && result.companies && result.companiesInputVal && result.companiesPropArray) {
            if (result.companiesInputVal.length > 1) {
                // If the Session has a selection saved - load it.
                let sessionCompanies = sessionStorage.getItem(SESSION_COMPANIES);
                if (sessionCompanies) {
                    let selectedNames = sessionCompanies.split(', ');
                    let relatedCompanies = result.companiesInputVal.filter((c) => {
                        return selectedNames.includes(c.companyName);
                    });
                    if (relatedCompanies && relatedCompanies.length > 0) {
                        result.companiesInputVal = relatedCompanies;
                    }
                } else {
                    // Select only the first company as a default selection.
                    result.companiesInputVal.splice(1, result.companiesInputVal.length);
                }
            }
            this.setDefaultCompanies(result);
        }
    }

    setDefaultCompanies(result) {
        if (result && result.companies && result.companiesInputVal) {
            this.setState({
                companies: result.companies,
                companiesInputVal: result.companiesInputVal
            }, this.getGDPRDataFindings);
        }
    }

    saveLoadedCompanies(companies, selectedCompanies) {
        let result = getLoadedCompaniesObject(companies, selectedCompanies, 'selectedDomains');

        if (result && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: companies,
                companiesInputVal: result.companiesInputVal,
                companiesDomains: result.companiesPropArray
            }, this.getGDPRDataFindings);
        }
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    getGDPRDataFindings() {
        if (this.state.companiesInputVal && Array.isArray(this.state.companiesInputVal) && this.state.companiesInputVal.length > 0) {
            let companyIDs = [];
            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !companyIDs.includes(currComp.id)) {
                    companyIDs.push(currComp.id);
                }
            });
            if (companyIDs.length > 0) {
                this.startLoader();
                let orgId = localStorage.getItem(ORGANIZATION_ID);

                const data = {
                    companyIDs: companyIDs,
                    orgId: orgId
                };
                $.ajax({
                    type: 'POST',
                    url: '/api/getGDPRData',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((res) => {
                    this.stopLoader();
                    let assetUserInputAndMitigatedData = [];
                    if (res && res.ok && res.data && Array.isArray(res.data)) {
                        if(res.assetUserInputAndMitigatedData){
                            assetUserInputAndMitigatedData = res.assetUserInputAndMitigatedData;
                        }
                        this.setState({fullData: res.data, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData})
                    }

                    this.setResults();
                }).fail(() => {
                    this.stopLoader();
                    app.addAlert('error', 'Error: failed to fetch data');
                });
            }
        }
    }

    add3Dots(str, limit) {
        const dots = '...';
        if (str && limit && str.length > limit) {
            str = str.substring(0, limit) + dots;
        }

        return str;
    }

    getGDPRTableText(dataValue, dataKey) {
        let text;
        if (dataKey === RESULT_GDPR) {
            text = (dataValue ? 'Comply' : 'Non Comply');
        } else if (dataKey === LAST_UPDATED) {
            text = moment(dataValue).format('YYYY-MM-DD');
        } else {
            text = '' + dataValue;
        }
        return text;
    }

    setResults() {
        const CHARS_LIMIT = 30;
        let tableData = [];
        let data = this.state.fullData || [];
        let tableDataToDownload = [];
        let assetUserInputAndMitigatedData = this.state.assetUserInputAndMitigatedData || [];
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;
        let demoIdx = 0;
        if (data && Array.isArray(data)) {
            data.map((currData) => {
                if (currData && currData.domain
                    && currData.hasOwnProperty(RESULT_GDPR)
                    && currData[RELATED_COMPANIES]
                    && Array.isArray(currData[RELATED_COMPANIES])) {
                    // Display only 'Non Comply' results
                    //if(!currData[RESULT_GDPR]) {
                    let tableEntityObj = {
                        id: currData.id,
                        surveyIdentifier: shortid.generate()
                    };

                    let tableEntityObjToDownload = {};

                    this.state.tableColumns.map((currColumn) => {
                        let currKey = currColumn.key;
                        if (currKey !== 'Importance' && currKey !== 'Comments' && currKey !== 'Mitigated') {
                            let currDataValue = getKeyNestedChildsIfExists(currData, currKey);
                            if (currDataValue != null) {
                                if (currKey === RELATED_COMPANIES) {
                                    tableEntityObj[currKey] = showExpandedDataChip(isInDemoMode? ['Company Name'] : currDataValue, 3, 5);
                                    tableEntityObjToDownload[currKey] = currDataValue.join(', ');
                                } else {
                                    let curVal = this.getGDPRTableText(currDataValue, currKey);
                                    if(isInDemoMode){
                                        if(currKey === 'domain'){
                                            curVal = 'domain-example-'+ demoIdx +'.com';
                                        }
                                    }
                                    // Apply data as string so FuzzySearch will be able to use this registry properly.
                                    tableEntityObj[currKey] = curVal;
                                    tableEntityObjToDownload[currKey] = tableEntityObj[currKey].toString();
                                }
                            } else {
                                tableEntityObj[currKey] = '';
                                tableEntityObjToDownload[currKey] = '';
                            }
                        }
                    });
                    demoIdx++;
                    let importance = 3;
                    let mitigatedBackground = 'black';
                    let comments = [];
                    let assetUserInputFlag = false;
                    let mitigatedBackgroundFlag = false;
                    let commentsFlag = false;

                    for(let j=0; j<assetUserInputAndMitigatedData.length; j++){
                        if(this.refs.dataTableViewTemplate.isCurRowNeedToBeUpdateCurNode(tableEntityObj, assetUserInputAndMitigatedData[j])){
                            if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'assetUserInput'){
                                importance = assetUserInputAndMitigatedData[j].importance? Number(assetUserInputAndMitigatedData[j].importance) : 3;
                                assetUserInputFlag = true;
                            }
                            if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'mitigated'){
                                mitigatedBackground = '#5bd25e';
                                mitigatedBackgroundFlag = true;
                            }
                            if(assetUserInputAndMitigatedData[j].comments){
                                comments = assetUserInputAndMitigatedData[j].comments? assetUserInputAndMitigatedData[j].comments : [];
                                commentsFlag = true;
                            }
                            if(mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag){
                                break;
                            }
                        }
                    }
                    let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';
                    tableEntityObj.Importance = this.refs.dataTableViewTemplate.getImportanceComponent(tableEntityObj, importance);
                    tableEntityObj.Comments = this.refs.dataTableViewTemplate.getCommentsComponent(tableEntityObj, comments.length, comments, badgeColor);
                    tableEntityObj.Mitigated = this.refs.dataTableViewTemplate.getMitigatedComponent(tableEntityObj, mitigatedBackground);

                    tableEntityObjToDownload.Importance = generalFunctions.getImportanceStr(importance);
                    tableEntityObjToDownload.Comments = generalFunctions.allCommentsToOneStr(comments);
                    tableEntityObjToDownload.Mitigated = mitigatedBackgroundFlag ? 'Yes' : 'No';
                    // Initialize main domain' subdomain value to None.
                    tableEntityObj.subdomain = NO_SUBDOMAIN;
                    tableEntityObjToDownload.subdomain = NO_SUBDOMAIN;

                    tableData.push(tableEntityObj);
                    tableDataToDownload.push(tableEntityObjToDownload);
                    //}

                    // Add results also for subdomains.
                    if (currData.subdomains && Array.isArray(currData.subdomains)) {
                        currData.subdomains.map((currSubdomain) => {
                            if (currSubdomain
                                && currSubdomain.subdomain
                                && currSubdomain.hasOwnProperty(RESULT_GDPR)) {
                                // Display only 'Non Comply' results
                                //if(!currSubdomain[RESULT_GDPR]) {
                                let subdomainTableEntityObj = {
                                    surveyIdentifier: shortid.generate(),
                                    domain: isInDemoMode? 'domain-example-'+ demoIdx +'.com' :  currData.domain,
                                    subdomain: isInDemoMode? 'subdomain-example-'+ demoIdx +'.com' : this.add3Dots(currSubdomain.subdomain, CHARS_LIMIT)
                                };

                                let subdomainTableEntityObjToDownload = {
                                    domain: isInDemoMode? 'domain-example-'+ demoIdx +'.com' : currData.domain,
                                    subdomain: isInDemoMode? 'subdomain-example-'+ demoIdx +'.com' : currSubdomain.subdomain
                                };

                                subdomainTableEntityObj[LAST_UPDATED] = subdomainTableEntityObjToDownload[LAST_UPDATED] = this.getGDPRTableText(currData[LAST_UPDATED], LAST_UPDATED);

                                // Add relatedCompanies manually from parent object.
                                subdomainTableEntityObj[RELATED_COMPANIES] = showExpandedDataChip(isInDemoMode? ['Company Name'] : currData[RELATED_COMPANIES], 3, 5);
                                subdomainTableEntityObjToDownload[RELATED_COMPANIES] = isInDemoMode? 'Company Name' : currData[RELATED_COMPANIES].join(', ');

                                this.state.tableColumns.map((currColumn) => {
                                    let currKey = currColumn.key;
                                    if (currKey !== 'Importance' && currKey !== 'Comments' && currKey !== 'Mitigated') {
                                        // Add only keys that weren't added manually before.
                                        if (!subdomainTableEntityObj.hasOwnProperty(currKey)) {
                                            let currDataValue = getKeyNestedChildsIfExists(currSubdomain, currKey);
                                            if (currDataValue != null) {
                                                let curVal = this.getGDPRTableText(currDataValue, currKey);
                                                if(isInDemoMode){
                                                    if(currKey === 'domain'){
                                                        curVal = 'domain-example-'+ demoIdx +'.com';
                                                    }
                                                }
                                                // Apply data as string so FuzzySearch will be able to use this registry properly.
                                                subdomainTableEntityObj[currKey] = curVal;
                                                subdomainTableEntityObjToDownload[currKey] = subdomainTableEntityObj[currKey].toString();
                                            } else {
                                                subdomainTableEntityObj[currKey] = '';
                                                subdomainTableEntityObjToDownload[currKey] = '';
                                            }
                                        }
                                    }
                                });
                                demoIdx++;
                                let importance = 3;
                                let mitigatedBackground = 'black';
                                let comments = [];
                                let assetUserInputFlag = false;
                                let mitigatedBackgroundFlag = false;
                                let commentsFlag = false;

                                for(let j=0; j<assetUserInputAndMitigatedData.length; j++){
                                    if(this.refs.dataTableViewTemplate.isCurRowNeedToBeUpdateCurNode(tableEntityObj, assetUserInputAndMitigatedData[j])){
                                        if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'assetUserInput'){
                                            importance = assetUserInputAndMitigatedData[j].importance? Number(assetUserInputAndMitigatedData[j].importance) : 3;
                                            assetUserInputFlag = true;
                                        }
                                        if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'mitigated'){
                                            mitigatedBackground = '#5bd25e';
                                            mitigatedBackgroundFlag = true;
                                        }
                                        if(assetUserInputAndMitigatedData[j].comments){
                                            comments = assetUserInputAndMitigatedData[j].comments? assetUserInputAndMitigatedData[j].comments : [];
                                            commentsFlag = true;
                                        }
                                        if(mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag){
                                            break;
                                        }
                                    }
                                }
                                let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';
                                subdomainTableEntityObj.Importance = this.refs.dataTableViewTemplate.getImportanceComponent(tableEntityObj, importance);
                                subdomainTableEntityObj.Comments = this.refs.dataTableViewTemplate.getCommentsComponent(tableEntityObj, comments.length, comments, badgeColor);
                                subdomainTableEntityObj.Mitigated = this.refs.dataTableViewTemplate.getMitigatedComponent(tableEntityObj, mitigatedBackground);

                                subdomainTableEntityObjToDownload.Importance = generalFunctions.getImportanceStr(importance);
                                subdomainTableEntityObjToDownload.Comments = generalFunctions.allCommentsToOneStr(comments);
                                subdomainTableEntityObjToDownload.Mitigated = mitigatedBackgroundFlag ? 'Yes' : 'No';

                                tableData.push(subdomainTableEntityObj);
                                tableDataToDownload.push(subdomainTableEntityObjToDownload);
                            }
                        });
                    }
                }
            });
        }
        this.setState({
            tableData: tableData,
            tableDataToDownload: tableDataToDownload
        });

        if(this.refs && this.refs.dataTableViewTemplate){
            this.refs.dataTableViewTemplate.doAfterSetResult();
        }
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        let resultClassName = classNames('search-results-container');

        const styles = {
            cardStyle: {
                margin: '20px 10px 20px 10px',
            },
            headline: {
                fontSize: 24,
                paddingTop: 16,
                marginBottom: 12,
                fontWeight: 400
            },
            slide: {
                padding: 10
            },
            inkBar: {
                backgroundColor: '#18FFFF'
            },
            tabItemContainer: {
                backgroundColor: 'rgb(48, 48, 48)'
            },
            tabItemLabel: {
                color: 'white'
            },
            emptyResultStyle: {
                backgroundColor: '#FFDD00',
                display: 'inline-block',
                padding: '1% 5%',
                marginTop: '240px',
                fontSize: 'large',
                textAlign: 'center'
            },
            wrappableStyle: {
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-word'
            },
            clickableWrappableStyle: {
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-word',
                'cursor': 'pointer'
            }
        };

        let infoText = textForTitleExpl.gdpr.expl;
        let shorterInfoText = textForTitleExpl.gdpr.explShort;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <div className={pageStyleClass}>
                    <Loader show={this.state.showLoader} message={spinner}>
                        <div>
                            <div style={{margin: '40px 0px 40px 10px'}}>
                                <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                                <span
                                    style={{position: 'relative', top: '-6px', fontSize: 22}}>WEB PRIVACY POLICY</span>
                            </div>
                            <TitleExplanationComponent
                                openInfoBox={this.state.openInfoBox}
                                anchorEl={this.state.anchorEl}
                                shorterInfoText={shorterInfoText}
                                infoText={infoText}
                                handleCloseInfoBox={this.handleCloseInfoBox.bind(this)}
                                handleOpenInfoBox={this.handleOpenInfoBox.bind(this)}
                            />
                            <SettingsFilterTemplate
                                myDataType={GDPR_NOT_COMPLY_TYPE}
                                companies={this.state.companies}
                                companiesInputVal={this.state.companiesInputVal}
                                updateParentState={this.updateState}
                                setDefaultCompanies={this.setDefaultCompanies}
                                saveLoadedCompanies={this.saveLoadedCompanies}
                            />
                            <DataTableViewTemplate
                                ref={"dataTableViewTemplate"}
                                myDataType={GDPR_NOT_COMPLY_TYPE}
                                tableColumns={this.state.tableColumns}
                                tableData={this.state.tableData}
                                tableColumnsToDownload={this.state.tableColumnsToDownload}
                                tableDataToDownload={this.state.tableDataToDownload}
                                assetUserInputAndMitigatedData={this.state.assetUserInputAndMitigatedData}
                                recalculateScore={this.recalculateScore.bind(this)}
                                setResults={this.setResults.bind(this)}
                                updateStateForAMData={this.updateStateForAMData.bind(this)}
                            />
                        </div>
                    </Loader>
                </div>
            </MuiThemeProvider>
        );
    }
}
