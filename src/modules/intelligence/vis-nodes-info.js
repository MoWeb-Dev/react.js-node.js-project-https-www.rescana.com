module.exports = {
    nodeTypeIP: 'IP',
    nodeTypeASN: 'ASN',
    nodeTypeCVE: 'CVE',
    nodeTypePort: 'port',
    nodeTypeHostname: 'hostname',
    nodeTypeDomain: 'domain',
    nodeTypeSubdomain: 'subdomain',
    descNamesForIP: {
        'ip_str': 'IP address',
        'last_update': 'Last Update',
        'org': 'Organization',
        'hostname': 'Hostname',
        'tags': 'Class',
        'hostnames': 'Hostnames'
    },
    networkNamesForIP: {
        'asn': 'ASN',
        'as_owner': 'ASN Owner',
        'range': 'ASN Range',
        'registrar': 'ASN Registrar',
        'isp': 'ISP',
        'os': 'Operating System'
    },
    geoLocationNamesForIP: {
        'country_name': 'Country',
        'country_code': 'Country Code',
        'latitude': 'Latitude',
        'longitude': 'Longitude',
        'area_code': 'Area Code',
        'postal_code': 'Postal Code',
        'city': 'City'
    },
    descNamesForASN: {
        'asn': 'ASN',
        'as_owner': 'ASN Owner',
        'range': 'Range',
        'registrar': 'Registrar',
        'description': 'Description',
        'countryCode': 'Country Code'
    },
    descNamesForDomain: {
        'whois': 'WHOIS'
    },
    descNamesForCVE: {
        'id': 'CVE',
        'score': 'CVE Score',
        'summary': 'CVE Summary',
        'impact': 'Impact'
    },
    descNamesForPort: {
        'data': 'Banner',
        'product': 'Product',
        'version': 'Product Version',
        'transport': 'Transport'
    },
    knownUsageNamesForPort: {
        'name': 'Name',
        'description': 'Description'
    }
};
