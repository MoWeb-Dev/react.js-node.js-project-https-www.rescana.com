import React from 'react';
import vis from 'vis-network/dist/vis-network.js';
import async from 'async';
import CircularProgress from 'material-ui/CircularProgress';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {PROJECT_NAME} from '../../../config/consts';
import Drawer from 'material-ui/Drawer';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import NavClose from 'material-ui/svg-icons/navigation/chevron-right';
import moment from 'moment';
import '../../../public/css/main.css';
import themeForFont from "../app/themes/themeForFont";
import generalFunctions from "../generalComponents/general-functions";

import {
    CVE_LOW_MAX,
    CVE_MEDIUM_MIN,
    CVE_MEDIUM_MAX,
    CVE_HIGH_MIN,
    CVE_HIGH_MAX,
    CVE_CRITICAL_MIN,
    CVE_CRITICAL_MAX
} from '../../../config/consts';
import {
    nodeTypeIP,
    nodeTypeASN,
    nodeTypeHostname,
    nodeTypeDomain,
    nodeTypeSubdomain,
    nodeTypeCVE,
    nodeTypePort
} from './vis-nodes-info';
import {
    descNamesForIP,
    networkNamesForIP,
    geoLocationNamesForIP,
    descNamesForASN,
    descNamesForCVE,
    descNamesForDomain,
    descNamesForPort,
    knownUsageNamesForPort
} from './vis-nodes-info';
import {isDomainNotSubdomain, fixTimezoneRange} from '../common/CommonHelpers';
import {getScoreColor} from '../common/UI-Helpers.jsx';

export class VisNetwork extends React.Component {
    constructor() {
        super();
        this.state = {
            showLoader: false
        };

        this.openNodeInfoTag = this.openNodeInfoTag.bind(this);
    }

    componentDidMount() {
        this.closeNodeInfo();
    }

    setLoader(state) {
        this.setState({
            showLoader: state
        });
    }

    setResults(data) {
        let isInDemoMode = this.props.user && this.props.user.isInDemoMode;
        let nodeArr = [], edgeArr = [], nodeIdArr = [], edgeIdArr = [], asnIdArr = [];
        let idx = 0;
        const DEFAULT_CVE_KEYS = ['cve', 'score'];

        async.each(data, (obj, cb) => {
            async.forEachOf(obj, (value, key, callback) => {
                    if (Array.isArray(value)) {
                        async.each(value, (item, done) => {
                                // In case it's an edge
                                if ((item.start && item.start.low) || (item.end && item.end.low)) {
                                    if (!edgeIdArr.includes(item.identity.low)) {
                                        edgeArr.push({label: item.type, from: item.start.low, to: item.end.low});
                                        edgeIdArr.push(item.identity.low);
                                    }
                                } else {
                                    // Prevent duplicates (item.identity.low == id)
                                    if (!nodeIdArr.includes(item.identity.low)) {
                                        let prop = VisNetwork.assignLabel(item.properties, isInDemoMode, idx);
                                        let nodeObj = {label: prop, id: item.identity.low, props: item.properties};
                                        nodeObj = VisNetwork.addColor(nodeObj, item);
                                        nodeArr.push(nodeObj);
                                        nodeIdArr.push(item.identity.low);

                                        // Create an ASN Array for clustering.
                                        if (item.properties.name === 'ASN') {
                                            asnIdArr.push({id: item.identity.low, name: item.properties.finding});
                                        }
                                    }
                                }
                            idx++;
                                done();
                            }, () => {
                                callback();
                            }
                        );
                    } else {
                        // In case it's an edge
                        if ((value.start && value.start.low) || (value.end && value.end.low)) {
                            if (!edgeIdArr.includes(value.identity.low)) {
                                edgeArr.push({label: value.type, from: value.start.low, to: value.end.low});
                                edgeIdArr.push(value.identity.low);
                            }
                        } else {
                            if (!nodeIdArr.includes(value.identity.low)) {
                                let prop = VisNetwork.assignLabel(value.properties, isInDemoMode, idx);
                                let nodeObj = {label: prop, id: value.identity.low, props: value.properties};
                                nodeObj = VisNetwork.addColor(nodeObj, value);
                                nodeArr.push(nodeObj);
                                nodeIdArr.push(value.identity.low);

                                // Create an ASN Array for clustering.
                                if (value.properties.name === 'ASN') {
                                    asnIdArr.push({id: item.identity.low, name: value.properties.finding});
                                }
                            }
                        }
                        idx++;
                        callback();
                    }
                }, (err) => {
                    if (err) console.log(err);
                    cb();
                }
            );
        });

        let nodes = new vis.DataSet(nodeArr);
        // create an array with edges
        let edges = new vis.DataSet(edgeArr);

        let nodesAndEdges = {
            nodes: nodes,
            edges: edges
        };
        let options = {
            layout: {
                improvedLayout: false
            },
            edges: {
                arrows: {
                    to: {
                        scaleFactor: 0.5
                    }
                },
                smooth: {
                    enabled: true,
                    type: 'horizontal',
                    roundness: 0.3
                },
                arrowStrikethrough: false,
                width: 0.8,
                hoverWidth: 0.3,
                font: {
                    size: 8,
                    color: '#696867',
                    align: 'middle',
                    strokeWidth: 2
                },
                shadow: false,
                color: {
                    color: '#bab8b7'
                }
            },
            nodes: {
                shadow: false,
                shape: 'circle',
                labelHighlightBold: false,
                color: {
                    background: '#adadad',
                    border: '#757575',
                    hover: {
                        background: '#abbade',
                        border: '#758ae0'
                    },
                    highlight: {
                        background: '#abbade',
                        border: '#758ae0'
                    }

                },
                font: {
                    color: '#ffffff',
                    size: 6
                },
                widthConstraint: {
                    minimum: 25,
                    maximum: 90
                }
            },
            interaction: {
                hover: true,
                hoverConnectedEdges: false,
                hideEdgesOnDrag: true,
                navigationButtons: false,
                keyboard: true
            },
            physics: {
                stabilization: {
                    enabled: true,
                    iterations: 120,
                    updateInterval: 50,
                    fit: true
                },
                timestep: 0.5,
                adaptiveTimestep: true,
                repulsion: {
                    centralGravity: 0.1,
                    springLength: 120,
                    springConstant: 0.095,
                    nodeDistance: 120
                },
                minVelocity: 0.1,
                solver: 'repulsion'
            }
        };

        let network = new vis.Network(this.refs.myRef, nodesAndEdges, options);

        let clusterOptions = {
            joinCondition: function (nodeOptions) {
                if (nodeOptions.props.cluster) {
                    return nodeOptions.props.cluster === 'spfCluster' || nodeOptions.props.cluster === 'dmarcCluster';
                }
            },
            processProperties: function (clusterOptions) {
                clusterOptions.label = 'spf data';
                clusterOptions.icon = {
                    face: 'Ionicons',
                    code: '\uf229',
                    size: 50,
                    color: '#f0a30a'
                };
                return clusterOptions;
            },
            clusterNodeProperties: {id: 'cluster', borderWidth: 1, shape: 'icon'}
        };

        network.clustering.cluster(clusterOptions);


        asnIdArr = [...new Set(asnIdArr)];


        for (let i = 0; i < asnIdArr.length; i++) {
            let clusterASNOptions = {
                joinCondition: function (parentNodeOptions, childNodeOptions) {
                    if (parentNodeOptions.id === asnIdArr[i].id) {
                        return true;
                    }
                },
                processProperties: function (clusterASNOptions, parentNodeOptions, childNodeOptions) {
                    clusterASNOptions.label = asnIdArr[i].name;
                    clusterASNOptions.icon = {
                        face: 'Ionicons',
                        code: '\uf341',
                        size: 70,
                        color: '#743ef0'
                    };
                    return clusterASNOptions;
                },
                clusterNodeProperties: {
                    id: 'asn-cluster' + asnIdArr[i].id,
                    font: {
                        color: '#696867',
                        size: 10
                    },
                    borderWidth: 1,
                    label: asnIdArr[i].name,
                    shape: 'icon'
                }
            };

            network.clusterByConnection(asnIdArr[i].id, clusterASNOptions);
        }


        network.on('hoverNode', (params) => {
            let isInDeomMode = this.props.user && this.props.user.isInDemoMode;
                let obj = nodeArr.find(findbyId, params.node);
                if (obj) {
                    let objProps;
                    // If this is CVE node - display only default props.
                    if(obj.props.cve) {
                        objProps = {};
                        DEFAULT_CVE_KEYS.map((currDefaultKey) => {
                            objProps[currDefaultKey] = obj.props[currDefaultKey];
                        });
                        objProps = JSON.stringify(objProps);
                        objProps = objProps.replace(/{/g, '').replace(/}/g, '').replace(/"/g, '');
                    }
                    else {
                        if(isInDeomMode && obj.label && obj.props) {
                            if (obj.props.hostname) {
                                objProps = JSON.stringify({hostname: obj.label});
                            } else if (obj.props.address) {
                                objProps = JSON.stringify({address: obj.label});
                            } else if (obj.props.bucket) {
                                objProps = JSON.stringify({bucket: obj.label});
                            } else if (obj.props.IP) {
                                objProps = JSON.stringify({IP: generalFunctions.createIpStrForDemoMode(obj.props.IP)});
                            } else {
                                objProps = JSON.stringify(obj.props);
                            }
                        } else {
                            objProps = JSON.stringify(obj.props);
                        }
                        objProps = objProps.replace(/{/g, '').replace(/}/g, '').replace(/"/g, '');
                    }
                    this.props.setInfoCard(objProps);
                }

                // If IP, ASN, Hostname, Port or CVE - change cursor to pointer.
                if (params.node && ((params.node === 'cluster' || params.node.toString().indexOf('asn-cluster') !== -1) ||
                    (nodeArr.find((n) => {
                        return (n.id && n.id === params.node && n.props &&
                            (n.props.address || (n.props.name && n.props.name === 'ASN') || n.props.hostname || n.props.cve || n.props.ports));
                    })))) {
                    network.canvas.body.container.style.cursor = 'pointer';
                } else {
                    network.canvas.body.container.style.cursor = 'no-drop';
                }
            }
        );

        network.on('blurNode', function () {
            network.canvas.body.container.style.cursor = 'default';
        });

        network.on('stabilizationProgress', () => {
            this.setState({
                showLoader: true
            });
        });

        network.once('stabilizationIterationsDone', () => {
            setTimeout(() => {
                this.setState({
                    showLoader: false
                });

                let fitOption = {
                    nodes: nodes.getIds()// nodes is type of vis.DataSet contains all the nodes
                };
                network.fit(fitOption);
            }, 500);
        });

        network.on('selectNode', (params) => {
            if (params.nodes.length === 1) {
                if (network.isCluster(params.nodes[0]) === true) {
                    network.openCluster(params.nodes[0]);
                } else if (params.nodes[0]) {
                    let nodeType = '';
                    let currNode = nodeArr.find((n) => {
                        let isFoundAndPermitted = false;
                        // Find the selected node by id.
                        if (n.id && n.id === params.nodes[0] && n.props) {
                            // Check if the node is from IP type, if so - get it's other data.
                            // If not - you can add here other permitted types. (type should be with unique props for identification).
                            if (n.props.address) {
                                nodeType = nodeTypeIP;
                                isFoundAndPermitted = true;
                            } else if (n.props.name && n.props.name === 'ASN') {
                                nodeType = nodeTypeASN;
                                isFoundAndPermitted = true;
                            } else if (n.props.hostname) {
                                nodeType = nodeTypeHostname;
                                isFoundAndPermitted = true;
                            } else if (n.props.cve) {
                                nodeType = nodeTypeCVE;
                                isFoundAndPermitted = true;
                            } else if (n.props.ports) {
                                nodeType = nodeTypePort;
                                isFoundAndPermitted = true;
                            }
                        }
                        return (isFoundAndPermitted);
                    });
                    // If the node is from a permitted type - get it's info.
                    if (currNode) {
                        let shouldGetNodeNearestInfo = true;
                        if(nodeType === nodeTypeCVE && currNode.props && typeof currNode.props === 'object') {
                            // If cve has extra data as properties - open in side window, otherwise fetch data as before.
                            const cveKeys = Object.keys(currNode.props);

                            // If this is the new CVE format (data as properties) - fix NodeInfoTag.
                            if(cveKeys && cveKeys.length > DEFAULT_CVE_KEYS.length) {
                                const data = cveKeys.map((currKey) => {
                                    // Add all props to NodeInfoTag.
                                    return {name: currKey, finding: currNode.props[currKey]};
                                });

                                this.openNodeInfoTag(currNode.props.cve, nodeType, data);

                                shouldGetNodeNearestInfo = false;
                            }
                        }
                        if(shouldGetNodeNearestInfo) {
                            // Get the currentNode's other data and open it in side menu.
                            let projectName = localStorage.getItem(PROJECT_NAME);
                            if (projectName) {
                                let datesRange = this.props.datesRange;
                                let data = {
                                    nodeID: params.nodes[0],
                                    nodeType: nodeType,
                                    project: projectName
                                };
                                if (currNode.props && currNode.props.ports) {
                                    data.portNumber = currNode.props.ports;
                                }
                                if (datesRange) {
                                    let splitDates = datesRange.split(this.props.datesRangeSeparator || ' - ');
                                    if (splitDates.length === 2) {
                                        try {
                                            let startDate = moment(splitDates[0]).format();
                                            let endDate = moment(splitDates[1]).format();
                                            if (startDate && endDate) {
                                                data.startDate = fixTimezoneRange(startDate, true);
                                                data.endDate = fixTimezoneRange(endDate, false);
                                            }
                                        } catch (e) {
                                            console.log('Failed to load selected dates: ', e);
                                        }
                                    }
                                }

                                $.ajax({
                                    type: 'POST',
                                    url: '/api/getNodeNearestInfo',
                                    data: JSON.stringify(data),
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    async: true
                                }).done((data) => {
                                    if (data && Array.isArray(data)) {
                                        if (nodeType === 'hostname') {
                                            // Call a function to separate domain and subdomain.
                                            if (isDomainNotSubdomain(currNode.props.hostname)) {
                                                nodeType = nodeTypeDomain;
                                            } else {
                                                nodeType = nodeTypeSubdomain;
                                            }
                                        }
                                        // For nodeType "IP" - address is assigned as nodeLabel.
                                        // For nodeType "ASN" - finding is assigned as nodeLabel.
                                        // For nodeType "CVE" - cve is assigned as nodeLabel.
                                        // For nodeType "port" - ports is assigned as nodeLabel.
                                        // For nodeType "domain & subdomain" - hostname is assigned as nodeLabel.
                                        this.openNodeInfoTag(currNode.props.address
                                            || currNode.props.finding
                                            || currNode.props.cve
                                            || currNode.props.ports
                                            || currNode.props.hostname, nodeType, data);
                                    } else {
                                        console.log('Failed to get node info');
                                    }
                                }).fail(() => {
                                    console.log('Failed to get node info');
                                });
                            }
                        }
                    }
                }
            }
        });

        // The second param in find, substitutes this
        function findbyId(item) {
            return item.id === this;
        }
    }

    openNodeInfoTag(nodeLabel, nodeType, extraInfo) {
        if (nodeType && extraInfo) {
            let resultInfo = [];
            if (extraInfo.length > 0) {
                let desc = [];
                let net = [];
                let geo = [];

                extraInfo.map((info) => {
                    let descPermittedNames;
                    let netPermittedNames;

                    if (nodeType === nodeTypeIP) {
                        descPermittedNames = descNamesForIP;
                        netPermittedNames = networkNamesForIP;
                    } else if (nodeType === nodeTypeASN) {
                        descPermittedNames = descNamesForASN;
                    } else if (nodeType === nodeTypeCVE) {
                        descPermittedNames = descNamesForCVE;
                    } else if (nodeType === nodeTypePort) {
                        descPermittedNames = descNamesForPort;
                        netPermittedNames = knownUsageNamesForPort;

                        // nodeType === "domain" || nodeType === "subdomain"
                    } else {
                        descPermittedNames = descNamesForDomain;
                    }

                    // This section checks if current node's info belongs to descPermittedNames' keys.
                    let objKey = Object.keys(descPermittedNames).find((key) => {
                        return key === info.name;
                    });
                    if (objKey) {
                        if (nodeType === nodeTypeIP && info.name === 'last_update' && info.finding) {
                            let relativeTime;
                            try {
                                relativeTime = moment(info.finding).format('LLL');
                                if (relativeTime) {
                                    info.finding = relativeTime;
                                }
                            } catch (e) {
                                console.log('Failed to stringify Last Update: ', e);
                            }

                            // If "Last Update" already exist - check if this finding is latest updated - and change it.
                            let existingLastUpdate = desc.find((n) => {
                                return n.name === descPermittedNames[objKey];
                            });

                            if (!existingLastUpdate) {
                                desc.push({name: descPermittedNames[objKey], finding: info.finding});
                            } else if (relativeTime) {
                                let newTime = moment(relativeTime);
                                if (newTime.isAfter(moment(existingLastUpdate.finding))) {
                                    existingLastUpdate.finding = newTime.format('LLL');
                                }
                            }
                        } else if (nodeType === nodeTypeASN && info.name === 'asn') {
                            if (!desc.find((n) => {
                                return n.name === descPermittedNames[objKey];
                            })) {
                                if (!info.finding.startsWith('AS') && !info.finding.startsWith('as')) {
                                    info.finding = 'AS' + info.finding;
                                }
                                desc.push({name: descPermittedNames[objKey], finding: info.finding});
                            }
                        } else if ((nodeType === nodeTypeDomain || nodeType === nodeTypeSubdomain) && info.name === 'whois') {
                            if (!desc.find((n) => {
                                return n.name === descPermittedNames[objKey];
                            })) {
                                desc.push({name: descPermittedNames[objKey], finding: '\n' + info.finding});
                            }
                        } else if (nodeType === nodeTypeCVE && info.name === 'impact') {
                            if (!desc.find((n) => {
                                return n.name === descPermittedNames[objKey];
                            })) {
                                desc.push({name: descPermittedNames[objKey], finding: '\n\t' + info.finding});
                            }
                        } else if (nodeType === nodeTypePort && info.name === 'data') {
                            desc.push({name: descPermittedNames[objKey], finding: '\n' + info.finding});
                        } else {
                            desc.push({name: descPermittedNames[objKey], finding: info.finding});
                        }

                        // Only nodeTypes "IP" and "port" has second title.
                    } else if (nodeType === nodeTypeIP || nodeType === nodeTypePort) {
                        // This section checks if current node's info belongs to netPermittedNames' keys.
                        objKey = Object.keys(netPermittedNames).find((key) => {
                            return key === info.name;
                        });
                        if (objKey) {
                            // Check if asn already exists.
                            if (info.name === 'asn' && info.finding) {
                                if (!net.find((n) => {
                                    return n.name === netPermittedNames[objKey];
                                })) {
                                    if (!info.finding.startsWith('AS') && !info.finding.startsWith('as')) {
                                        info.finding = 'AS' + info.finding;
                                    }
                                    net.push({name: netPermittedNames[objKey], finding: info.finding});
                                }
                            } else {
                                net.push({name: netPermittedNames[objKey], finding: info.finding});
                            }

                            // Only nodeType "IP" has third title.
                        } else if (nodeType === nodeTypeIP) {
                            // This section checks if current node's info belongs to geoLocationNamesForIP' keys.
                            objKey = Object.keys(geoLocationNamesForIP).find((key) => {
                                return key === info.name;
                            });
                            if (objKey) {
                                geo.push({name: geoLocationNamesForIP[objKey], finding: info.finding});
                            }
                        }
                    }
                });

                // For all nodeTypes - default title is Description.
                const defaultTitle = 'Description';

                if (nodeType === nodeTypeIP) {
                    // for IP there are desc, net and geo titles.
                    resultInfo = this.addDataByTitles(resultInfo, desc, defaultTitle, net, 'Network', geo, 'GEO Location');
                } else if (nodeType === nodeTypePort) {
                    // for Port there are desc and net titles.
                    resultInfo = this.addDataByTitles(resultInfo, desc, defaultTitle, net, 'Known Usage For Port', null, null);

                    // nodeType === "ASN" || nodeType === "CVE" || nodeType === "domain" || nodeType === "subdomain"
                } else {
                    // For other nodeTypes there is only desc title.
                    resultInfo = this.addDataByTitles(resultInfo, desc, defaultTitle, null, null, null, null);
                }

                // If any data is shown - check if defaultProp exist - if not then add it at first position.
                if (resultInfo.length > 0) {
                    let defaultProp;
                    if (nodeType === nodeTypeIP) {
                        defaultProp = 'IP address';
                    } else if (nodeType === nodeTypeASN) {
                        defaultProp = 'ASN';
                    } else if (nodeType === nodeTypeCVE) {
                        defaultProp = 'CVE';
                    } else if (nodeType === nodeTypePort) {
                        defaultProp = 'Port';

                        // nodeType === "domain" || nodeType === "subdomain"
                    } else {
                        defaultProp = 'Hostname';
                    }

                    resultInfo = this.addDefaultProp(defaultProp, defaultTitle, nodeLabel, desc, resultInfo);
                }
            }
            // Save the results and re-render to open the Info Drawer.
            this.setState({nodeExtraInfo: {type: nodeType, info: resultInfo, nodeDefaultLabel: nodeLabel}});
        }
    }

    // This function is called for adding a default property while opening NodeInfoTag.
    addDefaultProp(defaultProp, defaultTitle, nodeLabel, propArray, resultInfo) {
        if (!propArray.find((n) => {
            return n.name === defaultProp;
        })) {
            let defaultObj = {name: defaultProp, finding: nodeLabel};
            if (propArray.length === 0) {
                propArray.push(defaultObj);
                resultInfo = [{title: defaultTitle, data: propArray}].concat(resultInfo);
            } else {
                propArray = [defaultObj].concat(propArray);
                let objectToUpdate = resultInfo.find((info) => {
                    return info.title === defaultTitle;
                });
                if (objectToUpdate) {
                    objectToUpdate.data = propArray;
                }
            }
        }

        return resultInfo;
    }

    // This function is called for separating the data to titles while opening NodeInfoTag.
    addDataByTitles(resultInfo, defaultData, defaultTitle, secData, secTitle, trdData, trdTitle) {
        if (!resultInfo || !Array.isArray(resultInfo)) {
            resultInfo = [];
        }
        if (defaultTitle && defaultData && Array.isArray(defaultData) && defaultData.length > 0) {
            resultInfo.push({title: defaultTitle, data: defaultData});
        }
        if (secTitle && secData && Array.isArray(secData) && secData.length > 0) {
            resultInfo.push({title: secTitle, data: secData});
        }
        if (trdTitle && trdData && Array.isArray(trdData) && trdData.length > 0) {
            resultInfo.push({title: trdTitle, data: trdData});
        }
        return resultInfo;
    }

    closeNodeInfo() {
        if (this.state.nodeExtraInfo) {
            this.setState({nodeExtraInfo: null});
        }
    }

    getFindings(isInDemoMode = false, info, curHostname) {
        let returnVal = info && info.finding? info.finding : '';
        if (isInDemoMode) {
            //if we in demo mode we want to remove all ips and hostname from the data(here we replace them)
            if (info && info.finding && info.name && info.name.toLowerCase() !== 'cve' && info.name.toLowerCase() !== 'cpe') {
                    let re = new RegExp(curHostname, 'g');
                    let ipPattern = /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/g;
                    if (typeof info.finding === 'object' || Array.isArray(info.finding)) {
                        returnVal = JSON.stringify(info.finding);
                        if(curHostname && curHostname !== ''){
                            returnVal = returnVal.toString().toLowerCase().replace(re, 'xxxxx.com ');
                        }
                        returnVal = returnVal.toString().toLowerCase().replace(ipPattern,  'xx.xx.xx.xx');
                    } else {
                        if(curHostname && curHostname !== ''){
                            returnVal = info.finding.toString().toLowerCase().replace(re,  'xxxxx.com ');
                        }
                        returnVal = returnVal.toString().toLowerCase().replace(ipPattern,  'xx.xx.xx.xx');
                    }
            }
        }
        return returnVal;
    }

    static assignLabel(props, isInDemoMode, idx = 0) {
        let prop;
        for (let key in props) {
            if (key !== 'id') {
                if (props.hasOwnProperty(key)) {
                    prop = VisNetwork.importantKey(props);
                    // If no special keyword found, get the first key and present it's value.
                    if (!prop) {
                        prop = props[key];
                    }
                    if (prop.length > 10) {
                        prop = prop.substring(0, 10) + '...';
                    }
                    if(isInDemoMode){
                        if(key === 'hostname'){
                            prop = 'domain'+ idx + '.com';
                        } else if(key ==='address'){
                            prop = generalFunctions.createIpStrForDemoMode(props[key], idx)
                        } else if(key ==='bucket'){
                            prop = 'bucket-'+ idx;
                        }
                    }
                    return prop;
                }
            }
        }
    }

    static addColor(nodeObj, item) {
        if (item.properties.cve) {
            if (item.properties.score >= CVE_CRITICAL_MIN && item.properties.score <= CVE_CRITICAL_MAX) {
                nodeObj.color = {
                    background: '#212121',
                    border: '#424242',
                    highlight: {background: '#424242', border: '#616161'},
                    hover: {background: '#424242', border: '#616161'}
                };
                nodeObj.font = {color: '#FFFFFF', size: 6};
            }

            if ((item.properties.score >= CVE_HIGH_MIN && item.properties.score < CVE_HIGH_MAX) || !item.properties.score) {
                nodeObj.color = {
                    background: '#D92E2E',
                    border: '#ED5555',
                    highlight: {background: '#FF8B8B', border: '#FFB5B5'},
                    hover: {background: '#FF8B8B', border: '#FFB5B5'}
                };
                nodeObj.font = {color: '#FFFFFF', size: 6};
            }

            if (item.properties.score >= CVE_MEDIUM_MIN && item.properties.score < CVE_MEDIUM_MAX) {
                nodeObj.color = {
                    background: '#FF5722',
                    border: '#FF7043',
                    highlight: {background: '#FF7043', border: '#FF8A65'},
                    hover: {background: '#FF7043', border: '#FF8A65'}
                };
                nodeObj.font = {color: '#FFFFFF', size: 6};
            }

            if (item.properties.score < CVE_LOW_MAX) {
                nodeObj.color = {
                    background: '#FFA000',
                    border: '#FFB300',
                    highlight: {background: '#FFCA28', border: '#FFD54F'},
                    hover: {background: '#FFCA28', border: '#FFD54F'}
                };
                nodeObj.font = {color: '#212121', size: 6};
            }
        }

        if (item.properties.spf === 'No Records Found' || item.properties.dmarc === 'No Records Found') {
            nodeObj.color = {
                background: '#ED5555',
                border: '#D92E2E',
                highlight: {background: '#FF8B8B', border: '#FFB5B5'},
                hover: {background: '#FF8B8B', border: '#FFB5B5'}
            };
            nodeObj.font = {color: '#FFFFFF', size: 6};
        }

        let labels = item.labels;
        for (let i = 0; i < labels.length; i++) {
            if (labels[i] === 'ports') {
                nodeObj.color = {
                    background: '#ffe082',
                    border: '#ffca28',
                    highlight: {background: '#ffecb3', border: '#ffe082'},
                    hover: {background: '#ffecb3', border: '#ffe082'}
                };
                nodeObj.font = {color: '#000000', size: 6};
            }
            if (labels[i] === 'subdomain') {
                nodeObj.color = {
                    background: '#81d4fa',
                    border: '#29b6f6',
                    highlight: {background: '#b3e5fc', border: '#81d4fa'},
                    hover: {background: '#b3e5fc', border: '#81d4fa'}
                };
                nodeObj.font = {color: '#000000', size: 6};
            }
            if (labels[i] === 'domain') {
                nodeObj.color = {
                    background: '#a5d6a7',
                    border: '#66bb6a',
                    highlight: {background: '#c8e6c9', border: '#a5d6a7'},
                    hover: {background: '#c8e6c9', border: '#a5d6a7'}
                };
                nodeObj.font = {color: '#000000', size: 6};
            }
            if (labels[i] === 'ASN') {
                nodeObj.color = {background: '#9575cd', border: '#7e57c2'};
            }
            if (labels[i] === 'vulns') {
                nodeObj.color = {
                    background: '#e53935',
                    border: '#d32f2f',
                    highlight: {background: '#ED5555', border: '#d32f2f'},
                    hover: {background: '#ED5555', border: '#d32f2f'}
                };
            }
            if (labels[i] === 'isp') {
                nodeObj.color = {
                    background: '#E91E63',
                    border: '#D81B60',
                    highlight: {background: '#F06292', border: '#EC407A'},
                    hover: {background: '#F06292', border: '#EC407A'}
                };
            }

            if (labels[i] === 'bucket') {
                nodeObj.color = {
                    background: '#5C6BC0',
                    border: '#3F51B5',
                    highlight: {background: '#5C6BC0', border: '#3F51B5'},
                    hover: {background: '#5C6BC0', border: '#3F51B5'}
                };
            }

            if (labels[i] === 'bucket') {
                nodeObj.color = {
                    background: '#5C6BC0',
                    border: '#3F51B5',
                    highlight: {background: '#5C6BC0', border: '#3F51B5'},
                    hover: {background: '#5C6BC0', border: '#3F51B5'}
                };
            }

            if (labels[i] === 'bucket_status') {
                if (nodeObj.props && nodeObj.props.bucket_status === 'Public') {
                    nodeObj.color = {
                        background: '#e53935',
                        border: '#d32f2f',
                        highlight: {background: '#ED5555', border: '#d32f2f'},
                        hover: {background: '#ED5555', border: '#d32f2f'}
                    };
                }
            }
        }
        return nodeObj;
    }

    // If one of the important keywords are in the object, we want to display their content
    static importantKey(props) {
        const importantKeys = ['hostname', 'ports', 'finding', 'prefix', 'spf', 'value', 'dmarc', 'cve'];
        for (let i = 0; i < importantKeys.length; i++) {
            if (props.hasOwnProperty(importantKeys[i])) {
                return props[importantKeys[i]];
            }
        }
    }

    render() {
        const classnames = require('classnames');
        let className = classnames('search-results-container');
        let isInDemoMode = this.props.user && this.props.user.isInDemoMode;
        let mapShow = {'display': this.state.showLoader ? 'none' : 'block'};
        let activityIndicator = this.state.showLoader ? (<div className="activityIndicator">
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <CircularProgress/>
            </MuiThemeProvider>
        </div>) : '';

        let mainDivStyle = this.state.nodeExtraInfo ? {width: '75%'} : {};
        let tagStyle = mapShow;
        tagStyle.height = '100%';
        tagStyle.overflowY = 'auto';
        const cardStyle = {margin: '20px', borderRadius: '5px'};
        const topHeaderStyle = {display: 'flex'};
        const headerStyle = {margin: '30px 20px 20px 20px', backgroundColor: 'rgba(48, 48, 48, 0.8)',color: 'white',padding: '20px', textAlign: 'center', fontSize: '18px', borderRadius: '5px'};
        const cardTitleStyle = {backgroundColor: 'rgba(48, 48, 48, 0.8)', borderRadius: '5px 5px 0px 0px'};
        const cardTextStyle = {display: 'inline-grid', wordBreak: 'break-word' };

        let curHostname = '';
        if(isInDemoMode){
            //gettig the current hostname so we can remove it later from all data for demo mode
            if (this.state.nodeExtraInfo && this.state.nodeExtraInfo.info) {
                this.state.nodeExtraInfo.info.map((currInfo) => {
                    if (currInfo.data) {
                        currInfo.data.map((info) => {
                            if (info.name && info.name.toLowerCase() === 'hostname' && info.finding) {
                                curHostname = info.finding;
                            }
                        });
                    }
                });
            }
        }
            let nodeInfoTag =
            <Drawer width={'20%'} openSecondary={true} open={!!this.state.nodeExtraInfo}
                    containerStyle={{backgroundColor: 'rgba(200, 200, 200, 0.6)',
                        height: '100%',
                        overflowY: 'auto','display': this.state.showLoader ? 'none' : 'block'}}
                    docked={true}
                    disableSwipeToOpen={true} onRequestChange={this.closeNodeInfo.bind(this)}>
                {!this.state.nodeExtraInfo ? '' : (
                    (!this.state.nodeExtraInfo.info || this.state.nodeExtraInfo.info.length === 0) ? (
                        <div>
                            <div style={topHeaderStyle}>
                                <IconButton tooltip="close" tooltipPosition="bottom-right"
                                            style={{marginLeft: 5, marginTop: 5}}
                                            onClick={this.closeNodeInfo.bind(this)}>
                                    <NavClose/>
                                </IconButton>

                            </div>
                            <Card style={headerStyle}><div>No information found for
                                { (this.state.nodeExtraInfo.type) ? ' ' + this.state.nodeExtraInfo.type : ' this node'}
                                {isInDemoMode? '' :' ' + this.state.nodeExtraInfo.nodeDefaultLabel || ''}</div></Card>
                        </div>) : (
                        <div>
                            <div style={topHeaderStyle}>

                                <IconButton tooltip="close" tooltipPosition="bottom-right"
                                            style={{marginLeft: 5, marginTop: 5}}
                                            onClick={this.closeNodeInfo.bind(this)}>
                                    <NavClose/>
                                </IconButton>
                            </div>
                            <Card style={headerStyle}><div>Information about
                                {(this.state.nodeExtraInfo.type) ? ' ' + this.state.nodeExtraInfo.type : ' this node'}</div>
                            </Card>
                            {this.state.nodeExtraInfo.info.map((currInfo, i) => {
                                if (currInfo && currInfo.title && currInfo.data && currInfo.data.length > 0) {
                                    return <Card key={i} style={cardStyle}>
                                        <CardHeader
                                            title={currInfo.title}
                                            style={cardTitleStyle}
                                            titleColor="white"
                                        />
                                        <CardText style={cardTextStyle}>
                                            {currInfo.data.map((data, j) => {
                                                if (data && data.name && data.finding) {
                                                    let spanStyle = {};
                                                    let contentStyle = {};

                                                    if (data.name === 'WHOIS' || data.name === 'Impact' || data.name === 'Banner') {
                                                        spanStyle.whiteSpace = 'pre-wrap';
                                                    } else if (data.name === 'CVE Score') {
                                                        contentStyle.color = getScoreColor(data.finding);
                                                    } else if (data.name === 'CVE') {
                                                        let CVEhref = 'https://nvd.nist.gov/vuln/detail/' + data.finding;
                                                        data.finding =
                                                            <a href={CVEhref} target="_blank">{data.finding}</a>;
                                                    }
                                                    return <span key={j}
                                                                 style={spanStyle}><b>{data.name}</b>: <span
                                                        style={contentStyle}>{this.getFindings(isInDemoMode, data, curHostname)}</span></span>;
                                                }
                                            })}
                                        </CardText>
                                    </Card>;
                                }
                            })}
                        </div>
                    )
                )}
            </Drawer>
        ;

        return (
            <div style={mainDivStyle}>
                {activityIndicator}
                <div ref="myRef" className={className} style={mapShow}/>
                {nodeInfoTag}
            </div>
        );
    }
}

export default VisNetwork;
