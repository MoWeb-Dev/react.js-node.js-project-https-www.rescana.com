import React from 'react';
import VisNetwork from './vis.jsx';
import {Card} from 'material-ui/Card';
import Chip from 'material-ui/Chip';
import FlatButton from 'material-ui/FlatButton';
import Popover from 'material-ui/Popover';
import DatePicker from '../common/date-picker.jsx';
import IconButton from 'material-ui/IconButton';
import ActionDates from 'material-ui/svg-icons/action/date-range';
import LegendIcon from 'material-ui/svg-icons/av/fiber-manual-record';
import classNames from 'classnames';
import Paper from 'material-ui/Paper';
import '../../../public/css/main.css';
import '../../../node_modules/vis-network/dist/vis-network.css';
import '../../../public/css/vis-override.css';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import DataTableTemplateSettingsModal from '../common/datatableTemplate/datatable-template-settings.jsx';
import SettingsButton from 'material-ui/svg-icons/editor/mode-edit';
import BreadCrumbs from '../common/breadCrumbs.jsx';
import {SESSION_COMPANIES} from '../../../config/consts.js';
import Loader from 'react-loader-advanced';

export class GraphResultsContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            showLoader: false,
            hasResults: false,
            resultsData: null,
            datesRangeSeparator: ' - ',
            domain: ''
        };

        this.setInfoCard = this.setInfoCard.bind(this);
    }

    clearDatesRange() {
        this.setState({datesRange: ''});
    }

    handleOpenSettings() {
        this.setState({
            openSettingsModal: true
        });
    }

    closeSettingsDialog() {
        this.setState({openSettingsModal: false});
    }

    clear() {
        this.setState({
            hasResults: false,
            resultsData: null
        });
    }

    componentDidMount() {
        if (window && window.location && window.location.pathname) {
            let pathNameRoutes = window.location.pathname.split('/');
            let domain = pathNameRoutes.pop();
            this.setState({domain: domain});
        }
    }

    componentWillReceiveProps(nextProps) {
        let stateToUpdate = {showNoDataMessage: nextProps.showNoDataMessage};
        if (!sessionStorage.getItem(SESSION_COMPANIES)) {
            stateToUpdate.openSettingsModal = false;
        }
        if (nextProps.hasOwnProperty('showLoader')) {
            stateToUpdate.showLoader = nextProps.showLoader;
        }
        this.setState(stateToUpdate, () => {
            let nextData = nextProps.data;

            if (nextData && nextData.length > 0 && JSON.stringify(nextProps.data) !== JSON.stringify(this.state.oldData)) {
                this.setState({oldData: nextProps.data});
                this.setResults(nextData);
            }
            this.setState({startDate: nextProps.startDate, endDate: nextProps.endDate, openMenu: nextProps.openMenu});
        });
    }

    setResults(data) {
        this.refs.VisNetwork.setResults(data);
    }

    setLoader(loaderState) {
        this.refs.VisNetwork.setLoader(loaderState);
    }

    setInfoCard(data) {

        let propsArr = data.split(',');
        this.setState({
            nodeInfo: propsArr
        });
    }

    openDateFilters(event) {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            openMenu: true,
            anchorEl: event.currentTarget
        });
    }

    handleCloseMenu() {
        this.setState({
            startDate: undefined,
            endDate: undefined,
            openMenu: false
        });
    }

    handleSelect(range) {
        // An object with two keys,
        // 'startDate' and 'endDate' which are Momentjs objects.
        this.setState({startDate: range.startDate, endDate: range.endDate});
    }

    startFilter(companiesFilter) {
        let startD = this.state.startDate;
        let endD = this.state.endDate;
        this.props.startFilter(startD, endD, companiesFilter);
    }

    render() {
        let style = {
            padding: '2px',
            margin: '55px 8px 10px 8px'
        };
        let infoCardStyle = {
            minHeight: '75px',
            margin: 'auto',
            marginTop: '10px'
        };
        let datePickerStyle = {
            minHeight: '40px',
            margin: 'auto',
            padding: '10px',
            display: 'inline-flex',
            alignItems: 'center'
        };
        const chipStyles = {
            chip: {
                margin: 4,
                float: 'left',
                zIndex: 0
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap'
            }
        };
        const settingsBtnStyle = {
            'marginTop': '0px',
            'marginLeft': '10px'
        };
        const settingsChipsStyle = {
            'display': 'inline-flex'
        };

        const legendStyle = {
            'marginLeft': '20px',
            'display': 'inline-flex'
        };

        const legendTextStyle = {
            "marginLeft": "5px",
            "marginTop": "1px"
        };

        const resultStyle = {
            backgroundColor: '#FFDD00',
            display: 'inline-block',
            padding: '1% 5%',
            marginTop: '240px',
            fontSize: 'large',
            textAlign: 'center'
        };

        const companiesSelectorStyle = {
            textAlign: 'left'
        };

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        let mapShow = {'display': this.state.showNoDataMessage ? 'none' : 'block'};
        let filterShow = {'display': this.props.singleDomain ? 'none' : 'block'};
        let resultClassName = classNames('search-results-container');

        let companiesSelectorComponent = <div style={companiesSelectorStyle}>
            <IconButton tooltip="Filter by companies" touch={true} tooltipPosition="bottom-right"
                        style={settingsBtnStyle}>
                <SettingsButton
                    onTouchTap={this.handleOpenSettings.bind(this)}
                />
            </IconButton>
            <div style={settingsChipsStyle}>
                {showExpandedDataChip(this.props.companiesInputVal && this.props.companiesInputVal.map((cmp) => {
                    return (cmp.companyName);
                }), 8, 5, 'Companies')}
            </div>
        </div>;

        let emptyResultIndicator = this.state.showNoDataMessage ? (
            <span className={resultClassName}>
                {companiesSelectorComponent}
                <Paper style={resultStyle} zDepth={4}><span style={{verticalAlign: 'middle'}}>No data to display.</span></Paper>
            </span>
        ) : '';

        return (
            <div style={style}>
                <Loader show={this.state.showLoader} message={spinner}>
                    <div>
                        <BreadCrumbs
                            pathArr={[this.state.domain]}
                            show={this.props.singleDomain}
                        />
                        <div style={datePickerStyle}>
                            <IconButton tooltip="Filter by dates" touch={true} tooltipPosition="bottom-right"
                                        onTouchTap={this.openDateFilters.bind(this)}>
                                <ActionDates/>
                            </IconButton>

                            <label>{this.props.datesRange}</label>
                            <Popover
                                open={this.state.openMenu}
                                anchorEl={this.state.anchorEl}
                                anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                                onRequestClose={this.handleCloseMenu.bind(this)}
                            >
                                <div>
                                    <DatePicker
                                        onChange={this.handleSelect.bind(this)}
                                    />
                                    <div style={{float: 'right', margin: '5px'}}>
                                        <FlatButton label="Cancel" secondary={true}
                                                    onTouchTap={this.handleCloseMenu.bind(this)}/>
                                        <FlatButton label="Go" secondary={true} onTouchTap={() => this.startFilter()}/>
                                    </div>
                                </div>
                            </Popover>
                            {emptyResultIndicator}
                            <span style={filterShow}>{companiesSelectorComponent}</span>
                        </div>
                        <div style={mapShow}>
                            <Card>
                                <VisNetwork ref="VisNetwork" setInfoCard={this.setInfoCard}
                                            datesRange={this.props.datesRange}
                                            user={this.props.user}
                                            datesRangeSeparator={this.state.datesRangeSeparator}/>
                                <div style={legendStyle}><LegendIcon color={'#a5d6a7'} /><span style={legendTextStyle}> Domain</span></div>
                                <div style={legendStyle}><LegendIcon color={'#81d4fa'} /><span style={legendTextStyle}> Subdomain</span></div>
                                <div style={legendStyle}><LegendIcon color={'#adadad'} /><span style={legendTextStyle}> IP</span></div>
                                <div style={legendStyle}><LegendIcon color={'#ffe082'} /><span style={legendTextStyle}> Port</span></div>
                                <div style={legendStyle}><LegendIcon color={'#5C6BC0'} /><span style={legendTextStyle}> Bucket</span></div>
                                <div style={legendStyle}><LegendIcon color={'#E91E63'} /><span style={legendTextStyle}> ISP</span></div>
                            </Card>
                            <div style={infoCardStyle}>
                                {
                                    this.state.nodeInfo && this.state.nodeInfo.map((prop, i) => {
                                        return <Chip key={i} style={chipStyles.chip}>
                                            {prop}
                                        </Chip>;
                                    })
                                }
                            </div>
                        </div>
                        <DataTableTemplateSettingsModal
                            open={this.state.openSettingsModal}
                            closeDialog={this.closeSettingsDialog.bind(this)}
                            title={'Filter By Companies'}
                            companies={this.props.companies}
                            companiesInputVal={this.props.companiesInputVal}
                            ApplyChangesFromSettings={this.props.ApplyChangesFromSettings}
                            myDataType={this.props.myDataType}
                        />
                    </div>
                </Loader>
            </div>
        );
    }
}

export default GraphResultsContainer;
