import React, {Component} from 'react';
import DataTables from 'material-ui-datatables';
import async from 'async';
import {CSVLink} from 'react-csv';
import FileDownloadIcon from 'material-ui/svg-icons/file/file-download';
import {Card} from 'material-ui/Card';
import FuzzySearch from 'fuzzy-search';
import Chip from 'material-ui/Chip';
import {
    CVE_LOW_MAX,
    CVE_MEDIUM_MIN,
    CVE_MEDIUM_MAX,
    CVE_HIGH_MIN,
    CVE_HIGH_MAX,
    CVE_CRITICAL_MIN, ORGANIZATION_ID, ORGANIZATION_NAME,
    CVE_CRITICAL_MAX, SESSION_COMPANIES, SESSION_CVE_SEVERITY
} from '../../../config/consts';
import Loader from 'react-loader-advanced';
import classNames from 'classnames';
import {isNumeric} from '../common/CommonHelpers.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import themeForFont from '../app/themes/themeForFont';
import GraphIcon from 'material-ui/svg-icons/hardware/device-hub';
import TableIcon from 'material-ui/svg-icons/navigation/arrow-forward';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import IconButton from 'material-ui/IconButton/IconButton';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import DataTableTemplateSettingsModal from '../common/datatableTemplate/datatable-template-settings.jsx';
import SettingsButton from 'material-ui/svg-icons/editor/mode-edit';
import MitigatedIcon from 'material-ui/svg-icons/action/done';
import BreadCrumbs from '../common/breadCrumbs.jsx';
import Paper from 'material-ui/Paper';
import ActionDates from 'material-ui/svg-icons/action/date-range';
import FlatButton from 'material-ui/FlatButton';
import Popover from 'material-ui/Popover';
import DatePicker from '../common/date-picker.jsx';
import DynamicDialog from '../app/dynamic-dialog.jsx';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Badge from 'material-ui/Badge';
import CommentIcon from 'material-ui/svg-icons/communication/comment';
import AssetUserInputCommentsDialog from '../generalComponents/comments-dialog.jsx';
import generalFunctions from '../generalComponents/general-functions.js';
import ReactTooltip from 'react-tooltip';
import moment from 'moment';

class TableResultsContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showLoader: false,
            nodeArr: [],
            tableColumns: [],
            tableData: [],
            sortedTableData: [],
            dataForDownload: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            page: 1,
            intelTableType: this.props.intelTableType,
            isFindingStatusDialogOpen: false,
            isCommentsDialogOpen: false,
            curCommentsArr: [],
            findingStatusDialogText: '',
            isAssetUserInputOrMitigatedBtnClicked: false
        };


        this.handleSortOrderChange = this.handleSortOrderChange.bind(this);
        this.handleFilterValueChange = this.handleFilterValueChange.bind(this);
        this.handleNextPageClick = this.handleNextPageClick.bind(this);
        this.handlePreviousPageClick = this.handlePreviousPageClick.bind(this);
        this.handleRowSizeChange = this.handleRowSizeChange.bind(this);
        this.handleCellClick = this.handleCellClick.bind(this);
        this.checkIfInRange = this.checkIfInRange.bind(this);
        this.handleOpenCommentsDialog = this.handleOpenCommentsDialog.bind(this);
    }

    componentDidMount() {
        let user = app.getAuthUser();
        if (user) {
            this.setState({user: user});
        }
    }

    componentWillReceiveProps(nextProps) {

        let stateToUpdate = {showNoDataMessage: nextProps.showNoDataMessage};
        if (!sessionStorage.getItem(SESSION_COMPANIES)) {
            stateToUpdate.openSettingsModal = true;
        }
        if (nextProps.hasOwnProperty('showLoader')) {
            this.setState({showLoader: nextProps.showLoader});
        }
        if (nextProps && nextProps.assetUserInputAndMitigatedData) {
            this.setState({assetUserInputAndMitigatedData: nextProps.assetUserInputAndMitigatedData});
        }

        if (nextProps) {
            this.setState({showScanDate: nextProps.showScanDate, latestScanDate: nextProps.latestScanDate});
        }

        this.setState(stateToUpdate, () => {
            let nextData = nextProps.data;

            if (nextData && nextData.length > 0 && JSON.stringify(nextProps.data) !== JSON.stringify(this.state.oldData)) {
                this.setState({oldData: nextProps.data});
            }

            this.setResults(nextData);
            this.setState({
                startDate: nextProps.startDate, endDate: nextProps.endDate, openMenu: nextProps.openMenu,
                intelTableType: nextProps.intelTableType
            });
        });
    }

    closeFindingDialog() {
        this.setState({isFindingStatusDialogOpen: false, isAssetUserInputOrMitigatedBtnClicked: false});
    }

    handleCloseCommentsDialog() {
        this.setState({isCommentsDialogOpen: false, isAssetUserInputOrMitigatedBtnClicked: false});
    }

    handleUpdateCommentsArea(curCommentsArr) {
        let curRowData = this.state.curRowData;
        let orgId = localStorage.getItem(ORGANIZATION_ID);
        if (curRowData && orgId && curCommentsArr && Array.isArray(curCommentsArr)) {
            curRowData.comments = curCommentsArr;
            this.setState({curRowData: curRowData, isCommentUpdate: true});
            this.updateFindingStatus(true);
            this.setState({curCommentsArr: curCommentsArr});
        } else {
            app.addAlert('error', 'Error: failed to update Comments Area');

        }
    }

    handleOpenCommentsDialog(curRowData, isFromMitigated = false) {
        let comments = [];
        if (curRowData.Comments &&
            curRowData.Comments.props &&
            curRowData.Comments.props.children &&
            curRowData.Comments.props.children[1] &&
            curRowData.Comments.props.children[1].props &&
            curRowData.Comments.props.children[1].props.children) {
            comments = curRowData.Comments.props.children[1].props.children;
        }
        this.setState({
            isFromMitigated: isFromMitigated,
            isAssetUserInputOrMitigatedBtnClicked: true,
            isCommentsDialogOpen: true,
            curCommentsArr: comments,
            curRowData: curRowData,
            findingStatusType: 'assetUserInput'
        });
    }

    updateFindingStatus(isCommentsAreaUpdate = false) {
        if (typeof isCommentsAreaUpdate !== 'boolean') {
            isCommentsAreaUpdate = false
        }
        let successMes = 'Finding status has been update!';
        let failedMes = 'Error: failed to update finding status.';

        if (isCommentsAreaUpdate) {
            successMes = 'Comments log has been update!';
            failedMes = 'Error: failed to update comments log.';
        }

        let objToUpdate = {};
        let curRowData = this.state.curRowData;

        //Indicates what data to update (assetUserInput or Mitigated).
        let isAssetUserInputUpdate = !!(curRowData &&
            ((curRowData.importance && typeof curRowData.importance === 'number') ||
                (curRowData.comments && Array.isArray(curRowData.comments))));

        //A flag that says if the connection should be created or destroyed.
        let shouldDetachMitigatedConnection = !!(this.state.isStatusMitigatedActive && this.state.findingStatusType && this.state.findingStatusType === 'mitigated');
        let type = this.state.findingStatusType;

        if (curRowData && typeof curRowData === 'object') {
            objToUpdate = this.getFindingStatusArrToUpdate(curRowData, isAssetUserInputUpdate);

            //check if obj is not empty
            if (Object.getOwnPropertyNames(objToUpdate).length > 0) {

                let curOrgObj = {
                    orgId: localStorage.getItem(ORGANIZATION_ID),
                    orgName: localStorage.getItem(ORGANIZATION_NAME)
                };
                let dataType = this.props.intelTableType;
                if (dataType === 'cve') {
                    dataType = 'cpe';
                }

                let findingStatusData = {
                    curOrgObj: curOrgObj,
                    statusType: type,
                    objToUpdate: objToUpdate,
                    shouldDetachMitigatedConnection: shouldDetachMitigatedConnection,
                    updateOnMongoDB: false,
                    isAssetUserInputUpdate: isAssetUserInputUpdate,
                    dataType: dataType || ''
                };

                $.ajax({
                    type: 'POST',
                    url: '/api/updateFindingStatus',
                    async: true,
                    data: JSON.stringify({findingStatusData}),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((res) => {
                    if (res && res.ok) {
                        if (res.assetUserInputAndMitigatedRes) {
                            this.setState({assetUserInputAndMitigatedData: res.assetUserInputAndMitigatedRes});
                        }

                        this.setResults(this.props.data);
                        if (this.state.orderKey && this.state.orderType) {
                            this.handleSortOrderChange(this.state.orderKey, this.state.orderType)
                        }
                        this.updateLastPageUserWasOn();

                        this.setState({isFindingStatusDialogOpen: false, isAssetUserInputOrMitigatedBtnClicked: false});
                        app.addAlert('success', successMes);

                        //if user update comments area we do not recalculate the score again
                        if (!isCommentsAreaUpdate) {
                            this.props.recalculateScore();
                        }

                        //if user mark asset as mitigated open comments dialog
                        if (!isAssetUserInputUpdate && type === 'mitigated' && !shouldDetachMitigatedConnection) {
                            this.handleOpenCommentsDialog(curRowData, true);
                        }
                    } else {
                        app.addAlert('error', failedMes);
                    }
                }).fail(() => {
                    app.addAlert('error', failedMes);
                });
            } else {
                app.addAlert('error', failedMes);
            }
        } else {
            app.addAlert('error', failedMes);
        }
    }

    checkIfPropOfCurNodeExists(curNode, type) {
        if (type && type === 'assetUserInput') {
            return !!(curNode.Importance &&
                curNode.Importance.props &&
                curNode.Importance.props.value)
        } else {
            return !!(curNode.Mitigated &&
                curNode.Mitigated.props &&
                curNode.Mitigated.props &&
                curNode.Mitigated.props.iconStyle &&
                curNode.Mitigated.props.iconStyle.color)
        }
    }

    isCurRowNeedToBeUpdateCurNode(curNode, curRowData) {
        let curIntelTableName = this.props.intelTableType;
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;
        let hashDataArr = this.state.hashDataArr;

        let isTrue = false;
        let ip = null, bucket = null, hostname = null;

        if (isInDemoMode) {
            if (curNode.IP) {
                ip = generalFunctions.getDataFromHashArr(curNode.IP, hashDataArr);
            }
            if (curNode.Hostname) {
                hostname = generalFunctions.getDataFromHashArr(curNode.Hostname, hashDataArr);
            }
            if (curNode.bucket) {
                bucket = generalFunctions.getDataFromHashArr(curNode.bucket, hashDataArr);
            }
        } else {
            if (curNode.IP) {
                ip = curNode.IP;
            }
            if (curNode.bucket) {
                bucket = curNode.bucket;
            }
            if (curNode.Hostname && curNode.Hostname.props && curNode.Hostname.props.children) {
                hostname = curNode.Hostname.props.children;
            }
        }

        if (curIntelTableName === 'IP') {
            if (ip && curRowData.ip && ip === curRowData.ip) {
                isTrue = true;
            }
        } else if (curIntelTableName === 'ports') {
            if (curNode.ports && curRowData.port && curNode.ports === curRowData.port &&
                ip && curRowData.ip && ip === curRowData.ip) {
                isTrue = true;
            }
        } else if (curIntelTableName === 'buckets') {
            if (bucket && curRowData.bucket && bucket === curRowData.bucket) {
                isTrue = true;
            }
        } else if (curIntelTableName === 'product') {
            if (curNode.product && curRowData.product && curNode.product === curRowData.product &&
                ip && curRowData.ip && ip === curRowData.ip) {
                isTrue = true;
            }
        } else if (curIntelTableName === 'isp') {
            if (curNode.isp && curRowData.isp && curNode.isp === curRowData.isp &&
                ip && curRowData.ip && ip === curRowData.ip) {
                isTrue = true;
            }
        } else if (curIntelTableName === 'cve') {
            let productVal;
            //because Product is an object with 2 props, one text for ui: "php 5.6.40"(can include the iis icon too), and one with the full name: "cpe:/a:php:php:5.6.40"
            //we check if the the full name prop is exist and use it.
            if (curNode.Product && curNode.Product !== 'string' && curNode.Product.props && curNode.Product.props.children &&
                curNode.Product.props.children[1] && curNode.Product.props.children[1].props &&
                curNode.Product.props.children[1].props.children) {
                productVal = curNode.Product.props.children[1].props.children;
            }
            if (this.props.onCveFirstTable) {
                if (productVal && curRowData.cpe && productVal === curRowData.cpe &&
                    ip && curRowData.ip && ip === curRowData.ip &&
                    (curRowData.cve === undefined || curRowData.cve === null)) {
                    isTrue = true;
                }
            } else {
                if (curNode.CVE && curNode.CVE.props && curNode.CVE.props.children && curRowData.cve && curNode.CVE.props.children === curRowData.cve &&
                    ip && curRowData.ip && ip === curRowData.ip &&
                    productVal && curRowData.cpe && productVal === curRowData.cpe) {
                    isTrue = true;
                }
            }
        } else if (curIntelTableName === 'spf' || curIntelTableName === 'dmarc') {
            if (curIntelTableName === 'spf') {
                if (curNode.SPF && curRowData.spf &&
                    curNode.SPF === curRowData.spf &&
                    hostname &&
                    curRowData.hostname && curRowData.hostname === hostname) {
                    isTrue = true;
                }
            } else {
                if (curNode.DMARC && curRowData.dmarc &&
                    curNode.DMARC === curRowData.dmarc &&
                    hostname &&
                    curRowData.hostname && curRowData.hostname === hostname) {
                    isTrue = true;
                }
            }
        }
        return isTrue;
    }

    getFindingStatusArrToUpdate(rowData, isAssetUserInputUpdate) {
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;
        let hashDataArr = this.state.hashDataArr;
        let curIntelTableName = this.state.intelTableType;
        let objToAdd = {};
        let AMNodePropsObject = {};
        if (isInDemoMode) {
            if (rowData.IP) {
                rowData.IP = generalFunctions.getDataFromHashArr(rowData.IP, hashDataArr);
            }
            if (rowData.Hostname) {
                rowData.Hostname = generalFunctions.getDataFromHashArr(rowData.Hostname, hashDataArr);
            }
            if (rowData.bucket) {
                rowData.bucket = generalFunctions.getDataFromHashArr(rowData.bucket, hashDataArr);
            }
        }
        if (rowData && typeof rowData === 'object') {
            if (curIntelTableName === 'IP') {// DOMAINS/IP
                if (rowData.IP) {
                    AMNodePropsObject = {ip: rowData.IP};
                    objToAdd = {type: 'ip', val: rowData.IP, AMNodePropsObject: AMNodePropsObject};
                }
            } else if (curIntelTableName === 'ports') { // OPEN PORTS
                if (rowData.ports && rowData.IP) {
                    AMNodePropsObject = {port: rowData.ports, ip: rowData.IP};
                    objToAdd = {
                        type: 'port',
                        val: rowData.ports,
                        extraVal: rowData.IP,
                        AMNodePropsObject: AMNodePropsObject
                    };
                }
            } else if (curIntelTableName === 'buckets') {// S3 BUCKETS
                if (rowData.bucket) {
                    AMNodePropsObject = {bucket: rowData.bucket};
                    objToAdd = {type: 'bucket', val: rowData.bucket, AMNodePropsObject: AMNodePropsObject};
                }
            } else if (curIntelTableName === 'product') {// DISCOVERED SYSTEMS
                if (rowData.product) {
                    AMNodePropsObject = {ip: rowData.IP, product: rowData.product};
                    objToAdd = {
                        type: 'product',
                        val: rowData.product,
                        extraVal: rowData.IP,
                        AMNodePropsObject: AMNodePropsObject
                    };
                }
            } else if (curIntelTableName === 'isp') {// ISP/HOSTING PROVIDER
                if (rowData.isp && rowData.IP) {
                    AMNodePropsObject = {ip: rowData.IP, isp: rowData.isp};
                    objToAdd = {
                        type: 'isp',
                        val: rowData.isp,
                        extraVal: rowData.IP,
                        AMNodePropsObject: AMNodePropsObject
                    };
                }
            } else if (curIntelTableName === 'cve') {// NETWORK
                let productVal;
                //because Product is an object with 2 props, one text for ui: "php 5.6.40"(can include the iis icon too), and one with the full name: "cpe:/a:php:php:5.6.40"
                //we check if the the full name prop is exist and use it.
                if (rowData.Product && rowData.Product !== 'string' && rowData.Product.props && rowData.Product.props.children &&
                    rowData.Product.props.children[1] && rowData.Product.props.children[1].props &&
                    rowData.Product.props.children[1].props.children) {
                    productVal = rowData.Product.props.children[1].props.children;
                }

                if (rowData.CVE &&
                    rowData.CVE.props &&
                    rowData.CVE.props.children && productVal) {
                    AMNodePropsObject = {ip: rowData.IP, cve: rowData.CVE.props.children, cpe: productVal};
                    objToAdd = {type: 'cve', val: rowData.CVE.props.children, AMNodePropsObject: AMNodePropsObject};

                } else if (productVal) {
                    AMNodePropsObject = {ip: rowData.IP, cpe: productVal};
                    objToAdd = {type: 'cpe', val: productVal, AMNodePropsObject: AMNodePropsObject};
                }
            } else if (curIntelTableName === 'spf') {// SPF RECORDS
                if (isInDemoMode && rowData.SPF && rowData.Hostname) {
                    let AMNodePropsObject = {spf: rowData.SPF, hostname: rowData.Hostname};
                    objToAdd = {
                        type: 'spf',
                        val: rowData.SPF,
                        extraVal: rowData.Hostname,
                        AMNodePropsObject: AMNodePropsObject
                    };
                } else if (rowData.SPF && rowData.Hostname &&
                    rowData.Hostname.props &&
                    rowData.Hostname.props.children) {
                    let AMNodePropsObject = {spf: rowData.SPF, hostname: rowData.Hostname.props.children};
                    objToAdd = {
                        type: 'spf',
                        val: rowData.SPF,
                        extraVal: rowData.Hostname.props.children,
                        AMNodePropsObject: AMNodePropsObject
                    };
                }
            } else if (curIntelTableName === 'dmarc') {// DMARC RECORDS
                if (isInDemoMode && rowData.DMARC && rowData.Hostname) {
                    AMNodePropsObject = {dmarc: rowData.DMARC, hostname: rowData.Hostname};
                    objToAdd = {
                        type: 'dmarc',
                        val: rowData.DMARC,
                        extraVal: rowData.Hostname,
                        AMNodePropsObject: AMNodePropsObject
                    };
                } else if (rowData.DMARC && rowData.Hostname &&
                    rowData.Hostname.props &&
                    rowData.Hostname.props.children) {
                    AMNodePropsObject = {dmarc: rowData.DMARC, hostname: rowData.Hostname.props.children};
                    objToAdd = {
                        type: 'dmarc',
                        val: rowData.DMARC,
                        extraVal: rowData.Hostname.props.children,
                        AMNodePropsObject: AMNodePropsObject
                    };
                }
            }
            if (isAssetUserInputUpdate) {
                if (rowData.importance) {
                    objToAdd.importance = rowData.importance;
                } else {
                    objToAdd.importance = this.checkIfPropOfCurNodeExists(rowData, 'assetUserInput') ? rowData.Importance.props.value : 3;
                }
                if (rowData.comments) {
                    objToAdd.comments = rowData.comments;
                }
            }
        }
        return objToAdd;
    }

    HandleSelectFindingStatus(curRowData, type) {
        //We use this to avoid cell click that redirect us to graph page
        this.setState({isAssetUserInputOrMitigatedBtnClicked: true, isFindingStatusDialogOpen: true}, () => {
            //checkIfPropOfCurNodeExists checks the style state of the component - this is to indicate the component state.
            let isPropOfCurNodeExists = this.checkIfPropOfCurNodeExists(curRowData, 'Mitigated');
            let findingStatusDialogText = '';
            let isStatusMitigatedActive = isPropOfCurNodeExists && curRowData.Mitigated.props.iconStyle.color !== "black";

            if (isStatusMitigatedActive) {
                findingStatusDialogText = 'status from mitigated to default';
            } else {
                findingStatusDialogText = 'status to mitigated';
            }

            this.setState({
                findingStatusDialogText: findingStatusDialogText,
                curRowData: curRowData,
                isStatusMitigatedActive: isStatusMitigatedActive,
                findingStatusType: type
            });
        });
    }

    checkIfInRange(curNode, minScore, maxScore) {
        let curScore = Number(curNode.Score);
        return minScore < curScore && curScore <= maxScore;
    }

    clickNVDLink(e) {
        e.stopPropagation();
    }

    onChangeStatusDropdown(curRowData, type, event, index, value) {
        this.setState({
            isAssetUserInputOrMitigatedBtnClicked: false,
            isFindingStatusDialogOpen: true,
            isFromMitigated: false
        });
        setTimeout(() => {
            curRowData.importance = value;
            this.setState({findingStatusDialogText: 'importance', curRowData: curRowData, findingStatusType: type});
        }, 1);
    }

    onTapStatusDropdown() {
        this.setState({isAssetUserInputOrMitigatedBtnClicked: true});
    }

    onDropdownClose() {
        this.setState({isAssetUserInputOrMitigatedBtnClicked: false});
    }

    getImportanceComponent(tableDataObj, importance = 3) {
        return <DropDownMenu
            value={Number(importance)}
            onTouchTap={this.onTapStatusDropdown.bind(this)}
            onChange={this.onChangeStatusDropdown.bind(this, tableDataObj, 'assetUserInput')}
            autoWidth={true}
            onClose={this.onDropdownClose.bind(this)}
            labelStyle={{left: 10, fontSize: 13}}
            style={{width: 170}}
            menuStyle={{borderRadius: 4}}
            menuItemStyle={{fontSize: 13}}
            anchorOrigin={{
                horizontal: 'left',
                vertical: 'bottom'
            }}>
            <MenuItem value={1} primaryText="False Positive"/>
            <MenuItem value={2} primaryText="Low"/>
            <MenuItem value={3} primaryText="Medium"/>
            <MenuItem value={4} primaryText="High"/>
            <MenuItem value={5} primaryText="Critical"/>
        </DropDownMenu>

    }

    getCommentsComponent(tableDataObj, commentsCount = 0, comments = [], badgeColor = '#82abea') {
        return <div style={{paddingLeft: 30}}><Badge badgeContent={commentsCount} secondary={true}
                                                     style={{marginLeft: -30, marginRight: -30}}
                                                     badgeStyle={{background: badgeColor, top: 17, right: 17}}>
            <IconButton data-tip={'Comments'}
                        data-for={'Comments'}
                        onTouchTap={this.handleOpenCommentsDialog.bind(this, tableDataObj, false)}>
                <CommentIcon style={{width: 5, height: 5}}/>
                <ReactTooltip id='Comments' place="right" effect="solid"/>
            </IconButton>
        </Badge>
            <span style={{display: 'none', visibility: 'hidden'}}>{comments}</span>
        </div>
    }

    getMitigatedComponent(tableDataObj, mitigatedBackground = 'black') {
        return <IconButton data-tip={'Mark as Mitigated'}
                           data-for={'mitigated'}
                           iconStyle={{
                               width: 30,
                               height: 30,
                               color: mitigatedBackground,
                               position: 'relative',
                               left: 6,
                               top: 2
                           }}
                           onTouchTap={this.HandleSelectFindingStatus.bind(this, tableDataObj, 'mitigated')}>
            <ReactTooltip id='mitigated' place="right" effect="solid"/>
            <MitigatedIcon viewBox={'3 3 35 35'}/>
        </IconButton>
    }


    setResults(data) {
        // TODO: Need to refactor this whole function.
        const sessionCveSeverity = sessionStorage.getItem(SESSION_CVE_SEVERITY);
        let tableColumns = [];
        let tableData = [];
        let tableDataForDownload = [];
        let nodeArr = [];
        let assetUserInputAndMitigatedData = this.state.assetUserInputAndMitigatedData;
        let showScanDate = this.state.showScanDate;
        let latestScanDate = this.state.latestScanDate;
        let showScanDateOffset = 0;
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;
        let hashDataArr = [];
        let exampleIdx = 0;
        let hashIdx = 0;

        if (Array.isArray(data)) {
            nodeArr = data.map((el) => {
                if (el) {
                    return el['nodes(path)'] || el['NODES(path)+COALESCE (NODES(path2),[])'];
                }
            });
        }

        if (Array.isArray(data) && showScanDate) {
           showScanDateOffset = 1;
        }


        // Create table column headers
        if (nodeArr && nodeArr[0]) {
            let wrappableStyle = {
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-word'
            };

            let columnHeaders = [];
            if (nodeArr[0][nodeArr[0].length - 1].labels[0].toLowerCase() === 'ip') {

                if (showScanDate) {
                    columnHeaders = ['Source', 'IP', 'Latest Scan Date', 'Importance', 'Comments', 'Mitigated', ' '];
                } else {
                    columnHeaders = ['Source', 'IP', 'Importance', 'Comments', 'Mitigated', ' '];
                }


                for (let i = 0; i < columnHeaders.length; i++) {
                    tableColumns[i] = {
                        sortable: true,
                        label: columnHeaders[i],
                        key: columnHeaders[i],
                        style: wrappableStyle
                    };
                }

                async.each(nodeArr, (nodes, callback) => {
                    let tableDataObj = {};
                    // We need to create a different table for downloading in CSV (without objects).
                    let tableDataObjForDownload = {};

                    //Go over all table columns.
                    for (let i = tableColumns.length - (5 + showScanDateOffset), j = nodes.length - 1; i >= 0; i--, j--) {
                        if (tableColumns[i] && nodes[j]) {
                            if (nodes[j].properties.hostname) {
                                //for no_domain node
                                if (nodes[j].properties.hostname === "no_domain") {
                                    tableDataObj[tableColumns[i]['key']] = "no domain";
                                    tableDataObjForDownload[tableColumns[i]['key']] = "no domain";
                                } else {
                                    let urlLink = 'http://' + nodes[j].properties.hostname.toString();
                                    tableDataObj[tableColumns[i]['key']] = <a href={urlLink} target="_blank"
                                                                              style={{'textDecoration': 'none'}}>{nodes[j].properties.hostname.toString()}</a>;
                                    tableDataObjForDownload[tableColumns[i]['key']] = nodes[j].properties.hostname.toString();
                                }
                                if (isInDemoMode) {
                                    let hashVal = 'domain-example-' + exampleIdx + '.com';
                                    tableDataObj[tableColumns[i]['key']] = hashVal;
                                    tableDataObjForDownload[tableColumns[i]['key']] = hashVal;
                                    hashDataArr[hashIdx] = {
                                        real: nodes[j].properties.hostname.toString(),
                                        demo: hashVal
                                    };
                                    hashIdx++;
                                }
                                //for no_domain node
                            } else if (nodes[j].properties.address && nodes[j].properties.companyName) {
                                tableDataObj[tableColumns[i]['key']] = nodes[j].properties.address;
                                tableDataObjForDownload[tableColumns[i]['key']] = nodes[j].properties.address;
                            } else {
                                let parts = [];
                                parts = JSON.stringify(nodes[j].properties).replace(/{/g, '').replace(/}/g, '').replace(/"/g, '').split(':');
                                let val = parts.pop();

                                if (isInDemoMode) {
                                    let hashVal = generalFunctions.createIpStrForDemoMode(val, exampleIdx);
                                    tableDataObj[tableColumns[i]['key']] = hashVal;
                                    tableDataObjForDownload[tableColumns[i]['key']] = hashVal;
                                    hashDataArr[hashIdx] = {real: val, demo: hashVal};
                                    hashIdx++;
                                } else {
                                    tableDataObj[tableColumns[i]['key']] = val;
                                    tableDataObjForDownload[tableColumns[i]['key']] = val;
                                }
                            }
                        } else {
                            tableDataObj[tableColumns[1]['key']] = '';
                            tableDataObjForDownload[tableColumns[1]['key']] = '';
                            if (isInDemoMode) {
                                let hashVal = 'domain-example-' + exampleIdx + '.com';
                                tableDataObj[tableColumns[0]['key']] = hashVal;
                                tableDataObjForDownload[tableColumns[0]['key']] = hashVal;
                                hashDataArr[hashIdx] = {real: nodes[0].properties.hostname.toString(), demo: hashVal};
                                hashIdx++;
                            } else {
                                tableDataObj[tableColumns[0]['key']] = nodes[0].properties.hostname.toString();
                                tableDataObjForDownload[tableColumns[0]['key']] = nodes[0].properties.hostname.toString();
                            }
                        }

                        //Add last scan Date to table
                        if (showScanDate) {
                            tableDataObj[tableColumns[2]['key']] = moment(latestScanDate).format('YYYY/MM/DD, HH:mm:ss');
                        }
                    }

                    let importance = 3;
                    let mitigatedBackground = 'black';
                    let comments = [];
                    let assetUserInputFlag = false;
                    let mitigatedBackgroundFlag = false;
                    let commentsFlag = false;
                    for (let i = 0; i < assetUserInputAndMitigatedData.length; i++) {
                        //This function checks each row wheter it needs to be updated with mitigation or asset user input information.
                        if (this.isCurRowNeedToBeUpdateCurNode(tableDataObj, assetUserInputAndMitigatedData[i])) {
                            if (assetUserInputAndMitigatedData[i].type && assetUserInputAndMitigatedData[i].type === 'assetUserInput') {
                                importance = assetUserInputAndMitigatedData[i].importance ? Number(assetUserInputAndMitigatedData[i].importance) : 3;
                                assetUserInputFlag = true;
                            }

                            if (assetUserInputAndMitigatedData[i].type && assetUserInputAndMitigatedData[i].type === 'mitigated') {
                                mitigatedBackground = '#5bd25e';
                                mitigatedBackgroundFlag = true;
                            }
                            if (assetUserInputAndMitigatedData[i].comments) {
                                comments = assetUserInputAndMitigatedData[i].comments ? assetUserInputAndMitigatedData[i].comments : [];
                                commentsFlag = true;
                            }
                            if (mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag) {
                                break;
                            }
                        }
                    }
                    let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';
                    //Add last scan Date to table
                    if (showScanDate) {
                        tableDataObj[tableColumns[2]['key']] = moment(latestScanDate).format('YYYY/MM/DD, HH:mm:ss');
                    }

                    tableDataObj[tableColumns[2 + showScanDateOffset]['key']] = this.getImportanceComponent(tableDataObj, importance);
                    tableDataObj[tableColumns[3 + showScanDateOffset]['key']] = this.getCommentsComponent(tableDataObj, comments.length, comments, badgeColor);
                    tableDataObj[tableColumns[4 + showScanDateOffset]['key']] = this.getMitigatedComponent(tableDataObj, mitigatedBackground);

                    tableDataObjForDownload[tableColumns[2]['key']] = moment(latestScanDate).format('YYYY/MM/DD, HH:mm:ss');
                    tableDataObjForDownload[tableColumns[2 + showScanDateOffset]['key']] = generalFunctions.getImportanceStr(importance);
                    tableDataObjForDownload[tableColumns[3 + showScanDateOffset]['key']] = generalFunctions.allCommentsToOneStr(comments);
                    tableDataObjForDownload[tableColumns[4 + showScanDateOffset]['key']] = mitigatedBackgroundFlag ? 'Yes' : 'No';
                    //graph icon adding to the last column
                    tableDataObj[tableColumns[5 + showScanDateOffset]['key']] =
                        <div><GraphIcon/><span style={{marginLeft: 10}}>Show Graph</span></div>;


                    //check if object is not already exist in dataTable
                    let isNeedToBeAdded = true;
                    for (let k = 0; k < tableData.length; k++) {
                        if (this.isAlreadyExistInTableData(tableData[k], tableDataObj)) {
                            isNeedToBeAdded = false;
                            break;
                        }
                    }

                    if (isNeedToBeAdded) {
                        // Push data objects to array.
                        tableData.push(tableDataObj);
                        tableDataForDownload.push(tableDataObjForDownload);
                    }
                    exampleIdx++;
                    callback();
                });

                this.setState({
                    hashDataArr: hashDataArr,
                    tableColumns: tableColumns,
                    totalRowCount: tableData.length,
                    tableData: tableData,
                    dataForDownload: tableDataForDownload,
                    sortedTableData: tableData.slice(0, this.state.rowCountInPage),
                    nodeArr: nodeArr
                });

                return;
            } else if (nodeArr[0][nodeArr[0].length - 1].labels[0].toLowerCase() === 'product') {
                if (showScanDate) {
                    columnHeaders = ['IP', nodeArr[0][nodeArr[0].length - 2].labels[0], nodeArr[0][nodeArr[0].length - 1].labels[0], 'Latest Scan Date', 'Importance', 'Comments', 'Mitigated', ' '];
                } else {
                    columnHeaders = ['IP', nodeArr[0][nodeArr[0].length - 2].labels[0], nodeArr[0][nodeArr[0].length - 1].labels[0], 'Importance', 'Comments', 'Mitigated', ' '];
                }
            } else {
                if (showScanDate) {
                    columnHeaders = ['Hostname', nodeArr[0][nodeArr[0].length - 2].labels[0], nodeArr[0][nodeArr[0].length - 1].labels[0], 'Latest Scan Date', 'Importance', 'Comments', 'Mitigated', ' '];
                } else {
                    columnHeaders = ['Hostname', nodeArr[0][nodeArr[0].length - 2].labels[0], nodeArr[0][nodeArr[0].length - 1].labels[0], 'Importance', 'Comments', 'Mitigated', ' '];
                }
            }

            for (let i = 0; i < nodeArr[0].length; i++) {
                if (nodeArr[0][i].labels[0].toLowerCase() === 'cve') {
                    if (this.props.onCveFirstTable && window.location.pathname && window.location.pathname === '/intel/cve') {
                        if (showScanDate) {
                            columnHeaders = ['Hostname', 'IP', 'Port', 'Product', 'Risk', 'Latest Scan Date', 'Importance', 'Comments', 'Mitigated', 'More Info'];
                        } else {
                            columnHeaders = ['Hostname', 'IP', 'Port', 'Product', 'Risk', 'Importance', 'Comments', 'Mitigated', 'More Info'];
                        }
                    } else {
                        if (showScanDate) {
                            columnHeaders = ['Hostname', 'IP', 'Port', 'Product', 'CVE', 'CVSS', 'Latest Scan Date', 'Importance', 'Comments', 'Mitigated', ' '];
                        } else {
                            columnHeaders = ['Hostname', 'IP', 'Port', 'Product', 'CVE', 'CVSS', 'Importance', 'Comments', 'Mitigated', ' '];
                        }
                    }
                }

                if (nodeArr[0][i].labels[0].toLowerCase() === 'spf') {
                    if (showScanDate) {
                        columnHeaders = ['Hostname', 'SPF', 'Latest Scan Date', 'Importance', 'Comments', 'Mitigated', ' '];
                    } else {
                        columnHeaders = ['Hostname', 'SPF', 'Importance', 'Comments', 'Mitigated', ' '];
                    }
                }

                if (nodeArr[0][i].labels[0].toLowerCase() === 'dmarc') {
                    if (showScanDate) {
                        columnHeaders = ['Hostname', 'DMARC', 'Latest Scan Date', 'Importance', 'Comments', 'Mitigated', ' '];
                    } else {
                        columnHeaders = ['Hostname', 'DMARC', 'Importance', 'Comments', 'Mitigated', ' '];
                    }
                }
            }

            for (let i = 0; i < columnHeaders.length; i++) {
                tableColumns[i] = {
                    sortable: true,
                    label: columnHeaders[i],
                    key: columnHeaders[i],
                    style: wrappableStyle
                };
            }

            let arrayOfUniqueCveNodes = [];

            // Loop through all nodes.
            async.each(nodeArr, (nodes, callback) => {
                    let tableDataObj = {};
                    // We need to create a different table for downloading in CSV (without objects).
                    let tableDataObjForDownload = {};
                    let dontAddToTable = false;
                    let shouldShowNodeOnTableFlag = true;
                    let curNode = {};

                    //create unique nodes in cve(network) table by ip and product
                    //for first table creation
                    if (this.props.onCveFirstTable && window.location.pathname && window.location.pathname === '/intel/cve') {
                        if (nodes[5] && nodes[5].labels[0] === 'cve') {
                            curNode = {
                                IP: nodes[2].properties.address,
                                Product: nodes[4].properties.cpe,
                                Score: nodes[5].properties.score
                            };
                        } else if (nodes[4] && nodes[4].labels[0] === 'cve') {
                            curNode = {
                                IP: nodes[1].properties.address,
                                Product: nodes[3].properties.cpe,
                                Score: nodes[4].properties.score
                            };
                        }
                        if (Object.keys(curNode).length !== 0 && curNode.constructor === Object) {
                            let found = arrayOfUniqueCveNodes.some(el =>
                                el.IP === curNode.IP &&
                                el.Product === curNode.Product);

                            if (!found) {
                                nodeArr.map(el => {
                                    if (el[2].properties.address === curNode.IP && el[4].properties.cpe === curNode.Product) {
                                        if (el[5].properties.score > curNode.Score) {
                                            curNode.Score = el[5].properties.score;
                                        }
                                    } else if (el[1].properties.address === curNode.IP && el[3].properties.cpe === curNode.Product) {
                                        if (el[4].properties.score > curNode.Score) {
                                            curNode.Score = el[4].properties.score;
                                        }
                                    }
                                });
                                arrayOfUniqueCveNodes.push(curNode);
                            } else {
                                shouldShowNodeOnTableFlag = false;
                            }
                        } else {
                            shouldShowNodeOnTableFlag = false;
                        }
                        //check cve severity for filter
                    } else if (sessionCveSeverity) {
                        if (nodes[5] && nodes[5].labels[0] === 'cve') {
                            curNode = {
                                Score: nodes[5].properties.score
                            };
                        } else if (nodes[4] && nodes[4].labels[0] === 'cve') {
                            curNode = {
                                Score: nodes[4].properties.score
                            };
                        }
                        if (sessionCveSeverity === 'Critical') {
                            shouldShowNodeOnTableFlag = Number(curNode.Score) >= CVE_CRITICAL_MIN && Number(curNode.Score) <= CVE_CRITICAL_MAX;
                        } else if (sessionCveSeverity === 'High') {
                            shouldShowNodeOnTableFlag = Number(curNode.Score) >= CVE_HIGH_MIN && Number(curNode.Score) < CVE_HIGH_MAX;
                        } else if (sessionCveSeverity === 'Medium') {
                            shouldShowNodeOnTableFlag = Number(curNode.Score) >= CVE_MEDIUM_MIN && Number(curNode.Score) < CVE_MEDIUM_MAX;
                        } else if (sessionCveSeverity === 'Low') {
                            shouldShowNodeOnTableFlag = Number(curNode.Score) < CVE_LOW_MAX;
                        }
                    }


                    if (shouldShowNodeOnTableFlag) {
                        // Loop through columns in the table and nodes
                        for (let i = tableColumns.length - (5 + showScanDateOffset), j = nodes.length - 1; i >= 0; i--, j--) {
                            // Create data objects for table
                            if (tableColumns[i] && nodes[j]) {
                                // Stringify properties, and take off excessive chars.
                                let parts = [];
                                if (nodes[j].properties.hostname) {
                                    //for no_domain node
                                    if (nodes[j].properties.hostname === "no_domain") {
                                        tableDataObj[tableColumns[i]['key']] = "no domain";
                                        tableDataObjForDownload[tableColumns[i]['key']] = "no domain";
                                    } else {
                                        let urlLink = 'http://' + nodes[j].properties.hostname.toString();
                                        tableDataObj[tableColumns[i]['key']] = <a href={urlLink} target="_blank"
                                                                                  style={{'textDecoration': 'none'}}>{nodes[j].properties.hostname.toString()}</a>;
                                        tableDataObjForDownload[tableColumns[i]['key']] = nodes[j].properties.hostname.toString();
                                    }
                                    if (isInDemoMode) {
                                        let hashVal = 'domain-example-' + exampleIdx + '.com';
                                        tableDataObj[tableColumns[i]['key']] = hashVal;
                                        tableDataObjForDownload[tableColumns[i]['key']] = hashVal;
                                        hashDataArr[hashIdx] = {
                                            real: nodes[j].properties.hostname.toString(),
                                            demo: hashVal
                                        };
                                        hashIdx++;
                                    }
                                } else if (nodes[j].properties.cpe) {
                                    let curFullNameProductValue = nodes[j].properties.cpe;
                                    let cpeArr = nodes[j].properties.cpe.split(':');
                                    let curCpeValueForUi = cpeArr[cpeArr.length - 3] + ' ' + cpeArr[cpeArr.length - 2] + ' ' + cpeArr[cpeArr.length - 1];
                                    curCpeValueForUi = curCpeValueForUi.replace(/_/gi, " ");
                                    curCpeValueForUi = curCpeValueForUi.replace("internet information server", "iis");
                                    curCpeValueForUi = curCpeValueForUi.replace("internet information services", "iis");

                                    //if text includes "iis" we add warning icon
                                    if (curCpeValueForUi.toLowerCase().includes("iis", 0)) {
                                        let toolTipText = <div
                                            style={{width: 300, whiteSpace: 'normal', textAlign: 'left'}}>
                                            The vulnerabilities are based on the software and version.
                                            Some vulnerabilities may not apply due to patches and updates.</div>;
                                        tableDataObj[tableColumns[i]['key']] = <div>
                                           <span><IconButton tooltip={toolTipText}
                                                             iconStyle={{
                                                                 width: 25,
                                                                 height: 25,
                                                                 position: 'relative',
                                                                 right: 9
                                                             }}
                                                             tooltipStyles={{marginRight: 20}}
                                                             style={{width: 25, height: 25}}
                                                             tooltipPosition="top-left">
                                                    <WarningIcon viewBox={'2 2 35 35'} color={'#db252b'}/>
                                            </IconButton>{curCpeValueForUi}</span>
                                            <span style={{
                                                display: 'none',
                                                visibility: 'hidden'
                                            }}>{curFullNameProductValue}</span></div>;
                                    } else {
                                        tableDataObj[tableColumns[i]['key']] = <div>{curCpeValueForUi}<span
                                            style={{display: 'none', visibility: 'hidden'}}>{curFullNameProductValue}</span>
                                        </div>;
                                    }
                                    tableDataObjForDownload[tableColumns[i]['key']] = curCpeValueForUi;
                                } else if (nodes[j].properties.cve) {
                                    if (this.props.onCveFirstTable && window.location.pathname && window.location.pathname === '/intel/cve') {
                                        let RiskScoreChip = this.setRiskScoreChip(curNode.Score);
                                        tableDataObj[tableColumns[tableColumns.length - (5 + showScanDateOffset)]['key']] =
                                            <Chip backgroundColor={RiskScoreChip.color}
                                                  labelColor={'#FFFFFF'}>{RiskScoreChip.label}</Chip>;
                                    } else {
                                        let color = this.setCVSSColor(+nodes[j].properties.score);
                                        let score = nodes[j].properties.score ? nodes[j].properties.score.split(':').pop() : nodes[j].properties.cve;
                                        tableDataObj[tableColumns[i]['key']] =
                                            <Chip backgroundColor={color} labelColor={'#FFFFFF'}>{score}</Chip>;
                                        tableDataObjForDownload[tableColumns[i]['key']] = score;
                                        i--;
                                        let cveId = nodes[j].properties.cve.split(':').pop();
                                        let cveLink = 'https://nvd.nist.gov/vuln/detail/' + cveId;
                                        tableDataObj[tableColumns[i]['key']] =
                                            <a href={cveLink} target="_blank"
                                               style={{'textDecoration': 'none'}}
                                               onClick={this.clickNVDLink.bind(this)}
                                            >{cveId}</a>;
                                        tableDataObjForDownload[tableColumns[i]['key']] = cveLink;
                                    }
                                } else if (nodes[j].properties.ports) {
                                    tableDataObj[tableColumns[i]['key']] = nodes[j].properties.ports;
                                    tableDataObjForDownload[tableColumns[i]['key']] = nodes[j].properties.ports;
                                } else if (nodes[j].properties.spf) {
                                    if (nodes.length === 2) {
                                        tableDataObj[tableColumns[i]['key']] = nodes[j].properties.spf;
                                        tableDataObjForDownload[tableColumns[i]['key']] = nodes[j].properties.spf;
                                    } else {
                                        dontAddToTable = true;
                                    }
                                } else if (nodes[j].properties.dmarc) {
                                    tableDataObj[tableColumns[1]['key']] = nodes[j].properties.dmarc;
                                    tableDataObjForDownload[tableColumns[1]['key']] = nodes[j].properties.dmarc;
                                } else if (nodes[j].properties.name !== 'mechanisms' && nodes[j].properties.name !== '[object Object]') {
                                    if (nodes[0].properties.hostname === "no_domain" && nodes[j].properties.address && nodes[j].properties.companyName) {
                                        if (isInDemoMode) {
                                            let hashVal = generalFunctions.createIpStrForDemoMode(nodes[j].properties.address, exampleIdx);
                                            tableDataObj[tableColumns[i]['key']] = hashVal;
                                            tableDataObjForDownload[tableColumns[i]['key']] = hashVal;
                                            hashDataArr[hashIdx] = {real: nodes[j].properties.address, demo: hashVal};
                                            hashIdx++;
                                        } else {
                                            tableDataObj[tableColumns[i]['key']] = nodes[j].properties.address;
                                            tableDataObjForDownload[tableColumns[i]['key']] = nodes[j].properties.address;
                                        }
                                    } else {
                                        parts = JSON.stringify(nodes[j].properties).replace(/{/g, '').replace(/}/g, '').replace(/"/g, '').split(':');
                                        let val = parts.pop();
                                        if (parts && parts[0] && parts[0] === 'address' || parts[0] === 'bucket') {
                                            if (isInDemoMode) {
                                                let hashVal = '';
                                                if (parts[0] === 'address') {
                                                    hashVal = generalFunctions.createIpStrForDemoMode(val, exampleIdx);
                                                } else {
                                                    hashVal = 'bucket_example_' + exampleIdx;
                                                }
                                                tableDataObj[tableColumns[i]['key']] = hashVal;
                                                tableDataObjForDownload[tableColumns[i]['key']] = hashVal;
                                                hashDataArr[hashIdx] = {real: val, demo: hashVal};
                                                hashIdx++;
                                            } else {
                                                tableDataObj[tableColumns[i]['key']] = val;
                                                tableDataObjForDownload[tableColumns[i]['key']] = val;
                                            }
                                        } else {
                                            tableDataObj[tableColumns[i]['key']] = val;
                                            tableDataObjForDownload[tableColumns[i]['key']] = val;
                                        }
                                    }
                                }
                            } else {
                                tableDataObj[tableColumns[1]['key']] = '';
                                tableDataObjForDownload[tableColumns[1]['key']] = '';
                                if (isInDemoMode) {
                                    let hashVal = 'domain-example-' + exampleIdx + '.com';
                                    tableDataObj[tableColumns[0]['key']] = hashVal;
                                    tableDataObjForDownload[tableColumns[0]['key']] = hashVal;
                                    hashDataArr[hashIdx] = {real: nodes[0].properties.hostname.toString(), demo: hashVal};
                                    hashIdx++;
                                } else {
                                    tableDataObj[tableColumns[0]['key']] = nodes[0].properties.hostname.toString();
                                    tableDataObjForDownload[tableColumns[0]['key']] = nodes[0].properties.hostname.toString();
                                }
                            }

                            //Add last scan Date to table
                            if (showScanDate) {
                                tableDataObj[tableColumns[tableColumns.length - 5]['key']] = moment(latestScanDate).format('YYYY/MM/DD, HH:mm:ss');
                            }
                        }

                        let importance = 3;
                        let mitigatedBackground = 'black';
                        let comments = [];
                        let assetUserInputFlag = false;
                        let mitigatedBackgroundFlag = false;
                        let commentsFlag = false;
                        for (let i = 0; i < assetUserInputAndMitigatedData.length; i++) {
                            if (this.isCurRowNeedToBeUpdateCurNode(tableDataObj, assetUserInputAndMitigatedData[i])) {
                                if (assetUserInputAndMitigatedData[i].type && assetUserInputAndMitigatedData[i].type === 'assetUserInput') {
                                    importance = assetUserInputAndMitigatedData[i].importance ? Number(assetUserInputAndMitigatedData[i].importance) : 3;
                                    assetUserInputFlag = true;
                                }
                                if (assetUserInputAndMitigatedData[i].type && assetUserInputAndMitigatedData[i].type === 'mitigated') {
                                    mitigatedBackground = '#5bd25e';
                                    mitigatedBackgroundFlag = true;
                                }
                                if (assetUserInputAndMitigatedData[i].comments) {
                                    comments = assetUserInputAndMitigatedData[i].comments ? assetUserInputAndMitigatedData[i].comments : [];
                                    commentsFlag = true;
                                }
                                if (mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag) {
                                    break;
                                }
                            }
                        }

                        if (!dontAddToTable) {
                            //graph icon adding to the last column
                            if (this.props.onCveFirstTable && window.location.pathname && window.location.pathname === '/intel/cve') {
                                tableDataObj[tableColumns[tableColumns.length - 1]['key']] = <TableIcon/>;
                            } else {
                                tableDataObj[tableColumns[tableColumns.length - 1]['key']] =
                                    <div><GraphIcon/><span style={{marginLeft: 10}}>Show Graph</span></div>;
                            }
                            if (nodeArr[0][1].labels[0].toLowerCase() === 'spf' || nodeArr[0][1].labels[0].toLowerCase() === 'dmarc') {
                                tableDataObj[tableColumns[tableColumns.length - 1]['key']] = '';
                            }
                            //Change comments icon color according to number of comments.
                            let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';

                            //Add asset configuration components to table (mitigated, comments and Importance).
                            tableDataObj[tableColumns[tableColumns.length - 4]['key']] = this.getImportanceComponent(tableDataObj, importance);
                            tableDataObj[tableColumns[tableColumns.length - 3]['key']] = this.getCommentsComponent(tableDataObj, comments.length, comments, badgeColor);
                            tableDataObj[tableColumns[tableColumns.length - 2]['key']] = this.getMitigatedComponent(tableDataObj, mitigatedBackground);

                            //This a change we have to do for when downloading table as CSV.
                            tableDataObjForDownload[tableColumns[tableColumns.length - 4]['key']] = generalFunctions.getImportanceStr(importance);
                            tableDataObjForDownload[tableColumns[tableColumns.length - 3]['key']] = generalFunctions.allCommentsToOneStr(comments);
                            tableDataObjForDownload[tableColumns[tableColumns.length - 2]['key']] = mitigatedBackgroundFlag ? 'Yes' : 'No';

                            let isNeedToBeAdded = true;
                            for (let k = 0; k < tableData.length; k++) {
                                if (this.isAlreadyExistInTableData(tableData[k], tableDataObj)) {
                                    isNeedToBeAdded = false;
                                    break;
                                }
                            }

                            if (isNeedToBeAdded) {
                                // Push data objects to array.
                                tableData.push(tableDataObj);
                                tableDataForDownload.push(tableDataObjForDownload);
                            }
                        }
                    }
                    exampleIdx++;
                    callback();
                }
            );
            //sort data by highest cve risk when coming from dashboard top vulnerable domains OR from cve severity panel
            if (((columnHeaders[4] && columnHeaders[4].toLowerCase() === 'cve')
                && window.location.pathname
                && window.location.pathname !== '/intel/cve') || sessionCveSeverity) {
                this.props.sortDataByCveHighestRisk(tableData);
            }

            this.setState({tableData: null});
            this.setState({
                hashDataArr: hashDataArr,
                tableColumns: tableColumns,
                totalRowCount: tableData.length,
                tableData: tableData,
                dataForDownload: tableDataForDownload,
                sortedTableData: tableData.slice(0, this.state.rowCountInPage),
                nodeArr: nodeArr
            });
        } else {
            // Show empty results.
            this.setState({
                tableColumns: [{key: 'label', label: 'No data to display.'}],
                hashDataArr: [],
                totalRowCount: 0,
                tableData: [],
                dataForDownload: [{'No data to display.': ''}],
                sortedTableData: [],
                nodeArr: []
            });
        }
    }

    isAlreadyExistInTableData(curObj, objToCompare) {
        let tableType = this.props.intelTableType;
        let isConditionTrue = false;
        switch (tableType) {
            case 'IP': {
                isConditionTrue = (objToCompare && objToCompare.IP &&
                    curObj && curObj.IP &&
                    objToCompare.Source && objToCompare.Source.props && objToCompare.Source.props.children && objToCompare.Source.props.children &&
                    curObj.Source && curObj.Source.props && curObj.Source.props.children &&
                    objToCompare.Source.props.children === curObj.Source.props.children &&
                    objToCompare.IP === curObj.IP);
                break;
            }
            case 'ports': {
                isConditionTrue = (objToCompare && objToCompare.IP && objToCompare.ports &&
                    curObj && curObj.IP && curObj.ports &&
                    objToCompare.Hostname && objToCompare.Hostname.props && objToCompare.Hostname.props.children && objToCompare.Hostname.props.children &&
                    curObj.Hostname && curObj.Hostname.props && curObj.Hostname.props.children &&
                    objToCompare.IP === curObj.IP && objToCompare.ports === curObj.ports &&
                    objToCompare.Hostname.props.children === curObj.Hostname.props.children);
                break;
            }
            case 'buckets': {
                isConditionTrue = (objToCompare && objToCompare.bucket &&
                    curObj && curObj.bucket &&
                    objToCompare.Hostname && objToCompare.Hostname.props && objToCompare.Hostname.props.children && objToCompare.Hostname.props.children &&
                    curObj.Hostname && curObj.Hostname.props && curObj.Hostname.props.children &&
                    objToCompare.Hostname.props.children === curObj.Hostname.props.children &&
                    objToCompare.bucket === curObj.bucket);
                break;
            }
            case 'product': {
                isConditionTrue = (objToCompare && objToCompare.IP && objToCompare.ports && objToCompare.product &&
                    curObj && curObj.IP && curObj.ports && curObj.product &&
                    objToCompare.IP === curObj.IP && objToCompare.ports === curObj.ports && objToCompare.product === curObj.product);
                break;
            }
            case 'isp': {
                isConditionTrue = (objToCompare && objToCompare.IP && objToCompare.isp &&
                    curObj && curObj.IP && curObj.isp &&
                    objToCompare.Hostname && objToCompare.Hostname.props && objToCompare.Hostname.props.children && objToCompare.Hostname.props.children &&
                    curObj.Hostname && curObj.Hostname.props && curObj.Hostname.props.children &&
                    objToCompare.IP === curObj.IP && objToCompare.isp === curObj.isp &&
                    objToCompare.Hostname.props.children === curObj.Hostname.props.children);
                break;
            }
            case 'cve': {
                let objToCompereProductVal;
                //because Product is an object with 2 props, one text for ui: "php 5.6.40"(can include the iis icon too), and one with the full name: "cpe:/a:php:php:5.6.40"
                //we check if the the full name prop is exist and use it.
                if (objToCompare.Product && objToCompare.Product !== 'string' && objToCompare.Product.props && objToCompare.Product.props.children &&
                    objToCompare.Product.props.children[1] && objToCompare.Product.props.children[1].props &&
                    objToCompare.Product.props.children[1].props.children) {
                    objToCompereProductVal = objToCompare.Product.props.children[1].props.children;
                }

                let curObjProductVal;
                //because Product is an object with 2 props, one text for ui: "php 5.6.40"(can include the iis icon too), and one with the full name: "cpe:/a:php:php:5.6.40"
                //we check if the the full name prop is exist and use it.
                if (objToCompare.Product && objToCompare.Product !== 'string' && objToCompare.Product.props && objToCompare.Product.props.children &&
                    objToCompare.Product.props.children[1] && objToCompare.Product.props.children[1].props &&
                    objToCompare.Product.props.children[1].props.children) {
                    curObjProductVal = objToCompare.Product.props.children[1].props.children;
                }
                if (this.props.onCveFirstTable) {
                    isConditionTrue = (objToCompare && objToCompare.IP && objToCompare.Port && objToCompereProductVal &&
                        curObj && curObj.IP && curObj.Port && curObjProductVal &&
                        objToCompare.Hostname && objToCompare.Hostname.props && objToCompare.Hostname.props.children && objToCompare.Hostname.props.children &&
                        curObj.Hostname && curObj.Hostname.props && curObj.Hostname.props.children &&
                        objToCompare.IP === curObj.IP && objToCompare.Port === curObj.Port &&
                        objToCompare.Hostname.props.children === curObj.Hostname.props.children &&
                        objToCompereProductVal === curObjProductVal);
                } else {
                    isConditionTrue = (objToCompare && objToCompare.IP && objToCompare.Port && objToCompereProductVal &&
                        curObj && curObj.IP && curObj.Port && curObjProductVal &&
                        objToCompare.Hostname && objToCompare.Hostname.props && objToCompare.Hostname.props.children && objToCompare.Hostname.props.children &&
                        objToCompare.CVE && objToCompare.CVE.props && objToCompare.CVE.props.children && objToCompare.CVE.props.children &&
                        curObj.Hostname && curObj.Hostname.props && curObj.Hostname.props.children &&
                        curObj.CVE && curObj.CVE.props && curObj.CVE.props.children && curObj.CVE.props.children &&
                        objToCompare.IP === curObj.IP && objToCompare.Port === curObj.Port &&
                        objToCompare.Hostname.props.children === curObj.Hostname.props.children &&
                        objToCompare.CVE.props.children === curObj.CVE.props.children &&
                        objToCompereProductVal === curObjProductVal);
                }
                break;
            }
            case 'spf': {
                isConditionTrue = (objToCompare && objToCompare.SPF &&
                    curObj && curObj.SPF &&
                    objToCompare.Hostname && objToCompare.Hostname.props && objToCompare.Hostname.props.children && objToCompare.Hostname.props.children &&
                    curObj.Hostname && curObj.Hostname.props && curObj.Hostname.props.children &&
                    objToCompare.Hostname.props.children === curObj.Hostname.props.children &&
                    objToCompare.SPF === curObj.SPF);
                break;
            }
            case 'dmarc': {
                isConditionTrue = (objToCompare && objToCompare.DMARC &&
                    curObj && curObj.DMARC &&
                    objToCompare.Hostname && objToCompare.Hostname.props && objToCompare.Hostname.props.children && objToCompare.Hostname.props.children &&
                    curObj.Hostname && curObj.Hostname.props && curObj.Hostname.props.children &&
                    objToCompare.Hostname.props.children === curObj.Hostname.props.children &&
                    objToCompare.DMARC === curObj.DMARC);
                break;
            }
            default:
                isConditionTrue = false;
        }
        return isConditionTrue;
    }

    setRiskScoreChip(score) {
        if (score >= CVE_CRITICAL_MIN && score <= CVE_CRITICAL_MAX) {
            return {label: 'Critical', color: '#d32f2f'};
        }

        if (score >= CVE_HIGH_MIN && score < CVE_HIGH_MAX) {
            return {label: 'High', color: '#F4511E'};
        }

        if (score >= CVE_MEDIUM_MIN && score < CVE_MEDIUM_MAX) {
            return {label: 'Medium', color: '#F57C00'};
        }

        if (score < CVE_LOW_MAX) {
            return {label: 'Low', color: '#0091ea'};
        }
    }

    setCVSSColor(score) {
        if (score >= CVE_CRITICAL_MIN && score <= CVE_CRITICAL_MAX) {
            return '#d32f2f';
        }

        if (score >= CVE_HIGH_MIN && score < CVE_HIGH_MAX) {
            return '#F4511E';
        }

        if (score >= CVE_MEDIUM_MIN && score < CVE_MEDIUM_MAX) {
            return '#F57C00';
        }

        if (score < CVE_LOW_MAX) {
            return '#0091ea';
        }
    }

    handleSortOrderChange(key, order) {
        this.setState({orderKey: key, orderType: order});
        let tableData = this.state.tableData;
        let textA;
        let textB;

        let getSortResult;
        if (order === 'asc') {
            getSortResult = (textA, textB) => {
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            };
        } else {
            getSortResult = (textA, textB) => {
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            };
        }

        tableData.sort((a, b) => {
            let keyA, keyB;

            //using color the order is fp -> mitigated -> default or backwards
            if (key === 'Importance') {
                if (a[key] && a[key].props && a[key].props.value) {
                    if (a[key].props.value === 4) {
                        keyA = 4;
                    } else if (a[key].props.value === 3) {
                        keyA = 3;
                    } else if (a[key].props.value === 2) {
                        keyA = 2;
                    } else if (a[key].props.value === 1) {
                        keyA = 1;
                    } else {
                        keyA = 0;
                    }
                }

                if (b[key] && b[key].props && b[key].props.value) {
                    if (b[key].props.value === 4) {
                        keyB = 4;
                    } else if (b[key].props.value === 3) {
                        keyB = 3;
                    } else if (b[key].props.value === 2) {
                        keyB = 2;
                    } else if (b[key].props.value === 1) {
                        keyB = 1;
                    } else {
                        keyB = 0;
                    }
                }
            } else if (key === 'Mitigated') {
                if (a[key] && a[key].props && a[key].props.iconStyle && a[key].props.iconStyle.color) {
                    if (a[key].props.iconStyle.color === 'black') {
                        keyA = '1';
                    } else {
                        keyA = '2';
                    }
                }
                if (b[key] && b[key].props && b[key].props.iconStyle && b[key].props.iconStyle.color) {
                    if (b[key].props.iconStyle.color === 'black') {
                        keyB = '1';
                    } else {
                        keyB = '2';
                    }
                }
            } else {
                // In case value is object and not a string or number.
                if (a[key] && a[key].props && a[key].props.hasOwnProperty("children")
                    && a[key].props.children && a[key].props.children[1]
                    && a[key].props.children[1].props && a[key].props.children[1].props.children) {
                    keyA = a[key].props.children[1].props.children;
                    // In case value is react object and not a string or number.
                } else if (a[key].props && a[key].props.hasOwnProperty("children")) {
                    keyA = a[key].props.children;
                } else {
                    keyA = a[key] || "";
                }

                // In case value is object and not a string or number.
                if (b[key] && b[key].props && b[key].props.hasOwnProperty("children")
                    && b[key].props.children && b[key].props.children[1]
                    && b[key].props.children[1].props && b[key].props.children[1].props.children) {
                    keyB = b[key].props.children[1].props.children;
                    // In case value is react object and not a string or number.
                } else if (b[key].props && b[key].props.hasOwnProperty("children")) {
                    keyB = b[key].props.children;
                } else {
                    keyB = b[key] || "";
                }
            }


            // Checks if not a number
            if (!isNumeric(keyA) && typeof keyA !== 'object') {
                textA = keyA.toUpperCase();
            } else {
                textA = +keyA;
            }

            // Checks if not a number
            if (!isNumeric(keyB) && typeof keyB !== 'object') {
                textB = keyB.toUpperCase();
            } else {
                textB = +keyB;
            }

            return getSortResult(textA, textB);
        });

        this.setState({
            sortedTableData: tableData.slice((this.state.page - 1) * this.state.rowCountInPage,
                (this.state.page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handleFilterValueChange(value) {
        // Get labels to search for when fuzzy searching.
        const keys = this.state.tableColumns.map((el) => {
            return el['label'];
        });

        let dataTableWithoutLastColumn = this.state.tableData;
        delete dataTableWithoutLastColumn[' '];

        //in case Hostname/Source are link objs we create another column of the Hostname/Source string so we use the search on them
        if (keys && keys.map((key) => {
            return key === 'Hostname' || key === 'Source';
        })) {
            dataTableWithoutLastColumn.map((row) => {
                if (row['Hostname'] && row['Hostname'].props && row['Hostname'].props.children) {
                    row['urlStr'] = row['Hostname'].props.children;
                } else if (row['Source'] && row['Source'].props && row['Source'].props.children) {
                    row['urlStr'] = row['Source'].props.children;
                }
            });
            keys.push('urlStr');
        }

        // Fuzzy search in tableData objects in every key.
        const searcher = new FuzzySearch(dataTableWithoutLastColumn, keys, {
            caseSensitive: false
        });

        const result = searcher.search(value);

        //when finish we remove the column
        result.map((row) => {
            delete row['urlStr'];
        });

        this.setState({
            sortedTableData: result
        });
    }

    handleCellClick(x, y, row) {
        let isAssetUserInputOrMitigatedBtnClicked = this.state.isAssetUserInputOrMitigatedBtnClicked;
        if (isAssetUserInputOrMitigatedBtnClicked !== undefined && !isAssetUserInputOrMitigatedBtnClicked && !(window.location.pathname && (window.location.pathname === '/intel/spf' || window.location.pathname === '/intel/dmarc'))) {
            this.props.changeShownView(row, this.state.assetUserInputAndMitigatedData, this.state.hashDataArr);
            this.setResults(this.props.data);
        }
    }

    updateLastPageUserWasOn() {
        let page = this.state.page;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
                (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handleNextPageClick() {
        let page = this.state.page + 1;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
                (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handlePreviousPageClick() {
        let page = this.state.page - 1;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice(page * this.state.rowCountInPage - this.state.rowCountInPage,
                page * this.state.rowCountInPage)
        });
    }

    handleRowSizeChange(ignore, rowCount) {
        let page = this.state.page;
        if (page > 1 && page > this.state.tableData.length / rowCount) {
            page--;
        }

        this.setState({
            rowCountInPage: rowCount,
            sortedTableData: this.state.tableData.slice((page - 1) * rowCount, (page - 1) * rowCount + rowCount),
            rowSize: rowCount,
            page: page
        });
    }

    handleOpenSettings() {
        this.setState({
            openSettingsModal: true
        });
    }

    closeSettingsDialog() {
        this.setState({openSettingsModal: false});
    }

    startFilter(companiesFilter) {
        let startD = this.state.startDate;
        let endD = this.state.endDate;
        this.props.startFilter(startD, endD, companiesFilter);
    }

    handleSelect(range) {
        // An object with two keys,
        // 'startDate' and 'endDate' which are Momentjs objects.
        this.setState({startDate: range.startDate, endDate: range.endDate});
    }

    openDateFilters(event) {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            openMenu: true,
            anchorEl: event.currentTarget
        });
    }

    handleCloseMenu() {
        this.setState({
            startDate: undefined,
            endDate: undefined,
            openMenu: false
        });
    }

    render() {
        let infoCardStyle = {
            minHeight: '40px',
            margin: 'auto'
        };

        let style = {
            padding: '10px',
            float: 'right'
        };

        let downloadStyle = {
            cursor: 'pointer'
        };

        let tableBodyStyle = {
            'overflowX': 'auto'
        };

        let tableStyle;

        if (window.location.pathname && (window.location.pathname === '/intel/spf' || window.location.pathname === '/intel/dmarc')) {
            tableStyle = {
                'width': '95%',
                'margin': 'auto'
            };
        } else {
            tableStyle = {
                'width': '95%',
                'margin': 'auto',
                'cursor': 'pointer'
            };
        }


        const resultStyle = {
            backgroundColor: '#FFDD00',
            display: 'inline-block',
            padding: '1% 5%',
            marginTop: '240px',
            fontSize: 'large',
            textAlign: 'center'
        };

        const companiesSelectorStyle = {
            textAlign: 'left'
        };
        const settingsBtnStyle = {
            'marginTop': '0px',
            'marginLeft': '10px'
        };
        const settingsChipsStyle = {
            'display': 'inline-flex'
        };
        let datePickerStyle = {
            minHeight: '40px',
            margin: 'auto',
            padding: '10px',
            display: 'inline-flex',
            alignItems: 'center'
        };

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        let resultClassName = classNames('search-results-container');

        let companiesSelectorComponent = <div style={companiesSelectorStyle}>
            <IconButton tooltip="Filter by companies" touch={true} tooltipPosition="bottom-right"
                        style={settingsBtnStyle}>
                <SettingsButton
                    onTouchTap={this.handleOpenSettings.bind(this)}
                />
            </IconButton>
            <div style={settingsChipsStyle}>
                {showExpandedDataChip(this.props.companiesInputVal && this.props.companiesInputVal.map((cmp) => {
                    return (cmp.companyName);
                }), 8, 5, 'Companies')}
            </div>
        </div>;

        let emptyResultIndicator = this.state.showNoDataMessage ? (
            <span className={resultClassName}>
                {companiesSelectorComponent}
                <Paper style={resultStyle} zDepth={4}><span style={{verticalAlign: 'middle'}}>No data to display.</span></Paper>
            </span>
        ) : '';

        let filterShow = {'display': this.props.singleDomain ? 'none' : 'block'};

        const spinner = <div className={spinnerClass}/>;

        let findingStatusDialogText = this.state.findingStatusDialogText;
        if (findingStatusDialogText && findingStatusDialogText.toLocaleLowerCase() === 'importance') {
            findingStatusDialogText = 'importance/status';
        }

        let dataType = this.state.intelTableType;
        if (dataType && typeof dataType === 'string') {
            if (dataType === 'IP') {// DOMAINS/IP
                dataType = 'ip\'s';
            } else if (dataType === 'ports') { // OPEN PORTS
                dataType = 'port\'s';
            } else if (dataType === 'buckets') {// S3 BUCKETS
                dataType = 'bucket\'s';
            } else if (dataType === 'product') {// DISCOVERED SYSTEMS
                dataType = 'product\'s';
            } else if (dataType === 'isp') {// ISP/HOSTING PROVIDER
                dataType = 'isp\'s';
            } else if (dataType === 'cve') {// NETWORK
                if (this.props.onCveFirstTable) {
                    dataType = 'cpe\'s';
                } else {
                    dataType = 'cve\'s';
                }
            } else if (dataType === 'spf') {// SPF RECORDS
                dataType = 'spf\'s';
            } else if (dataType === 'dmarc') {// DMARC RECORDS
                dataType = 'dmarc\'s';
            } else {
                dataType = '';
            }
        }

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div style={style}>
                    <BreadCrumbs
                        pathArr={[this.state.domain]}
                        show={this.props.singleDomain}
                    />
                    <div style={datePickerStyle}>
                        <IconButton tooltip="Filter by dates" touch={true} tooltipPosition="bottom-right"
                                    onTouchTap={this.openDateFilters.bind(this)}>
                            <ActionDates/>
                        </IconButton>

                        <label>{this.props.datesRange}</label>
                        <Popover
                            open={this.state.openMenu}
                            anchorEl={this.state.anchorEl}
                            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                            targetOrigin={{horizontal: 'left', vertical: 'top'}}
                            onRequestClose={this.handleCloseMenu.bind(this)}>
                            <div>
                                <DatePicker
                                    onChange={this.handleSelect.bind(this)}
                                />
                                <div style={{float: 'right', margin: '5px'}}>
                                    <FlatButton label="Cancel" secondary={true}
                                                onTouchTap={this.handleCloseMenu.bind(this)}/>
                                    <FlatButton label="Go" secondary={true} onTouchTap={() => this.startFilter()}/>
                                </div>
                            </div>
                        </Popover>
                        {emptyResultIndicator}
                        <span style={filterShow}>{companiesSelectorComponent}</span>
                    </div>
                    <DataTableTemplateSettingsModal
                        open={this.state.openSettingsModal}
                        closeDialog={this.closeSettingsDialog.bind(this)}
                        title={'Filter By Companies'}
                        companies={this.props.companies}
                        companiesInputVal={this.props.companiesInputVal}
                        ApplyChangesFromSettings={this.props.ApplyChangesFromSettings}
                        myDataType={this.props.myDataType}
                    />
                    <Card style={infoCardStyle}>
                        <Loader show={this.state.showLoader || false} message={spinner}>
                            <DataTables
                                height={'auto'}
                                selectable={false}
                                showHeaderToolbar={true}
                                tableBodyStyle={tableBodyStyle}
                                tableStyle={tableStyle}
                                columns={this.state.tableColumns}
                                data={this.state.sortedTableData}
                                showCheckboxes={false}
                                showRowHover={true}
                                toolbarIconRight={[
                                    <div>
                                        <CSVLink
                                            data={this.state.dataForDownload}
                                            filename={'rescana_intel.csv'}>
                                            <FileDownloadIcon
                                                style={downloadStyle}
                                            >
                                            </FileDownloadIcon>
                                        </CSVLink>
                                    </div>
                                ]}
                                onCellClick={this.handleCellClick}
                                onFilterValueChange={this.handleFilterValueChange}
                                onSortOrderChange={this.handleSortOrderChange}
                                onNextPageClick={this.handleNextPageClick}
                                onRowSizeChange={this.handleRowSizeChange}
                                onPreviousPageClick={this.handlePreviousPageClick}
                                headerToolbarMode={'filter'}
                                page={this.state.page}
                                rowSize={this.state.rowSize}
                                count={this.state.totalRowCount}
                            />
                        </Loader>
                        <DynamicDialog
                            open={this.state.isFindingStatusDialogOpen}
                            title={'Change Finding Status'}
                            bodyText={'Are you sure you wish to change the ' + dataType + ' ' + findingStatusDialogText + '?'}
                            btnOkLable={'Yes'}
                            btnCancelLable={'No'}
                            cancelBtnEnabled={true}
                            handlebBtnOkClick={this.updateFindingStatus.bind(this)}
                            handlebBtnCancelClick={this.closeFindingDialog.bind(this)}
                        />
                        <AssetUserInputCommentsDialog
                            open={this.state.isCommentsDialogOpen}
                            title={'Comments'}
                            updateCommentsArea={this.handleUpdateCommentsArea.bind(this)}
                            handlebBtnCloseClick={this.handleCloseCommentsDialog.bind(this)}
                            curCommentsArr={this.state.curCommentsArr || []}
                            isFromMitigated={this.state.isFromMitigated}
                        />
                    </Card>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default TableResultsContainer;
