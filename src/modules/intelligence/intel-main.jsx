import React from 'react';
import GraphResultsContainer from './graph-results-container.jsx';
import TableResultsContainer from './table-results-container.jsx';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import classNames from 'classnames';
import {browserHistory} from 'react-router';
import FlatButton from 'material-ui/FlatButton';
import {
    ORGANIZATION_ID,
    PROJECT_NAME,
    SESSION_COMPANIES,
    SESSION_CVE_SEVERITY_PANEL,
    SESSION_CVE_SEVERITY, SESSION_FIRST_CVE_TABLE
} from '../../../config/consts';
import '../../../public/css/main.css';
import {getDefaultCompanies} from '../common/datatableTemplate/datatable-template-helpers.js';
import {fixTimezoneRange} from '../common/CommonHelpers.js';
import themeForFont from "../app/themes/themeForFont";
import {isNumeric} from "../common/CommonHelpers";
import textForTitleExpl from '../appGeneralFiles/text-for-title-explanations.js';
import TitleExplanationComponent from '../appGeneralFiles/title-explanation-component.jsx';
import generalFunctions from "../generalComponents/general-functions.js";

export class Intelligence extends React.Component {
    constructor() {
        super();
        this.state = {
            data: [],
            menuItemCallerName: '',
            searchResultsData: null,
            showLoader: false,
            filterIndex: 0,
            showGraphView: true,
            showNoDataMessage: false,
            companiesInputVal: [],
            selectedData: [],
            onCveFirstTable: true,
            onCveSecondTable: false,
            isMounted: false,
            assetUserInputAndMitigatedData: [],
            showScanDate: false,
            openInfoBox: false
        };

        this.getMenuItemCallerName = this.getMenuItemCallerName.bind(this);
        this.submitQuery = this.submitQuery.bind(this);
        this.toggleLoader = this.toggleLoader.bind(this);
        this.handleUnlisten = this.handleUnlisten.bind(this);
        this.changeShownView = this.changeShownView.bind(this);
        this.handleCheckIfDisable = this.handleCheckIfDisable.bind(this);
        this.recalculateScore = this.recalculateScore.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        //check if we redirect from menu and need to see first table
        let showFirstTable = sessionStorage.getItem(SESSION_FIRST_CVE_TABLE);
        if (showFirstTable === 'true') {
            this.setState({onCveFirstTable: true, onCveSecondTable: false});
        }
    }

    componentDidMount() {
        let user = app.getAuthUser();
        this.setState({isMounted: true, user: user});
        if (!user || user.userRole === 'basic') {
            app.routeAuthUser();
        } else {
            let showGraphView = false;

            this.setState({showGraphView: showGraphView},
                // This function will call handleUnlisten at the end.
                this.setDefaultCompanies
            );
        }
    }

    recalculateScore() {
        let data = {};

        if (this.state.companiesInputVal && this.state.companiesInputVal.length > 0) {
            data.companyIDs = [];
            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        data.orgId = localStorage.getItem(ORGANIZATION_ID);
        data.saveScoresOnDB = true;
        $.ajax({
            type: 'POST',
            url: '/api/getMaxCVSScore',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'Calculated Score');
            } else {
                app.addAlert('Error', 'Error calculating score');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to calculate score');
        });
    }

    handleUnlisten() {
        let unlisten = browserHistory.listen((location) => {
                let pathNameRoutes = location.pathname.split('/');
                let params = pathNameRoutes.pop();
                if (params) {
                    this.setState({
                        menuItemCallerName: params
                    });
                }
                let clearData = [];
                if (pathNameRoutes) {
                    let path = pathNameRoutes.pop();

                    if (path === 'intel') {
                        this.setState({singleDomain: false, intelType: params, data: clearData}, () => {
                            this.runFilter(params, this.state.companiesInputVal);
                        });
                    }

                    let cat = pathNameRoutes.pop();
                    // e.g /intel/cve/domain/bla.com
                    if (cat === 'cve' && path === 'domain') {
                        this.setState({singleDomain: true, data: clearData}, () => {
                            this.runFilter(path, params, cat);
                        });
                    }
                }
            }
        );

        this.setState({
            unlisten: unlisten
        });
    }

    componentWillUnmount() {
        if (this.state.unlisten) {
            this.state.unlisten();
        }
        this.setState({isMounted: false})
    }

    setDefaultCompanies() {
        let pn = localStorage.getItem(PROJECT_NAME);
        if (pn) {
            $.ajax({
                type: 'POST',
                url: '/api/getCompaniesByProjectName',
                async: true,
                data: JSON.stringify({project: pn}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((projectCompanies) => {
                this.setCompaniesToState(projectCompanies);
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load project\'s companies.');
            });
        }
    }

    setCompaniesToState(projectCompanies) {
        let result = getDefaultCompanies(projectCompanies, null, 'companies');
        const cvePanel = sessionStorage.getItem(SESSION_CVE_SEVERITY_PANEL);
        let companiesInputVal;
        if (result) {
            //check if the page called from main dashboard cve severity panel, if yes use all companies otherwise filter the companies
            if (result.companies && result.companiesInputVal && result.companiesPropArray) {
                if (cvePanel && cvePanel === "mainDashboard") {
                    companiesInputVal = result.companiesInputVal;
                } else if (result.companiesInputVal.length > 0) {
                    // If the Session has a selection saved - load it.
                    let sessionCompanies = sessionStorage.getItem(SESSION_COMPANIES);
                    if (sessionCompanies) {

                        let selectedNames = sessionCompanies.split(', ');
                        let relatedCompanies = result.companiesInputVal.filter((c) => {
                            return selectedNames.includes(c.companyName);
                        });
                        if (relatedCompanies && relatedCompanies.length > 0) {
                            result.companiesInputVal = relatedCompanies;
                            companiesInputVal = result.companiesInputVal;
                        }
                    } else {
                        // Select only the first company as a default selection.
                        //result.companiesInputVal.splice(1, result.companiesInputVal.length);
                        companiesInputVal = result.companiesInputVal;
                    }
                }
            }

            this.setState({
                companies: result.companies,
                companiesInputVal: companiesInputVal
            }, this.handleUnlisten);
        }
    }

    runFilter(params, filter, category) {
        if (params) {
            if (this.refs && this.refs.graphResultsContainer) {
                this.refs.graphResultsContainer.clearDatesRange();
                if (this.refs.graphResultsContainer.refs && this.refs.graphResultsContainer.refs.VisNetwork) {
                    this.refs.graphResultsContainer.refs.VisNetwork.closeNodeInfo();
                }

                if (params === 'cve') {
                    this.getGraph('getVulnsByIP', filter);
                } else if (params === 'ports') {
                    this.getGraph('getPortsByIP', filter);
                } else if (params === 'asn') {
                    this.getGraph('getASNByIP', filter);
                } else if (params === 'IP') {
                    this.getGraph('getAllIPs', filter);
                } else if (params === 'product') {
                    this.getGraph('getProductsByIP', filter);
                } else if (params === 'VPN') {
                    this.getGraph('getVPNByIP', filter);
                } else if (params === 'spf') {
                    this.getGraph('getSpf', filter);
                } else if (params === 'dmarc') {
                    this.getGraph('getDmarc', filter);
                } else if (params === 'isp') {
                    this.getGraph('getIsp', filter);
                } else if (params === 'buckets') {
                    this.getGraph('getBuckets', filter);
                } else if (params === 'domain') {
                    this.getGraph('getByDomain', filter, category);
                } else {
                    console.log('Unknown parameter for runFilter: ' + params);
                }
            }
        }
    }

    getGraph(cmd, filter, type) {
        let user = app.getAuthUser();
        let projectName = localStorage.getItem(PROJECT_NAME);
        const orgId = localStorage.getItem(ORGANIZATION_ID);
        let data = {uid: user.id, project: projectName, orgId: orgId};
        let url = '/api/' + cmd;
        if (filter && filter.length > 0) {
            data.graphFilter = filter;
        }

        if (type) {
            data.type = type;
        }
        if(cmd === 'getVulnsByIP'){
            data.onCveFirstTable = this.state.onCveFirstTable;
        }

        if (user && user.id) {
            this.toggleLoader();
            let requestIntelType = this.state.intelType;
            $.ajax({
                type: 'post',
                url: url,
                async: true,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                this.toggleLoader(() => {
                    let showNoDataMessage = false;
                    let showScanDate = false;
                    let latestScanDate = '';

                    if (data && data[0] && data[0].length === 0) {
                        showNoDataMessage = true;
                    }

                    if (data && data[2] && data[2].scanDateShow) {
                        showScanDate = data[2].scanDateShow;
                    }

                    if (data && data[2] && data[2].latestScanDate) {
                        latestScanDate = data[2].latestScanDate;
                    }

                    let assetUserInputAndMitigatedData = data[1] && data[1].length > 0? data[1] : [];
                    let responseIntelType = this.state.intelType;
                    // This is a fix for multiple click on several intel types before each one returns its data.
                    if (requestIntelType === responseIntelType) {
                        let sessionCveSeverity = sessionStorage.getItem(SESSION_CVE_SEVERITY);
                        if (sessionCveSeverity) {
                            this.setState({onCveFirstTable: false});
                        }
                        this.setState({data: data[0], assetUserInputAndMitigatedData: assetUserInputAndMitigatedData, showNoDataMessage: showNoDataMessage, showScanDate : showScanDate, latestScanDate : latestScanDate}, () => {
                            this.submitQuery(this.state.data);
                        });
                    }
                });
            }).fail((jqXHR, textStatus, errorThrown) => {
                this.toggleLoader();
                console.log(errorThrown);
            });
        }
    }

    runFilterByDates(params, startDate, endDate, companiesFilter) {
        if (params) {
            if (params === 'cve') {
                this.getGraphByDates('getVulnsByIP', startDate, endDate, companiesFilter);
            } else if (params === 'ports') {
                this.getGraphByDates('getPortsByIP', startDate, endDate, companiesFilter);
            } else if (params === 'asn') {
                this.getGraphByDates('getASNByIP', startDate, endDate, companiesFilter);
            } else if (params === 'IP') {
                this.getGraphByDates('getAllIPs', startDate, endDate, companiesFilter);
            } else if (params === 'product') {
                this.getGraphByDates('getProductsByIP', startDate, endDate, companiesFilter);
            } else if (params === 'VPN') {
                this.getGraphByDates('getVPNByIP', startDate, endDate, companiesFilter);
            } else if (params === 'spf') {
                this.getGraphByDates('getSpf', startDate, endDate, companiesFilter);
            } else if (params === 'dmarc') {
                this.getGraphByDates('getDmarc', startDate, endDate, companiesFilter);
            } else if (params === 'isp') {
                this.getGraphByDates('getIsp', startDate, endDate, companiesFilter);
            } else if (params === 'buckets') {
                this.getGraphByDates('getBuckets', startDate, endDate, companiesFilter);
            } else {
                console.log('Unknown parameter for runFilterByDates: ' + params);
            }
        }
    }

    getGraphByDates(filter, startDate, endDate, companiesFilter) {
        let user = app.getAuthUser();
        let projectName = localStorage.getItem(PROJECT_NAME);
        const orgId = localStorage.getItem(ORGANIZATION_ID);
        if (user && user.id) {
            this.toggleLoader();
            let requestIntelType = this.state.intelType;
            $.ajax({
                type: 'post',
                url: '/api/' + filter,
                async: true,
                data: JSON.stringify({
                    uid: user.id,
                    project: projectName,
                    orgId: orgId,
                    companiesFilter: companiesFilter,
                    startDate: startDate,
                    endDate: endDate
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                this.toggleLoader(() => {
                    let showNoDataMessage = false;
                    let showScanDate = false;
                    let latestScanDate = '';

                    if (data && data[0] && data[0].length === 0) {
                        showNoDataMessage = true;
                    }
                    let assetUserInputAndMitigatedData = data[1] && data[1].length > 0 ? data[1] : [];
                    if (data && data[2] && data[2].scanDateShow) {
                        showScanDate = data[2].scanDateShow;
                    }

                    if (data && data[2] && data[2].latestScanDate) {
                        latestScanDate = data[2].latestScanDate;
                    }

                    let responseIntelType = this.state.intelType;
                    // This is a fix for multiple click on several intel types before each one returns its data.
                    if (requestIntelType === responseIntelType) {
                        this.setState({data: data[0], assetUserInputAndMitigatedData: assetUserInputAndMitigatedData, showNoDataMessage: showNoDataMessage, showScanDate : showScanDate, latestScanDate : latestScanDate}, () => {
                            this.submitQuery(this.state.data);
                        });
                    }
                });
            }).fail((jqXHR, textStatus, errorThrown) => {
                this.toggleLoader();
                console.log(errorThrown);
            });
        }
    }

    filterDataByDates(startDate, endDate, companiesFilter) {
        let params = this.props.params.filter;
        this.runFilterByDates(params, startDate, endDate, companiesFilter);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    submitQuery(data) {
        if (!data) {
            return;
        }

        if (this.refs && this.refs.graphResultsContainer) {
            this.refs.graphResultsContainer.clear();
        }
        this.setState({data: data, showGraphView: false});
    }

    toggleLoader(cb) {
        if (this.state.showLoader) {
            this.setState({showLoader: false}, () => {
                if (this.refs && this.refs.graphResultsContainer) {
                    this.refs.graphResultsContainer.setLoader(this.state.showLoader);
                }
                if (cb) {
                    cb();
                }
            });
        } else {
            this.setState({showLoader: true}, () => {
                if (this.refs && this.refs.graphResultsContainer) {
                    this.refs.graphResultsContainer.setLoader(this.state.showLoader);
                }
                if (cb) {
                    cb();
                }
            });
        }
    }

    setTableLoader(isOn = false){
        this.setState({showLoader: isOn});
    }

    sortDataByCveHighestRisk(allNodes) {
        if (allNodes.length > 0) {
            // In case this is the default load of tableData - sort it by totalScores.
            if (!this.state.initCompaniesSortByScore) {
                allNodes.sort((a, b) => {

                    let scoreA;
                    let scoreB;
                    if (a['nodes(path)'] && a['nodes(path)'][5]) {
                        scoreA = a['nodes(path)'][5].properties.score || 0;
                    } else if (a['nodes(path)'] && a['nodes(path)'][4]) {
                        scoreA = a['nodes(path)'][4].properties.score || 0;
                    } else if (a.CVSS) {
                        scoreA = a.CVSS.props.children || 0;
                    }
                    if (b['nodes(path)'] && b['nodes(path)'][5]) {
                        scoreB = b['nodes(path)'][5].properties.score || 0;
                    } else if (b['nodes(path)'] && b['nodes(path)'][4]) {
                        scoreB = b['nodes(path)'][4].properties.score || 0;
                    } else if (b.CVSS) {
                        scoreB = b.CVSS.props.children || 0;
                    }

                    if (isNumeric(scoreA) && isNumeric(scoreB)) {
                        scoreA = Number(scoreA);
                        scoreB = Number(scoreB);
                    }
                    // sort ASC or DESC by displayScoreBackwards. (Highest risk score should be first by default)
                    let sortResult = ((scoreA > scoreB) ? -1 : (scoreA < scoreB) ? 1 : 0);
                    return sortResult;
                });
            }
        }
    }

    changeShownView(row, assetUserInputAndMitigatedData, hashDataArr) {
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;
        let allNodes = [];
        let idx;
        //when clicking on first table row (in cve(network) page)
        if (row && this.state.onCveFirstTable && window.location.pathname && window.location.pathname === '/intel/cve') {
            if (isInDemoMode && row.IP) {
                row.IP = generalFunctions.getDataFromHashArr(row.IP, hashDataArr);
            }
            for (let i = 0; i < this.state.data.length; i++) {
                let dataPath = this.state.data[i]['nodes(path)'] || this.state.data[i]['NODES(path)+COALESCE (NODES(path2),[])'];
                //because Product is an object with 2 props, one text for ui: "php 5.6.40"(can include the iis icon too), and one with the full name: "cpe:/a:php:php:5.6.40"
                //we check if the the full name prop is exist and use it.
                let productVal;

                if(row.Product && row.Product !== 'string' && row.Product.props && row.Product.props.children &&
                    row.Product.props.children[1] && row.Product.props.children[1].props &&
                    row.Product.props.children[1].props.children){
                    productVal = row.Product.props.children[1].props.children;
                }
                if (dataPath[4].properties.cpe && dataPath[2].properties.address) {
                    let curValue = dataPath[4].properties.cpe;
                    // let cpeArr = dataPath[4].properties.cpe.split(':');
                    // cpeArr = cpeArr[cpeArr.length - 3] + ' ' + cpeArr[cpeArr.length - 2] + ' ' + cpeArr[cpeArr.length - 1];
                    if (row.IP === dataPath[2].properties.address && productVal === curValue) {
                        allNodes.push(this.state.data[i]);
                    }
                } else if (dataPath[3].properties.cpe && dataPath[1].properties.address) {
                    let curValue = dataPath[3].properties.cpe;
                    // let cpeArr = dataPath[3].properties.cpe.split(':');
                    // cpeArr = cpeArr[cpeArr.length - 3] + ' ' + cpeArr[cpeArr.length - 2] + ' ' + cpeArr[cpeArr.length - 1];
                    if (row.IP === dataPath[1].properties.address && productVal === curValue) {
                        allNodes.push(this.state.data[i]);
                    }
                }
            }
            this.sortDataByCveHighestRisk(allNodes);
            this.setState({data: allNodes, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData, onCveFirstTable: false, onCveSecondTable: true});
        }
        // when clicking table row or back button
        else {
            if (row) {
                idx = this.CheckTableRowOnClick(row, hashDataArr);
            }
            if (!isNaN(idx) || idx === 0) {
                for (let i = 0; i < this.state.data.length; i++) {
                    let firstNode = this.state.data[i]['nodes(path)'] || this.state.data[i]['NODES(path)+COALESCE (NODES(path2),[])'];
                    let selectedNode = this.state.data[idx]['nodes(path)'] || this.state.data[idx]['NODES(path)+COALESCE (NODES(path2),[])'];
                    if (firstNode[0].labels[1] === selectedNode[0].labels[1]) {
                        allNodes.push(this.state.data[i]);
                    }
                }
                this.setState({showGraphView: !this.state.showGraphView, selectedData: allNodes, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
            } else if (this.state.showGraphView) {
                this.setState({showGraphView: !this.state.showGraphView, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
            } else {
                this.setState({onCveFirstTable: true, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
                this.setDefaultCompanies();
            }
        }
    }

    CheckTableRowOnClick(row, hashDataArr) {
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;
        if(isInDemoMode){
            if(row.IP){
                row.IP = generalFunctions.getDataFromHashArr(row.IP, hashDataArr);
            }
            if(row.Hostname){
                row.Hostname = generalFunctions.getDataFromHashArr(row.Hostname, hashDataArr);
            }
            if(row.Source){
                row.Source = generalFunctions.getDataFromHashArr(row.Source, hashDataArr);
            }
            if(row.Source && row.Source.props && row.Source.props.children){
                row.Source.props.children = generalFunctions.getDataFromHashArr(row.Source.props.children, hashDataArr);
            }
            if(row.bucket){
                row.bucket = generalFunctions.getDataFromHashArr(row.bucket, hashDataArr);
            }
        }
        for (let i = 0; i < this.state.data.length; i++) {
            let dataPath = this.state.data[i]['NODES(path)+COALESCE (NODES(path2),[])'] || this.state.data[i]['nodes(path)'];

            if (dataPath[1] && (dataPath[1].labels[0] === 'spf' || dataPath[1].labels[0] === 'dmarc')) {
                return;
            }
            //when its SubDomain
            if (dataPath[1] && dataPath[1].labels[0] === 'subdomain') {
                if (dataPath === this.state.data[i]['NODES(path)+COALESCE (NODES(path2),[])']) {
                    //domains/ip
                    if (row.IP === dataPath[2].properties.address &&
                        ((row.Source === "no domain" || row.Source === dataPath[1].properties.hostname) ||
                            (row.Source.props && row.Source.props.children &&
                                row.Source.props.children === dataPath[1].properties.hostname))) {
                        return i;
                    }
                } else {
                    //s3 buckets
                    if (dataPath[2] && dataPath[2].properties.bucket) {
                        if (((row.Hostname && row.Hostname === dataPath[1].properties.hostname) ||
                            (row.Hostname && row.Hostname.props && row.Hostname.props.children && row.Hostname.props.children === dataPath[1].properties.hostname)) &&
                            row.bucket === dataPath[2].properties.bucket &&
                            row.bucket_status === dataPath[3].properties.bucket_status) {
                            return i;
                        }
                    } else if (dataPath[3] && dataPath[3].properties.ports) {
                        //open ports
                        if (row.IP === dataPath[2].properties.address &&
                            ((row.Hostname === "no domain" || row.Hostname === dataPath[1].properties.hostname) ||
                                (row.Hostname && row.Hostname.props && row.Hostname.props.children &&
                                    row.Hostname.props.children === dataPath[1].properties.hostname)) &&
                            row.ports === dataPath[3].properties.ports) {
                            return i;
                            //network
                        } else if (dataPath[5] && dataPath[5].properties.cve) {
                            if (row.IP === dataPath[2].properties.address &&
                                (row.Hostname === dataPath[1].properties.hostname || row.Hostname.props.children === dataPath[1].properties.hostname) &&
                                row.Port === dataPath[3].properties.ports &&
                                row.CVSS.props.children === dataPath[5].properties.score &&
                                row.CVE.props.children === dataPath[5].properties.cve) {
                                return i;
                            }
                            //discovered systems
                        } else if (dataPath[4] && dataPath[4].properties.finding) {
                            if (row.IP === dataPath[2].properties.address &&
                                row.ports === dataPath[3].properties.ports &&
                                row.product === dataPath[4].properties.finding) {
                                return i;
                            }
                        }
                    }
                    //isp
                    else if (dataPath[3] && dataPath[3].properties.finding) {
                        if (row.IP === dataPath[2].properties.address &&
                            ((row.Hostname === "no domain" || row.Hostname === dataPath[1].properties.hostname) ||
                                (row.Hostname && row.Hostname.props && row.Hostname.props.children && row.Hostname.props.children === dataPath[1].properties.hostname)) &&
                            row.isp === dataPath[3].properties.finding) {
                            return i;
                        }
                    }
                }
                //when its Domain
            } else {
                if (dataPath === this.state.data[i]['NODES(path)+COALESCE (NODES(path2),[])']) {
                    //domains/ip
                    if (row.IP === dataPath[1].properties.address &&
                        ((row.Source === "no domain" || row.Source === dataPath[0].properties.hostname) ||
                            (row.Source.props && row.Source.props.children &&
                                row.Source.props.children === dataPath[0].properties.hostname))) {
                        return i;
                    }
                } else {
                    //s3 buckets
                    if (dataPath[1] && dataPath[1].properties.bucket) {
                        if (((row.Hostname && row.Hostname === dataPath[0].properties.hostname) ||
                            (row.Hostname && row.Hostname.props && row.Hostname.props.children && row.Hostname.props.children === dataPath[0].properties.hostname)) &&
                            row.bucket === dataPath[1].properties.bucket &&
                            row.bucket_status === dataPath[2].properties.bucket_status) {
                            return i;
                        }
                    } else if (dataPath[2] && dataPath[2].properties.ports) {
                        //open ports
                        if (row.IP === dataPath[1].properties.address &&
                            ((row.Hostname === "no domain" || row.Hostname === dataPath[0].properties.hostname) ||
                                (row.Hostname && row.Hostname.props && row.Hostname.props.children &&
                                    row.Hostname.props.children === dataPath[0].properties.hostname)) &&
                            row.ports === dataPath[2].properties.ports) {
                            return i;
                            //network
                        } else if (dataPath[4] && dataPath[4].properties.cve) {
                            if (row.IP === dataPath[1].properties.address &&
                                (row.Hostname && row.Hostname === dataPath[0].properties.hostname || row.Hostname.props.children === dataPath[0].properties.hostname) &&
                                row.Port === dataPath[2].properties.ports &&
                                row.CVSS.props.children === dataPath[4].properties.score &&
                                row.CVE.props.children === dataPath[4].properties.cve) {
                                return i;
                            }
                            //discovered systems
                        } else if (dataPath[3] && dataPath[3].properties.finding) {
                            if (row.IP === dataPath[1].properties.address &&
                                row.ports === dataPath[2].properties.ports &&
                                row.product === dataPath[3].properties.finding) {
                                return i;
                            }
                        }
                    }
                    //isp
                    else if (dataPath[2] && dataPath[2].properties.finding) {
                        if (row.IP === dataPath[1].properties.address &&
                            ((row.Hostname === "no domain" ||  row.Hostname === dataPath[0].properties.hostname)||
                                (row.Hostname && row.Hostname.props && row.Hostname.props.children && row.Hostname.props.children === dataPath[0].properties.hostname)) &&
                            row.isp === dataPath[2].properties.finding) {
                            return i;
                        }
                    }
                }
            }
        }
    }

    ApplyChangesFromSettings(companiesToShow) {
        if (companiesToShow) {
            this.setState({companiesInputVal: companiesToShow}, this.restartFilter(companiesToShow));
        }
    }

    restartFilter(companiesFilter) {
        let startD = this.state.startDate;
        let endD = this.state.endDate;
        if (startD && endD) {
            this.startFilter(startD, endD, companiesFilter);
        } else {
            let pathNameRoutes = location.pathname.split('/');
            let params = pathNameRoutes.pop();
            if (pathNameRoutes && pathNameRoutes.pop() === 'intel') {
                this.runFilter(params, companiesFilter);
            }
        }
    }

    startFilter(startD, endD, companiesFilter) {
        if (startD && endD) {
            // Fix timezones issues.
            startD = fixTimezoneRange(startD, true);

            endD = fixTimezoneRange(endD, false);

            this.setState({
                datesRange: startD.format('LL') + ' - ' + endD.format('LL'),
                openMenu: false,
                startDate: undefined,
                endDate: undefined
            }, () => {
                this.filterDataByDates(startD.utc().format(), endD.utc().format(), companiesFilter);
            });
        } else {
            app.addAlert('warning', 'Please select a date range.');
        }
    }

    getMenuItemCallerName() {
        let callerName;
        switch (this.state.menuItemCallerName) {
            case 'IP': {
                callerName = 'DOMAINS/IP';
                break;
            }
            case 'ports': {
                callerName = 'OPEN PORTS';
                break;
            }
            case 'buckets': {
                callerName = 'S3 BUCKETS';
                break;
            }
            case 'product': {
                callerName = 'DISCOVERED SYSTEMS';
                break;
            }
            case 'isp': {
                callerName = 'ISP/HOSTING PROVIDER';
                break;
            }
            case 'cve': {
                const cveSeverity = sessionStorage.getItem(SESSION_CVE_SEVERITY);
                callerName = cveSeverity ? 'NETWORK: ' + cveSeverity.toLocaleUpperCase() : 'NETWORK ';
                break;
            }
            case 'spf': {
                callerName = 'SPF RECORDS';
                break;
            }
            case 'dmarc': {
                callerName = 'DMARC RECORDS';
                break;
            }
            default:
                callerName = 'NETWORK';
        }
        return callerName;
    }

    handleCheckIfDisable() {
        let sessionCve = sessionStorage.getItem(SESSION_CVE_SEVERITY);
        //disabled if we are on the first table of cve
        if ((this.state.onCveFirstTable &&
            window.location.pathname &&
            window.location.pathname === '/intel/cve') ||
            //disabled if we came from cve severity panel
            !this.state.showGraphView &&
            sessionCve &&
            window.location.pathname &&
            window.location.pathname === '/intel/cve' ||
            !this.state.showGraphView &&
            window.location.pathname &&
            window.location.pathname !== '/intel/cve') {
            return true;
        } else {
            return false;
        }
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    getDescriptionInfoText(type, isShorterText) {
        let text = '';
        if (type) {
            switch (type) {
                case 'IP': {
                    text = isShorterText? textForTitleExpl.ipAndDomain.explShort : textForTitleExpl.ipAndDomain.expl;
                    break;
                }
                case 'ports': {
                    text = isShorterText? textForTitleExpl.ports.explShort : textForTitleExpl.ports.expl;
                    break;
                }
                case 'buckets': {
                    text = isShorterText? textForTitleExpl.buckets.explShort : textForTitleExpl.buckets.expl;
                    break;
                }
                case 'product': {
                    text = isShorterText? textForTitleExpl.product.explShort : textForTitleExpl.product.expl;
                    break;
                }
                case 'isp': {
                    text = isShorterText? textForTitleExpl.isp.explShort : textForTitleExpl.isp.expl;
                    break;
                }
                case 'cve': {
                    text = isShorterText? textForTitleExpl.cve.explShort : textForTitleExpl.cve.expl;
                    break;
                }
                case 'dmarc': {
                    text = isShorterText? textForTitleExpl.dmarc.explShort : textForTitleExpl.dmarc.expl;
                    break;
                }
                case 'spf': {
                    text = isShorterText? textForTitleExpl.spf.explShort : textForTitleExpl.spf.expl;
                    break;
                }
                default: {
                    text='';
                    break;
                }
            }
        }
        return text;
    }

    render() {
        let pageMainClass = classNames({
            'surveyContainerLTR': true
        });

        let showGraph = this.state.showGraphView;

        // This is a fix for dmarc and spf to display data only in table.
        let graphData;
        let displayOnlyTable;
        let isDisabled = this.handleCheckIfDisable();

        let buttonStyle = {color:'#212121' ,boxShadow: "2px 2px 2px 2px #e0e0e0", borderRadius:5};
        if(isDisabled){
            buttonStyle = {color: "grey", boxShadow: "2px 2px 2px 2px #e0e0e0", borderRadius:5};
        }

        if (this.state.intelType && ['dmarc', 'spf'].includes(this.state.intelType)) {
            graphData = [];
            displayOnlyTable = true;
            showGraph = false;
        } else {
            graphData = this.state.selectedData;
            displayOnlyTable = false;
        }

        let graphComponentClass = classNames({
            'showBlock': showGraph,
            'hide': !showGraph
        });

        let tableComponentClass = classNames({
            'showBlock': !showGraph,
            'hide': showGraph
        });

        let infoText = this.getDescriptionInfoText(this.state.menuItemCallerName, false);
        let shorterInfoText = this.getDescriptionInfoText(this.state.menuItemCallerName, true);

        // Loader will be displayed separated on GraphResultsContainer or TableResultsContainer through this.state.showLoader
        return (
            <div className={pageMainClass}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <div>
                        <div style={{margin: '40px 0px -15px 10px'}}>
                            <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                            <span style={{
                                position: 'relative',
                                top: '-6px',
                                fontSize: 22
                            }}>{this.getMenuItemCallerName()}</span>
                        </div><br/><br/><br/>
                        <div style={{marginTop: '-5px'}}>
                            <TitleExplanationComponent
                                openInfoBox={this.state.openInfoBox}
                                anchorEl={this.state.anchorEl}
                                shorterInfoText={shorterInfoText}
                                infoText={infoText}
                                handleCloseInfoBox={this.handleCloseInfoBox.bind(this)}
                                handleOpenInfoBox={this.handleOpenInfoBox.bind(this)}
                            />
                        </div>
                        <div style={{margin: '0px 10px 10px 10px', float: 'right'}}>
                            <FlatButton label="Back"
                                        style={buttonStyle}
                                        disabled={isDisabled}
                                        labelStyle={{bottom: 2}}
                                        backgroundColor={isDisabled? '#e2e2e2' : 'white'}
                                        onTouchTap={this.changeShownView.bind(this)}/>
                        </div>
                        <div className={graphComponentClass}>
                            <GraphResultsContainer
                                data={graphData}
                                intelComponent={this}
                                datesRange={this.state.datesRange}
                                openMenu={this.state.openMenu}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                singleDomain={this.state.singleDomain}
                                ref="graphResultsContainer"
                                companiesInputVal={this.state.companiesInputVal}
                                companies={this.state.companies}
                                ApplyChangesFromSettings={this.ApplyChangesFromSettings.bind(this)}
                                showNoDataMessage={this.state.showNoDataMessage}
                                runFilter={this.runFilter.bind(this)}
                                startFilter={this.startFilter.bind(this)}
                                showLoader={this.state.showLoader}
                                user={this.state.user}
                            />
                        </div>
                        <div className={tableComponentClass}>
                            <TableResultsContainer
                                intelComponent={this}
                                data={this.state.data}
                                ref="tableResultsContainer"
                                showLoader={this.state.showLoader}
                                changeShownView={this.changeShownView}
                                companies={this.state.companies}
                                companiesInputVal={this.state.companiesInputVal}
                                ApplyChangesFromSettings={this.ApplyChangesFromSettings.bind(this)}
                                openMenu={this.state.openMenu}
                                datesRange={this.state.datesRange}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                runFilter={this.runFilter.bind(this)}
                                startFilter={this.startFilter.bind(this)}
                                singleDomain={this.state.singleDomain}
                                onCveFirstTable={this.state.onCveFirstTable}
                                sortDataByCveHighestRisk={this.sortDataByCveHighestRisk.bind(this)}
                                intelTableType={this.state.menuItemCallerName}
                                assetUserInputAndMitigatedData={this.state.assetUserInputAndMitigatedData}
                                showScanDate={this.state.showScanDate}
                                latestScanDate={this.state.latestScanDate}
                                setDefaultCompanies={this.setDefaultCompanies.bind(this)}
                                recalculateScore={this.recalculateScore.bind(this)}
                                setTableLoader={this.setTableLoader.bind(this)}
                            />
                        </div>
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}

export default Intelligence;
