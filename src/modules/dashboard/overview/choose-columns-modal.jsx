import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import CheckedIcon from 'material-ui/svg-icons/toggle/check-box';
import UncheckedIcon from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import {Row} from 'react-grid-system';
import Checkbox from 'material-ui/Checkbox';

const shortid = require('shortid');

/**
 * A modal dialog can be closed by selecting one of the actions.
 */
export default class ChooseColumnsModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: this.props.open,
            tableColumns: []
        };
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            let stateObj = {};

            if (nextProps.tableColumns && Array.isArray(nextProps.tableColumns)) {
                stateObj.tableColumns = [];
                nextProps.tableColumns.map((currColumn) => {
                    if (currColumn && currColumn.label) {
                        // Duplicate object to avoid reference mistakes.
                        let columnToAdd = JSON.parse(JSON.stringify(currColumn));
                        if (!columnToAdd.hasOwnProperty('checked')) {
                            columnToAdd.checked = false;
                        }
                        stateObj.tableColumns.push(columnToAdd);
                    }
                });
            }

            this.setState(stateObj);
        }
    }

    handleDone() {
        this.props.navigateDone(this.state.tableColumns);
    }

    setAllColumnsChecked(value) {
        if (typeof value === 'boolean') {
            let stateToUpdate = {
                tableColumns: this.state.tableColumns
            };
            stateToUpdate.tableColumns.map((currColumn) => {
                if(currColumn.label !== "Company Name"){
                    currColumn.checked = value;
                }
            });
            this.setState(stateToUpdate);
        }
    }

    selectAllColumns() {
        this.setAllColumnsChecked(true);
    }

    clearAllColumns() {
        this.setAllColumnsChecked(false);
    }

    editColumnChecked(colIndex, e) {
        if (this.state.tableColumns.length > colIndex) {
            let stateToUpdate = {
                tableColumns: this.state.tableColumns
            };
            stateToUpdate.tableColumns[colIndex].checked = e.target.checked;
            this.setState(stateToUpdate);
        }
    }

    render() {
        const styles = {
            divStyle: {
                padding: '0px 20px'
            },
            topButton: {
                margin: '18px 0px 18px 18px'
            },
            checkboxStyle: {
                marginTop: '5px'
            }
        };

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.props.navigateCancel}
            />,
            <FlatButton
                label="Apply"
                secondary={true}
                onTouchTap={this.handleDone.bind(this)}
            />
        ];

        return (
            <Dialog
                title={'Choose Overview Columns'}
                actions={actions}
                modal={false}
                open={this.props.open}
                onRequestClose={this.props.navigateCancel}
                autoScrollBodyContent={true}
            >
                <Row style={styles.divStyle}>
                    <div>
                        <FlatButton
                            label="Select All"
                            icon={<CheckedIcon/>}
                            onClick={this.selectAllColumns.bind(this)}
                            style={styles.topButton}
                        />
                        <FlatButton
                            label="Clear All"
                            icon={<UncheckedIcon/>}
                            onClick={this.clearAllColumns.bind(this)}
                            style={styles.topButton}
                        />
                    </div>
                    <div>
                        {this.state.tableColumns.map((column, i) => {
                            let disableColumn = (i === 0);
                                return <Checkbox
                                key={shortid.generate()}
                                style={styles.checkboxStyle}
                                label={column.label}
                                checked={column.checked}
                                disabled={disableColumn}
                                onCheck={this.editColumnChecked.bind(this, i)}
                            />;
                        })}
                    </div>
                </Row>
            </Dialog>
        );
    }
};
