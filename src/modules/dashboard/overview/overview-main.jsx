import React from 'react';
import {
    OVERVIEW_TYPE,
    PROJECT_NAME,
    SCORE_TYPES,
    OVERVIEW_COLUMN_TYPES,
    OVERVIEW_DATE_FORMAT,
    ORGANIZATION_ID
} from '../../../../config/consts';
import classNames from 'classnames';
import {showExpandedDataChip} from '../../common/UI-Helpers.jsx';
import {getKeyNestedChildsIfExists, calculateTotalScore} from '../../common/CommonHelpers.js';
import '../../../../public/css/survey.css';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Tabs, Tab} from 'material-ui/Tabs';
import {Card} from 'material-ui/Card';
import Paper from 'material-ui/Paper';
import SwipeableViews from 'react-swipeable-views';
import Loader from 'react-loader-advanced';
import DataTableViewTemplate from '../../common/datatableTemplate/datatable-template.jsx';
import FlatButton from 'material-ui/FlatButton';
import ActionColumn from 'material-ui/svg-icons/action/view-column';
import ContentFilter from 'material-ui/svg-icons/content/filter-list';
import ChooseColumnsModal from './choose-columns-modal.jsx';
import ChooseFiltersModal from './choose-filters-modal.jsx';
import {isNumeric} from '../../common/CommonHelpers.js';
import themeForFont from "../../app/themes/themeForFont";


const shortid = require('shortid');
const moment = require('moment');


export default class OverviewDashboard extends React.Component {
    constructor() {
        super();

        let tableColumns = [
            {
                key: 'companyName',
                label: 'Company Name',
                type: OVERVIEW_COLUMN_TYPES.STRING,
                checked: true,
                sortable: true
            }, {
                key: 'maxIntelScore',
                label: 'Score',
                type: OVERVIEW_COLUMN_TYPES.NUMBER_LIMIT_100,
                checked: true,
                sortable: true
            }, {
                key: 'sensitivity',
                label: 'Sensitivity',
                type: OVERVIEW_COLUMN_TYPES.STRING,
                checked: true,
                sortable: true
            }, {
                key: 'extraData.classifications',
                label: 'Classifications',
                type: OVERVIEW_COLUMN_TYPES.CLOSED_OPTIONS,
                dataSource: [],
                checked: true,
                noneSearchable: true
            }, {
                key: 'extraData.date_of_contacts',
                label: 'Date of Contact',
                type: OVERVIEW_COLUMN_TYPES.DATES_RANGE,
                checked: true,
                noneSearchable: true
            }, {
                key: 'extraData.date_of_surveys',
                label: 'Date of Last Survey',
                type: OVERVIEW_COLUMN_TYPES.SINGLE_DATE,
                checked: false,
                noneSearchable: true
            }
        ];

        const styles = {
            cardStyle: {
                margin: 10
            },
            slide: {
            },
            inkBar: {
                backgroundColor: 'rgba(230, 230, 230, 0.9)'
            },
            tabItemContainer: {
                backgroundColor: 'white'
            },
            tabItemLabel: {
                fontSize: 16,
                fontWeight: 300,
                color: 'black'
            },
            emptyResultStyle: {
                backgroundColor: '#FFDD00',
                display: 'inline-block',
                padding: '1% 5%',
                marginTop: '240px',
                fontSize: 'large',
                textAlign: 'center'
            },
            wrappableStyle: {
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-word'
            },
            clickableWrappableStyle: {
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-word',
                'cursor': 'pointer'
            },
            topButton: {
                margin: '18px 0px 18px 18px'
            }
        };

        let tableColumnsToDownload = [];

        tableColumns.map((currColumn) => {
            if (currColumn && currColumn.label && currColumn.key) {
                tableColumnsToDownload.push({
                    key: currColumn.key,
                    label: currColumn.label
                });
            }
        });

        this.state = {
            showLoader: false,
            openColumnsModal: false,
            openFiltersModal: false,
            slideTabIndex: 0,
            companies: [],
            initCompaniesSortByScore: false,
            dataResult: {
                tabs: [],
                swipeableViews: []
            },
            tableColumns: tableColumns,
            tableColumnsToDownload: tableColumnsToDownload,
            filtersToApply: {},
            styles: styles
        };

        this.handleTabChange = this.handleTabChange.bind(this);
        this.setResults = this.setResults.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            this.getCompaniesData();
        }
    }

    fillTableColumnsDataSource(companies) {
        if (companies && Array.isArray(companies)) {
            // initCompaniesSortByScore is set here to false to fix default sort of data by scores.
            let stateToUpdate = {
                tableColumns: this.state.tableColumns,
                initCompaniesSortByScore: false
            };

            // Find all companies classifications and update matching tableColumn's dataSource.
            let classKey = 'extraData.classifications';
            let matchingClassColumn = stateToUpdate.tableColumns.find((c) => {
                return (c && c.key && c.key === classKey);
            });

            if (matchingClassColumn) {
                let classificationsDataSource = [];

                companies.map((currComp) => {
                    let currCompClass = getKeyNestedChildsIfExists(currComp, classKey);
                    if (currCompClass && Array.isArray(currCompClass)) {
                        currCompClass.map((currClass) => {
                            if (currClass && currClass.label && !classificationsDataSource.includes(currClass.label)) {
                                classificationsDataSource.push(currClass.label);
                            }
                        });
                    }
                });

                matchingClassColumn.dataSource = classificationsDataSource;
                this.setState(stateToUpdate);
            }
        }
    }

    getCompaniesData() {
        let pn = localStorage.getItem(PROJECT_NAME);
        const orgId = localStorage.getItem(ORGANIZATION_ID);

        if (pn) {
            this.startLoader();
            $.ajax({
                type: 'POST',
                url: '/api/getMaxCVSScore',
                async: true,
                data: JSON.stringify({project: pn, orgId:orgId}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((companies) => {
                this.stopLoader(()=>{
                    if (companies && Array.isArray(companies)) {
                        this.setState({companies: companies}, () => {
                            this.fillTableColumnsDataSource(companies);
                        });
                    }
                });
            }).fail(() => {
                this.stopLoader();
                app.addAlert('error', 'Error: failed to load project\'s companies.');
            });
        }
    }

    handleTabChange(value) {
        if (value !== this.state.slideTabIndex) {
            this.setState({
                slideTabIndex: value
            });
        }
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader(optionalCB = null) {
        if (this.state.showLoader) {
            this.setState({showLoader: false}, () => {
                if (optionalCB != null && typeof optionalCB === 'function') {
                    optionalCB();
                }
            });
        }
    }

    renderSensitivity(sensitivity) {
        let sensStr = '';
        if (sensitivity === 1) {
            sensStr = 'Low Sensitivity';
        } else if (sensitivity === 2) {
            sensStr = 'Moderate Sensitivity';
        } else if (sensitivity === 3) {
            sensStr = 'High Sensitivity';
        } else if (sensitivity === 4) {
            sensStr = 'Very High Sensitivity';
        }
        return sensStr;
    }

    isPassedFilter(filtersToApply, currColumn, currCompanyValue) {
        let result = true;

        if (filtersToApply && currColumn && filtersToApply.hasOwnProperty(currColumn.key) && currColumn.type) {
            let currFilter = filtersToApply[currColumn.key];
            if (currFilter) {
                switch (currColumn.type) {
                case OVERVIEW_COLUMN_TYPES.NUMBER_LIMIT_100: {
                    if (currFilter.fromNumber != null && isNumeric(currFilter.fromNumber)) {
                        let fromNumber = Number(currFilter.fromNumber);
                        if (currCompanyValue == null || currCompanyValue === '' || currCompanyValue < fromNumber) {
                            result = false;
                        }
                    }
                    if (currFilter.toNumber != null && isNumeric(currFilter.toNumber)) {
                        let toNumber = Number(currFilter.toNumber);
                        if (currCompanyValue == null || currCompanyValue === '' || currCompanyValue > toNumber) {
                            result = false;
                        }
                    }

                    break;
                }
                case OVERVIEW_COLUMN_TYPES.CLOSED_OPTIONS: {
                    if (currFilter.filtered != null && Array.isArray(currFilter.filtered) && currFilter.filtered.length > 0) {
                        let filtered = currFilter.filtered;
                        if (currCompanyValue == null || !Array.isArray(currCompanyValue)
                                || !currCompanyValue.find((v) => {
                                    return (v && filtered.includes(v));
                                })) {
                            result = false;
                        }
                    }
                    break;
                }
                default: {
                    break;
                }
                }
            }
        }

        return result;
    }

    setResults() {
        let tabs = [], swipeableViews = [];

        let data = this.state.companies;

        let filtersToApply = this.state.filtersToApply;

        let user = app.getAuthUser();

        if (data && Array.isArray(data)) {
            const resultClassName = classNames('search-results-container');

            let styles = this.state.styles;

            let displayScoreBackwards = app.shouldDisplayScoreBackwards(user);

            tabs.push(<Tab key={shortid.generate()} style={styles.tabItemLabel} label="Overview"
                value={this.state.slideTabIndex}/>);
            if (data.length === 0) {
                swipeableViews.push(<Card className={resultClassName} style={styles.cardStyle}>
                    <Paper style={styles.emptyResultStyle} zDepth={4}><span style={{verticalAlign: 'middle'}}>No data to display.</span></Paper>
                </Card>);
            } else {
                let tableData = [], tableDataToDownload = [];
                data.map((currCompany, i) => {
                    if (currCompany && currCompany.id && currCompany.companyName) {
                        let companyTableObj = {};
                        let companyTableObjToDownload = {};
                        let passedFilter = true;
                        companyTableObj.key = i;
                        companyTableObj.id = currCompany.id;
                        this.state.tableColumns.map((currColumn) => {
                            if (currColumn && currColumn.key) {
                                let currCompanyValue = getKeyNestedChildsIfExists(currCompany, currColumn.key);
                                if (currCompanyValue != null) {
                                    if (Array.isArray(currCompanyValue)) {
                                        // This is a fix for extraData objects.
                                        if (currColumn.key.includes('extraData')) {
                                            currCompanyValue = currCompanyValue.map((currEntity) => {
                                                let returnedVal;
                                                if (currEntity) {
                                                    if (currEntity.label) {
                                                        returnedVal = currEntity.label;
                                                    } else if (currEntity.startDate) {
                                                        returnedVal = moment.utc(currEntity.startDate).format(OVERVIEW_DATE_FORMAT);
                                                        if (currEntity.endDate && currEntity.startDate !== currEntity.endDate) {
                                                            returnedVal += ' - ' + moment.utc(currEntity.endDate).format(OVERVIEW_DATE_FORMAT);
                                                        }
                                                    } else {
                                                        returnedVal = '';
                                                    }
                                                } else {
                                                    returnedVal = '';
                                                }
                                                return returnedVal;
                                            });
                                        }
                                        companyTableObj[currColumn.key] = showExpandedDataChip(currCompanyValue, 5, 5);
                                        companyTableObjToDownload[currColumn.key] = currCompanyValue.join(', ');
                                    }
                                    else {
                                        if (currColumn.key === 'maxIntelScore') {
                                            let totalToDisplay;
                                            if(currCompany.scoresData && currCompany.scoresData.hasOwnProperty('totalScore')) {
                                                totalToDisplay = currCompany.scoresData.totalScore;
                                            }
                                            else {
                                                // Calc total score.
                                                let isSurveyNA = (currCompany.surveyScore == undefined || currCompany.isSurveyNA);
                                                totalToDisplay = calculateTotalScore(currCompany.maxIntelScore, currCompany.surveyScore || 0, currCompany.ratios, isSurveyNA);
                                            }
                                            if (displayScoreBackwards) {
                                                totalToDisplay = app.switchScoreBackwards(totalToDisplay, SCORE_TYPES.COMPANY);
                                            }
                                            currCompanyValue = totalToDisplay;
                                        } else if (currColumn.key === 'sensitivity') {
                                            currCompanyValue = this.renderSensitivity(currCompanyValue);
                                        }
                                        companyTableObj[currColumn.key] = currCompanyValue.toString();
                                        companyTableObjToDownload[currColumn.key] = currCompanyValue;
                                    }
                                } else {
                                    companyTableObj[currColumn.key] = '';
                                    companyTableObjToDownload[currColumn.key] = '';
                                }

                                // Apply Filters if exist.
                                if (!this.isPassedFilter(filtersToApply, currColumn, currCompanyValue)) {
                                    passedFilter = false;
                                }
                            }
                        });

                        if (passedFilter) {
                            tableData.push(companyTableObj);
                            tableDataToDownload.push(companyTableObjToDownload);
                        }
                    }
                });

                // Display only checked tableColumns.
                let tableColumnsToDisplay = this.state.tableColumns.filter((c) => {
                    return (c && c.checked);
                });

                if (tableColumnsToDisplay.length > 0) {
                    if (tableData.length > 0) {
                        // In case this is the default load of tableData - sort it by totalScores.
                        if (!this.state.initCompaniesSortByScore) {
                            tableData.sort((a, b) => {
                                let scoreA = a['maxIntelScore'] || 0;
                                let scoreB = b['maxIntelScore'] || 0;
                                if (isNumeric(scoreA) && isNumeric(scoreB)) {
                                    scoreA = Number(scoreA);
                                    scoreB = Number(scoreB);
                                }
                                // sort ASC or DESC by displayScoreBackwards. (Highest risk score should be first by default)
                                let sortResult = (displayScoreBackwards) ? ((scoreA < scoreB) ? -1 : (scoreA > scoreB) ? 1 : 0) :
                                    ((scoreA > scoreB) ? -1 : (scoreA < scoreB) ? 1 : 0);
                                return sortResult;
                            });
                            this.state.initCompaniesSortByScore = true;
                        }
                        swipeableViews.push(<div style={styles.slide}>
                            <DataTableViewTemplate
                                myDataType={OVERVIEW_TYPE}
                                tableColumns={tableColumnsToDisplay}
                                tableData={tableData}
                                tableColumnsToDownload={this.state.tableColumnsToDownload}
                                tableDataToDownload={tableDataToDownload}
                            />
                        </div>);
                    } else {
                        swipeableViews.push(<div style={styles.slide}>
                            <Card className={resultClassName} style={styles.cardStyle}>
                                <Paper style={styles.emptyResultStyle} zDepth={4}>
                                    <span style={{verticalAlign: 'middle'}}>No data to display.</span>
                                </Paper>
                            </Card>
                        </div>);
                    }
                } else {
                    swipeableViews.push(<div style={styles.slide}>
                        <Card className={resultClassName} style={styles.cardStyle}>
                            <Paper style={styles.emptyResultStyle} zDepth={4}><span style={{verticalAlign: 'middle'}}>No columns to display.</span></Paper>
                        </Card>
                    </div>);
                }
            }
        }
        return ({
            tabs: tabs,
            swipeableViews: swipeableViews
        });
    }

    openColumnsModal(e) {
        e.preventDefault();

        if (!this.state.openColumnsModal) {
            this.setState({openColumnsModal: true});
        }
    }

    openFiltersModal(e) {
        e.preventDefault();

        if (!this.state.openFiltersModal) {
            this.setState({openFiltersModal: true});
        }
    }

    closeColumnsModal() {
        if (this.state.openColumnsModal) {
            this.setState({openColumnsModal: false});
        }
    }

    closeFiltersModal() {
        if (this.state.openFiltersModal) {
            this.setState({openFiltersModal: false});
        }
    }

    saveColumnsChanges(editedTableColumns) {
        let stateObjToUpdate = {openColumnsModal: false};
        if (editedTableColumns && Array.isArray(editedTableColumns)) {
            stateObjToUpdate.tableColumns = editedTableColumns;
        }
        this.setState(stateObjToUpdate);
    }

    saveFiltersChanges(editedTableFilters) {
        let stateObjToUpdate = {openFiltersModal: false};
        if (editedTableFilters) {
            stateObjToUpdate.filtersToApply = editedTableFilters;
        }
        this.setState(stateObjToUpdate);
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        let styles = this.state.styles;

        let dataResult = this.setResults();

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div className={pageStyleClass}>
                    <div style={{margin: '40px 0px 40px 10px'}}>
                        <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                        <span style={{ position: 'relative', top: '-6px', fontSize: 22}}>COMPANY ANALYSIS</span>
                    </div>
                    <Loader show={this.state.showLoader} message={spinner}>
                        <div>
                            <Card style={styles.cardStyle}>
                                <FlatButton
                                    label="Choose columns"
                                    icon={<ActionColumn color={'#0091ea'}/>}
                                    onClick={this.openColumnsModal.bind(this)}
                                    style={styles.topButton}
                                />
                                <ChooseColumnsModal
                                    open={this.state.openColumnsModal}
                                    tableColumns={this.state.tableColumns}
                                    navigateCancel={this.closeColumnsModal.bind(this)}
                                    navigateDone={this.saveColumnsChanges.bind(this)}
                                />
                                <FlatButton
                                    label="Choose Filters"
                                    icon={<ContentFilter color={'#0091ea'}/>}
                                    onClick={this.openFiltersModal.bind(this)}
                                    style={styles.topButton}
                                />
                                <ChooseFiltersModal
                                    open={this.state.openFiltersModal}
                                    tableColumns={this.state.tableColumns}
                                    filtersToApply={this.state.filtersToApply}
                                    navigateCancel={this.closeFiltersModal.bind(this)}
                                    navigateDone={this.saveFiltersChanges.bind(this)}
                                />
                            </Card>
                            {/*<Card style={styles.cardStyle}>*/}
                                {/*<Tabs*/}
                                    {/*onChange={this.handleTabChange}*/}
                                    {/*value={this.state.slideTabIndex}*/}
                                    {/*tabItemContainerStyle={styles.tabItemContainer}*/}
                                    {/*inkBarStyle={styles.inkBar}*/}
                                    {/*children={dataResult.tabs}*/}
                                {/*/>*/}
                                <SwipeableViews
                                    index={this.state.slideTabIndex}
                                    onChangeIndex={this.handleTabChange}
                                    children={dataResult.swipeableViews}
                                />
                            {/*</Card>*/}
                        </div>
                    </Loader>
                </div>
            </MuiThemeProvider>
        );
    }
}
