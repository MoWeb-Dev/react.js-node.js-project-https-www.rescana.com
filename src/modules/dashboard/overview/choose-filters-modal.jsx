import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {Row, Col} from 'react-grid-system';
// import Checkbox from 'material-ui/Checkbox';
// import {RadioButton} from 'material-ui/RadioButton';
// import DatePicker from 'material-ui/DatePicker';
// import DatePickerMain from '../../common/datePicker/date-picker-main.jsx';
// import DropDownMenu from 'material-ui/DropDownMenu';
// import MenuItem from 'material-ui/MenuItem';
// import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
// import ContentRemove from 'material-ui/svg-icons/content/remove-circle-outline';
// import ContentAdd from 'material-ui/svg-icons/content/add-circle-outline';
import CommunicationClearAll from 'material-ui/svg-icons/communication/clear-all';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete/index';
import {OVERVIEW_COLUMN_TYPES} from '../../../../config/consts.js';
import {isNumeric} from '../../common/CommonHelpers.js';

/**
 * A modal dialog can be closed by selecting one of the actions.
 */
export default class ChooseFiltersModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tableColumns: [],
            filtersToApply: {}
        };
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            let stateObj = {};

            if (nextProps.tableColumns && Array.isArray(nextProps.tableColumns)) {
                stateObj.tableColumns = [];
                nextProps.tableColumns.map((currColumn) => {
                    // Allow filters only on checked columns.
                    if (currColumn && currColumn.label && currColumn.checked) {
                        // Duplicate object to avoid reference mistakes.
                        let columnToAdd = JSON.parse(JSON.stringify(currColumn));

                        stateObj.tableColumns.push(columnToAdd);
                    }
                });
                if (nextProps.filtersToApply) {
                    stateObj.filtersToApply = nextProps.filtersToApply;
                } else {
                    stateObj.filtersToApply = {};
                }
            } else {
                stateObj.tableColumns = [];
                stateObj.filtersToApply = {};
            }

            this.setState(stateObj);
        }
    }

    clearAllFilters() {
        this.setState({filtersToApply: {}});
    }

    handleDone() {
        // Validate all filters.
        if (this.state.filtersToApply) {
            let filtersToReturn = {};
            let isOK = true;
            let filtersToApply = this.state.filtersToApply;
            let filterKeys = Object.keys(filtersToApply);
            if (filterKeys && Array.isArray(filterKeys)) {
                filterKeys.map((currKey) => {
                    if (currKey && filtersToApply[currKey].type && filtersToApply[currKey].label) {
                        switch (filtersToApply[currKey].type) {
                        case OVERVIEW_COLUMN_TYPES.NUMBER_LIMIT_100: {
                            if (filtersToApply[currKey].fromNumber != null && filtersToApply[currKey].toNumber != null) {
                                if (isNumeric(filtersToApply[currKey].fromNumber)) {
                                    filtersToApply[currKey].fromNumber = Number(filtersToApply[currKey].fromNumber);

                                    if (isNumeric(filtersToApply[currKey].toNumber)) {
                                        filtersToApply[currKey].toNumber = Number(filtersToApply[currKey].toNumber);

                                        if (filtersToApply[currKey].toNumber < filtersToApply[currKey].fromNumber) {
                                            isOK = false;
                                            app.addAlert('error', filtersToApply[currKey].label + ' range is not valid');
                                        } else {
                                            filtersToApply[currKey].toNumber = filtersToApply[currKey].toNumber.toString();
                                        }
                                    }

                                    filtersToApply[currKey].fromNumber = filtersToApply[currKey].fromNumber.toString();

                                    filtersToReturn[currKey] = filtersToApply[currKey];
                                } else if (isNumeric(filtersToApply[currKey].toNumber)) {
                                    filtersToApply[currKey].toNumber = Number(filtersToApply[currKey].toNumber).toString();

                                    filtersToReturn[currKey] = filtersToApply[currKey];
                                }
                            }
                            break;
                        }
                        case OVERVIEW_COLUMN_TYPES.CLOSED_OPTIONS: {
                            if (filtersToApply[currKey].filtered != null && Array.isArray(filtersToApply[currKey].filtered) && filtersToApply[currKey].filtered.length > 0) {
                                filtersToReturn[currKey] = filtersToApply[currKey];
                            }
                            break;
                        }
                        default: {
                            break;
                        }
                        }
                    }
                });
            }

            if (isOK) {
                this.props.navigateDone(filtersToReturn);
            }
        }
    }

    changePropLimitedNumber(relatedTableColumn, propertyToUpdate, event) {
        if (relatedTableColumn && relatedTableColumn.key && event && event.target && event.target.hasOwnProperty('value')) {
            let value = event.target.value;
            if ((value === '') || (isNumeric(value) && value >= 0 && value <= 100)) {
                // Update prop from filters.
                let stateToUpdate = {
                    filtersToApply: this.state.filtersToApply
                };
                stateToUpdate.filtersToApply[relatedTableColumn.key][propertyToUpdate] = value;
                this.setState(stateToUpdate);
            } else {
                app.addAlert('error', 'Should be between 0-100.');
            }
        }
    }

    // This generic function pushes the value to infoField array.
    addInfoField(infoField, value) {
        let stateToUpdate = {
            filtersToApply: this.state.filtersToApply
        };
        if (stateToUpdate.filtersToApply.hasOwnProperty(infoField) && stateToUpdate.filtersToApply[infoField].hasOwnProperty('filtered') && Array.isArray(stateToUpdate.filtersToApply[infoField].filtered)) {
            stateToUpdate.filtersToApply[infoField].filtered.push(value);
            this.setState(stateToUpdate);
        }
    }

    // This generic function removes the itemToDelete from infoField only in case it is an array.
    removeInfoFieldArrayItem(infoField, itemToDelete) {
        let stateToUpdate = {
            filtersToApply: this.state.filtersToApply
        };
        if (stateToUpdate.filtersToApply.hasOwnProperty(infoField) && stateToUpdate.filtersToApply[infoField].hasOwnProperty('filtered') && Array.isArray(stateToUpdate.filtersToApply[infoField].filtered)) {
            let itemIndex = stateToUpdate.filtersToApply[infoField].filtered.indexOf(itemToDelete);
            if (itemIndex > -1) {
                stateToUpdate.filtersToApply[infoField].filtered.splice(itemIndex, 1);
                this.setState(stateToUpdate);
            }
        }
    }

    handleAddChipInfoField(infoFieldName, dataSource, chip) {
        if (chip && dataSource && Array.isArray(dataSource)) {
            let matchedType = dataSource.find((t) => {
                let lowerCaseType = t;
                if (lowerCaseType) {
                    lowerCaseType = lowerCaseType.toString().toLowerCase();
                }
                return lowerCaseType === chip.toString().toLowerCase();
            });

            if (matchedType && this.state.filtersToApply[infoFieldName] && this.state.filtersToApply[infoFieldName].filtered
                && Array.isArray(this.state.filtersToApply[infoFieldName].filtered) && !this.state.filtersToApply[infoFieldName].filtered.includes(matchedType)) {
                this.addInfoField(infoFieldName, matchedType);
            }
        }
    }

    handleDeleteChipItemInfoField(infoFieldName, chip) {
        if (chip && infoFieldName) {
            this.removeInfoFieldArrayItem(infoFieldName, chip);
        }
    }

    getDataChildren(tableColumns, styles) {
        let result = [];
        if (tableColumns && Array.isArray(tableColumns) && tableColumns.length > 0) {
            let stateObject = {
                filtersToApply: this.state.filtersToApply
            };
            let filtersToApply = stateObject.filtersToApply;
            let uniqueID = 50; // Chosen randomly.
            tableColumns.map((currColumn) => {
                if (currColumn && currColumn.key && currColumn.label && currColumn.type) {
                    let filterDivChildren = [];
                    if (!filtersToApply.hasOwnProperty(currColumn.key)) {
                        filtersToApply[currColumn.key] = {
                            type: currColumn.type,
                            label: currColumn.label
                        };
                    }
                    switch (currColumn.type) {
                    case OVERVIEW_COLUMN_TYPES.NUMBER_LIMIT_100: {
                        if (!filtersToApply[currColumn.key].hasOwnProperty('fromNumber')) {
                            filtersToApply[currColumn.key].fromNumber = '';
                            filtersToApply[currColumn.key].toNumber = '';
                        }
                        filterDivChildren.push(<span key={uniqueID++}>From</span>);
                        filterDivChildren.push(<TextField
                            key={uniqueID++}
                            hintText="Any"
                            style={styles.numberLimit100}
                            value={filtersToApply[currColumn.key].fromNumber}
                            onChange={this.changePropLimitedNumber.bind(this, currColumn, 'fromNumber')}
                        />);
                        filterDivChildren.push(<span key={uniqueID++} style={styles.singleDateText}>To</span>);
                        filterDivChildren.push(<TextField
                            key={uniqueID++}
                            hintText="Any"
                            style={styles.numberLimit100}
                            value={filtersToApply[currColumn.key].toNumber}
                            onChange={this.changePropLimitedNumber.bind(this, currColumn, 'toNumber')}
                        />);
                        break;
                    }
                    case OVERVIEW_COLUMN_TYPES.CLOSED_OPTIONS: {
                        if (!filtersToApply[currColumn.key].hasOwnProperty('filtered')) {
                            filtersToApply[currColumn.key].filtered = [];
                        }
                        if (currColumn.dataSource && Array.isArray(currColumn.dataSource) && currColumn.dataSource.length > 0) {
                            filterDivChildren.push(<ChipInput
                                key={uniqueID++}
                                hintText="Search Classification"
                                floatingLabelText="Filter Classifications"
                                menuStyle={styles.autoCompleteStyle}
                                filter={AutoComplete.caseInsensitiveFilter}
                                value={filtersToApply[currColumn.key].filtered}
                                dataSource={currColumn.dataSource}
                                onRequestAdd={(chip) => this.handleAddChipInfoField(currColumn.key, currColumn.dataSource, chip)}
                                onRequestDelete={(chip) => this.handleDeleteChipItemInfoField(currColumn.key, chip)}
                                maxSearchResults={50}
                                style={styles.companyInfoTextField}
                                openOnFocus={true}
                                fullWidth={false}
                                fullWidthInput={true}
                            />);
                        } else {
                            filterDivChildren.push(<span key={uniqueID++}>No {currColumn.label} were found.</span>);
                        }
                        break;
                    }
                    /* case "NUMBER_LIMIT_100": {
                            if(currColumn.dataSource && Array.isArray(currColumn.dataSource)) {
                                filterDivChildren.push(<span key={uniqueID++}>From</span>);
                                filterDivChildren.push(<IconButton key={uniqueID++}
                                                                   style={styles.numberLimit10Minus}><ContentRemove/></IconButton>);
                                filterDivChildren.push(<DropDownMenu key={uniqueID++}
                                                                     style={styles.numberLimit10Menu} maxHeight={250}
                                                                     value={"Any"}>
                                    {currColumn.dataSource.map((currVal) => {
                                        return <MenuItem key={uniqueID++} value={currVal}
                                                         primaryText={currVal}/>;
                                    })}
                                </DropDownMenu>);
                                filterDivChildren.push(<IconButton key={uniqueID++}
                                                                   style={styles.numberLimit10Plus}><ContentAdd/></IconButton>);
                                filterDivChildren.push(<span key={uniqueID++}>To</span>);
                                filterDivChildren.push(<IconButton key={uniqueID++}
                                                                   style={styles.numberLimit10Minus}><ContentRemove/></IconButton>);
                                filterDivChildren.push(<DropDownMenu key={uniqueID++}
                                                                     style={styles.numberLimit10Menu} maxHeight={250}
                                                                     value={"Any"}>
                                    {currColumn.dataSource.map((currVal) => {
                                        return <MenuItem key={uniqueID++} value={currVal}
                                                         primaryText={currVal}/>;
                                    })}
                                </DropDownMenu>);
                                filterDivChildren.push(<IconButton key={uniqueID++}
                                                                   style={styles.numberLimit10Plus}><ContentAdd/></IconButton>);
                            }
                            break;
                        }
                        case OVERVIEW_COLUMN_TYPES.DATES_RANGE: {
                            filterDivChildren.push(<div key={uniqueID++}>
                                <RadioButton
                                    key={uniqueID++}
                                    value="expires_range"
                                    style={styles.datesRangeRadio}
                                    checked={true}
                                />
                                <span key={uniqueID++}>Expires Between</span>
                                <DatePickerMain
                                    filterDataByDates={()=>{}}
                                    disableDefaultDates={true}
                                    buttonTooltip="Choose Dates"
                                />
                            </div>);
                            filterDivChildren.push(<RadioButton
                                    key={uniqueID++}
                                    value="regular_range"
                                    style={styles.datesRangeRadio}
                                />);
                        }
                        case OVERVIEW_COLUMN_TYPES.SINGLE_DATE: {
                            filterDivChildren.push(<span key={uniqueID++}>From</span>);
                            filterDivChildren.push(<DatePicker
                                key={uniqueID++}
                                hintText="Choose Any"
                                container="inline"
                                style={styles.singleDatePicker}
                                textFieldStyle={styles.singleDatePickerText}
                            />);
                            filterDivChildren.push(<span key={uniqueID++} style={styles.singleDateText}>To</span>);
                            filterDivChildren.push(<DatePicker
                                key={uniqueID++}
                                hintText="Choose Any"
                                container="inline"
                                style={styles.singleDatePicker}
                                textFieldStyle={styles.singleDatePickerText}
                            />);
                            break;
                        }*/
                    default: {
                        // Add other types and filters.
                        /* filterDivChildren.push(<div key={uniqueID++} style={styles.childFilterDiv}>
                                <Checkbox label="aaa 1"/>
                                <Checkbox label="bbb 2"/>
                            </div>);*/
                        break;
                    }
                    }

                    let filterDiv = <div key={uniqueID++} style={styles.childFilterDiv}>
                        {filterDivChildren}
                    </div>;

                    let rowStyle = (result.length === 0) ? styles.childFirstRow : styles.childRow;

                    let spanTop = (currColumn.key === 'extraData.classifications') ? '43px' : '14px';

                    if (filterDivChildren.length > 0) {
                        result.push(<div key={uniqueID++} style={styles.childMainDiv}>
                            <Row style={rowStyle}>
                                <Col sm={3}>
                                    <div style={{paddingLeft: '5px', paddingTop: spanTop}}>
                                        <b key={uniqueID++}>{currColumn.label}</b>
                                    </div>
                                </Col>
                                <Col sm={9}>
                                    {filterDiv}
                                </Col>
                            </Row>
                        </div>);
                    }
                }
            });
        } else {
            // If there are no selected columns, display a matching message.
            result.push(<span key={49} style={{verticalAlign: 'middle'}}>No columns to display.</span>);
        }
        return result;
    }

    render() {
        const styles = {
            divStyle: {
                padding: '0px 20px',
                overflow: 'auto',
                display: 'inline-grid',
                width: '98%'
            },
            childMainDiv: {
                display: 'inline-grid',
                alignItems: 'center',
                paddingTop: '6px'
            },
            childFirstRow: {
                borderTop: '1px solid rgb(224, 224, 224)',
                borderBottom: '1px solid rgb(224, 224, 224)',
                paddingTop: '0px'
            },
            childRow: {
                borderBottom: '1px solid rgb(224, 224, 224)'
            },
            childLabelDiv: {
                width: '30%'
            },
            childLabel: {
                marginLeft: '15px'
            },
            childFilterDiv: {
                whiteSpace: 'nowrap'
            },
            singleDatePicker: {
                display: 'inline-block',
                marginLeft: '15px'
            },
            singleDateText: {
                marginLeft: '15px'
            },
            singleDatePickerText: {
                width: '100px',
                cursor: 'pointer'
            },
            datesRangeRadio: {
                width: '40px',
                display: 'inline-table'
            },
            numberLimit100: {
                width: '40px',
                marginLeft: '15px'
            },
            numberLimit10Menu: {
                display: 'inline-flex'
            },
            numberLimit10Minus: {
                margin: '0px -20px 0px 0px ',
                padding: '0px'
            },
            numberLimit10Plus: {
                margin: '0px 10px 0px -20px',
                padding: '0px'
            },
            autoCompleteStyle: {
                overflowY: 'auto',
                height: '200px',
                maxHeight: '200px'
            },
            companyInfoTextField: {
                'width': '85%'
            },
            topButton: {
                margin: '18px 0px 18px 18px'
            }
        };

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.props.navigateCancel}
            />,
            <FlatButton
                label="Apply"
                secondary={true}
                onTouchTap={this.handleDone.bind(this)}
            />
        ];

        let dataChildren = this.getDataChildren(this.state.tableColumns, styles);

        return (
            <Dialog
                title={'Choose Filters'}
                actions={actions}
                modal={false}
                open={this.props.open}
                onRequestClose={this.props.navigateCancel}
                autoScrollBodyContent={true}
            >
                <Row style={styles.divStyle}>
                    <div>
                        <FlatButton
                            label="Clear All Filters"
                            icon={<CommunicationClearAll/>}
                            onClick={this.clearAllFilters.bind(this)}
                            style={styles.topButton}
                        />
                    </div>
                    {dataChildren}
                </Row>
            </Dialog>
        );
    }
};
