import React from 'react';
import {Card, CardHeader} from 'material-ui/Card';
import {typography} from 'material-ui/styles';
import {Map, TileLayer, Popup, Marker} from 'react-leaflet';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../app/themes/themeForFont";
import generalFunctions from "../generalComponents/general-functions.js";

class assetMap extends React.Component {
    constructor() {
        super();
    }

    render() {
        let styles = {
            paper: {
                minHeight: 200,
                marginBottom: '35px',
                padding: '0px',
            },
            header: {
                padding: 10,
                marginLeft: '7px'
            },
            listItem: {
                padding: '6px'
            },
            mapHiderOnLoad: {}
        };

        if (this.props.showLoader) {
            styles.mapHiderOnLoad.opacity = 0.1;
        }
        let user = this.props.user;

        let isInDemoMode = user && user.isInDemoMode;

        const position = [51.505, -0.09];

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div style={{marginTop: 15}}>
                    <div style={{marginBottom: '13px'}}>
                        <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                        <span style={{position: 'relative', top: '-6px', fontSize: 16}}>ASSETS LOCATION</span>
                    </div>
                    <Card style={styles.paper}>
                        <Map center={position} zoom={2} style={styles.mapHiderOnLoad}>
                            <TileLayer
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            />
                            {
                                Object.keys(this.props.data || {}).map((key, i) => {
                                    let locationObj = this.props.data[key];
                                    if (locationObj.latitude && locationObj.longitude) {
                                        let lat = parseFloat(locationObj.latitude);
                                        let lng = parseFloat(locationObj.longitude);
                                        return <Marker position={[lat, lng]} key={i}>
                                            <Popup>
                                        <span>
                                            <b>Domain</b> : {isInDemoMode ? 'domain-example-'+ i +'.com': locationObj.domain}
                                            <br/>
                                            <br/>
                                            <b>IP</b> : {isInDemoMode ? generalFunctions.createIpStrForDemoMode(key ,i) : key}
                                        </span>
                                            </Popup>
                                        </Marker>;
                                    }
                                })
                            }
                        </Map>
                    </Card>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default assetMap;
