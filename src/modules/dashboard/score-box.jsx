import React from 'react';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import {grey800} from 'material-ui/styles/colors';
import {typography} from 'material-ui/styles';
import {Circle, Caption} from 'react-percentage-circle';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Avatar from 'material-ui/Avatar';
import FontIcon from 'material-ui/FontIcon';
import ReactTooltip from 'react-tooltip';
import {browserHistory} from 'react-router';
import {SESSION_COMPANIES} from "../../../config/consts";
import LinearProgress from 'material-ui/LinearProgress';
import themeForFont from "../app/themes/themeForFont";
import ReactSpeedometer from "react-d3-speedometer"
import FlagIcon from 'material-ui/svg-icons/content/flag.js';


class ScoreBox extends React.Component {
    setSensitivityColor(sensitivity) {
        if (sensitivity === 1) {
            return '#0091ea';
        } else if (sensitivity === 2) {
            return  '#ffa000';
        } else if (sensitivity === 3) {
            return  '#f17200';
        } else if (sensitivity === 4) {
            return  '#D92E2E';
        }
    }

    setSensitivityText(sensitivity) {
        if (sensitivity === 1) {
            return 'Low Sensitivity';
        } else if (sensitivity === 2) {
            return 'Moderate Sensitivity';
        } else if (sensitivity === 3) {
            return 'High Sensitivity';
        } else if (sensitivity === 4) {
            return 'Very High Sensitivity';
        }
    }

    openDrillDown(companyID) {
        if (companyID) {
            if (this.props.title) {
                const companyName = this.props.title;
                this.setCompanyOnSession(companyName)
            }
            browserHistory.push('/company-dashboard/' + companyID);
        }
    }

    setCompanyOnSession(companyName) {
        if (companyName) {
            // Change current company as selected on Session - so its info will be displayed next in intel pages.
            sessionStorage.setItem(SESSION_COMPANIES, companyName);
        }
    }

    checkGaugeValue(valueNum, isBackwards) {
        let gaugeVal = this.props.title === "Survey" ? "N/A" : "No Data";
        if (valueNum !== null) {
            if (valueNum < 33) {
                gaugeVal = isBackwards ? "High" : "Low";
            } else if (valueNum >= 33 && valueNum < 66) {
                gaugeVal = "Medium";
            } else if (valueNum >= 66 && valueNum <= 100) {
                gaugeVal = isBackwards ? "Low" : "High";
            }
        }
        return gaugeVal;
    }

    add3Dots(str, limit) {
        let dots = '...';
        if (str && limit && str.length > limit) {
            str = str.substring(0, limit) + dots;
        }

        return str;
    }

    render() {
        const {user, title, value, color, surveyScore, intelScore, sensitivity, companyID, hideCardFrame, disableClick, clickFunc, isUnsafeCompany} = this.props;

        const isEnableToUseVendorAssessment = user && (user.isVendorAssessmentEnabled || user.userRole === "admin");
        let styles = {
            content: {
                justifyContent: 'center',
                width: '57%',
                padding: '10px',
                margin: 'auto'
            },
            gaugeContent: {
                width: 190,
                height: 130,
                padding: '5px',
                margin: 'auto'
            },
            gaugeText: {
                textAlign: 'center',
                position: 'relative',
                fontSize: 22,
                bottom: 12,
                margin: 'auto',
                color: '#4b4b4b'
            },
            subtext: {
                fontSize: 12,
                paddingBottom: 10,
                marginLeft: 20,
                marginTop: 5,
                color: grey800
            },
            subStyle: {
                fontSize: 12
            },
            text: {
                fontSize: 20,
                fontWeight: typography.fontWeightLight,
                color: grey800
            },
            avatar: {
                marginRight: 5,
                cursor: 'default'
            },
            assessmentScoreStyle: {
                width: '18%',
                position: 'absolute',
                left: 25,
                bottom: 40,
            },
            unsafeStyle: {
                position: 'absolute',
                right: 0,
                paddingRight: '15px',
                bottom: 0
            },
            numValStyle:{
                fontSize: 14,
                textAlign: 'center',
                color: 'grey',
                left: 6,
                top: 126,
                position: 'relative'
            }
        };
        if (this.props.inCompDashboard) {
            styles.content.width = '63%';
            styles.gaugeContent.width = 210;
            styles.gaugeContent.position = 'relative';
            styles.gaugeContent.top = 50;
            styles.gaugeText.top = 50;
        }

        let headerStyle = {
            // whiteSpace: 'nowrap',
            // overflowX: 'hidden',
            // textOverflow: 'ellipsis',
            fontSize: 16,
            fontWeight: typography.fontWeightLight,
        };

        if (title && title.length >= 20) {
            if (title.length >= 20) {
                headerStyle.fontSize = 12;
                // headerStyle.width = '60%';
            } else if (title.length >= 30) {
                headerStyle.fontSize = 11;
                // headerStyle.width = '60%';
            }
        }

        let onClickFunc;

        if (disableClick) {
            styles.cardStyle = {
                borderRadius: '5px 5px 5px 5px',
                cursor: 'default',
                margin: 'auto'

            };

            onClickFunc = () => {
            };
        } else {
            styles.cardStyle = {
                borderRadius: '5px 5px 5px 5px',
                cursor: 'pointer',
                margin: 'auto'
            };

            // If Click is allowed and a click event is inserted - use it.
            // otherwise use the default drillDown func (Meaning you're in main Dashboard).
            if (clickFunc != null) {
                onClickFunc = clickFunc;
            } else {
                onClickFunc = this.openDrillDown.bind(this, companyID);
            }
        }

        if (hideCardFrame) {
            styles.cardStyle.boxShadow = '';
            styles.content.marginTop = '40px';
            styles.content.marginBottom = '20px';
            styles.content.padding = '0px 46px 20px 46px';

        }

        let intelScoreStyle = {};
        if (intelScore == null) {
            intelScoreStyle.display = 'none';
        }
        let surveyScoreStyle = {};

        let unSafeStyle = {
            marginRight : '4px',
            border : '3px solid #be2923',
            borderRadius: '5px',
            background : '#c72e2a',
            color : 'white',
            paddingRight : '3px'
        };

        if (surveyScore == null) {
            surveyScoreStyle.display = 'none';
            unSafeStyle.display = 'none';
        } else if (intelScore != null) {
            surveyScoreStyle.marginLeft = '7px';
            unSafeStyle.marginLeft = '7px';
        }
        let scoreDisplayStyle = {
            fontSize: '23px', fontFamily: 'Barlow'
        };

        // This props is used to display a different value than the one being actually taken in consideration.
        let valueToDisplay = (this.props.valueToDisplay != null) ? this.props.valueToDisplay : value;

        let valueNum;
        //in case user choose to use gauge display
        let gaugeDisplayEnabled = user && user.userPreferencesConfig && user.userPreferencesConfig.gaugeDisplay;
        if (gaugeDisplayEnabled) {
            valueNum = (this.props.valueToDisplay != null) ? this.props.valueToDisplay : value;
            let gaugeValue = valueNum !== null ? valueNum : this.props.title === "Survey" ? "N/A" : "No Data";
            if (user.userPreferencesConfig.displayScoreBackwards) {
                gaugeValue = this.checkGaugeValue(valueNum, true);
            } else {
                gaugeValue = this.checkGaugeValue(valueNum, false);
            }
            valueToDisplay = gaugeValue;
            scoreDisplayStyle.fontSize = 16;
        }

        if(valueToDisplay === 'Low'){
            styles.numValStyle.left = 4;
        }

        let assessmentScore = this.props.assessmentScore || 0;

        // This props is used to differentiate between a non-existing survey to an existing one's surveyScore.
        let surveyScoreToDisplay = (this.props.isSurveyNA) ? 'N/A' : surveyScore;


        // This props is used for the percent of the circle that will be painted in color.
        let circlePercentValue = (this.props.circlePercentValue) ? this.props.circlePercentValue : value;
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <Card style={styles.cardStyle} onClick={onClickFunc}>
                    <CardHeader avatar={<Avatar
                        icon={<FontIcon style={{cursor: 'default'}} className="icon ion-speedometer"/>}
                        color={this.setSensitivityColor(sensitivity)}
                        backgroundColor={'#FFFFFF'}
                        style={styles.avatar}
                        size={40}
                        data-tip={this.setSensitivityText(sensitivity)}
                        data-for={'label'}
                    />}
                                titleStyle={headerStyle} subtitleStyle={styles.subStyle} subtitle="Risk Score"
                                title={this.add3Dots(title, 18)}
                                data-tip={title && title.length >= 18 ? title : null}
                                data-for={'label'}

                    />
                    {isUnsafeCompany && this.props.inCompDashboard?
                        <span data-tip={'Company didn\'t answer obligatory questions according to ' + title + '\'s requirements.'}
                              data-for={'label'}>
                            <img src={'/images/unsafe_label.png'} style={styles.unsafeStyle} width={110} height={110} alt={' '}/>
                        <span style={{position: 'absolute',
                            right: 0,
                            paddingRight: '14px',
                            paddingBottom: '-2px',
                            bottom: 0,
                        color:'white',
                        fontSize: 10}}><FlagIcon viewBox={'0 0 30 30'} style={{color: 'white'}}/></span>
                        </span>
                        : ''}
                    {gaugeDisplayEnabled ?
                        <div>
                            {!this.props.inCompDashboard &&
                            <div style={styles.numValStyle}>
                                <span
                                    style={valueToDisplay === 'Medium' ? {color: color, marginRight: 45} : valueToDisplay === 'High' ? {color: color, marginRight: 67} : {color: color, marginRight: 70}}>
                                    {(valueToDisplay == null) ? '' : valueToDisplay.toString()}</span>
                                <span>{'Score: ' + valueNum}</span>
                            </div>
                            }
                            <div style={styles.gaugeContent}>
                                <ReactSpeedometer
                                    maxValue={100}
                                    minValue={0}
                                    value={circlePercentValue}
                                    needleColor={color}
                                    startColor={"#f3f3f3"}
                                    segments={1}
                                    maxSegmentLabels={1}
                                    endColor={"#f3f3f3"}
                                    ringWidth={90}
                                    needleHeightRatio={0.5}
                                    fluidWidth={true}
                                    needleTransitionDuration={3000}
                                    needleTransition="easeElastic"
                                    textColor={'white'}/>
                            </div>
                            {this.props.inCompDashboard && <div style={styles.gaugeText}>
                                {(valueToDisplay == null) ? '' : valueToDisplay.toString()}
                            </div>}
                        </div>
                        :
                        <div style={styles.content}>
                            <Circle percent={circlePercentValue} strokeWidth="12" strokeColor={color}>
                                <Caption text={(valueToDisplay == null) ?
                                    '' : valueToDisplay.toString()} x="50" y="58"
                                         textAnchor="middle"
                                         className='caption-text'
                                         style={scoreDisplayStyle}/>
                            </Circle>
                        </div>
                    }
                    {isEnableToUseVendorAssessment && (this.props.assessmentScore || this.props.assessmentScore === 0) &&
                    <div style={{margin: '8px 0px 2px 20px'}}>
                        <div style={{color: grey800, fontSize: 10}}>Assessment Progress: {assessmentScore || 0}%</div>
                        <LinearProgress style={{width: '60%'}} mode="determinate" value={assessmentScore}/>
                    </div>
                    }
                    <div style={styles.subtext}>
                        <span style={intelScoreStyle}>Intel Score: {intelScore} </span>

                        {isUnsafeCompany ?
                            <span style={unSafeStyle}
                                  data-tip={'Company didn\'t answer obligatory questions according to the survey requirements.'}
                                  data-for={'label'}>
                                <FlagIcon viewBox={'-2 -2 35 35'} style={{width:17, height: 17, color:"white"}}/>
                                Survey Score: {surveyScoreToDisplay}
                            </span>
                            :
                            <span style={surveyScoreStyle}>Survey Score: {surveyScoreToDisplay}</span> }
                    </div>
                    <ReactTooltip id='label' place="bottom"/>
                </Card>
            </MuiThemeProvider>
        );
    }
}

export default ScoreBox;
