import React from 'react';
import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List/List';
import {Card, CardTitle} from 'material-ui/Card';
import ListItem from 'material-ui/List/ListItem';
import {Row, Col} from 'react-grid-system';
import {Pie, Doughnut} from 'react-chartjs-2';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../app/themes/themeForFont";

class VendorsSeverity extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: app.getAuthUser(),
        };
    }

    componentDidMount() {
        if (!this.state.user) {
            app.routeAuthUser();
        }
    }

    // This function makes sure that the Pie cake will re-render only if a new data is inserted.
    shouldComponentUpdate(nextProps) {
        return !(nextProps && nextProps.data && this.props && this.props.data && nextProps.data === this.props.data);
    }

    culcScoreForSeverity(vendorsData) {
        let displayScoreBackwards = app.shouldDisplayScoreBackwards(this.state.user);
        let lowCount = 0;
        let mediumCount = 0;
        let highCount = 0;
        vendorsData.map((vendor) => {
            let curScore = vendor && vendor.totalScore ? vendor.totalScore : 0;
            if (curScore < 33) {
                displayScoreBackwards ? highCount++ : lowCount++;
            }
            if (curScore >= 33 && curScore < 66) {
                mediumCount++;
            }
            if (curScore >= 66 && curScore <= 100) {
                displayScoreBackwards ? lowCount++ : highCount++;
            }
        });
        return [highCount, mediumCount, lowCount];
    }

    render() {
        const styles = {
            pieChartDiv: {
                height: 180,
                textAlign: 'center',
                marginTop: 18,
                marginLeft: 18
            },
            noDataDiv: {
                height: 140,
                textAlign: 'center',
                paddingTop: 100
            },
            title: {
                fontSize: 16,
                padding: '10px',
                fontWeight: 300,
                color: 'black',
                marginLeft: '7px'
            }
        };

        const labels = [
            {label: 'High', backgroundColor: '#d32f2f'},
            {label: 'Medium', backgroundColor: '#F57C00'},
            {label: 'Low', backgroundColor: '#0091ea'}
        ];

        let vendorsData = this.props.data;
        let severityArray = [0,0,0];
        if (vendorsData) {
            severityArray = this.culcScoreForSeverity(vendorsData);
        }

        const data = {
            labels: [
                ' High Risk',
                ' Medium Risk',
                ' Low Risk'
            ],
            datasets: [{
                data: severityArray,
                backgroundColor: [
                    '#d32f2f',
                    '#F57C00',
                    '#0091ea'
                ],
                hoverBackgroundColor: [
                    '#d32f2f',
                    '#F57C00',
                    '#0091ea'
                ]
            }]
        };

        let noDataYet = <div style={styles.noDataDiv}>
            <p style={{fontSize: '24px', marginTop: '25px'}}>No data found</p>
        </div>;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <div style={{marginBottom: '13px'}}>
                        <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                        <span style={{position: 'relative', top: '-6px', fontSize: 16}}>VENDORS SEVERITY</span>
                    </div>
                    <Card style={{borderRadius: '5px', height: '280px', paddingTop: 25}}>
                        {!severityArray ? noDataYet :
                            <Row>
                                <Col sm={4} xs={12} md={4} lg={4}>
                                    <List style={{cursor: 'default'}}>
                                        <ListItem
                                        style={{backgroundColor: 'rgba(0,0,0,0.05)',
                                            color:'black',
                                            fontSize: 14,
                                            cursor: 'default',
                                            marginBottom: 3,
                                            textAlign: 'center',
                                            borderRadius: '0px 35px 35px 0px'}}
                                        disabled={true}
                                        key={1000}>
                                        {'Number Of Vendors: ' + this.props.vendorsAmount}
                                        </ListItem>
                                        {labels.map((item,i) =>
                                            <ListItem
                                                style={{cursor: 'default',backgroundColor: 'white',fontSize: 13, borderRadius: '0px 35px 35px 0px'}}
                                                key={item.label}
                                                disabled={true}
                                                leftAvatar={
                                                    <Avatar icon={item.icon}
                                                            backgroundColor={item.backgroundColor}
                                                            size={28}
                                                            style={{marginLeft: 15, marginTop: 5}}

                                                    >{severityArray[i]}</Avatar>
                                                }>
                                                {item.label}
                                            </ListItem>
                                        )}
                                    </List>
                                </Col>
                                <Col sm={8} xs={12} md={10} lg={8}>
                                    <div style={styles.pieChartDiv}>
                                        <Pie data={data}
                                             width={30}
                                             height={30}
                                             options={{maintainAspectRatio: false}}
                                             legend={{display: false}}
                                             redraw
                                        />
                                    </div>
                                </Col>
                            </Row>
                        }
                    </Card>
                </div>
            </MuiThemeProvider>
        );
    };
}

export default VendorsSeverity;
