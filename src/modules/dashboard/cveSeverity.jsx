import React from 'react';
import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List/List';
import {Card, CardTitle} from 'material-ui/Card';
import ListItem from 'material-ui/List/ListItem';
import {Row, Col} from 'react-grid-system';
import {Pie} from 'react-chartjs-2';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../app/themes/themeForFont";
import {browserHistory} from "react-router";
import {SESSION_CVE_SEVERITY, SESSION_CVE_SEVERITY_PANEL, SESSION_COMPANIES} from "../../../config/consts";

class CveSeverity extends React.Component {
    constructor() {
        super();

        this.state = {
            user: app.getAuthUser(),
            cveSeverity: []
        };
    }

    componentDidMount() {
        if (!this.state.user) {
            app.routeAuthUser();
        }
    }

    // This function makes sure that the Pie cake will re-render only if a new data is inserted.
    shouldComponentUpdate(nextProps) {
        return !(nextProps && nextProps.data && this.props && this.props.data && nextProps.data === this.props.data);
    }

    handleRedirect(labelName) {
        if (labelName === 'Critical' && (this.props.data.criticalCount.length === 0 || !this.props.data.criticalCount)) {
            app.addAlert('warning', 'No Critical Data Exists')
        } else if (labelName === 'High' && (this.props.data.highCount.length === 0 || !this.props.data.highCount)) {
            app.addAlert('warning', 'No High Data Exists')
        } else if (labelName === 'Medium' && (this.props.data.mediumCount.length === 0 || !this.props.data.mediumCount)) {
            app.addAlert('warning', 'No Medium Data Exists')
        } else if (labelName === 'Low' && (this.props.data.lowCount.length === 0 || !this.props.data.lowCount)) {
            app.addAlert('warning', 'No Low Data Exists')
        } else {
            this.handleLink(labelName, '/intel/cve');
        }
    }

    handleLink(labelName, linkURL) {
        if (linkURL) {
            this.setCompanyAndCveSeverityOnSession(labelName);
            browserHistory.push(linkURL);
        }
    }

    setCompanyAndCveSeverityOnSession(labelName) {
        if (labelName && this.props.fromWhere) {
            if (this.props.companyName) {
                sessionStorage.setItem(SESSION_COMPANIES, this.props.companyName);
            }
            // Change current CVE SEVERITY as selected on Session - so its info will be displayed next in intel pages.
            sessionStorage.setItem(SESSION_CVE_SEVERITY, labelName);
            sessionStorage.setItem(SESSION_CVE_SEVERITY_PANEL, this.props.fromWhere);
        }
    }

    render() {
        const styles = {
            pieChartDiv: {
                height: 200,
                textAlign: 'center',
                marginTop: 18,
                marginLeft: 18
            },
            noDataDiv: {
                height: 140,
                textAlign: 'center',
                paddingTop: 100
            },
            title: {
                fontSize: 16,
                padding: '10px',
                fontWeight: 300,
                color: 'black',
                marginLeft: '7px'
            }
        };

        const labels = [
            {label: 'Critical', backgroundColor: '#d32f2f'},
            {label: 'High', backgroundColor: '#F4511E'},
            {label: 'Medium', backgroundColor: '#F57C00'},
            {label: 'Low', backgroundColor: '#0091ea'}
        ];

        const data = {
            labels: [
                'Critical',
                'High',
                'Medium',
                'Low'
            ],
            datasets: [{
                data: [this.props.data.criticalCount, this.props.data.highCount, this.props.data.mediumCount, this.props.data.lowCount],
                backgroundColor: [
                    '#d32f2f',
                    '#F4511E',
                    '#F57C00',
                    '#0091ea'
                ],
                hoverBackgroundColor: [
                    '#d32f2f',
                    '#F4511E',
                    '#F57C00',
                    '#0091ea'
                ]
            }]
        };

        let noDataYet = <div style={styles.noDataDiv}>
            <p style={{fontSize: '24px', marginTop: '25px'}}>No data found</p>
        </div>;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <div style={{marginBottom: '13px'}}>
                        <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                        <span style={{position: 'relative', top: '-6px', fontSize: 16}}>CVE SEVERITY</span>
                    </div>
                    <Card style={{borderRadius: '5px', height: '280px', paddingTop: 25}}>
                        {(!this.props.data.criticalCount
                            && !this.props.data.highCount
                            && !this.props.data.mediumCount
                            && !this.props.data.lowCount) ? noDataYet :
                            <Row>
                                <Col sm={8} xs={12} md={10} lg={8}>
                                    <div style={styles.pieChartDiv}>
                                        <Pie data={data}
                                             width={30}
                                             height={30}
                                             options={{
                                                 maintainAspectRatio: false
                                             }}
                                             legend={{display: false}}
                                             redraw
                                        />
                                    </div>
                                </Col>

                                <Col sm={4} xs={12} md={4} lg={4}>
                                    <List>
                                        {labels.map((item) =>
                                            <ListItem
                                                style={{fontSize: 14, borderRadius: '35px 0px 0px 35px'}}
                                                key={item.label}
                                                onClick={this.handleRedirect.bind(this, item.label)}
                                                leftAvatar={
                                                    <Avatar icon={item.icon}
                                                            backgroundColor={item.backgroundColor}
                                                            size={30}
                                                            style={{marginTop: 5.5}}
                                                    />
                                                }>
                                                {item.label}
                                            </ListItem>
                                        )}
                                    </List>
                                </Col>
                            </Row>
                        }
                    </Card>
                </div>
            </MuiThemeProvider>
        );
    };
}

export default CveSeverity;
