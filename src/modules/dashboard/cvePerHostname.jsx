import React from 'react';
import {Card, CardHeader} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import {browserHistory} from 'react-router';
import parseDomain from 'parse-domain';
import Popover from 'material-ui/Popover/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import NavigateMore from 'material-ui/svg-icons/navigation/more-vert';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../app/themes/themeForFont";
import {PROJECT_ISSELFASSESSMENT} from "../../../config/consts";

class CvePerHostname extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: app.getAuthUser(),
            cvePerHostname: [],
            topResultsLimit: 5,
            openTopResultsSelector: false
        };
    }

    componentDidMount() {
        if (!this.state.user) {
            app.routeAuthUser();
        }
    }

    redirect(hostname) {
        if (this.state.user && this.state.user.userRole !== 'basic') {
            let domain = parseDomain(hostname).domain + '.' + parseDomain(hostname).tld;
            browserHistory.push('/intel/cve/domain/' + domain);
        }
    }

    vendorRedirect(vendorId) {
        if (vendorId) {
            browserHistory.push('/company-dashboard/' + vendorId);
        }
    }

    openTopResultsPopover(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openTopResultsSelector) {
            this.setState({
                openTopResultsSelector: true,
                anchorEl: event.currentTarget
            });
        }
    }

    closeTopResultsPopover() {
        if (this.state.openTopResultsSelector) {
            this.setState({openTopResultsSelector: false});
        }
    }
    changeTopResultsLimit(newLimit) {
        let stateObjToUpdate = {
            openTopResultsSelector: false
        };

        if (this.state.topResultsLimit !== newLimit) {
            stateObjToUpdate.topResultsLimit = newLimit;
        }

        this.setState(stateObjToUpdate);
    }

    render() {
        const styles = {
            paper: {
                minHeight: 280,
                height: 280,
                borderRadius: '5px',
                marginBottom: '25px',
                paddingTop: '10px'
            },
            list: {
                fontSize: 12,
                overflowY: 'auto',
                maxHeight: 215
            },
            div: {
                marginLeft: 'auto',
                marginRight: 'auto',
                width: '95%',
                fontSize: 12
            },
            noDataDiv: {
                textAlign: 'center',
                paddingTop: 96
            },
            header: {
                padding: 10,
            },
            listItem: {
                boarderRadius: 3,
                fontSize: 12,
                padding: 13
            },
            menuItem: {
                fontSize: 14,
            }
        };
        const user = this.props.user || {};
        const isInDemoMode = user && user.isInDemoMode;
        let isSelfAssessmentProject = localStorage.getItem(PROJECT_ISSELFASSESSMENT);
        let gaugeDisplayEnabled = user && user.userPreferencesConfig && user.userPreferencesConfig.gaugeDisplay;
        let isInMainDashboard = this.props.inMainDashboard || false;
        let data;
        let titleEnd = '';

        //In case we are in main dashboard and the project is Self assessment project with more then one company we show vulnerable entities
        if(isInMainDashboard && isSelfAssessmentProject === 'true'){
            data = this.props.data;
            titleEnd = 'ENTITIES';
        //In case we are in main dashboard we show vulnerable vendors
        } else if (isInMainDashboard) {
            data = this.props.data;
            titleEnd = 'VENDORS';

        //In case we are in company dashboard we show vulnerable domains
        } else {
            data = this.props.data.sort((a, b) => {
                return b.cve - a.cve;
            });
            titleEnd = 'DOMAINS';
        }

        let cardTitle;

        if (this.state.topResultsLimit) {
            cardTitle = 'TOP ' + this.state.topResultsLimit + ' VULNERABLE ' + titleEnd;

            data = data.slice(0, this.state.topResultsLimit);
        } else {
            // When topResultsLimit is 0 - meaning all data should be displayed.
            cardTitle = 'ALL VULNERABLE ' + titleEnd;
        }

        const noDataYet = <div style={styles.noDataDiv}>
            <p style={{fontSize: '24px', marginTop: '25px'}}>No data found</p>
        </div>;

        const limitSelector = <span>
            <NavigateMore color={'black'} onClick={this.openTopResultsPopover.bind(this)}/>
            <Popover
                open={this.state.openTopResultsSelector}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                onRequestClose={this.closeTopResultsPopover.bind(this)}>
                <Menu value={this.state.topResultsLimit}>
                    <MenuItem style={styles.menuItem} primaryText="Top 5" value={5}
                              onClick={this.changeTopResultsLimit.bind(this, 5)}/>
                    <MenuItem style={styles.menuItem} primaryText="Top 10" value={10}
                              onClick={this.changeTopResultsLimit.bind(this, 10)}/>
                    <MenuItem style={styles.menuItem} primaryText="Top 30" value={30}
                              onClick={this.changeTopResultsLimit.bind(this, 30)}/>
                    <MenuItem style={styles.menuItem} primaryText="Show All" value={0}
                              onClick={this.changeTopResultsLimit.bind(this, 0)}/>
                </Menu>
            </Popover>
        </span>;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <div style={{marginBottom: '13px'}}>
                        <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                        <span style={{position: 'relative', top: '-6px', fontSize: 16, width: '100%'}}>{cardTitle}
                            <span style={{float: 'right'}}>{limitSelector}</span></span>
                    </div>
                    <Card style={styles.paper}>
                        <div style={styles.div}>
                            {(data && data.length === 0) ? noDataYet :
                                <List style={styles.list} key={80}>
                                    {
                                        data.map((item, i) => {
                                            return <div key={i}>
                                                <ListItem innerDivStyle={styles.listItem}
                                                          onClick={isInMainDashboard ?
                                                              this.vendorRedirect.bind(this, item.id) :
                                                              this.redirect.bind(this, item.hostname)}
                                                          primaryText={isInMainDashboard ?
                                                              (isInDemoMode? 'Company Name Example ' + i : item.companyName):
                                                              (isInDemoMode? 'domain-example-' + i + '.com' : item.hostname)}>
                                            <span key={'i' + i}
                                                  style={{
                                                      fontSize: 12,
                                                      float: 'right'
                                                  }}>{isInMainDashboard ?
                                                gaugeDisplayEnabled ? 'Total Risk: ' + item.gaugeVal : 'Total Score: ' + item.totalScore
                                                : item.cve}
                                            </span>
                                                </ListItem>
                                                <Divider/>
                                            </div>;
                                        })
                                    }
                                </List>
                            }
                        </div>
                    </Card>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default CvePerHostname;
