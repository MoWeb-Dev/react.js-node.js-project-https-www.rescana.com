import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {Row, Col, ClearFix} from 'react-grid-system';
import ScoreBox from '../dashboard/score-box.jsx';
import {typography} from 'material-ui/styles';
import {grey600} from 'material-ui/styles/colors';
import CvePerHostname from './cvePerHostname.jsx';
import {browserHistory} from 'react-router';
import CveSeverity from '../dashboard/cveSeverity.jsx';
import classNames from 'classnames';
import AssetMap from './assetMap.jsx';
import '../../../public/css/main.css';
import {
    PROJECT_NAME,
    SCORE_TYPES,
    SESSION_COMPANIES,
    DASHBOARD_COMPANIES_SORT_TYPES,
    DASHBOARD_SORT_TYPE, PROJECT_ISSELFASSESSMENT, ORGANIZATION_ID
} from '../../../config/consts';
import {groupLatLan, calculateTotalScore, chooseColor} from '../common/CommonHelpers.js';
import Pagination from 'material-ui-pagination';
import Loader from 'react-loader-advanced';
import themeForFont from "../app/themes/themeForFont";
import TextField from 'material-ui/TextField';
import SearchIcon from 'material-ui/svg-icons/action/search';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Paper from 'material-ui/Paper';
import VendorsSeverity from "./vendorsSeverity.jsx";
import {Doughnut, Pie} from 'react-chartjs-2';
import Avatar from 'material-ui/Avatar';
import ListItem from 'material-ui/List/ListItem';
import List from 'material-ui/List/List';

const _ = require('lodash');


const MAX_COMPANIES_AT_ONE_PAGE = 10;

class Dashboard extends React.Component {
    constructor() {
        super();

        // Check if localStorage contains any valid preference for companies sort type in dashboard.
        const dst = localStorage.getItem(DASHBOARD_SORT_TYPE);
        let defaultSelectedDisplaySort = (dst && DASHBOARD_COMPANIES_SORT_TYPES.includes(dst)) ? dst : DASHBOARD_COMPANIES_SORT_TYPES[0];

        this.state = {
            portsCount: 0,
            companies: [],
            currentCompaniesPage: 1,
            sliceStartIndex: 0,
            totalCompanies: 0,
            showLoader: false,
            activeAjaxCalls: 0,
            searchField: '',
            allProjectCompanies: [],
            filteredCompaniesFromSearch: [],
            vulnerableVendorArrSorted: [],
            openMoreInfoDialog: false,
            selectedDisplaySort: defaultSelectedDisplaySort
        };
        this.checkCompaniesForSearchMatch = this.checkCompaniesForSearchMatch.bind(this);
        this.getMaxCVSScore = this.getMaxCVSScore.bind(this);
    }

    componentDidMount() {
        app.setFlagVisibile(false);
        // Check if the user still logged in.
        let user = app.getAuthUser();
        if (!user) {
            app.routeAuthUser();
        } else {
            // Listen to url change.
            let unlisten = browserHistory.listen((location) => {
                let pn = localStorage.getItem(PROJECT_NAME);
                // Or if in dashboard page and session storage contains project name.
                if (location.pathname.startsWith('/dashboard')) {
                    if (pn) {
                        this.startLoader();
                        this.getAllCompaniesByProjectName(pn,
                            this.getMaxCVSScore.bind(this, pn));
                        this.getAllProjectAssetsLocation(pn);
                    } else {
                        app.addAlert('info', 'No projects available.');
                    }
                    // If path parameter 'project' exists
                } else if (this.props.params.project) {
                    this.startLoader();
                    this.getAllCompaniesByProjectName(this.props.params.project,
                        this.getMaxCVSScore.bind(this, this.props.params.project));
                    this.getAllProjectAssetsLocation(this.props.params.project);
                }
            });

            // The return value from browserHistory is used to unlisten (weird).
            this.setState({
                browserHistoryListen: unlisten,
                user: user
            });
        }
    }

    componentWillUnmount() {
        // Unlisten to browserHistory
        if (this.state.browserHistoryListen) {
            this.state.browserHistoryListen();
        }
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (this.state.companies) {
            this.saveCompaniesOnSession();
        }
    }

    saveCompaniesOnSession() {
        sessionStorage.setItem(SESSION_COMPANIES, this.state.companies.map((c) => {
            return c.companyName;
        }).join(', '));
    }

    getMaxCVSScore(project) {
        this.state.activeAjaxCalls++;
        const orgId = localStorage.getItem(ORGANIZATION_ID);

        let data = {
            project: project || this.state.project,
            selectedDisplaySort: this.state.selectedDisplaySort,
            orgId: orgId,
            getExtraInfo: false
        };

        $.ajax({
            type: 'POST',
            url: '/api/getMaxCVSScore',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data && Array.isArray(data) && data.length > 0) {
                // showLoader = false will stop the loader
                this.setState({
                    companies: data.slice(this.state.sliceStartIndex, this.state.sliceStartIndex + MAX_COMPANIES_AT_ONE_PAGE),
                    allProjectCompanies: data,
                    filteredCompaniesFromSearch: (data.slice(this.state.sliceStartIndex, this.state.sliceStartIndex + MAX_COMPANIES_AT_ONE_PAGE)),
                }, this.reduceAjaxCall);
                this.createMostVulnerableVendorArr(data);
            } else {
                this.reduceAjaxCall();
            }
        }).fail((jqXHR, textStatus, errorThrown) => {
            this.reduceAjaxCall();
            if (textStatus === 'parsererror') {
                app.removeAuthUser();
                app.handleSignOut();
            }
        });
    }

    createMostVulnerableVendorArr(data) {
        const user = this.state.user;
        let displayScoreBackwards = app.shouldDisplayScoreBackwards(user);

        let gaugeDisplayEnabled = user && user.userPreferencesConfig && user.userPreferencesConfig.gaugeDisplay;

        let newDataArr = [];
        data.map((comp) => {
            if (comp.companyName && comp.id && comp.scoresData && comp.scoresData.hasOwnProperty("totalScore")) {
                let totalScore = comp.scoresData.totalScore;
                let gaugeVal = 'No Data';
                if (gaugeDisplayEnabled) {
                    if (displayScoreBackwards) {
                        totalScore = app.switchScoreBackwards(comp.scoresData.totalScore, SCORE_TYPES.COMPANY);
                        gaugeVal = this.checkGaugeValue(totalScore, true);
                    } else {
                        gaugeVal = this.checkGaugeValue(totalScore, false);
                    }
                } else {
                    if (displayScoreBackwards) {
                        totalScore = app.switchScoreBackwards(comp.scoresData.totalScore, SCORE_TYPES.COMPANY);
                    }
                }

                let compToAdd = {
                    id: comp.id,
                    companyName: comp.companyName,
                    totalScore: totalScore,
                    gaugeVal: gaugeVal
                };
                newDataArr.push(compToAdd);
            }
        });

        let vulnerableVendorArr = this.sortArrByTotalScore(newDataArr, displayScoreBackwards);
        this.setState({
            vulnerableVendorArrSorted: vulnerableVendorArr
        });
    }

    checkGaugeValue(valueNum, isBackwards) {
        let gaugeVal = "No Data";
        if (valueNum !== null) {
            if (valueNum < 33) {
                gaugeVal = isBackwards ? "High" : "Low";
            } else if (valueNum >= 33 && valueNum < 66) {
                gaugeVal = "Medium";
            } else if (valueNum >= 66 && valueNum <= 100) {
                gaugeVal = isBackwards ? "Low" : "High";
            }
        }
        return gaugeVal;
    }

    sortArrByTotalScore(data, isBackwards) {
        if (data && data.length > 0) {
            data.sort((a, b) => {
                let numA = 0;
                let numB = 0;
                if (a.hasOwnProperty("totalScore")) {
                    numA = Number(a.totalScore);
                }
                if (b.hasOwnProperty("totalScore")) {
                    numB = Number(b.totalScore);
                }

                return isBackwards ?
                    (numA < numB) ? -1 : (numA > numB) ? 1 : 0
                    : (numA > numB) ? -1 : (numA < numB) ? 1 : 0;
            });
        }
        return data;
    }

    getAllProjectAssetsLocation(project) {
        this.state.activeAjaxCalls++;

        let data = {
            project: project || this.state.project
        };

        $.ajax({
            type: 'POST',
            url: '/api/getLocations',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((locations) => {
            let latLan = groupLatLan(locations);
            this.setState({
                locations: latLan
            }, this.reduceAjaxCall);
        }).fail((jqXHR, textStatus, errorThrown) => {
            this.reduceAjaxCall();

            if (textStatus === 'parsererror') {
                app.removeAuthUser();
                app.handleSignOut();
            }
        });
    }

    getAllCompaniesByProjectName(project, cb = () => {
    }) {
        if (project) {
            this.state.activeAjaxCalls++;

            let dataToSend = {
                project: project
            };

            $.ajax({
                type: 'POST',
                url: '/api/getCompaniesByProjectName',
                async: true,
                data: JSON.stringify(dataToSend),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((result) => {
                let totalCompanies;
                if (result) {
                    let isSelfAssessmentProject = localStorage.getItem(PROJECT_ISSELFASSESSMENT);
                    //if project is self assessment project and it has only 1 company we drill down right away
                    if(result.length === 1 && isSelfAssessmentProject === 'true with one company'){
                        this.openDrillDown(result[0]._id, result[0].companyName);
                    }
                    totalCompanies = result.length || 0;
                }
                // Not calling reduceAjaxCall since there is a callback afterwards.
                this.state.activeAjaxCalls--;

                this.setState({
                    totalCompanies: totalCompanies,
                }, cb);
            }).fail((jqXHR, textStatus, errorThrown) => {
                this.reduceAjaxCall();

                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        }
    }

    openDrillDown(companyID, companyName) {
        if (companyID) {
            if (companyName) {
                this.setCompanyOnSession(companyName)
            }
            browserHistory.push('/company-dashboard/' + companyID);
        }
    }

    setCompanyOnSession(companyName) {
        if (companyName) {
            // Change current company as selected on Session - so its info will be displayed next in intel pages.
            sessionStorage.setItem(SESSION_COMPANIES, companyName);
        }
    }

    handlePaginationChange(number) {
        if (this.state.currentCompaniesPage !== number) {
            this.setState({
                currentCompaniesPage: number,
                sliceStartIndex: (number - 1) * MAX_COMPANIES_AT_ONE_PAGE
            }, this.getPartialDataAfterPagination);
        }
    }

    getPartialDataAfterPagination() {
        this.startLoader();

        let pn = localStorage.getItem(PROJECT_NAME) || this.props.params.project;

        this.getMaxCVSScore(pn);
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    reduceAjaxCall() {
        this.state.activeAjaxCalls--;
        if (this.state.activeAjaxCalls < 1) {
            this.stopLoader();
        }
    }

    checkCompaniesForSearchMatch(value) {
        let filteredCompaniesArr = [];
        this.state.allProjectCompanies.map((el) => {
            if (el.companyName && el.companyName.toLowerCase().includes(value)) {
                filteredCompaniesArr.push(el);
            }
        });
        return filteredCompaniesArr;
    }

    handleChangeOnSearch(e) {
        let value = e.target.value.toLowerCase();
        let filteredCompaniesArr = [];
        if (value === '') {
            this.setState({
                filteredCompaniesFromSearch: this.state.companies,
            });
        } else {
            filteredCompaniesArr = this.checkCompaniesForSearchMatch(value);
            this.setState({
                filteredCompaniesFromSearch: filteredCompaniesArr,
            });
        }
    }


    render() {
        const orgId = localStorage.getItem(ORGANIZATION_ID);
        let user = this.state.user || {};
        let canUseVendorAssessment = (user.isVendorAssessmentEnabled || user.userRole === "admin") || false;
        let gaugeDisplayEnabled = user && user.userPreferencesConfig && user.userPreferencesConfig.gaugeDisplay;

        let containerStyle = classNames({
            'containerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForSurveys': true
        });

        const spinner = <div className={spinnerClass}/>;

        let style = {
            row: {
                padding: '10px',
                height: '276px'
            },
            companyRow: {
                margin: '30px -5px'
            },
            locationRow: {
                margin: '30px -5px'
            },
            paginationRow: {
                margin: '30px -5px',
                display: 'grid',
                grid: 'auto / auto auto auto',
            },
            vendorAssessmentRow: {
                margin: '30px -5px'
            },
            navigation: {
                fontSize: 15,
                fontWeight: typography.fontWeightLight,
                color: grey600,
                paddingBottom: 15,
                display: 'block'
            },
            infoBox: {
                cursor: 'pointer'
            },
            paperStyle: {
                borderRadius: 5,
                fontSize: 14,
                height: 200,
                padding: '20px 20px 25px 0px'
            },
            pieChartDiv: {
                height: 150,
                textAlign: 'center',
                margin: '0px 20px 20px 450px',
                position: 'relative',
                bottom: 172
            },
        };

        let currentPageCompanies = this.state.filteredCompaniesFromSearch;

        let displayScoreBackwards = app.shouldDisplayScoreBackwards(this.state.user);


        let scoreBoxs = currentPageCompanies && currentPageCompanies.map((company, i) => {
            let isSurveyNA = (company.surveyScore == undefined || company.isSurveyNA);
            let total = (company.hasOwnProperty("totalScore")) ? company.totalScore :
                calculateTotalScore(company.maxIntelScore, company.surveyScore || 0, company.ratios, isSurveyNA);

            let color = chooseColor(total);
            let intelScore = company.maxIntelScore || 0;
            let surveyScore = company.surveyScore || 0;
            let smNum = 2.4;
            let styleObj = {};
            /* In case max columns in first row is exceeded - add spacing from top elements. */
            if ((i + 1) > (12 / smNum)) {
                styleObj = {marginTop: '30px'};
            }
            let totalToDisplay = total;
            if (displayScoreBackwards) {
                totalToDisplay = app.switchScoreBackwards(totalToDisplay, SCORE_TYPES.COMPANY);
                intelScore = app.switchScoreBackwards(intelScore, SCORE_TYPES.COMPANY);
            }

            let circlePercentValue;
            if (gaugeDisplayEnabled) {
                circlePercentValue = total;
            } else {
                // If total is 0 - display all circle as Green = chooseColor(0).
                circlePercentValue = (total === 0) ? 100 : total;
            }
            let isUnsafeCompany = false;
            if(orgId && orgId !== 'No Id' && company.dataByOrg && typeof company.dataByOrg === 'object'){
                Object.keys(company.dataByOrg).map((key)=>{
                    if(key === orgId && company.dataByOrg[key].hasOwnProperty('isUnsafeCompany')){
                        isUnsafeCompany = company.dataByOrg[key].isUnsafeCompany;
                    }
                })
            }
            return <Col sm={smNum} key={i} style={styleObj}>
                <div>
                    <ScoreBox
                        user={user}
                        key={company.createDate}
                        title={company.companyName}
                        value={total || 0}
                        valueToDisplay={totalToDisplay || 0}
                        circlePercentValue={circlePercentValue}
                        color={color}
                        surveyScore={surveyScore}
                        intelScore={intelScore}
                        sensitivity={company.sensitivity}
                        companyID={company.id}
                        isSurveyNA={isSurveyNA}
                        assessmentScore={company.assessmentScore || 0}
                        isUnsafeCompany={isUnsafeCompany || false}
                    />
                </div>
            </Col>;
        });

        let cyberRiskAssessmentCount = 0;
        let intelligenceAssessmentInProgressCount = 0;
        let vendorAmountNum = this.state.allProjectCompanies.length || 0;

        this.state.allProjectCompanies.map((comp) => {
            if (comp && comp.assessmentScore && comp.assessmentScore === 100) {
                cyberRiskAssessmentCount++;
            }
            if (comp && comp.scoresData && comp.scoresData.intelScore && comp.scoresData.intelScore > 0) {
                intelligenceAssessmentInProgressCount++;
            }
        });

        const completedAssessmentData = {
            labels: [' Completed', ' Not Completed',],
            datasets: [{
                data: [cyberRiskAssessmentCount, vendorAmountNum - cyberRiskAssessmentCount],
                backgroundColor: ['#0091ea', '#eeeeee'],
                hoverBackgroundColor: ['#0091ea', '#EEEEEE']
            }]
        };

        const inProgressData = {
            labels: [' In Progress', ' Not In Progress',],
            datasets: [{
                data: [intelligenceAssessmentInProgressCount, vendorAmountNum - intelligenceAssessmentInProgressCount],
                backgroundColor: ['#0091ea', '#EEEEEE'],
                hoverBackgroundColor: ['#0091ea', '#EEEEEE']
            }]
        };

        const completedLabels = [
            {
                label: 'Completed',
                color: 'white',
                backgroundColor: '#0091ea',
                count: cyberRiskAssessmentCount},
            {
                label: 'Not Completed',
                color: 'black',
                backgroundColor: '#EEEEEE',
                count: (vendorAmountNum - cyberRiskAssessmentCount)
            }
        ];

        const inProgressLabels = [
            {
                label: 'In Progress',
                color: 'white',
                backgroundColor: '#0091ea',
                count: intelligenceAssessmentInProgressCount
            },
            {
                label: 'Not In Progress',
                color: 'black',
                backgroundColor: '#EEEEEE',
                count: (vendorAmountNum - intelligenceAssessmentInProgressCount)
            }
        ];
        let cyberRiskAssessment = <span> </span>;
        let intelligenceAssessmentInProgress = <span> </span>;

        if (this.state.allProjectCompanies) {
            cyberRiskAssessment = <Paper style={style.paperStyle}>
                <ListItem disabled={true} style={{
                    backgroundColor: 'rgba(0,0,0,0.05)', cursor: 'default', width: 340, fontSize: 14,
                    marginBottom: 3, textAlign: 'center', borderRadius: '0px 35px 35px 0px'
                }}>Completed Cyber Risk Assessment</ListItem>
                <List style={{width: '40%'}}>
                    {completedLabels.map((item) =>
                        <ListItem
                            disabled={true}
                            style={{
                                marginLeft: 15,
                                fontSize: 13,
                                cursor: 'default',
                                backgroundColor: 'white',
                                borderRadius: '0px 35px 35px 0px'
                            }}
                            key={item.label}
                            leftAvatar={<Avatar icon={item.icon} backgroundColor={item.backgroundColor} size={28}
                                                style={{color: item.color, marginTop: 6}}/>}>
                            {item.label}
                        </ListItem>
                    )}
                </List>
                <div style={style.pieChartDiv}>
                    <Doughnut data={completedAssessmentData} width={30} height={30}
                              options={{maintainAspectRatio: false}}
                              legend={{display: false}}/>
                              <span style={{color:'#0091ea',fontSize: 35, position: 'relative', bottom: 86 }}>{completedLabels[0].count}</span>
                              <span style={{color:'#b5b5b5', fontSize: 25, position: 'relative',bottom: 86 }}>/{vendorAmountNum}</span>
                </div>
            </Paper>;

            intelligenceAssessmentInProgress = <Paper style={style.paperStyle}>
                <ListItem disabled={true} style={{
                    backgroundColor: 'rgba(0,0,0,0.05)', cursor: 'default', marginBottom: 3, width: 340, fontSize: 14,
                    textAlign: 'center', borderRadius: '0px 35px 35px 0px'
                }}>Intelligence Assessment In Progress</ListItem>
                <List style={{width: '40%'}}>
                    {inProgressLabels.map((item) =>
                        <ListItem
                            disabled={true}
                            style={{
                                marginLeft: 15,
                                fontSize: 13,
                                cursor: 'default',
                                backgroundColor: 'white',
                                borderRadius: '0px 35px 35px 0px'
                            }}
                            key={item.label}
                            leftAvatar={<Avatar icon={item.icon} backgroundColor={item.backgroundColor} size={28}
                                                style={{color: item.color, marginTop: 6}}/>}>
                            {item.label}
                        </ListItem>
                    )}
                </List>
                <div style={style.pieChartDiv}>
                    <Doughnut data={inProgressData} width={30} height={30} options={{maintainAspectRatio: false}}
                              legend={{display: false}}/>
                    <span style={{color:'#0091ea',fontSize: 37, position: 'relative', bottom: 86 }}>{inProgressLabels[0].count}</span>
                </div>
            </Paper>;
        }

        return (
            <Loader show={this.state.showLoader} message={spinner}>
                <div className={containerStyle}>
                    <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                        <div style={{margin: '22px', fontSize: '30px'}}>
                            <ClearFix/>
                            {canUseVendorAssessment &&
                            <Row style={style.vendorAssessmentRow}>
                                <div style={{marginBottom: '13px', marginLeft: '14px'}}>
                                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                                    <span style={{position: 'relative', top: '-6px', fontSize: 16}}>
                                        {"ASSESSMENT SUMMARY"}
                                    </span>
                                </div>
                                <Col sm={6}>
                                    {intelligenceAssessmentInProgress}
                                </Col>
                                <Col sm={6}>
                                    {cyberRiskAssessment}
                                </Col>
                            </Row>
                            }
                            <ClearFix/>
                            <Row style={style.row}>
                                <Col sm={6}>
                                    <VendorsSeverity data={this.state.vulnerableVendorArrSorted} key={'f1'}
                                                     vendorsAmount={this.state.allProjectCompanies.length}
                                                     fromWhere={"mainDashboard"}/>
                                </Col>
                                <Col sm={6}>
                                    <CvePerHostname user={this.state.user} inMainDashboard={true}
                                                    data={this.state.vulnerableVendorArrSorted}
                                                    key={'f2'}/>
                                </Col>
                            </Row>
                            <ClearFix/>
                            <Row style={style.companyRow}>
                                {currentPageCompanies &&
                                <div style={{marginBottom: '13px', marginLeft: '14px'}}>
                                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                                    <span style={{position: 'relative', top: '-6px', fontSize: 16}}>COMPANIES</span>
                                    <SearchIcon style={{position: 'relative', left: 30, color: 'rgba(0,0,0,0.5)'}}/>
                                    <TextField
                                        hintText="Search"
                                        type="text"
                                        style={{position: 'relative', top: '-5px', left: 38, fontSize: 16}}
                                        underlineShow={true}
                                        onChange={this.handleChangeOnSearch.bind(this)}
                                    />
                                    <IconMenu
                                        iconButtonElement={<IconButton><MoreVertIcon/></IconButton>}
                                        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                                        targetOrigin={{horizontal: 'right', vertical: 'top'}}
                                        style={{
                                            float: "right",
                                            position: "relative",
                                            top: -10,
                                            fontSize: 12,
                                        }}
                                        value={this.state.selectedDisplaySort}
                                        onChange={(event) => {
                                            if (event && event.target && event.target.textContent && this.state.selectedDisplaySort !== event.target.textContent) {
                                                if (event.preventDefault) {
                                                    event.preventDefault();
                                                }
                                                const sortType = event.target.textContent;
                                                // First, change the selected sort option.
                                                this.setState({
                                                    selectedDisplaySort: sortType,
                                                    showLoader: true
                                                }, () => {
                                                    // Save new preference on localStorage.
                                                    localStorage.setItem(DASHBOARD_SORT_TYPE, sortType);

                                                    const pn = localStorage.getItem(PROJECT_NAME);
                                                    // Then, Fetch companies again.
                                                    this.getMaxCVSScore(pn);
                                                });
                                            }
                                        }}
                                    >
                                        {DASHBOARD_COMPANIES_SORT_TYPES.map((currSortType, index) => {
                                            return <MenuItem key={index} value={currSortType}
                                                             primaryText={currSortType}/>;
                                        })}
                                    </IconMenu>
                                </div>}
                                {scoreBoxs}
                            </Row>
                            <ClearFix/>
                            {<Row style={style.paginationRow}>
                                <div/>
                                <Pagination
                                    styleRoot={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                    }}
                                    total={Math.ceil(this.state.totalCompanies / MAX_COMPANIES_AT_ONE_PAGE)}
                                    current={this.state.currentCompaniesPage}
                                    display={5}
                                    onChange={(number) => this.handlePaginationChange(number)}/>
                                <div/>
                            </Row>
                            }

                            <ClearFix/>
                            <Row style={style.locationRow}>
                                <Col>
                                    <AssetMap user={user} data={this.state.locations} showLoader={this.state.showLoader}/>
                                </Col>
                            </Row>
                        </div>
                    </MuiThemeProvider>
                </div>
            </Loader>
        );
    }
}

export default Dashboard;
