import React from 'react';
import {
    BOTNETS_TYPE,
    ORGANIZATION_ID,
    PROJECT_NAME,
    RSS_FEEDS_EXTRA_INFO,
    SESSION_COMPANIES
} from '../../../config/consts';
import classNames from 'classnames';
import {getLoadedCompaniesObject, getDefaultCompanies} from '../common/datatableTemplate/datatable-template-helpers.js';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import '../../../public/css/survey.css';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Tabs, Tab} from 'material-ui/Tabs';
import {Card} from 'material-ui/Card';
import SwipeableViews from 'react-swipeable-views';
import SettingsFilterTemplate from '../common/datatableTemplate/settings-filter-template-main.jsx';
import Loader from 'react-loader-advanced';
import DataTableViewTemplate from '../common/datatableTemplate/datatable-template.jsx';
import DatePickerMain from '../common/datePicker/date-picker-main.jsx';
import themeForFont from "../app/themes/themeForFont";
import generalFunctions from "../generalComponents/general-functions";

import textForTitleExpl from "../appGeneralFiles/text-for-title-explanations";
const shortid = require('shortid');
import TitleExplanationComponent from '../appGeneralFiles/title-explanation-component.jsx';

export default class Botnets extends React.Component {
    constructor() {
        super();

        this.state = {
            showLoader: false,
            slideTabIndex: 0,
            companies: [],
            companiesInputVal: [],
            data: [],
            openInfoBox: false,
            assetUserInputAndMitigatedData: [],
            isTableDataReady: false,
            styles: {
                cardStyle: {
                    margin: 13,
                    color: 'rgb(250, 250, 250)'
                },
                headline: {
                    fontSize: 24,
                    paddingTop: 16,
                    marginBottom: 12,
                    fontWeight: 400
                },
                slide: {
                    padding: 10
                },
                inkBar: {
                    backgroundColor: 'white'
                },
                tabItemContainer: {
                    backgroundColor: '#0091ea', color: 'white'
                },
                tabItemLabel: {
                    fontSize: '16px',
                    color: 'white'
                },
                emptyResultStyle: {
                    backgroundColor: '#FFDD00',
                    display: 'inline-block',
                    padding: '1% 5%',
                    marginTop: '240px',
                    fontSize: 'large',
                    textAlign: 'center'
                },
                wrappableStyle: {
                    'whiteSpace': 'pre-wrap',
                    'wordBreak': 'break-word'
                },
                clickableWrappableStyle: {
                    'whiteSpace': 'pre-wrap',
                    'wordBreak': 'break-word',
                    'cursor': 'pointer'
                }
            },
            isTableTemplateIsMounted: false
        };

        this.handleTabChange = this.handleTabChange.bind(this);
        this.setResults = this.setResults.bind(this);
        this.updateState = this.updateState.bind(this);
        this.setDefaultCompanies = this.setDefaultCompanies.bind(this);
        this.saveLoadedCompanies = this.saveLoadedCompanies.bind(this);
        this.recalculateScore = this.recalculateScore.bind(this);
        this.updateStateForAMData = this.updateStateForAMData.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
        this.setState({isTableDataReady: true})
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            this.setState({user: user});
            // In case it is desired to save user preferences, use: this.getAllCompaniesByID(user);
            this.getAllowedCompanies(true);
        }
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    //AMData = assetUserInput and mitigated Data
    updateStateForAMData(assetUserInputAndMitigatedData) {
        this.setState({assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
    }

    recalculateScore() {
        let data = {};
        if (this.state.companiesInputVal && this.state.companiesInputVal.length > 0) {
            data.companyIDs = [];
            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        data.orgId = localStorage.getItem(ORGANIZATION_ID);
        data.saveScoresOnDB = true;
        $.ajax({
            type: 'POST',
            url: '/api/getMaxCVSScore',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'CalculateScores');
            } else {
                app.addAlert('Error', 'Error calculating scores');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to calculate scores');
        });
    }

    getAllowedCompanies(isDefault, selectedCompanies) {
        $.ajax({
            type: 'GET',
            url: '/api/getAllCompaniesById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((companies) => {
            if (isDefault) {
                this.saveDefaultCompanies(companies);
            } else {
                this.saveLoadedCompanies(companies, selectedCompanies);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load companies.');
        });
    }

    saveDefaultCompanies(companies) {
        if (companies) {
            let pn = localStorage.getItem(PROJECT_NAME);
            if (pn) {
                $.ajax({
                    type: 'POST',
                    url: '/api/getCompaniesByProjectName',
                    async: true,
                    data: JSON.stringify({project: pn}),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((projectCompanies) => {
                    this.setDefaultCompaniesToState(companies, projectCompanies);
                }).fail(() => {
                    app.addAlert('error', "Error: failed to load project's companies.");
                });
            } else {
                this.setDefaultCompaniesToState(companies, null);
            }
        }
    }

    handleTabChange(value) {
        if (value !== this.state.slideTabIndex) {
            this.setState({
                slideTabIndex: value
            });
        }
    }

    updateState(stateObj) {
        if (stateObj) {
            this.setState(stateObj, this.getBotnetsFindings);
        }
    }

    setDefaultCompaniesToState(companies, projectCompanies) {
        let result = getDefaultCompanies(companies, projectCompanies, 'selectedDomains');
        if (result && result.companies && result.companiesInputVal && result.companiesPropArray) {
            if (result.companiesInputVal.length > 1) {
                // If the Session has a selection saved - load it.
                let sessionCompanies = sessionStorage.getItem(SESSION_COMPANIES);
                if (sessionCompanies) {
                    let selectedNames = sessionCompanies.split(', ');
                    let relatedCompanies = result.companiesInputVal.filter((c) => {
                        return selectedNames.includes(c.companyName);
                    });
                    if (relatedCompanies && relatedCompanies.length > 0) {
                        result.companiesInputVal = relatedCompanies;
                    }
                } else {
                    // Select only the first company as a default selection.
                    result.companiesInputVal.splice(1, result.companiesInputVal.length);
                }
            }
            this.setDefaultCompanies(result);
        }
    }

    setDefaultCompanies(result) {
        if (result && result.companies && result.companiesInputVal) {
            this.setState({
                companies: result.companies,
                companiesInputVal: result.companiesInputVal
            }, this.getBotnetsFindings);
        }
    }

    saveLoadedCompanies(companies, selectedCompanies) {
        let result = getLoadedCompaniesObject(companies, selectedCompanies, 'selectedDomains');

        if (result && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: companies,
                companiesInputVal: result.companiesInputVal,
                companiesDomains: result.companiesPropArray
            }, this.getBotnetsFindings);
        }
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    getBotnetsFindings() {
        if (this.state.companiesInputVal && Array.isArray(this.state.companiesInputVal) && this.state.companiesInputVal.length > 0) {
            let companyIDs = [];
            let orgId = localStorage.getItem(ORGANIZATION_ID);

            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !companyIDs.includes(currComp.id)) {
                    companyIDs.push(currComp.id);
                }
            });
            if (companyIDs.length > 0) {
                this.startLoader();

                let data = {
                    companyIDs: companyIDs,
                    orgId: orgId
                };

                let startDate = this.state.startDate;
                let endDate = this.state.endDate;

                if (startDate && endDate) {
                    data.startDate = startDate;
                    data.endDate = endDate;
                }
                $.ajax({
                    type: 'POST',
                    url: '/api/getBotnets',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((res) => {
                    this.stopLoader();

                    let resultData = [];
                    let slideTabIndex = this.state.slideTabIndex;
                    let assetUserInputAndMitigatedData = [];
                    if (res && res.ok && res.data && Array.isArray(res.data)) {
                        resultData = res.data;
                        slideTabIndex = 0;
                        if(res.assetUserInputAndMitigatedData){
                            assetUserInputAndMitigatedData = res.assetUserInputAndMitigatedData;
                        }
                    }
                    this.setState({fullData: resultData, isTableTemplateIsMounted: false, slideTabIndex: slideTabIndex,
                        assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
                }).fail(() => {
                    this.stopLoader();
                    app.addAlert('error', 'Error: failed to fetch data');
                });
            }
        }
    }

    setResults() {
        const manualKeysAtStart = [{
            key: 'domain',
            label: 'Domain'
        }, {
            key: 'address',
            label: 'IP Address'
        }];

        const manualKeysAtEnd = [{
            key: 'relatedCompanies',
            label: 'Related Companies'
        }];

        const styles = this.state.styles;

        let data = this.state.fullData;
        let assetUserInputAndMitigatedData = this.state.assetUserInputAndMitigatedData;
        let tabs = [];
        let swipeableViews = [];
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;

        if (data && Array.isArray(data) && data.length > 0) {
            let idx = 0;
            let demoIdx=0;
            data.map((currData) => {
                if (currData.feedName && currData.feedData && Array.isArray(currData.feedData)) {
                    tabs[idx] = (<Tab key={shortid.generate()} style={styles.tabItemLabel} label={currData.feedName} value={idx}/>);

                    // Search RSS_FEED_EXTRA_INFO for feed's extra info.
                    let extraInfo = RSS_FEEDS_EXTRA_INFO.find((currConfigFeed) => {
                        return (currConfigFeed
                            && currConfigFeed.feedName
                            && currConfigFeed.feedName.toLowerCase() === currData.feedName.toLowerCase());
                    });

                    let extraInfoName;
                    let extraInfoData;
                    let extraInfoDescription;

                    if (extraInfo && extraInfo.feedData && Array.isArray(extraInfo.feedData) && extraInfo.feedData.length > 0) {
                        extraInfoName = extraInfo.feedName;
                        extraInfoData = extraInfo.feedData;
                        extraInfoDescription = (extraInfo.feedDescription) ? extraInfo.feedDescription : '';
                    } else {
                        extraInfoName = '';
                        extraInfoData = [];
                        extraInfoDescription = '';
                    }

                    // Iterate on feedData and arrange by: Domain, IP, otherProps, RelatedCompanies.
                    let tableColumns = [];
                    let tableColumnsToDownload = [];
                    let tableData = [];
                    let tableDataToDownload = [];
                    let columnsStyle = (extraInfoName) ? styles.clickableWrappableStyle : styles.wrappableStyle;

                    currData.feedData.map((currData) => {
                        if (currData && currData.domain && currData.address && currData.relatedCompanies) {
                            // If tableColumns hasn't been initialized yet - set according to first entity keys.
                            if (tableColumns.length === 0) {
                                // If this feed type has a configured extra data - mark it as clickable.

                                // First add the properties saved as manualKeysAtStart.
                                manualKeysAtStart.map((current) => {
                                    tableColumns.push({
                                        key: current.key,
                                        label: current.label,
                                        style: columnsStyle,
                                        sortable: true
                                    });

                                    tableColumnsToDownload.push({
                                        key: current.key,
                                        label: current.label
                                    });
                                });

                                // Then add other properties saved on object (that are not included in manualKeysAtStart or manualKeysAtEnd).
                                Object.keys(currData).map((currKey) => {
                                    if (currKey && currKey !== 'id' && !manualKeysAtStart.find((current) => {
                                            return current.key === currKey;
                                        })
                                        && !manualKeysAtEnd.find((current) => {
                                            return current.key === currKey;
                                        })) {
                                        tableColumns.push({
                                            key: currKey,
                                            label: currKey,
                                            style: columnsStyle,
                                            sortable: true
                                        });

                                        tableColumnsToDownload.push({
                                            key: currKey,
                                            label: currKey
                                        });
                                    }
                                });
                                // At last add the properties saved as manualKeysAtEnd.
                                manualKeysAtEnd.map((current) => {
                                    tableColumns.push({
                                        key: current.key,
                                        label: current.label,
                                        style: columnsStyle,
                                        noneSearchable: true
                                    });

                                    tableColumnsToDownload.push({
                                        key: current.key,
                                        label: current.label
                                    });
                                });

                                tableColumns[tableColumns.length] = {
                                    sortable: true,
                                    label: 'Importance',
                                    key: 'Importance',
                                    style: columnsStyle,
                                    noneSearchable: true};

                                tableColumns[tableColumns.length] = {
                                    sortable: false,
                                    label: 'Comments',
                                    key: 'Comments',
                                    style: columnsStyle,
                                    noneSearchable: true};

                                tableColumns[tableColumns.length] = {
                                    sortable: true,
                                    label: 'Mitigated',
                                    key: 'Mitigated',
                                    style: columnsStyle,
                                    noneSearchable: true};
                            }

                            if(tableColumnsToDownload.length < 8) {
                                tableColumnsToDownload[tableColumnsToDownload.length] = {
                                    key: 'Importance',
                                    label: 'Importance',
                                };
                            }

                            if(tableColumnsToDownload.length < 8){
                                tableColumnsToDownload[tableColumnsToDownload.length] = {
                                    key: 'Comments',
                                    label: 'Comments',
                                };
                            }
                            if(tableColumnsToDownload.length < 8){
                                tableColumnsToDownload[tableColumnsToDownload.length] = {
                                    key: 'Mitigated',
                                    label: 'Mitigated',
                                };
                            }


                            let tableEntityObj = {
                                surveyIdentifier: shortid.generate()
                            };

                            tableEntityObj.id = currData.id ? currData.id : null;

                            let tableEntityObjToDownload = {};

                            tableColumns.map((currColumn) => {
                                let currKey = currColumn.key;
                                if (currKey !== 'Importance' && currKey !== 'Comments' && currKey !== 'Mitigated') {
                                    if (currData[currKey]) {
                                        if (currKey === 'relatedCompanies') {
                                            tableEntityObj[currKey] = showExpandedDataChip(isInDemoMode? ['Company Name'] : currData[currKey], 3, 5);
                                            tableEntityObjToDownload[currKey] = isInDemoMode? 'Company Name' : currData[currKey].join(', ');
                                        } else {
                                            let curVal = '' + currData[currKey];
                                            if (isInDemoMode) {
                                                if (currKey === 'domain') {
                                                    curVal = 'domain-example-'+ demoIdx +'.com';
                                                } else if (currKey === 'address') {
                                                    curVal = generalFunctions.createIpStrForDemoMode(currData[currKey], demoIdx);
                                                }
                                            }
                                            // Apply all data as string so FuzzySearch will be able to use this registry properly.
                                            tableEntityObj[currKey] = curVal;
                                            tableEntityObjToDownload[currKey] = curVal;
                                        }
                                    } else {
                                        tableEntityObj[currKey] = '';
                                        tableEntityObjToDownload[currKey] = '';
                                    }
                                }
                            });
                            demoIdx++;

                            let importance = 3;
                            let mitigatedBackground = 'black';
                            let comments = [];
                            let assetUserInputFlag = false;
                            let mitigatedBackgroundFlag = false;
                            let commentsFlag = false;
                            let curRef = 'dataTableViewTemplate' + idx;

                            if(this.refs[curRef]){
                                for(let j=0; j<assetUserInputAndMitigatedData.length; j++){
                                    if(this.refs[curRef].isCurRowNeedToBeUpdateCurNode(tableEntityObj, assetUserInputAndMitigatedData[j])){
                                        if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'assetUserInput'){
                                            importance = assetUserInputAndMitigatedData[j].importance? Number(assetUserInputAndMitigatedData[j].importance) : 3;
                                            assetUserInputFlag = true;
                                        }
                                        if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'mitigated'){
                                            mitigatedBackground = '#5bd25e';
                                            mitigatedBackgroundFlag = true;
                                        }
                                        if(assetUserInputAndMitigatedData[j].comments){
                                            comments = assetUserInputAndMitigatedData[j].comments? assetUserInputAndMitigatedData[j].comments : [];
                                            commentsFlag = true;
                                        }
                                        if(mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag){
                                            break;
                                        }
                                    }
                                }
                                let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';

                                tableEntityObj.Importance = this.refs[curRef].getImportanceComponent(tableEntityObj, importance);
                                tableEntityObj.Comments = this.refs[curRef].getCommentsComponent(tableEntityObj, comments.length, comments, badgeColor);
                                tableEntityObj.Mitigated = this.refs[curRef].getMitigatedComponent(tableEntityObj, mitigatedBackground);

                                if(tableEntityObjToDownload.Importance === undefined){
                                    tableEntityObjToDownload.Importance = generalFunctions.getImportanceStr(importance);
                                }
                                if(tableEntityObjToDownload.Comments === undefined){
                                    tableEntityObjToDownload.Comments = generalFunctions.allCommentsToOneStr(comments);
                                }
                                if(tableEntityObjToDownload.Mitigated === undefined){
                                    tableEntityObjToDownload.Mitigated =  mitigatedBackgroundFlag ? 'Yes' : 'No';
                                }
                            }

                            tableData.push(tableEntityObj);
                            tableDataToDownload.push(tableEntityObjToDownload);
                        }
                    });
                    swipeableViews[idx] = (<div style={styles.slide}>
                        <DataTableViewTemplate
                            ref={"dataTableViewTemplate" + idx}
                            myDataType={BOTNETS_TYPE}
                            botnetsFeedName={currData.feedName}
                            tableColumns={tableColumns}
                            tableData={tableData}
                            tableColumnsToDownload={tableColumnsToDownload}
                            tableDataToDownload={tableDataToDownload}
                            extraInfoName={extraInfoName}
                            extraInfoData={extraInfoData}
                            extraInfoDescription={extraInfoDescription}
                            assetUserInputAndMitigatedData={this.state.assetUserInputAndMitigatedData}
                            recalculateScore={this.recalculateScore.bind(this)}
                            setResults={this.setResults.bind(this)}
                            updateStateForAMData={this.updateStateForAMData.bind(this)}
                        />
                    </div>);
                    idx++;
                }
            });
        } else {
            swipeableViews.push(<div style={{color: '#929292', padding: 50, margin: 'auto', textAlign: 'center'}}> No data to display</div>);
        }
        return {
            tabs: tabs,
            swipeableViews: swipeableViews
        };
    }

    filterDataByDates(startDate, endDate) {
        this.state.startDate = startDate;
        this.state.endDate = endDate;
        this.getBotnetsFindings();
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;
        const styles = this.state.styles;

        let swipeableViews = [];
        let tabs = [];
        let fullData = this.state.fullData;
        let isTableTemplateIsMounted = this.state.isTableTemplateIsMounted;

        //Happens after table are created
        if(isTableTemplateIsMounted){
            let dataResult = this.setResults();
            swipeableViews = dataResult.swipeableViews;
            tabs = dataResult.tabs;
        }

        let infoText = textForTitleExpl.botnets.expl;
        let shorterInfoText = textForTitleExpl.botnets.explShort;

        //Create tables first
        if(fullData && Array.isArray(fullData) && !isTableTemplateIsMounted){
            for(let i=0; i<fullData.length;i++){
                swipeableViews.push(<div style={styles.slide}>
                    <DataTableViewTemplate
                        ref={"dataTableViewTemplate" + i}
                        myDataType={BOTNETS_TYPE}
                        tableColumns={[]}
                        tableData={[]}
                        assetUserInputAndMitigatedData={this.state.assetUserInputAndMitigatedData}
                        recalculateScore={this.recalculateScore.bind(this)}
                        setResults={this.setResults.bind(this)}
                        updateStateForAMData={this.updateStateForAMData.bind(this)}
                    />
                </div>);
            }
            this.setState({isTableTemplateIsMounted: true});
        }

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div className={pageStyleClass}>
                    <div style={{margin: '40px 0px 40px 10px'}}>
                        <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                        <span style={{position: 'relative', top: '-6px', fontSize: 22}}>BOTNETS & MALWARE</span>
                    </div>
                    <TitleExplanationComponent
                        openInfoBox={this.state.openInfoBox}
                        anchorEl={this.state.anchorEl}
                        shorterInfoText={shorterInfoText}
                        infoText={infoText}
                        handleCloseInfoBox={this.handleCloseInfoBox.bind(this)}
                        handleOpenInfoBox={this.handleOpenInfoBox.bind(this)}
                    />
                    <Loader show={this.state.showLoader} message={spinner}>
                        <div>
                            <SettingsFilterTemplate
                                myDataType={BOTNETS_TYPE}
                                companies={this.state.companies}
                                companiesInputVal={this.state.companiesInputVal}
                                updateParentState={this.updateState}
                                setDefaultCompanies={this.setDefaultCompanies}
                                saveLoadedCompanies={this.saveLoadedCompanies}
                            />
                            <Card style={{margin: '20px 10px 20px 10px'}}>
                                <DatePickerMain
                                    filterDataByDates={this.filterDataByDates.bind(this)}
                                />
                            </Card>
                            <Card style={styles.cardStyle}>
                                <Tabs
                                    onChange={this.handleTabChange}
                                    value={this.state.slideTabIndex}
                                    tabItemContainerStyle={styles.tabItemContainer}
                                    children={tabs}
                                />
                                <SwipeableViews
                                    index={this.state.slideTabIndex}
                                    onChangeIndex={this.handleTabChange}
                                    children={swipeableViews}
                                />
                            </Card>
                        </div>
                    </Loader>
                </div>
            </MuiThemeProvider>
        );
    }
}
