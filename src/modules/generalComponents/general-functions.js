module.exports.getImportanceStr = (importance) => {
    let importanceStr = '-';
    if (importance) {
        if (Number(importance) === 1) {
            importanceStr = 'False Positive';
        } else if (Number(importance) === 2) {
            importanceStr = 'Low';
        } else if (Number(importance) === 3) {
            importanceStr = 'Medium';
        } else if (Number(importance) === 4) {
            importanceStr = 'High';
        }
    }
    return importanceStr;
};


module.exports.allCommentsToOneStr =(comments) => {
    let commentsStr = '';
    if(comments && Array.isArray(comments) && comments.length > 0){
        comments.map((comment)=>{
            commentsStr += comment + " \r\n ";
        });
    }
    return commentsStr;
};


module.exports.getDataFromHashArr = (val, hashDataArr) => {
    let hashVal = '';
    if(hashDataArr && Array.isArray(hashDataArr) && hashDataArr.length > 0){
        hashDataArr.map((curData)=>{
            if(curData && curData.demo && curData.real && curData.demo === val){
                hashVal = curData.real.toString();
                return hashVal;
            }
        });
    }
    return hashVal;
};

module.exports.createIpStrForDemoMode = (ip, idx = 1) => {
    let returnedStr = 'xx.xx.' + idx + '.';
    if(ip && ip.length > 0){
        returnedStr += ip.split('.')[3];
    } else {
        returnedStr += 'xx';
    }
    return returnedStr;
};
