import React from 'react';
import Divider from 'material-ui/Divider';
import InfoIcon from 'material-ui/svg-icons/action/help-outline';


export default class InformationHelpText extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let divStyle = {};
        if(this.props.dir !== 'ltr'){
            divStyle.textAlign= 'right';
        }

        return <div dir={this.props.dir|| 'ltr'} style={divStyle}>
            <Divider style={{marginTop: this.props.aboveSpace || '10px', marginBottom: 10}}/>
            <div style={{fontSize: 16.5, marginBottom: 5}}>
                <span><InfoIcon style={{margin: '0 8px 0 8px'}} color={'#626262'}/> {this.props.explanationHeader || ''} </span>
            </div>
            <div style={{fontSize: 14, margin: '0 42px 0 42px'}}>
                {this.props.explanationText || ''}
            </div>
        </div>
    }
}
