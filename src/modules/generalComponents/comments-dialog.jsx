import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import {Card} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import DropZone from 'react-dropzone';
import moment from 'moment';


export default class DynamicDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputText: '',
        };

    }

    handleInputTextChange(e) {
        let inputText = this.state.inputText || '';
        inputText = e.target.value;
        this.setState({inputText: inputText});
    };

    addComment() {
        let curCommentsArr = this.props.curCommentsArr || [];
        let inputText = this.state.inputText || '';
        const curTime = moment().format('YYYY/MM/DD, hh:mm:ss a');
        let curUser = app.getAuthUser();
        let userName = curUser && curUser.email ? " - " + curUser.email : ' ';
        let comment = curTime + userName + ": " + inputText;

        curCommentsArr.push(comment);
        this.props.updateCommentsArea(curCommentsArr);
        this.setState({inputText: ''});
    };

    deleteComment(idx) {
        let curCommentsArr = this.props.curCommentsArr || {};
        curCommentsArr.splice(idx, 1);
        this.props.updateCommentsArea(curCommentsArr);
    };

    render() {

        const styles = {
            inputCommentStyle:{
                grid: 'auto / auto 100px',
                display: 'grid'
            },
            cardStyle: {
                padding: '15px',
                margin: 10
            },
            notificationCardStyle: {
                borderRadius: 10,
                margin: 15,
                backgroundColor: '#eeeeee',
                height: 190,
                flexWrap: 'nowrap',
                overflowX: 'auto',
            },
            textAreaStyle: {
                fontSize: 14,
                outline: 'none',
                resize: 'none',
                border: '2px solid #e0e0e0',
                height: 20,
                borderRadius: 10,
                margin: 15,
                backgroundColor: '#eeeeee',
                flexWrap: 'nowrap',
                overflowX: 'auto',
                width: '95%',
                padding: 10,

            },
            attachmentStyle: {
                marginRight: 45,
                cursor: "pointer", width: 20, height: 20
            },
            deleteAttachmentStyle: {
                marginRight: 45,
                position: "relative",
                top: -21,
                left: 38,
                cursor: "pointer", width: 20, height: 20
            }
        };

        let curCommentsArr = this.props.curCommentsArr;

        return <Dialog
            open={this.props.open || false}
            title={this.props.title}
            anchorOrigin={{horizontal: 'middle', vertical: 'center'}}
            modal={false}
            handlebBtnCancelClick={this.props.closeCommentsDialog}
            actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
            bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
            autoScrollBodyContent={true}
            repositionOnUpdate={false}
            contentStyle={{padding: 15, maxWidth: '50%',height: '500px', borderRadius: '7px 7px 7px 7px'}}
            titleStyle={{
                fontSize: 18,
                background: 'rgba(0,0,0,0.7)',
                color: 'white', textAlign: 'center',
                borderRadius: '2px 2px 0px 0px',
                textTransform: 'uppercase',
            }}
            actions={[
                <FlatButton
                    label={'Close'}
                    primary={true}
                    backgroundColor={'#0091ea'}
                    style={{borderRadius: 5, marginRight: '10px', color: 'white', marginBottom: '5px', height: 40}}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    keyboardFocused={false}
                    onTouchTap={this.props.handlebBtnCloseClick}
                />
            ]}>
            <br/>
            <br/>
            {this.props.isFromMitigated && <div style={{margin: 12}}>Please enter comment regarding status change.</div>}
            <div style={{margin: 12}}>Comments Log:</div>
                <Card style={styles.notificationCardStyle}>
                    {
                        curCommentsArr.map((item, i) => {
                            return <div key={i}>
                                <div style={{grid: 'auto 10px / auto 50px', display: 'grid', padding: 5}}>
                                            <span style={{padding: '0px 15px 0px 15px', position: "relative", top: 5}}>
                                                <span style={{fontSize: 13}}>{curCommentsArr[i] ? curCommentsArr[i] : null}</span>
                                            </span>
                                    <i className="material-icons"
                                       style={{position: 'relative', top: 5, cursor: "pointer", float: "right",}}
                                       onClick={this.deleteComment.bind(this, i)}>delete</i>
                                </div>
                                <Divider/>
                            </div>;
                        }).reverse()
                    }
                </Card>
                <textarea
                    placeholder={" Enter Comment"}
                    rows="2"
                    wrap="off"
                    style={styles.textAreaStyle}
                    value={this.state.inputText}
                    onChange={this.handleInputTextChange.bind(this)}
                />
                <br/>
                <FlatButton
                    label={'Add comment'}
                    primary={true}
                    backgroundColor={'rgba(0,0,0,0.7)'}
                    style={{borderRadius: 5, float:'right', color: 'white', height: 40, margin :15}}
                    hoverColor={'rgba(0,0,0,0.4)'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 13}}
                    keyboardFocused={false}
                    onTouchTap={this.addComment.bind(this)}
                />
                <br/>
                <br/>
                <br/>
        </Dialog>;
    }
}
