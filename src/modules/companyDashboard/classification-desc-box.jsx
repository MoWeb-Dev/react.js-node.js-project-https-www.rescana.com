import React from 'react';
import {Card, CardHeader} from 'material-ui/Card';
import {typography} from 'material-ui/styles';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {INFORMATION_BOX_TYPE} from "../../../config/consts.js";
import DataTableViewTemplate from '../common/datatableTemplate/datatable-template.jsx';
import '../../../public/css/customScrollbar.css';
import InfoIcon from 'material-ui/svg-icons/action/perm-device-information';
import ClassIcon from 'material-ui/svg-icons/device/screen-lock-portrait';
import SectorIcon from 'material-ui/svg-icons/image/style';
import DatesIcon from 'material-ui/svg-icons/action/date-range';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';

const shortid = require('shortid');
const moment = require('moment');
const DATE_FORMAT = 'LL';

class ClassificationDescriptionBox extends React.Component {
    constructor() {
        super();

        const wrapBreakStyle = {
            wordBreak: 'break-word',
            whiteSpace: 'pre-wrap'
        };

        this.state = {
            tableColumns: [
                {
                    label: '',
                    key: 'label',
                    style: wrapBreakStyle,
                    noneSearchable: true
                }
            ]
        };
    }

    // Children will contain maximum 5 rows (one for each data type)
    addDataChildren(description, classifications, sectors, dateOfContacts, dateOfSurveys) {
        let children = [];

        let addChildren = (data, subheader, icon) => {
            if (data && Array.isArray(data) && data.length > 0) {
                let allData = data.map((currDesc) => {
                    if (currDesc) {
                        let dataText;

                        if (currDesc.label) {
                            dataText = currDesc.label;
                        } else if (currDesc.startDate && currDesc.endDate) {
                            dataText = moment.utc(currDesc.startDate).format(DATE_FORMAT);
                            if (currDesc.startDate !== currDesc.endDate) {
                                dataText += ' - ' + moment.utc(currDesc.endDate).format(DATE_FORMAT);
                            }
                        } else {
                            dataText = '';
                        }
                        return <ListItem
                            key={shortid.generate()}
                            primaryText={dataText}
                            leftIcon={icon}
                            style={{fontSize: '13px'}}
                            disabled={true}
                        />;
                    }
                });
                if (allData.length > 0) {
                    children.push({label: <List key={shortid.generate()}>
                        <Subheader style={{lineHeight: '28px'}}>{subheader}</Subheader>
                        {allData}
                    </List>});
                }
            }
        };

        addChildren(description, 'Description', <InfoIcon />);

        addChildren(classifications, 'Classifications', <ClassIcon />);

        addChildren(sectors, 'Sectors', <SectorIcon />);

        addChildren(dateOfContacts, 'Date Of Contact', <DatesIcon />);

        addChildren(dateOfSurveys, 'Date Of Last Survey', <DatesIcon />);

        return children;
    }

    render() {
        const {title, description, classifications, sectors, dateOfContacts, dateOfSurveys} = this.props;

        let styles = {
            subStyle: {
                fontSize: 12
            },
            headerTitle: {
                fontSize: 16,
                fontWeight: typography.fontWeightLight,
                color: 'rgb(255, 255, 255)'
            },
            header: {
                padding: '10px'
            },
            cardStyle: {
                background: 'linear-gradient(60deg, #ffa726, #fb8c00)'
            },
            childrenDiv: {
                overflow: 'auto',
                maxHeight: '235px',
                padding: '0px',
                marginTop: '5px'
            }
        };

        let dataToDisplay = this.addDataChildren(description, classifications, sectors, dateOfContacts, dateOfSurveys);

        return (
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <div>
                    <Card style={styles.cardStyle}>
                        <CardHeader
                            titleStyle={styles.headerTitle}
                            subtitleStyle={styles.subStyle}
                            showExpandableButton={false}
                            title={title}
                            style={styles.header}
                        />
                    </Card>
                    <div className="scrollableDiv" style={styles.childrenDiv}>
                        <DataTableViewTemplate
                            myDataType={INFORMATION_BOX_TYPE}
                            tableColumns={this.state.tableColumns}
                            tableData={dataToDisplay}
                            hideHeaderToolbar={true}
                            hideFooterToolbar={true}
                        />
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default ClassificationDescriptionBox;
