import React from 'react';
import {Bar} from 'react-chartjs-2';


class BarChart extends React.Component {
    render() {
        const {labels, data, countDescriber} = this.props;

        const metadata = {
            labels: labels,
            datasets: [
                {
                    label: countDescriber,
                    backgroundColor: 'rgba(30, 136, 229, 1)',
                    borderColor: 'rgba(30, 136, 229, 1)',
                    borderWidth: 0,
                    hoverBackgroundColor: '#039be5',
                    hoverBorderColor: 'rgba(30, 136, 229, 1)',
                    data: data
                }
            ]
        };

        return (
            <Bar
                data={metadata}
                width={355}
                height={270}
                options={{
                    maintainAspectRatio: false
                }}
            />
        );
    }
}

export default BarChart;
