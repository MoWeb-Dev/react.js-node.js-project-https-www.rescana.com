import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import {Card} from 'material-ui/Card';
import DropZone from 'react-dropzone';
import {Caption, Circle} from "react-percentage-circle";
import ReactTooltip from 'react-tooltip';
import Divider from 'material-ui/Divider';
import moment from 'moment';


class VendorAssessmentModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: this.props.open || false,
            assessmentFields: this.props.assessmentFields || {},
            assessmentScore: this.props.assessmentScore || 0,
            notificationAreaComments: this.props.notificationAreaComments || [],
            acceptedFiles: ['image/jpeg', 'image/png', 'image/bmp', 'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/visio', 'application/x-visio', 'application/vnd.visio', 'application/visio.drawing',
                'application/vsd', 'image/x-vsd', 'zz-application/zz-winassoc-vsd',
                'application/vnd.ms-visio.viewer', 'application/pdf',
                /* "application/zip"*/ 'application/octet-stream']
        };
        this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
        this.handleCommentChange = this.handleCommentChange.bind(this);
        this.fillAssessmentCurFieldsState = this.fillAssessmentCurFieldsState.bind(this);
    }

    handleCheckBoxChange(checkBoxName) {
        let assessmentFields = this.state.assessmentFields;
        assessmentFields[checkBoxName].isChecked = !this.state.assessmentFields[checkBoxName].isChecked;
        this.setState({assessmentFields: assessmentFields});
    }

    calculateAssessmentScore(allAssessmentFields, fieldName) {
        let score = 0;
        if (allAssessmentFields.intelAssessment && allAssessmentFields.intelAssessment.isChecked !== null) {
            score += allAssessmentFields.intelAssessment.isChecked ? 10 : 0;
        }
        if (allAssessmentFields.submitQuestionnaire && allAssessmentFields.submitQuestionnaire.isChecked !== null) {
            score += allAssessmentFields.submitQuestionnaire.isChecked ? 10 : 0;
        }
        if (allAssessmentFields.completedQuestionnaire && allAssessmentFields.completedQuestionnaire.isChecked !== null) {
            score += allAssessmentFields.completedQuestionnaire.isChecked ? 10 : 0;
        }
        if (allAssessmentFields.questionnaireAnalysis && allAssessmentFields.questionnaireAnalysis.isChecked !== null) {
            score += allAssessmentFields.questionnaireAnalysis.isChecked ? 40 : 0;
        }
        if (allAssessmentFields.validation && allAssessmentFields.validation.isChecked !== null) {
            score += allAssessmentFields.validation.isChecked ? 15 : 0;
        }
        if (allAssessmentFields.report && allAssessmentFields.report.isChecked !== null) {
            score += allAssessmentFields.report.isChecked ? 15 : 0;
        }
        return score;
    }

    clearAllFields() {
        this.setState({
            assessmentFields: {
                intelAssessment: {isChecked: false, comment: '', attachment: ''},
                submitQuestionnaire: {isChecked: false, comment: '', attachment: ''},
                completedQuestionnaire: {isChecked: false, comment: '', attachment: ''},
                questionnaireAnalysis: {isChecked: false, comment: '', attachment: ''},
                validation: {isChecked: false, comment: '', attachment: ''},
                report: {isChecked: false, comment: '', attachment: ''},
            },
            notificationAreaComments: []
        });
    }

    cancel() {
        this.setState({open: false});
        this.props.close();
        this.clearAllFields();
    }

    save(fieldNameToSave) {
        let allAssessmentFields = this.state.assessmentFields || {};
        const assessmentScore = this.calculateAssessmentScore(allAssessmentFields, fieldNameToSave);
        //in case we update all checkboxs fields
        if (fieldNameToSave && fieldNameToSave === "all") {
            this.saveVendorAssessmentData(allAssessmentFields, fieldNameToSave, assessmentScore);
            //in case we add notification to the notification area
        } else {
            let objFields = allAssessmentFields[fieldNameToSave];
            this.saveAttachmentFile(objFields, fieldNameToSave, assessmentScore, this.state.notificationAreaComments);
        }
    }

    saveAttachmentFile(assessmentFields, fieldNameToSave, assessmentScore, notificationAreaComments) {
        if (assessmentFields && assessmentFields.attachment) {
            // check if we already uploaded this file
            if (assessmentFields.attachment.preview && assessmentFields.attachment instanceof Blob) {
                let data = new FormData();
                data.append('vendorAssessmentFile', assessmentFields.attachment, assessmentFields.attachment.name);
                $.ajax({
                    url: '/api/saveVendorAssessmentFile',
                    dataType: 'json',
                    data: data,
                    enctype: 'multipart/form-data',
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: (res) => {
                        if (res && res.result && res.result[0] && res.result[0].path && res.result[0].scanStatus) {
                            let neededFields = {};
                            neededFields.scanStatus = res.result[0].scanStatus;
                            neededFields.path = res.result[0].path;
                            neededFields.name = assessmentFields.attachment.name;
                            assessmentFields.attachment = neededFields;
                            if (assessmentFields.isChecked !== null && assessmentFields.isChecked) {
                                this.addCommentToNotificationArea(fieldNameToSave, neededFields);
                                this.updateNotificationAreaOnMongo(notificationAreaComments);
                            }
                            app.addAlert('success', 'Vendor Assessment file Updated!');
                        } else {
                            app.addAlert('error', 'Error: failed to save Vendor Assessment file');
                        }
                    }
                });
            } else {
                if (assessmentFields.isChecked !== null && assessmentFields.isChecked) {
                    this.addCommentToNotificationArea(fieldNameToSave, assessmentFields.attachment);
                    this.updateNotificationAreaOnMongo(notificationAreaComments);
                }
                console.log('In saveAttachmentFile() - file have no preview field means the file is already been uploaded')
            }
        } else {
            if (assessmentFields.isChecked !== null && assessmentFields.isChecked) {
                this.addCommentToNotificationArea(fieldNameToSave, {});
                this.updateNotificationAreaOnMongo(notificationAreaComments);
            }
            console.log('In saveAttachmentFile() - no file to save')
        }
    }

    updateNotificationAreaOnMongo(notificationAreaComments) {
        let data = {};
        data.compId = this.props.companyId;
        data.notificationAreaComments = notificationAreaComments;
        if (data && data.compId && data.notificationAreaComments) {
            $.ajax({
                type: 'POST',
                url: '/api/updateAssessmentNotificationArea',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    this.setState(
                        {
                            notificationAreaComments: notificationAreaComments
                        });
                    this.forceUpdate();
                    app.addAlert('success', 'Notification Area Updated!');
                } else {
                    app.addAlert('error', 'Error: failed to update Notification Area');
                }
            }).fail((jqXHR, textStatus, errorThrown) => {
                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        } else {
            app.addAlert('error', 'Error: failed to update Notification Area');

        }
    }

    saveVendorAssessmentData(assessmentFields, fieldNameToSave, assessmentScore) {
        let data = {};
        data.compId = this.props.companyId;
        data.assessmentFields = assessmentFields;
        data.assessmentScore = assessmentScore;

        if (data && data.compId && (data.assessmentScore || data.assessmentScore === 0) && data.assessmentFields) {
            $.ajax({
                type: 'POST',
                url: '/api/saveVendorAssessmentData',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    this.setState({
                        assessmentFields: assessmentFields,
                        assessmentScore: assessmentScore,
                    });
                    this.props.updateAssessmentProgressAndFields(this.state.assessmentFields, this.state.assessmentScore, this.state.notificationAreaComments);
                    app.addAlert('success', 'Vendor Assessment Updated!');
                } else {
                    app.addAlert('error', 'Error: failed to save Vendor Assessment data');
                }
            }).fail((jqXHR, textStatus, errorThrown) => {
                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        } else {
            app.addAlert('error', 'Error: failed to save Vendor Assessment data');
        }
    }

    addCommentToNotificationArea(fieldName, fileFields) {
        let notificationAreaComments = this.state.notificationAreaComments || [];
        let assessmentFields = this.state.assessmentFields || {};
        const curTime = moment().format('YYYY/MM/DD, hh:mm:ss a');
        let commentObj = {};
        let commentStartWith = "";

        if (fieldName === "intelAssessment") {
            commentStartWith = "Intel Assessment";
        } else if (fieldName === "submitQuestionnaire") {
            commentStartWith = "Submit Questionnaire";
        } else if (fieldName === "completedQuestionnaire") {
            commentStartWith = "Completed Questionnaire";
        } else if (fieldName === "questionnaireAnalysis") {
            commentStartWith = "Questionnaire Analysis";
        } else if (fieldName === "validation") {
            commentStartWith = "Validation";
        } else if (fieldName === "report") {
            commentStartWith = "Report";
        }
        commentObj.comment = curTime + " - " + commentStartWith + ": " + assessmentFields[fieldName].comment.toString();
        commentObj.file = fileFields;
        notificationAreaComments.push(commentObj);
        this.setState({notificationAreaComments: notificationAreaComments});
    }

    fillAssessmentCurFieldsState(assessmentFields, notificationAreaComments) {
        if (assessmentFields) {
            this.setState({
                assessmentFields: assessmentFields,
                notificationAreaComments: notificationAreaComments || []
            });
        } else {
            this.clearAllFields()
        }
        this.forceUpdate();
    }

    handleCommentChange(fieldName, e) {
        let assessmentFields = this.state.assessmentFields || {};
        assessmentFields[fieldName].comment = e.target.value;
        this.setState({assessmentFields: assessmentFields});
    };

    deleteAttachment(fieldName) {
        let assessmentFields = this.state.assessmentFields || {};
        assessmentFields[fieldName].attachment = {};
        this.setState({assessmentFields: assessmentFields});
    };

    deleteComment(idx) {
        let notificationAreaComments = this.state.notificationAreaComments || {};
        notificationAreaComments.splice(idx, 1);
        this.setState({notificationAreaComments: notificationAreaComments});
        this.updateNotificationAreaOnMongo(notificationAreaComments);
    };

    handleChangeAttachment(attachmentFile, fieldName) {
        let assessmentFields = this.state.assessmentFields;
        if (attachmentFile && fieldName) {
            assessmentFields[fieldName].attachment = attachmentFile;
            this.setState({assessmentFields: assessmentFields});
        } else {
            app.addAlert('error', 'Error: Failed to upload the file!');
        }
        this.forceUpdate();
    };

    willReceivePropsHelper(nextProps, fieldName) {
        let field = {};
        if (nextProps.assessmentFields[fieldName].isChecked !== null) {
            field.isChecked = nextProps.assessmentFields[fieldName].isChecked;
        } else {
            field.isChecked = false;
        }
        if (nextProps.assessmentFields[fieldName].comment) {
            field.comment = nextProps.assessmentFields[fieldName].comment;
        } else {
            field.comment = "";
        }
        if (nextProps.assessmentFields[fieldName].attachment) {
            field.attachment = nextProps.assessmentFields[fieldName].attachment;
        }
        return field;
    }

    componentWillReceiveProps(nextProps, nextContext) {
        let stateObj = {open: nextProps.open || false};
        if (nextProps.assessmentFields) {
            stateObj.assessmentFields = {};
            if (nextProps.assessmentFields.intelAssessment) {
                stateObj.assessmentFields.intelAssessment = this.willReceivePropsHelper(nextProps, 'intelAssessment');
            } else {
                stateObj.assessmentFields.intelAssessment = {isChecked: false, comment: ""};
            }
            if (nextProps.assessmentFields.submitQuestionnaire) {
                stateObj.assessmentFields.submitQuestionnaire = this.willReceivePropsHelper(nextProps, 'submitQuestionnaire');
            } else {
                stateObj.assessmentFields.submitQuestionnaire = {isChecked: false, comment: ""};
            }
            if (nextProps.assessmentFields.completedQuestionnaire) {
                stateObj.assessmentFields.completedQuestionnaire = this.willReceivePropsHelper(nextProps, 'completedQuestionnaire');
            } else {
                stateObj.assessmentFields.completedQuestionnaire = {isChecked: false, comment: ""};
            }
            if (nextProps.assessmentFields.questionnaireAnalysis) {
                stateObj.assessmentFields.questionnaireAnalysis = this.willReceivePropsHelper(nextProps, 'questionnaireAnalysis');
            } else {
                stateObj.assessmentFields.questionnaireAnalysis = {isChecked: false, comment: ""};
            }
            if (nextProps.assessmentFields.validation) {
                stateObj.assessmentFields.validation = this.willReceivePropsHelper(nextProps, 'validation');
            } else {
                stateObj.assessmentFields.validation = {isChecked: false, comment: ""};
            }
            if (nextProps.assessmentFields.report) {
                stateObj.assessmentFields.report = this.willReceivePropsHelper(nextProps, 'report');
            } else {
                stateObj.assessmentFields.report = {isChecked: false, comment: ""};
            }
        }

        if (nextProps.assessmentScore) {
            stateObj.assessmentScore = nextProps.assessmentScore;
        } else {
            stateObj.assessmentScore = 0;
        }

        if (nextProps.notificationAreaComments) {
            stateObj.notificationAreaComments = nextProps.notificationAreaComments;
        } else {
            stateObj.notificationAreaComments = [];
        }
        this.setState(stateObj);
    }

    onDropRejected() {
        return app.addAlert('error', "File too big, max size is 3MB");
    }

    onAttach(fieldName, attachment) {
        if (attachment && attachment.length !== 1) {
            app.addAlert('error', "You can only select a single file");
        } else {
            this.handleChangeAttachment(attachment[0], fieldName);
        }
    }


    render() {
        const styles = {
            divStyle: {
                grid: '20px / 260px auto 50px 80px 130px',
                display: 'grid'
            },
            bottomDivStyle: {
                grid: '20px / auto 260px',
                display: 'grid'
            },
            cardStyle: {
                padding: '15px',
                margin: 10
            },
            notificationCardStyle: {
                borderRadius: 10,
                margin: 15,
                backgroundColor: '#eeeeee',
                height: 150,
                flexWrap: 'nowrap',
                overflowX: 'auto'
            },
            checkBoxStyle: {
                fontSize: 14
            },
            textAreaStyle: {
                fontSize: 14,
                outline: 'none',
                resize: 'none',
                position: "relative",
                borderBottom: '2px solid #e0e0e0',
                borderTop: '0px',
                borderRight: '0px',
                borderLeft: '0px',
                marginRight: 45,
                top: 2,
                width: 'auto',
                height: 28,
                backgroundColor: '#f5f5f5'
            },
            attachmentStyle: {
                marginRight: 45,
                cursor: "pointer", width: 20, height: 20
            },
            deleteAttachmentStyle: {
                marginRight: 45,
                position: "relative",
                top: -21,
                left: 38,
                cursor: "pointer", width: 20, height: 20
            }
        };
        let scoreDisplayStyle = {
            fontSize: '23px', fontFamily: 'Barlow'
        };

        const path = '/vendorAssessmentFiles';
        const addBtn = 'Add Comment';
        let assessmentFields = this.state.assessmentFields || {};
        let assessmentScore = this.state.assessmentScore;
        let notificationAreaComments = this.state.notificationAreaComments || [];


        return (
            <div>
                <Dialog
                    title={"Vendor Assessment" + (this.props.companyName ? " of " + this.props.companyName : "")}
                    actions={[
                        <FlatButton
                            label="Update All"
                            primary={true}
                            style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                            keyboardFocused={true}
                            backgroundColor={'#0091ea'}
                            hoverColor={'#12a4ff'}
                            rippleColor={'white'}
                            labelStyle={{fontSize: 10}}
                            onTouchTap={this.save.bind(this, "all")}
                        />,
                        <FlatButton
                            label="Close"
                            primary={true}
                            backgroundColor={'#0091ea'}
                            style={{color: 'white', marginBottom: '5px', height: 40}}
                            keyboardFocused={true}
                            hoverColor={'#12a4ff'}
                            rippleColor={'white'}
                            labelStyle={{fontSize: 10}}
                            onTouchTap={this.cancel.bind(this)}
                        />
                    ]}
                    actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                    bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                    autoScrollBodyContent={true}
                    modal={true}
                    open={this.state.open}
                    onRequestClose={this.props.close}
                    repositionOnUpdate={false}
                    contentStyle={{
                        maxWidth: '60%',
                        minHeight: '100%',
                        borderRadius: '7px 7px 7px 7px'
                    }}
                    titleStyle={{
                        fontSize: 18,
                        background: 'rgba(0,0,0,0.7)',
                        color: 'white', textAlign: 'center',
                        borderRadius: '2px 2px 0px 0px',
                        textTransform: 'uppercase',
                    }}>
                    <br/>
                    <div style={{margin: 10}}>Assessment:</div>
                    <div style={styles.cardStyle}>
                        <div style={styles.divStyle}>
                            <Checkbox
                                label="Intelligence Assessment (10%)"
                                labelStyle={styles.checkBoxStyle}
                                checked={assessmentFields.intelAssessment.isChecked || false}
                                onCheck={this.handleCheckBoxChange.bind(this, 'intelAssessment')}
                            />
                            {assessmentFields.intelAssessment.isChecked ?
                                <textarea
                                    placeholder={" Enter Comment"}
                                    rows="2"
                                    wrap="off"
                                    style={styles.textAreaStyle}
                                    value={assessmentFields.intelAssessment.comment}
                                    onChange={this.handleCommentChange.bind(this, 'intelAssessment')}
                                /> : <span> </span>
                            }
                            {assessmentFields.intelAssessment.isChecked ?
                                <DropZone
                                    accept={this.state.acceptedFiles.join(',')}
                                    onDrop={this.onAttach.bind(this, 'intelAssessment')}
                                    style={{width: 20, height: 20}}
                                    disabled={!assessmentFields.intelAssessment.isChecked}
                                    onDropRejected={this.onDropRejected.bind(this)}
                                    maxSize={3000000}>
                                    <i className="material-icons"
                                       style={styles.attachmentStyle}>attach_file</i>
                                </DropZone>
                                : <span> </span>
                            }
                            {assessmentFields.intelAssessment.isChecked &&
                            assessmentFields.intelAssessment.attachment &&
                            assessmentFields.intelAssessment.attachment.name ?
                                <span>
                                <a
                                    style={styles.attachmentStyle}
                                    target="_blank"
                                    href={
                                        assessmentFields.intelAssessment.attachment &&
                                        assessmentFields.intelAssessment.attachment.path ?
                                            path + assessmentFields.intelAssessment.attachment.path : ""}
                                    data-tip={
                                        assessmentFields.intelAssessment.attachment &&
                                        assessmentFields.intelAssessment.attachment.name ?
                                            assessmentFields.intelAssessment.attachment.name.toString() : ""}>File</a>
                                    <i className="material-icons"
                                       onClick={this.deleteAttachment.bind(this, 'intelAssessment')}
                                       style={styles.deleteAttachmentStyle}>delete</i>
                                </span>
                                : <span> </span>
                            }
                            {assessmentFields.intelAssessment.isChecked &&
                            <FlatButton
                                primary={true}
                                style={{borderRadius: 3, height: 30}}
                                backgroundColor={'rgba(0,0,0,0.7)'}
                                hoverColor={'#12a4ff'}
                                rippleColor={'white'}
                                onTouchTap={this.save.bind(this, 'intelAssessment')}>
                                <span style={{fontSize: 10, color: 'white', position: 'relative', bottom: 4}}>{addBtn}</span>
                            </FlatButton>
                            }
                        </div>
                    </div>
                    <div style={styles.cardStyle}>
                        <div style={styles.divStyle}>
                            <Checkbox
                                label="Submitting Questionnaire (10%)"
                                labelStyle={styles.checkBoxStyle}
                                checked={assessmentFields.submitQuestionnaire.isChecked}
                                onCheck={this.handleCheckBoxChange.bind(this, 'submitQuestionnaire')}
                            />
                            {assessmentFields.submitQuestionnaire.isChecked ?
                                <textarea
                                    placeholder={"Enter Comment"}
                                    rows="2"
                                    wrap="off"
                                    style={styles.textAreaStyle}
                                    value={assessmentFields.submitQuestionnaire.comment}
                                    onChange={this.handleCommentChange.bind(this, 'submitQuestionnaire')}
                                />
                                : <span> </span>
                            }
                            {assessmentFields.submitQuestionnaire.isChecked ?
                                <DropZone
                                    accept={this.state.acceptedFiles.join(',')}
                                    onDrop={this.onAttach.bind(this, 'submitQuestionnaire')}
                                    style={{width: 20, height: 20}}
                                    onDropRejected={this.onDropRejected.bind(this)}
                                    maxSize={3000000}>
                                    <i className="material-icons"
                                       style={styles.attachmentStyle}>attach_file</i>
                                </DropZone>
                                : <span> </span>
                            }
                            {assessmentFields.submitQuestionnaire.isChecked &&
                            assessmentFields.submitQuestionnaire.attachment &&
                            assessmentFields.submitQuestionnaire.attachment.name ?
                                <span>
                                <a style={styles.attachmentStyle}
                                   target="_blank"
                                   href={
                                       assessmentFields.submitQuestionnaire.attachment &&
                                       assessmentFields.submitQuestionnaire.attachment.path ?
                                           path + assessmentFields.submitQuestionnaire.attachment.path : ""}
                                   data-tip={assessmentFields.submitQuestionnaire.attachment &&
                                   assessmentFields.submitQuestionnaire.attachment.name ?
                                       assessmentFields.submitQuestionnaire.attachment.name.toString() : ""}>
                                    File
                                </a>
                                <i className="material-icons"
                                   onClick={this.deleteAttachment.bind(this, 'submitQuestionnaire')}
                                   style={styles.deleteAttachmentStyle}>delete</i>
                                </span>
                                : <span> </span>
                            }
                            {assessmentFields.submitQuestionnaire.isChecked &&
                            <FlatButton
                                primary={true}
                                style={{borderRadius: 3, height: 30}}
                                backgroundColor={'rgba(0,0,0,0.7)'}
                                hoverColor={'#12a4ff'}
                                rippleColor={'white'}
                                onTouchTap={this.save.bind(this, 'submitQuestionnaire')}>
                                <span style={{fontSize: 10, color: 'white', position: 'relative', bottom: 4}}>{addBtn}</span>
                            </FlatButton>
                            }
                        </div>
                    </div>
                    <div style={styles.cardStyle}>
                        <div style={styles.divStyle}>
                            <Checkbox
                                label="Completed Questionnaire (10%)"
                                labelStyle={styles.checkBoxStyle}
                                checked={assessmentFields.completedQuestionnaire.isChecked || false}
                                onCheck={this.handleCheckBoxChange.bind(this, 'completedQuestionnaire')}
                            />
                            {assessmentFields.completedQuestionnaire.isChecked ?
                                <textarea
                                    placeholder={" Enter Comment"}
                                    rows="2"
                                    wrap="off"
                                    style={styles.textAreaStyle}
                                    value={assessmentFields.completedQuestionnaire.comment}
                                    onChange={this.handleCommentChange.bind(this, 'completedQuestionnaire')}
                                />
                                : <span> </span>
                            }
                            {assessmentFields.completedQuestionnaire.isChecked ?
                                <DropZone
                                    accept={this.state.acceptedFiles.join(',')}
                                    onDrop={this.onAttach.bind(this, 'completedQuestionnaire')}
                                    style={{width: 20, height: 20}}
                                    onDropRejected={this.onDropRejected.bind(this)}
                                    maxSize={3000000}>
                                    <i className="material-icons"
                                       style={styles.attachmentStyle}>attach_file</i>
                                </DropZone>
                                : <span> </span>
                            }
                            {assessmentFields.completedQuestionnaire.isChecked &&
                            assessmentFields.completedQuestionnaire.attachment &&
                            assessmentFields.completedQuestionnaire.attachment.name ?
                                <span>
                                <a style={styles.attachmentStyle}
                                   target="_blank"
                                   href={
                                       assessmentFields.completedQuestionnaire.attachment &&
                                       assessmentFields.completedQuestionnaire.attachment.path ?
                                           path + assessmentFields.completedQuestionnaire.attachment.path : ""}
                                   data-tip={assessmentFields.completedQuestionnaire.attachment &&
                                   assessmentFields.completedQuestionnaire.attachment.name ?
                                       assessmentFields.completedQuestionnaire.attachment.name.toString() : ""}>
                                    File
                                </a>
                                <i className="material-icons"
                                   onClick={this.deleteAttachment.bind(this, 'completedQuestionnaire')}
                                   style={styles.deleteAttachmentStyle}>delete</i>
                                </span>
                                : <span> </span>
                            }
                            {assessmentFields.completedQuestionnaire.isChecked &&
                            <FlatButton
                                primary={true}
                                style={{borderRadius: 3, height: 30}}
                                backgroundColor={'rgba(0,0,0,0.7)'}
                                hoverColor={'#12a4ff'}
                                rippleColor={'white'}
                                onTouchTap={this.save.bind(this, 'completedQuestionnaire')}>
                                <span style={{fontSize: 10, color: 'white', position: 'relative', bottom: 4}}>{addBtn}</span>
                            </FlatButton>
                            }
                        </div>
                    </div>
                    <div style={styles.cardStyle}>
                        <div style={styles.divStyle}>
                            <Checkbox
                                label="Questionnaire Analysis (40%)"
                                labelStyle={styles.checkBoxStyle}
                                checked={assessmentFields.questionnaireAnalysis.isChecked || false}
                                onCheck={this.handleCheckBoxChange.bind(this, 'questionnaireAnalysis')}
                            />
                            {assessmentFields.questionnaireAnalysis.isChecked ?
                                <textarea
                                    placeholder={" Enter Comment"}
                                    rows="2"
                                    wrap="off"
                                    style={styles.textAreaStyle}
                                    value={assessmentFields.questionnaireAnalysis.comment}
                                    onChange={this.handleCommentChange.bind(this, 'questionnaireAnalysis')}
                                />
                                : <span> </span>
                            }
                            {assessmentFields.questionnaireAnalysis.isChecked ?
                                <DropZone
                                    accept={this.state.acceptedFiles.join(',')}
                                    onDrop={this.onAttach.bind(this, 'questionnaireAnalysis')}
                                    style={{width: 20, height: 20}}
                                    onDropRejected={this.onDropRejected.bind(this)}
                                    maxSize={3000000}>
                                    <i className="material-icons"
                                       style={styles.attachmentStyle}>attach_file</i>
                                </DropZone>
                                : <span> </span>
                            }
                            {assessmentFields.questionnaireAnalysis.isChecked &&
                            assessmentFields.questionnaireAnalysis.attachment &&
                            assessmentFields.questionnaireAnalysis.attachment.name ?
                                <span>
                                <a style={styles.attachmentStyle}
                                   target="_blank"
                                   href={
                                       assessmentFields.questionnaireAnalysis.attachment &&
                                       assessmentFields.questionnaireAnalysis.attachment.path ?
                                           path + assessmentFields.questionnaireAnalysis.attachment.path : ""}
                                   data-tip={assessmentFields.questionnaireAnalysis.attachment &&
                                   assessmentFields.questionnaireAnalysis.attachment.name ?
                                       assessmentFields.questionnaireAnalysis.attachment.name.toString() : ""}>
                                    File
                                </a>
                                <i className="material-icons"
                                   onClick={this.deleteAttachment.bind(this, 'questionnaireAnalysis')}
                                   style={styles.deleteAttachmentStyle}>delete</i>
                                </span>
                                : <span> </span>
                            }
                            {assessmentFields.questionnaireAnalysis.isChecked &&
                            <FlatButton
                                primary={true}
                                style={{borderRadius: 3, height: 30}}
                                backgroundColor={'rgba(0,0,0,0.7)'}
                                hoverColor={'#12a4ff'}
                                rippleColor={'white'}
                                onTouchTap={this.save.bind(this, 'questionnaireAnalysis')}>
                                <span style={{fontSize: 10, color: 'white', position: 'relative', bottom: 4}}>{addBtn}</span>
                            </FlatButton>
                            }
                        </div>
                    </div>
                    <div style={styles.cardStyle}>
                        <div style={styles.divStyle}>
                            <Checkbox
                                label="Validation (15%)"
                                labelStyle={styles.checkBoxStyle}
                                checked={assessmentFields.validation.isChecked || false}
                                onCheck={this.handleCheckBoxChange.bind(this, 'validation')}
                            />
                            {assessmentFields.validation.isChecked ?
                                <textarea
                                    placeholder={" Enter Comment"}
                                    rows="2"
                                    wrap="off"
                                    style={styles.textAreaStyle}
                                    value={assessmentFields.validation.comment}
                                    onChange={this.handleCommentChange.bind(this, 'validation')}
                                />
                                : <span> </span>
                            }
                            {assessmentFields.validation.isChecked ?
                                <DropZone
                                    accept={this.state.acceptedFiles.join(',')}
                                    onDrop={this.onAttach.bind(this, 'validation')}
                                    style={{width: 20, height: 20}}
                                    onDropRejected={this.onDropRejected.bind(this)}
                                    maxSize={3000000}>
                                    <i className="material-icons"
                                       style={styles.attachmentStyle}>attach_file</i>
                                </DropZone>
                                : <span> </span>
                            }
                            {assessmentFields.validation.isChecked &&
                            assessmentFields.validation.attachment &&
                            assessmentFields.validation.attachment.name ?
                                <span>
                                <a style={styles.attachmentStyle}
                                   target="_blank"
                                   href={
                                       assessmentFields.validation.attachment &&
                                       assessmentFields.validation.attachment.path ?
                                           path + assessmentFields.validation.attachment.path : ""}
                                   data-tip={assessmentFields.validation.attachment &&
                                   assessmentFields.validation.attachment.name ?
                                       assessmentFields.validation.attachment.name.toString() : ""}>
                                    File
                                </a>
                                <i className="material-icons"
                                   onClick={this.deleteAttachment.bind(this, 'validation')}
                                   style={styles.deleteAttachmentStyle}>delete</i>
                                </span>
                                : <span> </span>
                            }
                            {assessmentFields.validation.isChecked &&
                            <FlatButton
                                primary={true}
                                style={{borderRadius: 3, height: 30}}
                                backgroundColor={'rgba(0,0,0,0.7)'}
                                hoverColor={'#12a4ff'}
                                rippleColor={'white'}
                                onTouchTap={this.save.bind(this, 'validation')}>
                                <span style={{fontSize: 10, color: 'white', position: 'relative', bottom: 4}}>{addBtn}</span>
                            </FlatButton>
                            }
                        </div>
                    </div>
                    <div style={styles.cardStyle}>
                        <div style={styles.divStyle}>
                            <Checkbox
                                label="Report (15%)"
                                labelStyle={styles.checkBoxStyle}
                                checked={assessmentFields.report.isChecked || false}
                                onCheck={this.handleCheckBoxChange.bind(this, 'report')}
                            />
                            {assessmentFields.report.isChecked ?
                                <textarea
                                    placeholder={" Enter Comment"}
                                    rows="2"
                                    wrap="off"
                                    style={styles.textAreaStyle}
                                    value={assessmentFields.report.comment}
                                    onChange={this.handleCommentChange.bind(this, 'report')}
                                />
                                : <span> </span>
                            }
                            {assessmentFields.report.isChecked ?
                                <DropZone
                                    accept={this.state.acceptedFiles.join(',')}
                                    onDrop={this.onAttach.bind(this, 'report')}
                                    style={{width: 20, height: 20}}
                                    onDropRejected={this.onDropRejected.bind(this)}
                                    maxSize={3000000}>
                                    <i className="material-icons"
                                       style={styles.attachmentStyle}>attach_file</i>
                                </DropZone>
                                : <span> </span>
                            }
                            {assessmentFields.report.isChecked &&
                            assessmentFields.report.attachment &&
                            assessmentFields.report.attachment.name ?
                                <span>
                                <a style={styles.attachmentStyle}
                                   target="_blank"
                                   href={
                                       assessmentFields.report.attachment &&
                                       assessmentFields.report.attachment.path ?
                                           path + assessmentFields.report.attachment.path : ""}
                                   data-tip={assessmentFields.report.attachment &&
                                   assessmentFields.report.attachment.name ?
                                       assessmentFields.report.attachment.name.toString() : ""}>
                                    File
                                </a>
                                <i className="material-icons"
                                   onClick={this.deleteAttachment.bind(this, 'report')}
                                   style={styles.deleteAttachmentStyle}>delete</i>
                                </span>
                                : <span> </span>
                            }
                            {assessmentFields.report.isChecked &&
                            <FlatButton
                                primary={true}
                                style={{borderRadius: 3, height: 30}}
                                backgroundColor={'rgba(0,0,0,0.7)'}
                                hoverColor={'#12a4ff'}
                                rippleColor={'white'}
                                onTouchTap={this.save.bind(this, 'report')}>
                                <span style={{fontSize: 10, color: 'white', position: 'relative', bottom: 4}}>{addBtn}</span>
                            </FlatButton>
                            }
                        </div>
                    </div>
                    <br/>
                    <div style={{margin: 10}}>Notification Area:</div>
                    <div style={styles.bottomDivStyle}>
                        <Card style={styles.notificationCardStyle}>
                            {
                                notificationAreaComments.map((item, i) => {
                                    return <div key={i}>
                                        <div style={{grid: 'auto 10px / auto 50px', display: 'grid', padding: 5}}>
                                            <span style={{padding: '0px 15px 0px 15px', position: "relative", top: 5}}>
                                                <span
                                                    style={{fontSize: 13}}>{notificationAreaComments[i].comment ? notificationAreaComments[i].comment : null}</span>
                                                <span style={{fontSize: 13, marginRight: 2}}>
                                                    {notificationAreaComments[i].file && notificationAreaComments[i].file.path ? ", " : ""}
                                                </span>
                                                <a href={notificationAreaComments[i].file && notificationAreaComments[i].file.path ?
                                                    path + notificationAreaComments[i].file.path : null}
                                                   data-tip={notificationAreaComments[i].file &&
                                                   notificationAreaComments[i].file.path && notificationAreaComments[i].file.name ?
                                                       notificationAreaComments[i].file.name.toString() : ""}
                                                   target="_blank"
                                                   style={{cursor: "pointer", fontSize: 13}}>
                                                    {notificationAreaComments[i].file && notificationAreaComments[i].file.path ? "File" : ""}</a>
                                            </span>
                                            <i className="material-icons"
                                               style={{position: 'relative', top: 5, cursor: "pointer", float: "right",}}
                                               onClick={this.deleteComment.bind(this, i)}>delete</i>
                                        </div>
                                        <Divider/>
                                    </div>;
                                }).reverse()
                            }
                        </Card>
                        <span style={{position: "relative", bottom: 40, width: 'auto', margin: 60}}>
                            <Circle percent={assessmentScore ? assessmentScore : 0} strokeWidth="12"
                                    strokeColor={"rgba(0,0,0,0.7)"}>
                                <Caption text={(assessmentScore ? assessmentScore.toString() : "0") + "%"} x="50" y="58"
                                         textAnchor="middle"
                                         className='caption-text'
                                         style={scoreDisplayStyle}/>
                            </Circle>
                        </span>
                    </div>
                    <ReactTooltip place="bottom"/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </Dialog>
            </div>
        );
    }
}

export default VendorAssessmentModal;
