import React from 'react';
import {Card, CardHeader} from 'material-ui/Card';
import {grey800} from 'material-ui/styles/colors';
import {typography} from 'material-ui/styles';
import BarChart from './bar-chart.jsx';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../app/themes/themeForFont";


class IntelBox extends React.Component {
    render() {
        const {cveScore, dnsScore, bucketsScore, emailsScore, dataleaksScore, blacklistsScore, botnetsScore, sslCertsScore} = this.props;

        let styles = {
            cardStyle: {
                boxShadow: '',
            },
            content: {
                padding: '0px 10px 10px'
            },
            data: {
                height: 295,
                overflowY: 'auto',
                fontSize: 12,
                fontWeight: typography.fontWeightLight,
                color: grey800
            },
            header: {
                fontSize: 16,
                fontWeight: typography.fontWeightLight
            },
            questions: {
                padding: '0px 25px 5px'
            }
        };

        const labels = ['CVE', 'DNS', 'Buckets', 'Emails', 'Dataleaks', 'Blacklists', 'Botnets', 'Https'];

        // The last 0 is necessary to fix the chart's ratio. (and will not be displayed).
        const data = [cveScore || 0, dnsScore || 0, bucketsScore || 0, emailsScore || 0, dataleaksScore || 0, blacklistsScore || 0, botnetsScore || 0, sslCertsScore || 0, 0];

        const countDescriber = 'Score';

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <Card style={styles.cardStyle}>
                    <CardHeader
                        titleStyle={styles.header}
                        title="INTEL FINDINGS"/>
                    <div style={styles.content}>
                        <div style={styles.data}>
                            <BarChart
                                labels={labels}
                                data={data}
                                countDescriber={countDescriber}
                            />
                        </div>
                    </div>
                    <div style={{marginBottom: '35px'}}>
                    </div>
                </Card>
            </MuiThemeProvider>
        );
    }
}

export default IntelBox;
