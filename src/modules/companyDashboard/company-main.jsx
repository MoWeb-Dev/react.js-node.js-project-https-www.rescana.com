import React from 'react';
import classNames from 'classnames';
import {browserHistory} from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {typography} from 'material-ui/styles';
import {grey600} from 'material-ui/styles/colors';
import {Row, Col, ClearFix} from 'react-grid-system';
import CvePerHostname from '../dashboard/cvePerHostname.jsx';
import CveSeverity from '../dashboard/cveSeverity.jsx';
import ScoreBox from '../dashboard/score-box.jsx';
import AssetMap from '../dashboard/assetMap.jsx';
import {groupLatLan, chooseColor} from '../common/CommonHelpers.js';
import IntelBox from './intel-box.jsx';
import SurveyBox from './survey-box.jsx';
import BreadCrumbs from '../common/breadCrumbs.jsx';
import {SCORE_TYPES, PROJECT_ISSELFASSESSMENT, ORGANIZATION_ID} from '../../../config/consts';
import MiniScoreBox from './mini-score-box.jsx';
import ContactsBox from './contacts-box.jsx';
import CompanyInformationBox from './information-box.jsx';
import ClassificationDescriptionBox from './classification-desc-box.jsx';
import EditIcon from 'material-ui/svg-icons/content/create';
import AssessmentIcon from 'material-ui/svg-icons/action/assessment';
import EditCompanyForm from './edit-company.jsx';
import themeForFont from '../app/themes/themeForFont';
import IconButton from 'material-ui/IconButton';
import VendorAssessmentModal from './vendor-assessment-modal.jsx';
import LinearProgress from 'material-ui/LinearProgress';


const shortid = require('shortid');

const IntelKeys = ['countCVEs',
    'countDNS',
    'countBuckets',
    'countEmailBreaches',
    'countDataleaks',
    'countBlacklists',
    'countBotnets',
    'countSSLCerts'];

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class CompanyDashboard extends React.Component {
    constructor() {
        super();
        this.state = {
            companyData: {},
            cvePerHostname: [],
            companyCVESeverity: {},
            surveyData: undefined,
            intelCounts: undefined,
            intelScoreRatios: undefined,
            intelScore: undefined,
            openEditCompanyModal: false,
            openVendorAssessmentModal: false,
            assessmentFields: {
                intelAssessment: {isChecked: false, comment: ''},
                submitQuestionnaire: {isChecked: false, comment: ''},
                completedQuestionnaire: {isChecked: false, comment: ''},
                questionnaireAnalysis: {isChecked: false, comment: ''},
                validation: {isChecked: false, comment: ''},
                report: {isChecked: false, comment: ''}
            },
            assessmentScore: 0,
            notificationAreaComments: []
        };

        this.getMaxCVSScore = this.getMaxCVSScore.bind(this);
        this.getCVESeverityData = this.getCVESeverityData.bind(this);
        this.getCVECountPerHostnameData = this.getCVECountPerHostnameData.bind(this);
        this.getAllCompanyAssetsLocation = this.getAllCompanyAssetsLocation.bind(this);
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            this.setState({user: user}, () => {
                if (window && window.location && window.location.pathname) {
                    let pathNameRoutes = window.location.pathname.split('/');
                    let params = pathNameRoutes.pop();
                    if (pathNameRoutes && pathNameRoutes.pop() === 'company-dashboard') {
                        // params is companyID.
                        this.getMaxCVSScore(params, user.userRole);
                        this.getCVESeverityData(params);
                        this.getCVECountPerHostnameData(params);
                        this.getAllCompanyAssetsLocation(params);
                    }
                }
            });
        }
    }

    getMaxCVSScore(companyID, userRole) {
        let orgId = localStorage.getItem(ORGANIZATION_ID);
        if (companyID) {
            $.ajax({
                type: 'POST',
                url: '/api/getMaxCVSScore',
                data: JSON.stringify({companyID: companyID, orgId: orgId}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data) {
                    let intelCounts = {};
                    let keys = IntelKeys;
                    let scoresData = data.scoresData || {};

                    for (let i = 0; i < keys.length; i++) {
                        if (scoresData.hasOwnProperty(keys[i])) {
                            intelCounts[keys[i]] = scoresData[keys[i]];
                        } else {
                            intelCounts[keys[i]] = 0;
                        }
                    }
                    let surveyData = undefined;

                    if (scoresData.surveyData && scoresData.surveyData.surveyScore != null && !scoresData.surveyData.isEmpty) {
                        surveyData = scoresData.surveyData;
                        data.isSurveyNA = false;
                    }
                    let intelScoreRatios = undefined;

                    if (data.intelScoreRatios) {
                        intelScoreRatios = data.intelScoreRatios;
                    }
                    let intelScore = undefined;
                    if (scoresData.hasOwnProperty('intelScore')) {
                        intelScore = scoresData.intelScore;
                    } else if (data.maxIntelScore != null) {
                        intelScore = data.maxIntelScore;
                    }

                    let assessmentFields = undefined;
                    if (data.assessmentFields) {
                        assessmentFields = data.assessmentFields;
                    }

                    let assessmentScore = undefined;
                    if (data.assessmentScore) {
                        assessmentScore = data.assessmentScore;
                    }

                    let notificationAreaComments = undefined;
                    if (data.notificationAreaComments) {
                        notificationAreaComments = data.notificationAreaComments;
                    }

                    let isBasic = !!(userRole && userRole === 'basic');

                    this.setState({
                        companyData: data,
                        intelCounts: intelCounts,
                        intelScoreRatios: intelScoreRatios,
                        intelScore: intelScore,
                        surveyData: surveyData,
                        isBasic: isBasic,
                        assessmentFields: assessmentFields,
                        assessmentScore: assessmentScore,
                        notificationAreaComments: notificationAreaComments
                    });
                }
            }).fail((jqXHR, textStatus, errorThrown) => {
                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        }
    }

    getCVESeverityData(companyID) {
        if (companyID) {
            $.ajax({
                type: 'POST',
                url: '/api/getCVESeverity',
                data: JSON.stringify({companyID: companyID}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data) {
                    this.setState({
                        companyCVESeverity: {
                            criticalCount: data.critical,
                            highCount: data.high,
                            mediumCount: data.medium,
                            lowCount: data.low
                        }
                    });
                } else {
                    app.addAlert('error', 'Error: failed to load CVE Severity data');
                }
            }).fail((jqXHR, textStatus, errorThrown) => {
                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        }
    }

    getCVECountPerHostnameData(companyID) {
        if (companyID) {
            $.ajax({
                type: 'POST',
                url: '/api/getCVECountPerHostname',
                data: JSON.stringify({companyID: companyID}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (!data) {
                    data = 0;
                }
                this.setState({
                    cvePerHostname: data
                });
            }).fail((jqXHR, textStatus, errorThrown) => {
                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        }
    }

    getAllCompanyAssetsLocation(companyID) {
        if (companyID) {
            $.ajax({
                type: 'POST',
                url: '/api/getLocations',
                data: JSON.stringify({companyID: companyID}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((locations) => {
                let latLan = groupLatLan(locations);

                this.setState({
                    locations: latLan
                });
            }).fail((jqXHR, textStatus, errorThrown) => {
                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        }
    }

    handleSurveyAnswerLink(said) {
        if (said) {
            browserHistory.push('/survey?said=' + said);
        }
    }

    openEditCompanyInfo(e) {
        e.preventDefault();
        this.setState({
            anchorEl: e.currentTarget,
            openEditCompanyModal: true
        });
    }

    openVendorAssessmentModal(e) {
        if (e) {
            e.preventDefault();
        }
        this.setState({
            anchorEl: e.currentTarget,
            openVendorAssessmentModal: true
        });
        this.refs.vendorAssessment.fillAssessmentCurFieldsState(this.state.assessmentFields, this.state.notificationAreaComments);
    }

    closeVendorAssessmentModal(e) {
        if (e) {
            e.preventDefault();
        }
        this.setState({
            openVendorAssessmentModal: false
        });
    }

    updateAssessmentProgressAndFields(assessmentFields, assessmentScore, notificationAreaComments) {
        this.setState({
            assessmentFields: assessmentFields || {},
            assessmentScore: assessmentScore || 0,
            notificationAreaComments: notificationAreaComments || []
        });
    }


    getEditedCompanyInfo(companyID) {
        if (companyID) {
            let user = app.getAuthUser();

            if (!user) {
                app.routeAuthUser();
            } else {
                this.getMaxCVSScore(companyID, user.userRole);
                this.closeEditForm();
            }
        }
    }

    closeEditForm() {
        if (this.state.openEditCompanyModal) {
            this.setState({openEditCompanyModal: false});
        }
    }

    updateInfoCheck(nid, checked) {
        if (nid && this.state.companyData
            && this.state.companyData.id
            && this.state.companyData.extraData
            && this.state.companyData.extraData.informations) {
            // Change checked value on server (only if allowedEdit).
            $.ajax({
                type: 'POST',
                url: '/api/updateCompanyInfoStatus',
                data: JSON.stringify({companyID: this.state.companyData.id, nid: nid, checked: checked}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((result) => {
                if (result && result.ok) {
                    let stateToUpdate = {
                        companyData: this.state.companyData
                    };

                    let matchingCompany = stateToUpdate.companyData.extraData.informations.find((cmp) => {
                        return cmp.nid === nid;
                    });

                    if (matchingCompany) {
                        matchingCompany.checked = checked;
                        this.setState(stateToUpdate, () => {
                            app.addAlert('success', 'Condition Updated Successfully');
                        });
                    }
                } else {
                    app.addAlert('error', 'Failed to Update Condition');
                }
            }).fail((jqXHR, textStatus, errorThrown) => {
                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        }
    }

    render() {
        const style = {
            row: {
                marginTop: '30px',
                padding: '10px',
                height: '276px'
            },
            scoresRow: {
                margin: '20px -5px 30px',
                height: 'auto'
            },
            contactsRow: {
                margin: '30px -5px 30px',
                height: 'auto'
            },
            companyRow: {
                margin: '30px -5px'
            },
            locationRow: {
                margin: '30px -5px'
            },
            navigation: {
                fontSize: 15,
                fontWeight: typography.fontWeightLight,
                color: grey600,
                paddingBottom: 15,
                display: 'block'
            },
            columnBox: {
                boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px',
                backgroundColor: 'rgb(255, 255, 255)',
                padding: '20px 20px 0px 0px'
            },
            halfDiv: {
                display: 'inline-block',
                verticalAlign: 'top',
                width: '50%'
            },
            surveyHalfDiv: {
                display: 'inline-block',
                verticalAlign: 'top',
                width: '60%'
            },
            scoreHalfDiv: {
                display: 'inline-block',
                verticalAlign: 'top',
                width: '40%'
            },
            halfDivTopSpace: {
                display: 'inline-block',
                verticalAlign: 'top',
                width: '50%',
                marginTop: '30px'
            },
            centeredDiv: {
                textAlign: 'center'
            },
            minTopSpaceCell: {
                marginTop: '15px'
            },
            topSpaceCell: {
                marginTop: '0px'
            },
            editButtonStyle: {
                cursor: 'pointer',
                position: 'absolute',
                right: '7px',
                top: '64px',
                margin: '16px 25px 0px',
                lineHeight: '25px',
                borderRadius: '50px'
            },
            assessmentButtonStyle: {
                cursor: 'pointer',
                position: 'absolute',
                right: '50px',
                top: '64px',
                margin: '16px 25px 0px',
                lineHeight: '25px',
                borderRadius: '50px'
            },
            breadCrumbsDiv: {
                display: 'inline-flex'
            },
            assessmentProgressStyle: {
                position: 'absolute',
                right: '110px',
                top: '64px',
                margin: '16px 25px 0px',
                lineHeight: '25px'
            }
        };

        let user = app.getAuthUser();
        let gaugeDisplayEnabled = user && user.userPreferencesConfig && user.userPreferencesConfig.gaugeDisplay;
        let isVendorAssessmentEnabled = user && (user.isVendorAssessmentEnabled || user.userRole === 'admin');
        let assessmentScore = this.state.assessmentScore;
        let riskAssessment = <span> </span>;
        riskAssessment = <div style={style.assessmentProgressStyle}>
            <span style={{position: 'relative', top: '-1.5px', fontSize: 13}}>
                <span>Assessment Progress: {assessmentScore || 0}%</span>
                <LinearProgress mode="determinate" value={assessmentScore}/>
            </span>
        </div>;

        const containerStyle = classNames({
            'containerLTR': true
        });

        let displayScoreBackwards = app.shouldDisplayScoreBackwards(this.state.user);

        const miniScoreBoxColSM = 3 || 12 / IntelKeys.length;

        let isIntelScoresLoaded = !!(this.state.intelCounts && this.state.intelScoreRatios);

        let cveScore = (isIntelScoresLoaded) ? this.state.intelCounts[IntelKeys[0]].score : 0;
        let dnsScore = (isIntelScoresLoaded) ? this.state.intelCounts[IntelKeys[1]].score : 0;
        let bucketsScore = (isIntelScoresLoaded) ? this.state.intelCounts[IntelKeys[2]].score : 0;
        let emailsScore = (isIntelScoresLoaded) ? this.state.intelCounts[IntelKeys[3]].score : 0;
        let dataleaksScore = (isIntelScoresLoaded) ? this.state.intelCounts[IntelKeys[4]].score : 0;
        let blacklistsScore = (isIntelScoresLoaded) ? this.state.intelCounts[IntelKeys[5]].score : 0;
        let botnetsScore = (isIntelScoresLoaded) ? this.state.intelCounts[IntelKeys[6]].score : 0;
        let sslCertsScore = (isIntelScoresLoaded) ? this.state.intelCounts[IntelKeys[7]].score : 0;
        let cveScoreColor = chooseColor(cveScore);
        let dnsScoreColor = chooseColor(dnsScore);
        let bucketsScoreColor = chooseColor(bucketsScore);
        let emailsScoreColor = chooseColor(emailsScore);
        let dataleaksScoreColor = chooseColor(dataleaksScore);
        let blacklistsScoreColor = chooseColor(blacklistsScore);
        let botnetsScoreColor = chooseColor(botnetsScore);
        let sslCertsScoreColor = chooseColor(sslCertsScore);

        if (displayScoreBackwards) {
            // scoreType is COMPANY because 100-score is required here.
            cveScore = app.switchScoreBackwards(cveScore, SCORE_TYPES.COMPANY);
            dnsScore = app.switchScoreBackwards(dnsScore, SCORE_TYPES.COMPANY);
            bucketsScore = app.switchScoreBackwards(bucketsScore, SCORE_TYPES.COMPANY);
            emailsScore = app.switchScoreBackwards(emailsScore, SCORE_TYPES.COMPANY);
            dataleaksScore = app.switchScoreBackwards(dataleaksScore, SCORE_TYPES.COMPANY);
            blacklistsScore = app.switchScoreBackwards(blacklistsScore, SCORE_TYPES.COMPANY);
            botnetsScore = app.switchScoreBackwards(botnetsScore, SCORE_TYPES.COMPANY);
            sslCertsScore = app.switchScoreBackwards(sslCertsScore, SCORE_TYPES.COMPANY);
        }
        let company = this.state.companyData;
        const orgId = localStorage.getItem(ORGANIZATION_ID);
        let isUnsafeCompany = false;
        if (orgId && orgId !== 'No Id' && company && company.dataByOrg && typeof company.dataByOrg === 'object') {
            Object.keys(company.dataByOrg).map((key) => {
                if (key === orgId && company.dataByOrg[key].hasOwnProperty('isUnsafeCompany')) {
                    isUnsafeCompany = company.dataByOrg[key].isUnsafeCompany;
                }
            });
        }

        let isSelfAssessmentProject = localStorage.getItem(PROJECT_ISSELFASSESSMENT);
        return (
            <div className={containerStyle}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <div style={{margin: '22px', fontSize: '30px'}}>
                        <div style={style.breadCrumbsDiv}>
                            {isSelfAssessmentProject === 'true with one company' ?
                                <div style={{width: 'fit-content', margin: '25px 5px 7px 12px'}}>
                                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                                    <span style={{position: 'relative', top: '-6px', fontSize: 18}}>{[this.state.companyData.companyName]} Self Assessment</span>
                                </div> :
                                <BreadCrumbs
                                    pathArr={[this.state.companyData.companyName]}
                                    show={true}/>
                            }
                            {isVendorAssessmentEnabled ? (() => {
                                return <span>
                                    <IconButton
                                        tooltip='Vendor Assessment'
                                        tooltipPosition="bottom-left"
                                        name="vendorAssessment"
                                        style={style.assessmentButtonStyle}
                                        onTouchTap={this.openVendorAssessmentModal.bind(this)}>
                                        <AssessmentIcon/>
                                    </IconButton>
                                    {riskAssessment}
                                    <VendorAssessmentModal
                                        companyId={this.state.companyData.id}
                                        companyName={this.state.companyData.companyName}
                                        open={this.state.openVendorAssessmentModal}
                                        close={this.closeVendorAssessmentModal.bind(this)}
                                        updateAssessmentProgressAndFields={this.updateAssessmentProgressAndFields.bind(this)}
                                        assessmentFields={this.state.assessmentFields}
                                        assessmentScore={this.state.assessmentScore}
                                        notificationAreaComments={this.state.notificationAreaComments}
                                        ref={'vendorAssessment'}
                                    />
                                </span>;
                            })() : null}
                            {(this.state.companyData && this.state.companyData.edit) ? (() => {
                                return <div>
                                    <IconButton
                                        tooltip='Edit Company Basic Information'
                                        tooltipPosition="bottom-left"
                                        name="editCompanyBasicInfoButton"
                                        style={style.editButtonStyle}
                                        onTouchTap={this.openEditCompanyInfo.bind(this)}
                                    >
                                        <EditIcon/>
                                    </IconButton>
                                    <EditCompanyForm
                                        open={this.state.openEditCompanyModal}
                                        companyFields={this.state.companyData}
                                        closeForm={this.closeEditForm.bind(this)}
                                        editUserCompany={this.getEditedCompanyInfo.bind(this)}
                                    />
                                </div>;
                            })() : null}
                        </div>
                        {(this.state.companyData) ? (() => {
                            return <ClearFix/>;
                        })() : null}
                        {(this.state.companyData) ? (() => {
                            const mdNum = 12;
                            const lgNum = 6;

                            return <Row style={style.companyRow}>
                                {(isIntelScoresLoaded) ? (() => {
                                    let companyData = this.state.companyData;
                                    let maxIntelScore = companyData.maxIntelScore;
                                    if (this.state.intelScore != null) {
                                        maxIntelScore = this.state.intelScore;
                                    }
                                    let colorIntel = chooseColor(maxIntelScore);
                                    let intelScore = maxIntelScore || 0;

                                    if (displayScoreBackwards) {
                                        // scoreType is COMPANY because 100-score is required here.
                                        intelScore = app.switchScoreBackwards(intelScore, SCORE_TYPES.COMPANY);
                                    }
                                    let circlePercentValue;
                                    if (gaugeDisplayEnabled) {
                                        circlePercentValue = maxIntelScore;
                                    } else {
                                        // If maxIntelScore is 0 - display all circle as Green = chooseColor(0).
                                        circlePercentValue = (maxIntelScore === 0) ? 100 : maxIntelScore;
                                    }

                                    return <Col md={12} lg={6} key={1}>
                                        <div style={style.columnBox}>
                                            <div style={style.scoreHalfDiv}>
                                                <ScoreBox
                                                    inCompDashboard={true}
                                                    user={user}
                                                    key={shortid.generate()}
                                                    title={'Intel'}
                                                    value={maxIntelScore}
                                                    valueToDisplay={intelScore}
                                                    circlePercentValue={circlePercentValue || 0}
                                                    color={colorIntel}
                                                    surveyScore={null}
                                                    intelScore={null}
                                                    sensitivity={companyData.sensitivity}
                                                    companyID={companyData.id}
                                                    hideCardFrame={true}
                                                    disableClick={true}
                                                />
                                            </div>
                                            <div style={style.halfDiv}>
                                                <IntelBox
                                                    cveScore={cveScore}
                                                    dnsScore={dnsScore}
                                                    bucketsScore={bucketsScore}
                                                    emailsScore={emailsScore}
                                                    dataleaksScore={dataleaksScore}
                                                    blacklistsScore={blacklistsScore}
                                                    botnetsScore={botnetsScore}
                                                    sslCertsScore={sslCertsScore}
                                                />
                                            </div>
                                        </div>
                                    </Col>;
                                })() : null}
                                {(this.state.surveyData && this.state.surveyData.surveyScore != null && !this.state.companyData.isSurveyNA) ? (() => {
                                    let companyData = this.state.companyData;
                                    let surveyData = this.state.surveyData;
                                    let surveyCounts = surveyData.counts;
                                    let surveyScore = surveyData.surveyScore || 0;
                                    let colorSurvey = chooseColor(surveyScore);
                                    let surveyScoreToDisplay = surveyScore;

                                    let circlePercentValue;
                                    if (gaugeDisplayEnabled) {
                                        circlePercentValue = surveyScore;
                                    } else {
                                        // If maxIntelScore is 0 - display all circle as Green = chooseColor(0).
                                        circlePercentValue = (surveyScore === 0) ? 100 : surveyScore;
                                    }


                                    return <Col md={12} lg={6} key={2}>
                                        <div style={style.columnBox}>
                                            <div style={style.scoreHalfDiv}>
                                                <ScoreBox
                                                    inCompDashboard={true}
                                                    user={user}
                                                    key={shortid.generate()}
                                                    title={'Survey'}
                                                    value={surveyScore || 0}
                                                    valueToDisplay={surveyScoreToDisplay}
                                                    circlePercentValue={circlePercentValue || 0}
                                                    color={colorSurvey}
                                                    surveyScore={null}
                                                    intelScore={null}
                                                    sensitivity={companyData.sensitivity}
                                                    companyID={companyData.id}
                                                    hideCardFrame={true}
                                                    disableClick={false}
                                                    clickFunc={this.handleSurveyAnswerLink.bind(this, surveyData.said)}
                                                    isUnsafeCompany={isUnsafeCompany || false}
                                                />
                                            </div>
                                            <div style={style.surveyHalfDiv}>
                                                <SurveyBox
                                                    surveyProgress={surveyData.progress}
                                                    countComply={surveyCounts.complyCount}
                                                    countNonComply={surveyCounts.nonComplyCount}
                                                    countOther={surveyCounts.otherCount}
                                                    CountNA={surveyCounts.NACount}
                                                />
                                            </div>
                                        </div>
                                    </Col>;
                                })() : (isIntelScoresLoaded) ? (() => {
                                    // This section adds an empty survey results to fill the blank space.
                                    let surveyScore = 0;
                                    let colorSurvey = chooseColor(surveyScore);
                                    let surveyScoreToDisplay = 'N/A';

                                    let circlePercentValue;
                                    if (gaugeDisplayEnabled) {
                                        circlePercentValue = surveyScore;
                                    } else {
                                        // If maxIntelScore is 0 - display all circle as Green = chooseColor(0).
                                        circlePercentValue = (surveyScore === 0) ? 100 : surveyScore;
                                    }

                                    return <Col md={12} lg={6} key={3}>
                                        <div style={style.columnBox}>
                                            <div style={style.scoreHalfDiv}>
                                                <ScoreBox
                                                    inCompDashboard={true}
                                                    user={user}
                                                    key={shortid.generate()}
                                                    title={'Survey'}
                                                    value={surveyScore}
                                                    valueToDisplay={surveyScoreToDisplay}
                                                    circlePercentValue={circlePercentValue}
                                                    color={colorSurvey}
                                                    surveyScore={null}
                                                    intelScore={null}
                                                    hideCardFrame={true}
                                                    disableClick={true}
                                                />
                                            </div>
                                            <div style={style.surveyHalfDiv}>
                                                <SurveyBox
                                                    surveyProgress={0}
                                                />
                                            </div>
                                        </div>
                                    </Col>;
                                })() : null}
                            </Row>;
                        })() : null}
                        {(this.state.intelCounts && this.state.intelScoreRatios) ? (() => {
                            return <ClearFix/>;
                        })() : null}
                        {(isIntelScoresLoaded) ? (() => {
                            let companyName = '';
                            if (this.state.companyData && this.state.companyData.companyName) {
                                companyName = this.state.companyData.companyName;
                            }

                            return <div>
                                <Row style={style.scoresRow}>
                                    <Col sm={12} md={6} lg={3} style={style.minTopSpaceCell}>
                                        <MiniScoreBox
                                            key={shortid.generate()}
                                            title={'CVE'}
                                            weight={this.state.intelScoreRatios.cveWeight}
                                            value={cveScore}
                                            info={this.state.intelCounts[IntelKeys[0]].scoreData}
                                            type={'cve'}
                                            isBasic={this.state.isBasic}
                                            companyName={companyName}
                                            scoreColor={cveScoreColor}
                                        />
                                    </Col>
                                    <Col sm={12} md={6} lg={3} style={style.minTopSpaceCell}>
                                        <MiniScoreBox
                                            key={shortid.generate()}
                                            title={'DNS'}
                                            weight={this.state.intelScoreRatios.dnsWeight}
                                            value={dnsScore}
                                            info={this.state.intelCounts[IntelKeys[1]].scoreData}
                                            type={'dns'}
                                            isBasic={this.state.isBasic}
                                            companyName={companyName}
                                            scoreColor={dnsScoreColor}
                                        />
                                    </Col>
                                    <Col sm={12} md={6} lg={3} style={style.minTopSpaceCell}>
                                        <MiniScoreBox
                                            key={shortid.generate()}
                                            title={'S3 Buckets'}
                                            weight={this.state.intelScoreRatios.bucketsWeight}
                                            value={bucketsScore}
                                            info={this.state.intelCounts[IntelKeys[2]].scoreData}
                                            type={'buckets'}
                                            isBasic={this.state.isBasic}
                                            companyName={companyName}
                                            scoreColor={bucketsScoreColor}
                                        />
                                    </Col>
                                    <Col sm={12} md={6} lg={3} style={style.minTopSpaceCell}>
                                        <MiniScoreBox
                                            key={shortid.generate()}
                                            title={'Emails'}
                                            weight={this.state.intelScoreRatios.breachesWeight}
                                            value={emailsScore}
                                            info={this.state.intelCounts[IntelKeys[3]].scoreData}
                                            type={'emails'}
                                            isBasic={this.state.isBasic}
                                            companyName={companyName}
                                            scoreColor={emailsScoreColor}
                                        />
                                    </Col>
                                </Row>
                                <Row style={style.scoresRow}>
                                    <Col sm={12} md={6} lg={3} style={style.topSpaceCell}>
                                        <MiniScoreBox
                                            key={shortid.generate()}
                                            title={'Dataleaks'}
                                            weight={this.state.intelScoreRatios.dataleaksWeight}
                                            value={dataleaksScore}
                                            info={this.state.intelCounts[IntelKeys[4]].scoreData}
                                            type={'dataleaks'}
                                            isBasic={this.state.isBasic}
                                            companyName={companyName}
                                            scoreColor={dataleaksScoreColor}
                                        />
                                    </Col>
                                    <Col sm={12} md={6} lg={3} style={style.topSpaceCell}>
                                        <MiniScoreBox
                                            key={shortid.generate()}
                                            title={'Blacklists'}
                                            weight={this.state.intelScoreRatios.blacklistsWeight}
                                            value={blacklistsScore}
                                            info={this.state.intelCounts[IntelKeys[5]].scoreData}
                                            type={'blacklists'}
                                            isBasic={this.state.isBasic}
                                            companyName={companyName}
                                            scoreColor={blacklistsScoreColor}
                                        />
                                    </Col>
                                    <Col sm={12} md={6} lg={3} style={style.topSpaceCell}>
                                        <MiniScoreBox
                                            key={shortid.generate()}
                                            title={'Botnets'}
                                            weight={this.state.intelScoreRatios.botnetsWeight}
                                            value={botnetsScore}
                                            info={this.state.intelCounts[IntelKeys[6]].scoreData}
                                            type={'botnets'}
                                            isBasic={this.state.isBasic}
                                            companyName={companyName}
                                            scoreColor={botnetsScoreColor}
                                        />
                                    </Col>
                                    <Col sm={12} md={6} lg={3} style={style.topSpaceCell}>
                                        <MiniScoreBox
                                            key={shortid.generate()}
                                            title={'Https'}
                                            weight={this.state.intelScoreRatios.sslCertsWeight}
                                            value={sslCertsScore}
                                            info={this.state.intelCounts[IntelKeys[7]].scoreData}
                                            type={'sslCerts'}
                                            isBasic={this.state.isBasic}
                                            companyName={companyName}
                                            scoreColor={sslCertsScoreColor}
                                        />
                                    </Col>
                                </Row>
                            </div>;
                        })() : null}
                        <ClearFix/>
                        <Row style={style.row}>
                            <Col md={12} lg={6}>
                                <CveSeverity data={this.state.companyCVESeverity} key={'f1'}
                                             companyName={this.state.companyData.companyName}
                                             fromWhere={'drillDownDashboard'}/>
                            </Col>
                            <Col sm={6} md={12} lg={6}>
                                <CvePerHostname user={user} data={this.state.cvePerHostname} key={'f2'}/>
                            </Col>
                        </Row>
                        <ClearFix/>
                        {(this.state.companyData && this.state.companyData.extraData) ? (() => {
                            let numOfExtraDataBoxesDisplayed = 0;
                            return <Row style={style.contactsRow}>
                                {(this.state.companyData.extraData.responsibles) ? (() => {
                                    numOfExtraDataBoxesDisplayed++;
                                    return <div style={style.halfDiv}>
                                        <Col sm={12} key={4}>
                                            <ContactsBox
                                                user={user}
                                                title={'Auditors'}
                                                isResponsibles={true}
                                                data={this.state.companyData.extraData.responsibles}
                                            />
                                        </Col>
                                    </div>;
                                })() : null}
                                {(this.state.companyData.extraData.contacts) ? (() => {
                                    numOfExtraDataBoxesDisplayed++;
                                    return <div style={style.halfDiv}>
                                        <Col sm={12} key={5}>
                                            <ContactsBox
                                                user={user}
                                                title={'Company Contacts'}
                                                data={this.state.companyData.extraData.contacts}
                                            />
                                        </Col>
                                    </div>;
                                })() : null}
                                {(this.state.companyData.extraData.informations) ? (() => {
                                    let infoDivStyle;
                                    if (numOfExtraDataBoxesDisplayed >= 2) {
                                        infoDivStyle = style.halfDivTopSpace;
                                    } else {
                                        infoDivStyle = style.halfDiv;
                                    }
                                    numOfExtraDataBoxesDisplayed++;
                                    return <div style={infoDivStyle}>
                                        <Col sm={12} key={5}>
                                            <CompanyInformationBox
                                                title={'Company Conditions'}
                                                data={this.state.companyData.extraData.informations}
                                                allowedEdit={this.state.companyData.edit}
                                                updateInfoCheck={this.updateInfoCheck.bind(this)}
                                            />
                                        </Col>
                                    </div>;
                                })() : null}
                                {(this.state.companyData.extraData.descriptions || this.state.companyData.extraData.classifications
                                    || this.state.companyData.extraData.sectors || this.state.companyData.extraData['date_of_contacts']
                                    || this.state.companyData.extraData['date_of_surveys']) ? (() => {
                                    let descDivStyle;

                                    if (numOfExtraDataBoxesDisplayed >= 2) {
                                        descDivStyle = style.halfDivTopSpace;
                                    } else {
                                        descDivStyle = style.halfDiv;
                                    }
                                    numOfExtraDataBoxesDisplayed++;
                                    return <div style={descDivStyle}>
                                        <Col sm={12} key={5}>
                                            <ClassificationDescriptionBox
                                                title={'Company Information'}
                                                description={this.state.companyData.extraData.descriptions}
                                                classifications={this.state.companyData.extraData.classifications}
                                                sectors={this.state.companyData.extraData.sectors}
                                                dateOfContacts={this.state.companyData.extraData['date_of_contacts']}
                                                dateOfSurveys={this.state.companyData.extraData['date_of_surveys']}
                                            />
                                        </Col>
                                    </div>;
                                })() : null}
                            </Row>;
                        })() : null}
                        <Row style={style.locationRow}>
                            <Col>
                                <AssetMap user={user} data={this.state.locations}/>
                            </Col>
                        </Row>
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
};
