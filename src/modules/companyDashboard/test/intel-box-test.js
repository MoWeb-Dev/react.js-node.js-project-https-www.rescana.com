const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const commonHelpers = require('../CommonHelpers');

describe('CommonHelpers', function() {
    describe('isDomainNotSubdomain(hostname) - hostname should be only domain without subdomains', function() {
        it('Should identify hostnames as domains', function() {
            let isValidDomainPassesOK = true;

            const validTries = [
                'google.com',
                'google.co.il',
                'www.google.com',
                'http://google.com',
                'https://google.com',
                'http://www.google.co.il',
                'https://www.a.de'
            ];

            for (let i=0; i<validTries.length; i++) {
                if (!commonHelpers.isDomainNotSubdomain(validTries[i])) {
                    isValidDomainPassesOK = false;
                    console.log('hostname: [', validTries[i], '] should be a domain but failed to identify it as such.');
                }
            }

            return expect(isValidDomainPassesOK).to.equal(true);
        });

        it('Should identify hostnames as subdomains', function() {
            let isValidSubdomainPassesOK = true;

            const inValidTries = [
                'a.google.com',
                'ww.google.co.il',
                'www.sub.google.net',
                'http://s.google.com',
                'https://s.google.com',
                'http://www.s.google.co.il',
                'https://www.a.a.de'
            ];

            for (let i=0; i<inValidTries.length; i++) {
                if (commonHelpers.isDomainNotSubdomain(inValidTries[i])) {
                    isValidSubdomainPassesOK = false;
                    console.log('hostname: [', inValidTries[i], '] should be a subdomain but failed to identify it as such.');
                }
            }

            return expect(isValidSubdomainPassesOK).to.equal(true);
        });

        it('Should not identify invalid hostnames as domains', function() {
            let isNotHostnamePassesOK = true;

            const notHostnames = [
                '',
                null,
                undefined,
                {},
                '.',
                '.com',
                'a',
                '.co.il'
            ];

            for (let i=0; i<notHostnames.length; i++) {
                if (commonHelpers.isDomainNotSubdomain(notHostnames[i])) {
                    isNotHostnamePassesOK = false;
                    console.log('hostname: [', notHostnames[i], '] should not be a domain but somehow was identified as such.');
                }
            }

            return expect(isNotHostnamePassesOK).to.equal(true);
        });
    });

    describe('clearHTMLTags(string) - should return a text without any HTML attributes', function() {
        it('Should remove HTML tags', function() {
            const dataWithTags = 'In October 2013, 153 million Adobe accounts were breached with each containing an internal ID, username, email, <em>encrypted</em> password and a password hint in plain text. The password cryptography was poorly done and <a href="http://stricture-group.com/files/adobe-top100.txt" target="_blank" rel="noopener">many were quickly resolved back to plain text</a>. The unencrypted hints also <a href="http://www.troyhunt.com/2013/11/adobe-credentials-and-serious.html" target="_blank" rel="noopener">disclosed much about the passwords</a> adding further to the risk that hundreds of millions of Adobe customers already faced.';

            const dataWithoutTags = 'In October 2013, 153 million Adobe accounts were breached with each containing an internal ID, username, email, encrypted password and a password hint in plain text. The password cryptography was poorly done and many were quickly resolved back to plain text. The unencrypted hints also disclosed much about the passwords adding further to the risk that hundreds of millions of Adobe customers already faced.';

            return expect(commonHelpers.clearHTMLTags(dataWithTags)).to.equal(dataWithoutTags);
        });

        it('Should decode HTML elements', function() {
            const dataEncoded = 'In April 2016, customer data obtained from the streaming app known as &quot;17&quot;.';

            const dataDecoded = 'In April 2016, customer data obtained from the streaming app known as "17".';

            return expect(commonHelpers.clearHTMLTags(dataEncoded)).to.equal(dataDecoded);
        });

        it('Should clear all HTML attributes', function() {
            const dataWithTagsEncoded = 'In April 2016, customer data obtained from the streaming app known as &quot;17&quot; <a href="http://motherboard.vice.com/read/another-day-another-hack-millions-of-user-accounts-for-streaming-app-17" target="_blank" rel="noopener">appeared listed for sale on a Tor hidden service marketplace</a>. The data contained over 4 million unique email addresses along with IP addresses, usernames and passwords stored as unsalted MD5 hashes.';

            const dataWithoutTagsDecoded = 'In April 2016, customer data obtained from the streaming app known as "17" appeared listed for sale on a Tor hidden service marketplace. The data contained over 4 million unique email addresses along with IP addresses, usernames and passwords stored as unsalted MD5 hashes.';

            return expect(commonHelpers.clearHTMLTags(dataWithTagsEncoded)).to.equal(dataWithoutTagsDecoded);
        });
    });
});
