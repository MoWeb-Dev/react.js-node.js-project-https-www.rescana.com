import React from 'react';
import AddCompanyModal from '../projects/company-modal.jsx';
import {DEFAULT_INTEL_SCORE_RATIOS, ORGANIZATION_ID} from '../../../config/consts.js';
import moment from 'moment/moment';


class EditCompanyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openAddNewCompanyModal: false,
            companies: this.props.companies,
            users: this.props.users,
            companyFields: this.props.companyFields || {
                companyName: '',
                allowedUsers: [],
                allowedEditUsers: [], // Users that are allowed to change company details.
                selectedDomains: [],
                ratios: {
                    surveyWeight: 50,
                    intelWeight: 50
                },
                intelScoreRatios: DEFAULT_INTEL_SCORE_RATIOS,
                sensitivity: 2,
                keywords: [],
                createDate: '',
                companyDescription: '',
                companyClassifications: [],
                companyInformation: []
            }
        };

        this.handleOpenAddNewCompanyModal = this.handleOpenAddNewCompanyModal.bind(this);
        this.getCompanyDataById = this.getCompanyDataById.bind(this);
    }

    handleOpenAddNewCompanyModal() {
        this.clearAllModalFields();
        this.setState({openAddNewCompanyModal: true});
    }

    getCompanyDataById(companyID) {
        const user = app.getAuthUser();
        const orgId = localStorage.getItem(ORGANIZATION_ID);
        if (!user) {
            app.routeAuthUser();
        } else if (companyID) {
            $.ajax({
                type: 'POST',
                url: '/api/getMaxCVSScore',
                data: JSON.stringify({companyID: companyID, orgId: orgId}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data) {
                    this.setState({
                        companyFields: data
                    });
                }
            }).fail((ignore, textStatus) => {
                if (textStatus === 'parsererror') {
                    app.removeAuthUser();
                    app.handleSignOut();
                }
            });
        }
    }

    clearAllModalFields() {
        const companyFields = this.state.companyFields;
        companyFields.companyName = '';
        companyFields.allowedEditUsers = [];
        companyFields.selectedDomains = [];
        companyFields.ratios = {
            surveyWeight: 50,
            intelWeight: 50
        };
        companyFields.intelScoreRatios = DEFAULT_INTEL_SCORE_RATIOS;
        companyFields.sensitivity = '';
        companyFields.createDate = '';
        companyFields.id = '';

        this.setState({
            openAddNewCompanyModal: false,
            companyFields: companyFields
        });
        this.props.closeForm();
    }

    editCompanyByUser(companyId, e) {
        e.preventDefault();
        const companyFields = this.state.companyFields;

        function matchesEl(el) {
            return el.id === companyId;
        }

        const company = this.state.companies.filter((companyId) => {
            return matchesEl(companyId);
        });

        companyFields.companyName = company[0].companyName;
        companyFields.allowedEditUsers = company[0].allowedEditUsers;
        companyFields.selectedDomains = company[0].selectedDomains;
        companyFields.sensitivity = company[0].sensitivity;
        companyFields.ratios = company[0].ratios;
        companyFields.intelScoreRatios = company[0].intelScoreRatios;
        companyFields.id = company[0].id;

        this.setState({
            openAddNewCompanyModal: true,
            companyFields: companyFields
        });
    }

    componentWillMount() {
        app.setFlagVisibile(false);

        // Check if the component has the required company data or not.
        if (this.state.companyFields && !this.state.companyFields.id && this.props.companyFields && this.props.companyFields.id) {
            this.getCompanyDataById(this.props.companyFields.id);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            const stateObj = {openAddNewCompanyModal: nextProps.open || false};

            if (nextProps.companyFields) {
                stateObj.companyFields = {
                    companyName: nextProps.companyFields.companyName || '',
                    allowedEditUsers: nextProps.companyFields.allowedEditUsers || [],
                    selectedDomains: nextProps.companyFields.selectedDomains || [],
                    sensitivity: nextProps.companyFields.sensitivity || '',
                    ratios: nextProps.companyFields.ratios || {
                        surveyWeight: 50,
                        intelWeight: 50
                    },
                    intelScoreRatios: nextProps.companyFields.intelScoreRatios || DEFAULT_INTEL_SCORE_RATIOS,
                    id: nextProps.companyFields.id || ''
                };

                if (nextProps.companyFields.extraData) {
                    if (nextProps.companyFields.extraData.hasOwnProperty('responsibles') && Array.isArray(nextProps.companyFields.extraData.responsibles)) {
                        stateObj.companyFields.companyResponsibles = nextProps.companyFields.extraData.responsibles;
                    }

                    if (nextProps.companyFields.extraData.hasOwnProperty('contacts') && Array.isArray(nextProps.companyFields.extraData.contacts)) {
                        stateObj.companyFields.companyContacts = nextProps.companyFields.extraData.contacts;
                    }

                    if (nextProps.companyFields.extraData.hasOwnProperty('informations') && Array.isArray(nextProps.companyFields.extraData.informations)) {
                        stateObj.companyFields.companyInformation = [];
                        nextProps.companyFields.extraData.informations.map((currInfo) => {
                            if (currInfo && currInfo.label) {
                                // If no 'checked' property exists - set as false.
                                currInfo.checked = !!currInfo.checked;
                                stateObj.companyFields.companyInformation.push(currInfo);
                            }
                        });
                    }

                    if (nextProps.companyFields.extraData.hasOwnProperty('classifications') && Array.isArray(nextProps.companyFields.extraData.classifications)) {
                        stateObj.companyFields.companyClassifications = [];
                        nextProps.companyFields.extraData.classifications.map((currClass) => {
                            if (currClass && currClass.label) {
                                stateObj.companyFields.companyClassifications.push(currClass.label);
                            }
                        });
                    }
                    if (nextProps.companyFields.extraData.hasOwnProperty('sectors') && Array.isArray(nextProps.companyFields.extraData.sectors)) {
                        stateObj.companyFields.companySectors = [];
                        nextProps.companyFields.extraData.sectors.map((currSector) => {
                            if (currSector && currSector.label) {
                                stateObj.companyFields.companySectors.push(currSector.label);
                            }
                        });
                    }

                    if (nextProps.companyFields.extraData.hasOwnProperty('date_of_contacts') && Array.isArray(nextProps.companyFields.extraData['date_of_contacts'])
                        && nextProps.companyFields.extraData['date_of_contacts'].length === 1) {
                        const rangeDates = nextProps.companyFields.extraData['date_of_contacts'][0];

                        if (rangeDates && rangeDates.startDate && rangeDates.endDate) {
                            stateObj.companyFields.companyDateOfContact = rangeDates;
                        }
                    }

                    if (nextProps.companyFields.extraData.hasOwnProperty('date_of_surveys') && Array.isArray(nextProps.companyFields.extraData['date_of_surveys'])
                        && nextProps.companyFields.extraData['date_of_surveys'].length === 1) {
                        const rangeDates = nextProps.companyFields.extraData['date_of_surveys'][0];

                        if (rangeDates && rangeDates.startDate && rangeDates.endDate) {
                            stateObj.companyFields.companyDateOfSurvey = rangeDates;
                        }
                    }

                    if (nextProps.companyFields.extraData.hasOwnProperty('descriptions') && Array.isArray(nextProps.companyFields.extraData.descriptions)
                        && nextProps.companyFields.extraData.descriptions.length === 1) {
                        const desc = nextProps.companyFields.extraData.descriptions[0];

                        if (desc && desc.label) {
                            stateObj.companyFields.companyDescription = desc.label;
                        }
                    }
                }
            }

            this.setState(stateObj);
        }
    }
    // ./node_modules/eslint/bin/eslint
    // ./node_modules/eslint/bin/eslint --fix --parser babel-eslint --no-eslintrc  --rule 'jsx-quotes: ["error", "prefer-single"]'
    editUserCompany(companyFields) {
        const user = app.getAuthUser();
        let orgId = localStorage.getItem(ORGANIZATION_ID);

        if (!user) {
            app.routeAuthUser();
        } else if (companyFields && !companyFields.companyName) {
            app.addAlert('error', 'Error: Company Name must be assigned.');
        } else {
            const dataToSend = companyFields;

            if (!dataToSend.sensitivity) {
                dataToSend.sensitivity = 2;
            }

            // This is a fix for editCompanyByUser.
            if (!dataToSend.hasOwnProperty('keywords')) {
                dataToSend.keywords = [];
            }

            if (!dataToSend.hasOwnProperty('allowedUsers')) {
                dataToSend.allowedUsers = [];
            }
            dataToSend.createDate = moment().format();
            $.ajax({
                type: 'post',
                url: '/api/editCompanyByUser',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({cid: companyFields.id, orgId:orgId, data: dataToSend}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.newCompany) {
                    app.addAlert('success', 'Done');
                    this.setState({openAddNewCompanyModal: false});

                    if (this.props.editUserCompany) {
                        this.props.editUserCompany(data.newCompany);
                    }
                } else if (data && data.error) {
                    app.addAlert('error', data.error);
                } else {
                    app.addAlert('error', 'Error: failed to Edit company.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add company.');
            });
        }
    }

    render() {
        return (
            <AddCompanyModal
                addCompany={this.editUserCompany.bind(this)}
                companyFields={this.state.companyFields}
                companies={this.state.companies}
                clearAllModalFields={this.clearAllModalFields.bind(this)}
                allUsers={this.state.users}
                openAddNewCompanyModal={this.state.openAddNewCompanyModal}
                inCompanyEditMode={true}
            />
        );
    }
}

export default EditCompanyForm;
