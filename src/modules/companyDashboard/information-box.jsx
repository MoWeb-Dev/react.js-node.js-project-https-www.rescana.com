import React from 'react';
import {Card, CardHeader} from 'material-ui/Card';
import {typography} from 'material-ui/styles';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {INFORMATION_BOX_TYPE} from '../../../config/consts.js';
import DataTableViewTemplate from '../common/datatableTemplate/datatable-template.jsx';
import Checkbox from 'material-ui/Checkbox';
import '../../../public/css/customScrollbar.css';

class CompanyInformationBox extends React.Component {
    constructor() {
        super();

        const wrapBreakStyle = {
            wordBreak: 'break-word',
            whiteSpace: 'pre-wrap'
        };

        this.state = {
            tableColumns: [
                {
                    label: '',
                    key: 'checkbox',
                    style: wrapBreakStyle,
                    noneSearchable: true
                }
            ]
        };
    }

    updateInfoCheck(nid, shouldDisable, e) {
        if (shouldDisable) {
            app.addAlert('warning', 'You do not have permissions to edit');
        } else {
            if (this.props && this.props.updateInfoCheck && nid) {
                this.props.updateInfoCheck(nid, e.target.checked);
            }
        }
    }

    addCheckboxToDataChildren(data) {
        const children = [];

        const shouldDisable = !this.props || !this.props.allowedEdit;

        const checkboxStyle = (shouldDisable) ? {
            cursor: 'not-allowed'
        } : {};

        if (data && Array.isArray(data)) {
            data.map((currEntity) => {
                if (currEntity && currEntity.nid && currEntity.label) {
                    children.push({
                        checkbox: <Checkbox
                            label={currEntity.label}
                            style={checkboxStyle}
                            checked={!!currEntity.checked}
                            onCheck={this.updateInfoCheck.bind(this, currEntity.nid, shouldDisable)}
                        />
                    });
                }
            });
        }

        return children;
    }

    render() {
        const {title, data} = this.props;

        const styles = {
            subStyle: {
                fontSize: 12
            },
            headerTitle: {
                fontSize: 16,
                fontWeight: typography.fontWeightLight,
                color: 'rgb(255, 255, 255)'
            },
            header: {
                padding: '10px'
            },
            cardStyle: {
                background: 'linear-gradient(60deg, #ffa726, #fb8c00)'
            },
            childrenDiv: {
                overflow: 'auto',
                maxHeight: '235px',
                padding: '0px',
                marginTop: '5px'
            }
        };

        const dataToDisplay = this.addCheckboxToDataChildren(data);

        return (
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <div>
                    <Card style={styles.cardStyle}>
                        <CardHeader
                            titleStyle={styles.headerTitle}
                            subtitleStyle={styles.subStyle}
                            showExpandableButton={false}
                            title={title}
                            style={styles.header}
                        />
                    </Card>
                    <div className='scrollableDiv' style={styles.childrenDiv}>
                        <DataTableViewTemplate
                            myDataType={INFORMATION_BOX_TYPE}
                            tableColumns={this.state.tableColumns}
                            tableData={dataToDisplay}
                            hideHeaderToolbar={true}
                            hideFooterToolbar={true}
                        />
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default CompanyInformationBox;
