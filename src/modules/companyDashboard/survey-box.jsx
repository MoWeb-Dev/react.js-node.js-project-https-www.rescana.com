import React from 'react';
import {Card, CardHeader, CardActions} from 'material-ui/Card';
import {grey800} from 'material-ui/styles/colors';
import {typography} from 'material-ui/styles';
import LinearProgress from 'material-ui/LinearProgress';
import BarChart from './bar-chart.jsx';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../app/themes/themeForFont";

class SurveyBox extends React.Component {
    render() {
        const {surveyProgress, countComply, countNonComply, countOther, CountNA} = this.props;

        const styles = {
            cardStyle: {
                boxShadow: ''
            },
            header: {
                fontSize: 16,
                fontWeight: typography.fontWeightLight,
                marginBottom: '10px'
            },
            content: {
                padding: '0px 10px 10px'
            },
            data: {
                height: 270,
                overflowY: 'auto',
                fontSize: 12,
                fontWeight: typography.fontWeightLight,
                color: grey800
            },
            questions: {
                padding: '0px 25px 5px'
            }
        };

        const progress = Math.floor(surveyProgress) || 0;

        const labels = ['Comply', 'Non Comply', 'Other', 'N/A'];

        // The last 0 is necessary to fix the chart's ratio. (and will not be displayed).
        const data = [countComply || 0, countNonComply || 0, countOther || 0, CountNA || 0, 0];

        const countDescriber = 'Questions';

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <Card style={styles.cardStyle}>
                    <CardHeader
                        titleStyle={styles.header}
                        title="SURVEY FINDINGS"/>
                    <div style={styles.content}>
                        <div style={styles.data}>
                            <BarChart
                                labels={labels}
                                data={data}
                                countDescriber={countDescriber}
                            />
                        </div>
                    </div>
                    <CardActions>
                        <div style={styles.header}>
                            <span>Survey Progress: {progress}%</span>
                            <LinearProgress mode="determinate" value={progress}/>
                        </div>
                    </CardActions>
                </Card>
            </MuiThemeProvider>
        );
    }
}

export default SurveyBox;
