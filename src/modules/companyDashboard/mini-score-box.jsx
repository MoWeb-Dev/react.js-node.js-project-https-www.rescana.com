import React from 'react';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import {grey800} from 'material-ui/styles/colors';
import {typography} from 'material-ui/styles';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Avatar from 'material-ui/Avatar';
import ReactTooltip from 'react-tooltip';
import {browserHistory} from 'react-router';
import InfoIcon from 'material-ui/svg-icons/action/help-outline';
import Popover from 'material-ui/Popover/Popover';
import Paper from 'material-ui/Paper';
import {SESSION_COMPANIES} from '../../../config/consts.js';
import {SESSION_CVE_SEVERITY, SESSION_CVE_SEVERITY_PANEL} from "../../../config/consts";

const shortid = require('shortid');

class MiniScoreBox extends React.Component {
    constructor() {
        super();

        this.state = {
            openInfoBox: false
        };
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    getDescriptionInfoText(type) {
        let text = '';
        if (type) {
            switch (type) {
            case 'cve': {
                text = 'This score represents the highest CVSS found on this company';
                break;
            }
            case 'dns': {
                text = 'This score represents the SPF\'s and DMARC\'s ratio between not configured records to all records';
                break;
            }
            case 'buckets': {
                text = 'This score represents the ratio between Public S3 Buckets to all Buckets';
                break;
            }
            case 'blacklists': {
                text = 'This score represents the risk of records found in Blacklists';
                break;
            }
            case 'emails': {
                text = 'This score represents the risk of emails in breaches';
                break;
            }
            case 'botnets': {
                text = 'This score represents the risk of related domains which were discovered as Botnets, C&C servers or Malware';
                break;
            }
            case 'sslCerts': {
                text = 'This score represents the risk of related domains or subdomains which were not configured with Https protocol';
                break;
            }
            default: {
                // Default is dataleaks.
                if (type === 'dataleaks') {
                    text = 'This score represents the risk of discovered Dataleaks';
                }
                break;
            }
            }
        }
        return text;
    }

    getBlacklistsKeyDescriber(key) {
        let desc = '';
        if (key) {
            switch (key) {
            case 'ip': {
                desc = 'IPs Blacklisted';
                break;
            }
            case 'domain': {
                desc = 'Domains Blacklisted';
                break;
            }
            case 'mx': {
                desc = 'MX Records Blacklisted';
                break;
            }
            default: {
                // Default is "ns".
                if (key === 'ns') {
                    desc = 'NS Records Blacklisted';
                }
                break;
            }
            }
        }
        return desc;
    }

    getDataChildren(type, info) {
        let children = [];

        const styles = {
            div: {
                display: 'flex'
            },
            bigSpan: {
                width: '85%'
            },
            smallSpan: {
                width: '15%',
                textAlign: 'center'
            }
        };

        if (type) {
            switch (type) {
            case 'cve': {
                let criticalCVSSCount = (info && info.criticalCVSSCount) ? info.criticalCVSSCount : 0;
                let mediumCVSSCount = (info && info.mediumCVSSCount) ? info.mediumCVSSCount : 0;
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Critical CVE's</span><span style={styles.smallSpan}>{criticalCVSSCount}</span></div>);
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Medium risk CVE's</span><span style={styles.smallSpan}>{mediumCVSSCount}</span></div>);
                break;
            }
            case 'dns': {
                let countNotConfiguredSPFs = (info && info.countNotConfiguredSPFs) ? info.countNotConfiguredSPFs : 0;
                let countNotConfiguredDMARCs = (info && info.countNotConfiguredDMARCs) ? info.countNotConfiguredDMARCs : 0;
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>SPF's not configured</span><span style={styles.smallSpan}>{countNotConfiguredSPFs}</span></div>);
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>DMARC's not configured</span><span style={styles.smallSpan}>{countNotConfiguredDMARCs}</span></div>);
                break;
            }
            case 'buckets': {
                let publicBucketsCount = (info && info.publicBucketsCount) ? info.publicBucketsCount : 0;
                let totalBucketsCount = (info && info.totalBucketsCount) ? info.totalBucketsCount : 0;
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Public Buckets</span><span style={styles.smallSpan}>{publicBucketsCount}</span></div>);
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Total Buckets</span><span style={styles.smallSpan}>{totalBucketsCount}</span></div>);
                break;
            }
            case 'blacklists': {
                if (!info) {
                    // Just a default initialize in case info is undefined.
                    info = {
                        ip: 0,
                        mx: 0
                    };
                }
                // Add each blacklist finding by keys. (There are 4 options only top 2 of them will be displayed).
                children = children.concat(Object.keys(info).map((currKey) => {
                    let description = this.getBlacklistsKeyDescriber(currKey);
                    return <div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>{description}</span><span style={styles.smallSpan}>{info[currKey]}</span></div>;
                }));
                break;
            }
            case 'emails': {
                let breachedEmailsCount = (info && info.breachedEmailsCount) ? info.breachedEmailsCount : 0;
                let totalEmailsCount = (info && info.totalEmailsCount) ? info.totalEmailsCount : 0;
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Emails in breaches</span><span style={styles.smallSpan}>{breachedEmailsCount}</span></div>);
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Total Emails</span><span style={styles.smallSpan}>{totalEmailsCount}</span></div>);
                break;
            }
            case 'botnets': {
                let infectedDomainsCount = (info && info.infectedDomainsCount) ? info.infectedDomainsCount : 0;
                let totalDomainsCount = (info && info.totalDomainsCount) ? info.totalDomainsCount : 0;
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Infected Domains</span><span style={styles.smallSpan}>{infectedDomainsCount}</span></div>);
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Total Domains</span><span style={styles.smallSpan}>{totalDomainsCount}</span></div>);
                break;
            }
            case 'sslCerts': {
                let notValidHttpsDomainsCount = (info && info.notValidHttpsDomainsCount) ? info.notValidHttpsDomainsCount : 0;
                let validHttpsDomainsCount = (info && info.validHttpsDomainsCount) ? info.validHttpsDomainsCount : 0;
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Domains without valid SSL</span><span style={styles.smallSpan}>{notValidHttpsDomainsCount}</span></div>);
                children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Domains with valid SSL</span><span style={styles.smallSpan}>{validHttpsDomainsCount}</span></div>);
                break;
            }
            default: {
                // Default is dataleaks.
                if (type === 'dataleaks') {
                    let criticalDataleaks = (info && info.criticalDataleaks) ? info.criticalDataleaks : 0;
                    let mediumDataleaks = (info && info.mediumDataleaks) ? info.mediumDataleaks : 0;
                    children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Critical Dataleaks</span><span style={styles.smallSpan}>{criticalDataleaks}</span></div>);
                    children.push(<div key={shortid.generate()} style={styles.div}><span style={styles.bigSpan}>Medium risk Dataleaks</span><span style={styles.smallSpan}>{mediumDataleaks}</span></div>);
                }
                break;
            }
            }
        }
        return children;
    }

    handleClickLinks(type, info, companyName) {
        let linkClick = () => {};
        if (type) {
            switch (type) {
            case 'cve': {
                linkClick = this.handleIntelCVELink.bind(this, companyName);
                //set cve severity empty because we redirect to cve page from another place
                sessionStorage.removeItem(SESSION_CVE_SEVERITY);
                sessionStorage.removeItem(SESSION_CVE_SEVERITY_PANEL);
                break;
            }
            case 'dns': {
                // If DMARC's findings are larger - move to DMARC page, otherwise move to CVE page.
                if (info && info.countNotConfiguredDMARCs && info.countNotConfiguredDMARCs > info.countNotConfiguredSPFs) {
                    linkClick = this.handleIntelDMARCLink.bind(this, companyName);
                } else {
                    linkClick = this.handleIntelSPFLink.bind(this, companyName);
                }
                break;
            }
            case 'buckets': {
                linkClick = this.handleIntelBucketsLink.bind(this, companyName);
                break;
            }
            case 'blacklists': {
                linkClick = this.handleIntelBlacklistsLink.bind(this, companyName);
                break;
            }
            case 'emails': {
                linkClick = this.handleIntelEmailBreachesLink.bind(this, companyName);
                break;
            }
            case 'botnets': {
                linkClick = this.handleIntelBotnetsLink.bind(this, companyName);
                break;
            }
            case 'sslCerts': {
                // If there are any not-valid sslCerts - display them, otherwise display the valid results.
                if (info && info.notValidHttpsDomainsCount) {
                    linkClick = this.handleNotValidIntelSSLCertsLink.bind(this, companyName);
                } else {
                    linkClick = this.handleValidIntelSSLCertsLink.bind(this, companyName);
                }
                break;
            }
            default: {
                // Default is dataleaks.
                linkClick = this.handleIntelDataleaksLink.bind(this, companyName);
                break;
            }
            }
        }
        return linkClick;
    }

    handleIntelCVELink(companyName) {
        this.handleLink(companyName, '/intel/cve');
    }

    handleIntelDMARCLink(companyName) {
        this.handleLink(companyName, '/intel/dmarc');
    }

    handleIntelSPFLink(companyName) {
        this.handleLink(companyName, '/intel/spf');
    }

    handleIntelBucketsLink(companyName) {
        this.handleLink(companyName, '/intel/buckets');
    }

    handleIntelBlacklistsLink(companyName) {
        this.handleLink(companyName, '/blacklists');
    }

    handleIntelEmailBreachesLink(companyName) {
        this.handleLink(companyName, '/breaches');
    }

    handleIntelBotnetsLink(companyName) {
        this.handleLink(companyName, '/botnets');
    }

    handleValidIntelSSLCertsLink(companyName) {
        this.handleLink(companyName, '/ssl-certs-discovery');
    }

    handleNotValidIntelSSLCertsLink(companyName) {
        this.handleLink(companyName, '/ssl-certs-vulnerability');
    }

    handleIntelDataleaksLink(companyName) {
        this.handleLink(companyName, '/dataleaks');
    }

    handleLink(companyName, linkURL) {
        if (linkURL) {
            this.setCompanyOnSession(companyName);

            browserHistory.push(linkURL);
        }
    }

    setCompanyOnSession(companyName) {
        if (companyName) {
            // Change current company as selected on Session - so its info will be displayed next in intel pages.
            sessionStorage.setItem(SESSION_COMPANIES, companyName);
        }
    }

    render() {
        const {title, weight, value, info, type, isBasic, companyName, scoreColor} = this.props;

        let styles = {
            content: {
                padding: '0px 46px 10px 46px'
            },
            subtext: {
                fontSize: 12,
                paddingBottom: 10,
                marginLeft: 20,
                color: grey800
            },
            subStyle: {
                fontSize: 12
            },
            text: {
                fontSize: 14,
                fontWeight: typography.fontWeightLight,
                color: grey800
            },
            header: {
                fontSize: 16,
                fontWeight: typography.fontWeightLight
            },
            avatar: {
                backgroundColor: grey800
            },
            infoTextPopover: {
                maxWidth: 200,
                maxHeight: 100,
                overflow: 'auto',
                padding: '5px'
            },
            dataDiv: {
                display: 'grid',
                paddingTop: 0
            }
        };

        if (scoreColor) {
            styles.avatar.backgroundColor = scoreColor;
        }

        // If user is basic - disable further drillDown links.
        const disableLinkClick = !!isBasic;
        let linkClick;
        if (disableLinkClick) {
            linkClick = ()=>{};
            styles.dataDiv.cursor = 'default';
        } else {
            linkClick = this.handleClickLinks(type, info, companyName);
            styles.dataDiv.cursor = 'pointer';
        }

        let infoText = this.getDescriptionInfoText(type);

        let dataChildren = this.getDataChildren(type, info);

        const scoreWeightAndData = <div children={[<span>Weight - {weight}%</span>]}/>;

        const handleInfoBox = <div>
            <InfoIcon color={grey800} onClick={this.handleOpenInfoBox.bind(this)}/>
            <Popover
                open={this.state.openInfoBox}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                onRequestClose={this.handleCloseInfoBox.bind(this)}>
                <Paper
                    style={styles.infoTextPopover}>
                    <span style={styles.text}>
                        {infoText}
                    </span>
                </Paper>
            </Popover>
        </div>;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <Card style={styles.cardStyle}>
                    <CardHeader
                        titleStyle={styles.header}
                        subtitleStyle={styles.subStyle}
                        showExpandableButton={true}
                        openIcon={handleInfoBox}
                        closeIcon={handleInfoBox}
                        avatar={<Avatar data-tip={value + ' out of 100'} style={styles.avatar}>{value}</Avatar>}
                        title={title}
                        subtitle={scoreWeightAndData}
                    />
                    <CardText style={styles.dataDiv} onClick={linkClick}>
                        {dataChildren}
                    </CardText>
                    <ReactTooltip place="right"/>
                </Card>
            </MuiThemeProvider>
        );
    }
}

export default MiniScoreBox;
