import React from 'react';
import {Card, CardHeader} from 'material-ui/Card';
import {typography} from 'material-ui/styles';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ResponsibleIcon from 'material-ui/svg-icons/action/supervisor-account';
import ContactIcon from 'material-ui/svg-icons/social/person';
import {CONTACTS_BOX_TYPE} from '../../../config/consts.js';
import DataTableViewTemplate from '../common/datatableTemplate/datatable-template.jsx';
import '../../../public/css/customScrollbar.css';

class ContactsBox extends React.Component {
    constructor() {
        super();

        const wrapBreakStyle = {
            wordBreak: 'break-word',
            whiteSpace: 'pre-wrap'
        };

        let emailStyle = JSON.parse(JSON.stringify(wrapBreakStyle));
        emailStyle.width= '35%';

        this.state = {
            tableColumns: [
                {
                    label: '',
                    key: 'avatar',
                    style: {
                        width: '7%'
                    },
                    noneSearchable: true
                }, {
                    sortable: true,
                    label: 'Name',
                    key: 'name',
                    style: wrapBreakStyle
                }, {
                    sortable: true,
                    label: 'Email',
                    key: 'email',
                    style: emailStyle
                }, {
                    sortable: true,
                    label: 'Phone',
                    key: 'phone',
                    style: wrapBreakStyle
                }, {
                    sortable: true,
                    label: 'Position',
                    key: 'position',
                    style: wrapBreakStyle
                }
            ]
        };
    }

    addAvatarToDataChildren(data, avatarIcon) {
        let children = [];
        let isInDemoMode = this.props.user && this.props.user.isInDemoMode;

        if (data && Array.isArray(data)) {
            data.map((currContact, i) => {
                if (currContact) {
                    let objKeys = Object.keys(currContact);

                    // Display the contact only if has at least one property filled.
                    if (objKeys && objKeys.length > 0) {
                        if(isInDemoMode){
                            if(currContact.name){
                                currContact.name = 'Contact Example ' + i;
                            }
                            if(currContact.email){
                                currContact.email = 'EmailExample' + i + '@mail.com';
                            }
                        }
                        // Duplicate the JSON so the avatar will be only for the display and not on the state.
                        let currContactToAdd = JSON.parse(JSON.stringify(currContact));
                        currContactToAdd.avatar = avatarIcon;
                        children.push(currContactToAdd);
                    }
                }
            });
        }

        return children;
    }

    render() {
        const {title, data, isResponsibles} = this.props;

        let styles = {
            subStyle: {
                fontSize: 12
            },
            headerTitle: {
                fontSize: 16,
                fontWeight: typography.fontWeightLight,
                color: 'rgb(255, 255, 255)'
            },
            header: {
                padding: '10px'
            },
            cardStyle: {
                background: 'rgb(30, 136, 229)'
            },
            childrenDiv: {
                overflow: 'auto',
                maxHeight: '235px',
                padding: '0px',
                marginTop: '5px'
            }
        };

        let avatarIcon = (isResponsibles) ? <ResponsibleIcon/> : <ContactIcon/>;

        let dataToDisplay = this.addAvatarToDataChildren(data, avatarIcon);

        return (
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <div>
                    <Card style={styles.cardStyle}>
                        <CardHeader
                            titleStyle={styles.headerTitle}
                            subtitleStyle={styles.subStyle}
                            showExpandableButton={false}
                            title={title}
                            style={styles.header}
                        />
                    </Card>
                    <div className="scrollableDiv" style={styles.childrenDiv}>
                        <DataTableViewTemplate
                            myDataType={CONTACTS_BOX_TYPE}
                            tableColumns={this.state.tableColumns}
                            tableData={dataToDisplay}
                            hideHeaderToolbar={true}
                            hideFooterToolbar={true}
                        />
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default ContactsBox;
