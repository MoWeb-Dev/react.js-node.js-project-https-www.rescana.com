import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {Card, CardText, CardTitle} from 'material-ui/Card';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import {RadioButton} from 'material-ui/RadioButton';
import EyeIcon from 'material-ui/svg-icons/image/remove-red-eye';
import Divider from 'material-ui/Divider';

const shortid = require('shortid');

export default class PasswordChanger extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showChangePasswordFields: false,
            user: app.getAuthUser(),
            userData: {
                oldPassword: '',
                newPassword: '',
                repeatPassword: ''
            },
            newPassErrors: [],
            passDisplay: {
                oldPass: false,
                newPass: false,
                repeatPass: false
            }
        };
        this.changePassword = this.changePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangePasswordDisplay = this.handleChangePasswordDisplay.bind(this);
        this.clearPasswords = this.clearPasswords.bind(this);
    }

    componentDidMount() {
        // custom rule will have name 'isPasswordMatch'
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            if (value !== this.state.userData.newPassword) {
                // Just a state cleaning (to remove previous errors).
                if (this.state.newPassErrors.length > 0) {
                    this.setState({newPassErrors: []});
                }

                return false;
            }
            return true;
        });
    }

    changePassword() {
        this.setState({showChangePasswordFields: true});
    }

    clearPasswords() {
        this.setState({
            showChangePasswordFields: false,
            userData: {
                oldPassword: '',
                newPassword: '',
                repeatPassword: ''
            },
            newPassErrors: [],
            passDisplay: {
                oldPass: false,
                newPass: false,
                repeatPass: false
            }
        });
    }

    handleChange(event) {
        this.state.userData[event.target.name] = event.target.value;
        this.setState({userData: this.state.userData});
    }

    handleChangePasswordDisplay(event) {
        if (event.target.value) {
            this.state.passDisplay[event.target.value] = !this.state.passDisplay[event.target.value];
            this.setState({passDisplay: this.state.passDisplay});
        }
    }

    handleSubmit() {
        $.ajax({
            type: 'POST',
            url: '/aut/changePassword',
            data: JSON.stringify({'userData': this.state.userData}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true
        }).done((data) => {
            if (data.ok) {
                this.clearPasswords();
                app.addAlert('success', 'Password successfully changed.');
            } else {
                let newPassErrors;
                let errorMsg;

                if (data && data.error) {
                    if (Array.isArray(data.error)) {
                        newPassErrors = data.error;
                        errorMsg = 'Password is not strong enough';
                    } else {
                        newPassErrors = [];
                        errorMsg = data.error;
                    }
                } else {
                    newPassErrors = [];
                    errorMsg = 'Failed to change password';
                }

                this.setState({newPassErrors: newPassErrors});
                app.addAlert('error', errorMsg);
            }
        }).fail((ignore, ignore2, error) => {
            app.addAlert('error', 'Failed to change password');
            console.log('Failed to reset password: ', error);
        });
    };

    render() {
        const styles = {
            divWithIconAndTextfield: {
                position: 'relative',
                display: 'inline-block'
            },
            iconOnTextfield: {
                position: 'absolute',
                right: 10,
                top: 35,
                width: 20,
                height: 20
            },
            textFieldWithRightIcon: {
                paddingRight: 30
            }
        };

        return (
            <div>
                <Card style={{margin: '30px 30px 0px 30px', padding: '20px 30px 20px 30px' }}>
                    <CardText style={{marginTop: 10, padding: 10, fontSize: 20}}>Credentials</CardText>
                    <div style={{paddingLeft: 10,marginBottom: 10, fontSize: 13, color: '#757575'}}>Reset Password</div>
                    <br/>
                    <Divider/>
                    <CardText>
                        <div>
                            <span>
                                Change your password to be extra safe.<br />
                            </span>
                            <br/>
                            {!this.state.showChangePasswordFields ?
                                <RaisedButton
                                    label="Reset Password"
                                    primary={true}
                                    onTouchTap={this.changePassword}/> : ''}
                            {this.state.showChangePasswordFields ?
                                <ValidatorForm
                                    ref="form"
                                    onSubmit={this.handleSubmit}
                                    onError={(errors) => console.log(errors)}>
                                    <div style={styles.divWithIconAndTextfield}>
                                        <RadioButton
                                            onClick={this.handleChangePasswordDisplay}
                                            style={styles.iconOnTextfield}
                                            checkedIcon={<EyeIcon/>}
                                            uncheckedIcon={<EyeIcon/>}
                                            checked={this.state.passDisplay.oldPass}
                                            value={'oldPass'}
                                        />
                                        <TextValidator
                                            label="Old password"
                                            floatingLabelText={'Old Password'}
                                            onChange={this.handleChange}
                                            name="oldPassword"
                                            type={(this.state.passDisplay.oldPass) ? 'text' : 'password'}
                                            validators={['required']}
                                            errorMessages={['this field is required']}
                                            value={this.state.userData.oldPassword}
                                            style={styles.textFieldWithRightIcon}
                                        />
                                    </div>
                                    <br/>
                                    <br/>
                                    <div style={styles.divWithIconAndTextfield}>
                                        <RadioButton
                                            onClick={this.handleChangePasswordDisplay}
                                            style={styles.iconOnTextfield}
                                            checkedIcon={<EyeIcon/>}
                                            uncheckedIcon={<EyeIcon/>}
                                            checked={this.state.passDisplay.newPass}
                                            value={'newPass'}
                                        />
                                        <TextValidator
                                            label="New password"
                                            floatingLabelText={'New Password'}
                                            onChange={this.handleChange}
                                            name="newPassword"
                                            type={(this.state.passDisplay.newPass) ? 'text' : 'password'}
                                            validators={['required']}
                                            errorMessages={['this field is required']}
                                            errorText={(this.state.newPassErrors.length > 0)?
                                                'password is not strong enough': ''}
                                            value={this.state.userData.newPassword}
                                            style={styles.textFieldWithRightIcon}
                                        />
                                    </div>
                                    <br/>
                                    {this.state.newPassErrors && this.state.newPassErrors.length > 0 ?
                                        <div style={{display: 'grid'}}>
                                            {this.state.newPassErrors.map((currError) => {
                                                return <span key={shortid.generate()} style={{color: 'red'}}>
                                                    {currError}
                                                </span>;
                                            })}
                                        </div> : ''}
                                    <br/>
                                    <div style={styles.divWithIconAndTextfield}>
                                        <RadioButton
                                            onClick={this.handleChangePasswordDisplay}
                                            style={styles.iconOnTextfield}
                                            checkedIcon={<EyeIcon/>}
                                            uncheckedIcon={<EyeIcon/>}
                                            checked={this.state.passDisplay.repeatPass}
                                            value={'repeatPass'}
                                        />
                                        <TextValidator
                                            label="Repeat password"
                                            floatingLabelText={'Repeat Password'}
                                            onChange={this.handleChange}
                                            name="repeatPassword"
                                            type={(this.state.passDisplay.repeatPass) ? 'text' : 'password'}
                                            validators={['isPasswordMatch', 'required']}
                                            errorMessages={['password mismatch', 'this field is required']}
                                            value={this.state.userData.repeatPassword}
                                            style={styles.textFieldWithRightIcon}
                                        />
                                    </div>
                                    <br/>
                                    <br/>
                                    <RaisedButton
                                        label="Submit"
                                        primary={true}
                                        type="submit"
                                    />
                                </ValidatorForm> : ''}
                        </div>
                    </CardText>
                </Card>
            </div>
        );
    }
};
