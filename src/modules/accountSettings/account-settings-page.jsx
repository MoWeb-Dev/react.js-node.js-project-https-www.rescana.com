import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AccountSettingsForms from './account-settings-forms.jsx';
import classNames from 'classnames';
import themeForFont from "../app/themes/themeForFont";


class AccountSettingsPage extends React.Component {
    constructor() {
        super();

        this.state = {
            accountSettingsForms: {
                personalInfo: {
                    jobTitle: '',
                    companyName: '',
                    shortBio: ''
                },
                password: {
                    currentPassword: '',
                    newPassword: '',
                    confirmPassword: ''
                },
                name: {
                    firstName: '',
                    lastName: ''
                },
                userPreferencesConfig: {
                    displayScoreBackwards: false,
                    displayIntelTableViewFirst: false,
                    gaugeDisplay: false
                },
                alertSystemConfig: {
                    sendIntelAlerts: false,
                    sendPhishingAlerts: false,
                    excludedCompaniesIntelAlert: []
                },
                twoFactorEnabled: false
            },
            errors: {
                personalInfoForm: {},
                passwordForm: {},
                nameForm: {},
                alarmSystemForm: {}
            },
            user: app.getAuthUser()
        };

        this.validator = {
            personalInfo: {
                isValid: this.personalInfoFormIsValid.bind(this)
            },
            password: {
                expressions: [
                    {
                        regExp: /^(?=\S+$).*$/,
                        message: 'No whitespace allowed in the entire string'
                    },
                    {
                        regExp: /^.{8,}$/,
                        message: 'Password must contains at least 8 characters'
                    },
                    {
                        regExp: /^(?=.*[0-9]).*$/,
                        message: 'A digit must occur at least once'
                    },
                    {
                        regExp: /^(?=.*[a-z]).*$/,
                        message: 'A lower case letter must occur at least once'
                    },
                    {
                        regExp: /^(?=.*[A-Z]).*$/,
                        message: 'An upper case letter must occur at least once'
                    }
                ],
                isValid: this.passwordFormIsValid.bind(this)
            },
            name: {
                isValid: this.nameFormIsValid.bind(this)
            },
            alertSystemConfig: {
                isValid: this.alarmSystemFormIsValid.bind(this)
            },
            userPreferencesConfig: {
                isValid: this.userPreferencesFormIsValid.bind(this)
            }
        };

        this.handleSaveAccountSettings = this.handleSaveAccountSettings.bind(this);
        this.setAccountSettingsState = this.setAccountSettingsState.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    componentDidMount() {
        if (!this.state.user) {
            app.routeAuthUser();
            return;
        }
        this.loadAccountSettingsData();
    }

    loadAccountSettingsData() {
        if (this.state.user.name !== undefined && this.state.user.name !== '') {
            this.state.accountSettingsForms.name = {
                firstName: this.state.user.name.firstName,
                lastName: this.state.user.name.lastName
            };
        }
        if (this.state.user.personalInfo !== undefined && this.state.user.personalInfo !== '') {
            this.state.accountSettingsForms.personalInfo = this.state.user.personalInfo;
        }
        if (this.state.user.alertSystemConfig !== undefined && this.state.user.alertSystemConfig !== '') {
            this.state.accountSettingsForms.alertSystemConfig = this.state.user.alertSystemConfig;
            if (!this.state.user.alertSystemConfig.excludedCompaniesIntelAlert) {
                this.state.accountSettingsForms.alertSystemConfig.excludedCompaniesIntelAlert = [];
            }
        }

        if (this.state.user.userPreferencesConfig != null) {
            this.state.accountSettingsForms.userPreferencesConfig = this.state.user.userPreferencesConfig;
        }

        if (this.state.user.twoFactorEnabled !== undefined) {
            this.state.accountSettingsForms.twoFactorEnabled = this.state.user.twoFactorEnabled;
        }

        this.setState({accountSettingsForms: this.state.accountSettingsForms});
    }

    personalInfoFormIsValid() {
        return true;
    }

    passwordFormIsValid() {
        let formIsValid = true;
        let passwordFormData = this.state.accountSettingsForms.password;

        this.state.errors.passwordForm = {}; // clear any previous errors

        if (!passwordFormData.currentPassword.length) {
            this.state.errors.passwordForm.currentPassword = 'Required';
            formIsValid = false;
        }
        if (!passwordFormData.newPassword.length) {
            this.state.errors.passwordForm.newPassword = 'Required';
            formIsValid = false;
        }
        if (!passwordFormData.confirmPassword.length) {
            this.state.errors.passwordForm.confirmPassword = 'Required';
            formIsValid = false;
        }

        if (formIsValid) {
            let passwordValidators = this.validator.password.expressions;

            for (let i = 0; i < passwordValidators.length; i++) {
                if (!passwordValidators[i].regExp.test(passwordFormData.newPassword)) {
                    formIsValid = false;
                    this.state.errors.passwordForm.newPassword = passwordValidators[i].message;
                    break;
                }
            }
        }

        if (formIsValid) {
            if (passwordFormData.newPassword != passwordFormData.confirmPassword) {
                this.state.errors.passwordForm.confirmPassword = "Passwords don't match";
                formIsValid = false;
            }
        }

        this.setState({errors: this.state.errors});
        return formIsValid;
    }

    nameFormIsValid() {
        let formIsValid = true;
        let nameFormData = this.state.accountSettingsForms.name;

        this.state.errors.nameForm = {}; // clear any previous errors

        if (!nameFormData.firstName.length || nameFormData.firstName.indexOf(' ') >= 0) {
            this.state.errors.nameForm.firstName = 'Required';
            formIsValid = false;
        }

        if (!nameFormData.lastName.length || nameFormData.firstName.indexOf(' ') >= 0) {
            this.state.errors.nameForm.lastName = 'Required';
            formIsValid = false;
        }

        this.setState({errors: this.state.errors});
        return formIsValid;
    }

    alarmSystemFormIsValid() {
        return true;
    }

    userPreferencesFormIsValid() {
        return true;
    }

    handleSaveAccountSettings(formId, event) {
        event.preventDefault();

        if (!this.validator[formId].isValid()) {
            return;
        }

        let data = {
            email: this.state.user.email,
            name: this.state.user.name,
            alertSystemConfig: this.state.user.alertSystemConfig
        };

        if (formId === 'password') {
            data['password'] = this.state.accountSettingsForms['password']['newPassword'];
            data['repeat'] = this.state.accountSettingsForms['password']['confirmPassword'];
        } else {
            data[formId] = this.state.accountSettingsForms[formId];
        }

        $.ajax({
            type: 'POST',
            url: '/aut/update_user',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data.ok) {
                this.state.user[formId] = this.state.accountSettingsForms[formId];
                app.setAuthUser(this.state.user);
                app.initUserDataDisplay();
                app.addAlert('success', 'Changes has been saved successfully!');
            } else {
                app.addAlert('error', 'Error: failed to save changes');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to save changes');
        });
    }

    setAccountSettingsState(formId, event) {
        let fieldName = event.target.name;
        let fieldValue = event.target.value;

        if (event.target.type === 'checkbox') {
            fieldValue = Boolean(event.target.checked);
        } else if (event.target.type === 'radio' && formId === 'userPreferencesConfig') {
            if (fieldName === 'displayScoreBackwards') {
                // This section is for Score Display.
                fieldValue = (fieldValue === 'backwards');
            } else {
                // This section is for Intelligence Display.
                fieldValue = (fieldValue === 'table');
            }
        }

        this.state.accountSettingsForms[formId][fieldName] = fieldValue;

        return this.setState({accountSettingsForms: this.state.accountSettingsForms});
    }

    handleAddRequestedField(chip, allData) {
        let accountSettingsForms = this.state.accountSettingsForms;
        let inputData = accountSettingsForms.alertSystemConfig.excludedCompaniesIntelAlert;
        let input;

        if (!Array.isArray(chip)) {
            input = chip;
        } else {
            input = chip[0];
        }

        // Check if allData contains the input.
        let inputIndex = allData.map((c) => {
            return c.companyName;
        }).indexOf(input.companyName);

        if (inputIndex > -1) {
            let inputObjToAdd = allData[inputIndex];

            // Check if inputDatasw2 not already contains the input.
            if (inputData.indexOf(inputObjToAdd) === -1) {
                // Add to the ChipSelect.
                inputData.push(inputObjToAdd);

                this.setState({accountSettingsForms: accountSettingsForms});
            } else {
                app.addAlert('warning', 'Company already selected.');
            }
        } else {
            app.addAlert('error', 'No such company.');
        }
    }

    handleDeleteRequestedField(value) {
        let accountSettingsForms = this.state.accountSettingsForms;
        let inputData = accountSettingsForms.alertSystemConfig.excludedCompaniesIntelAlert;

        // Check the index of the value of selectedChip in inputData.
        let index = inputData.map((c) => {
            return c.id;
        }).indexOf(value);
        if (index > -1) {
            // Update the ChipSelect.
            inputData.splice(index, 1);

            this.setState({accountSettingsForms: accountSettingsForms});
        }
    }

    render() {
        let pageMainClass = classNames({
            'containerLTR': true
        });

        return (
            <div className={pageMainClass}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <div>
                        <div style={{margin: '53px', marginBottom: '55px', fontSize: 24}}>ACCOUNT</div>
                        <AccountSettingsForms
                            data={this.state.accountSettingsForms}
                            onSave={this.handleSaveAccountSettings}
                            onChange={this.setAccountSettingsState}
                            errors={this.state.errors}
                            handleAddRequestedField={this.handleAddRequestedField.bind(this)}
                            handleDeleteRequestedField={this.handleDeleteRequestedField.bind(this)}/>
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}

export default AccountSettingsPage;
