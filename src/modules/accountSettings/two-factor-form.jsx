import React from 'react';
import Toggle from 'material-ui/Toggle';
import RaisedButton from 'material-ui/RaisedButton';
import {Card, CardText, CardTitle} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import {browserHistory} from 'react-router';
import PasswordChanger from './change-password.jsx';
import Divider from 'material-ui/Divider';

export default class ManageSecurityForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tfaImage: '',
            showChangePasswordField: false,
            code: '',
            user: app.getAuthUser()
        };
    }

    setupTwoFactor() {
        $.ajax({
            type: 'POST',
            url: '/api/setupTwoFactAuth',
            data: {},
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true
        }).done((data) => {
            if (data.ok) {
                this.setState({'tfaImage': data.qr});
            } else {
                app.addAlert('error', data.mfaError);
            }
        }).fail(() => {
            app.addAlert('error', 'Failed to start two factor authentication setup');
        });
    }

    enableTwoFactor(ignore, isInputChecked) {
        $.ajax({
            type: 'POST',
            url: '/api/enableTwoFactor',
            data: JSON.stringify({'enableTwoFactor': isInputChecked}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true
        }).done((data) => {
            if (data.ok) {
                this.setState({'tfaImage': data.qr});
                if (!this.props.setup2fFromLogin) {
                    this.state.user.twoFactorEnabled = isInputChecked;
                    app.setAuthUser(this.state.user);
                }
            } else {
                app.addAlert('error', data.mfaError);
            }
        }).fail(() => {
            app.addAlert('error', 'Failed to enable two factor authentication.');
        });
    }

    verifyTwoFactor() {
        $.ajax({
            type: 'POST',
            url: '/api/verifyTwoFactor',
            data: JSON.stringify({code: this.state.code}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true
        }).done((data) => {
            if (data.ok) {
                if (!this.props.setup2fFromLogin) {
                    this.state.user.twoFactorEnabled = true;
                    this.setState({'tfaImage': ''});
                    app.addAlert('success', 'Account verified and Two Factor Authentication enabled.');
                } else {
                    this.props.login(this.state.code);
                    app.addAlert('success', 'Account verified and Two Factor Authentication enabled.');
                }
            } else {
                app.addAlert('warning', 'Could not verify.');
            }
        }).fail(() => {
            app.addAlert('error', 'Failed to start two factor authentication setup');
        });
    }

    handleChange(e) {
        this.setState({code: e.target.value});
    }

    render() {
        let toggled = this.state.user && this.state.user.twoFactorEnabled;

        return (
            <div>
                <PasswordChanger/>
                <Card style={{margin: 30, padding: '20px 30px 20px 30px' }}>
                    <CardText style={{marginTop: 10,marginBottom: 10, padding: 10, fontSize: 20}}>Two Factor Authentication</CardText>
                    <br/>
                    <Divider/>
                    <CardText>
                        {!this.state.tfaImage ?
                            <div>
                                {!this.props.setup2fFromLogin ?
                                    <Toggle
                                        label={'Enable Two Factor Authentication'}
                                        toggled={toggled}
                                        onToggle={this.enableTwoFactor.bind(this)}
                                    /> : ''}
                                <br/>
                                <span>To set up Two Factor Authentication please download the Google Authenticator App <br/>
                                from the Play Store or App store and press the SET UP button.<br/>
                                </span>
                            </div> : ''}
                        <br/>
                        <br/>
                        {this.state.tfaImage ?
                            <div>
                                <span>
                                    <li>Select "Set up account" in the App.</li>
                                    <li>Choose "Scan a barcode".</li>
                                    <li>Enter the code received in the Google Authenticator app in the field below.</li>
                                </span>
                                <br/>
                                <img
                                    style={!this.props.setup2fFromLogin ? {'marginLeft': '32%'} : {'marginLeft': '18%'}}
                                    src={this.state.tfaImage} alt=''/>
                                <br/>
                                <br/>
                                <div>
                                    <TextField
                                        floatingLabelText="6 Digit Code"
                                        name="6 Digit Code"
                                        onChange={this.handleChange.bind(this)}
                                    />
                                </div>
                                <br/>
                                <br/>
                                <RaisedButton
                                    label="Verify"
                                    primary={true}
                                    onTouchTap={this.verifyTwoFactor.bind(this)}/>
                                <br/>
                            </div> : <RaisedButton
                                label="SET UP"
                                primary={true}

                                onTouchTap={this.setupTwoFactor.bind(this)}/>}
                    </CardText>
                </Card>
            </div>
        );
    }
};
