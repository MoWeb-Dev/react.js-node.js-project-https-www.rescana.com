import React from 'react';
import TextField from 'material-ui/TextField';
import {Tabs, Tab} from 'material-ui/Tabs';
import Checkbox from 'material-ui/Checkbox';
import Form from '../common/form.jsx';
import {Card, CardText, CardTitle} from 'material-ui/Card';
import ManageSecurityForm from './two-factor-form.jsx';
import AutoComplete from 'material-ui/AutoComplete/index';
import ChipInput from 'material-ui-chip-input';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

const styles = {
    headline: {
        fontSize: 20,
        paddingTop: 16,
        marginBottom: 12,
        marginTop: 1,
        fontWeight: 400
    },
    button: {
        margin: 12
    },
    tabContent: {
        width: '50%',
        marginLeft: '23%',
        marginTop: '20px'
    }
};

class AccountSettingsForms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'personalInfo',
            tfaImage: ''
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        this.setState({
            selectedTab: value
        });
    }

    render() {
        return (
            <Tabs
                value={this.state.value}
                onChange={this.handleChange}
            >
                {/* <Tab label="Profile info" value="personalInfo">
                    <div style={styles.tabContent}>
                        <ManagePersonalInformationForm
                            identifier="personalInfo"
                            data={this.props.data.personalInfo}
                            onSubmit={this.props.onSave}
                            onChange={this.props.onChange}/>
                    </div>
                </Tab>*/}
                <Tab label="Settings" value="accountSettings">
                    <div style={styles.tabContent}>
                        {/* <ManagePasswordForm
                            identifier="password"
                            data={this.props.data.password}
                            onSubmit={this.props.onSave}
                            onChange={this.props.onChange}
                            errors={this.props.errors.passwordForm}/>*/}
                        <ManageSettingsForm
                            identifier="name"
                            data={this.props.data}
                            onSubmit={this.props.onSave}
                            onChange={this.props.onChange}
                            errors={this.props.errors.nameForm}/>
                    </div>
                </Tab>
                <Tab label="Alert system" value="alertSystem">
                    <div style={styles.tabContent}>
                        <ManageAlertSystemForm
                            identifier="alertSystemConfig"
                            data={this.props.data.alertSystemConfig}
                            onSubmit={this.props.onSave}
                            onChange={this.props.onChange}
                            handleAddRequestedField={this.props.handleAddRequestedField.bind(this)}
                            handleDeleteRequestedField={this.props.handleDeleteRequestedField.bind(this)}/>
                    </div>
                </Tab>
                <Tab label="Security & Login" value="security">
                    <div style={styles.tabContent}>
                        <ManageSecurityForm
                            identifier="Security"
                            twoFactorEnabled={this.props.data.twoFactorEnabled}
                            onChange={this.props.onChange}/>
                    </div>
                </Tab>
            </Tabs>
        );
    }
}

class ManagePersonalInformationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Card>
                <CardTitle title={'Personal Information'}/>
                <CardText>
                    <Form
                        identifier={this.props.identifier}
                        saveButtonLabel="Save personal info"
                        onSubmit={this.props.onSubmit}>

                        <TextField
                            floatingLabelText="Job Title"
                            name="jobTitle"
                            value={this.props.data.jobTitle}
                            onChange={this.props.onChange.bind(this, this.props.identifier)}
                        />
                        <TextField
                            floatingLabelText="Company Name"
                            name="companyName"
                            value={this.props.data.companyName}
                            onChange={this.props.onChange.bind(this, this.props.identifier)}
                        />
                        <TextField
                            name="shortBio"
                            value={this.props.data.shortBio}
                            floatingLabelText="Short bio"
                            hintText="Tell us about yourself..."
                            multiLine={true}
                            rows={6}
                            rowsMax={20}
                            onChange={this.props.onChange.bind(this, this.props.identifier)}
                        />
                    </Form>
                </CardText>
            </Card>
        );
    }
}

class ManagePasswordForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Card>
                <CardTitle title={'Password'}/>
                <CardText>
                    <Form
                        identifier={this.props.identifier}
                        saveButtonLabel="Update"
                        onSubmit={this.props.onSubmit}>

                        <TextField
                            floatingLabelText="Current Password*"
                            name="currentPassword"
                            value={this.props.data.currentPassword}
                            type="password"
                            onChange={this.props.onChange.bind(this, this.props.identifier)}
                            errorText={this.props.errors.currentPassword}/>
                        <TextField
                            floatingLabelText="New Password*"
                            name="newPassword"
                            value={this.props.data.newPassword}
                            type="password"
                            onChange={this.props.onChange.bind(this, this.props.identifier)}
                            errorText={this.props.errors.newPassword}/>
                        <TextField
                            floatingLabelText="Confirm Password*"
                            name="confirmPassword"
                            value={this.props.data.confirmPassword}
                            type="password"
                            onChange={this.props.onChange.bind(this, this.props.identifier)}
                            errorText={this.props.errors.confirmPassword}/>
                    </Form>
                </CardText>
            </Card>
        );
    }
}

class ManageSettingsForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const styles = {
            alignStyle: {
                marginBottom: 16,
                marginLeft: 10
            }
        };

        const NORMAL_VALUE = 'normal';

        const BACKWARDS_VALUE = 'backwards';

        const GAUGE_VALUE = 'gaugeDisplay';

        const USER_PREFERENCES_FORM = 'userPreferencesConfig';

        const GRAPH_VIEW_FIRST = 'graph';

        const TABLE_VIEW_FIRST = 'table';

        let defaultScorePreference = (this.props.data.userPreferencesConfig.displayScoreBackwards) ?
            BACKWARDS_VALUE : NORMAL_VALUE;

        let defaultIntelViewPreference = (this.props.data.userPreferencesConfig.displayIntelTableViewFirst) ?
            TABLE_VIEW_FIRST : GRAPH_VIEW_FIRST;

        return (<div>
                <Card style={{margin: 30, padding: '20px 30px 0px 30px' }}>
                    <CardText style={{marginTop: 10, padding: 10, fontSize: 20}}>Profile</CardText>
                    <Form
                        identifier={this.props.identifier}
                        saveButtonLabel="Update"
                        onSubmit={this.props.onSubmit}>
                        <TextField
                            floatingLabelText="First Name"
                            name="firstName"
                            value={this.props.data.name.firstName}
                            onChange={this.props.onChange.bind(this, this.props.identifier)}
                            errorText={this.props.errors.firstName}
                            style={styles.alignStyle}/>

                        <TextField
                            floatingLabelText="Last Name"
                            name="lastName"
                            value={this.props.data.name.lastName}
                            onChange={this.props.onChange.bind(this, this.props.identifier)}
                            errorText={this.props.errors.lastName}
                            style={styles.alignStyle}/>

                    </Form>
                </Card>
                <Card style={{margin: 30,  padding: '20px 30px 0px 30px'}}>
                    <CardText style={{marginTop: 10, padding: 10, fontSize: 20}}>User Preferences</CardText>
                    <Form
                        identifier={USER_PREFERENCES_FORM}
                        saveButtonLabel="Save"
                        onSubmit={this.props.onSubmit}>
                        <CardText style={{marginTop: 10, padding: '10px 0px 20px 15px', fontSize: 16}}>Score Display</CardText>
                        <RadioButtonGroup
                            name="displayScoreBackwards"
                            valueSelected={defaultScorePreference}
                            onChange={this.props.onChange.bind(this, USER_PREFERENCES_FORM)}>
                            <RadioButton
                                value={NORMAL_VALUE}
                                label="A lower score means the company security posture is better"
                                style={styles.alignStyle}
                            />
                            <RadioButton
                                value={BACKWARDS_VALUE}
                                label="A higher score means the company security posture is better"
                                style={styles.alignStyle}
                            />

                        </RadioButtonGroup>
                        <CardText style={{marginTop: 10, padding: '10px 0px 20px 15px', fontSize: 16}}>Visual Display</CardText>
                        <span style={{position: "relative", left: 10}}>
                        <Checkbox
                            label="Low, medium and high gauge display"
                            name={GAUGE_VALUE}
                            style={styles.radioButton}
                            checked={this.props.data.userPreferencesConfig.gaugeDisplay}
                            onCheck={this.props.onChange.bind(this, USER_PREFERENCES_FORM)}
                        />
                        </span>
                        <br/>
                    </Form>
                </Card>
            </div>
        );
    }
}

class ManageAlertSystemForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allCompanies: []
        };
    }

    componentDidMount() {
        if (this.state.allCompanies.length === 0) {
            $.ajax({
                type: 'GET',
                url: '/api/getAllCompaniesPartialById',
                async: true,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((companies) => {
                if (companies && Array.isArray(companies)) {
                    // Save only necessary data on each company.
                    companies = companies.map((c) => {
                        return {
                            id: c.id,
                            companyName: c.companyName
                        };
                    });
                    this.setState({allCompanies: companies});
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load companies.');
            });
        }
    }

    render() {
        const autoCompleteStyle = {overflowY: 'auto', height: '200px', maxHeight: '200px'};

        let excludedCompaniesChipStyle = (this.props.data.sendIntelAlerts) ?
            {display: 'inline-block'} : {display: 'none'};

        let allCompanies = this.state.allCompanies;
        const styles = {
            alignStyle: {
                marginBottom: 16,
                marginLeft: 20
            }
        };
        return (
            <Card style={{margin: 30,  padding: '20px 30px 10px 30px'}}>
                <CardText style={{marginTop: 10, padding: 10, fontSize: 20}}>Alerts</CardText>
                <Form
                    identifier={this.props.identifier}
                    saveButtonLabel="Save"
                    onSubmit={this.props.onSubmit}
                    style={{paddingBottom: '20px'}}>

                    <Checkbox
                        label="Send me phishing alerts"
                        name="sendPhishingAlerts"
                        style={styles.radioButton}
                        checked={this.props.data.sendPhishingAlerts}
                        onCheck={this.props.onChange.bind(this, this.props.identifier)}
                    />
                    <br/>
                    <Checkbox
                        label="Send me intelligence alerts"
                        name="sendIntelAlerts"
                        style={styles.radioButton}
                        checked={this.props.data.sendIntelAlerts}
                        onCheck={this.props.onChange.bind(this, this.props.identifier)}
                    />
                    <br/>
                    <ChipInput
                        hintText="Enter companies you wish to exclude from intelligence alerts"
                        floatingLabelText="Excluded companies from intelligence alerts"
                        menuStyle={autoCompleteStyle}
                        filter={AutoComplete.caseInsensitiveFilter}
                        value={this.props.data.excludedCompaniesIntelAlert}
                        dataSource={allCompanies}
                        dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                        onRequestAdd={(chip) => this.props.handleAddRequestedField(chip, allCompanies)}
                        onRequestDelete={(chip) => this.props.handleDeleteRequestedField(chip)}
                        maxSearchResults={50}
                        style={excludedCompaniesChipStyle}
                        fullWidth={true}
                        fullWidthInput={true}
                        openOnFocus={true}
                    />
                </Form>
            </Card>
        );
    }
}

export {
    AccountSettingsForms as default,
    ManagePersonalInformationForm,
    ManagePasswordForm,
    ManageSettingsForm,
    ManageAlertSystemForm
};
