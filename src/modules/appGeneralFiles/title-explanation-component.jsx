import React from 'react';
import InfoIcon from 'material-ui/svg-icons/action/info-outline';
import Popover from 'material-ui/Popover/Popover';
import Paper from 'material-ui/Paper';
import {typography} from "material-ui/styles";
import IconButton from 'material-ui/IconButton';

export default class TitleExplanationComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let styles = {
            infoTextPopover: {
                maxWidth: 600,
                maxHeight: 400,
                overflow: 'auto',
                padding: '20px',
                borderRadius: 3
            },
            text: {
                fontSize: 14,
                fontWeight: typography.fontWeightLight,
            },
            infoIcon: {
                position: 'relative',
                top: '4.7px',
                height: 24,
                width: 24,
                marginLeft: 10
            }
        };

        let infoText = this.props.infoText;
        let shorterInfoText = this.props.shorterInfoText;

        let showInfoIcon = true;
        if(infoText === ''){
            showInfoIcon = false;
        }

        let shortTextStyle = !showInfoIcon ? {position:'relative', bottom: 14, marginLeft: '34px', color: '#626262'} : {marginLeft: '34px', color: '#626262'};


        return (<div style={{position: 'relative', bottom: 44}}>
                {!showInfoIcon ? <br/> : null}
                <span style={shortTextStyle}>{shorterInfoText}</span>
                {showInfoIcon && <IconButton
                    tooltip='Full Explanation'
                    tooltipPosition="bottom-right"
                    style={styles.infoIcon}
                    iconStyle={{position: 'relative', right: 8, bottom: 4}}
                    name="info">
                    <InfoIcon viewBox={'2 2 35 35'} color={'#626262'}
                              onClick={this.props.handleOpenInfoBox}/>
                </IconButton>
                }
                <Popover
                    open={this.props.openInfoBox}
                    anchorEl={this.props.anchorEl}
                    anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    onRequestClose={this.props.handleCloseInfoBox}>
                    <Paper
                        style={styles.infoTextPopover}>
                        <span style={styles.text}>{infoText}</span>
                    </Paper>
                </Popover>
            </div>
        )
    }
}
