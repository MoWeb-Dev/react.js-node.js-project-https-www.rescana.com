

module.exports.ipAndDomain = {
    explShort: 'IPs and Domains related to the company.',
    expl : ''
};

module.exports.ports = {
    explShort: 'Open ports are considered an attack surface. The service that is listing on a port, could be vulnerable to a remotely exploitable vulnerability.',
    expl : 'Open ports are considered an attack surface. The service that is listing on a port, ' +
        'could be vulnerable to a remotely exploitable vulnerability.\n' +
        'An important principle in security is reducing your attack surface, and ensuring that' +
        ' servers have the minimum number of exposed services.'
};

module.exports.buckets = {
    explShort: 'Amazon Simple Storage Service (S3 buckets) is storage service offered by Amazon on the cloud.',
    expl : 'Amazon Simple Storage Service (S3 buckets) ' +
        'is storage service offered by Amazon on the cloud. This storage service became very popular storage ' +
        'alternative. The risk involved with S3 buckets is mainly due to the case in which the storage is configured ' +
        'to be open for public access leading in many cases to leakage of company confidential data. Our automatic scan ' +
        'technology is continuously looking for publicly open S3 buckets with the goal to identify and alert our customers ' +
        'as soon as possible.'
};

//Discovered systems
module.exports.product = {
    explShort: 'External facing services and products connected to the company.',
    expl : ''
};


module.exports.isp = {
    explShort: 'Internet service provider (ISP).',
    expl : 'Knowing which Internet service provider (ISP) or hosting provider is being used is important and has a very big impact on the security of a company.\n' +
        'Badly managed security controls on ISP\'s and Hosting providers can disrupt business activities and may pose a risk to your website.\n' +
        'Even if one website on the server is compromised, other websites on the same server risk infection.'
};

module.exports.cve = {
    explShort: 'Common Vulnerabilities and Exposures (CVE).',
    expl : 'This analysis module is looking for systems that are running vulnerable software. \n' +
        'Hackers look for vulnerable systems open from the Internet in their attempts to compromise the systems.' +
        ' The systems maybe vulnerable for 2 primary reasons a) released patched were not implemented or b) the ' +
        'systems are end of life versions and were not replaced by supported versions.\n' +
        'CVE is well known list of publicly knowns cybersecurity vulnerabilities.\n' +
        ' Once we identify a company asset we search the CVE list to identify if the software running contains' +
        ' any known vulnerabilities. When found we present the vulnerability details. '
};

module.exports.dmarc = {
    explShort: 'Domain based Message Authentication Reporting and Conformance (DMARC).',
    expl : 'Email authentication mechanisms address the challenge of unauthorized use of company email ' +
        'servers for email spoofing often used for phishing attacks. Rescana assess the usage of the two' +
        ' major authentication schemes SPF and DMARC used for email authentication. Domains without authentication' +
        ' records may allow attackers to abuse the domain names and use them for phishing or spamming purposes. '
};

module.exports.spf = {
    explShort: 'Sender Policy Framework (SPF).',
    expl : 'Email authentication mechanisms address the challenge of unauthorized use of company email ' +
        'servers for email spoofing often used for phishing attacks. Rescana assess the usage of the two' +
        ' major authentication schemes SPF and DMARC used for email authentication. Domains without authentication' +
        ' records may allow attackers to abuse the domain names and use them for phishing or spamming purposes. '
};

//Social Engineering
module.exports.brand = {
    explShort: 'This list of domains represents domains that might be used for phishing attacks against the company.',
    expl : 'This list of domains represents domains that might be used for phishing attacks against the company.' +
        ' The websites are collected using permutations of the domain name and are then analysed to show URL,' +
        ' screenshot and HTML resemblance.'
};

module.exports.dataleaks = {
    explShort: 'This section searches for keywords which are related to the company that appear in paste sites such as Pastebin.',
    expl : 'This section searches for keywords which are related to the company that appear in paste sites such as Pastebin. \n' +
        ' Data found could include: code snippets, database dumps, email corresponding, hacking target lists and more.' +
        ' When a page with such data is found it is advised to start a takedown process.'
};

module.exports.blacklists = {
    explShort: 'Blacklists ara Lists that hold suspicious domains, IPs and email addresses that were found' +
        ' to be involved in illegitimate activities or hold inappropriate content.',
    expl : 'Multiple companies and agencies maintain publicly available black lists.' +
        ' There are 4 types of lists: domain, IP, MX and NS blacklists. These lists hold suspicious domains, ' +
        'IPs and email addresses that were found to be involved in illegitimate activities or hold inappropriate content. '
};

module.exports.breaches = {
    explShort: 'Emails that were found in various breaches.',
    expl : 'The following is a list of emails that were found in various breaches.' +
        ' This could also give an indication about the security awareness status in the company.'
};

//Web Privacy Policy
module.exports.gdpr = {
    explShort: 'Web Privacy Policy pages and Cookie Banners are required for GDPR compliance.',
    expl : 'Web Privacy Policy pages and Cookie Banners are required for GDPR compliance.' +
        ' Rescana checks whether they exist on the domains and subdomains related to the company.'
};

module.exports.ssl = {
    explShort: 'The certificate is a key component in establishing secure communication.',
    expl : 'SSL certificates are used to prove ownership of a public key.' +
        ' The certificate holds several fields about the owner, its expiry date and its issuer. ' +
        'The certificate is a key component in establishing secure communication. '
};

module.exports.botnets = {
    explShort: 'Botnets are network of connected devices which are jointly executing malicious payloads.',
    expl : 'Botnets are network of connected devices (many time compromised) which are jointly executing malicious payloads. Botnets often used for: distributed denial of service, steal data, send spam and other malicious activities.' +
        ' We are scanning the internet in an attempt to identify company\'s compromised devices that run under such botnets.'
};

