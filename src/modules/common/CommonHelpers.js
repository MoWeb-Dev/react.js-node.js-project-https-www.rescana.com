const heDecode = require('he').decode;
const FuzzySearch = require('fuzzy-search');
const moment = require('moment');

module.exports.isDomainNotSubdomain = (hostname) => {
    let result;
    const approvedSubdomains = ['', 'www', 'http://', 'https://', 'http://www', 'https://www'];
    if (hostname) {
        const parseDomain = require('parse-domain');
        result = parseDomain(hostname);
    }
    return (result
        && result.tld
        && result.domain
        && approvedSubdomains.includes(result.subdomain));
};

module.exports.clearHTMLTags = (text) => {
    return heDecode(text.replace(/<(?:.|\n)*?>/gm, ''));
};

module.exports.FuzzySearchInTable = (tableColumns, tableData, value) => {
    let result;

    if (tableColumns && Array.isArray(tableColumns)
        && tableColumns.length > 0
        && tableData && Array.isArray(tableData)
        && tableData.length > 0) {
        // Get labels to search for when fuzzy searching.
        const keys = tableColumns.map((el) => {
            return el['key'];
        });
        // Fix none searchable datatable items for
        // FuzzySearch (like Chip elements or buttons).
        for (let i = tableColumns.length - 1; i >= 0; i--) {
            if (tableColumns[i].noneSearchable) {
                keys.splice(i, 1);
            }
        }

        //in case domain/subdomain are link objs we create another column of the domain/subdomain string so we use the search on them
        if (keys && keys.map((key) => {
            return key === 'domain' || key === 'subdomain';
        })) {
            tableData.map((row) => {
                if (row['domain'] && row['domain'].props && row['domain'].props.children) {
                    row['urlStr'] = row['domain'].props.children;

                } else if (row['subdomain'] && row['subdomain'].props && row['subdomain'].props.href) {
                    row['urlStr'] = row['subdomain'].props.href.replace('http://', '');
                }
                if(row['days_remaining'] && Number(row['days_remaining'])){
                    row['days_remaining'] = row['days_remaining'].toString();
                }
            });
            keys.push('urlStr');
        }


        // Fuzzy search in tableData objects in every key.
        const searcher = new FuzzySearch(tableData, keys, {
            caseSensitive: false
        });
        result = searcher.search(value);

        //when finish we remove the column
        result.map((row) => {
            delete row['urlStr'];
        });
    } else {
        result = [];
    }

    return {
        sortedTableData: result,
        fuzzyString: value
    };
};

module.exports.groupLatLan = (locations) => {
    const locationObj = {};
    for (let i = 0; i < locations.length; i++) {
        if (!locationObj[locations[i].ip]) {
            locationObj[locations[i].ip] = {};
        }
        if (locations[i].dis === 'latitude') {
            locationObj[locations[i].ip].latitude = locations[i].value;
        } else if (locations[i].dis === 'longitude') {
            locationObj[locations[i].ip].longitude = locations[i].value;
        }

        if (locations[i].subdomain !== 'none') {
            locationObj[locations[i].ip].domain = locations[i].subdomain;
        } else {
            locationObj[locations[i].ip].domain = locations[i].hostname;
        }
    }
    return locationObj;
};

module.exports.calculateTotalScore = (maxIntelScore, surveyScore, ratios, isSurveyNA) => {
    maxIntelScore = Math.floor(maxIntelScore);
    surveyScore = Math.floor(surveyScore);
    let shouldCalcSurvey = true;
    if (isNaN(surveyScore) || isSurveyNA) {
        shouldCalcSurvey = false;
        surveyScore = 0;
    }
    if (!surveyScore) {
        surveyScore = 0;
    }
    if (!maxIntelScore || isNaN(maxIntelScore)) {
        maxIntelScore = 0;
    }

    if (ratios && (ratios.surveyWeight || ratios.surveyWeight === 0)
        && (ratios.intelWeight || ratios.intelWeight === 0)) {
        maxIntelScore = maxIntelScore * (ratios.intelWeight / 100);
        surveyScore = surveyScore * (ratios.surveyWeight / 100);
    } else if (shouldCalcSurvey) {
        maxIntelScore = maxIntelScore * 0.5;
        surveyScore = surveyScore * 0.5;
    }

    return Math.floor(maxIntelScore + surveyScore);
};

module.exports.chooseColor = (num) => {
    if (num < 33) {
        return '#0091ea';
    }
    if (num >= 33 && num < 66) {
        return '#F57C00';
    }
    if (num >= 66 && num <= 100) {
        return '#d32f2f';
    } else {
        return '#303030';
    }
};

// num must be delivered as a number already.
module.exports.getSingleOrPlural = (num, noun, isCapital = false) => {
    let result;
    if (num && isNumeric(num)) {
        if (num === 1) {
            if (isCapital) {
                result = 'One';
            } else {
                result = 'one';
            }
            if (noun) {
                result += ' ' + noun;
            }
        } else {
            result = num.toString();

            if (noun) {
                result += ' ' + noun + 's';
            }
        }
    } else {
        result = '';
    }
    return result;

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
};

// This function corrects the format of moment object
// as startDate or endDate to the right timezone.
module.exports.fixTimezoneRange = (date, isStartOfRange) => {
    // Fix timezones issues - before changing to UTC,
    // make sure the dates will remain as selected.
    let fixedDate = moment(date).set({
        'hour': parseInt('11', 10),
        'minute': parseInt('59', 10),
        'second': parseInt('59', 10)
    });

    if (isStartOfRange) {
        // Fix timezones issues - after changing to UTC,
        // make sure the dates will be sent to DB as selected.
        fixedDate = moment(fixedDate).utc().set({
            'hour': parseInt('0', 10),
            'minute': parseInt('0', 10),
            'second': parseInt('1', 10)
        });
    } else {
        fixedDate = moment(fixedDate).utc().set({
            'hour': parseInt('23', 10),
            'minute': parseInt('59', 10),
            'second': parseInt('59', 10)
        });
    }
    return fixedDate;
};

// This function returns obj[key], but if key is
// nested(contains a '.') then returns the last nested child.
// Example: ({name: {first: "f", last: 'l'}}, "name.first") => will return "f".
module.exports.getKeyNestedChildsIfExists = (obj, key) => {
    let result;
    // getResult is an anonymous function that will
    // return the result in its relevant type.
    const getResult = (child) => {
        return (child != null) ? child : '';
    };
    if (obj && key != null) {
        const keyNestedChar = '.';
        if (key.includes(keyNestedChar)) {
            const nestedChilds = key.split(keyNestedChar);
            let currChild = obj;
            nestedChilds.map((currChildKey) => {
                currChild = getResult(currChild[currChildKey]);
            });
            result = currChild;
        } else {
            const child = obj[key];
            result = getResult(child);
        }
    } else {
        result = '';
    }
    return result;
};

// This function fixes delete problems with indexes on keys.
module.exports.deleteIndexInArray = (array, idx) => {
    let isDeleted = false;

    if (array.length > idx) {
        const temp = [];

        for (let i = array.length - 1; i > idx; i--) {
            temp.push(array.pop());
        }

        array.pop();

        isDeleted = true;

        for (let i = temp.length; i > 0; i--) {
            array.push(temp.pop());
        }
    }

    return (isDeleted);
};

module.exports.isNumeric = (n) => {
    return !isNaN(parseFloat(n)) && isFinite(n);
};
