import React from 'react';
import Dialog from 'material-ui/Dialog';
import IconButton from 'material-ui/IconButton';
import NavClose from 'material-ui/svg-icons/navigation/close';
import {deleteIndexInArray} from '../../common/CommonHelpers';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import ActionDelete from 'material-ui/svg-icons/action/delete-forever';
import Checkbox from 'material-ui/Checkbox';

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class EditInformationModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            companyInformation: []
        };

        this.checkInformationFieldsValid = this.checkInformationFieldsValid.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let stateObj = {};

        if (nextProps.companyInformation && Array.isArray(nextProps.companyInformation)) {
            // Duplicate the array to avoid reference mistakes.
            stateObj.companyInformation = [];
            nextProps.companyInformation.map((currElement) => {
                stateObj.companyInformation.push(JSON.parse(JSON.stringify(currElement)));
            });
        }
        this.setState(stateObj);
    }

    checkInformationFieldsValid(companyInformation) {
        let isValid = true;

        if (companyInformation && Array.isArray(companyInformation)) {
            companyInformation.map((currData) => {
                if (currData && !currData.label) {
                    isValid = false;
                }
            });
        }
        return isValid;
    }

    checkSaveChanges(companyInformation) {
        if (this.checkInformationFieldsValid(companyInformation)) {
            this.props.saveChanges(companyInformation);
        } else {
            app.addAlert('error', 'Some information has no data');
        }
    }

    handleChangeInformationCheckboxField(index, e) {
            e.preventDefault();

        let stateObjToUpdate = {
            companyInformation: this.state.companyInformation
        };

        if (stateObjToUpdate.companyInformation.length > index) {
            stateObjToUpdate.companyInformation[index].checked = e.target.checked;

            this.setState(stateObjToUpdate);
        }
    }

    handleChangeInformationLabelField(index, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        let stateObjToUpdate = {
            companyInformation: this.state.companyInformation
        };

        if (stateObjToUpdate.companyInformation.length > index - 1) {
            stateObjToUpdate.companyInformation[index].label = e.target.value;

            this.setState(stateObjToUpdate);
        }
    }

    handleDeleteInformation(index, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        let stateObjToUpdate = {
            companyInformation: this.state.companyInformation
        };

        if (stateObjToUpdate.companyInformation.length > index - 1
            && deleteIndexInArray(stateObjToUpdate.companyInformation, index)) {
            this.setState(stateObjToUpdate);
        }
    }

    getDataToDisplay(data) {
        if (data && Array.isArray(data) && data.length > 0) {
            const styles = {
                companyInfoTextField: {
                    'width': '80%'
                },
                infoHintCheckbox: {
                    cursor: 'text'
                },
                infoHintCheckboxLabel: {
                    width: '100%'
                },
                editCheckbox: {
                    display: 'inline-block',
                    width: 'auto',
                    top:10
                },
                editInfoTextField: {
                    width: '75%'
                }
            };

            return data.map((currData, currIndex) => {
                if (currData && currData.hasOwnProperty('checked') && currData.hasOwnProperty('label')) {
                    return <div key={currIndex}>
                        <Checkbox
                            checked={currData.checked}
                            onCheck={this.handleChangeInformationCheckboxField.bind(this, currIndex)}
                            style={styles.editCheckbox}
                        />
                        &nbsp;&nbsp;
                        <TextField
                            floatingLabelText="Company Information"
                            hintText={<Checkbox
                                label="Example: This company signed an NDA"
                                checked={true}
                                disabled={true}
                                style={styles.infoHintCheckbox}
                                labelStyle={styles.infoHintCheckboxLabel}
                            />}
                            onChange={this.handleChangeInformationLabelField.bind(this, currIndex)}
                            value={currData.label}
                            style={styles.editInfoTextField}
                        />
                        &nbsp;&nbsp;
                        <FlatButton
                            label="Delete"
                            icon={<ActionDelete/>}
                            onClick={this.handleDeleteInformation.bind(this, currIndex)}
                            style={{left: 25,bottom: 8, color: 'white', borderRadius: 6, height: 40}}
                            backgroundColor={'rgba(0,0,0,0.7'}
                            hoverColor={'#0091ea'}
                        />
                    </div>;
                } else {
                    return null;
                }
            });
        } else {
            return null;
        }
    }

    render() {
        if (this.props) {
            const topHeaderStyle = {display: 'flex'};

            let dialogTitle = 'Edit Company Information';

            let dialogTitleElement = <div style={topHeaderStyle}>
                <h5 style={{textAlign: 'center',width: '100%'}}>{dialogTitle}</h5>
                <IconButton
                    tooltip="close"
                    touch={true}
                    tooltipPosition="bottom-left"
                    iconStyle={{color: "'white"}}

                    onClick={this.props.closeDialog}>
                    <NavClose/>
                </IconButton>
            </div>;

            let actions = [
                <FlatButton
                    label="Save"
                    primary={true}
                    keyboardFocused={true}
                    style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                    backgroundColor={'#0091ea'}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    onTouchTap={this.checkSaveChanges.bind(this, this.state.companyInformation)}
                />,
                <FlatButton
                    label="Cancel"
                    primary={true}
                    keyboardFocused={true}
                    backgroundColor={'#0091ea'}
                    style={{color: 'white', marginBottom: '5px', height: 40}}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    onTouchTap={this.props.closeDialog}
                />
            ];

            let companyInformation = this.getDataToDisplay(this.state.companyInformation);

            return (
                <Dialog
                    title={dialogTitleElement}
                    contentStyle={{maxWidth: '60%', borderRadius: '7px 7px 7px 7px'}}
                    titleStyle={{
                        fontSize: 18,
                        background: 'rgba(0,0,0,0.7)',
                        color: 'white',
                        borderRadius: '2px 2px 0px 0px',
                        textTransform: 'uppercase',
                    }}
                    actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                    bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                    actions={actions}
                    modal={false}
                    open={this.props.open}
                    onRequestClose={this.props.closeDialog}
                    autoScrollBodyContent={true}>
                    <div>
                        <br/>
                        <br/>
                        {companyInformation}
                        <br/>
                        <br/>
                    </div>
                </Dialog>
            );
        }
    }
};
