import React, {PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import IconButton from 'material-ui/IconButton';
import NavClose from 'material-ui/svg-icons/navigation/close';
import {YES_NO_MODAL_TYPES} from '../../../../config/consts';
import FlatButton from 'material-ui/FlatButton';

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class YesNoModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props) {
            const {title, message, paramObject} = this.props;
            const topHeaderStyle = {display: 'flex'};
            const headerStyle = {textAlign: 'center', width: '100%'};
            const dataDivStyle = {padding: '25px'};
            const paragraphStyle = {fontSize: '18px', whiteSpace: 'pre-wrap'};

            const dialogTitleElement = <div style={topHeaderStyle}>
                <h4 style={headerStyle}>{title}</h4>
                <IconButton
                    tooltip="close"
                    touch={true}
                    tooltipPosition="bottom-left"
                    onClick={this.props.getUserResult.bind(this, YES_NO_MODAL_TYPES.CANCEL, paramObject)}>
                    <NavClose/>
                </IconButton>
            </div>;

            const actions = [
                <FlatButton
                    label="Save"
                    primary={true}
                    keyboardFocused={true}
                    onTouchTap={this.props.getUserResult.bind(this, YES_NO_MODAL_TYPES.OK, paramObject)}
                />,
                <FlatButton
                    label="Discard"
                    primary={true}
                    keyboardFocused={true}
                    onTouchTap={this.props.getUserResult.bind(this, YES_NO_MODAL_TYPES.DISCARD, paramObject)}
                />
            ];

            return (
                <Dialog
                    title={dialogTitleElement}
                    actions={actions}
                    modal={false}
                    open={this.props.open}
                    onRequestClose={this.props.getUserResult.bind(this, YES_NO_MODAL_TYPES.CANCEL, paramObject)}
                    autoScrollBodyContent={true}
                >
                    <div style={dataDivStyle}>
                        <p style={paragraphStyle}>
                            {message}
                        </p>
                    </div>
                </Dialog>
            );
        }
    }
};

YesNoModal.propTypes = {
    open: PropTypes.bool.isRequired,
    getUserResult: PropTypes.func.isRequired,
    message: PropTypes.string.isRequired,
    title: PropTypes.string,
    paramObject: PropTypes.object
};
