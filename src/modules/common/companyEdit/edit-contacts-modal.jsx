import React from 'react';
import Dialog from 'material-ui/Dialog';
import IconButton from 'material-ui/IconButton';
import NavClose from 'material-ui/svg-icons/navigation/close';
import {COMPANY_CONTACT_TYPES} from '../../../../config/consts';
import {deleteIndexInArray} from '../../common/CommonHelpers';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import ActionDelete from 'material-ui/svg-icons/action/delete-forever';
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class EditContactsModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            companyResponsibles: [],
            companyContacts: []
        };

        this.addContactField = this.addContactField.bind(this);
        this.checkContactFieldsValid = this.checkContactFieldsValid.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let stateObj = {};

        if (nextProps.companyResponsibles && Array.isArray(nextProps.companyResponsibles)) {
            // Duplicate the array to avoid reference mistakes.
            stateObj.companyResponsibles = [].concat(nextProps.companyResponsibles);
        }

        if (nextProps.companyContacts && Array.isArray(nextProps.companyContacts)) {
            // Duplicate the array to avoid reference mistakes.
            stateObj.companyContacts = [].concat(nextProps.companyContacts);
        }

        this.setState(stateObj);
    }

    addContactField(contactType, contactIndex, contactField, value) {
        let stateFieldToAdd;

        if (contactType === COMPANY_CONTACT_TYPES.RESPONSIBLE) {
            stateFieldToAdd = 'companyResponsibles';
        } else {
            // this means type is regular 'CONTACT'.
            stateFieldToAdd = 'companyContacts';
        }
        if (this.state.hasOwnProperty(stateFieldToAdd) && this.state[stateFieldToAdd].length > contactIndex) {
            let fieldToUpdate = this.state[stateFieldToAdd];
            fieldToUpdate[contactIndex][contactField] = value;
            let stateToUpdate = {};
            stateToUpdate[stateFieldToAdd] = fieldToUpdate;
            this.setState(stateToUpdate);
        }
    }

    checkContactFieldsValid(companyResponsibles, companyContacts) {
        let isValid = true;

        let isSomeDataMissing = (currContact) => {
            if (!currContact || (!currContact.email && !currContact.name && !currContact.phone && !currContact.position)) {
                isValid = false;
            }
        };

        companyResponsibles.map(isSomeDataMissing);
        companyContacts.map(isSomeDataMissing);

        return isValid;
    }

    checkSaveChanges(companyResponsibles, companyContacts) {
        if (this.checkContactFieldsValid(companyResponsibles, companyContacts)) {
            this.props.saveChanges(companyResponsibles, companyContacts);
        } else {
            app.addAlert('error', 'Some contacts has no data');
        }
    }

    handleChangeContactEmailField(dataType, index, e) {
        this.addContactField(dataType, index, 'email', e.target.value);
    }
    handleChangeContactNameField(dataType, index, e) {
        this.addContactField(dataType, index, 'name', e.target.value);
    }
    handleChangeContactPhoneField(dataType, index, e) {
        this.addContactField(dataType, index, 'phone', e.target.value);
    }
    handleChangeContactPositionField(dataType, index, e) {
        this.addContactField(dataType, index, 'position', e.target.value);
    }

    handleDeleteContact(dataType, index) {
        let stateFieldToRemove;

        if (dataType === COMPANY_CONTACT_TYPES.RESPONSIBLE) {
            stateFieldToRemove = 'companyResponsibles';
        } else {
            // this means type is regular 'CONTACT'.
            stateFieldToRemove = 'companyContacts';
        }

        if (deleteIndexInArray(this.state[stateFieldToRemove], index)) {
            // Force a rerender to show results.
            this.forceUpdate();
        }
    }

    getDataToDisplay(data, dataType) {
        if (data && Array.isArray(data) && data.length > 0) {
            const companyInfoTextField = {
                'width': '20%'
            };

            return data.map((currData, currIndex) => {
                return <div key={currIndex}>
                    <ValidatorForm
                        ref="form"
                        onSubmit={this.handleDeleteContact.bind(this, dataType, currIndex)}
                        onError={errors => console.log(errors)}>
                        <TextValidator
                            floatingLabelText="Email"
                            hintText="Enter Email"
                            name={"email"}
                            validators={['required', 'isEmail']}
                            errorMessages={['this field is required', 'email is not valid']}
                            onChange={this.handleChangeContactEmailField.bind(this, dataType, currIndex)}
                            value={currData.email || ""}
                            style={companyInfoTextField}
                        />
                        &nbsp;&nbsp;
                        <TextValidator
                            floatingLabelText="Name"
                            hintText="Enter name"
                            name={"name"}
                            validators={['required']}
                            errorMessages={['this field is required']}
                            onChange={this.handleChangeContactNameField.bind(this, dataType, currIndex)}
                            value={currData.name || ""}
                            style={companyInfoTextField}
                        />
                        &nbsp;&nbsp;
                        <TextValidator
                            floatingLabelText="Phone"
                            hintText="Enter phone"
                            name={"phone"}
                            validators={['isNumber']}
                            errorMessages={['numeric characters only']}
                            onChange={this.handleChangeContactPhoneField.bind(this, dataType, currIndex)}
                            value={currData.phone || ""}
                            style={companyInfoTextField}
                        />
                        &nbsp;&nbsp;
                        <TextValidator
                            floatingLabelText="Position"
                            hintText="Enter position"
                            name={"position"}
                            onChange={this.handleChangeContactPositionField.bind(this, dataType, currIndex)}
                            value={currData.position || ""}
                            style={companyInfoTextField}
                        />
                        <FlatButton
                            label="Delete"
                            type={"submit"}
                            icon={<ActionDelete/>}
                            //onClick={this.handleDeleteContact.bind(this, dataType, currIndex)}
                            style={{left: 25,bottom: 8, color: 'white', borderRadius: 6, height: 40}}
                            backgroundColor={'rgba(0,0,0,0.7'}
                            hoverColor={'#0091ea'}
                        />
                        <br />
                        <br />
                    </ValidatorForm>
                </div>;
            });
        } else {
            return null;
        }
    }

    render() {
        if (this.props) {
            const topHeaderStyle = {display: 'flex'};
            const dataDivStyle = {position: 'relative' ,left: 60, display: 'inline-grid'};

            let dialogTitle = this.props.modalTitle;

            let dialogTitleElement = <div style={topHeaderStyle}>
                <h5 style={{textAlign: 'center',width: '100%'}}>{dialogTitle}</h5>
                <IconButton
                    tooltip="close"
                    touch={true}
                    tooltipPosition="bottom-left"
                    iconStyle={{color: "'white"}}
                    onClick={this.props.closeDialog}>
                    <NavClose/>
                </IconButton>
            </div>;

            let actions = [
                <FlatButton
                    label="Save"
                    primary={true}
                    style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                    backgroundColor={'#0091ea'}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    onTouchTap={this.checkSaveChanges.bind(this, this.state.companyResponsibles, this.state.companyContacts)}
                />,
                <FlatButton
                    label="Cancel"
                    primary={true}
                    backgroundColor={'#0091ea'}
                    style={{color: 'white', marginBottom: '5px', height: 40}}
                    keyboardFocused={true}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    onTouchTap={this.props.closeDialog}
                />
            ];
            let companyResponsibles = this.getDataToDisplay(this.state.companyResponsibles, COMPANY_CONTACT_TYPES.RESPONSIBLE);

            let companyContacts = this.getDataToDisplay(this.state.companyContacts, COMPANY_CONTACT_TYPES.CONTACT);

            return (
                <Dialog
                    title={dialogTitleElement}
                    modal={false}
                    contentStyle={{maxWidth: '60%', borderRadius: '7px 7px 7px 7px'}}
                    titleStyle={{
                        fontSize: 18,
                        background: 'rgba(0,0,0,0.7)',
                        color: 'white',
                        borderRadius: '2px 2px 0px 0px',
                        textTransform: 'uppercase',
                    }}
                    actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                    bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                    actions={actions}
                    open={this.props.open}
                    onRequestClose={this.props.closeDialog}
                    autoScrollBodyContent={true}>
                    <div style={dataDivStyle}>
                        <br/>
                        <br/>
                        {companyResponsibles}
                        {companyContacts}
                        <br/>
                        <br/>
                    </div>
                </Dialog>
            );
        }
    }
};
