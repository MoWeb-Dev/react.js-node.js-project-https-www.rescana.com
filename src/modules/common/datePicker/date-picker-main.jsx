import React, {PropTypes} from 'react';
import FlatButton from 'material-ui/FlatButton';
import Popover from 'material-ui/Popover';
import DatePicker from '../date-picker.jsx';
import IconButton from 'material-ui/IconButton';
import ActionDates from 'material-ui/svg-icons/action/date-range';
import '../../../../public/css/main.css';
import {fixTimezoneRange} from '../CommonHelpers';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";

const moment = require('moment');
const DATE_FORMAT = 'LL';

export class DatePickerMain extends React.Component {
    constructor() {
        super();

        const today = moment().format(DATE_FORMAT);
        const datesRangeSeparator = ' - ';

        this.state = {
            startDate: undefined,
            endDate: undefined,
            datesRange: today + datesRangeSeparator + today,
            datesRangeSeparator: datesRangeSeparator
        };
    }

    clearDatesRange() {
        this.setState({datesRange: ''});
    }

    componentWillMount() {
        app.setFlagVisibile(false);

        if (this.props && this.props.disableDefaultDates) {
            this.state.datesRange = '';
        }
    }

    openDateFilters(event) {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            openMenu: true,
            anchorEl: event.currentTarget
        });
    }

    handleCloseMenu() {
        this.setState({
            startDate: undefined,
            endDate: undefined,
            openMenu: false
        });
    }

    handleSelect(range) {
        // An object with two keys,
        // 'startDate' and 'endDate' which are Momentjs objects.
        this.setState({startDate: range.startDate, endDate: range.endDate});
    }

    startFilter() {
        let startD = this.state.startDate;
        let endD = this.state.endDate;

        if (startD && endD) {
            // Fix timezones issues.
            startD = fixTimezoneRange(startD, true);
            endD = fixTimezoneRange(endD, false);

            this.setState({
                datesRange: startD.format(DATE_FORMAT) + this.state.datesRangeSeparator + endD.format(DATE_FORMAT),
                openMenu: false,
                startDate: undefined,
                endDate: undefined
            }, () => {
                if (this.props.filterDataByDates) {
                    this.props.filterDataByDates(startD.utc().format(), endD.utc().format());
                }
            });
        } else {
            app.addAlert('warning', 'Please select a date range.');
        }
    }

    render() {
        const datePickerStyle = {
            minHeight: '40px',
            margin: 'auto',
            padding: '10px',
            display: 'inline-flex',
            alignItems: 'center'
        };

        let buttonTooltip;

        if (this.props.buttonTooltip) {
            buttonTooltip = this.props.buttonTooltip;
        } else {
            buttonTooltip = 'Filter by dates';
        }

        let datesRangeToDisplay;

        if (!this.state.datesRange
            && this.props.replaceDefaultDates
            && this.props.replaceDefaultDates.startDate
            && this.props.replaceDefaultDates.endDate) {
            datesRangeToDisplay = moment.utc(this.props.replaceDefaultDates.startDate).format(DATE_FORMAT)
                + this.state.datesRangeSeparator
                + moment.utc(this.props.replaceDefaultDates.endDate).format(DATE_FORMAT);
        } else {
            datesRangeToDisplay = this.state.datesRange;
        }

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div style={datePickerStyle}>
                    <IconButton
                        tooltip={buttonTooltip}
                        touch={true}
                        tooltipPosition="bottom-right"
                        onTouchTap={this.openDateFilters.bind(this)}>
                        <ActionDates/>
                    </IconButton>
                    <label>{datesRangeToDisplay}</label>
                    <Popover
                        open={this.state.openMenu}
                        anchorEl={this.state.anchorEl}
                        anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                        targetOrigin={{horizontal: 'left', vertical: 'top'}}
                        onRequestClose={this.handleCloseMenu.bind(this)}>
                        <div>
                            <DatePicker
                                onChange={this.handleSelect.bind(this)}
                            />
                            <div style={{float: 'right', margin: '5px'}}>
                                <FlatButton
                                    label="Cancel" s
                                    econdary={true}
                                    onTouchTap={this.handleCloseMenu.bind(this)}/>
                                <FlatButton label="Go" secondary={true} onTouchTap={() => this.startFilter()}/>
                            </div>
                        </div>
                    </Popover>
                </div>
            </MuiThemeProvider>
        );
    }
}

DatePickerMain.propTypes = {
    filterDataByDates: PropTypes.func.isRequired,
    buttonTooltip: PropTypes.string,
    disableDefaultDates: PropTypes.bool,
    replaceDefaultDates: PropTypes.object
};

export default DatePickerMain;
