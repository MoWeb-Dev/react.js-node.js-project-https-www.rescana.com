import React from 'react';
import classNames from 'classnames';
import {browserHistory} from 'react-router';

class BreadCrumbs extends React.Component {
    constructor() {
        super();

        this.state = {};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.pathArr && nextProps.pathArr.length > 0) {
            this.setState({pathArr: nextProps.pathArr});
        }

        this.setState({show: nextProps.show});
    }

    handleMainDashboardLink() {
        browserHistory.push('/dashboard');
    }

    render() {
        const breadcrumbHover = classNames({
            'breadcrumbHover': true
        });

        const style = {
            breadcrumbs: {
                width: 'fit-content',
                margin: '25px 5px 10px',
                fontSize: 16,
                cursor: 'default',
                display: this.state.show ? 'block' : 'none'
            },
            breadcrumbHover: {
                padding: '1px 1px 4px 4px',
                borderRadius: '2px',
                cursor: 'pointer'
            }
        };
        let path = '';
        let dashboard = <span
            style={style.breadcrumbHover}
            className={breadcrumbHover}
            onClick={this.handleMainDashboardLink.bind(this)}
        > Dashboard </span>;

        if (this.state.pathArr) {
            path = this.state.pathArr.map((pathObj, i) => {
                return <span key={i}><span style={{paddingRight: '0px'}}> / </span> {pathObj} </span>;
            });
        }

        return (
            <div style={style.breadcrumbs}>
                {dashboard}
                {path}
            </div>
        );
    }
}

export default BreadCrumbs;
