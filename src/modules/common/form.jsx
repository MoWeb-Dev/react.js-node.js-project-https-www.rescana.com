import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
    headline: {
        fontSize: 20,
        paddingTop: 16,
        marginBottom: 12,
        marginTop: 1,
        fontWeight: 400
    },
    button: {
        margin: 12
    }
};

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let fields = this.props.children.map((field, i) => {
            return (
                <div key={i}>
                    {field}
                </div>
            );
        });

        let formStyle = (this.props.style) ? this.props.style : {paddingBottom: 25};

        return (
            <div style={formStyle}>
                <h2 style={styles.headline}>{this.props.title}</h2>
                <hr/>
                <form onSubmit={this.props.onSubmit.bind(this, this.props.identifier)}>
                    {fields}
                    <RaisedButton
                        type="submit"
                        label={this.props.saveButtonLabel}
                        primary={true}
                        style={styles.button}/>
                </form>
            </div>
        );
    }
}

export default Form;
