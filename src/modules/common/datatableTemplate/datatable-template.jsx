import {Card} from 'material-ui/Card';
import DataTables from 'material-ui-datatables';
import React from 'react';
import {CSVLink} from 'react-csv';
import FileDownloadIcon from 'material-ui/svg-icons/file/file-download';
import {
    BOTNETS_TYPE,
    SSL_CERTS_TYPE,
    WIFI_NETWORKS_TYPE,
    INFORMATION_BOX_TYPE,
    DISCOVERED_DOMAINS_TYPE,
    OVERVIEW_TYPE,
    GDPR_NOT_COMPLY_TYPE, ORGANIZATION_ID, ORGANIZATION_NAME, BLACKLISTS_TYPE, DATALEAKS_TYPE, EMAIL_BREACHES_TYPE
} from '../../../../config/consts';
import {FuzzySearchInTable, isNumeric} from '../CommonHelpers.js';
import DataTableTemplateInfoModal from './datatable-template-info.jsx';
import PropTypes from 'prop-types';
import {browserHistory} from "react-router";
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import SortIcon from 'material-ui/svg-icons/av/sort-by-alpha';
import DropDownMenu from 'material-ui/DropDownMenu';
import MitigatedIcon from 'material-ui/svg-icons/action/done';
import CommentIcon from 'material-ui/svg-icons/communication/comment';
import Badge from 'material-ui/Badge';
import AssetUserInputCommentsDialog from '../../generalComponents/comments-dialog.jsx';
import DynamicDialog from '../../app/dynamic-dialog.jsx';
import themeForFont from "../../app/themes/themeForFont.js";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ReactTooltip from 'react-tooltip';

let searchDelayTimer;

// Note: This component works right now only with botnets & sslCerts & Overview & contactsBox & informationBox & discoveredDomains & admin-manage-tables.
export default class DataTableViewTemplate extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tableColumns: [],
            tableData: [],
            sortedTableData: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            page: 1,
            searchQuery: '',
            sortMethod: 'default',
            openExtraInfo: false,
            isFindingStatusDialogOpen: false,
            isCommentsDialogOpen: false,
            curCommentsArr: [],
            findingCurType: ''
        };

        this.handleSortOrderChange = this.handleSortOrderChange.bind(this);
        this.handleFilterValueChange = this.handleFilterValueChange.bind(this);
        this.handleNextPageClick = this.handleNextPageClick.bind(this);
        this.handlePreviousPageClick = this.handlePreviousPageClick.bind(this);
        this.handleRowSizeChange = this.handleRowSizeChange.bind(this);
        this.getImportanceComponent = this.getImportanceComponent.bind(this);
        this.getCommentsComponent = this.getCommentsComponent.bind(this);
        this.getMitigatedComponent = this.getMitigatedComponent.bind(this);
        this.doAfterSetResult = this.doAfterSetResult.bind(this);
    }

    handleUpdateCommentsArea(curCommentsArr) {
        let curRowData = this.state.curRowData;
        let orgId = localStorage.getItem(ORGANIZATION_ID);
        if (curRowData && orgId && curCommentsArr && Array.isArray(curCommentsArr)) {
            curRowData.comments = curCommentsArr;
            this.setState({curRowData: curRowData});
            this.updateFindingStatus(true);
            this.setState({curCommentsArr: curCommentsArr});
        } else {
            app.addAlert('error', 'Error: failed to update Comments Area');
        }
    }

    handleCloseCommentsDialog() {
        this.setState({isCommentsDialogOpen: false, isAssetUserInputOrMitigatedBtnClicked: false});
    }

    onTapStatusDropdown() {
        this.setState({isAssetUserInputOrMitigatedBtnClicked: true});
    }
    onDropdownClose() {
        this.setState({isAssetUserInputOrMitigatedBtnClicked: false});
    }

    onChangeStatusDropdown(curRowData, type, event, index, value) {
        this.setState({isFindingStatusDialogOpen: true});
        setTimeout(() => {
            curRowData.importance = value;

            if(this.props.myDataType === BOTNETS_TYPE ||
                this.props.myDataType === GDPR_NOT_COMPLY_TYPE ||
                this.props.myDataType === SSL_CERTS_TYPE){
                if(curRowData && curRowData.id){
                    curRowData.identifier = curRowData.id;
                }
            }

            this.setState({findingCurType: 'importance', curRowData: curRowData, findingStatusType: type});
        }, 1);
    }

    HandleMitigatedMark(curRowData) {
        //We use this to avoid cell click that redirect us to graph page
        this.setState({isAssetUserInputOrMitigatedBtnClicked: true, isFindingStatusDialogOpen: true});
        let isPropOfCurNodeExists = this.checkIfPropOfCurNodeExists(curRowData, 'Mitigated');
        let findingCurType = '';
        let isStatusMitigatedActive = isPropOfCurNodeExists && curRowData.Mitigated && curRowData.Mitigated.props &&
            curRowData.Mitigated.props.iconStyle && curRowData.Mitigated.props.iconStyle.color &&
            curRowData.Mitigated.props.iconStyle.color !== "black";

        if (isStatusMitigatedActive) {
            findingCurType = 'status from mitigated to default';
        } else {
            findingCurType = 'status to mitigated';
        }

        if(this.props.myDataType === BOTNETS_TYPE ||
            this.props.myDataType === GDPR_NOT_COMPLY_TYPE ||
            this.props.myDataType === SSL_CERTS_TYPE){
            if(curRowData && curRowData.id){
                curRowData.identifier = curRowData.id;
            }
        }

        this.setState({
            findingCurType: findingCurType,
            curRowData: curRowData,
            isStatusMitigatedActive: isStatusMitigatedActive,
            findingStatusType: 'mitigated'});
    }

    handleOpenCommentsDialog(curRowData, isFromMitigated = false) {
        let comments = [];
        if(curRowData.Comments &&
            curRowData.Comments.props &&
            curRowData.Comments.props.children &&
            curRowData.Comments.props.children[1] &&
            curRowData.Comments.props.children[1].props &&
            curRowData.Comments.props.children[1].props.children){
            comments = curRowData.Comments.props.children[1].props.children;
        }
        if(this.props.myDataType === BOTNETS_TYPE ||
            this.props.myDataType === GDPR_NOT_COMPLY_TYPE ||
            this.props.myDataType === SSL_CERTS_TYPE){
            if(curRowData && curRowData.id){
                curRowData.identifier = curRowData.id;
            }
        }
        this.setState({
            isFromMitigated: isFromMitigated,
            isAssetUserInputOrMitigatedBtnClicked: true,
            isCommentsDialogOpen: true,
            curCommentsArr: comments,
            curRowData: curRowData,
            findingStatusType: 'assetUserInput'});
    }

     getImportanceComponent(tableDataObj, importance = 3) {
        return <DropDownMenu
            value={Number(importance)}
            onTouchTap={this.onTapStatusDropdown.bind(this)}
            onClose={this.onDropdownClose.bind(this)}
            onChange={this.onChangeStatusDropdown.bind(this, tableDataObj, 'assetUserInput')}
            autoWidth={false}
            labelStyle={{left: 10, fontSize: 13}}
            style={{width: 170}}
            menuStyle={{borderRadius: 4}}
            menuItemStyle={{fontSize: 13}}
            anchorOrigin={{
                horizontal: 'left',
                vertical: 'bottom'
            }}>
            <MenuItem value={1} primaryText="False Positive"/>
            <MenuItem value={2} primaryText="Low"/>
            <MenuItem value={3} primaryText="Medium"/>
            <MenuItem value={4} primaryText="High"/>
            <MenuItem value={5} primaryText="Critical"/>
        </DropDownMenu>
    }


    getCommentsComponent(tableDataObj, commentsCount = 0, comments = [], badgeColor = '#82abea') {
        return <div style={{paddingLeft: 30}}><Badge badgeContent={commentsCount} secondary={true} style={{marginLeft: -30, marginRight: -30}}
                                                     badgeStyle={{background: badgeColor, top: 17, right: 17}}>
            <IconButton data-tip={'Comments'}
                        data-for={'Comments'}
                        onTouchTap={this.handleOpenCommentsDialog.bind(this, tableDataObj, false)}>
                <CommentIcon style={{width: 5, height: 5}}/>
                <ReactTooltip id='Comments' place="right" effect="solid"/>
            </IconButton>
        </Badge>
            <span style={{display: 'none', visibility: 'hidden'}}>{comments}</span>
        </div>
    }

    getMitigatedComponent(tableDataObj, mitigatedBackground = 'black') {
        return <IconButton data-tip={'Mark as Mitigated'}
                           data-for={'mitigated'}
                                       iconStyle={{ width: 30, height: 30,color: mitigatedBackground, position: 'relative',left:6, top: 2}}
                                       onTouchTap={this.HandleMitigatedMark.bind(this, tableDataObj)}>
            <ReactTooltip id='mitigated' place="right" effect="solid"/>
            <MitigatedIcon viewBox={'3 3 35 35'}/>
        </IconButton>
    }

    doAfterSetResult() {
        if (this.props.assetUserInputAndMitigatedData && Array.isArray(this.props.assetUserInputAndMitigatedData)) {
            this.setState({assetUserInputAndMitigatedData: this.props.assetUserInputAndMitigatedData});
        }
    }

    closeFindingDialog() {
        this.setState({isFindingStatusDialogOpen: false, isAssetUserInputOrMitigatedBtnClicked: false});
    }

    handleSortOrderChange(key, order) {
        this.setState({orderKey: key, orderType: order});
        let tableData = this.state.tableData;
        let textA;
        let textB;

        let getSortResult;
        if (order === 'asc') {
            getSortResult = (textA, textB) => {
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            };
        } else {
            getSortResult = (textA, textB) => {
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            };
        }

        tableData.sort((a, b) => {
            let keyA, keyB;

            //using color the order is fp -> mitigated -> default or backwards
            if(key === 'Importance'){
                if(a[key] && a[key].props && a[key].props.value) {
                    if(a[key].props.value === 4){
                        keyA = 4;
                    } else if(a[key].props.value === 3){
                        keyA = 3;
                    } else if(a[key].props.value === 2){
                        keyA = 2;
                    } else if(a[key].props.value === 1){
                        keyA = 1;
                    } else {
                        keyA = 0;
                    }
                }

                if(b[key] && b[key].props && b[key].props.value) {
                    if(b[key].props.value === 4){
                        keyB = 4;
                    } else if(b[key].props.value === 3){
                        keyB = 3;
                    } else if(b[key].props.value === 2){
                        keyB = 2;
                    } else if(b[key].props.value === 1){
                        keyB = 1;
                    } else {
                        keyB = 0;
                    }
                }
            } else if(key === 'Mitigated'){
                if(a[key] && a[key].props && a[key].props.iconStyle && a[key].props.iconStyle.color) {
                    if(a[key].props.iconStyle.color === 'black'){
                        keyA = '1';
                    } else {
                        keyA = '2';
                    }
                }
                if(b[key] && b[key].props && b[key].props.iconStyle && b[key].props.iconStyle.color) {
                    if(b[key].props.iconStyle.color === 'black'){
                        keyB = '1';
                    } else {
                        keyB = '2';
                    }
                }
            } else {
                // In case value is object and not a string or number.
                if(a[key] && a[key].props && a[key].props.hasOwnProperty("children")
                    && a[key].props.children && a[key].props.children[1]
                    && a[key].props.children[1].props && a[key].props.children[1].props.children){
                    keyA = a[key].props.children[1].props.children;
                    // In case value is react object and not a string or number.
                } else if (a[key].props && a[key].props.hasOwnProperty("children")) {
                    keyA = a[key].props.children;
                } else {
                    keyA = a[key];
                }

                // In case value is object and not a string or number.
                if(b[key] && b[key].props && b[key].props.hasOwnProperty("children")
                    && b[key].props.children && b[key].props.children[1]
                    && b[key].props.children[1].props && b[key].props.children[1].props.children){
                    keyB = b[key].props.children[1].props.children;
                    // In case value is react object and not a string or number.
                } else if (b[key].props && b[key].props.hasOwnProperty("children")) {
                    keyB = b[key].props.children;
                } else {
                    keyB = b[key];
                }
            }


            // Checks if not a number
            if (keyA && !isNumeric(keyA) && typeof keyA !== 'object') {
                textA = keyA.toUpperCase();
            } else {
                textA = +keyA;
            }

            // Checks if not a number
            if (keyB && !isNumeric(keyB) && typeof keyB !== 'object') {
                textB = keyB.toUpperCase();
            } else {
                textB = +keyB;
            }

            return getSortResult(textA, textB);
        });

        this.setState({
            sortedTableData: tableData.slice((this.state.page - 1) * this.state.rowCountInPage,
                (this.state.page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handleFilterValueChange(value) {
        // fuzzyString is returned here and saved on state for future searches.
        let stateObj = FuzzySearchInTable(this.state.tableColumns, this.state.tableData, value);

        // Fix search results out of table bounds.
        if (stateObj.sortedTableData && Array.isArray(stateObj.sortedTableData)) {
            stateObj.page = 1;
            stateObj.sortedTableData = stateObj.sortedTableData.slice((stateObj.page - 1) * this.state.rowCountInPage,
                (stateObj.page - 1) * this.state.rowCountInPage + this.state.rowCountInPage);
        }

        this.setState(stateObj);
    }

    updateLastPageUserWasOn() {
        let page = this.state.page;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
                (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handleNextPageClick() {
        let page = this.state.page + 1;
        if(this.props.handleChangeDataViewClick && typeof this.props.handleChangeDataViewClick === 'function') {
            this.setState({page: page},
                () => {
                    this.props.handleChangeDataViewClick(page, this.state.rowCountInPage, this.state.searchQuery, this.state.sortMethod);
                });
        }
        else {
            let stateObj = {
                page: page,
                sortedTableData: this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
                    (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
            };

            this.setState(stateObj);
        }
    }

    handlePreviousPageClick() {
        let page = this.state.page - 1;
        if(this.props.handleChangeDataViewClick && typeof this.props.handleChangeDataViewClick === 'function') {
            this.setState({page: page},
                () => {
                    this.props.handleChangeDataViewClick(page, this.state.rowCountInPage, this.state.searchQuery, this.state.sortMethod);
                });
        }
        else {
            let stateObj = {
                page: page,
                sortedTableData: this.state.tableData.slice(page * this.state.rowCountInPage - this.state.rowCountInPage,
                    page * this.state.rowCountInPage)
            };
            this.setState(stateObj);
        }
    }

    handleRowSizeChange(ignore, rowCount) {
        let page = this.state.page;

        if (page > 1 && page > this.state.tableData.length / rowCount) {
            page--;
        }

        if(this.props.handleChangeDataViewClick && typeof this.props.handleChangeDataViewClick === 'function') {
            page = 1;
            this.setState({
                rowCountInPage: rowCount,
                page: page
            }, () => {
                this.props.handleChangeDataViewClick(page, rowCount, this.state.searchQuery, this.state.sortMethod);
            });
        }
        else {
            let stateObj = {
                rowCountInPage: rowCount,
                sortedTableData: this.state.tableData.slice((page - 1) * rowCount, (page - 1) * rowCount + rowCount),
                page: page
            };

            this.setState(stateObj);
        }
    }

    openExtraInfo(name, data, description, clickedRow, clickedCol, clickedData) {
        if (this.props && this.props.myDataType) {
            if (this.props.myDataType === BOTNETS_TYPE) {
                // Description is optional.
                if (name && data && Array.isArray(data)) {
                    this.setState({
                        extraInfoName: name,
                        extraInfoData: data,
                        extraInfoDescription: description,
                        openExtraInfo: true
                    });
                }
            } else if (this.props.myDataType === SSL_CERTS_TYPE || this.props.myDataType === WIFI_NETWORKS_TYPE) {
                // Meaning this type is sslCerts.
                if (clickedData && clickedData.extraData && Array.isArray(clickedData.extraData) && clickedData.extraData.length > 0) {
                    let domain = clickedData.extraData.find((currEntity) => {
                        return currEntity && currEntity.name && currEntity.name.toLowerCase() === 'domain';
                    });

                    let modalName = (domain && domain.finding) ? domain.finding + ' ' : '';
                    if(this.props.myDataType === WIFI_NETWORKS_TYPE) {
                        if(clickedData && clickedData.ssid) {
                            modalName += clickedData.ssid + ' ';
                        }
                        modalName += 'Wifi Network';
                    }
                    else {
                        modalName += 'SSL Certificate';
                    }

                    this.setState({
                        extraInfoName: modalName,
                        extraInfoData: clickedData.extraData,
                        extraInfoDescription: '',
                        openExtraInfo: true
                    });
                }
            }
        }
    }

    closeExtraInfo() {
        this.setState({extraInfoName: '', extraInfoData: [], extraInfoDescription: '', openExtraInfo: false});
    }

    redirectToCompanyDashboard(rowIdx) {
        let companyId;

        this.state.sortedTableData.map((curCompany, i) => {
                curCompany.key = i;
        });

        this.state.sortedTableData.map(curCompany => {
            if (curCompany.key === rowIdx) {
                companyId = curCompany.id;
            }
        });

        if (companyId) {
            browserHistory.push('/company-dashboard/' + companyId);
        }
    }

    componentWillMount() {
        app.setFlagVisibile(false);

        // This is a fix for sslCerts / contactsBox / infoBox / Overview / discoveredDomains to display data from the first time loading this component.
        if (this.props
            && this.props.myDataType
            && this.props.tableData
            && Array.isArray(this.props.tableData)
            && this.props.tableData.length > 0) {
            this.state.tableData = this.props.tableData;

            this.state.sortedTableData = this.state.tableData.slice((this.state.page - 1) * this.state.rowCountInPage,
                (this.state.page - 1) * this.state.rowCountInPage + this.state.rowCountInPage);

            if(this.props.totalRowCount && isNumeric(this.props.totalRowCount)) {
                this.state.totalRowCount = Number(this.props.totalRowCount);
            }
            else {
                this.state.totalRowCount = this.state.tableData.length;
            }

            if (this.props.tableColumns && Array.isArray(this.props.tableColumns) && this.props.tableColumns.length > 0) {
                this.state.tableColumns = this.props.tableColumns;
            }
            if(this.props.page){
                this.state.page = this.props.page;
            }
            if (this.props.assetUserInputAndMitigatedData && Array.isArray(this.props.assetUserInputAndMitigatedData)) {
                this.setState({assetUserInputAndMitigatedData: this.props.assetUserInputAndMitigatedData});
            }
        }
    }

    closeFindingDialog() {
        this.setState({isFindingStatusDialogOpen: false, isAssetUserInputOrMitigatedBtnClicked: false});
    }

    updateFindingStatus(isCommentsAreaUpdate = false) {
        if(typeof isCommentsAreaUpdate !== 'boolean'){
            isCommentsAreaUpdate = false
        }
        let successMes = 'Finding status has been update!';
        let failedMes = 'Error: failed to update finding status.';

        if(isCommentsAreaUpdate){
            successMes = 'Comments log has been update!';
            failedMes = 'Error: failed to update comments log.';
        }

        let curRowData = this.state.curRowData;
        let shouldDetachMitigatedConnection = !!(this.state.isStatusMitigatedActive && this.state.findingStatusType && this.state.findingStatusType === 'mitigated');
        let type = this.state.findingStatusType;
        let isAssetUserInputUpdate = !!(curRowData &&
            ((curRowData.importance && typeof curRowData.importance === 'number') ||
                (curRowData.comments && Array.isArray(curRowData.comments))));

        if (curRowData && typeof curRowData === 'object' && curRowData.identifier && typeof curRowData.identifier === 'string'&& curRowData.identifier !== '' ) {
            let curOrgObj = {
                orgId: localStorage.getItem(ORGANIZATION_ID),
                orgName: localStorage.getItem(ORGANIZATION_NAME)
            };

            let objToUpdate = {val: curRowData.identifier, type: this.props.myDataType};

            //Getting assetUserData to update
            if(isAssetUserInputUpdate){
                if(curRowData && curRowData.importance){
                    objToUpdate.importance = curRowData.importance;
                    objToUpdate.comments = curRowData && curRowData.Comments && curRowData.Comments.props && curRowData.Comments.props.children &&
                    curRowData.Comments.props.children[1] && curRowData.Comments.props.children[1].props &&
                    curRowData.Comments.props.children[1].props.children ? curRowData.Comments.props.children[1].props.children : [];
                } else if(curRowData && curRowData.comments){
                    objToUpdate.importance = curRowData && curRowData.Importance.props && curRowData.Importance.props.value ? curRowData.Importance.props.value : 2;
                    objToUpdate.comments = curRowData.comments;
                } else {
                    objToUpdate.importance = 2;
                    objToUpdate.comments = [];
                }
            }

            let findingStatusData = {
                curOrgObj: curOrgObj,
                statusType: type,
                objToUpdate: objToUpdate,
                shouldDetachMitigatedConnection: shouldDetachMitigatedConnection,
                updateOnMongoDB: true,
                isAssetUserInputUpdate: isAssetUserInputUpdate,
                dataType: this.props.myDataType
            };

            $.ajax({
                type: 'POST',
                url: '/api/updateFindingStatus',
                async: true,
                data: JSON.stringify({findingStatusData}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((res) => {
                if (res && res.ok) {
                    if(res.assetUserInputAndMitigatedRes){
                        this.setState({assetUserInputAndMitigatedData: res.assetUserInputAndMitigatedRes});
                        this.props.updateStateForAMData(res.assetUserInputAndMitigatedRes);
                    }
                    this.props.setResults();

                    if(this.state.orderKey && this.state.orderType){
                        this.handleSortOrderChange(this.state.orderKey, this.state.orderType)
                    }
                    this.updateLastPageUserWasOn();

                    this.setState({
                        isFindingStatusDialogOpen: false,
                        isAssetUserInputOrMitigatedBtnClicked: false,
                    });
                    app.addAlert('success', successMes);
                    //if user update comments area we do not recalculate the score again
                    if(!isCommentsAreaUpdate){
                        this.props.recalculateScore();
                    }
                    //if user mark asset as mitigated open comments dialog
                    if(!isAssetUserInputUpdate && type === 'mitigated' && !shouldDetachMitigatedConnection){
                        this.handleOpenCommentsDialog(curRowData, true);
                    }
                } else {
                    app.addAlert('error', failedMes);
                }
            }).fail(() => {
                app.addAlert('error', failedMes);
            });
        } else {
            app.addAlert('error', failedMes);
        }
    }

    checkIfPropOfCurNodeExists(curNode, type){
        if(type && type === 'assetUserInput'){
            return !!(curNode.Importance &&
                curNode.Importance.props &&
                curNode.Importance.props.value)
        } else {
            return !!(curNode.Mitigated &&
                curNode.Mitigated.props &&
                curNode.Mitigated.props &&
                curNode.Mitigated.props.iconStyle &&
                curNode.Mitigated.props.iconStyle.color)
        }
    }

    isCurRowNeedToBeUpdateCurNode(curNode, curRowData){
        let isTrue = false;
        if(this.props.myDataType === BOTNETS_TYPE ||
            this.props.myDataType === GDPR_NOT_COMPLY_TYPE ||
            this.props.myDataType === SSL_CERTS_TYPE){
            if(curRowData && curRowData.identifier && curNode && curNode.id &&
                curRowData.identifier === curNode.id){
                isTrue = true;
            }
        }

        return isTrue;
    }

    componentDidMount() {
        let user = app.getAuthUser();
        if (user) {
            this.setState({user: user});
        }
        if (this.props && this.props.hideFooterToolbar) {
            // If there is not footer - show as many results as possible (100 is the max).
            this.handleRowSizeChange(null, 100);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps && typeof nextProps === 'object') {
            let stateObj = {};

            if (nextProps.hasOwnProperty('tableColumns')) {
                stateObj.tableColumns = nextProps.tableColumns;
            }

            if(nextProps && nextProps.assetUserInputAndMitigatedData){
                stateObj.assetUserInputAndMitigatedData =  nextProps.assetUserInputAndMitigatedData;
            }

            if (nextProps.hasOwnProperty('tableData')) {
                if(nextProps.handleChangeDataViewClick && typeof nextProps.handleChangeDataViewClick === 'function') {
                    // If handleChangeDataViewClick is inserted - meaning its a pagination component.
                    stateObj.tableData = nextProps.tableData;

                    stateObj.sortedTableData = nextProps.tableData;
                }
                else {
                    stateObj.tableData = nextProps.tableData;
                    if (!Array.isArray(nextProps.tableData) || nextProps.tableData.length === 0) {
                        stateObj.tableColumns = [{key: 'label', label: 'No data to display.'}];
                    }

                    stateObj.sortedTableData = nextProps.tableData.slice((this.state.page - 1) * this.state.rowCountInPage,
                        (this.state.page - 1) * this.state.rowCountInPage + this.state.rowCountInPage);
                }

                if(nextProps.hasOwnProperty('totalRowCount') && isNumeric(nextProps.totalRowCount)) {
                    stateObj.totalRowCount = Number(nextProps.totalRowCount);
                }
                else {
                    stateObj.totalRowCount = nextProps.tableData.length;
                }
            }

            // handleFilterValueChange is called here to apply the search fuzzyString in case it exists.
            this.setState(stateObj, () => {
                // If handleChangeDataViewClick is not inserted - meaning its not a pagination component.
                if(!(this.props.handleChangeDataViewClick && typeof this.props.handleChangeDataViewClick === 'function')) {
                    if(this.state.fuzzyString){
                        this.handleFilterValueChange(this.state.fuzzyString || '');
                    }
                }
            });
        }
    }

    handleSearchTextChanged(e) {
        if(searchDelayTimer) {
            clearTimeout(searchDelayTimer);
        }
        if(e.preventDefault) {
            e.preventDefault();
        }
        if(e && e.target && e.target.hasOwnProperty('value')) {
            this.setState({
                searchQuery: e.target.value
            }, () => {
                // After updating the search query - set a timer to start the search.
                searchDelayTimer = setTimeout(() => {
                    if(this.props.handleChangeDataViewClick && typeof this.props.handleChangeDataViewClick === 'function') {
                        const stateToUpdate = {
                            page: 1
                        };
                        this.setState(stateToUpdate,
                            () => {
                                this.props.handleChangeDataViewClick(stateToUpdate.page, this.state.rowCountInPage, this.state.searchQuery, this.state.sortMethod);
                            });
                    }
                }, 2000);
            });
        }
    }

    handleSortMethodChanged(e, value) {
        if(e.preventDefault) {
            e.preventDefault();
        }

        if(value && this.state.sortMethod !== value) {
            this.setState({
                sortMethod: value,
                page: 1
            }, () => {
                if(this.props.handleChangeDataViewClick && typeof this.props.handleChangeDataViewClick === 'function') {
                    this.props.handleChangeDataViewClick(this.state.page, this.state.rowCountInPage, this.state.searchQuery, this.state.sortMethod);
                }
            });
        }
    }

    render() {
        const cardStyle = {
            'margin': '10px 10px 20px 10px',
            'display': 'inline-block'
        };

        const tableBodyStyle = {
            'overflowX': 'auto'
        };

        let tableStyle;
        let tableHeaderStyle = {};

        let tableRowStyle;

        if(this.props && this.props.myDataType && this.props.myDataType === OVERVIEW_TYPE) {
            tableRowStyle = {
                cursor: 'pointer'
            };
        } else {
            tableRowStyle={}
        }

        if (this.props && this.props.myDataType && this.props.myDataType === INFORMATION_BOX_TYPE) {
            tableHeaderStyle.display = 'none';
            tableStyle = {
                'width': '95%',
                'margin': '3% auto'
            };
        } else {
            tableStyle = {
                'width': '100%',
                'margin': 'auto',
            };
        }

        const downloadStyle = {
            cursor: 'pointer'
        };

        let fileNameCSV;
        let defaultPropForCSV;

        if (this.props && this.props.myDataType) {
            if (this.props.myDataType === BOTNETS_TYPE) {
                fileNameCSV = 'rescana_botnets';
                if (this.props.botnetsFeedName) {
                    fileNameCSV += '_' + this.props.botnetsFeedName;
                }

                fileNameCSV += '.csv';
                defaultPropForCSV = 'Botnets';
            } else if (this.props.myDataType === OVERVIEW_TYPE) {
                fileNameCSV = 'rescana_overview.csv';

                defaultPropForCSV = 'Overview';
            } else if (this.props.myDataType === GDPR_NOT_COMPLY_TYPE) {
                fileNameCSV = 'rescana_GDPR_compliance.csv';

                defaultPropForCSV = 'Web Privacy Policy';
            } else if (this.props.myDataType === DISCOVERED_DOMAINS_TYPE) {
                fileNameCSV = 'rescana_discovered_domains.csv';

                defaultPropForCSV = 'Discovered Domains';
            } else if (this.props.myDataType === WIFI_NETWORKS_TYPE) {
                fileNameCSV = 'rescana_wifi_networks.csv';

                defaultPropForCSV = 'Wifi Networks';
            } else {
                // Meaning this type is sslCerts.
                fileNameCSV = 'rescana_';
                if (this.props.displayOnlyValid) {
                    fileNameCSV += 'valid_';
                } else {
                    fileNameCSV += 'not_valid_';
                }
                fileNameCSV += 'https.csv';

                defaultPropForCSV = 'SSL Certificates';
            }
        } else {
            fileNameCSV = 'rescana.csv';
            defaultPropForCSV = 'rescana';
        }

        let csvData;
        let csvColumns;
        if (!this.props || !this.props.tableDataToDownload || !Array.isArray(this.props.tableDataToDownload) || !this.props.tableDataToDownload.length
            || !this.props.tableColumnsToDownload) {
            let csvDefaultObj = {};
            csvDefaultObj[defaultPropForCSV] = 'No data found.';
            csvData = [csvDefaultObj];
            csvColumns = [defaultPropForCSV];
        } else {
            csvData = this.props.tableDataToDownload;
            csvColumns = this.props.tableColumnsToDownload;
        }

        let downloadCSV = (this.props && this.props.hideDownloadExcel) ? null : [
            <div>
                <CSVLink
                    data={csvData}
                    headers={csvColumns}
                    filename={fileNameCSV}>
                    <FileDownloadIcon
                        style={downloadStyle}>
                    </FileDownloadIcon>
                </CSVLink>
            </div>
        ];

        let onCellClickFunc;
        if(this.state.isAssetUserInputOrMitigatedBtnClicked){
            onCellClickFunc = function () {
            };
        } else if (this.props && this.props.extraInfoName && this.props.extraInfoData) {
            onCellClickFunc = this.openExtraInfo.bind(this, this.props.extraInfoName, this.props.extraInfoData, this.props.extraInfoDescription);
        } else if (this.props && this.props.myDataType && (this.props.myDataType === SSL_CERTS_TYPE || this.props.myDataType === WIFI_NETWORKS_TYPE)) {
            // cancel openExtraInfo params for botnets and click event will pass clicked object extraData.
            onCellClickFunc = this.openExtraInfo.bind(this, '', [], '');
        } else if (this.props && this.props.myDataType && this.props.myDataType === OVERVIEW_TYPE) {
            onCellClickFunc = this.redirectToCompanyDashboard.bind(this);
        } else {
            onCellClickFunc = function () {
            };
        }

        // These are props that are used in contactsBox in companyDashboard.
        const showHeaderToolbar = !this.props.hideHeaderToolbar;

        const showFooterToolbar = !this.props.hideFooterToolbar;

        // These are props that are used in admin tables.
        const showCardBackground = !this.props.hideCardBackground;

        const searchTextFieldStyle = {
            marginLeft: 25,
            marginBottom: 5,
            fontSize: 14,
            width: '20%'
        };

        let findingCurType = this.state.findingCurType;
        if(findingCurType && findingCurType.toLocaleLowerCase() === 'importance'){
            findingCurType = 'importance/status';
        }

        let dataType = this.props.myDataType;
        if(dataType && typeof dataType === 'string'){
            if (dataType === SSL_CERTS_TYPE) {
                dataType = 'ssl\'s';
            } else if (dataType === GDPR_NOT_COMPLY_TYPE) {
                dataType = 'web privacy banner\'s';
            } else if (dataType === BOTNETS_TYPE) {
                dataType = 'botnet\'s';
            } else {
                dataType = '';
            }
        }
        const searchAndSortComponent = (this.props.searchHintText && this.props.sortOptions && Array.isArray(this.props.sortOptions)) ? <div>
            <TextField
                floatingLabelText="Search"
                hintText={this.props.searchHintText}
                onChange={this.handleSearchTextChanged.bind(this)}
                value={this.state.searchQuery}
                style={searchTextFieldStyle}
                hintStyle={{marginLeft: 10}}
                inputStyle={{marginLeft: 10}}
            />
            <IconMenu
                iconButtonElement={<IconButton
                    tooltip="sort"
                    touch={true}
                    tooltipPosition="bottom-center"
                ><SortIcon/></IconButton>}
                anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                style={{marginLeft: '20px'}}
                menuStyle={{backgroundColor: '#444444'}}
                value={this.state.sortMethod}
                onChange={this.handleSortMethodChanged.bind(this)}
            >
                {this.props.sortOptions.map((currSort, i) => {
                    if(currSort && currSort.primaryText && currSort.value && currSort.icon) {
                        return <MenuItem key={i}
                                         primaryText={currSort.primaryText}
                                         value={currSort.value}
                                         leftIcon={currSort.icon}
                                         style={currSort.style || {}}
                        />;
                    }
                })}
            </IconMenu>
        </div> : null;

        const tableWithInfoModal = <div>
            <DataTables
                height={'auto'}
                selectable={false}
                showRowHover={true}
                showHeaderToolbar={showHeaderToolbar}
                showFooterToolbar={showFooterToolbar}
                tableHeaderStyle={tableHeaderStyle}
                tableBodyStyle={tableBodyStyle}
                tableStyle={tableStyle}
                tableRowStyle={tableRowStyle}
                columns={this.state.tableColumns}
                data={this.state.sortedTableData}
                showCheckboxes={false}
                onCellClick={onCellClickFunc}
                onFilterValueChange={this.handleFilterValueChange}
                onSortOrderChange={this.handleSortOrderChange}
                onNextPageClick={this.handleNextPageClick}
                onRowSizeChange={this.handleRowSizeChange}
                onPreviousPageClick={this.handlePreviousPageClick}
                headerToolbarMode={'filter'}
                page={this.state.page}
                rowSize={this.state.rowCountInPage}
                count={this.state.totalRowCount}
                toolbarIconRight={downloadCSV}
            />
            <DataTableTemplateInfoModal
                user={this.state.user || {}}
                open={this.state.openExtraInfo}
                closeDialog={this.closeExtraInfo.bind(this)}
                name={this.state.extraInfoName}
                description={this.state.extraInfoDescription}
                data={this.state.extraInfoData}
                myDataType={this.props.myDataType}
            />
            <DynamicDialog
                open={this.state.isFindingStatusDialogOpen}
                title={'Change Finding Status'}
                bodyText={'Are you sure you wish to change the '+ dataType +' '+ findingCurType +'?'}
                btnOkLable={'Yes'}
                btnCancelLable={'No'}
                cancelBtnEnabled={true}
                handlebBtnOkClick={this.updateFindingStatus.bind(this)}
                handlebBtnCancelClick={this.closeFindingDialog.bind(this)}
            />
            <AssetUserInputCommentsDialog
                open={this.state.isCommentsDialogOpen}
                title={'Comments'}
                updateCommentsArea={this.handleUpdateCommentsArea.bind(this)}
                handlebBtnCloseClick={this.handleCloseCommentsDialog.bind(this)}
                curCommentsArr={this.state.curCommentsArr || []}
                isFromMitigated={this.state.isFromMitigated}
            />
        </div>;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    {showCardBackground ? (
                        <Card style={cardStyle}>
                            <div>
                                {searchAndSortComponent}
                                {tableWithInfoModal}
                            </div>
                        </Card>
                    ) : (
                        <div>
                            <div>
                                {searchAndSortComponent}
                                {tableWithInfoModal}
                            </div>
                        </div>
                    )}
                </div>
            </MuiThemeProvider>
        );
    }
}

DataTableViewTemplate.propTypes = {
    myDataType: PropTypes.string.isRequired,
    tableColumns: PropTypes.array.isRequired,
    tableData: PropTypes.array.isRequired,
    extraInfoName: PropTypes.string,
    extraInfoData: PropTypes.array,
    extraInfoDescription: PropTypes.string,
    hideHeaderToolbar: PropTypes.bool,
    hideFooterToolbar: PropTypes.bool,
    hideDownloadExcel: PropTypes.bool,
    hideCardBackground: PropTypes.bool,
    totalRowCount: PropTypes.number,
    page: PropTypes.number,
    handleChangeDataViewClick: PropTypes.func,
};
