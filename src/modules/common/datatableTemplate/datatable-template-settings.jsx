import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete';
import {findCompanyIndexByID, findCompanyIndexByName} from '../../dataleaks/dataleaks-helpers.js';
import {
    EMAIL_BREACHES_TYPE,
    BLACKLISTS_TYPE,
    SESSION_COMPANIES,
    GRAPH_COMPANIES_FILTER_LIMIT
} from '../../../../config/consts';
import {getDefaultCompanies} from "./datatable-template-helpers";

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class DataTableTemplateSettingsModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            companies: [],
            companiesInputVal: [],
            companiesInputProps: []
        };
    }

    handleClose(isCancelClicked = true) {
        let stateObj = {open: false};

        // Before Modal is closed - if no selection was made - select first company as default on sessionStorage.
        if (isCancelClicked && !sessionStorage.getItem(SESSION_COMPANIES) && this.state.companies && this.state.companies.length > 0) {
            stateObj.companiesInputVal = [this.state.companies[0]];
        }
        this.setState(stateObj, () => {
            // If the default selection was made - save it on sessionStorage.
            if (stateObj.hasOwnProperty('companiesInputVal')) {
                this.saveCompaniesOnSession();
            }

            // If Apply led here then call its props.
            if (!isCancelClicked) {
                this.props.ApplyChangesFromSettings(this.state.companiesInputVal, this.state.companiesInputProps);
            }
            this.props.closeDialog();
        });
    };

    handleAddRequestedCompanyField(chip, companiesPropName) {
        if (!Array.isArray(chip)) {
            chip = [chip];
        }

        let companiesOptions = this.state.companies;
        let companiesInputVal = [];
        let companiesInputProps = [];

        companiesInputVal = companiesInputVal.concat(this.state.companiesInputVal);
        companiesInputProps = companiesInputProps.concat(this.state.companiesInputProps);

        let input = '';

        if (chip && chip[0] && chip[0].companyName) {
            input = chip[0].companyName;
        }
        // Check if the companies contain the input.
        let inputIndex = findCompanyIndexByName(companiesOptions, input);

        if (inputIndex > -1) {
            let companyObj = companiesOptions[inputIndex];
            let isExist = false;

            for (let i = 0; i < companiesInputVal.length; i++) {
                if (companiesInputVal[i] && companiesInputVal[i].companyName && companiesInputVal[i].companyName === input) {
                    isExist = true;
                    break;
                }
            }
            // Check if input was not already selected.
            if (!isExist) {
                // Add to the ChipSelect.
                companiesInputVal = companiesInputVal.concat(companyObj);

                // Add propData to state.
                let addedPropData = companyObj[companiesPropName];

                if (addedPropData) {
                    addedPropData.map((prop) => {
                        if (companiesInputProps.indexOf(prop) === -1) {
                            companiesInputProps.push(prop);
                        }
                    });
                }

                this.setState({companiesInputVal: companiesInputVal, companiesInputProps: companiesInputProps});
            }
        } else {
            app.addAlert('error', 'No such company.');
        }
    }

    handleDeleteRequestedCompanyField(value, index, companiesPropName) {
        let companiesInputVal = [];
        let companiesInputProps = [];

        companiesInputVal = companiesInputVal.concat(this.state.companiesInputVal);

        let companies = this.state.companies;

        companiesInputProps = companiesInputProps.concat(this.state.companiesInputProps);
        let selectedIndex = findCompanyIndexByID(companies, value);

        if (selectedIndex > -1) {
            let propDataToDelete = companies[selectedIndex][companiesPropName];

            companiesInputVal.splice(index, 1);

            if (propDataToDelete) {
                propDataToDelete.map((prop) => {
                    let isFoundInOtherCompanies = false;
                    for (let i = 0; i < companiesInputVal.length; i++) {
                        if (companiesInputVal[i][companiesPropName] && companiesInputVal[i][companiesPropName].indexOf(prop) !== -1) {
                            isFoundInOtherCompanies = true;
                            break;
                        }
                    }
                    if (!isFoundInOtherCompanies) {
                        companiesInputProps.splice(companiesInputProps.indexOf(prop), 1);
                    }
                });
            }

            this.setState({companiesInputVal: companiesInputVal, companiesInputProps: companiesInputProps});
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open === true) {
            let stateObj = {open: nextProps.open};

            if (nextProps.companies) {
                stateObj.companies = nextProps.companies;
            }

            if (nextProps.companiesInputVal) {
                stateObj.companiesInputVal = nextProps.companiesInputVal;
            }

            if (nextProps.companiesPropData) {
                stateObj.companiesInputProps = nextProps.companiesPropData;
            }

            // Set all companies as selected by default, so user can only delete or re-add them.
            this.setState(stateObj);
        }
    }

    saveCompaniesOnSession() {
        sessionStorage.setItem(SESSION_COMPANIES, this.state.companiesInputVal.map((c) => {
            return c.companyName;
        }).join(', '));
    }

    ApplySettings() {
        if (this.state.companiesInputVal && this.state.companiesInputVal.length > 0) {
            this.saveCompaniesOnSession();
            this.handleClose(false);
        } else {
            app.addAlert('warning', 'At least one company is required.');
        }
    }

    handleClear() {
        let companiesInputVal = [];
        let companiesInputProps = [];
        if (this.state.companiesInputVal && this.state.companiesInputVal.length > 0) {
            this.setState({companiesInputVal: companiesInputVal, companiesInputProps: companiesInputProps});
        } else {
            app.addAlert('warning', 'Already Cleared.');
        }
    }

    render() {
        let buttonsContainer = classNames({
            'uploadDialogButtonsLTR': true
        });

        const actions = [
            <FlatButton
                label="Clear"
                secondary={true}
                onTouchTap={this.handleClear.bind(this)}
                primary={true}
                backgroundColor={'#0091ea'}
                style={{float: 'left', marginLeft: '5px', color: 'white', marginBottom: '5px', height: 40}}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                keyboardFocused={false}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                backgroundColor={'#0091ea'}
                style={{marginRight: '5px', color: 'white', marginBottom: '5px', height: 40}}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                onTouchTap={this.handleClose.bind(this, true)}
            />,
            <FlatButton
                label="Apply"
                primary={true}
                style={{
                    color: 'white',
                    marginBottom: '5px',
                    marginRight: '5px',
                    height: 40
                }}
                backgroundColor={'#0091ea'}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                onTouchTap={this.ApplySettings.bind(this)}
            />
        ];

        let companiesPropName;

        if (this.props.myDataType &&
            (this.props.myDataType === EMAIL_BREACHES_TYPE || this.props.myDataType === BLACKLISTS_TYPE)) {
            companiesPropName = 'selectedDomains';

            /* Meaning that data type is Dataleaks. */
        } else {
            companiesPropName = 'keywords';
        }

        return (
            <div>
                <Dialog
                    title={'Filter By Companies'}
                    actions={actions}
                    actionsContainerClassName={buttonsContainer}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose.bind(this, true)}
                    actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                    bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                    repositionOnUpdate={false}
                    contentStyle={{borderRadius: '7px 7px 7px 7px'}}
                    titleStyle={{
                        fontSize: 18,
                        background: 'rgba(0,0,0,0.7)',
                        color: 'white', textAlign: 'center',
                        borderRadius: '2px 2px 0px 0px',
                        textTransform: 'uppercase',
                    }}
                    autoScrollBodyContent={true}>
                    <br/>
                    <ChipInput
                        hintText="Requested companies"
                        floatingLabelText="Requested companies"
                        value={this.state.companiesInputVal}
                        filter={AutoComplete.fuzzyFilter}
                        dataSource={this.props.companies}
                        dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                        onRequestAdd={(chip) => this.handleAddRequestedCompanyField(chip, companiesPropName)}
                        onRequestDelete={(chip, index) => this.handleDeleteRequestedCompanyField(chip, index, companiesPropName)}
                        maxSearchResults={5}
                        openOnFocus={true}
                        fullWidth={true}
                        fullWidthInput={true}
                    />
                    <br/>
                    <br/>
                </Dialog>
            </div>
        );
    }
};
