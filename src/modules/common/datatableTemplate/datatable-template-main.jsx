import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardHeader} from 'material-ui/Card';
import DataTables from 'material-ui-datatables';
import IconButton from 'material-ui/IconButton';
import SettingsButton from 'material-ui/svg-icons/editor/mode-edit';
import React from 'react';
import {CSVLink} from 'react-csv';
import FileDownloadIcon from 'material-ui/svg-icons/file/file-download';
import DataTableTemplateSettingsModal from './datatable-template-settings.jsx';
import {showExpandedDataChip} from '../UI-Helpers.jsx';
import {
    PROJECT_NAME,
    EMAIL_BREACHES_TYPE,
    BLACKLISTS_TYPE,
    SESSION_COMPANIES,
    GRAPH_COMPANIES_FILTER_LIMIT,
    DATALEAKS_TYPE,
    ORGANIZATION_ID,
    ORGANIZATION_NAME,
    BOTNETS_TYPE,
    SSL_CERTS_TYPE,
    GDPR_NOT_COMPLY_TYPE
} from '../../../../config/consts';
import DataTableTemplateInfoModal from './datatable-template-info.jsx';
import HIBP_Credits from '../../breaches/hibp-credits.jsx';
import {FuzzySearchInTable} from '../CommonHelpers.js';
import PropTypes from 'prop-types';
import themeForFont from "../../app/themes/themeForFont";
import {getDefaultCompanies, getLoadedCompaniesObject} from "./datatable-template-helpers";
import DynamicDialog from '../../app/dynamic-dialog.jsx';
import {isNumeric} from "../CommonHelpers";
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import MitigatedIcon from 'material-ui/svg-icons/action/done';
import CommentIcon from 'material-ui/svg-icons/communication/comment';
import Badge from 'material-ui/Badge';
import AssetUserInputCommentsDialog from '../../generalComponents/comments-dialog.jsx';
import ReactTooltip from 'react-tooltip';

export default class DataTableTemplate extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tableColumns: [],
            tableData: [],
            sortedTableData: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            page: 1,
            openSettingsModal: false,
            openExtraInfo: false,
            isFindingStatusDialogOpen: false,
            isCommentsDialogOpen: false,
            curCommentsArr: [],
            findingCurType: ''
        };

        this.handleSortOrderChange = this.handleSortOrderChange.bind(this);
        this.handleFilterValueChange = this.handleFilterValueChange.bind(this);
        this.handleNextPageClick = this.handleNextPageClick.bind(this);
        this.handlePreviousPageClick = this.handlePreviousPageClick.bind(this);
        this.handleRowSizeChange = this.handleRowSizeChange.bind(this);
        this.handleRowSelection = this.handleRowSelection.bind(this);
        this.updateSettingsOfUser = this.updateSettingsOfUser.bind(this);
        this.getAllCompaniesByID = this.getAllCompaniesByID.bind(this);
        this.getAllowedCompanies = this.getAllowedCompanies.bind(this);
        this.saveDefaultCompanies = this.saveDefaultCompanies.bind(this);
        this.setDefaultCompaniesToState = this.setDefaultCompaniesToState.bind(this);
        this.handleChangeToSecondTable = this.handleChangeToSecondTable.bind(this);
        this.doAfterSetResult = this.doAfterSetResult.bind(this);
    }

    handleUpdateCommentsArea(curCommentsArr) {
        let curRowData = this.state.curRowData;
        let orgId = localStorage.getItem(ORGANIZATION_ID);
        if (curRowData && orgId && curCommentsArr && Array.isArray(curCommentsArr)) {
            curRowData.comments = curCommentsArr;
            this.setState({curRowData: curRowData});
            this.updateFindingStatus(true);
            this.setState({curCommentsArr: curCommentsArr});
        } else {
            app.addAlert('error', 'Error: failed to update Comments Area');
        }
    }

    handleCloseCommentsDialog() {
        this.setState({isCommentsDialogOpen: false, isAssetUserInputOrMitigatedBtnClicked: false});
    }

    onTapStatusDropdown() {
        this.setState({isAssetUserInputOrMitigatedBtnClicked: true});
    }
    onDropdownClose() {
        this.setState({isAssetUserInputOrMitigatedBtnClicked: false});
    }

    onChangeStatusDropdown(curRowData, type, event, index, value) {
        this.setState({isFindingStatusDialogOpen: true});
        setTimeout(() => {
            curRowData.importance = value;

            if (this.props.myDataType === BLACKLISTS_TYPE ||
                this.props.myDataType === DATALEAKS_TYPE ||
                this.props.myDataType === EMAIL_BREACHES_TYPE) {
                if (curRowData && curRowData.id) {
                    curRowData.identifier = curRowData.id;
                }
            }

            this.setState({findingCurType: 'importance', curRowData: curRowData, findingStatusType: type});
        }, 1);
    }

    HandleMitigatedMark(curRowData) {
        //We use this to avoid cell click that redirect us to graph page
        this.setState({isAssetUserInputOrMitigatedBtnClicked: true, isFindingStatusDialogOpen: true});
        let isPropOfCurNodeExists = this.checkIfPropOfCurNodeExists(curRowData, 'Mitigated');
        let findingCurType = '';

        let isStatusMitigatedActive = isPropOfCurNodeExists && curRowData.Mitigated && curRowData.Mitigated.props &&
            curRowData.Mitigated.props.iconStyle && curRowData.Mitigated.props.iconStyle.color &&
            curRowData.Mitigated.props.iconStyle.color !== "black";

        if (isStatusMitigatedActive) {
            findingCurType = 'status from mitigated to default';
        } else {
            findingCurType = 'status to mitigated';
        }

        if (this.props.myDataType === BLACKLISTS_TYPE ||
            this.props.myDataType === DATALEAKS_TYPE ||
            this.props.myDataType === EMAIL_BREACHES_TYPE) {
            if (curRowData && curRowData.id) {
                curRowData.identifier = curRowData.id;
            }
        }

        this.setState({
            findingCurType: findingCurType,
            curRowData: curRowData,
            isStatusMitigatedActive: isStatusMitigatedActive,
            findingStatusType: 'mitigated'});
    }

    checkIfPropOfCurNodeExists(curNode, type){
        if(type && type === 'assetUserInput'){
            return !!(curNode.Importance &&
                curNode.Importance.props &&
                curNode.Importance.props.value)
        } else {
            return !!(curNode.Mitigated &&
                curNode.Mitigated.props &&
                curNode.Mitigated.props &&
                curNode.Mitigated.props.iconStyle &&
                curNode.Mitigated.props.iconStyle.color)
        }
    }

    handleOpenCommentsDialog(curRowData, isFromMitigated = false) {
        let comments = [];
        if(curRowData.Comments &&
            curRowData.Comments.props &&
            curRowData.Comments.props.children &&
            curRowData.Comments.props.children[1] &&
            curRowData.Comments.props.children[1].props &&
            curRowData.Comments.props.children[1].props.children){
            comments = curRowData.Comments.props.children[1].props.children;
        }

        if (this.props.myDataType === BLACKLISTS_TYPE ||
            this.props.myDataType === DATALEAKS_TYPE ||
            this.props.myDataType === EMAIL_BREACHES_TYPE) {
            if (curRowData && curRowData.id) {
                curRowData.identifier = curRowData.id;
            }
        }

        this.setState({
            isFromMitigated: isFromMitigated,
            isAssetUserInputOrMitigatedBtnClicked: true,
            isCommentsDialogOpen: true,
            curCommentsArr: comments,
            curRowData: curRowData,
            findingStatusType: 'assetUserInput'});
    }

    getImportanceComponent(tableDataObj, importance = 3) {
        return <DropDownMenu
            value={Number(importance)}
            onTouchTap={this.onTapStatusDropdown.bind(this)}
            onClose={this.onDropdownClose.bind(this)}
            onChange={this.onChangeStatusDropdown.bind(this, tableDataObj, 'assetUserInput')}
            autoWidth={false}
            labelStyle={{left: 10, fontSize: 13}}
            style={{width: 170}}
            menuStyle={{borderRadius: 4}}
            menuItemStyle={{fontSize: 13}}
            anchorOrigin={{
                horizontal: 'left',
                vertical: 'bottom'
            }}>
            <MenuItem value={1} primaryText="False Positive"/>
            <MenuItem value={2} primaryText="Low"/>
            <MenuItem value={3} primaryText="Medium"/>
            <MenuItem value={4} primaryText="High"/>
            <MenuItem value={5} primaryText="Critical"/>
        </DropDownMenu>

    }

    getCommentsComponent(tableDataObj, commentsCount = 0, comments = [], badgeColor = '#82abea') {
        return <div style={{paddingLeft: 30}}><Badge badgeContent={commentsCount} secondary={true} style={{marginLeft: -30, marginRight: -30}}
                           badgeStyle={{background: badgeColor, top: 17, right: 17}}>
            <IconButton data-tip={'Comments'}
                        data-for={'Comments'}
                        onTouchTap={this.handleOpenCommentsDialog.bind(this, tableDataObj, false)}>
                <CommentIcon style={{width: 5, height: 5}}/>
                <ReactTooltip id='Comments' place="right" effect="solid"/>
            </IconButton>
        </Badge>
            <span style={{display: 'none', visibility: 'hidden'}}>{comments}</span>
        </div>
    }

    getMitigatedComponent(tableDataObj, mitigatedBackground = 'black') {
        return <IconButton
            data-tip={'Mark as Mitigated'}
            data-for={'mitigated'}
            iconStyle={{width: 30, height: 30, color: mitigatedBackground, position: 'relative', left: 6, top: 2}}
            onTouchTap={this.HandleMitigatedMark.bind(this, tableDataObj)}>
            <ReactTooltip id='mitigated' place="right" effect="solid"/>
            <MitigatedIcon viewBox={'3 3 35 35'}/>
        </IconButton>
    }

    recalculateScore() {
        let data = {};

        if (this.state.companiesInputVal && this.state.companiesInputVal.length > 0) {
            data.companyIDs = [];
            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        data.orgId = localStorage.getItem(ORGANIZATION_ID);
        data.saveScoresOnDB = true;
        $.ajax({
            type: 'POST',
            url: '/api/getMaxCVSScore',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'CalculateScores');
            } else {
                app.addAlert('Error', 'Error calculating scores');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to calculate scores');
        });
    }

    handleSortOrderChange(key, order) {
        this.setState({orderKey: key, orderType: order});
        let tableData = this.state.tableData;
        let textA;
        let textB;

        let getSortResult;
        if (order === 'asc') {
            getSortResult = (textA, textB) => {
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            };
        } else {
            getSortResult = (textA, textB) => {
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            };
        }

        tableData.sort((a, b) => {
            let keyA, keyB;

            //using color the order is fp -> mitigated -> default or backwards
            if(key === 'Importance'){
                if(a[key] && a[key].props && a[key].props.value) {
                    if(a[key].props.value === 4){
                        keyA = 4;
                    } else if(a[key].props.value === 3){
                        keyA = 3;
                    } else if(a[key].props.value === 2){
                        keyA = 2;
                    } else if(a[key].props.value === 1){
                        keyA = 1;
                    } else {
                        keyA = 0;
                    }
                }

                if(b[key] && b[key].props && b[key].props.value) {
                    if(b[key].props.value === 4){
                        keyB = 4;
                    } else if(b[key].props.value === 3){
                        keyB = 3;
                    } else if(b[key].props.value === 2){
                        keyB = 2;
                    } else if(b[key].props.value === 1){
                        keyB = 1;
                    } else {
                        keyB = 0;
                    }
                }
            } else if(key === 'Mitigated'){
                if(a[key] && a[key].props && a[key].props.iconStyle && a[key].props.iconStyle.color) {
                    if(a[key].props.iconStyle.color === 'black'){
                        keyA = '1';
                    } else {
                        keyA = '2';
                    }
                }
                if(b[key] && b[key].props && b[key].props.iconStyle && b[key].props.iconStyle.color) {
                    if(b[key].props.iconStyle.color === 'black'){
                        keyB = '1';
                    } else {
                        keyB = '2';
                    }
                }
            } else {
                // In case value is object and not a string or number.
                if(a[key].props && a[key].props.hasOwnProperty("children")
                    && a[key].props.children && a[key].props.children[1]
                    && a[key].props.children[1].props && a[key].props.children[1].props.children){
                    keyA = a[key].props.children[1].props.children;
                    // In case value is react object and not a string or number.
                } else if (a[key].props && a[key].props.hasOwnProperty("children")) {
                    keyA = a[key].props.children;
                } else {
                    keyA = a[key];
                }

                // In case value is object and not a string or number.
                if(b[key].props && b[key].props.hasOwnProperty("children")
                    && b[key].props.children && b[key].props.children[1]
                    && b[key].props.children[1].props && b[key].props.children[1].props.children){
                    keyB = b[key].props.children[1].props.children;
                    // In case value is react object and not a string or number.
                } else if (b[key].props && b[key].props.hasOwnProperty("children")) {
                    keyB = b[key].props.children;
                } else {
                    keyB = b[key];
                }
            }


            // Checks if not a number
            if (!isNumeric(keyA) && typeof keyA !== 'object') {
                textA = keyA.toUpperCase();
            } else {
                textA = +keyA;
            }

            // Checks if not a number
            if (!isNumeric(keyB) && typeof keyB !== 'object') {
                textB = keyB.toUpperCase();
            } else {
                textB = +keyB;
            }

            return getSortResult(textA, textB);
        });

        this.setState({
            sortedTableData: tableData.slice((this.state.page - 1) * this.state.rowCountInPage,
                (this.state.page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handleOpenSettings() {
        this.setState({
            openSettingsModal: true
        });
    }

    closeSettingsDialog() {
        this.setState({openSettingsModal: false});
    }

    openExtraInfo(nodeID, name, data) {
        let dataToDisplay;

        if (this.props && this.props.hasOwnProperty('arrangeExtraInfo')) {
            dataToDisplay = this.props.arrangeExtraInfo(data);
        } else {
            dataToDisplay = data;
        }

        this.setState({extraInfoName: name, extraInfoData: dataToDisplay, openExtraInfo: true});
    }

    closeExtraInfo() {
        this.setState({extraInfoName: '', extraInfoData: [], openExtraInfo: false});
    }

    handleFilterValueChange(value) {
        let stateObj = FuzzySearchInTable(this.state.tableColumns, this.state.tableData, value);

        this.setState(stateObj, this.props.updateParentState(stateObj));
    }

    updateLastPageUserWasOn() {
        let page = this.state.page;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
                (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handleNextPageClick() {
        let page = this.state.page + 1;
        let stateObj = {
            page: page,
            sortedTableData: this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
                (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        };

        this.setState(stateObj, this.props.updateParentState(stateObj));
    }

    handlePreviousPageClick() {
        let page = this.state.page - 1;
        let stateObj = {
            page: page,
            sortedTableData: this.state.tableData.slice(page * this.state.rowCountInPage - this.state.rowCountInPage,
                page * this.state.rowCountInPage)
        };

        this.setState(stateObj, this.props.updateParentState(stateObj));
    }

    handleRowSizeChange(ignore, rowCount) {
        let page = this.state.page;
        if (page > 1 && page > this.state.tableData.length / rowCount) {
            page--;
        }

        let stateObj = {
            rowCountInPage: rowCount,
            sortedTableData: this.state.tableData.slice((page - 1) * rowCount, (page - 1) * rowCount + rowCount),
            rowSize: rowCount,
            page: page
        };

        this.setState(stateObj, this.props.updateParentState(stateObj));
    }

    handleChangeToSecondTable(rowObj) {
        this.props.changeTableState(false);
        this.props.setResults(false, rowObj);
    }

    handleRowSelection(rowNumber, columnNumber, rowObj) {
        // This is for EMAIL_BREACHES_TYPE.
        if (rowObj) {
            //In case we are on the second table and want to see the email breach information on dialog
            if (rowObj.breach && rowObj.breach_nodeID) {
                const dataToSend = {
                    nodeID: rowObj.breach_nodeID.low ? rowObj.breach_nodeID.low : rowObj.breach_nodeID
                };

                $.ajax({
                    type: 'POST',
                    url: '/api/getEmailBreachInfo',
                    data: JSON.stringify(dataToSend),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((data) => {
                    if (data && Array.isArray(data)) {
                        if (data.length > 0) {
                            this.openExtraInfo(dataToSend.nodeID, rowObj.breach, data);
                        } else {
                            app.addAlert('info', 'No additional information found');
                        }
                    } else {
                        app.addAlert('error', 'Error: failed to retrieve breach information');
                    }
                }).fail(() => {
                    app.addAlert('error', 'Error: failed to retrieve breach information');
                });
                //In case we are on the first table
            } else {
                this.handleChangeToSecondTable(rowObj);
            }
            // This is for BLACKLISTS_TYPE.
        } else if (rowObj && rowObj.domain && rowObj.mx && rowObj.ns) {
            this.openExtraInfo(null, rowObj.domain, rowObj);
        }
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            this.setState({user: user});
            // In case it is desired to save user preferences, use: this.getAllCompaniesByID(user);
            this.getAllowedCompanies(true);
        }
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    doAfterSetResult() {
        if (this.props.assetUserInputAndMitigatedData && Array.isArray(this.props.assetUserInputAndMitigatedData)) {
            this.setState({assetUserInputAndMitigatedData: this.props.assetUserInputAndMitigatedData});
        }
    }

    closeFindingDialog() {
        this.setState({isFindingStatusDialogOpen: false, isAssetUserInputOrMitigatedBtnClicked: false});
    }

    updateFindingStatus(isCommentsAreaUpdate = false) {
        if(typeof isCommentsAreaUpdate !== 'boolean'){
            isCommentsAreaUpdate = false
        }
        let successMes = 'Finding status has been update!';
        let failedMes = 'Error: failed to update finding status.';

        if(isCommentsAreaUpdate){
            successMes = 'Comments log has been update!';
            failedMes = 'Error: failed to update comments log.';
        }

        let curRowData = this.state.curRowData;
        let shouldDetachMitigatedConnection = !!(this.state.isStatusMitigatedActive && this.state.findingStatusType && this.state.findingStatusType === 'mitigated');
        let type = this.state.findingStatusType;
        let isAssetUserInputUpdate = !!(curRowData &&
            ((curRowData.importance && typeof curRowData.importance === 'number') ||
                (curRowData.comments && Array.isArray(curRowData.comments))));

        if (curRowData && typeof curRowData === 'object' && curRowData.identifier && typeof curRowData.identifier === 'string'&& curRowData.identifier !== '' ) {
            let curOrgObj = {
                orgId: localStorage.getItem(ORGANIZATION_ID),
                orgName: localStorage.getItem(ORGANIZATION_NAME)
            };

            let objToUpdate = {val: curRowData.identifier, type: this.props.myDataType};

            //Getting assetUserData to update
            if(isAssetUserInputUpdate){
                if(curRowData && curRowData.importance){
                    objToUpdate.importance = curRowData.importance;
                    objToUpdate.comments = curRowData && curRowData.Comments && curRowData.Comments.props && curRowData.Comments.props.children &&
                        curRowData.Comments.props.children[1] && curRowData.Comments.props.children[1].props &&
                    curRowData.Comments.props.children[1].props.children ? curRowData.Comments.props.children[1].props.children : [];
                } else if(curRowData && curRowData.comments){
                    objToUpdate.importance = curRowData && curRowData.Importance.props && curRowData.Importance.props.value ? curRowData.Importance.props.value : 2;
                    objToUpdate.comments = curRowData.comments;
                } else {
                    objToUpdate.importance = 2;
                    objToUpdate.comments = [];
                }
            }

            let findingStatusData = {
                curOrgObj: curOrgObj,
                statusType: type,
                objToUpdate: objToUpdate,
                shouldDetachMitigatedConnection: shouldDetachMitigatedConnection,
                updateOnMongoDB: true,
                isAssetUserInputUpdate: isAssetUserInputUpdate,
                dataType: this.props.myDataType
            };

            $.ajax({
                type: 'POST',
                url: '/api/updateFindingStatus',
                async: true,
                data: JSON.stringify({findingStatusData}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((res) => {
                if (res && res.ok) {
                    if(res.assetUserInputAndMitigatedRes){
                        this.setState({assetUserInputAndMitigatedData: res.assetUserInputAndMitigatedRes});
                        this.props.updateStateForAMData(res.assetUserInputAndMitigatedRes);
                    }
                    if (this.props.myDataType === EMAIL_BREACHES_TYPE) {
                        this.props.setResults(
                            true,// onFirstTable
                            null,// rowObject
                            false);// needsToAddColumns
                    } else {
                        this.props.setResults();
                    }

                    if(this.state.orderKey && this.state.orderType){
                        this.handleSortOrderChange(this.state.orderKey, this.state.orderType)
                    }
                    this.updateLastPageUserWasOn();

                    this.setState({
                        isFindingStatusDialogOpen: false,
                        isAssetUserInputOrMitigatedBtnClicked: false,
                    });
                    app.addAlert('success', successMes);
                    //if user update comments area we do not recalculate the score again
                    if(!isCommentsAreaUpdate){
                        this.recalculateScore();
                    }
                    //if user mark asset as mitigated open comments dialog
                    if(!isAssetUserInputUpdate && type === 'mitigated' && !shouldDetachMitigatedConnection){
                        this.handleOpenCommentsDialog(curRowData, true);
                    }
                } else {
                    app.addAlert('error', failedMes);
                }
            }).fail(() => {
                app.addAlert('error', failedMes);
            });
        } else {
            app.addAlert('error', failedMes);
        }
    }

    isCurRowNeedToBeUpdateCurNode(curNode, curRowData){
        let isTrue = false;
        if(this.props.myDataType === BLACKLISTS_TYPE ||
            this.props.myDataType === DATALEAKS_TYPE ||
            this.props.myDataType === EMAIL_BREACHES_TYPE){
            if(curRowData && curRowData.identifier && curNode && curNode.id &&
                curRowData.identifier === curNode.id){
                isTrue = true;
            }
        }

        return isTrue;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps && typeof nextProps === 'object') {
            let stateObj = {};

            if (nextProps.hasOwnProperty('tableColumns')) {
                stateObj.tableColumns = nextProps.tableColumns;
            }

            if (nextProps.hasOwnProperty('tableData')) {
                stateObj.tableData = nextProps.tableData;

                if (!Array.isArray(nextProps.tableData) || nextProps.tableData.length === 0) {
                    stateObj.tableColumns = [{key: 'label', label: 'No data to display.'}];
                }
            }

            if (nextProps.hasOwnProperty('sortedTableData')) {
                stateObj.sortedTableData = nextProps.sortedTableData;
            }

            if (nextProps.hasOwnProperty('totalRowCount')) {
                stateObj.totalRowCount = nextProps.totalRowCount;
            }

            if (nextProps.hasOwnProperty('companies')) {
                stateObj.companies = nextProps.companies;
            }

            if (nextProps.hasOwnProperty('companiesInputVal')) {
                stateObj.companiesInputVal = nextProps.companiesInputVal;
            }

            if (nextProps.hasOwnProperty('companiesKeywords')) {
                stateObj.companiesKeywords = nextProps.companiesKeywords;
            } else if (nextProps.hasOwnProperty('companiesDomains')) {
                stateObj.companiesDomains = nextProps.companiesDomains;
            }
            if (nextProps.hasOwnProperty('isOnFirstTable')) {
                stateObj.isOnFirstTable = nextProps.isOnFirstTable;
            }

            if(nextProps && nextProps.assetUserInputAndMitigatedData){
                stateObj.assetUserInputAndMitigatedData =  nextProps.assetUserInputAndMitigatedData;
            }

            this.setState(stateObj);
        }
    }

    getAllCompaniesByID(user) {
        let ajaxURL;
        let dataConfigName;
        let dataConfigPropName;

        if (this.props && this.props.myDataType && this.props.myDataType === EMAIL_BREACHES_TYPE) {
            ajaxURL = '/aut/get_user_emailBreachesConfig';
            dataConfigName = 'emailBreachesConfig';
            dataConfigPropName = 'selectedCompanies';
        } else if (this.props && this.props.myDataType && this.props.myDataType === BLACKLISTS_TYPE) {
            ajaxURL = '/aut/get_user_blacklistedDomainsConfig';
            dataConfigName = 'blacklistedDomainsConfig';
            dataConfigPropName = 'selectedCompanies';

            /* Meaning that type is Dataleaks. */
        } else {
            ajaxURL = '/aut/get_user_dataleaksConfig';
            dataConfigName = 'dataleaksConfig';
            dataConfigPropName = 'selectedCompanies';
        }

        $.ajax({
            type: 'POST',
            url: ajaxURL,
            data: JSON.stringify({email: user.email}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data.ok && data[dataConfigName] && data[dataConfigName][dataConfigPropName]) {
                let selectedCompanies = data[dataConfigName][dataConfigPropName];
                this.getAllowedCompanies(false, selectedCompanies);
            } else {
                this.getAllowedCompanies(true);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load data');
            this.props.getData();
        });
    }

    getAllowedCompanies(isDefault, selectedCompanies) {
        $.ajax({
            type: 'GET',
            url: '/api/getAllCompaniesById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((companies) => {
            if (isDefault) {
                this.saveDefaultCompanies(companies);
            } else {
                this.props.saveLoadedCompanies(companies, selectedCompanies);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load companies.');
        });
    }

    saveDefaultCompanies(companies) {
        if (companies) {
            let pn = localStorage.getItem(PROJECT_NAME);

            if (pn) {
                $.ajax({
                    type: 'POST',
                    url: '/api/getCompaniesByProjectName',
                    async: true,
                    data: JSON.stringify({project: pn}),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((projectCompanies) => {
                    this.setDefaultCompaniesToState(companies, projectCompanies);
                }).fail(() => {
                    app.addAlert('error', "Error: failed to load project's companies.");
                });
            } else {
                this.setDefaultCompaniesToState(companies, null);
            }
        }
    }

    setDefaultCompaniesToState(companies, projectCompanies) {
        let companyPropToReturn = this.props.myDataType === DATALEAKS_TYPE ? "keywords" : "selectedDomains";

        let result = getDefaultCompanies(companies, projectCompanies, companyPropToReturn);

        if (result && result.companies && result.companiesInputVal && result.companiesPropArray) {
            if (result.companiesInputVal.length > 1) {
                // If the Session has a selection saved - load it.
                let sessionCompanies = sessionStorage.getItem(SESSION_COMPANIES);
                if (sessionCompanies) {
                    let selectedNames = sessionCompanies.split(', ');
                    let relatedCompanies = result.companiesInputVal.filter((c) => {
                        return selectedNames.includes(c.companyName);
                    });
                    if (relatedCompanies && relatedCompanies.length > 0) {
                        let relatedCompaniesPropArray = [];

                        if (companyPropToReturn === "selectedDomains") {
                            relatedCompanies.map((comp) => {
                                for (let i = 0; i < comp.selectedDomains.length; i++) {
                                    relatedCompaniesPropArray.push(comp.selectedDomains[i]);
                                }
                            });
                        } else if (companyPropToReturn === "keywords") {
                            relatedCompanies.map((comp) => {
                                for (let i = 0; i < comp.keywords.length; i++) {
                                    relatedCompaniesPropArray.push(comp.keywords[i]);
                                }
                            });
                        }
                        result.companiesPropArray = relatedCompaniesPropArray;
                        result.companiesInputVal = relatedCompanies;
                    }
                } else {
                    // Select only the first company as a default selection.
                    result.companiesInputVal.splice(1, result.companiesInputVal.length);
                }
            }
            this.props.setDefaultCompanies(result);
        }
    }

    updateSettingsOfUser() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let selectedCompanies = [];

            if (this.state.companiesInputVal) {
                selectedCompanies = this.state.companiesInputVal.map((company) => {
                    if (company.id && company.companyName) {
                        return {companyName: company.companyName, id: company.id};
                    }
                });
            }

            let data = {
                email: user.email,
                selectedCompanies: selectedCompanies
            };

            let ajaxURL;

            if (this.props && this.props.myDataType && this.props.myDataType === EMAIL_BREACHES_TYPE) {
                ajaxURL = '/aut/update_user_emailBreachesConfig';
            } else if (this.props && this.props.myDataType && this.props.myDataType === BLACKLISTS_TYPE) {
                ajaxURL = '/aut/update_user_blacklistedDomainsConfig';

                /* Meaning that type is Dataleaks. */
            } else {
                ajaxURL = '/aut/update_user_dataleaksConfig';
            }

            $.ajax({
                type: 'POST',
                url: ajaxURL,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data.ok) {
                    app.addAlert('success', 'Changes have been saved successfully!');
                } else {
                    app.addAlert('error', 'Error: failed to save changes');
                }
                this.props.getData();
            }).fail(() => {
                app.addAlert('error', 'Error: failed to save changes');
                this.props.getData();
            });
        }
    }

    ApplyChangesFromSettings(companiesToShow, companiesPropData) {
        if (companiesToShow && companiesPropData) {
            // Show only selected companies results.
            let stateObj = {companiesInputVal: companiesToShow};

            if (this.props && this.props.myDataType &&
                (this.props.myDataType === EMAIL_BREACHES_TYPE || this.props.myDataType === BLACKLISTS_TYPE)) {
                stateObj.companiesDomains = companiesPropData;

                /* Meaning that type is Dataleaks */
            } else {
                stateObj.companiesKeywords = companiesPropData;
            }
            this.setState(stateObj,
                this.props.updateParentState(stateObj,
                    this.updateSettingsOfUser)
            );
        }
    }

    render() {
        const tableBodyStyle = {
            'overflowX': 'auto'
        };

        const tableStyle = {
            'width': '95%',
            'margin': 'auto'
        };

        const settingsBtnStyle = {
            'marginTop': '10px',
            'marginLeft': '10px'
        };

        const settingsChipsStyle = {
            'display': 'inline-flex'
        };

        const downloadStyle = {
            cursor: 'pointer'
        };

        let onCellClickFunc;

        if (this.state.isAssetUserInputOrMitigatedBtnClicked) {
            onCellClickFunc = function () {};
        } else if (this.props && this.props.enableRowClick) {
            onCellClickFunc = this.handleRowSelection;
        } else {
            onCellClickFunc = function () {};
        }

        let dataCredits;
        let companiesPropData;
        let fileNameCSV;
        let defaultPropForCSV;

        if (this.props && this.props.myDataType && this.props.myDataType === EMAIL_BREACHES_TYPE) {
            companiesPropData = this.state.companiesDomains;
            dataCredits =
                <HIBP_Credits style={{float: 'right'}}/>;
            fileNameCSV = 'rescana_breaches.csv';
            defaultPropForCSV = 'Emails Found in Breaches';
        } else if (this.props && this.props.myDataType && this.props.myDataType === BLACKLISTS_TYPE) {
            companiesPropData = this.state.companiesDomains;
            dataCredits = '';
            fileNameCSV = 'rescana_blacklists.csv';
            defaultPropForCSV = 'Blacklists';
        } else {
            companiesPropData = this.state.companiesKeywords;
            dataCredits = '';
            fileNameCSV = 'rescana_dataleaks.csv';
            defaultPropForCSV = 'Dataleaks';
        }

        let csvData;
        let csvColumns;

        if (!this.props || !this.props.tableDataToDownload || !Array.isArray(this.props.tableDataToDownload) || !this.props.tableDataToDownload.length
            || !this.props.tableColumnsToDownload) {
            let csvDefaultObj = {};

            csvDefaultObj[defaultPropForCSV] = 'No data found.';
            csvData = [csvDefaultObj];
            csvColumns = [defaultPropForCSV];
        } else {
            //in case we are on email breaches table and we want to download the SECOND table
            if (this.props.tableDataToDownload &&
                this.props.tableDataToDownload[0] &&
                this.props.tableDataToDownload[0].breach &&
                this.props.myDataType === EMAIL_BREACHES_TYPE) {
                csvColumns =  [
                    {label: 'Email', key: 'email'},
                    {label: 'Appeared in Breach', key: 'breach'},
                    {label: 'Breach Date', key: 'breach_date'},
                    {label: 'Compromised Data', key: 'breach_classes'},
                    {label: 'Related Companies', key: 'relatedCompanies'}
                ];
            //in case we are on email breaches table and we want to download the FIRST table
            } else if(this.props.myDataType === EMAIL_BREACHES_TYPE) {
                csvColumns =  [
                    {label: 'Email', key: 'email'},
                    {label: 'Related Companies', key: 'relatedCompanies'},
                    {label: 'Importance', key: 'Importance'},
                    {label: 'Comments', key: 'Comments'},
                    {label: 'Mitigated', key: 'Mitigated'},
                    ];
            //all other cases
            } else {
                csvColumns = this.props.tableColumnsToDownload;
            }
            csvData = this.props.tableDataToDownload;
        }

        let downloadCSV = [
            <div>
                <CSVLink
                    data={csvData}
                    headers={csvColumns}
                    filename={fileNameCSV}>
                    <FileDownloadIcon
                        style={downloadStyle}>
                    </FileDownloadIcon>
                </CSVLink>
            </div>
        ];

        let componentTitle = (defaultPropForCSV) ? defaultPropForCSV + ' ' : '';
        componentTitle += 'Findings For';

        let findingCurType = this.state.findingCurType;
        if(findingCurType && findingCurType.toLocaleLowerCase() === 'importance'){
            findingCurType = 'importance/status';
        }

        let dataType = this.props.myDataType;
        if(dataType && typeof dataType === 'string'){
            if (dataType === DATALEAKS_TYPE) {
                dataType = 'dataleak\'s';
            } else if (dataType === BLACKLISTS_TYPE) {
                dataType = 'blacklist\'s';
            } else if (dataType === EMAIL_BREACHES_TYPE) {
                dataType = 'email\'s';
            } else {
                dataType = '';
            }
        }
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Card style={{margin: 10}}>
                        <CardHeader
                            title={componentTitle}
                        />
                        <IconButton
                            tooltip="Filter by companies"
                            touch={true}
                            tooltipPosition="bottom-right"
                            style={settingsBtnStyle}>
                            <SettingsButton
                                onTouchTap={this.handleOpenSettings.bind(this)}
                            />
                        </IconButton>
                        <div style={settingsChipsStyle}>
                            {showExpandedDataChip(this.state.companiesInputVal && this.state.companiesInputVal.map((cmp) => {
                                return (cmp.companyName);
                            }), 8, 5, 'Companies')}
                        </div>
                        <DataTableTemplateSettingsModal
                            open={this.state.openSettingsModal}
                            closeDialog={this.closeSettingsDialog.bind(this)}
                            companies={this.state.companies}
                            companiesInputVal={this.state.companiesInputVal}
                            companiesPropData={companiesPropData}
                            ApplyChangesFromSettings={this.ApplyChangesFromSettings.bind(this)}
                            myDataType={this.props.myDataType}
                        />
                    </Card>
                    <Card style={{margin: 10, display: 'inline-block'}}>
                        <DataTables
                            height={'auto'}
                            selectable={false}
                            showRowHover={true}
                            showHeaderToolbar={true}
                            tableBodyStyle={tableBodyStyle}
                            tableStyle={tableStyle}
                            columns={this.state.tableColumns}
                            data={this.state.sortedTableData}
                            showCheckboxes={false}
                            onCellClick={onCellClickFunc}
                            onFilterValueChange={this.handleFilterValueChange}
                            onSortOrderChange={this.handleSortOrderChange}
                            onNextPageClick={this.handleNextPageClick}
                            onRowSizeChange={this.handleRowSizeChange}
                            onPreviousPageClick={this.handlePreviousPageClick}
                            headerToolbarMode={'filter'}
                            page={this.state.page}
                            rowSize={this.state.rowSize}
                            count={this.state.totalRowCount}
                            toolbarIconRight={downloadCSV}
                        />
                        {dataCredits}
                        <DataTableTemplateInfoModal
                            user={this.state.user}
                            open={this.state.openExtraInfo}
                            closeDialog={this.closeExtraInfo.bind(this)}
                            name={this.state.extraInfoName}
                            data={this.state.extraInfoData}
                            myDataType={this.props.myDataType}
                        />
                        <DynamicDialog
                            open={this.state.isFindingStatusDialogOpen}
                            title={'Change Finding Status'}
                            bodyText={'Are you sure you wish to change the ' + dataType + ' '+ findingCurType +'?'}
                            btnOkLable={'Yes'}
                            btnCancelLable={'No'}
                            cancelBtnEnabled={true}
                            handlebBtnOkClick={this.updateFindingStatus.bind(this,)}
                            handlebBtnCancelClick={this.closeFindingDialog.bind(this)}
                        />
                        <AssetUserInputCommentsDialog
                            open={this.state.isCommentsDialogOpen}
                            title={'Comments'}
                            updateCommentsArea={this.handleUpdateCommentsArea.bind(this)}
                            handlebBtnCloseClick={this.handleCloseCommentsDialog.bind(this)}
                            curCommentsArr={this.state.curCommentsArr || []}
                            isFromMitigated={this.state.isFromMitigated}
                        />
                    </Card>
                </div>
            </MuiThemeProvider>
        );
    }
}

DataTableTemplate.propTypes = {
    myDataType: PropTypes.string.isRequired,
    tableColumns: PropTypes.array.isRequired,
    tableData: PropTypes.array.isRequired,
    sortedTableData: PropTypes.array.isRequired,
    totalRowCount: PropTypes.number.isRequired,
    companies: PropTypes.array.isRequired,
    companiesInputVal: PropTypes.array.isRequired,
    updateParentState: PropTypes.func.isRequired,
    setResults: PropTypes.func.isRequired,
    updateStateForAMData: PropTypes.func.isRequired,
    getData: PropTypes.func.isRequired,
    setDefaultCompanies: PropTypes.func.isRequired,
    saveLoadedCompanies: PropTypes.func.isRequired,
    changeTableState: PropTypes.func.isRequired,
    companiesDomains: PropTypes.array,
};
