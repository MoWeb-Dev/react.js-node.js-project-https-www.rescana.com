import {findCompanyIndexByID} from '../../dataleaks/dataleaks-helpers';

export function getLoadedCompaniesObject(companies, selectedCompanies, companyPropToReturn) {
    let companiesInput = [], companiesPropArray = [];

    if (companies && selectedCompanies &&
        companyPropToReturn &&
        Array.isArray(companies) &&
        Array.isArray(selectedCompanies) &&
        typeof companyPropToReturn === 'string') {
        companiesInput = selectedCompanies.map((item) => {
            if (item.id && item.companyName) {
                let currProp = [];
                const companyIndex = findCompanyIndexByID(companies, item.id);

                if (companyIndex > -1 &&
                    companies[companyIndex][companyPropToReturn]) {
                    currProp = companies[companyIndex][companyPropToReturn];
                    currProp.map((prop) => {
                        if (companiesPropArray.indexOf(prop) === -1) {
                            companiesPropArray.push(prop);
                        }
                    });
                }
                const selectedInputObj = {
                    companyName: item.companyName,
                    id: item.id
                };
                selectedInputObj[companyPropToReturn] = currProp;
                return selectedInputObj;
            }
        });
    }

    return {
        companiesInputVal: companiesInput,
        companiesPropArray: companiesPropArray
    };
}

export function getDefaultCompanies(companies, projectCompanies, companyPropToReturn) {
    let companiesForDD = [];
    const companiesSelected = [];
    const companiesPropArray = [];

    if (companies &&
        Array.isArray(companies) &&
        companyPropToReturn &&
        typeof companyPropToReturn === 'string') {
        companiesForDD = companies.map((item) => {
            const obj = {companyName: item.companyName, id: item.id || item._id};
            obj[companyPropToReturn] = item[companyPropToReturn] || [];

            // If localStorage has no selected projectName - show all
            // data from Prop(like domains email breaches).
            // If localStorage has a selected projectName - show only
            // related data from Prop(like domains email breaches).
            if ((!projectCompanies) ||
                (projectCompanies && projectCompanies.find((pc) => {
                    return pc.companyName === item.companyName;
                }))) {
                companiesSelected.push(obj);

                if (item[companyPropToReturn]) {
                    item[companyPropToReturn].map((prop) => {
                        if (companiesPropArray.indexOf(prop) === -1) {
                            companiesPropArray.push(prop);
                        }
                    });
                }
            }
            return obj;
        });
    }

    return {
        companies: companiesForDD,
        companiesInputVal: companiesSelected,
        companiesPropArray: companiesPropArray
    };
}
