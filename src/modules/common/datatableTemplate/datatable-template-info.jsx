import React from 'react';
import Dialog from 'material-ui/Dialog';
import IconButton from 'material-ui/IconButton';
import NavClose from 'material-ui/svg-icons/navigation/close';
import {EMAIL_BREACHES_TYPE, SSL_CERTS_TYPE} from '../../../../config/consts';
import HIBP_Credits from '../../breaches/hibp-credits.jsx';

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class DataTableTemplateInfoModal extends React.Component {
    constructor(props) {
        super(props);
    }

    getFindingForDisplay(finding) {
        let displayFinding;

        if (finding != null) {
            if (Array.isArray(finding) || typeof finding === 'object') {
                displayFinding = JSON.stringify(finding);
            } else {
                displayFinding = finding.toString();
            }
        } else {
            displayFinding = '';
        }
        return displayFinding;
    }

    getDemoModeText(data, domainName, idx = 0) {
        let displayFinding = '';

        if (this.props.myDataType === SSL_CERTS_TYPE ) {
            if(data && data.name && data.finding && domainName){
                let re = new RegExp(domainName, 'g');
                if(typeof data.finding === 'object' || Array.isArray(data.finding)){
                    displayFinding = JSON.stringify(data.finding);
                    displayFinding = displayFinding.toString().replace(re, "domain-example-" + idx + '.com');
                } else {
                    displayFinding = data.finding.toString().replace(re, "domain-example-" + idx + '.com');
                }

            }
        } else {
            displayFinding = this.getFindingForDisplay(data.finding);
        }

        return displayFinding;
    }

    render() {
        if (this.props) {
            let isInDemoMode = this.props.user && this.props.user.isInDemoMode;
            const topHeaderStyle = {display: 'flex'};
            const headerStyle = {margin: '20px',textAlign: 'center', width: '100%',fontSize: '20px'};
            const dataDivStyle = {display: 'inline-grid'};
            const descriptionStyle = {whiteSpace: 'pre-wrap', fontSize: '16px'};

            let dialogTitle;

            if (this.props.name) {
                dialogTitle = 'Information About ' + this.props.name;

                if (this.props.myDataType === EMAIL_BREACHES_TYPE) {
                    dialogTitle += ' Breach';
                }
            } else {
                dialogTitle = 'Information';
            }

            let dialogTitleElement = <div style={topHeaderStyle}>
                <div style={headerStyle}>
                    <span style={{marginRight: '15px', fontSize: '45px', color: '#ff4081'}}>|</span>
                    <span style={{ position: 'relative', top: '-6px', fontSize: 22,}}>{dialogTitle}</span>
                </div>
                <br/>
                <div>
                    <IconButton
                        tooltip="close"
                        touch={true}
                        tooltipPosition="bottom-left"
                        onClick={this.props.closeDialog}>
                        <NavClose/>
                    </IconButton>
                </div>
            </div>;

            let actions = [];
            let domainName = '';

            if (this.props.myDataType === EMAIL_BREACHES_TYPE) {
                actions.push(<HIBP_Credits/>);
            } else {
                // Add spacing at the bottom.
                actions.push(<div style={{margin: '10px'}}/>);
                this.props.data && this.props.data.map((curData)=>{
                    if(curData && curData.finding && curData.name && curData.name === 'Domain'){
                        domainName = curData.finding;
                    }
                });
            }

            let descriptionData = (this.props.description) ? <p style={descriptionStyle}>{this.props.description}</p> : null;

            let idx = 1;
            return (
                <div>
                    <Dialog
                        title={dialogTitleElement}
                        actions={actions}
                        modal={false}
                        actionsContainerStyle={{paddingBottom: 20, paddingRight: 20}}
                        open={this.props.open}
                        onRequestClose={this.props.closeDialog}
                        autoScrollBodyContent={true}>
                        <div style={dataDivStyle}>
                            {descriptionData}
                            {(this.props.data && this.props.data.map((currData, i) => {
                                if (currData && currData.name && currData.finding != null) {
                                    return <div key={i}>
                                        <b>{currData.name}: </b>
                                            {<span>{isInDemoMode? this.getDemoModeText(currData, domainName, idx): this.getFindingForDisplay(currData.finding)}</span>}
                                    </div>;
                                }
                            }))}
                        </div>
                    </Dialog>
                </div>
            );
        }
    }
};
