import {Card, CardHeader} from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import SettingsButton from 'material-ui/svg-icons/editor/mode-edit';
import React from 'react';
import DataTableTemplateSettingsModal from './datatable-template-settings.jsx';
import {showExpandedDataChip} from '../UI-Helpers.jsx';
import PropTypes from 'prop-types';
import {BOTNETS_TYPE, GDPR_NOT_COMPLY_TYPE, WIFI_NETWORKS_TYPE} from '../../../../config/consts.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";

// Note: This component works right now only with botnets.
export default class SettingsFilterTemplate extends React.Component {
    constructor() {
        super();

        this.state = {
            openSettingsModal: false,
            companies: [],
            companiesInputVal: [],
            companiesDomains: []
        };
    }

    handleOpenSettings() {
        this.setState({
            openSettingsModal: true
        });
    }

    closeSettingsDialog() {
        this.setState({openSettingsModal: false});
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps && typeof nextProps === 'object') {
            let stateObj = {};

            if (nextProps.hasOwnProperty('companies')) {
                stateObj.companies = nextProps.companies;
            }
            if (nextProps.hasOwnProperty('companiesInputVal')) {
                stateObj.companiesInputVal = nextProps.companiesInputVal;
            }
            if (nextProps.hasOwnProperty('companiesDomains')) {
                stateObj.companiesDomains = nextProps.companiesDomains;
            }

            this.setState(stateObj);
        }
    }

    ApplyChangesFromSettings(companiesToShow, companiesPropData) {
        if (companiesToShow && companiesPropData) {
            // Show only selected companies results.
            let stateObj = {
                companiesInputVal: companiesToShow,
                companiesDomains: companiesPropData
            };

            this.setState(stateObj,
                this.props.updateParentState(stateObj)
            );
        }
    }

    render() {
        const cardStyle = {
            'margin': '10px'
        };

        const settingsBtnStyle = {
            'marginTop': '10px',
            'marginLeft': '10px'
        };

        const settingsChipsStyle = {
            'display': 'inline-flex'
        };

        let cardTitle = 'Findings For';

        if (this.props.myDataType) {
            if (this.props.myDataType === BOTNETS_TYPE) {
                cardTitle = 'Botnets ' + cardTitle;
            } else if (this.props.myDataType === GDPR_NOT_COMPLY_TYPE) {
                cardTitle = 'Web Privacy Policy ' + cardTitle;
            } else if (this.props.myDataType === WIFI_NETWORKS_TYPE) {
                cardTitle = 'Wifi Networks ' + cardTitle;
            } else {
                // Meaning this type is sslCerts.
                cardTitle = 'SSL Certificates ' + cardTitle;
            }
        }

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <Card style={cardStyle}>
                    <CardHeader
                        title={cardTitle}
                    />
                    <IconButton
                        tooltip="Filter by companies"
                        touch={true}
                        tooltipPosition="bottom-right"
                        style={settingsBtnStyle}>
                        <SettingsButton
                            onTouchTap={this.handleOpenSettings.bind(this)}
                        />
                    </IconButton>
                    <div style={settingsChipsStyle}>
                        {showExpandedDataChip(this.state.companiesInputVal && this.state.companiesInputVal.map((cmp) => {
                            return (cmp.companyName);
                        }), 8, 5, 'Companies')}
                    </div>
                    <DataTableTemplateSettingsModal
                        open={this.state.openSettingsModal}
                        closeDialog={this.closeSettingsDialog.bind(this)}
                        companies={this.state.companies}
                        companiesInputVal={this.state.companiesInputVal}
                        companiesPropData={this.state.companiesDomains}
                        ApplyChangesFromSettings={this.ApplyChangesFromSettings.bind(this)}
                        myDataType={this.props.myDataType}
                        title={'Botnets Filter By Companies'}
                    />
                </Card>
            </MuiThemeProvider>
        );
    }
}

SettingsFilterTemplate.propTypes = {
    myDataType: PropTypes.string.isRequired,
    companies: PropTypes.array.isRequired,
    companiesInputVal: PropTypes.array.isRequired,
    updateParentState: PropTypes.func.isRequired,
    companiesDomains: PropTypes.array
};
