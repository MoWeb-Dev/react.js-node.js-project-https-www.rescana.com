import {LANG_EN, LTR, RTL, AUTH_USER} from '../../../config/consts';

export function isLTRLanguage(lang) {
    return (lang === LANG_EN);
}

export function getLanguageDir(lang) {
    return (isLTRLanguage(lang)) ? LTR : RTL;
}

export function isAuth(ignore, replace, callback) {
    let user = localStorage.getItem(AUTH_USER);

    try {
        user = JSON.parse(user);
    } catch (e) {
        user = null;
    }

    if (!user) {
        replace({
            pathname: '/login'
        });
        callback();
    } else {
        const uid = user.id;
        $.ajax({
            type: 'post',
            url: '/api/isAuthUser',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({uid: uid}),
            dataType: 'json'
        }).done((data) => {
            if (data && !data.ok) {
                replace({
                    pathname: '/dashboard'
                });
                if (data.error) {
                    alert(data.error);
                }
            }
            callback();
        }).fail(() => {
            replace({
                pathname: '/dashboard'
            });
            alert('The server can\'t process this request right now.');
            callback();
        });
    }
}
