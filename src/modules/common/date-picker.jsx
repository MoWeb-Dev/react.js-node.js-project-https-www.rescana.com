import React, {PropTypes} from 'react';
import {DateRange} from 'react-date-range';

/**
 * This component handles date ranges.
 * Input should be a function to handle a range change.
 * Output will be an object with two keys,
 * 'startDate' and 'endDate' which are Momentjs objects.
 */
export class DatePicker extends React.Component {
    constructor() {
        super();
    }

    handleSelect(range) {
        // An object with two keys,
        // 'startDate' and 'endDate' which are Momentjs objects.
        this.setState({startDate: range.startDate, endDate: range.endDate});
    }

    render() {
        const DateRangeTheme = {
            DateRange: {
                background: '#ffffff'
            },
            Calendar: {
                background: 'transparent',
                color: '#95a5a6'
            },
            MonthAndYear: {
                background: '#303030',
                color: '#ffffff'
            },
            MonthButton: {
                background: '#ffffff'
            },
            MonthArrowPrev: {
                borderRightColor: '#303030'
            },
            MonthArrowNext: {
                borderLeftColor: '#303030'
            },
            Weekday: {
                background: '#303030',
                color: '#ffffff'
            },
            Day: {
                transition: 'transform .1s ease, box-shadow .1s ease, background .1s ease'
            },
            DaySelected: {
                background: '#00B8D4'
            },
            DayActive: {
                background: '#00ACC1',
                boxShadow: 'none'
            },
            DayInRange: {
                background: '#0097A7',
                color: '#fff'
            },
            DayHover: {
                background: '#ffffff',
                color: '#7f8c8d',
                transform: 'scale(1.1) translateY(-10%)',
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.4)'
            }
        };


        return (
            <DateRange
                onChange={(this.props.onChange) ? this.props.onChange : this.handleSelect.bind(this)}
                theme={DateRangeTheme}
            />
        );
    }
}

DatePicker.propTypes = {
    onChange: PropTypes.func.isRequired
};

export default DatePicker;
