import React from 'react';
import Chip from 'material-ui/Chip';
import ReactTooltip from 'react-tooltip';
import {CVE_LOW_MAX, CVE_MEDIUM_MIN, CVE_MEDIUM_MAX, CVE_HIGH_MIN, CVE_HIGH_MAX} from '../../../config/consts';

const shortid = require('shortid');

export function showExpandedDataChip(expandedData, numToExpand, linesBreak, dataDetails = '') {
    const styles = {
        chip: {
            margin: 4
        }
    };

    if (expandedData) {
        if (expandedData.length <= numToExpand) {
            return expandedData.map((data) => (
                <Chip key={data} style={styles.chip}>{data}</Chip>));
        } else {
            let dataTips = '';

            for (let i = 0; i < expandedData.length; i++) {
                dataTips += expandedData[i];
                if (i !== expandedData.length - 1) {
                    if ((i + 1) % linesBreak === 0) {
                        dataTips += '<br/>';
                    } else {
                        dataTips += ', ';
                    }
                }
            }

            if (dataDetails) {
                dataDetails = ' ' + dataDetails;
            }

            return <Chip key={shortid.generate()} style={styles.chip} data-tip={dataTips}>
                <ReactTooltip place="right" multiline={true}/>{expandedData.length + dataDetails}</Chip>;
        }
    } else {
        return null;
    }
}

export function getScoreColor(score) {
    if (score == null) {
        return 'black';
    }
    if (score < CVE_LOW_MAX) {
        return '#0091ea';
    }
    if (score >= CVE_MEDIUM_MIN && score <= CVE_MEDIUM_MAX) {
        return '#F57C00';
    }
    if (score > CVE_HIGH_MIN && score < CVE_HIGH_MAX) {
        return 'red';
    } else {
        return 'black';
    }
}
