import FeedItem from './brand-feed-item.jsx';
import React from 'react';
import Dialog from 'material-ui/Dialog';
import Card from 'material-ui/Card';

class MainContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            data: [],
            openModal: false,
            icon: null,
            url: null
        };

        this.closeModal = this.closeModal.bind(this);
    }

    openModal(icon, url) {
        this.setState({openModal: true, icon: icon, url: url});
    }

    closeModal() {
        this.setState({openModal: false});
    }

    getFeed(aid) {
        $.ajax({
            type: 'get',
            url: '/api/feed',
            data: {aid: aid},
            dataType: 'json'
        }).done((data) => {
            this.showFeed(data);
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    handleRemoveItem(itemId, parentId) {
        $.ajax({
            type: 'post',
            url: '/api/removePublicApp',
            data: {id: itemId, aid: parentId},
            dataType: 'json'
        }).done(() => {
            this.getFeed(parentId);
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    showFeed(data) {
        this.props.brandComponent.feedContainer.setState({data: data});
    }

    render() {
        let emptyFeed = !!this.state.data.length;
        let feedList;
        let initExpand;

        this.props.brandComponent.feedContainer = this;

        initExpand = this.state.data.length === 1;

        if (emptyFeed) {
            feedList = this.state.data.sort(
                function(a, b) {
                    return b['isWatched'] - a['isWatched'];
                }).map((dataItem) => {
                if (dataItem.url && dataItem.url.indexOf('http://') === -1) {
                    dataItem.url = 'http://' + dataItem.url;
                }
                return <FeedItem
                    key={dataItem.id}
                    aid={dataItem.aid}
                    url={dataItem.url}
                    icon={dataItem.icon}
                    contentScore={dataItem.contentScore ? dataItem.contentScore : '0'}
                    screenShotScore={dataItem.screenShotScore ? dataItem.screenShotScore : '0'}
                    domainScore={dataItem.domainScore ? dataItem.domainScore : '0'}
                    geoIPCountry={dataItem.geoIPCountry}
                    dnsARecord={dataItem.dnsARecord}
                    dnsMx={dataItem.dnsMx}
                    dnsNs={dataItem.dnsNs}
                    reason={dataItem.reason}
                    isWatched={dataItem.isWatched}
                    initExpand={initExpand}
                    openModal={this.openModal.bind(this, dataItem.icon, dataItem.url)}
                    onRemove={this.handleRemoveItem.bind(this, dataItem.id, dataItem.aid)}
                />;
            });
        } else {
            feedList = <div style={{
                textAlign: 'center',
                position: 'fixed',
                top: '50%',
                left: '60%'
            }}>
                There are no feed items
            </div>;
        }

        return (
            <div style={{
                float: 'right',
                width: '74.7%',
                margin: '40px 25px 25px 0px'
            }}>
                {feedList}
                <Dialog
                    style={{padding: 200}}
                    title={this.state.url}
                    modal={false}
                    open={this.state.openModal}
                    onRequestClose={this.closeModal}
                >
                    <img src={this.state.icon} alt=''/>
                </Dialog>
            </div>
        );
    }
}

export default MainContainer;
