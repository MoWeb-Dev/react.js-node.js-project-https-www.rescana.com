import {Card, CardText, CardHeader} from 'material-ui/Card';
import React from 'react';
import Delete from 'material-ui/svg-icons/action/delete';
import Chip from 'material-ui/Chip';
import {cyan100, cyanA700} from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';

const imageStyle = {
        width: 80,
        height: 80,
        cursor: 'pointer'
    },
    thumbStyle = {
        float: 'left',
        overflow: 'hidden'
    },
    descriptionStyle = {
        float: 'left',
        fontSize: 18,
        paddingTop: 30
    },
    scoreStyle = {
        float: 'right',
        fontSize: 15,
        paddingRight: 40
    },
    moreInfoStyle = {
        width: '60%',
        padding: '0px 0px 20px 30px'
    },
    removeBtnCTStyle = {
        padding: 0,
        float: 'right'
    },
    removeBtnStyle = {
        color: '#757575'
    },
    watchedChip = {
        padding: 0,
        marginTop: '80px',
        marginRight: '-29px',
        float: 'right'
    },
    chip = {
        margin: 4
    };

class FeedItem extends React.Component {
    constructor() {
        super();
        this.state = {
            expanded: false,
            phishImg: '/svg/no_image.svg'
        };

        this.removePublicApp = this.removePublicApp.bind(this);
        this.handleExpandChange = this.handleExpandChange.bind(this);
        this.handleImageError = this.handleImageError.bind(this);
    }

    handleExpandChange(expanded) {
        this.setState({expanded: expanded});
    }

    removePublicApp() {
        this.setState({expanded: false});
        this.props.onRemove();
    }

    handleImageError() {
        this.setState({phishImg: '/svg/no_image.svg'});
    }

    componentDidMount() {
        this.handleExpandChange(this.props.initExpand);

        if (this.props.icon) {
            this.setState({phishImg: this.props.icon});
        }
    }

    render() {
        let contentScoreText = '';
        let URLScoreText = '';
        let screenShotScoreText = '';
        let deletePublicAppCardText = '';
        let watchedCardText = '';

        if (this.props.contentScore) {
            contentScoreText = 'Content Resemblance : ' + this.props.contentScore + '%';
        }

        if (this.props.domainScore) {
            URLScoreText = 'URL Resemblance : ' + this.props.domainScore + '%';
        }

        if (this.props.screenShotScore) {
            screenShotScoreText = 'Screenshot Resemblance : ' + this.props.screenShotScore + '%';
        }

        if (this.props.isWatched) {
            deletePublicAppCardText = (<CardText style={removeBtnCTStyle}>
                <Delete onClick={this.removePublicApp} style={removeBtnStyle}/>
            </CardText>);

            watchedCardText = (<CardText style={watchedChip}>
                <Chip
                    backgroundColor={cyanA700}
                    labelColor={cyan100}
                    style={chip}
                >
                    Watched
                </Chip>
            </CardText>);
        }

        return (
            <Card expanded={this.state.expanded} onExpandChange={this.handleExpandChange} initiallyExpanded={this.state.expanded}>
                <CardHeader actAsExpander={true} >
                    <CardText style={thumbStyle}>
                        <img
                            src={this.state.phishImg} style={imageStyle} alt='' onClick={this.props.openModal}
                            onError={this.handleImageError}/>
                    </CardText>
                    <CardText style={descriptionStyle}>
                        <a href={this.props.url} target="_blank"> {this.props.url} </a>
                    </CardText>
                    {deletePublicAppCardText}
                    {watchedCardText}
                    <CardText style={scoreStyle} title={this.props.reason}>
                        {screenShotScoreText}<br/>
                        {URLScoreText}<br/>
                        {contentScoreText}
                    </CardText>
                    <div style={{clear: 'both'}}/>
                </CardHeader>
                <Divider/>
                <CardText style={moreInfoStyle} expandable={true} >
                    <h5>Additional info</h5>
                    Geo IP Country : {this.props.geoIPCountry ? this.props.geoIPCountry : 'N/A'}<br />
                    DNS A Record : {this.props.dnsARecord ? this.props.dnsARecord : 'N/A'}<br />
                    DNS MX : {this.props.dnsMx ? this.props.dnsMx : 'N/A'}<br />
                    DNS NS : {this.props.dnsNs ? this.props.dnsNs : 'N/A'}<br />
                </CardText>
            </Card>
        );
    }
}

export default FeedItem;
