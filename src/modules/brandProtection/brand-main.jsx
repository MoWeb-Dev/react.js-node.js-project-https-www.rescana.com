import LeftList from './left-menu.jsx';
import MainContainer from './brand-main-container.jsx';
import React from 'react';
import DialogWithAddNew from '../app/modal-add-new.jsx';
import 'jquery';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import classNames from 'classnames';
import themeForFont from "../app/themes/themeForFont";
import textForTitleExpl from "../appGeneralFiles/text-for-title-explanations";
import TitleExplanationComponent from '../appGeneralFiles/title-explanation-component.jsx';

export class Brand extends React.Component {
    constructor() {
        super();
        this.render = this.render.bind(this);
        this.state = {context: {aid: ''}, openInfoBox: false};
    }

    onSearch(data) {
        this.setState({brands: data});
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    render() {
        let pageMainClass = classNames({
            'containerLTR': true
        });

        let infoText = textForTitleExpl.brand.expl;
        let shorterInfoText = textForTitleExpl.brand.explShort;

        return (
            <div className={pageMainClass}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <div>
                        <div style={{margin: '58px 0px 0px 26px'}}>
                            <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                            <span style={{position: 'relative', top: '-6px', fontSize: 22}}>SOCIAL ENGINEERING</span>
                        </div>
                        <br/>
                        <br/>
                        <div style={{marginLeft: 15}}>
                            <TitleExplanationComponent
                                openInfoBox={this.state.openInfoBox}
                                anchorEl={this.state.anchorEl}
                                shorterInfoText={shorterInfoText}
                                infoText={infoText}
                                handleCloseInfoBox={this.handleCloseInfoBox.bind(this)}
                                handleOpenInfoBox={this.handleOpenInfoBox.bind(this)}
                            />
                        </div>
                        <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                            <LeftList brandComponent={this}/>
                        </MuiThemeProvider>
                        <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                            <MainContainer brandComponent={this}/>
                        </MuiThemeProvider>
                        <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                            <DialogWithAddNew brandComponent={this} onSearch={this.onSearch.bind(this)}/>
                        </MuiThemeProvider>
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}

export default Brand;
