import Delete from 'material-ui/svg-icons/action/delete';
import {ListItem} from 'material-ui/List';
import React from 'react';

class LeftMenuItem extends React.Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
        this.getFeed = this.getFeed.bind(this);
        this.showFeed = this.showFeed.bind(this);
    }

    handleClick() {
        this.getFeed();
    }

    getFeed() {
        $.ajax({
            type: 'get',
            url: '/api/feed',
            data: {aid: this.props.appId},
            dataType: 'json'
        }).done((data) => {
            this.showFeed(data);
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    showFeed(data) {
        this.props.brandComponent.feedContainer.setState({data: data});
    }

    render() {
        return (
            <ListItem
                value = {this.props.idx}
                primaryText={this.props.primaryText}
                // appId={this.props.key}
                key={this.props.key}
                appId={this.props.appId}
                onClick={this.handleClick}
                rightIcon={<Delete onClick={this.props.removeApp} />}
            />
        );
    }
}

export default LeftMenuItem;
