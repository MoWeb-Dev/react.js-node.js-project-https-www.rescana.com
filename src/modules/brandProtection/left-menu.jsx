import {List, ListItem, makeSelectable} from 'material-ui/List';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import Divider from 'material-ui/Divider';
import Delete from 'material-ui/svg-icons/action/delete';
import React from 'react';
import Checkbox from 'material-ui/Checkbox';
import Card from 'material-ui/Card';

let SelectableList = makeSelectable(List);

function wrapState(ComposedComponent) {
    return React.createClass({
        getInitialState() {
            return {selectedIndex: 0};
        },
        handleUpdateSelectedIndex(ignore, index) {
            this.setState({
                selectedIndex: index
            });
        },
        render() {
            return (
                <ComposedComponent
                    {...this.props}
                    {...this.state}
                    value = {this.state.selectedIndex}
                    onChange= {this.handleUpdateSelectedIndex}
                />
            );
        }
    });
}

SelectableList = wrapState(SelectableList);

export class LeftList extends React.Component {
    constructor() {
        super();
        this.render = this.render.bind(this);
        this.state = {data: {}};

        this.handleClick = this.handleClick.bind(this);
        this.getFeed = this.getFeed.bind(this);
        this.showFeed = this.showFeed.bind(this);
        this.removeApp = this.removeApp.bind(this);
    }

    componentDidMount() {
        let user = app.getAuthUser();
        let firstRun = true;

        if (!user) {
            app.routeAuthUser();
            return;
        }

        this.getData(firstRun);
    }

    getData(firstRun) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'get',
                url: '/api/getAppNames',
                data: {uid: user.id, pName: localStorage.getItem('pn')}
            }).done((data) => {
                this.setState({data: data});

                if (firstRun && Object.keys(data)[0]) {
                    this.getFeed(Object.keys(data)[0]);
                }
            }).fail((jqXHR, textStatus, errorThrown) => {
                console.log(errorThrown);
            });
        }
    }

    removeApp(aid, e) {
        this.props.brandComponent.mfbCommands.setState({aid: null});

        e.preventDefault();
        e.stopPropagation();
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'get',
                url: '/api/removeCustomerApp',
                data: {aid: aid, pName: localStorage.getItem('pn')},
                dataType: 'json'
            }).done(() => {
                this.getData();
            }).fail((jqXHR, textStatus, errorThrown) => {
                console.log(errorThrown);
            });
        }
    }

    handleClick(aid) {
        this.getFeed(aid);
        this.props.brandComponent.mfbCommands.setState({aid: aid});
    }

    getFeed(aid) {
        $.ajax({
            type: 'get',
            url: '/api/feed',
            data: {aid: aid},
            dataType: 'json'
        }).done((data) => {
            this.showFeed(data);
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    showFeed(data) {
        this.props.brandComponent.feedContainer.setState({data: data});
    }

    render() {
        let listData = this.state.data;

        this.props.brandComponent.appList = this;

        return (
            <Card style={{
                margin: '40px 0px 25px 25px',
                padding: '10px 20px 20px 20px',
                float: 'left',
                width: '20%'
            }}>
                <List>
                    <ListItem
                        primaryText="Protected Apps"
                        leftIcon={<ContentInbox />}
                        leftCheckbox={<Checkbox style={{display: 'none'}} />}
                        style={{cursor: 'default'}}
                    />
                </List>
                <Divider />
                <SelectableList defaultValue={0}>
                    {Object.keys(listData).map((key, index) => {
                        return (
                            <ListItem
                                value={index}
                                key={key}
                                primaryText={listData[key]}
                                onClick={this.handleClick.bind(this, key)}
                                rightIcon={<Delete onClick={this.removeApp.bind(this, key)} />}
                            />);
                    })
                    }
                </SelectableList>
            </Card>
        );
    };
}

export default LeftList;
