import React from 'react';
import {WIFI_NETWORKS_TYPE, PROJECT_NAME, SESSION_COMPANIES} from '../../../config/consts';
import classNames from 'classnames';
import {getLoadedCompaniesObject, getDefaultCompanies} from '../common/datatableTemplate/datatable-template-helpers.js';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import {getKeyNestedChildsIfExists} from '../common/CommonHelpers.js';
import '../../../public/css/survey.css';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card} from 'material-ui/Card';
import Paper from 'material-ui/Paper';
import SettingsFilterTemplate from '../common/datatableTemplate/settings-filter-template-main.jsx';
import Loader from 'react-loader-advanced';
import DataTableViewTemplate from '../common/datatableTemplate/datatable-template.jsx';

const shortid = require('shortid');
import themeForFont from '../app/themes/themeForFont';


const RELATED_COMPANIES = 'relatedCompanies';


export default class WifiNetworks extends React.Component {
    constructor() {
        super();

        const wrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word'
        };
        const clickableWrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word',
            'cursor': 'pointer'
        };
        const smallClickableWrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word',
            'cursor': 'pointer',
            'width': '9%'
        };

        this.state = {
            showLoader: false,
            companies: [],
            companiesInputVal: [],
            tableData: [],
            tableDataToDownload: [],
            data: [],
            /* Create table column headers */
            tableColumns: [
                {
                    sortable: true,
                    label: 'SSID',
                    key: 'ssid',
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Encryption',
                    key: 'encryption',
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Type',
                    key: 'type',
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'Country',
                    key: 'country',
                    style: smallClickableWrappableStyle
                }, {
                    label: 'Related Companies',
                    key: RELATED_COMPANIES,
                    style: wrappableStyle,
                    noneSearchable: true
                }
            ],
            tableColumnsToDownload: [
                {label: 'SSID', key: 'ssid'},
                {label: 'Encryption', key: 'encryption'},
                {label: 'Type', key: 'type'},
                {label: 'Transid', key: 'transid'},
                {label: 'First Time', key: 'firsttime'},
                {label: 'Last Time', key: 'lasttime'},
                {label: 'Last Update', key: 'lastupdt'},
                {label: 'Net id', key: 'netid'},
                {label: 'WEP', key: 'wep'},
                {label: 'BCN Interval', key: 'bcninterval'},
                {label: 'Free Net', key: 'freenet'},
                {label: 'DHCP', key: 'dhcp'},
                {label: 'Paynet', key: 'paynet'},
                {label: 'User Found', key: 'userfound'},
                {label: 'Channel', key: 'channel'},
                {label: 'Country', key: 'country'},
                {label: 'Region', key: 'region'},
                {label: 'House Number', key: 'housenumber'},
                {label: 'Road', key: 'road'},
                {label: 'City', key: 'city'},
                {label: 'Postal Code', key: 'postalcode'},
                {label: 'Trilat', key: 'trilat'},
                {label: 'Trilong', key: 'trilong'},
                {label: 'Related Companies', key: RELATED_COMPANIES}
            ],
            tableExtraDataColumns: [
                {label: 'SSID', key: 'ssid'},
                {label: 'Encryption', key: 'encryption'},
                {label: 'Type', key: 'type'},
                {label: 'Transid', key: 'transid'},
                {label: 'First Time', key: 'firsttime'},
                {label: 'Last Time', key: 'lasttime'},
                {label: 'Last Update', key: 'lastupdt'},
                {label: 'Net id', key: 'netid'},
                {label: 'WEP', key: 'wep'},
                {label: 'BCN Interval', key: 'bcninterval'},
                {label: 'Free Net', key: 'freenet'},
                {label: 'DHCP', key: 'dhcp'},
                {label: 'Paynet', key: 'paynet'},
                {label: 'User Found', key: 'userfound'},
                {label: 'Channel', key: 'channel'},
                {label: 'Country', key: 'country'},
                {label: 'Region', key: 'region'},
                {label: 'House Number', key: 'housenumber'},
                {label: 'Road', key: 'road'},
                {label: 'City', key: 'city'},
                {label: 'Postal Code', key: 'postalcode'},
                {label: 'Trilat', key: 'trilat'},
                {label: 'Trilong', key: 'trilong'}
            ]
        };

        this.setResults = this.setResults.bind(this);
        this.updateState = this.updateState.bind(this);
        this.setDefaultCompanies = this.setDefaultCompanies.bind(this);
        this.saveLoadedCompanies = this.saveLoadedCompanies.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            // In case it is desired to save user preferences, use: this.getAllCompaniesByID(user);
            this.getAllowedCompanies(true);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            // Default displayOnlyValid is true, if given 'false' - save it.
            let displayOnlyValid = !(nextProps.hasOwnProperty('displayOnlyValid') && !nextProps.displayOnlyValid);
            this.setState({displayOnlyValid: displayOnlyValid});
        }
    }

    getAllowedCompanies(isDefault, selectedCompanies) {
        $.ajax({
            type: 'GET',
            url: '/api/getAllCompaniesById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((companies) => {
            if (isDefault) {
                this.saveDefaultCompanies(companies);
            } else {
                this.saveLoadedCompanies(companies, selectedCompanies);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load companies.');
        });
    }

    saveDefaultCompanies(companies) {
        if (companies) {
            let pn = localStorage.getItem(PROJECT_NAME);
            if (pn) {
                $.ajax({
                    type: 'POST',
                    url: '/api/getCompaniesByProjectName',
                    async: true,
                    data: JSON.stringify({project: pn}),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((projectCompanies) => {
                    this.setDefaultCompaniesToState(companies, projectCompanies);
                }).fail(() => {
                    app.addAlert('error', 'Error: failed to load project\'s companies.');
                });
            } else {
                this.setDefaultCompaniesToState(companies, null);
            }
        }
    }

    updateState(stateObj) {
        if (stateObj) {
            this.setState(stateObj, this.getWifiNetworksFindings);
        }
    }

    setDefaultCompaniesToState(companies, projectCompanies) {
        let result = getDefaultCompanies(companies, projectCompanies, 'selectedDomains');
        if (result && result.companies && result.companiesInputVal && result.companiesPropArray) {
            if (result.companiesInputVal.length > 1) {
                // If the Session has a selection saved - load it.
                let sessionCompanies = sessionStorage.getItem(SESSION_COMPANIES);
                if (sessionCompanies) {
                    let selectedNames = sessionCompanies.split(', ');
                    let relatedCompanies = result.companiesInputVal.filter((c) => {
                        return selectedNames.includes(c.companyName);
                    });
                    if (relatedCompanies && relatedCompanies.length > 0) {
                        result.companiesInputVal = relatedCompanies;
                    }
                } else {
                    // Select only the first company as a default selection.
                    result.companiesInputVal.splice(1, result.companiesInputVal.length);
                }
            }
            this.setDefaultCompanies(result);
        }
    }

    setDefaultCompanies(result) {
        if (result && result.companies && result.companiesInputVal) {
            this.setState({
                companies: result.companies,
                companiesInputVal: result.companiesInputVal
            }, this.getWifiNetworksFindings);
        }
    }

    saveLoadedCompanies(companies, selectedCompanies) {
        let result = getLoadedCompaniesObject(companies, selectedCompanies, 'selectedDomains');

        if (result && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: companies,
                companiesInputVal: result.companiesInputVal,
                companiesDomains: result.companiesPropArray
            }, this.getWifiNetworksFindings);
        }
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    getWifiNetworksFindings() {
        if (this.state.companiesInputVal && Array.isArray(this.state.companiesInputVal) && this.state.companiesInputVal.length > 0) {
            let companyIDs = [];
            this.state.companiesInputVal.map((currComp) => {
                if (currComp && currComp.id && !companyIDs.includes(currComp.id)) {
                    companyIDs.push(currComp.id);
                }
            });
            if (companyIDs.length > 0) {
                this.startLoader();

                const data = {
                    companyIDs: companyIDs
                };
                $.ajax({
                    type: 'POST',
                    url: '/api/getWifiNetworks',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((res) => {
                    this.stopLoader();

                    let resultData;
                    if (res && res.ok && res.data && Array.isArray(res.data)) {
                        resultData = res.data;
                    } else {
                        resultData = [];
                    }
                    this.setResults(resultData);
                }).fail(() => {
                    this.stopLoader();
                    app.addAlert('error', 'Error: failed to fetch data');
                });
            }
        }
    }

    add3Dots(str, limit) {
        const dots = '...';
        if (str.length > limit) {
            str = str.substring(0, limit) + dots;
        }

        return str;
    }

    setResults(data) {
        const CHARS_LIMIT = 20;
        let tableData = [];
        let tableDataToDownload = [];

        if (data && Array.isArray(data)) {
            data.map((currData) => {
                if (currData && currData.findings
                    && Array.isArray(currData.findings)) {

                    currData.findings.map((currFinding) => {
                        if(currFinding) {
                            const tableEntityObj = {
                                surveyIdentifier: shortid.generate()
                            };

                            this.state.tableColumns.map((currColumn) => {
                                let currKey = currColumn.key;
                                if (currKey === RELATED_COMPANIES && currData[RELATED_COMPANIES] && Array.isArray(currData[RELATED_COMPANIES])) {
                                    tableEntityObj[currKey] = showExpandedDataChip(currData[RELATED_COMPANIES], 3, 5);
                                }
                                else {
                                    const currDataValue = getKeyNestedChildsIfExists(currFinding, currKey);
                                    if (currDataValue != null) {
                                        // Apply data as string so FuzzySearch will be able to use this registry properly.
                                        tableEntityObj[currKey] = this.add3Dots(currDataValue, CHARS_LIMIT);
                                    } else {
                                        tableEntityObj[currKey] = '';
                                    }
                                }
                            });

                            const tableEntityObjToDownload = {};
                            this.state.tableColumnsToDownload.map((currColumn) => {
                                let currKey = currColumn.key;
                                if (currKey === RELATED_COMPANIES && currData[RELATED_COMPANIES] && Array.isArray(currData[RELATED_COMPANIES])) {
                                    tableEntityObjToDownload[currKey] = currData[RELATED_COMPANIES].join(', ');
                                }
                                else {
                                    const currDataValue = getKeyNestedChildsIfExists(currFinding, currKey);
                                    if (currDataValue != null) {
                                        tableEntityObjToDownload[currKey] = currDataValue;
                                    } else {
                                        tableEntityObjToDownload[currKey] = '';
                                    }
                                }
                            });

                            // Add extra data to tableEntityObj for future click event.
                            tableEntityObj.extraData = [];
                            this.state.tableExtraDataColumns.map((currColumn) => {
                                if (currColumn && currColumn.key && currColumn.label) {
                                    let finding = getKeyNestedChildsIfExists(currFinding, currColumn.key);
                                    if (finding != null && finding !== '') {
                                        tableEntityObj.extraData.push({
                                            name: currColumn.label,
                                            finding: finding
                                        });
                                    }
                                }
                            });

                            tableData.push(tableEntityObj);
                            tableDataToDownload.push(tableEntityObjToDownload);
                        }
                    });
                }
            });
        }
        this.setState({
            tableData: tableData,
            tableDataToDownload: tableDataToDownload
        });
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        let resultClassName = classNames('search-results-container');

        const styles = {
            cardStyle: {
                margin: '20px 10px 20px 10px'
            },
            headline: {
                fontSize: 24,
                paddingTop: 16,
                marginBottom: 12,
                fontWeight: 400
            },
            slide: {
                padding: 10
            },
            inkBar: {
                backgroundColor: '#ffffff'
            },
            tabItemContainer: {
                backgroundColor: 'rgb(48, 48, 48)'
            },
            tabItemLabel: {
                color: 'white'
            },
            emptyResultStyle: {
                backgroundColor: '#FFDD00',
                display: 'inline-block',
                padding: '1% 5%',
                marginTop: '240px',
                fontSize: 'large',
                textAlign: 'center'
            },
            wrappableStyle: {
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-word'
            },
            clickableWrappableStyle: {
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-word',
                'cursor': 'pointer'
            }
        };

        let dataResultIndicator = (this.state.tableData && this.state.tableData.length > 0 && this.state.tableDataToDownload) ? (
            <DataTableViewTemplate
                myDataType={WIFI_NETWORKS_TYPE}
                tableColumns={this.state.tableColumns}
                tableData={this.state.tableData}
                tableColumnsToDownload={this.state.tableColumnsToDownload}
                tableDataToDownload={this.state.tableDataToDownload}
                displayOnlyValid={this.state.displayOnlyValid}
            />
        ) : (
            <Card className={resultClassName} style={styles.cardStyle}>
                <Paper style={styles.emptyResultStyle} zDepth={4}><span style={{verticalAlign: 'middle'}}>No data to display.</span></Paper>
            </Card>
        );

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div className={pageStyleClass}>
                    <Loader show={this.state.showLoader} message={spinner}>
                        <div>
                            <div style={{margin: '40px 0px 40px 10px'}}>
                                <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                                <span style={{position: 'relative', top: '-6px', fontSize: 22}}>Wifi Networks</span>
                            </div>
                            <SettingsFilterTemplate
                                myDataType={WIFI_NETWORKS_TYPE}
                                companies={this.state.companies}
                                companiesInputVal={this.state.companiesInputVal}
                                updateParentState={this.updateState}
                                setDefaultCompanies={this.setDefaultCompanies}
                                saveLoadedCompanies={this.saveLoadedCompanies}
                            />
                            {dataResultIndicator}
                        </div>
                    </Loader>
                </div>
            </MuiThemeProvider>
        );
    }
}
