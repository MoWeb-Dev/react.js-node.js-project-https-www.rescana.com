import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardTitle} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import Loader from 'react-loader-advanced';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import ActionAdd from 'material-ui/svg-icons/content/add-circle';
import ReportSettingsModalManualData from './report-add-manual.jsx';
import {PROJECT_NAME, ORGANIZATION_ID} from '../../../../config/consts';
import DataTables from 'material-ui-datatables';
import EditButton from 'material-ui/svg-icons/editor/mode-edit';
import DeleteButton from 'material-ui/svg-icons/action/delete-forever';
import '../../../../public/css/main.css';
import '../../../../public/css/survey.css';
import themeForFont from "../../app/themes/themeForFont";

const shortid = require('shortid');

const TYPE_EXCEL = 'EXCEL';
const TYPE_PDF = 'PDF';
const TYPE_WORD = 'WORD';

/**
 * A windows for selecting settings before exporting a report.
 */
export default class ExportIntelReportSettings extends React.Component {
    constructor(props) {
        super(props);

        const wrappableStyle = {
            whiteSpace: 'pre-wrap',
            wordBreak: 'break-word'
        };

        this.state = {
            isProjectsSelected: false,
            companies: [],
            projects: [],
            companiesInputVal: [],
            projectsInputVal: [],
            showLoader: false,
            showManualDataPage: false,
            manualData: [],
            editFindingObject: {},
            editFindingIndex: -1,
            filesID: shortid.generate(),
            tableColumns: [{
                label: 'Title',
                key: 'title',
                style: wrappableStyle
            }, {
                label: 'Content',
                key: 'text',
                style: wrappableStyle
            }, {
                label: 'Attachments',
                key: 'files',
                style: wrappableStyle
            }, {
                label: 'Edit',
                key: 'edit'
            }, {
                label: 'Delete',
                key: 'delete'
            }]
        };

        this.findInputInDataByName = this.findInputInDataByName.bind(this);
        this.findInputInDataByID = this.findInputInDataByID.bind(this);
        this.handleAddRequestedField = this.handleAddRequestedField.bind(this);
        this.handleDeleteRequestedField = this.handleDeleteRequestedField.bind(this);
        this.NotifySuccess = this.NotifySuccess.bind(this);
        this.getProjectsByID = this.getProjectsByID.bind(this);
        this.setProjectsOnState = this.setProjectsOnState.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (user) {
            this.setState({admin: user.admin}, () => {
                this.getProjectsByID(user);
            });
        }
    }

    getProjectsByID(user) {
        $.ajax({
            type: 'GET',
            url: '/api/getAllProjectsById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projects) => {
            if (projects && Array.isArray(projects) && projects.length !== 0) {
                this.setProjectsOnState(projects, user);
            } else if (user) {
                this.setProjectsOnState(null, user);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
        });
    }

    setProjectsOnState(projects, user) {
        // Set loaded project as default if exists.
        let loadedProject = localStorage.getItem(PROJECT_NAME);
        let isLoadedProjectFound = false;

        let stateObj = {
            isProjectsSelected: false,
            showLoader: false,
            showManualDataPage: this.state.showManualDataPage,
            manualData: [],
            editFindingObject: {},
            editFindingIndex: -1
        };

        if (projects) {
            stateObj.companies = [];
            stateObj.projects = projects.map((currPrj) => {
                if (currPrj.id && currPrj.projectName) {
                    if (loadedProject && !isLoadedProjectFound && currPrj.projectName === loadedProject) {
                        isLoadedProjectFound = true;
                        stateObj.companiesInputVal = [];
                    }
                    if (currPrj.selectedCompanies) {
                        currPrj.selectedCompanies.map((currCmp) => {
                            if (currCmp.id && currCmp.name) {
                                let isCmpExist = false;
                                for (let i = 0; i < stateObj.companies.length; i++) {
                                    if (stateObj.companies[i].id === currCmp.id) {
                                        isCmpExist = true;
                                        break;
                                    }
                                }
                                if (!isCmpExist) {
                                    stateObj.companies.push({
                                        id: currCmp.id,
                                        name: currCmp.name,
                                        selectedDomains: currCmp.selectedDomains
                                    });
                                }
                            }
                        });
                    }
                    return {
                        id: currPrj.id,
                        projectName: currPrj.projectName,
                        selectedCompanies: currPrj.selectedCompanies
                    };
                }
            });
            // Set all projects and companies as selected if no loaded project is found, so user can only delete or re-add them.
            // This assign copies the array not by reference.
            if (!isLoadedProjectFound) {
                stateObj.projectsInputVal = [].concat(stateObj.projects);
                stateObj.companiesInputVal = [].concat(stateObj.companies);
            }
        }

        if (user) {
            stateObj.user = user;
        }
        this.setState(stateObj);
        this.setState({isProjectsSelected: false});
    }


    findInputInDataByName(input, data) {
        let index = -1;
        if (data) {
            for (let i = 0; i < data.length; i++) {
                if ((data[i].projectName && data[i].projectName === input) ||
                    (data[i].name && data[i].name === input)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    findInputInDataByID(input, data) {
        let index = -1;
        if (data) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].id && data[i].id === input) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    handleAddRequestedField(chip, fieldType) {
        let allData;
        let inputData;
        let input = '';
        let type;
        if (!Array.isArray(chip)) {
            chip = [chip];
        }

        if (fieldType === 'project') {

            allData = this.state.projects;
            inputData = this.state.projectsInputVal;
            type = 'projects';
            if (chip && chip[0] && chip[0].projectName) {
                input = chip[0].projectName;
            }
        } else {
            allData = this.state.companies;
            inputData = this.state.companiesInputVal;
            type = 'companies';

            if (chip && chip[0] && chip[0].name) {
                input = chip[0].name;
            }
        }
        if(inputData && inputData.length >= 3){
            app.addAlert('error', 'Please Choose no more then 3 ' + type + '.');
        } else {
            // Check if allData contains the input.
            let inputIndex = this.findInputInDataByName(input, allData);

            if (inputIndex > -1) {
                let inputObjToAdd = allData[inputIndex];

                // Check if inputData not already contains the input.
                if (this.findInputInDataByName(input, inputData) === -1) {
                // Add to the ChipSelect.
                inputData = inputData.concat(inputObjToAdd);

                let stateInputObjToUpdate = (fieldType === 'project') ?
                    {'projectsInputVal': inputData} : {'companiesInputVal': inputData};

                this.setState(stateInputObjToUpdate);
                } else {
                    let capitalFieldType = fieldType.substring(0, 1).toUpperCase() + fieldType.substring(1);

                    app.addAlert('warning', capitalFieldType + ' already selected.');
                }
            } else {
                app.addAlert('error', 'No such ' + fieldType + '.');
            }
        }
    }

    handleDeleteRequestedField(value, index, fieldType) {
        let inputData;

        if (fieldType === 'project') {
            inputData = this.state.projectsInputVal;
        } else {
            inputData = this.state.companiesInputVal;
        }

        // Check the index of the value(id) of selectedChip in inputData.
        if (this.findInputInDataByID(value, inputData) > -1) {
            inputData.splice(index, 1);

            let stateInputObjToUpdate = (fieldType === 'project') ?
                {'projectsInputVal': inputData} : {'companiesInputVal': inputData};

            this.setState(stateInputObjToUpdate);
        }
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    requestReport(dataObj) {
        this.startLoader();

        $.ajax({
            type: 'POST',
            url: '/api/exportReport',
            data: JSON.stringify(dataObj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            timeout: 60000,
            async: true
        }).done((res) => {
            this.stopLoader();

            if (!res.ok) {
                app.addAlert('error', 'Error: Failed to export');
            } else {
                res.dataArr.map((curData)=>{
                    // Meaning that the PDF was created successfully. (res.data is the fileName)
                    window.location = '/intelReport/' + curData;
                });
                this.NotifySuccess();
            }
        }).fail(() => {
            this.stopLoader();
            app.addAlert('error', 'Error: Failed to export');
        });
    }

    handleExportReport(reportType, e) {
        e.preventDefault();
        let orgId = localStorage.getItem(ORGANIZATION_ID);
        if (this.state.user && this.state.user.id) {
            if (this.state.isProjectsSelected && this.state.projectsInputVal) {
                if (this.state.projectsInputVal.length > 0) {
                    let dataObj = {
                        uid: this.state.user.id,
                        orgId: orgId,
                        projects: this.state.projectsInputVal,
                        manualData: this.state.manualData,
                        manualDataID: this.state.filesID,
                        windowLocation: window && window.location && window.location.origin ? window.location.origin + '/intelReport/' : null,
                        displayScoreBackwards: !!(this.state.user.userPreferencesConfig &&
                            this.state.user.userPreferencesConfig.displayScoreBackwards)
                    };

                    dataObj.type = (reportType === TYPE_PDF) ? 'pdf' : (reportType === TYPE_WORD) ? 'word' : 'excel';
                    dataObj.reportKind = 'intelReport';

                    this.requestReport(dataObj);
                } else {
                    app.addAlert('error', 'Please select at least one project.');
                }
            } else if (!this.state.isProjectsSelected && this.state.companiesInputVal) {
                if (this.state.companiesInputVal.length > 0) {
                    let dataObj = {
                        uid: this.state.user.id,
                        orgId: orgId,
                        companies: this.state.companiesInputVal,
                        manualData: this.state.manualData,
                        manualDataID: this.state.filesID,
                        windowLocation: window && window.location && window.location.origin ? window.location.origin + '/intelReport/' : null,
                        displayScoreBackwards: !!(this.state.user.userPreferencesConfig &&
                            this.state.user.userPreferencesConfig.displayScoreBackwards)
                    };

                    dataObj.type = (reportType === TYPE_PDF) ? 'pdf' : (reportType === TYPE_WORD) ? 'word' : 'excel';
                    dataObj.reportKind = 'intelReport';

                    this.requestReport(dataObj);
                } else {
                    app.addAlert('error', 'Please select at least one company.');
                }
            }
        } else {
            app.addAlert('error', 'Error: failed to load user\'s data');
        }
    }

    NotifySuccess() {
        console.log('Report created successfully.');
        app.addAlert('success', 'Report created successfully.');
    }

    changeSelection(ignore, value) {
        if (value && value === 'project') {
            this.setState({isProjectsSelected: true});
        } else {
            this.setState({isProjectsSelected: false});
        }
    }

    navigateToManualDataPage() {
        const maxData = 10;
        if (this.state.manualData.length < maxData) {
            this.setState({
                showManualDataPage: true
            });
        } else {
            app.addAlert('error', 'Limit of ' + maxData + ' findings exceeded');
        }
    }

    navigateFromManualDataPage() {
        // State closes the modal and resets editMode to false.
        this.setState({showManualDataPage: false, editFindingObject: {}, editFindingIndex: -1});
    }

    saveDataFromManualPage(newData, editIndex = -1) {
        let manualData = this.state.manualData;

        // Check if any files exist.
        if (newData.files && newData.files.length > 0) {
            let files = newData.files;
            let filesUploadedAlready = [];

            let data = new FormData();

            // A boolean indicates whether a new image inserted (and need to be saved on server) or not.
            let isNewImagesAdded = false;

            for (let i = 0; i < files.length; i++) {
                // Separate between files already saved on server and those that are not.
                if (files[i].name && !files[i].path) {
                    data.append('intelManualFinding', files[i], this.state.filesID + files[i].name);
                    isNewImagesAdded = true;
                } else {
                    filesUploadedAlready = filesUploadedAlready.concat(files[i]);
                }
            }

            if (isNewImagesAdded) {
                $.ajax({
                    url: '/api/intelImageUpload',
                    dataType: 'json',
                    data: data,
                    enctype: 'multipart/form-data',
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: (res) => {
                        if (res && res.result) {
                            let filesPaths = res.result;
                            newData.files = filesUploadedAlready.concat(filesPaths);

                            // This section handles that new files inserted in EditMode.
                            if (editIndex !== -1) {
                                manualData[editIndex] = newData;

                                // State saves the new data from modal, closes the modal and resets editMode to false.
                                this.setState({
                                    manualData: manualData,
                                    showManualDataPage: false,
                                    editFindingObject: {},
                                    editFindingIndex: -1
                                },
                                app.addAlert('success', 'Finding updated.'));

                            // This section handles that new files are inserted in AddMode.
                            } else {
                                manualData.push(newData);

                                // State saves the new data from modal and closes the modal.
                                this.setState({manualData: manualData, showManualDataPage: false},
                                    app.addAlert('success', 'New finding inserted.'));
                            }
                        } else {
                            app.addAlert('error', 'Failed to add new finding.');
                        }
                    },
                    error: () => {
                        app.addAlert('error', 'Failed to add new finding.');
                    }
                });
            } else {
                // This section handles that are files but all of them are already saved and it is in EditMode.
                if (editIndex !== -1) {
                    manualData[editIndex] = newData;

                    // State saves the new data from modal, closes the modal and resets editMode to false.
                    this.setState({
                        manualData: manualData,
                        showManualDataPage: false,
                        editFindingObject: {},
                        editFindingIndex: -1
                    },
                    app.addAlert('success', 'Finding updated.'));

                // This section handles that are files but all of them are already saved and it is AddMode.
                } else {
                    manualData.push(newData);

                    // State saves the new data from modal and closes the modal.
                    this.setState({manualData: manualData, showManualDataPage: false},
                        app.addAlert('success', 'New finding inserted.'));
                }
            }
        } else {
            // This section handles that there are no files inserted but other data is changed in EditMode.
            if (editIndex !== -1) {
                manualData[editIndex] = newData;

                // State saves the new data from modal, closes the modal and resets editMode to false.
                this.setState({
                    manualData: manualData,
                    showManualDataPage: false,
                    editFindingObject: {},
                    editFindingIndex: -1
                },
                app.addAlert('success', 'Finding updated.'));

            // This section handles that there are no files inserted but other data is changed in AddMode.
            } else {
                manualData.push(newData);

                // State saves the new data from modal and closes the modal.
                this.setState({manualData: manualData, showManualDataPage: false},
                    app.addAlert('success', 'New finding inserted.'));
            }
        }
    }

    editTableFinding(editData, editIdx) {
        // Open the modal in edit mode.
        this.setState({showManualDataPage: true, editFindingObject: editData, editFindingIndex: editIdx});
    }

    deleteTableFinding(index) {
        let manualData = this.state.manualData;
        manualData.splice(index, 1);
        this.setState({manualData: manualData});
    }

    handleClear() {
        let companiesInputVal = [];
        let projectsInputVal = [];
        if (this.state.companiesInputVal && this.state.companiesInputVal.length > 0 || this.state.projectsInputVal && this.state.projectsInputVal.length > 0) {
            this.setState({companiesInputVal: companiesInputVal, projectsInputVal: projectsInputVal});
        } else {
            app.addAlert('warning', 'Already Clear.');
        }
    }

    render() {
        let user = app.getAuthUser();

        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        const cardStyle = {margin: 10};

        const rdbStyle = {marginTop: '12%', marginLeft: '30px'};

        const rowStyle = {display: 'flex', };

        const firstColStyle = {width: '20%', marginTop: '43px', marginLeft: -15};
        const labelStyle = {marginTop: '30px', marginLeft: '50px'};

        const chipsStyle = {
            width: '80%',
            overflowY: 'auto',
            marginRight: '30px',
            paddingBottom: '8px',
            marginBottom: '8px',
            height: 'auto',
            maxHeight: '280px'
        };

        const myFindingsStyle = {paddingTop: '25px', fontSize: '16px'};

        let projectsChip = (this.state.isProjectsSelected) ? {display: 'inline-block'} : {display: 'none'};

        let companiesChip = (this.state.isProjectsSelected) ? {display: 'none'} : {display: 'inline-block'};

        let tableBodyStyle = {
            'overflowX': 'auto'
        };

        let tableStyle = {
            'width': '99%',
            'margin': 'auto'
        };

        let buttonStyle = {
            cursor: 'pointer'
        };

        let manualData = this.state.manualData;

        let tableData = [];

        for (let i = 0; i < manualData.length; i++) {
            const maxChars = 15;
            let title;
            let text;

            if (manualData[i].title.length > maxChars) {
                title = manualData[i].title.slice(0, maxChars) + '...';
            } else {
                title = manualData[i].title;
            }

            if (manualData[i].text.length > maxChars) {
                text = manualData[i].text.slice(0, maxChars) + '...';
            } else {
                text = manualData[i].text;
            }

            tableData[i] = {
                title: title,
                text: text,
                files: manualData[i].files.length,
                edit: <EditButton onTouchTap={this.editTableFinding.bind(this, manualData[i], i)}
                    style={buttonStyle}/>,
                delete: <DeleteButton onTouchTap={this.deleteTableFinding.bind(this, i)}
                    style={buttonStyle}/>
            };
        }

        const actions = [
            <FlatButton
                key={65}
                label="Export PDF"
                secondary={true}
                onTouchTap={this.handleExportReport.bind(this, TYPE_PDF)}
                disabled={this.state.showLoader}
            />,
            <FlatButton
                key={66}
                label="Export Excel"
                secondary={true}
                onTouchTap={this.handleExportReport.bind(this, TYPE_EXCEL)}
                disabled={this.state.showLoader}
            />

        ];

        if (this.state.admin) {
            actions.push(<FlatButton
                key={67}
                label="Export Word"
                secondary={true}
                onTouchTap={this.handleExportReport.bind(this, TYPE_WORD)}
                disabled={this.state.showLoader}
            />);
        }

        return (
            <div className={pageStyleClass}>
                <Loader show={this.state.showLoader} message={spinner}>
                    <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                        <div>
                            <div style={{margin: '40px 0px 40px 10px'}}>
                                <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                                <span style={{ position: 'relative', top: '-6px', fontSize: 22}}>EXPORT INTELLIGENCE REPORT</span>
                            </div>
                            <Card style={{margin: 10,marginBottom: 20, paddingLeft: 30}}>
                                <p style={myFindingsStyle}>Please select the companies you would like to review:</p>
                                <div style={rowStyle}>
                                    <div style={firstColStyle}>
                                        <span style={labelStyle}>Export by companies:</span>
                                        {/*<RadioButtonGroup name="exportOptions" defaultSelected="company"*/}
                                            {/*style={rdbStyle}*/}
                                            {/*onChange={this.changeSelection.bind(this)}>*/}
                                            {/*<RadioButton*/}
                                                {/*value="company"*/}
                                                {/*label="Export by companies"*/}
                                                {/*style={{marginBottom: 16}}*/}
                                            {/*/>*/}
                                            {/*<RadioButton*/}
                                                {/*value="project"*/}
                                                {/*label="Export by projects"*/}
                                                {/*style={{marginBottom: 16}}*/}
                                            {/*/>*/}
                                        {/*</RadioButtonGroup>*/}
                                    </div>
                                    <div style={chipsStyle}>
                                        {/*<ChipInput*/}
                                            {/*hintText="Requested Projects"*/}
                                            {/*floatingLabelText="Requested Projects"*/}
                                            {/*value={this.state.projectsInputVal}*/}
                                            {/*filter={AutoComplete.fuzzyFilter}*/}
                                            {/*dataSource={this.state.projects}*/}
                                            {/*dataSourceConfig={{'text': 'projectName', 'value': 'id'}}*/}
                                            {/*onRequestAdd={(chip) => this.handleAddRequestedField(chip, 'project')}*/}
                                            {/*onRequestDelete={(chip, index) => this.handleDeleteRequestedField(chip, index, 'project')}*/}
                                            {/*maxSearchResults={5}*/}
                                            {/*style={projectsChip}*/}
                                            {/*fullWidth={true}*/}
                                            {/*fullWidthInput={true}*/}
                                            {/*openOnFocus={true}*/}
                                        {/*/>*/}
                                        <ChipInput
                                            hintText="Requested Companies"
                                            floatingLabelText="Requested Companies"
                                            value={this.state.companiesInputVal}
                                            filter={AutoComplete.fuzzyFilter}
                                            dataSource={this.state.companies}
                                            dataSourceConfig={{'text': 'name', 'value': 'id'}}
                                            onRequestAdd={(chip) => this.handleAddRequestedField(chip, 'company')}
                                            onRequestDelete={(chip, index) => this.handleDeleteRequestedField(chip, index, 'company')}
                                            maxSearchResults={5}
                                            style={companiesChip}
                                            fullWidth={true}
                                            fullWidthInput={true}
                                            openOnFocus={true}
                                        />
                                        <FlatButton
                                            label="Clear"
                                            onTouchTap={this.handleClear.bind(this)}
                                            primary={true}
                                            backgroundColor={'#f0f0f0'}
                                            style={{float: 'right', margin: '10px 0px 5px 0px', color: '#0091ea', height: 40}}
                                            hoverColor={'#e6e6e6'}
                                            rippleColor={'white'}
                                            labelStyle={{fontSize: 10}}
                                            keyboardFocused={false}
                                        />
                                    </div>
                                </div>
                            </Card>
                            {(user && user.userRole && user.userRole === 'basic') ? null :
                                <Card style={{margin: 10,marginBottom: 20, paddingLeft: 30}}>
                                    <p style={myFindingsStyle}>Optional : Click 'Add' if you wish to add findings of
                                        your own at the end of the report</p>
                                    <div style={rowStyle}>
                                        <div style={firstColStyle}>
                                            <FlatButton
                                                label="Add"
                                                icon={<ActionAdd/>}
                                                onClick={this.navigateToManualDataPage.bind(this)}
                                                style={{marginBottom: '20px', marginLeft: 18}}
                                            />
                                            <ReportSettingsModalManualData
                                                open={this.state.showManualDataPage}
                                                navigateDone={this.saveDataFromManualPage.bind(this)}
                                                navigateCancel={this.navigateFromManualDataPage.bind(this)}
                                                editFindingObject={this.state.editFindingObject}
                                                editFindingIndex={this.state.editFindingIndex}
                                                navigateDoneEdit={this.saveDataFromManualPage.bind(this)}
                                            />
                                        </div>
                                        {(!this.state.manualData || this.state.manualData.length === 0) ? null :
                                            <div style={chipsStyle}>
                                                <DataTables
                                                    selectable={false}
                                                    showRowHover={true}
                                                    showFooterToolbar={false}
                                                    showCheckboxes={false}
                                                    tableBodyStyle={tableBodyStyle}
                                                    tableStyle={tableStyle}
                                                    columns={this.state.tableColumns}
                                                    data={tableData}
                                                />
                                            </div>
                                        }
                                    </div>
                                </Card>
                            }
                            <Card style={{margin: 10, padding: 15}}>
                                {actions}
                            </Card>
                        </div>
                    </MuiThemeProvider>
                </Loader>
            </div>
        );
    }
};
