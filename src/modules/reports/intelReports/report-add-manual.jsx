import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import TextField from 'material-ui/TextField';
import Dropzone from 'react-dropzone';
import FontIcon from 'material-ui/FontIcon';
import {Row} from 'react-grid-system';
import IconButton from 'material-ui/IconButton';
import ActionDelete from 'material-ui/svg-icons/content/clear';
import {isImage} from '../../survey/common/survey-helpers.js';
import themeForFont from "../../app/themes/themeForFont";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";

/**
 * A modal dialog can be closed by selecting one of the actions.
 */
export default class ReportSettingsModalManualData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            manualData: {
                title: '',
                text: '',
                files: []
            },
            open: this.props.open,
            isEditMode: false,
            editFindingIndex: -1,
            acceptedFiles: ['image/jpeg', 'image/png', 'image/bmp']
        };
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            let stateObj = {open: nextProps.open || false};

            if (nextProps.editFindingIndex !== -1) {
                stateObj.manualData = {};
                stateObj.manualData.title = nextProps.editFindingObject.title || '';
                stateObj.manualData.text = nextProps.editFindingObject.text || '';
                stateObj.manualData.files = [].concat(nextProps.editFindingObject.files) || [];
                stateObj.isEditMode = true;
                stateObj.editFindingIndex = nextProps.editFindingIndex;
            } else {
                stateObj.manualData = {
                    title: '',
                    text: '',
                    files: []
                };
                stateObj.isEditMode = false;
                stateObj.editFindingIndex = -1;
            }

            this.setState(stateObj);
        }
    }

    updateAdditionalDataTitle(event) {
        let manualData = this.state.manualData;
        manualData.title = event.target.value;
        this.setState({manualData: manualData});
    }

    updateAdditionalDataText(event) {
        let manualData = this.state.manualData;
        manualData.text = event.target.value;
        this.setState({manualData: manualData});
    }

    handleDone() {
        let manualData = this.state.manualData;

        if (!manualData.title.toString().trim() && !manualData.text.toString().trim() && manualData.files.length === 0) {
            app.addAlert('warning', 'Please insert any data or click cancel');
        } else if (!manualData.title.toString().trim()) {
            app.addAlert('warning', 'Title is required');
        } else if (!manualData.text.toString().trim() && manualData.files.length === 0) {
            app.addAlert('warning', 'Please enter description or add an image');
        } else if (this.state.isEditMode) {
            this.props.navigateDoneEdit(manualData, this.state.editFindingIndex);
        } else {
            this.props.navigateDone(manualData);
        }
    }

    onDrop(files) {
        const maxFiles = 2;
        if ((this.state.manualData.files && this.state.manualData.files.length + files.length > maxFiles) ||
            (!this.state.manualData.files && files.length > maxFiles)) {
            app.addAlert('error', 'Limit of ' + maxFiles + ' images exceeded');
        } else {
            let manualData = this.state.manualData;
            manualData.files = manualData.files.concat(files);
            this.setState({manualData: manualData});
        }
    }

    onDropRejected() {
        return app.addAlert('error', 'File too big, max size is 3MB');
    }

    handleRemove(file, fileIndex) {
        // TODO: check what to do with this row below in comment: This is to prevent memory leaks.
        // window.URL.revokeObjectURL(file.preview);
        let manualData = this.state.manualData;
        manualData.files.splice(fileIndex, 1);
        this.setState({manualData: manualData});
    }

    render() {
        let dropZoneClass = classNames({
            'dropZoneDark': true
        });
        let activeDropZoneClass = classNames({
            'stripes': true
        });
        let rejectDropZoneClass = classNames({
            'rejectStripes': true
        });
        let uploadCloudIcon = classNames({
            'material-icons': true,
            'uploadIconSize': true
        });

        let imageContainer = classNames({
            'imageContainer': true,
            'col-md-3': true,
            'fileIconImg': true
        });

        let middle = classNames({
            'middleLTR': true
        });

        let modalTitle = (this.state.isEditMode) ? 'Edit Finding' : 'Add Finding';

        const divStyle = {padding: '0px 20px'};

        let previews = this.state.manualData.files.map((file, i) => {
            let img;
            let path = file.preview || '/intelImage' + file.path;
            if (isImage(file)) {
                img = <img className="smallPreviewImg" src={path}/>;
            } else {
                img = <FileIcon className="smallPreviewImg"/>;
            }

            return <div className={imageContainer} key={i}>
                {img}
                <div className={middle}>
                    <IconButton touch={true} tooltip="Delete" tooltipPosition="bottom-right">
                        <ActionDelete
                            className="removeBtn"
                            onTouchTap={this.handleRemove.bind(this, file, i)}
                        />
                    </IconButton>
                </div>
            </div>;
        });

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.props.navigateCancel}
            />,
            <FlatButton
                label="Done"
                secondary={true}
                onTouchTap={this.handleDone.bind(this)}
            />

        ];

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title={modalTitle}
                        actions={actions}
                        modal={false}
                        open={this.state.open}
                        onRequestClose={this.props.navigateCancel}
                        autoScrollBodyContent={true}
                    >
                        <Row style={divStyle}>
                            <TextField
                                hintText="Enter title here"
                                floatingLabelText="Title"
                                onChange={this.updateAdditionalDataTitle.bind(this)}
                                value={this.state.manualData.title}
                            /><br/>
                            <TextField
                                hintText="Enter description here"
                                floatingLabelText="Description for the finding"
                                multiLine={true}
                                rows={1}
                                rowsMax={4}
                                fullWidth={true}
                                onChange={this.updateAdditionalDataText.bind(this)}
                                value={this.state.manualData.text}
                            /><br/>
                            <Dropzone
                                accept={this.state.acceptedFiles.join(',')}
                                onDrop={this.onDrop.bind(this)}
                                className={dropZoneClass}
                                activeClassName={activeDropZoneClass}
                                rejectClassName={rejectDropZoneClass}
                                onDropRejected={this.onDropRejected.bind(this)}
                                maxSize={3000000}
                            >
                                <div className="dropzoneTextStyle">
                                    <p className="dropzoneParagraph">Drag and drop image files here or click</p>
                                    <br/>
                                    <FontIcon
                                        className={uploadCloudIcon}
                                    >
                                        cloud_upload
                                    </FontIcon>
                                </div>
                            </Dropzone>
                            <br/>
                            {this.state.manualData.files.length ? <span>Preview:</span> : ''}
                            <div className="row">
                                {previews}
                            </div>

                        </Row>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
