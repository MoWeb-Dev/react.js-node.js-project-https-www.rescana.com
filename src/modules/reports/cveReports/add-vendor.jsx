import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import Loader from 'react-loader-advanced';
import {Row} from 'react-grid-system';
import {
    Step,
    Stepper,
    StepButton,
    StepContent
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete/index';
import Checkbox from 'material-ui/Checkbox';
import ReactTooltip from 'react-tooltip';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";


/**
 * A modal dialog can be closed by selecting one of the actions.
 */
export default class CVEReportSettingsModalAddData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: this.props.open,
            data: {
                vendor: '',
                products: []
            },
            allVendors: [],
            allProducts: [],
            stepIndex: 0,
            isAllProductsChecked: true,
            isEditMode: false,
            editObjectIndex: -1,
            shouldLoadProducts: true,
            showLoader: false
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            let stateObj = {open: nextProps.open || false};

            if (nextProps.allVendors) {
                stateObj.allVendors = nextProps.allVendors;
            }

            if (nextProps.editObject && nextProps.editObjectIndex !== -1) {
                stateObj.data = {
                    vendor: nextProps.editObject.vendor || '',
                    products: [].concat(nextProps.editObject.products) || []
                };
                stateObj.stepIndex = 1;
                stateObj.isEditMode = true;
                stateObj.editObjectIndex = nextProps.editObjectIndex;
                stateObj.showLoader = false;

                stateObj.isAllProductsChecked = !(nextProps.editObject.products.length);

                if (nextProps.editObject.allProducts) {
                    stateObj.allProducts = nextProps.editObject.allProducts;
                    stateObj.shouldLoadProducts = false;
                } else {
                    stateObj.allProducts = [];
                    stateObj.shouldLoadProducts = true;
                }
            } else {
                stateObj.data = {
                    vendor: '',
                    products: []
                };
                stateObj.allProducts = [];
                stateObj.stepIndex = 0;
                stateObj.isAllProductsChecked = true;
                stateObj.isEditMode = false;
                stateObj.editObjectIndex = -1;
                stateObj.shouldLoadProducts = true;
                stateObj.showLoader = false;
            }

            this.setState(stateObj);
        }
    }

    handleNext() {
        if (this.state.data.vendor) {
            let stepIndex = this.state.stepIndex;
            if (stepIndex < 1) {
                this.setState({stepIndex: stepIndex + 1});
            }
        } else {
            app.addAlert('warning', 'Please select a vendor first');
        }
    };

    handlePrev() {
        let stepIndex = this.state.stepIndex;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };

    handleStepActionsClick(step) {
        if (step === 0) {
            this.setState({stepIndex: step});
        } else if (step === 1) {
            if (this.state.data.vendor) {
                this.setState({stepIndex: step});
            } else {
                app.addAlert('warning', 'Please select a vendor first');
            }
        }
    }

    renderStepActions(step) {
        return (
            <div style={{margin: '12px 0'}}>
                {step < 1 && <RaisedButton
                    label="Next"
                    disableTouchRipple={true}
                    disableFocusRipple={true}
                    primary={true}
                    onClick={this.handleNext.bind(this)}
                    style={{marginRight: 12}}
                />}
                {step > 0 && (
                    <FlatButton
                        label="Back"
                        disableTouchRipple={true}
                        disableFocusRipple={true}
                        onClick={this.handlePrev.bind(this)}
                    />
                )}
            </div>
        );
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    updateProductsCheckBox() {
        if (this.state.shouldLoadProducts) {
            // Meaning that user unchecked the checkbox for the first time.
            if (this.state.data.vendor) {
                // Get this vendor products.
                this.changeCheckboxState(this.loadProductsByVendor(this.state.data.vendor));
            } else {
                app.addAlert('warning', 'Please select a vendor first');
            }
        } else {
            this.changeCheckboxState();
        }
    }

    // cb is default to an empty function so it would do nothing after setting state unless a callback function is inserted.
    changeCheckboxState(cb = () => {
    }) {
        this.setState({isAllProductsChecked: !this.state.isAllProductsChecked}, cb);
    }

    loadProductsByVendor(vendor) {
        this.startLoader();

        $.ajax({
            type: 'POST',
            url: '/api/getAllProductsByVendors',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({vendor: vendor}),
            dataType: 'json'
        }).done((res) => {
            this.stopLoader();

            if (res.data) {
                if (res.data.product && Array.isArray(res.data.product) && res.data.product.length) {
                    this.setState({
                        allProducts: res.data.product,
                        shouldLoadProducts: false
                    });
                } else {
                    this.setState({
                        allProducts: [],
                        shouldLoadProducts: false
                    }, () => {
                        app.addAlert('error', 'Error: No products were found.');
                    });
                }
            } else if (res.err) {
                app.addAlert('error', res.err);
            }
        }).fail(() => {
            this.stopLoader();

            app.addAlert('error', 'Error: failed to load products.');
        });
    }

    handleChangeVendorNameField(chosenRequest, index) {
        if (index !== -1 || this.state.allVendors.includes(chosenRequest)) {
            let data = this.state.data;
            if (data.vendor !== chosenRequest) {
                data.vendor = chosenRequest;
                data.products = [];
                // Prepare to load this vendor's products, and move to stepIndex 1.
                this.setState({
                    data: data,
                    allProducts: [],
                    isAllProductsChecked: true,
                    shouldLoadProducts: true,
                    stepIndex: 1
                });
            }
        } else {
            app.addAlert('warning', 'Please enter an existing vendor');
        }
    }

    handleAddRequestedField(chip) {
        let allData = this.state.allProducts;
        let inputData = this.state.data.products;
        let input;

        if (!Array.isArray(chip)) {
            input = chip;
        } else {
            input = chip[0];
        }

        // Check if allData contains the input.
        let inputIndex = allData.indexOf(input);

        if (inputIndex > -1) {
            let inputObjToAdd = allData[inputIndex];

            // Check if inputData not already contains the input.
            if (inputData.indexOf(input) === -1) {
                // Add to the ChipSelect.
                let data = this.state.data;

                data.products.push(inputObjToAdd);

                this.setState({data: data});
            } else {
                app.addAlert('warning', 'Product already selected.');
            }
        } else {
            app.addAlert('error', 'No such product.');
        }
    }

    handleDeleteRequestedField(value) {
        let inputData = this.state.data.products;

        // Check the index of the value of selectedChip in inputData.
        let index = inputData.indexOf(value);
        if (index > -1) {
            // Update the ChipSelect.
            let data = this.state.data;

            data.products.splice(index, 1);

            this.setState({data: data});
        }
    }

    handleDone() {
        if (this.state.data.vendor) {
            let data = this.state.data;

            if (this.state.isAllProductsChecked) {
                data.products = [];
            }

            if (this.state.allProducts.length) {
                data.allProducts = this.state.allProducts;
            }

            if (this.state.isEditMode) {
                this.props.navigateDoneEdit(data, this.state.editObjectIndex);
            } else {
                this.props.navigateDone(data);
            }
        }
    }

    render() {
        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        const divStyle = {padding: '0px 20px'};

        const autoCompleteStyle = {overflowY: 'auto', height: '200px', maxHeight: '200px'};

        let modalTitle = (this.state.isEditMode) ? 'Edit Vendor' : 'Add Vendor';

        let productsChip = (this.state.isAllProductsChecked) ? {display: 'none'} : {display: 'inline-block'};

        let isVendorSelected = !!this.state.data.vendor;

        if (isVendorSelected) {
            modalTitle += ' - ' + this.state.data.vendor;
        }

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.props.navigateCancel}
            />,
            <FlatButton
                label="Done"
                secondary={true}
                onTouchTap={this.handleDone.bind(this)}
                disabled={!isVendorSelected || this.state.showLoader}
            />

        ];

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title={modalTitle}
                        actions={actions}
                        modal={false}
                        open={this.state.open}
                        onRequestClose={this.props.navigateCancel}
                        autoScrollBodyContent={true}
                    >
                        <Loader show={this.state.showLoader} message={spinner}>
                            <Row style={divStyle}>
                                <Stepper
                                    activeStep={this.state.stepIndex}
                                    linear={false}
                                    orientation="vertical"
                                >
                                    <Step>
                                        <StepButton onClick={this.handleStepActionsClick.bind(this, 0)}>
                                            Select Vendor
                                        </StepButton>
                                        <StepContent>
                                            <AutoComplete
                                                hintText="Search Vendor Name"
                                                floatingLabelText="Vendor Name"
                                                dataSource={this.state.allVendors}
                                                menuStyle={autoCompleteStyle}
                                                maxSearchResults={50}
                                                filter={AutoComplete.caseInsensitiveFilter}
                                                onNewRequest={this.handleChangeVendorNameField.bind(this)}
                                                searchText={this.state.data.vendor}
                                                fullWidth={true}
                                                openOnFocus={true}
                                                disabled={this.state.isEditMode}
                                            />
                                            <br/>
                                            {this.renderStepActions(0)}
                                        </StepContent>
                                    </Step>
                                    <Step>
                                        <StepButton onClick={this.handleStepActionsClick.bind(this, 1)}>
                                            Choose Products (Optional)
                                        </StepButton>
                                        <StepContent>
                                            <br/>
                                            <span
                                                style={{width: '100%'}}>Which products could be of interest to you?</span>
                                            <br/>
                                            <Checkbox
                                                label="All Products"
                                                checked={this.state.isAllProductsChecked}
                                                onCheck={this.updateProductsCheckBox.bind(this)}
                                                style={{margin: '10px 0px'}}
                                                data-tip={(this.state.isAllProductsChecked) ?
                                                    'Uncheck to select specific products' : 'Check to select all products'}
                                            />
                                            <ReactTooltip place="bottom"/>
                                            <br/>
                                            <ChipInput
                                                hintText="Search Products"
                                                floatingLabelText="Requested Products"
                                                menuStyle={autoCompleteStyle}
                                                filter={AutoComplete.caseInsensitiveFilter}
                                                value={this.state.data.products}
                                                dataSource={this.state.allProducts}
                                                onRequestAdd={(chip) => this.handleAddRequestedField(chip)}
                                                onRequestDelete={(chip) => this.handleDeleteRequestedField(chip)}
                                                maxSearchResults={50}
                                                style={productsChip}
                                                fullWidth={true}
                                                fullWidthInput={true}
                                                openOnFocus={true}
                                            />
                                            <br/>
                                            {this.renderStepActions(1)}
                                        </StepContent>
                                    </Step>
                                </Stepper>
                            </Row>
                        </Loader>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
