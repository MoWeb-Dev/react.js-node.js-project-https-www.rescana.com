import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardTitle} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import Loader from 'react-loader-advanced';
import ActionAdd from 'material-ui/svg-icons/content/add-circle';
import ReportSettingsModalManualData from '../intelReports/report-add-manual.jsx';
import {PROJECT_NAME, SESSION_COMPANIES, ORGANIZATION_ID} from '../../../../config/consts';
import DataTables from 'material-ui-datatables';
import EditButton from 'material-ui/svg-icons/editor/mode-edit';
import DeleteButton from 'material-ui/svg-icons/action/delete-forever';
import '../../../../public/css/main.css';
import '../../../../public/css/survey.css';
import themeForFont from "../../app/themes/themeForFont";
import ArrowDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down-circle';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

const shortid = require('shortid');

const TYPE_EXCEL = 'EXCEL';
const TYPE_PDF = 'PDF';
const TYPE_WORD = 'WORD';

/**
 * A windows for selecting settings before exporting a report.
 */
export default class ExportSurveyReportSettings extends React.Component {
    constructor(props) {
        super(props);

        const wrappableStyle = {
            whiteSpace: 'pre-wrap',
            wordBreak: 'break-word'
        };

        this.state = {
            isProjectsSelected: true,
            companies: [],
            projects: [],
            companiesInputVal: [],
            projectsInputVal: [],
            showLoader: false,
            showManualDataPage: false,
            manualData: [],
            editFindingObject: {},
            editFindingIndex: -1,
            filesID: shortid.generate(),
            tableColumns: [{
                label: 'Title',
                key: 'title',
                style: wrappableStyle
            }, {
                label: 'Content',
                key: 'text',
                style: wrappableStyle
            }, {
                label: 'Attachments',
                key: 'files',
                style: wrappableStyle
            }, {
                label: 'Edit',
                key: 'edit'
            }, {
                label: 'Delete',
                key: 'delete'
            }],
            projectIdx: ''
        };

        this.NotifySuccess = this.NotifySuccess.bind(this);
        this.getProjectsByID = this.getProjectsByID.bind(this);
        this.setProjectsOnState = this.setProjectsOnState.bind(this);
        this.handleOpenMenu = this.handleOpenMenu.bind(this);
        this.handleCloseMenu = this.handleCloseMenu.bind(this);
        this.handleProjectSelect = this.handleProjectSelect.bind(this);
    }
    handleOpenMenu(event) {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            openMenu: true,
            anchorEl: event.currentTarget
        });
    }

    handleCloseMenu() {
        this.setState({
            openMenu: false
        });
    }

    handleProjectSelect(idx, e) {
        e.preventDefault();

        this.setState({projectIdx: this.state.projects[idx].projectName, projectsInputVal: [this.state.projects[idx]], openMenu: false});
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (user) {
            this.setState({admin: user.admin}, () => {
                this.getProjectsByID(user);
            });
        }
    }

    getProjectsByID(user) {
        $.ajax({
            type: 'GET',
            url: '/api/getAllProjectsById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projects) => {
            if (projects && Array.isArray(projects) && projects.length !== 0) {
                this.setProjectsOnState(projects, user);
            } else if (user) {
                this.setProjectsOnState(null, user);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
        });
    }

    setProjectsOnState(projects, user) {
        // Set loaded project as default if exists.
        let loadedProject = localStorage.getItem(PROJECT_NAME);
        let isLoadedProjectFound = false;

        let stateObj = {
            isProjectsSelected: true,
            showLoader: false,
            showManualDataPage: this.state.showManualDataPage,
            manualData: [],
            editFindingObject: {},
            editFindingIndex: -1,
            projectIdx: loadedProject ? loadedProject : ''
        };

        if (projects) {
            stateObj.companies = [];
            stateObj.projects = projects.map((currPrj) => {
                if (currPrj.id && currPrj.projectName) {
                    if (loadedProject && !isLoadedProjectFound && currPrj.projectName === loadedProject) {
                        isLoadedProjectFound = true;
                        stateObj.projectsInputVal = [{
                            id: currPrj.id,
                            projectName: currPrj.projectName,
                            selectedCompanies: currPrj.selectedCompanies
                        }];
                        stateObj.companiesInputVal = [];
                    }
                    return {
                        id: currPrj.id,
                        projectName: currPrj.projectName,
                        selectedCompanies: currPrj.selectedCompanies
                    };
                }
            });
        }

        if (user) {
            stateObj.user = user;
        }

        this.setState(stateObj);
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    requestReport(dataObj) {
        this.startLoader();

        $.ajax({
            type: 'POST',
            url: '/api/exportReport',
            data: JSON.stringify(dataObj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            timeout: 60000,
            async: true
        }).done((res) => {
            this.stopLoader();

            if (!res.ok) {
                app.addAlert('error', 'Error: Failed to export');
            } else {
                res.dataArr.map((curData)=>{
                    // Meaning that the PDF was created successfully. (curData is the fileName)
                    window.location = '/surveyReport/' + curData;
                });

                this.NotifySuccess();
            }
        }).fail(() => {
            this.stopLoader();
            app.addAlert('error', 'Error: Failed to export');
        });
    }

    handleExportReport(reportType, e) {
        e.preventDefault();
        let orgId = localStorage.getItem(ORGANIZATION_ID);
        if (this.state.user && this.state.user.id) {
            if (this.state.isProjectsSelected && this.state.projectsInputVal) {
                    let dataObj = {
                        uid: this.state.user.id,
                        orgId: orgId,
                        projects: this.state.projectsInputVal,
                        manualData: this.state.manualData,
                        manualDataID: this.state.filesID,
                        windowLocation: window && window.location && window.location.origin ? window.location.origin + '/surveyReport/' : null,
                        displayScoreBackwards: !!(this.state.user.userPreferencesConfig &&
                            this.state.user.userPreferencesConfig.displayScoreBackwards)
                    };

                    dataObj.type = (reportType === TYPE_PDF) ? 'pdf' : (reportType === TYPE_WORD) ? 'word' : 'excel';
                    dataObj.reportKind = 'surveyReport';

                    this.requestReport(dataObj);
            }
        } else {
            app.addAlert('error', 'Error: failed to load user\'s data');
        }
    }

    NotifySuccess() {
        console.log('Report created successfully.');
        app.addAlert('success', 'Report created successfully.');
    }

    navigateToManualDataPage() {
        const maxData = 10;
        if (this.state.manualData.length < maxData) {
            this.setState({
                showManualDataPage: true
            });
        } else {
            app.addAlert('error', 'Limit of ' + maxData + ' findings exceeded');
        }
    }

    navigateFromManualDataPage() {
        // State closes the modal and resets editMode to false.
        this.setState({showManualDataPage: false, editFindingObject: {}, editFindingIndex: -1});
    }

    saveDataFromManualPage(newData, editIndex = -1) {
        let manualData = this.state.manualData;

        // Check if any files exist.
        if (newData.files && newData.files.length > 0) {
            let files = newData.files;
            let filesUploadedAlready = [];

            let data = new FormData();

            // A boolean indicates whether a new image inserted (and need to be saved on server) or not.
            let isNewImagesAdded = false;

            for (let i = 0; i < files.length; i++) {
                // Separate between files already saved on server and those that are not.
                if (files[i].name && !files[i].path) {
                    data.append('intelManualFinding', files[i], this.state.filesID + files[i].name);
                    isNewImagesAdded = true;
                } else {
                    filesUploadedAlready = filesUploadedAlready.concat(files[i]);
                }
            }

            if (isNewImagesAdded) {
                $.ajax({
                    url: '/api/intelImageUpload',
                    dataType: 'json',
                    data: data,
                    enctype: 'multipart/form-data',
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: (res) => {
                        if (res && res.result) {
                            let filesPaths = res.result;
                            newData.files = filesUploadedAlready.concat(filesPaths);

                            // This section handles that new files inserted in EditMode.
                            if (editIndex !== -1) {
                                manualData[editIndex] = newData;

                                // State saves the new data from modal, closes the modal and resets editMode to false.
                                this.setState({
                                    manualData: manualData,
                                    showManualDataPage: false,
                                    editFindingObject: {},
                                    editFindingIndex: -1
                                },
                                app.addAlert('success', 'Finding updated.'));

                            // This section handles that new files are inserted in AddMode.
                            } else {
                                manualData.push(newData);

                                // State saves the new data from modal and closes the modal.
                                this.setState({manualData: manualData, showManualDataPage: false},
                                    app.addAlert('success', 'New finding inserted.'));
                            }
                        } else {
                            app.addAlert('error', 'Failed to add new finding.');
                        }
                    },
                    error: () => {
                        app.addAlert('error', 'Failed to add new finding.');
                    }
                });
            } else {
                // This section handles that are files but all of them are already saved and it is in EditMode.
                if (editIndex !== -1) {
                    manualData[editIndex] = newData;

                    // State saves the new data from modal, closes the modal and resets editMode to false.
                    this.setState({
                        manualData: manualData,
                        showManualDataPage: false,
                        editFindingObject: {},
                        editFindingIndex: -1
                    },
                    app.addAlert('success', 'Finding updated.'));

                // This section handles that are files but all of them are already saved and it is AddMode.
                } else {
                    manualData.push(newData);

                    // State saves the new data from modal and closes the modal.
                    this.setState({manualData: manualData, showManualDataPage: false},
                        app.addAlert('success', 'New finding inserted.'));
                }
            }
        } else {
            // This section handles that there are no files inserted but other data is changed in EditMode.
            if (editIndex !== -1) {
                manualData[editIndex] = newData;

                // State saves the new data from modal, closes the modal and resets editMode to false.
                this.setState({
                    manualData: manualData,
                    showManualDataPage: false,
                    editFindingObject: {},
                    editFindingIndex: -1
                },
                app.addAlert('success', 'Finding updated.'));

            // This section handles that there are no files inserted but other data is changed in AddMode.
            } else {
                manualData.push(newData);

                // State saves the new data from modal and closes the modal.
                this.setState({manualData: manualData, showManualDataPage: false},
                    app.addAlert('success', 'New finding inserted.'));
            }
        }
    }

    editTableFinding(editData, editIdx) {
        // Open the modal in edit mode.
        this.setState({showManualDataPage: true, editFindingObject: editData, editFindingIndex: editIdx});
    }

    deleteTableFinding(index) {
        let manualData = this.state.manualData;
        manualData.splice(index, 1);
        this.setState({manualData: manualData});
    }

    render() {
        let user = app.getAuthUser();

        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        const rowStyle = {display: 'flex'};

        const firstColStyle = {width: '20%', paddingBottom: '8px'};

        const chipsStyle = {
            width: '80%',
            overflowY: 'auto',
            marginRight: '30px',
            paddingBottom: '8px',
            marginBottom: '8px',
            height: 'auto',
            maxHeight: '280px'
        };

        const myFindingsStyle = {padding: '15px 0px 0px 15px', fontSize: '16px'};

        let tableBodyStyle = {
            'overflowX': 'auto'
        };

        let tableStyle = {
            'width': '99%',
            'margin': 'auto'
        };

        let buttonStyle = {
            cursor: 'pointer'
        };

        let manualData = this.state.manualData;

        let tableData = [];

        for (let i = 0; i < manualData.length; i++) {
            const maxChars = 15;
            let title;
            let text;

            if (manualData[i].title.length > maxChars) {
                title = manualData[i].title.slice(0, maxChars) + '...';
            } else {
                title = manualData[i].title;
            }

            if (manualData[i].text.length > maxChars) {
                text = manualData[i].text.slice(0, maxChars) + '...';
            } else {
                text = manualData[i].text;
            }

            tableData[i] = {
                title: title,
                text: text,
                files: manualData[i].files.length,
                edit: <EditButton onTouchTap={this.editTableFinding.bind(this, manualData[i], i)}
                    style={buttonStyle}/>,
                delete: <DeleteButton onTouchTap={this.deleteTableFinding.bind(this, i)}
                    style={buttonStyle}/>
            };
        }

        let projects = this.state.projects
            .sort((a, b) => {
                let textA = a.projectName;
                let textB = b.projectName;
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            })
            .map((project, idx) => {
                return <MenuItem value={project.projectName}
                                 key={idx}
                                 style={{color: 'black', fontSize: '13px'}}
                                 primaryText={project.projectName}
                                 onTouchTap={this.handleProjectSelect.bind(this, idx)}
                />
            });

        const actions = [
            <FlatButton
                key={65}
                label="Export PDF"
                secondary={true}
                onTouchTap={this.handleExportReport.bind(this, TYPE_PDF)}
                disabled={this.state.showLoader}
            />,
            <FlatButton
                key={66}
                label="Export Excel"
                secondary={true}
                onTouchTap={this.handleExportReport.bind(this, TYPE_EXCEL)}
                disabled={this.state.showLoader}
            />

        ];

        if (this.state.admin) {
            actions.push(<FlatButton
                key={67}
                label="Export Word"
                secondary={true}
                onTouchTap={this.handleExportReport.bind(this, TYPE_WORD)}
                disabled={this.state.showLoader}
            />);
        }

        return (
            <div className={pageStyleClass}>
                <Loader show={this.state.showLoader} message={spinner}>
                    <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                        <div>
                            <div style={{margin: '40px 0px 40px 10px'}}>
                                <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                                <span style={{ position: 'relative', top: '-6px', fontSize: 22}}>EXPORT SURVEY REPORT</span>
                            </div>
                            <Card style={{margin: 10, padding: 15}}>
                                <p style={myFindingsStyle}>Please select the project you would like to review:</p>
                                <div style={rowStyle}>
                                    <div style={chipsStyle}>
                                        <FlatButton
                                            label={this.state.projectIdx}
                                            labelPosition="after"
                                            icon={<ArrowDownIcon
                                                color='#212121' style={{height: 25, width: 25, top: -2}}/>}
                                            labelStyle={{color: 'black',  textTransform: 'capitalize', bottom: '2px', marginRight: 5}}
                                            onTouchTap={this.handleOpenMenu}
                                            style={{height: '50px', color: 'black', position: 'relative', top: '-6px', marginLeft: 18}}
                                        />
                                        <Popover
                                            tooltip='Projects'
                                            tooltipPosition="bottom-center"
                                            open={this.state.openMenu}
                                            anchorEl={this.state.anchorEl}
                                            anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                                            targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                                            onRequestClose={this.handleCloseMenu}>
                                            <Menu>
                                                {projects}
                                            </Menu>
                                        </Popover>
                                    </div>
                                </div>
                            </Card>
                            {(user && user.userRole && user.userRole === 'basic') ? null :
                                <Card style={{margin: 10, padding: 15}}>
                                    <p style={myFindingsStyle}>Optional: Click 'Add' if you wish to add findings of
                                        your own at the end of the report</p>
                                    <div style={rowStyle}>
                                        <div style={firstColStyle}>
                                            <FlatButton
                                                label="Add"
                                                icon={<ActionAdd/>}
                                                onClick={this.navigateToManualDataPage.bind(this)}
                                                style={{marginBottom: '10px', marginLeft: 18}}
                                            />
                                            <ReportSettingsModalManualData
                                                open={this.state.showManualDataPage}
                                                navigateDone={this.saveDataFromManualPage.bind(this)}
                                                navigateCancel={this.navigateFromManualDataPage.bind(this)}
                                                editFindingObject={this.state.editFindingObject}
                                                editFindingIndex={this.state.editFindingIndex}
                                                navigateDoneEdit={this.saveDataFromManualPage.bind(this)}
                                            />
                                        </div>
                                        {(!this.state.manualData || this.state.manualData.length === 0) ? null :
                                            <div style={chipsStyle}>
                                                <DataTables
                                                    selectable={false}
                                                    showRowHover={true}
                                                    showFooterToolbar={false}
                                                    showCheckboxes={false}
                                                    tableBodyStyle={tableBodyStyle}
                                                    tableStyle={tableStyle}
                                                    columns={this.state.tableColumns}
                                                    data={tableData}
                                                />
                                            </div>
                                        }
                                    </div>
                                </Card>
                            }
                            <Card style={{margin: 10, padding: 15}}>
                                <p style={myFindingsStyle}>Please select one of the possibilities to export your project surveys status report: </p>
                                {actions}
                            </Card>
                        </div>
                    </MuiThemeProvider>
                </Loader>
            </div>
        );
    }
};
