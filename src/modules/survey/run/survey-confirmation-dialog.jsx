import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";

export default class confirmationDialog extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };
    }

    handleClose() {
        this.props.closeConfirm();
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open
        });
    }

    render() {
        const actions = [
            <FlatButton
                label="Done"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleClose.bind(this)}
            />
        ];

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title="Survey Submit"
                        actions={actions}
                        modal={true}
                        open={this.state.open}
                        onRequestClose={this.handleClose.bind(this)}
                    >
                        Thank you for submitting the survey, a notification will be sent to the survey author.
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
}
