import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import t from '../../../../config/i18n';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import {LTR, RTL, MY_LANG, DEFAULT_LANG} from '../../../../config/consts';
import {isLTRLanguage} from '../../common/AppHelpers';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class excelSurveyDownloadDialog extends React.Component {
    constructor(props) {
        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            open: false,
            message: '',
            recipient: '',
            senderEmail: '',
            senderEmailExpiry: '',
            sender: '',
            lang: myLang,
            excelFormat: 'multiple_sheets'
        };
    }

    handleClose() {
        this.props.close();
    };

    handleDownload(e) {
        e.preventDefault();
        this.props.download(this.state.excelFormat, this.props.said);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open,
            excelFormat: 'multiple_sheets',
            said: nextProps.data
        });
    }

    setExcelFormat(ignore, value) {
        this.setState({excelFormat: value});
    }

    render() {
        const actions = [
            <FlatButton
                label={t[this.state.lang]['Survey_Download']}
                primary={true}
                style={{
                    color: 'white',
                    marginBottom: '5px',
                    marginRight: '5px',
                    height: 40
                }}
                keyboardFocused={true}
                backgroundColor={'#0091ea'}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                onClick={this.handleDownload.bind(this)}
            />,
            <FlatButton
                label={t[this.state.lang]['Survey_Cancel']}
                primary={true}
                backgroundColor={'#0091ea'}
                style={{color: 'white',marginRight: '5px', marginBottom: '5px', height: 40}}
                keyboardFocused={true}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                onClick={this.handleClose.bind(this)}
            />
        ];

        let style = {
            radioBtn: {
                'marginBottom': '5px'
            },
            radioBtnGrp: {
                'marginTop': '5px'
            }
        };

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = (isLTRLanguage(myLang)) ? LTR : RTL;
        let isLTR = (myDir === LTR);

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title={t[this.state.lang]['Survey_Excel_Download']}
                        actions={actions}
                        open={this.state.open || false}
                        actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                        bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                        autoScrollBodyContent={true}
                        modal={true}
                        repositionOnUpdate={false}
                        contentStyle={{direction: myDir, maxWidth: '30%', borderRadius: '7px 7px 7px 7px'}}
                        titleStyle={{
                            fontSize: 18,
                            background: 'rgba(0,0,0,0.7)',
                            color: 'white', textAlign: 'center',
                            borderRadius: '2px 2px 0px 0px',
                            textTransform: 'uppercase',
                        }}
                    >
                        <br/>
                        <br/>
                        <span>{t[this.state.lang]['Survey_Excel_OneSheet']}</span>
                        <br/>
                        <br/>
                        <RadioButtonGroup
                            name="excelFormat"
                            defaultSelected="multiple_sheets"
                            onChange={this.setExcelFormat.bind(this)}
                            style={style.radioBtnGrp}
                        >
                            <RadioButton
                                value="multiple_sheets"
                                label={t[this.state.lang]['Survey_Excel_MultiSheet_Label']}
                                style={style.radioBtn}
                            />
                            <RadioButton
                                value="one_sheet"
                                label={t[this.state.lang]['Survey_Excel_OneSheet_Label']}
                                style={style.radioBtn}
                            />
                        </RadioButtonGroup>
                        <br/>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
