import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Dropzone from 'react-dropzone';
import FontIcon from 'material-ui/FontIcon';
import ActionDelete from 'material-ui/svg-icons/content/clear';
import FileIcon from 'material-ui/svg-icons/editor/insert-drive-file';
import IconButton from 'material-ui/IconButton';
import classNames from 'classnames';
import ReactTooltip from 'react-tooltip';
import {isImage} from '../common/survey-helpers.js';
import {LTR, RTL, MY_LANG, MY_DIR, DEFAULT_LANG, DEFAULT_DIR, WILDFIRE_STATUS_TYPES} from '../../../../config/consts';
import t from '../../../../config/i18n';
import {isLTRLanguage} from '../../common/AppHelpers';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class UploadModal extends React.Component {
    constructor(props) {
        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.state = {
            open: false,
            files: this.props.files || [],
            disabled: true,
            acceptedFiles: ['image/jpeg', 'image/png', 'image/bmp', 'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/visio', 'application/x-visio', 'application/vnd.visio', 'application/visio.drawing',
                'application/vsd', 'image/x-vsd', 'zz-application/zz-winassoc-vsd',
                'application/vnd.ms-visio.viewer', 'application/pdf',
                /* "application/zip"*/ 'application/octet-stream'],
            lang: myLang,
            dir: myDir
        };
    }

    handleClose() {
        this.props.closeDialog();
        this.setState({open: false});
    };

    onDrop(files) {
        let oldFiles = this.state.files;
        oldFiles = oldFiles.concat(files);
        if (oldFiles.length > 3) {
            app.addAlert('error', 'Cannot upload more then 3 items per question. Please use zip to combine into one file if needed.');
        } else {
            this.setState({
                files: oldFiles
            }, this.changeButtonDisable);
        }
    }

    handleRemove(file, fileIndex) {
        // This is to prevent memory leaks.
        window.URL.revokeObjectURL(file.preview);
        let files = this.state.files;
        files.splice(fileIndex, 1);
        this.setState(files, this.changeButtonDisable);
        if (file.path) {
            this.props.deleteImage(file);
        }
    }

    changeButtonDisable() {
        if (this.state.files.length !== 0) {
            this.setState({
                disabled: false
            });
        } else {
            this.setState({
                disabled: true
            });
        }
    }

    saveImages() {
        if (this.state.files.length > 3) {
            app.addAlert('error', 'Cannot upload more then 3 items per question.');
        } else {
            this.props.saveImages(this.state.files);
        }
    }

    onDropRejected() {
        return app.addAlert('error', 'File is of unaccepted type or is larger than max size(5MB)');
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open,
            files: nextProps.files
        });
    }

    render() {
        let img;
        let dropZoneClass = classNames({
            'dropZone': true
        });
        let activeDropZoneClass = classNames({
            'stripes': true
        });
        let rejectDropZoneClass = classNames({
            'rejectStripes': true
        });
        let uploadCloudIcon = classNames({
            'material-icons': true,
            'uploadIconSize': true
        });
        let imageContainer = classNames({
            'imageContainer': true,
            'col-md-3': true,
            'fileIconImg': true
        });

        let myLang = this.props.loadedLang || this.state.lang;

        let isLTR = isLTRLanguage(myLang);

        let myDir = (isLTR) ? LTR : RTL;

        let buttonsContainer = classNames({
            'uploadDialogButtonsLTR': isLTR,
            'uploadDialogButtonsRTL': !isLTR
        });

        let middle = classNames({
            'middleLTR': isLTR,
            'middleRTL': !isLTR
        });

        let toolTipPos = (isLTR) ? 'bottom-right' : 'bottom-left';

        let previews = this.state.files.map((file, i) => {
            // Display the file only if there is no property called scanStatus or if there is one and its value is SAFE.
            let shouldDisplayFile = !!(!file.hasOwnProperty('scanStatus') || (file.scanStatus && file.scanStatus === WILDFIRE_STATUS_TYPES.SAFE));
            let path = file.preview || '/evidence' + file.path;
            if (isImage(file)) {
                if (shouldDisplayFile) {
                    img = <img className="smallPreviewImg" src={path}/>;
                } else {
                    img = <img className="smallPreviewImg" src={path} data-tip={'Image was discovered as Malicious'}/>;
                }
            } else {
                if (shouldDisplayFile) {
                    img = <FileIcon className="smallPreviewImg"/>;
                } else {
                    img = <FileIcon className="smallPreviewImg" data-tip={'File was discovered as Malicious'}/>;
                }
            }

            return <div className={imageContainer} key={i}>
                {img}
                <div className={middle}>
                    <IconButton touch={true} tooltip={t[myLang]['Survey_Delete']} tooltipPosition={toolTipPos}>
                        <ActionDelete
                            className="removeBtn"
                            onTouchTap={this.handleRemove.bind(this, file, i)}
                        />
                    </IconButton>
                </div>
                <ReactTooltip place="top"/>
            </div>;
        });

        const actions = [
            <FlatButton
                label={t[myLang]['UploadDialog_Cancel']}
                primary={true}
                onTouchTap={this.handleClose.bind(this)}
            />,
            <FlatButton
                label={t[myLang]['UploadDialog_Submit']}
                primary={true}
                disabled={this.state.disabled}
                onTouchTap={this.saveImages.bind(this)}
            />
        ];

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title={t[myLang]['UploadDialog_Title']}
                        actions={actions}
                        actionsContainerClassName={buttonsContainer}
                        modal={false}
                        open={this.state.open}
                        contentStyle={{direction: myDir}}
                        onRequestClose={this.handleClose.bind(this)}
                        autoScrollBodyContent={true}
                    >
                        <Dropzone
                            accept={this.state.acceptedFiles.join(',')}
                            onDrop={this.onDrop.bind(this)}
                            className={dropZoneClass}
                            activeClassName={activeDropZoneClass}
                            rejectClassName={rejectDropZoneClass}
                            onDropRejected={this.onDropRejected.bind(this)}
                            maxSize={5000000}
                        >
                            <div className="dropzoneTextStyle">
                                <p className="dropzoneParagraph">{t[myLang]['UploadDialog_Dropzone_Text']}</p>
                                <br/>
                                <FontIcon
                                    className={uploadCloudIcon}
                                >
                                    cloud_upload
                                </FontIcon>
                            </div>
                        </Dropzone>
                        <br/>
                        <div className="row">
                            {this.state.files.length ? <span>Preview:</span> : ''}
                        </div>
                        <div className="row">
                            <br/>
                            {previews}
                        </div>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
