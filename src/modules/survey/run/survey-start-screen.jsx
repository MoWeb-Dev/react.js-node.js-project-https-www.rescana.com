import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardTitle} from 'material-ui/Card';
import PlayButton from 'material-ui/svg-icons/av/play-arrow';
import SendButton from 'material-ui/svg-icons/communication/email';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton/IconButton';
import MenuItem from 'material-ui/MenuItem';
import DownloadButton from 'material-ui/svg-icons/file/file-download';
import DataTables from 'material-ui-datatables';
import FlatButton from 'material-ui/FlatButton';
import SendSurveyModal from './send-survey-dialog.jsx';
import DownloadExcelOptions from './excel-survey-download-dialog.jsx';
import ShareLinkSurveyModal from './share-survey-link-dialog.jsx';
import FuzzySearch from 'fuzzy-search';
import {browserHistory} from 'react-router';
import React from 'react';
import {deleteIDFromSurveys} from '../common/survey-helpers';
import {RTL, MY_LANG, MY_DIR, DEFAULT_LANG, DEFAULT_DIR, LTR, ORGANIZATION_ID} from '../../../../config/consts';
import t from '../../../../config/i18n';
import moment from 'moment';
import classNames from 'classnames';
import Loader from 'react-loader-advanced';
import '../../../../public/css/survey.css';
import UploadExistingSurveyModal from './upload-existing-dialog.jsx';
import themeForFont from "../../app/themes/themeForFont";
import Delete from 'material-ui/svg-icons/action/delete';
import DeleteDialog from "../../app/delete-dialog.jsx";
import EditIcon from 'material-ui/svg-icons/image/edit';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';


export default class SurveyStartScreen extends React.Component {
    constructor() {
        super();

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.state = {
            openSendSurveyModal: false,
            openShareLinkModal: false,
            openDownloadExcelOptionsModal: false,
            openUploadExistingSurveyModal: false,
            dataForSend: {},
            dataForExcel: {},
            tableColumns: [],
            tableData: [],
            sortedTableData: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            page: 1,
            lang: myLang,
            dir: myDir,
            showLoader: false,
            openDeleteDialog: false,
            curRowIdToDelete: null,
            openEditSurveyDialog: false,
            curSurveyObj: {surveyName: '', surveyId: 0},
            fullData: {}
        };

        this.handleOpenDeleteDialog = this.handleOpenDeleteDialog.bind(this);
        this.handleCloseDeleteDialog = this.handleCloseDeleteDialog.bind(this);
        this.deleteSurveysAnswers = this.deleteSurveysAnswers.bind(this);
        this.deleteSurveyAnswers = this.deleteSurveyAnswers.bind(this);
        this.sendSurveyAnswers = this.sendSurveyAnswers.bind(this);
        this.getSurveysWithAnswers = this.getSurveysWithAnswers.bind(this);
        this.setResults = this.setResults.bind(this);
        this.handleSortOrderChange = this.handleSortOrderChange.bind(this);
        this.handleFilterValueChange = this.handleFilterValueChange.bind(this);
        this.handleNextPageClick = this.handleNextPageClick.bind(this);
        this.handlePreviousPageClick = this.handlePreviousPageClick.bind(this);
        this.handleRowSizeChange = this.handleRowSizeChange.bind(this);
        this.startLoader = this.startLoader.bind(this);
        this.stopLoader = this.stopLoader.bind(this);
        this.handleCloseEditSurveyDialog = this.handleCloseEditSurveyDialog.bind(this);
    }

    setResults(data) {
        let isRight = (this.state.dir === RTL);

        let hideFlag = false;
        if (this.state.user && this.state.user.userRole === 'basic') {
            hideFlag = true;
        }

        let hide = classNames({
            'hide': hideFlag
        });

        const wrappableStyle = {
            whiteSpace: 'pre-wrap',
            wordBreak: 'break-word'
        };

        // Create table column headers
        let tableColumns = [{
            sortable: true,
            label: t[this.state.lang]['Survey_Name'],
            key: 'SurveyName',
            alignRight: isRight,
            style: wrappableStyle
        }, {
            sortable: true,
            label: t[this.state.lang]['Survey_Surveyed_Company'],
            key: 'Company',
            alignRight: isRight,
            style: wrappableStyle
        }, {
            sortable: true,
            label: t[this.state.lang]['Survey_Complete'],
            key: 'Complete',
            alignRight: isRight,
            style: wrappableStyle
        }, {
            sortable: true,
            label: t[this.state.lang]['Survey_Last_Modified'],
            key: 'LastChangedDate',
            alignRight: isRight,
            style: wrappableStyle
        }, {
            label: t[this.state.lang]['Survey_Continue'],
            key: 'Continue',
            alignRight: isRight
        }, {
            label: t[this.state.lang]['Survey_Edit'],
            key: 'Edit',
            className: hide,
            alignRight: isRight
        }, {
            label: t[this.state.lang]['Survey_Download'],
            key: 'Download',
            alignRight: isRight
        }, {
            label: t[this.state.lang]['Survey_Send'],
            key: 'Send',
            className: hide,
            alignRight: isRight
        }, {
            label: t[this.state.lang]['Survey_Delete'],
            key: 'Delete',
            className: hide,
            alignRight: isRight
        }];

        let cursorStyle = {
            cursor: 'pointer'
        };

        let tableData = [];

        for (let i = 0; i < data.length; i++) {
            let progress = data[i].progress || 0;
            tableData[i] = {
                SurveyName: data[i].name,
                LastChangedDate: moment(data[i].lastChangedDate).calendar(null, {
                    sameDay: '[Today at] h:mma',
                    nextDay: '[Tomorrow at] h:mma',
                    nextWeek: 'dddd [at] h:mma',
                    lastDay: '[Yesterday at] h:mma',
                    lastWeek: '[Last] dddd [at] h:mma',
                    sameElse: 'YYYY/MM/DD, HH:mm:ss'
                }),
                Company: data[i].selectedCompanyName,
                Complete: Math.floor(progress) + '%',
                Continue: <PlayButton onTouchTap={this.runSurvey.bind(this, data[i].said)} style={cursorStyle}/>,
                Edit: <EditIcon onTouchTap={this.handleOpenEditSurvey.bind(this, data[i])} style={cursorStyle}/>,
                Download: <IconMenu
                    iconButtonElement={<IconButton><DownloadButton/></IconButton>}
                    anchorOrigin={{horizontal: 'middle', vertical: 'center'}}
                    targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                    menuStyle={{textAlign: 'center'}}
                    key={13}
                >
                    <MenuItem key={14} primaryText="Excel"
                              onTouchTap={this.showExcelDownloadModal.bind(this, data[i].said)}/>
                    <MenuItem key={15} primaryText="PDF" onTouchTap={this.createSurveyAnswerPDF.bind(this, data[i])}/>
                </IconMenu>,
                Delete: <IconButton key={19} onTouchTap={this.handleOpenDeleteDialog.bind(this, data[i].said)}><Delete/></IconButton>,

                Send: <IconMenu
                    className={hide}
                    iconButtonElement={<IconButton><SendButton/></IconButton>}
                    anchorOrigin={{horizontal: 'middle', vertical: 'center'}}
                    targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                    menuStyle={{textAlign: 'center'}}
                    key={16}>
                    <MenuItem key={17} primaryText="Send Email"
                              onTouchTap={this.openSendSurveyModal.bind(this, i, data[i])}/>
                    <MenuItem key={18} primaryText="Share Link"
                              onTouchTap={this.openShareLinkModal.bind(this, i, data[i])}/>
                </IconMenu>,
                surveyIdentifier: data[i].said
            };
        }

        this.setState({
            fullData: data,
            tableColumns: tableColumns,
            totalRowCount: tableData.length,
            tableData: tableData,
            sortedTableData: tableData.slice(0, this.state.rowCountInPage)
        });
    }

    handleOpenDeleteDialog(rowToDelete) {
        this.setState({openDeleteDialog: true, curRowIdToDelete: rowToDelete});
    };

    handleOpenEditSurvey(rowObj) {

        if (rowObj) {
            let curSurveyObject = this.state.curSurveyObj;
            curSurveyObject.surveyId = rowObj._id || rowObj.id || 0;
            curSurveyObject.surveyName = rowObj.name || "";
            this.setState({openEditSurveyDialog: true, curSurveyObj: curSurveyObject});
        }
    };

    handleCloseEditSurveyDialog() {
        this.setState({openEditSurveyDialog: false, curSurveyObj: {surveyName: '', surveyId: 0}});
    };

    handleChangeSurveyName(e) {
        let curSurveyObject = this.state.curSurveyObj;
        curSurveyObject.surveyName = e.target.value || "";
        this.setState({curSurveyObj: curSurveyObject});
    };

    saveSurveyEditedData() {
        let curSurveyObject = this.state.curSurveyObj;
        if (curSurveyObject && curSurveyObject.surveyId) {
            if (curSurveyObject.surveyName !== null && curSurveyObject.surveyName === '') {
                app.addAlert('error', 'Error: Survey Name Field is Required!');
            } else {
                $.ajax({
                    type: 'POST',
                    url: '/api/saveSurveyEditedData',
                    data: JSON.stringify(curSurveyObject),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((res) => {
                    if (res && res.ok) {
                        let data = this.state.fullData;
                        for (let i = 0; i < data.length; i++) {
                            if (data[i] && data[i]._id === curSurveyObject.surveyId) {
                                data[i].name = curSurveyObject.surveyName;
                                break;
                            }
                        }
                        this.setResults(data);
                        this.handleCloseEditSurveyDialog();
                        app.addAlert('success', 'Updated Survey!');
                    } else {
                        app.addAlert('error', 'Error: failed to update survey');
                    }
                }).fail(() => {
                    app.addAlert('error', 'Error: failed to update survey');
                });
            }
        } else {
            app.addAlert('error', 'Error: failed to update survey');
        }
    }

    handleCloseDeleteDialog() {
        this.setState({openDeleteDialog: false, curRowIdToDelete: null});
    };

    turnToNumber(text) {
        let numberFromText = 0;
        if (text && typeof text === 'string') {
            let textWithoutPercent = text.slice(0, -1);
            numberFromText = Number(textWithoutPercent);
            if (typeof numberFromText !== 'number') {
                numberFromText = 0;
            }
        }
        return numberFromText;
    };

    handleSortOrderChange(key, order) {
        let tableData = this.state.tableData;
        if (order === 'asc') {
            tableData.sort((a, b) => {
                let textA = key === "Complete" ? this.turnToNumber(a[key]) : a[key].toUpperCase();
                let textB = key === "Complete" ? this.turnToNumber(b[key]) : b[key].toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else {
            tableData.sort((a, b) => {
                let textA = key === "Complete" ? this.turnToNumber(a[key]) : a[key].toUpperCase();
                let textB = key === "Complete" ? this.turnToNumber(b[key]) : b[key].toUpperCase();
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        }

        this.setState({
            sortedTableData: tableData.slice((this.state.page - 1) * this.state.rowCountInPage,
                (this.state.page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    createSurveyAnswerPDF(survey) {
        if (survey && survey.said) {
            if (survey.pages && Array.isArray(survey.pages) && survey.pages.length > 0) {
                this.startLoader();
                const orgId = localStorage.getItem(ORGANIZATION_ID);

                let dataObj = {
                    said: survey.said,
                    orgId: orgId
                };

                $.ajax({
                    type: 'POST',
                    url: '/api/exportSurveyReport',
                    data: JSON.stringify(dataObj),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: true
                }).done((res) => {
                    this.stopLoader();
                    if (res.ok && res.data) {
                        // Meaning that the PDF was created successfully. (res.data is the fileName)
                        window.location = '/surveyReport/' + res.data;
                    } else {
                        app.addAlert('error', 'Error: failed to export survey PDF');
                    }
                }).fail(() => {
                    this.stopLoader();
                    app.addAlert('error', 'Error: failed to export survey PDF');
                });
            } else {
                app.addAlert('error', t[this.state.lang]['Survey_getSurveysWithAnswers_Page_Error']);
            }
        } else {
            app.addAlert('error', t[this.state.lang]['Survey_getSurveysWithAnswers_Error']);
        }
    }

    runSurvey(said) {
        browserHistory.push({
            pathname: '/survey',
            search: '?said=' + said
        });
    }

    downloadSurveyExcel(excelFormat, said) {
        const orgId = localStorage.getItem(ORGANIZATION_ID);

        $.ajax({
            type: 'GET',
            url: '/api/downloadSurveyWithAnswers',
            data: {said: said, excelFormat: excelFormat, orgId:orgId},
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data && Object.keys(data).length === 0 && data.constructor === Object) {
                app.addAlert('error', t[this.state.lang]['Survey_getSurveysWithAnswers_Error']);
            } else {
                window.location = '/surveyExcel/' + data.fileName;
            }
            this.setState({openDownloadExcelOptionsModal: false});
        }).fail((e) => {
            console.log(e);
            app.addAlert('error', t[this.state.lang]['Survey_getSurveysWithAnswers_Error']);
        });
    }

    showExcelDownloadModal(said) {
        this.setState({openDownloadExcelOptionsModal: true, said: said});
    }

    createNewSurvey() {
        browserHistory.push('/survey-new');
    }

    uploadExistingSurveyModal() {
        if (!this.state.openUploadExistingSurveyModal) {
            this.setState({openUploadExistingSurveyModal: true});
        }
    }

    closeUploadExistingSurveyModal() {
        if (this.state.openUploadExistingSurveyModal) {
            this.setState({openUploadExistingSurveyModal: false});
        }
    }

    openUploadedSurvey(newSaid) {
        if (newSaid) {
            browserHistory.push({
                pathname: '/survey',
                search: '?said=' + newSaid
            });
        }
    }

    getSurveysWithAnswers() {
        $.ajax({
            type: 'POST',
            url: '/api/getAllSurveysWithAnswers',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data) {
                this.setResults(data);
            }
        }).fail((e) => {
            console.log(e);
            app.addAlert('error', t[this.state.lang]['Survey_getSurveysWithAnswers_Error']);
        });
    }

    deleteSurveyAnswers(srvSaid) {
        let tableData = this.state.tableData;
        deleteIDFromSurveys(tableData, srvSaid);

        if (!this.state.fuzzyString) {
            let page = this.state.page;
            if (page > 1 && page > tableData.length / this.state.rowCountInPage) {
                page--;
            }

            this.setState({
                tableData: tableData,
                totalRowCount: tableData.length,
                page: page,
                sortedTableData: tableData.slice((page - 1) * this.state.rowCountInPage,
                    (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
            });
        } else {
            this.setState({
                tableData: tableData
            }, this.handleFilterValueChange(this.state.fuzzyString));
        }
        this.deleteSurveysAnswers(srvSaid);
    }

    deleteSurveysAnswers(srvSaid) {
        $.ajax({
            type: 'POST',
            url: '/api/deleteSurveyAnswers',
            data: JSON.stringify({said: srvSaid}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(() => {
            console.log('deleted');
        }).fail(() => {
            app.addAlert('error', t[this.state.lang]['Survey_deleteSurveysAnswers_Error']);
        });
    }

    openSendSurveyModal(ignore, data) {
        this.setState({
            openSendSurveyModal: true,
            dataForSend: {
                surveyName: data.name,
                said: data.said,
                sender: data.respondent,
                companyName: data.selectedCompanyName,
                companyId: data.selectedCompanyId,
            }
        });
    }

    closeSendSurveyModal() {
        this.setState({
            openSendSurveyModal: false
        });
    }

    openShareLinkModal(ignore, data) {
        this.setState({
            openShareLinkModal: true,
            dataForSend: {
                surveyName: data.name,
                said: data.said,
                sender: data.respondent,
                companyName: data.selectedCompanyName,
                companyId: data.selectedCompanyId,
            },
            surveyLink: ''
        });
    }

    closeShareLinkModal() {
        this.setState({
            openShareLinkModal: false
        });
    }

    closeDownloadExcelOptionsModal() {
        this.setState({
            openDownloadExcelOptionsModal: false
        });
    }

    sendSurveyAnswers(data) {
        $.ajax({
            type: 'POST',
            url: '/api/sendSurveyAnswersEmail',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(() => {
            this.setState({openSendSurveyModal: false});
            app.addAlert('success', 'Email sent!');
        }).fail(() => {
            app.addAlert('error', 'Error: failed to send email');
        });
    }

    createSurveyLink(data) {
        $.ajax({
            type: 'POST',
            url: '/api/createSurveyLink',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data.ok && data.surveyLink) {
                this.setState(
                    {
                        surveyLink: data.surveyLink
                    }
                );
            } else {
                app.addAlert('error', 'Error: failed to create link.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to create link.');
        });
    }

    handleFilterValueChange(value) {
        // Get labels to search for when fuzzy searching.
        const keys = this.state.tableColumns.map((el) => {
            return el['key'];
        });

        // Fuzzy search in tableData objects in every key.
        const searcher = new FuzzySearch(this.state.tableData, keys, {
            caseSensitive: false
        });

        const result = searcher.search(value);

        this.setState({
            sortedTableData: result,
            fuzzyString: value
        });
    }

    handleNextPageClick() {
        let page = this.state.page + 1;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
                (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handlePreviousPageClick() {
        let page = this.state.page - 1;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice(page * this.state.rowCountInPage - this.state.rowCountInPage,
                page * this.state.rowCountInPage)
        });
    }

    handleRowSizeChange(ignore, rowCount) {
        let page = this.state.page;
        if (page > 1 && page > this.state.tableData.length / rowCount) {
            page--;
        }

        this.setState({
            rowCountInPage: rowCount,
            sortedTableData: this.state.tableData.slice((page - 1) * rowCount, (page - 1) * rowCount + rowCount),
            rowSize: rowCount,
            page: page
        });
    }

    componentDidMount() {
        this.state.user = app.getAuthUser();

        if (!this.state.user) {
            app.routeAuthUser();
        }

        this.getSurveysWithAnswers();
    }

    componentWillMount() {
        app.setFlagVisibile(true);
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        let tableBodyStyle = {
            'overflowX': 'auto',
            'direction': this.state.dir
        };

        let tableStyle = {
            'width': '95%',
            'margin': 'auto'
        };

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        let hideFlag = false;
        if (this.state.user && this.state.user.userRole === 'basic') {
            hideFlag = true;
        }

        let hide = classNames({
            'hide': hideFlag
        });

        let isRight = (this.state.dir === RTL);

        return (
            <div className={pageStyleClass}>
                <Loader show={this.state.showLoader} message={spinner}>
                    <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                        <div>
                            <div style={{margin: '40px 0px 40px 10px'}}>
                                <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                                <span style={{
                                    position: 'relative',
                                    top: '-6px',
                                    fontSize: 22
                                }}>{t[this.state.lang]['Surveys_Dashboard']}</span>
                            </div>
                            <Card style={{margin: 10}}>
                                <div dir={myDir}>
                                    <FlatButton
                                        style={{
                                            boxShadow: "-4px 6px 6px -4px #e0e0e0",
                                            borderRadius:5,
                                            margin: '35px 0px 0px 35px'}}
                                        secondary={true}
                                        label={t[this.state.lang]['Survey_Start_New']}
                                        onTouchTap={this.createNewSurvey.bind(this)}
                                        className={hide}
                                    />
                                    <FlatButton
                                        style={{
                                            boxShadow: "-4px 6px 6px -4px #e0e0e0",
                                            borderRadius:5,
                                            margin: '35px 0px 0px 35px'}}
                                        secondary={true}
                                        label={t[this.state.lang]['Survey_Upload_Existing']}
                                        onTouchTap={this.uploadExistingSurveyModal.bind(this)}
                                        className={hide}
                                    />
                                </div>
                                <DataTables
                                    height={'auto'}
                                    selectable={false}
                                    showRowHover={true}
                                    showHeaderToolbar={true}
                                    tableBodyStyle={tableBodyStyle}
                                    tableStyle={tableStyle}
                                    columns={this.state.tableColumns}
                                    data={this.state.sortedTableData}
                                    showCheckboxes={false}
                                    onCellClick={this.handleCellClick}
                                    onFilterValueChange={this.handleFilterValueChange}
                                    onSortOrderChange={this.handleSortOrderChange}
                                    onNextPageClick={this.handleNextPageClick}
                                    onRowSizeChange={this.handleRowSizeChange}
                                    onPreviousPageClick={this.handlePreviousPageClick}
                                    page={this.state.page}
                                    rowSize={this.state.rowSize}
                                    count={this.state.totalRowCount}
                                />
                                <DeleteDialog
                                    open={this.state.openDeleteDialog}
                                    onRequestClose={this.handleCloseDeleteDialog}
                                    onDelete={() => {
                                        this.deleteSurveyAnswers(this.state.curRowIdToDelete);
                                        this.handleCloseDeleteDialog();
                                    }}/>
                                <Dialog
                                    title={t[this.state.lang]['Survey_Edit']}
                                    style={{margin: 10}}
                                    actions={[
                                        <FlatButton
                                            label={t[this.state.lang]['Survey_Save']}
                                            primary={true}
                                            style={{
                                                color: 'white',
                                                marginBottom: '5px',
                                                marginRight: '5px',
                                                height: 40
                                            }}
                                            keyboardFocused={true}
                                            backgroundColor={'#0091ea'}
                                            hoverColor={'#12a4ff'}
                                            rippleColor={'white'}
                                            labelStyle={{fontSize: 10}}
                                            onTouchTap={this.saveSurveyEditedData.bind(this)}
                                        />,
                                        <FlatButton
                                            label={t[this.state.lang]['Survey_Cancel']}
                                            primary={true}
                                            backgroundColor={'#0091ea'}
                                            style={{ marginRight: '5px',color: 'white', marginBottom: '5px', height: 40}}
                                            keyboardFocused={true}
                                            hoverColor={'#12a4ff'}
                                            rippleColor={'white'}
                                            labelStyle={{fontSize: 10}}
                                            onTouchTap={this.handleCloseEditSurveyDialog.bind(this)}
                                        />
                                    ]}
                                    open={this.state.openEditSurveyDialog}
                                    onRequestClose={this.handleCloseEditSurveyDialog.bind(this)}
                                    actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                                    bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                                    autoScrollBodyContent={true}
                                    modal={true}
                                    repositionOnUpdate={false}
                                    contentStyle={{direction: myDir,maxWidth: '30%', borderRadius: '7px 7px 7px 7px'}}
                                    titleStyle={{
                                        fontSize: 18,
                                        background: 'rgba(0,0,0,0.7)',
                                        color: 'white', textAlign: 'center',
                                        borderRadius: '2px 2px 0px 0px',
                                        textTransform: 'uppercase',
                                    }}>
                                    <br/>
                                    <span style={isRight ? {
                                        fontSize: 16,
                                        //float: 'right'
                                    } : {fontSize: 16}}>{t[this.state.lang]['Survey_Edit_Name']}</span>
                                    <TextField
                                        id="survey_name"
                                        style={{margin: 10, fontSize: 16}}
                                        value={this.state.curSurveyObj.surveyName || ''}
                                        onChange={this.handleChangeSurveyName.bind(this)}/>
                                    <br/>
                                </Dialog>
                            </Card>
                            <SendSurveyModal
                                open={this.state.openSendSurveyModal}
                                close={this.closeSendSurveyModal.bind(this)}
                                sendSurvey={this.sendSurveyAnswers.bind(this)}
                                data={this.state.dataForSend}
                            />
                            <DownloadExcelOptions
                                open={this.state.openDownloadExcelOptionsModal}
                                close={this.closeDownloadExcelOptionsModal.bind(this)}
                                download={this.downloadSurveyExcel.bind(this)}
                                said={this.state.said}
                            />
                            <ShareLinkSurveyModal
                                open={this.state.openShareLinkModal}
                                createSurveyLink={this.createSurveyLink.bind(this)}
                                close={this.closeShareLinkModal.bind(this)}
                                surveyLink={this.state.surveyLink}
                                data={this.state.dataForSend}
                            />
                            <UploadExistingSurveyModal
                                open={this.state.openUploadExistingSurveyModal}
                                closeDialog={this.closeUploadExistingSurveyModal.bind(this)}
                                loadedLang={this.state.lang}
                                openUploadedSurvey={this.openUploadedSurvey.bind(this)}
                            />

                        </div>
                    </MuiThemeProvider>
                </Loader>
            </div>
        );
    }
}
