import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import t from '../../../../config/i18n';
import classNames from 'classnames';
import {LTR, RTL, MY_LANG, DEFAULT_LANG} from '../../../../config/consts';
import {isLTRLanguage} from '../../common/AppHelpers';
import copy from 'copy-to-clipboard';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class ShareLinkSurveyModal extends React.Component {
    constructor(props) {
        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            open: false,
            message: '',
            recipient: '',
            surveyLink: '',
            surveyName: '',
            senderEmail: '',
            senderEmailExpiry: '',
            sender: '',
            lang: myLang,
            linkHasBeenCreated: false
        };
    }

    handleChangeSenderEmail(event) {
        this.setState({
            senderEmail: event.target.value
        });
    };

    handleChangeSenderEmailExpiry(event) {
        this.setState({
            senderEmailExpiry: event.target.value
        });
    };

    handleClose() {
        this.setState({
            senderEmail: '',
            senderEmailExpiry: '',
            surveyLink: ''
        });

        this.setState({linkHasBeenCreated: false})
        this.props.close();
    };

    handleCopyToClipboard() {
        copy(this.state.surveyLink);
        app.addAlert('info', 'Copied Link to clipboard');
    };

    handleSubmit(e) {
        e.preventDefault();
        let tokenCreationDate = new Date();
        this.setState({linkHasBeenCreated: true})

        let data = {
            message: this.state.message,
            senderEmail: this.state.senderEmail,
            surveyName: this.state.surveyName,
            senderEmailExpiry: this.state.senderEmailExpiry,
            said: this.props.data.said,
            tokenCreationTime: new Date().toISOString(),
            tokenExpirationTime: new Date(tokenCreationDate.setTime(tokenCreationDate.getTime() +
                this.state.senderEmailExpiry * 86400000)).toISOString(),
            companyName: this.props.data.companyName,
            companyId: this.props.data.companyId,
            isSentWithEmail: false
        };
        this.props.createSurveyLink(data);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open,
            surveyLink: nextProps.surveyLink,
            surveyName: nextProps.data && nextProps.data.surveyName
        });
    }

    render() {
        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = (isLTRLanguage(myLang)) ? LTR : RTL;
        let isLTR = (myDir === LTR);

        let senderEmailTextField = classNames({
            'senderEmailTextFieldLTR': isLTR,
            'senderEmailTextFieldRTL': !isLTR
        });

        let buttonsContainer = classNames({
            'uploadDialogButtonsLTR': isLTR,
            'uploadDialogButtonsRTL': !isLTR
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title="Create Survey Link"
                        open={this.state.open || false}
                        actionsContainerClassName={buttonsContainer}
                        actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                        bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                        autoScrollBodyContent={true}
                        modal={true}
                        repositionOnUpdate={false}
                        contentStyle={{direction: myDir, borderRadius: '7px 7px 7px 7px'}}
                        titleStyle={{
                            fontSize: 18,
                            background: 'rgba(0,0,0,0.7)',
                            color: 'white', textAlign: 'center',
                            borderRadius: '2px 2px 0px 0px',
                            textTransform: 'uppercase',
                        }}
                    >
                        <br/>
                        <br/>
                        <span style={{padding: '0px 30px 0px 30px'}}>{t[this.state.lang]['Survey_link_share']}</span>
                        <br/>
                        <ValidatorForm ref="form"
                                       className="commentForm"
                                       onSubmit={this.handleSubmit.bind(this)}
                                       onError={(errors) => console.log(errors)}>
                            <div style={{padding: '0px 30px 0px 30px'}} className="row">
                                <TextValidator
                                    name="senderEmail"
                                    validators={['required', 'isEmail']}
                                    errorMessages={[t[this.state.lang]['Survey_Field_Required'], t[this.state.lang]['Survey_Email_Not_Valid']]}
                                    id="sender-email-field"
                                    hintText={t[this.state.lang]['Survey_Sender_Email']}
                                    floatingLabelText={t[this.state.lang]['Survey_Sender_Email']}
                                    className={senderEmailTextField}
                                    value={this.state.senderEmail}
                                    onChange={this.handleChangeSenderEmail.bind(this)}
                                />
                                <TextValidator
                                    name="senderEmailExpiry"
                                    validators={['required', 'isNumber', 'minNumber:3', 'maxNumber:60']}
                                    errorMessages={[t[this.state.lang]['Survey_Field_Required'],
                                        t[this.state.lang]['Survey_Email_Days_Not_Valid'],
                                        t[this.state.lang]['Survey_Email_Days_Not_Fit'],
                                        t[this.state.lang]['Survey_Email_Days_Not_Fit']]}
                                    id="sender-email-expiry-field"
                                    hintText={'3-60'}
                                    floatingLabelText={t[this.state.lang]['Survey_Sender_Email_Expiry']}
                                    className={senderEmailTextField}
                                    value={this.state.senderEmailExpiry}
                                    onChange={this.handleChangeSenderEmailExpiry.bind(this)}
                                />
                            </div>
                            <br/>
                            {this.state.surveyLink ?
                                <div className="row" style={{paddingBottom: '20px', padding: '20px 30px 0px 30px'}}>
                                    <TextField
                                        id="survey-link"
                                        value={this.state.surveyLink || ' '}
                                        floatingLabelText={t[this.state.lang]['Survey_Link']}
                                    />
                                    <div style={{paddingBottom: '20px', paddingTop: '26px', marginTop: '7px'}}>
                                        <FlatButton
                                            label={t[this.state.lang]['Survey_Copy_Clipboard']}
                                            primary={true}
                                            onTouchTap={this.handleCopyToClipboard.bind(this)}
                                        />
                                    </div>
                                </div> : ''
                            }
                            <br/>
                            <div style={{float: 'right'}} className="row">
                                <RaisedButton
                                    label={t[this.state.lang]['Survey_Create_Link']}
                                    disabled={this.state.linkHasBeenCreated}
                                    primary={true}
                                    style={{
                                        color: 'white',
                                        marginBottom: '5px',
                                        marginRight: '5px',
                                        height: 40
                                    }}
                                    keyboardFocused={true}
                                    backgroundColor={'#0091ea'}
                                    hoverColor={'#12a4ff'}
                                    rippleColor={'white'}
                                    labelStyle={{fontSize: 10}}
                                    type="submit"
                                />
                                <FlatButton
                                    label={t[this.state.lang]['Survey_Close']}
                                    primary={true}
                                    backgroundColor={'#0091ea'}
                                    style={{color: 'white', marginRight: '5px', marginBottom: '5px', height: 40}}
                                    keyboardFocused={true}
                                    hoverColor={'#12a4ff'}
                                    rippleColor={'white'}
                                    labelStyle={{fontSize: 10}}
                                    onTouchTap={this.handleClose.bind(this)}
                                />
                            </div>
                        </ValidatorForm>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
