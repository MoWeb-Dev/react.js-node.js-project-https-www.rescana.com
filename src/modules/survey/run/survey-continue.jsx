import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Tabs from 'material-scrollable-tabs-build/build/Tabs/Tabs.js';
import Tab from 'material-scrollable-tabs-build/build/Tabs/Tab.js';
import {Card, CardText, CardTitle} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import UploadModal from './upload-dialog.jsx';
import classNames from 'classnames';
import ActionDelete from 'material-ui/svg-icons/content/clear';
import ActionDownload from 'material-ui/svg-icons/file/file-download';
import IconButton from 'material-ui/IconButton';
import FileIcon from 'material-ui/svg-icons/editor/insert-drive-file';
import {isImage} from '../common/survey-helpers.js';
import ReactTooltip from 'react-tooltip';
import Promise from 'bluebird';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import DoneIcon from 'material-ui/svg-icons/action/done';
import ConfirmationDialog from './survey-confirmation-dialog.jsx';
import {isLTRLanguage} from '../../common/AppHelpers';
import {
    LTR,
    RTL,
    DEFAULT_LANG,
    WILDFIRE_STATUS_TYPES,
    ORGANIZATION_ID,
    ORGANIZATION_NAME
} from '../../../../config/consts';
import t from '../../../../config/i18n';
import Loader from 'react-loader-advanced';
import '../../../../public/css/survey.css';
import themeForFont from "../../app/themes/themeForFont";
import RightArrowIcon from 'material-ui/svg-icons/navigation/chevron-right';
import LeftArrowIcon from 'material-ui/svg-icons/navigation/chevron-left';
import FlatButton from 'material-ui/FlatButton';
import DynamicDialog from '../../app/dynamic-dialog.jsx';

const moment = require('moment');

let findGetParameter = (parameterName) => {
    let result = null;
    let tmp = [];
    let items = location.search.substr(1).split('&');

    for (let index = 0; index < items.length; index++) {
        tmp = items[index].split('=');
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
};

export default class SurveyContinue extends React.Component {
    constructor() {
        super();
        this.render = this.render.bind(this);
        this.state = {
            questionnaire: {},
            openConfirm: false,
            showLoader: true,
            isOpenFollowUpCategoryAddedDialog: false
        };
    }

    getCookie(cname) {
        let name = cname + '=';
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }

    componentDidMount() {
        let uid = this.getCookie('uid');
        this.setState({
            uid: uid
        });

        let said = '';

        if (this.props.location.query && this.props.location.query.said) {
            said = this.props.location.query.said;
        }

        $.ajax({
            type: 'POST',
            url: '/api/getSurveyContinueWithAnswers',
            data: JSON.stringify({uid: uid, said: said, token: findGetParameter('token')}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true
        }).done((data) => {
            this.setState({'questionnaire': data});
        }).fail(() => {
        });
    }

    updateCompanySurveyScore(companyId, score, isUnsafeCompany) {
        return new Promise((resolve) => {
            if (score >= 0 && companyId) {
                const orgId = localStorage.getItem(ORGANIZATION_ID);

                let data = {id: companyId, orgId: orgId, surveyScore: score, isUnsafeCompany: isUnsafeCompany, token: findGetParameter('token')};
                console.log(score);
                $.ajax({
                    type: 'POST',
                    url: '/api/updateCompanySurveyContinueScore',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: true
                }).done(() => {
                    resolve();
                }).fail(() => {
                    resolve();
                });
            }
        });
    }

    saveSurveyWithAnswers() {
        let questionnaire = this.state.questionnaire;

        //checking if company is unsafe
        let isUnsafeCompany = false;
        let noGoQuestionsCount = questionnaire.counts.noGoQuestionsCount;
        if(noGoQuestionsCount || noGoQuestionsCount === 0){
            let minNoGoToAnswerNum = questionnaire && questionnaire.preferences && questionnaire.preferences.minNoGoToAnswerNum ? questionnaire.preferences.minNoGoToAnswerNum : 0;
            let noGoAnsweredWrongCount = questionnaire && questionnaire.counts && questionnaire.counts.noGoAnsweredWrongCount ? questionnaire.counts.noGoAnsweredWrongCount : 0;
            isUnsafeCompany = minNoGoToAnswerNum < noGoAnsweredWrongCount;
        }

        // If no survey answer ID exists, create one.
        if (!this.state.said) {
            this.saveToDB(questionnaire)
                .then(() => {
                    this.updateCompanySurveyScore(questionnaire.selectedCompanyId, questionnaire.score, isUnsafeCompany);
                });
        } else {
            questionnaire.said = this.state.said;
            this.saveToDB(questionnaire)
                .then(() => {
                    this.updateCompanySurveyScore(questionnaire.selectedCompanyId, questionnaire.score, isUnsafeCompany);
                });
        }
    }

    saveToDB(questionnaire) {
        return new Promise((resolve) => {
            questionnaire.lastChangedDate = moment().format();

            const orgId = localStorage.getItem(ORGANIZATION_ID);
            const orgName = localStorage.getItem(ORGANIZATION_NAME);

            if(orgId && orgName && typeof orgId === 'string' && typeof orgName === 'string' && orgId !== '' && orgName !== ''){
                questionnaire.orgId = orgId;
                questionnaire.orgName = orgName;
            }

            //checking if company is unsafe
            let isUnsafeCompany = false;
            let noGoQuestionsCount = questionnaire.counts.noGoQuestionsCount;
            if(noGoQuestionsCount || noGoQuestionsCount === 0){
                let minNoGoToAnswerNum = questionnaire && questionnaire.preferences && questionnaire.preferences.minNoGoToAnswerNum ? questionnaire.preferences.minNoGoToAnswerNum : 0;
                let noGoAnsweredWrongCount = questionnaire && questionnaire.counts && questionnaire.counts.noGoAnsweredWrongCount ? questionnaire.counts.noGoAnsweredWrongCount : 0;
                isUnsafeCompany = minNoGoToAnswerNum < noGoAnsweredWrongCount;
            }

            questionnaire.isUnsafeCompany = isUnsafeCompany;

            $.ajax({
                type: 'POST',
                url: '/api/saveSurveyContinueWithAnswers',
                data: JSON.stringify({questionnaire: questionnaire, token: findGetParameter('token')}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done(() => {
                console.log('done');
                resolve();
            }).fail(() => {
                console.log('Failed to save data');
                resolve();
            });
        });
    }

    saveAnswer(pageIndex, categoryIndex, questionIndex, answer) {
        let questionnaire = this.state.questionnaire;
        questionnaire = this.triggerFollowUpCategoryIfNeeded(questionnaire, questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex], answer);
        questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].answer = answer;
        let scoreObj = this.calcScore(questionnaire.pages);
        questionnaire.score = scoreObj.score;
        questionnaire.counts = scoreObj.countObj;
        questionnaire.noGoAnsweredWrongArr = scoreObj.noGoAnsweredWrongArr || [];
        this.setState({questionnaire: questionnaire});
        this.saveSurveyWithAnswers();
    }

    triggerFollowUpCategoryIfNeeded(questionnaire, curQuestion, questionAnswer) {
        if (curQuestion && curQuestion.followUpQuestionDataArr && Array.isArray(curQuestion.followUpQuestionDataArr) && curQuestion.followUpQuestionDataArr.length > 0) {
            let arrNamesToUpdate = [];
            let categoryAddedFlag = false;
            if (questionnaire && questionnaire.pages) {
                let pages = questionnaire.pages;
                // Loop through pages.
                for (let k = 0; k < pages.length; k++) {
                    if (pages[k] && pages[k].elements) {
                        let arrIdxToUpdate = [];
                        // Loop through categories.
                        for (let i = 0; i < pages[k].elements.length; i++) {
                            if (pages[k].elements[i] && pages[k].elements[i].followUpCategoryData) {

                                let followUpCategoryData = pages[k].elements[i].followUpCategoryData;

                                curQuestion.followUpQuestionDataArr.map((question) => {
                                    if (!!(followUpCategoryData && question &&
                                        followUpCategoryData.isFollowUpCategory && question.isFollowUpQuestion &&
                                        followUpCategoryData.followUpCategoryAnswer && question.followUpCategoryAnswer &&
                                        this.getCorrectAnswerInText(followUpCategoryData.followUpCategoryAnswer) === questionAnswer &&
                                        followUpCategoryData.followUpCategoryId && question.followUpCategoryId &&
                                        followUpCategoryData.followUpCategoryId === question.followUpCategoryId)) {
                                        categoryAddedFlag = true;
                                        arrIdxToUpdate.push(i);
                                        if (pages[k].elements[i].name) {
                                            arrNamesToUpdate.push(pages[k].elements[i].name);
                                        }
                                    } else {
                                        if (pages[k].elements[i].followUpCategoryData) {
                                            pages[k].elements[i].followUpCategoryData.isShown = false;
                                        }
                                    }
                                });
                            }
                        }
                        if (categoryAddedFlag) {
                            arrIdxToUpdate.map((idxToUpdate) => {
                                pages[k].elements[idxToUpdate].followUpCategoryData.isShown = true
                            });
                            this.setState({
                                isOpenFollowUpCategoryAddedDialog: true,
                                followUpCategoryAddedName: arrNamesToUpdate
                            });
                        }
                    }
                }
            }
        }
        return questionnaire;
    }

    updateCloseFollowUpCategoryDialog(){
        this.setState({isOpenFollowUpCategoryAddedDialog: false});
    }

    createStringArr(arr) {
        let stringArr = '';
        if (arr && Array.isArray(arr) && arr.length > 0) {
            for (let i = 0; i < arr.length; i++) {
                if (i === arr.length - 1) {
                    stringArr += ' "' + arr[i] + '" ';
                } else if (i === 0 && arr.length > 1) {
                    stringArr += '"' + arr[i] + '"';
                    stringArr += ', ';
                } else {
                    stringArr += '"' + arr[i] + '", ';
                    stringArr += ', ';
                }
            }
            stringArr += '\n\n';
        }

        return stringArr;
    }

    saveComments(pageIndex, categoryIndex, questionIndex, comment) {
        let questionnaire = this.state.questionnaire;
        // This changes the state but no re-render is required so setState isn't called.
        questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].comment = comment;
        this.saveSurveyWithAnswers();
    }

    saveImages(pageIndex, categoryIndex, questionIndex, files) {
        let data = new FormData();
        for (let i = 0; i < files.length; i++) {
            data.append('surveyEvidence', files[i], files[i].name);
        }

        $.ajax({
            url: '/api/surveyImageContinueUpload?token=' + findGetParameter('token'),
            dataType: 'json',
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: (res) => {
                let questionnaire = this.state.questionnaire;
                let files = questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].files;
                if (!files) {
                    files = [];
                }
                files = files.concat(res.result);
                questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].files = files;
                this.setState({questionnaire: questionnaire}, this.saveSurveyWithAnswers);
            }
        });
    }

    deleteImage(pageIndex, categoryIndex, questionIndex, file) {
        let questionnaire = this.state.questionnaire;
        let filesArr = questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].files;
        let newFilesArr = filesArr;

        for (let i = 0; i < filesArr.length; i++) {
            if (filesArr[i].path === file.path) {
                newFilesArr.splice(i, 1);
            }
        }

        questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].files = newFilesArr;
        this.setState({'questionnaire': questionnaire}, this.saveSurveyWithAnswers);
    }

    closeConfirm() {
        this.setState({openConfirm: false});
    }

    finishSurvey() {
        let token = findGetParameter('token');
        $.ajax({
            type: 'POST',
            url: '/api/sendDoneNotification',
            data: JSON.stringify({token: token}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(() => {
            this.setState({openConfirm: true});
            this.calculateCVSSScore();
        }).fail(() => {
            console.log('failed to send email');
        });
    }

    calculateCVSSScore() {
        let data = {};
        let questionnaire = this.state.questionnaire;
        if (questionnaire.selectedCompanyId) {

            data.companyID = questionnaire.selectedCompanyId;
            data.saveScoresOnDB = true;
            data.token = findGetParameter('token');
            $.ajax({
                type: 'POST',
                url: '/api/getMaxCVSScoreFromServer',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done((res) => {
                if (res && res.ok) {
                    console.log('Score Updated!');
                } else {
                    console.error('Error: Failed to calculate score.');
                }
            }).fail(() => {
                console.error('Error: Failed to calculate score.');
            });
        } else {
            console.error('Error: Failed to calculate score.');
        }
    }

    calcScore(pages) {
        let ComplyScore = 0;
        let weightSum = 0;
        let noGoAnsweredWrongArr = [];
        let countObj = {
            NACount: 0,
            complyCount: 0,
            nonComplyCount: 0,
            questionsCount: 0,
            noGoQuestionsCount: 0,
            noGoAnsweredWrongCount: 0,
            noGoAnsweredRightCount: 0
        };
        let NAscore = 0;
        // Loop through pages.
        for (let k = 0; k < pages.length; k++) {
            // Loop through categories.
            for (let i = 0; i < pages[k].elements.length; i++) {
                if(pages[k].elements && pages[k].elements[i] &&
                    (!pages[k].elements[i].followUpCategoryData ||
                        (pages[k].elements[i].followUpCategoryData && pages[k].elements[i].followUpCategoryData.isShown))){
                // Loop through questions in each category.
                for (let j = 0; j < pages[k].elements[i].elements.length; j++) {
                    let question = pages[k].elements[i].elements[j];
                    weightSum += question.weight;
                    countObj.questionsCount++;

                    if (question.answer === 'Comply') {
                        countObj.complyCount++;
                        ComplyScore += question.weight;
                    } else if (question.answer === 'Other') {
                        countObj.NACount++;
                        NAscore += question.weight;
                    } else if (!question.answer) {
                        countObj.NACount++;
                        NAscore += question.weight;
                    } else {
                        countObj.nonComplyCount++;
                    }

                    if (question.noGoQuestionData
                        && question.noGoQuestionData.isNoGoQuestion
                        && question.noGoQuestionData.noGoCorrectAnswer) {
                        let correctAnswer = this.getCorrectAnswerInText(question.noGoQuestionData.noGoCorrectAnswer);
                        if(correctAnswer && typeof correctAnswer === 'string' && correctAnswer !== ''){
                            countObj.noGoQuestionsCount++;
                            if (!question.answer || (question.answer !== correctAnswer)) {
                                countObj.noGoAnsweredWrongCount++;
                                let questionObj = {
                                    questionText: question.name,
                                    vendorAnswer: question.answer || 'Not Answered',
                                    correctAnswer: correctAnswer,
                                    vendorComment: question.comment || ''
                                };
                                noGoAnsweredWrongArr.push(questionObj);
                            } else {
                                countObj.noGoAnsweredRightCount++;
                            }
                        }
                    }
                }
            }
            }
        }
        return {score: 1 - ((ComplyScore + NAscore) / weightSum), countObj: countObj, noGoAnsweredWrongArr: noGoAnsweredWrongArr};
    }

    getCorrectAnswerInText(answer) {
        let answerInText = '';
        if (answer) {
            if (answer === 1) {
                answerInText = 'Comply';
            } else if (answer === 2) {
                answerInText = 'Non Comply';
            } else if (answer === 3) {
                answerInText = 'Other';
            }
        }
        return answerInText;
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    render() {
        let myLang = this.state.questionnaire.lang || DEFAULT_LANG;
        let isLTR = isLTRLanguage(myLang);
        let myDir = (isLTR) ? LTR : RTL;

        let followUpCategoryAddedNamesArr = this.state.followUpCategoryAddedName || [];


        let categoriesAddedText = '';
        if (followUpCategoryAddedNamesArr && Array.isArray(followUpCategoryAddedNamesArr) && followUpCategoryAddedNamesArr.length > 0) {
            let categoriesNamesStr = this.createStringArr(followUpCategoryAddedNamesArr, myDir);
            if (followUpCategoryAddedNamesArr.length === 1) {
                categoriesAddedText = t[myLang]['Follow_Up_Categories_Added_One'] + categoriesNamesStr + '.';
            }
            else {
                categoriesAddedText = t[myLang]['Follow_Up_Categories_Added_MoreThenOne'] + categoriesNamesStr + '.';
            }
        }


        let floatingDoneBtn = classNames({
            'floatingDoneBtnLTR': isLTR,
            'floatingDoneBtnRTL': !isLTR
        });

        let pageStyleClass = classNames({
            'surveyContinueContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForSurveys': true
        });

        const spinner = <div className={spinnerClass}/>;

        let isRTLObj = {};

        if (!isLTR) {
            isRTLObj = {
                isRtl: true
            };
        }

        let logoFile ={};
        let questionnaire = this.state.questionnaire;
        if(questionnaire && questionnaire.logo){
            logoFile = questionnaire.logo;
        }
        let img;
        let path = '';

        if(logoFile && logoFile.name ||  logoFile.path){
            path = logoFile.preview || '/orgLogo' + logoFile.path;
            if (isImage(logoFile)) {
                img = <img alt='' style={{
                    height: '100px',
                    width: 'initial',
                    maxWidth: '100%',
                    marginTop: '5px',
                    marginRight: '10px',
                    borderRadius: 10
                }} src={path}/>;
            } else {
                img = <FileIcon style={{
                    height: '100px',
                    width: 'initial',
                    maxWidth: '100%',
                    marginTop: '5px',
                    marginRight: '10px',
                    borderRadius: 10
                }}/>;
            }
        }

        return (
            <div className={pageStyleClass} dir={myDir}>
                <Loader show={this.state.showLoader} message={spinner}>
                    <MuiThemeProvider muiTheme={getMuiTheme(isRTLObj)}>
                        <div>
                            <Questionnaire data={this.state.questionnaire}
                                           saveAnswer={this.saveAnswer.bind(this)}
                                           saveComments={this.saveComments.bind(this)}
                                           saveImages={this.saveImages.bind(this)}
                                           deleteImage={this.deleteImage.bind(this)}
                                           lang={myLang}
                                           stopLoader={this.stopLoader.bind(this)}
                                           surveyLogo={img}
                            />
                            {(this.state.showLoader) ? null : (
                                <div className={floatingDoneBtn}>
                                    <FloatingActionButton
                                        secondary={true}
                                        style={{margin: 25}}
                                        data-tip={t[myLang]['Survey_Editor_Done_tooltip']}
                                        onTouchTap={this.finishSurvey.bind(this)}
                                    >
                                        <DoneIcon/>
                                    </FloatingActionButton>
                                    <ReactTooltip place="top"/>
                                </div>
                            )}
                            <DynamicDialog
                                open={this.state.isOpenFollowUpCategoryAddedDialog}
                                title={t[myLang]['Follow_Up_Categories_Added_Title']}
                                bodyText={categoriesAddedText}
                                btnOkLable={t[myLang]['Survey_Continue']}
                                cancelBtnEnabled={false}
                                dir={myDir}
                                handlebBtnOkClick={this.updateCloseFollowUpCategoryDialog.bind(this)}/>
                            <ConfirmationDialog closeConfirm={this.closeConfirm.bind(this)}
                                                open={this.state.openConfirm}/>
                        </div>
                    </MuiThemeProvider>
                </Loader>
            </div>
        );
    }
}

class Questionnaire extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 0
        };

        this.handleNextTab = this.handleNextTab.bind(this);
        this.handlePreviousTab = this.handlePreviousTab.bind(this);
        this.handleActive = this.handleActive.bind(this);
    }

    componentDidUpdate() {
        if (this.props && this.props.data && this.props.data.sid) {
            this.props.stopLoader();
        }
    }

    saveAnswer(pageIndex, categoryIndex, questionIndex, answer) {
        this.props.saveAnswer(pageIndex, categoryIndex, questionIndex, answer);
    }

    saveComments(pageIndex, categoryIndex, questionIndex, comment) {
        this.props.saveComments(pageIndex, categoryIndex, questionIndex, comment);
    }

    saveImages(pageIndex, categoryIndex, questionIndex, files) {
        this.props.saveImages(pageIndex, categoryIndex, questionIndex, files);
    }

    deleteImage(pageIndex, categoryIndex, questionIndex, file) {
        this.props.deleteImage(pageIndex, categoryIndex, questionIndex, file);
    }

    handleActive(value) {
        this.setState({value: value});
    }

    handleNextTab() {
        let value = this.state.value;
        if (value !== (this.props.data.pages.length - 1)) {
            value = value + 1;
            this.setState({value: value});
        }
        this.forceUpdate();
    }

    handlePreviousTab() {
        let value = this.state.value;
        if (value !== 0) {
            value = value - 1;
            this.setState({value: value});
        }
        this.forceUpdate();
    }

    handleTabChange(value) {
        this.setState({
            value: value
        }, () => {
            if (this.props.activeTab) {
                this.props.resetActiveTab();
            }
        });
    };

    render() {
        if (this.props.data && this.props.data.pages) {

            let tabStyle = {};

            let curQuestionnairePage = {};
            let value = this.state.value || 0;
            if(this.props.data.pages[value]){
                curQuestionnairePage = this.props.data.pages[value];
            }

            let tabs = this.props.data.pages.map((questionnairePage, i) => {
                return (
                    <Tab isMultiLine={true} onActive={this.handleActive.bind(this, i)} label={questionnairePage.name}
                         value={i} key={i}>
                    </Tab>
                );
            });

            return (
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>

                    <div>
                        <span style={{textAlign: 'left'}}>{this.props.surveyLogo}</span>
                        <span style={{
                            margin: '35px',
                            marginBottom: '55px',
                            fontSize: 26,
                            color: 'black'}}>{this.props.data.name}</span>
                        <div>
                            <br/>
                            <br/>
                            <Tabs
                                value={this.props.activeTab || this.state.value}
                                onChange={value => this.setState({value: value})}
                                tabItemContainerStyle={tabStyle}
                                tabType={'scrollable-buttons'}>
                                {tabs}
                            </Tabs>
                            <QuestionnaireTab page={curQuestionnairePage}
                                              saveAnswer={this.saveAnswer.bind(this, value)}
                                              saveComments={this.saveComments.bind(this, value)}
                                              saveImages={this.saveImages.bind(this, value)}
                                              deleteImage={this.deleteImage.bind(this, value)}
                                              lang={this.props.lang}
                            />
                            <div style={{marginTop: 30}}>
                            <FlatButton style={{boxShadow: "2px 2px 2px 2px #e0e0e0", radius: '2px', margin: 2.5}}
                                        primary={true}
                                        disabled={this.state.value === 0}
                                        icon={<LeftArrowIcon/>}
                                        label={'Back'}
                                        labelStyle={{textTransform: 'none'}}
                                        onClick={this.handlePreviousTab.bind(this)}/>
                            <FlatButton style={{boxShadow: "2px 2px 2px 2px #e0e0e0",radius: '2px', margin: 2.5}}
                                        label={'Next'}
                                        labelPosition={"before"}
                                        icon={<RightArrowIcon/>}
                                        labelStyle={{textTransform: 'none'}}
                                        primary={true}
                                        disabled={this.state.value === this.props.data.pages.length - 1}
                                        onClick={this.handleNextTab.bind(this)}/>
                            </div>
                        </div>
                    </div>
                </MuiThemeProvider>
            );
        } else {
            return (
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <Tabs onChange={this.handleTabChange}>
                    </Tabs>
                </MuiThemeProvider>
            );
        }
    }
}

class QuestionnaireTab extends React.Component {
    constructor(props) {
        super(props);

        this.handleTabChange = this.handleTabChange.bind(this);
    }

    handleTabChange(value) {
        this.setState({
            value: value,
            questionList: []
        });
    };

    saveAnswer(categoryIndex, qustionIndex, answer) {
        this.props.saveAnswer(categoryIndex, qustionIndex, answer);
    }

    saveComments(categoryIndex, qustionIndex, comment) {
        this.props.saveComments(categoryIndex, qustionIndex, comment);
    }

    saveImages(categoryIndex, qustionIndex, files) {
        this.props.saveImages(categoryIndex, qustionIndex, files);
    }

    deleteImage(categoryIndex, qustionIndex, file) {
        this.props.deleteImage(categoryIndex, qustionIndex, file);
    }

    render() {
        let subCategories = this.props.page.elements.map((subCategory, i) => {
            let isFollowUpCategory = subCategory && subCategory.followUpCategoryData &&
                subCategory.followUpCategoryData.isFollowUpCategory && !!(!subCategory.followUpCategoryData.isShown);

            if (isFollowUpCategory) {
                return '';
            } else {
                return (
                    <QuestionsSubCategory
                        questions={subCategory.elements}
                        name={subCategory.name}
                        key={i}
                        saveAnswer={this.saveAnswer.bind(this, i)}
                        saveComments={this.saveComments.bind(this, i)}
                        saveImages={this.saveImages.bind(this, i)}
                        deleteImage={this.deleteImage.bind(this, i)}
                        lang={this.props.lang}
                    />
                );
            }

        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <form onSubmit={this.onSubmit}>
                        {subCategories}
                    </form>
                </div>
            </MuiThemeProvider>
        );
    }
}

class QuestionsSubCategory extends React.Component {
    constructor(props) {
        super(props);
    }

    saveAnswer(questionIndex, answer) {
        this.props.saveAnswer(questionIndex, answer);
    }

    saveComments(questionIndex, comment) {
        this.props.saveComments(questionIndex, comment);
    }

    saveImages(questionIndex, files) {
        this.props.saveImages(questionIndex, files);
    }

    deleteImage(questionIndex, file) {
        this.props.deleteImage(questionIndex, file);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            questions: nextProps.questions
        });
    }

    render() {
        let questions = [];
        if (this.props.questions && this.props.questions.length > 0) {
            questions = this.props.questions.map((question, i) => {
                return <QuestionListItem key={i + 1}
                                         index={i + 1}
                                         question={question}
                                         saveAnswer={this.saveAnswer.bind(this, i)}
                                         saveComments={this.saveComments.bind(this, i)}
                                         saveImages={this.saveImages.bind(this, i)}
                                         deleteImage={this.deleteImage.bind(this, i)}
                                         lang={this.props.lang}
                />;
            });
        }

        let style = {
            cardStyle: {
                'marginTop': '10px'
            },
            titleStyle: {
                'float': 'left'
            },
            iconStyle: {
                'float': 'left',
                'marginTop': '10px'
            }

        };

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <Card style={style.cardStyle}>
                    <div>
                        <CardTitle style={style.headerStyle} title={this.props.name}/>
                    </div>
                    <CardText>
                        {questions}
                    </CardText>
                </Card>
            </MuiThemeProvider>
        );
    }
}

class QuestionListItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '' || this.props.question.name,
            question: {} || this.props.question,
            commentValue: '' || this.props.question.comment,
            openUploadModal: false,
            files: this.props.question.files
        };
    }

    onChange(index, e, answer) {
        e.preventDefault();

        if (answer === 'Other') {
            this.refs['comment_' + index].focus();
        }

        this.props.saveAnswer(answer);
    }

    handleBlur(event) {
        event.preventDefault();
        this.props.saveComments(event.target.value);
    }

    handleCommentsChange(event) {
        this.setState({
            commentValue: event.target.value
        });
    }

    saveImages(files) {
        this.setState({files: files, openUploadModal: false});
        this.props.saveImages(files);
    }

    handleOpenUpload() {
        this.setState({
            openUploadModal: true
        });
    }

    deleteImage(fileName) {
        this.props.deleteImage(fileName);
    }

    closeDialog() {
        this.setState({openUploadModal: false});
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps) {
            if (nextProps.question && nextProps.question.files) {
                this.setState({
                    files: nextProps.question.files,
                });
            } else {
                this.setState({
                    files: []
                });
            }
            if (nextProps.question.comment && nextProps.question.comment) {
                this.setState({
                    commentValue: nextProps.question.comment
                });
            } else {
                this.setState({
                    commentValue: ''
                });
            }
        }
    }

    render() {
        let index = '';
        let files = this.state.files;
        let previews;
        let token = findGetParameter('token');

        let myLang = this.props.lang;

        let isLTR = isLTRLanguage(myLang);

        let middleBigPic = classNames({
            'middleBigPicLTR': isLTR,
            'middleBigPicRTL': !isLTR
        });

        let imageColClass = classNames({
            'col-md-2': true,
            'imageContainer': true,
            'fileIconImg': true
        });

        if (this.props.index !== 0) {
            index = this.props.index + '. ';
        }

        if (!files) {
            files = [];
        }

        previews = files.map((file, i) => {
            // Display the file only if there is no property called scanStatus or if there is one and its value is SAFE.
            let shouldDisplayFile = !!(!file.hasOwnProperty('scanStatus') || (file.scanStatus && file.scanStatus === WILDFIRE_STATUS_TYPES.SAFE));
            let img;
            let fileName = file.name || file.path;
            let path = file.preview || '/evdnc' + file.path + '?token=' + token;

            if (isImage(file)) {
                img = <img className="smallPreviewImg" src={path} data-tip={fileName.replace('/', '')}/>;
            } else {
                img = <FileIcon className="smallPreviewImg" data-tip={fileName.replace('/', '')}/>;
            }

            return (shouldDisplayFile) ? <div className={imageColClass} key={i}>
                <div>
                    {img}
                </div>
                <div className={middleBigPic}>
                    <IconButton touch={true} tooltip={t[myLang]['Survey_Delete']} tooltipPosition="bottom-center">
                        <ActionDelete
                            className="removeBtn"
                            onTouchTap={this.deleteImage.bind(this, file)}
                        />
                    </IconButton>
                    <IconButton touch={true} tooltip={t[myLang]['Survey_Evidence_Download']}
                                tooltipPosition="bottom-center">
                        <a href={path} download="Evidence">
                            <ActionDownload
                                className="downloadBtn"
                            />
                        </a>
                    </IconButton>
                </div>
                <ReactTooltip place="bottom"/>
            </div> : null;
        });

        let questionColClass = classNames({
            'col-md-4': true
        });
        let style = {
            comments: {
                'marginBottom': '35px'
            },
            radioBtn: {
                'marginBottom': '5px'
            },
            radioBtnGrp: {
                'marginTop': '5px'
            },
            question: {
                'marginTop': '15px'
            },
            addImageBtn: {
                'marginTop': '15px'
            }
        };

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div style={style.question}>
                    <div className="row">
                        <div className={questionColClass}>
                            {index}
                            {this.props.question.name}
                            <br/>
                            <RadioButtonGroup style={style.radioBtnGrp}
                                              onChange={this.onChange.bind(this, this.props.index)}
                                              name="questionsRadioGroup"
                                              valueSelected={this.props.question.answer}>
                                <RadioButton
                                    value="Non Comply"
                                    label={t[myLang]['Template_Quest_Choice1']}
                                    style={style.radioBtn}
                                />
                                <RadioButton
                                    value="Comply"
                                    label={t[myLang]['Template_Quest_Choice2']}
                                    style={style.radioBtn}
                                />
                                <RadioButton
                                    value="Other"
                                    label={t[myLang]['Template_Quest_Choice3']}
                                />
                            </RadioButtonGroup>
                            <RaisedButton style={style.addImageBtn} label={t[myLang]['Survey_Attach_Evidence']}
                                          onTouchTap={this.handleOpenUpload.bind(this)}/>
                            <br/>
                            <TextField style={style.comments}
                                       floatingLabelText={t[myLang]['Template_Quest_Comments']}
                                       multiLine={true}
                                       ref={'comment_' + this.props.index}
                                       value={this.state.commentValue}
                                       onBlur={this.handleBlur.bind(this)}
                                       onChange={this.handleCommentsChange.bind(this)}
                                       rows={3}
                            />
                        </div>
                        {previews}
                    </div>
                    <UploadModal
                        open={this.state.openUploadModal}
                        saveImages={this.saveImages.bind(this)}
                        deleteImage={this.deleteImage.bind(this)}
                        files={files}
                        closeDialog={this.closeDialog.bind(this)}
                    />
                    <Divider/>
                </div>
            </MuiThemeProvider>
        );
    }
}
