import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Tabs from 'material-scrollable-tabs-build/build/Tabs/Tabs.js';
import Tab from 'material-scrollable-tabs-build/build/Tabs/Tab.js';
import {Card, CardText, CardTitle} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Divider from 'material-ui/Divider';
import {browserHistory} from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import UploadModal from './upload-dialog.jsx';
import classNames from 'classnames';
import ActionDelete from 'material-ui/svg-icons/content/clear';
import ActionDownload from 'material-ui/svg-icons/file/file-download';
import IconButton from 'material-ui/IconButton';
import FileIcon from 'material-ui/svg-icons/editor/insert-drive-file';
import {isImage} from '../common/survey-helpers.js';
import ReactTooltip from 'react-tooltip';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import DoneIcon from 'material-ui/svg-icons/action/done';
import Promise from 'bluebird';
import {LTR, RTL, MY_LANG, DEFAULT_LANG, WILDFIRE_STATUS_TYPES, ORGANIZATION_ID, ORGANIZATION_NAME} from '../../../../config/consts';
import t from '../../../../config/i18n';
import {isLTRLanguage} from '../../common/AppHelpers';
import Loader from 'react-loader-advanced';
import '../../../../public/css/survey.css';
import themeForFont from '../../app/themes/themeForFont';
import RightArrowIcon from 'material-ui/svg-icons/navigation/chevron-right';
import LeftArrowIcon from 'material-ui/svg-icons/navigation/chevron-left';
import FlatButton from 'material-ui/FlatButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Paper from 'material-ui/Paper';
import DynamicDialog from '../../app/dynamic-dialog.jsx';

const moment = require('moment');

export default class Survey extends React.Component {
    constructor() {
        super();
        this.render = this.render.bind(this);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            questionnaire: {},
            lang: myLang,
            loadedLang: undefined,
            showLoader: true,
            markedAsImportantFilter: false,
            markedAsNoGoFilter: false,
            dropDownFilterVal: 'defaultQ',
            isOpenFollowUpCategoryAddedDialog: false
        };

        this.handleActive = this.handleActive.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    componentDidMount() {
        let said = '';

        if (this.props.location.query && this.props.location.query.said) {
            said = this.props.location.query.said;
        }

        $.ajax({
            type: 'POST',
            url: '/api/getSurveyWithAnswers',
            data: JSON.stringify({said: said}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true
        }).done((data) => {
            let loadedLang = data.lang;
            this.state.loadedLang = loadedLang;
            this.setState({questionnaire: data, lang: loadedLang});
        }).fail(() => {
            app.addAlert('error', t[this.state.lang]['Survey_getSurveysWithAnswers_Error']);
        });
    }

    onChangeDropdown(event, index, value) {
        if(value && (value === 'defaultQ' || value === 'importantQ' || value === 'noGoQ')){
            this.setState({dropDownFilterVal: value});

            if (value === 'importantQ') {
                this.showFavs();
            } else if (value === 'noGoQ') {
                this.toggleShowNoGoQuestion()
            } else {
                this.setState({markedAsImportantFilter: false ,markedAsNoGoFilter: false, showNoDataLabel: false});
            }
        }
    };


    updateCompanySurveyScore(companyId, score, isUnsafeCompany) {
        return new Promise((resolve) => {
            if (score >= 0 && companyId) {
                const orgId = localStorage.getItem(ORGANIZATION_ID);

                let data = {id: companyId, orgId: orgId, surveyScore: score, isUnsafeCompany: isUnsafeCompany};
                console.log(score);
                $.ajax({
                    type: 'POST',
                    url: '/api/updateCompanySurveyScore',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: true
                }).done(() => {
                    resolve();
                }).fail(() => {
                    resolve();
                });
            }
        });
    }

    handleActive(value) {
        this.setState({value: value});
    }

    saveSurveyWithAnswers() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }
        let questionnaire = this.state.questionnaire;

        //checking if company is unsafe
        let isUnsafeCompany = false;
        let noGoQuestionsCount = questionnaire.counts.noGoQuestionsCount;
        if(noGoQuestionsCount || noGoQuestionsCount === 0){
            let minNoGoToAnswerNum = questionnaire && questionnaire.preferences && questionnaire.preferences.minNoGoToAnswerNum ? questionnaire.preferences.minNoGoToAnswerNum : 0;
            let noGoAnsweredWrongCount = questionnaire && questionnaire.counts && questionnaire.counts.noGoAnsweredWrongCount ? questionnaire.counts.noGoAnsweredWrongCount : 0;
            isUnsafeCompany = minNoGoToAnswerNum < noGoAnsweredWrongCount;
        }

        // If no survey answer ID exists, create one.
        if (!this.state.said) {
            this.saveToDB(questionnaire)
                .then(() => {
                    this.updateCompanySurveyScore(questionnaire.selectedCompanyId, questionnaire.score, isUnsafeCompany);
                });
        } else {
            questionnaire.said = this.state.said;
            this.saveToDB(questionnaire)
                .then(() => {
                    this.updateCompanySurveyScore(questionnaire.selectedCompanyId, questionnaire.score, isUnsafeCompany);
                });
        }
    }

    saveToDB(questionnaire) {
        return new Promise((resolve) => {
            questionnaire.lastChangedDate = moment().format();

            const orgId = localStorage.getItem(ORGANIZATION_ID);
            const orgName = localStorage.getItem(ORGANIZATION_NAME);

            if(orgId && orgName && typeof orgId === 'string' && typeof orgName === 'string' && orgId !== '' && orgName !== ''){
                questionnaire.orgId = orgId;
                questionnaire.orgName = orgName;
            }

            //checking if company is unsafe
            let isUnsafeCompany = false;
            let noGoQuestionsCount = questionnaire.counts.noGoQuestionsCount;
            if(noGoQuestionsCount || noGoQuestionsCount === 0){
                let minNoGoToAnswerNum = questionnaire && questionnaire.preferences && questionnaire.preferences.minNoGoToAnswerNum ? questionnaire.preferences.minNoGoToAnswerNum : 0;
                let noGoAnsweredWrongCount = questionnaire && questionnaire.counts && questionnaire.counts.noGoAnsweredWrongCount ? questionnaire.counts.noGoAnsweredWrongCount : 0;
                isUnsafeCompany = minNoGoToAnswerNum < noGoAnsweredWrongCount;
            }

            questionnaire.isUnsafeCompany = isUnsafeCompany;

            $.ajax({
                type: 'POST',
                url: '/api/saveSurveyWithAnswers',
                data: JSON.stringify({questionnaire}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done(() => {
                console.log('done');
                resolve();
            }).fail(() => {
                app.addAlert('error', t[this.state.lang]['Survey_createNewSurvey_Error']);
                resolve();
            });
        });
    }

    saveAnswer(pageIndex, categoryIndex, questionIndex, answer) {
        let questionnaire = this.state.questionnaire;
        questionnaire = this.triggerFollowUpCategoryIfNeeded(questionnaire, questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex], answer);
        questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].answer = answer;
        let scoreObj = this.calcScore(questionnaire.pages);
        questionnaire.score = scoreObj.score;
        questionnaire.counts = scoreObj.countObj;
        questionnaire.noGoAnsweredWrongArr = scoreObj.noGoAnsweredWrongArr || [];
        this.setState({questionnaire: questionnaire});
        this.saveSurveyWithAnswers();
    }

    triggerFollowUpCategoryIfNeeded(questionnaire, curQuestion, questionAnswer) {
        if (curQuestion && curQuestion.followUpQuestionDataArr && Array.isArray(curQuestion.followUpQuestionDataArr) && curQuestion.followUpQuestionDataArr.length > 0) {
            let arrNamesToUpdate = [];
            let categoryAddedFlag = false;
            if (questionnaire && questionnaire.pages) {
                let pages = questionnaire.pages;
                // Loop through pages.
                for (let k = 0; k < pages.length; k++) {
                    if (pages[k] && pages[k].elements) {
                        let arrIdxToUpdate = [];
                        // Loop through categories.
                        for (let i = 0; i < pages[k].elements.length; i++) {
                            if (pages[k].elements[i] && pages[k].elements[i].followUpCategoryData) {

                                let followUpCategoryData = pages[k].elements[i].followUpCategoryData;

                                curQuestion.followUpQuestionDataArr.map((question) => {
                                    if (!!(followUpCategoryData && question &&
                                        followUpCategoryData.isFollowUpCategory && question.isFollowUpQuestion &&
                                        followUpCategoryData.followUpCategoryAnswer && question.followUpCategoryAnswer &&
                                        this.getCorrectAnswerInText(followUpCategoryData.followUpCategoryAnswer) === questionAnswer &&
                                        followUpCategoryData.followUpCategoryId && question.followUpCategoryId &&
                                        followUpCategoryData.followUpCategoryId === question.followUpCategoryId)) {
                                        categoryAddedFlag = true;
                                        arrIdxToUpdate.push(i);
                                        if (pages[k].elements[i].name) {
                                            arrNamesToUpdate.push(pages[k].elements[i].name);
                                        }
                                    } else {
                                        if (pages[k].elements[i].followUpCategoryData) {
                                            pages[k].elements[i].followUpCategoryData.isShown = false;
                                        }
                                    }
                                });
                            }
                        }
                        if (categoryAddedFlag) {
                            arrIdxToUpdate.map((idxToUpdate) => {
                                pages[k].elements[idxToUpdate].followUpCategoryData.isShown = true
                            });
                            this.setState({
                                isOpenFollowUpCategoryAddedDialog: true,
                                followUpCategoryAddedName: arrNamesToUpdate
                            });
                        }
                    }
                }
            }
        }
        return questionnaire;
    }

    createStringArr(arr) {
        let stringArr = '';
        if (arr && Array.isArray(arr) && arr.length > 0) {
            for (let i = 0; i < arr.length; i++) {
                if (i === arr.length - 1) {
                    stringArr += ' "' + arr[i] + '" ';
                } else if (i === 0 && arr.length > 1) {
                    stringArr += '"' + arr[i] + '"';
                    stringArr += ', ';
                } else {
                    stringArr += '"' + arr[i] + '", ';
                    stringArr += ', ';
                }
            }
            stringArr += '\n\n';
        }

        return stringArr;
    }

    updateCloseFollowUpCategoryDialog(){
        this.setState({isOpenFollowUpCategoryAddedDialog: false});
    }

    saveComments(pageIndex, categoryIndex, questionIndex, comment) {
        let questionnaire = this.state.questionnaire;
        // This changes the state but no re-render is required so setState isn't called.
        questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].comment = comment;
        this.saveSurveyWithAnswers();
    }

    saveImages(pageIndex, categoryIndex, questionIndex, files) {
        let data = new FormData();
        for (let i = 0; i < files.length; i++) {
            data.append('surveyEvidence', files[i], files[i].name);
        }

        $.ajax({
            url: '/api/surveyImageUpload',
            dataType: 'json',
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: (res) => {
                let questionnaire = this.state.questionnaire;
                let files = questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].files;
                if (!files) {
                    files = [];
                }
                files = files.concat(res.result);
                questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].files = files;
                this.setState({questionnaire: questionnaire}, this.saveSurveyWithAnswers);
            }
        });
    }

    deleteImage(pageIndex, categoryIndex, questionIndex, file) {
        let questionnaire = this.state.questionnaire;
        let filesArr = questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].files;
        let newFilesArr = filesArr;

        for (let i = 0; i < filesArr.length; i++) {
            if (filesArr[i].path === file.path) {
                newFilesArr.splice(i, 1);
            }
        }

        questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex].files = newFilesArr;
        this.setState({questionnaire: questionnaire}, this.saveSurveyWithAnswers);
    }

    finishSurvey() {
        this.calculateCVSSScore();
        app.addAlert('success', 'Done! Survey Saved.');
        browserHistory.push({
            pathname: '/survey-start'
        });
    }

    calculateCVSSScore() {
        let data = {};
        let questionnaire = this.state.questionnaire;
        const orgId = localStorage.getItem(ORGANIZATION_ID);
        if (questionnaire.selectedCompanyId) {

            data.companyID = questionnaire.selectedCompanyId;
            data.saveScoresOnDB = true;
            data.orgId = orgId;
            $.ajax({
                type: 'POST',
                url: '/api/getMaxCVSScore',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((res) => {
                if (res && res.ok) {
                    app.addAlert('success', 'Score Updated!');
                } else {
                    app.addAlert('Error', 'Error: Failed to calculate score.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: Failed to calculate score.');
            });
        } else {
            app.addAlert('Error', 'Error: Failed to calculate score.');
        }
    }

    calcScore(pages) {
        let ComplyScore = 0;
        let weightSum = 0;
        let noGoAnsweredWrongArr = [];
        let countObj = {
            NACount: 0,
            complyCount: 0,
            nonComplyCount: 0,
            questionsCount: 0,
            noGoQuestionsCount: 0,
            noGoAnsweredWrongCount: 0,
            noGoAnsweredRightCount: 0
        };
        let NAscore = 0;
        // Loop through pages.
        for (let k = 0; k < pages.length; k++) {
            // Loop through categories.
            for (let i = 0; i < pages[k].elements.length; i++) {
                if (pages[k].elements && pages[k].elements[i] &&
                    (!pages[k].elements[i].followUpCategoryData ||
                        (pages[k].elements[i].followUpCategoryData && pages[k].elements[i].followUpCategoryData.isShown))) {
                // Loop through questions in each category.
                for (let j = 0; j < pages[k].elements[i].elements.length; j++) {
                    let question = pages[k].elements[i].elements[j];
                    weightSum += question.weight;
                    countObj.questionsCount++;

                    if (question.answer === 'Comply') {
                        countObj.complyCount++;
                        ComplyScore += question.weight;
                    } else if (question.answer === 'Other') {
                        countObj.NACount++;
                        NAscore += question.weight;
                    } else if (!question.answer) {
                        countObj.NACount++;
                        NAscore += question.weight;
                    } else {
                        countObj.nonComplyCount++;
                    }

                    if (question.noGoQuestionData
                        && question.noGoQuestionData.isNoGoQuestion
                        && question.noGoQuestionData.noGoCorrectAnswer) {
                        let correctAnswer = this.getCorrectAnswerInText(question.noGoQuestionData.noGoCorrectAnswer);
                        if (correctAnswer && typeof correctAnswer === 'string' && correctAnswer !== '') {
                            countObj.noGoQuestionsCount++;
                            if (!question.answer || (question.answer !== correctAnswer)) {
                                countObj.noGoAnsweredWrongCount++;
                                let questionObj = {
                                    questionText: question.name,
                                    vendorAnswer: question.answer || 'Not Answered',
                                    correctAnswer: correctAnswer,
                                    vendorComment: question.comment || ''
                                };
                                noGoAnsweredWrongArr.push(questionObj);
                            } else {
                                countObj.noGoAnsweredRightCount++;
                            }
                        }
                    }
                }
            }
            }
        }
        return {score: 1 - ((ComplyScore + NAscore) / weightSum), countObj: countObj, noGoAnsweredWrongArr: noGoAnsweredWrongArr};
    }


    getCorrectAnswerInText(answer) {
        let answerInText = '';
        if (answer) {
            if (answer === 1) {
                answerInText = 'Comply';
            } else if (answer === 2) {
                answerInText = 'Non Comply';
            } else if (answer === 3) {
                answerInText = 'Other';
            }
        }
        return answerInText;
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    findFirstPageForFilter(type) {
        let questionnaire = this.state.questionnaire;
        for (let k = 0; k < questionnaire.pages.length; k++) {
            for (let j = 0; j < questionnaire.pages[k].elements.length; j++) {
                for (let i = 0; i < questionnaire.pages[k].elements[j].elements.length; i++) {
                    if (questionnaire.pages[k].elements[j] &&
                        (!questionnaire.pages[k].elements[j].followUpCategoryData ||
                            (questionnaire.pages[k].elements[j].followUpCategoryData && questionnaire.pages[k].elements[j].followUpCategoryData.isShown))) {
                        if (type === 'fav') {
                            if (questionnaire.pages[k].elements[j].elements[i].markedAsImportant) {
                                return k;
                            }
                        } else if (type === 'noGo') {
                            if (questionnaire.pages[k].elements[j].elements[i].noGoQuestionData &&
                                questionnaire.pages[k].elements[j].elements[i].noGoQuestionData.isNoGoQuestion) {
                                return k;
                            }
                        }
                    }
                }
            }
        }
    }

    showFavs() {
        // Find a page with favs we can jump to if we are focused on a page without favs.
        let tabWithFavs = this.findFirstPageForFilter('fav');
        let activeTab = null;

        // If we have a page index and the filter by favs is on
        if (!isNaN(tabWithFavs) && !this.state.markedAsImportantFilter) {
            activeTab = 'tab' + tabWithFavs;
        }

        if(activeTab === null){
            this.setState({showNoDataLabel: true})
        }

        this.setState({markedAsImportantFilter: true, markedAsNoGoFilter: false, activeTab: activeTab});
    }

    toggleShowNoGoQuestion() {
        // Find a page with no go questions we can jump to if we are focused on a page without no go questions.
        let tabWithNoGO = this.findFirstPageForFilter('noGo');
        let activeTab = null;

        // If we have a page index and the filter by no go questions is on
        if (!isNaN(tabWithNoGO) && !this.state.markedAsNoGoFilter) {
            activeTab = 'tab' + tabWithNoGO;
        }

        if(activeTab === null){
            this.setState({showNoDataLabel: true})
        }

        this.setState({markedAsNoGoFilter: true, markedAsImportantFilter: false, activeTab: activeTab});
    }

    resetActiveTab() {
        this.setState({activeTab: null});
    }

    render() {
        let myLang = this.state.loadedLang || this.state.lang || DEFAULT_LANG;
        let followUpCategoryAddedNamesArr = this.state.followUpCategoryAddedName || [];
        let isLTR = isLTRLanguage(myLang);

        let myDir = (isLTR) ? LTR : RTL;

        let categoriesAddedText = '';
        if (followUpCategoryAddedNamesArr && Array.isArray(followUpCategoryAddedNamesArr) && followUpCategoryAddedNamesArr.length > 0) {
            let categoriesNamesStr = this.createStringArr(followUpCategoryAddedNamesArr, myDir);
            if (followUpCategoryAddedNamesArr.length === 1) {
                categoriesAddedText = t[myLang]['Follow_Up_Categories_Added_One'] + categoriesNamesStr + '.';
            }
            else {
                categoriesAddedText = t[myLang]['Follow_Up_Categories_Added_MoreThenOne'] + categoriesNamesStr + '.';
            }
        }

        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        let floatingDoneBtn = classNames({
            'floatingDoneBtnLTR': isLTR,
            'floatingDoneBtnRTL': !isLTR
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForSurveys': true
        });

        const spinner = <div className={spinnerClass}/>;

        let isRTLObj = {};

        if (myDir && myDir === RTL) {
            isRTLObj = {
                isRtl: true
            };
        }

        let user = app.getAuthUser();
        let logoFile = {};
        if (user && user.logo) {
            logoFile = user.logo;
        }
        let img;
        let path = '';

        if (logoFile && logoFile.name || logoFile.path) {
            path = logoFile.preview || '/orgLogo' + logoFile.path;
            if (isImage(logoFile)) {
                img = <img alt='' style={{
                    height: '100px',
                    width: 'initial',
                    maxWidth: '100%',
                    marginTop: '5px',
                    marginRight: '10px',
                    borderRadius: 10
                }} src={path}/>;
            } else {
                img = <FileIcon style={{
                    height: '100px',
                    width: 'initial',
                    maxWidth: '100%',
                    marginTop: '5px',
                    marginRight: '10px',
                    borderRadius: 10
                }}/>;
            }
        }

        let showNoDataLabel = this.state.showNoDataLabel || false;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(isRTLObj)}>
                <div className={pageStyleClass} dir={myDir}>
                    <Loader show={this.state.showLoader} message={spinner}>
                        <MuiThemeProvider muiTheme={getMuiTheme(isRTLObj)}>
                            <div>
                                {showNoDataLabel ? <div style={{
                                    top: '200px',
                                    fontSize: 18,
                                    position: 'absolute',
                                    left: '46.5%',
                                }}>No Data To Show.</div> : ''}
                                <Questionnaire data={this.state.questionnaire}
                                               saveAnswer={this.saveAnswer.bind(this)}
                                               saveComments={this.saveComments.bind(this)}
                                               saveImages={this.saveImages.bind(this)}
                                               deleteImage={this.deleteImage.bind(this)}
                                               onChangeDropdown={this.onChangeDropdown.bind(this)}
                                               loadedLang={this.state.loadedLang}
                                               markedAsImportantFilter={this.state.markedAsImportantFilter}
                                               markedAsNoGoFilter={this.state.markedAsNoGoFilter}
                                               dropDownFilterVal={this.state.dropDownFilterVal}
                                               checkFavFilter={this.showFavs.bind(this)}
                                               checkNoGoFilter={this.toggleShowNoGoQuestion.bind(this)}
                                               activeTab={this.state.activeTab}
                                               resetActiveTab={this.resetActiveTab.bind(this)}
                                               stopLoader={this.stopLoader.bind(this)}
                                               surveyLogo={img}
                                />
                                {(this.state.showLoader) ? null : (
                                    <div className={floatingDoneBtn}>
                                        <FloatingActionButton
                                            style={{margin: 25}}
                                            secondary={true}
                                            data-tip={t[myLang]['Survey_Editor_Done_tooltip']}
                                            onTouchTap={this.finishSurvey.bind(this)}
                                        >
                                            <DoneIcon/>
                                        </FloatingActionButton>
                                        <ReactTooltip place="top"/>
                                    </div>
                                )}
                                <DynamicDialog
                                    open={this.state.isOpenFollowUpCategoryAddedDialog}
                                    title={t[myLang]['Follow_Up_Categories_Added_Title']}
                                    bodyText={categoriesAddedText}
                                    btnOkLable={t[myLang]['Survey_Continue']}
                                    cancelBtnEnabled={false}
                                    dir={myDir}
                                    handlebBtnOkClick={this.updateCloseFollowUpCategoryDialog.bind(this)}/>
                            </div>
                        </MuiThemeProvider>
                    </Loader>
                </div>
            </MuiThemeProvider>
        );
    }
}

class Questionnaire extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 0
        };

        this.handleTabChange = this.handleTabChange.bind(this);
    }

    componentDidUpdate() {
        if (this.props && this.props.data && this.props.data.sid) {
            this.props.stopLoader();
        }
    }
    saveAnswer(pageIndex, categoryIndex, questionIndex, answer) {
        this.props.saveAnswer(pageIndex, categoryIndex, questionIndex, answer);
    }

    saveComments(pageIndex, categoryIndex, questionIndex, comment) {
        this.props.saveComments(pageIndex, categoryIndex, questionIndex, comment);
    }

    saveImages(pageIndex, categoryIndex, questionIndex, files) {
        this.props.saveImages(pageIndex, categoryIndex, questionIndex, files);
    }

    deleteImage(pageIndex, categoryIndex, questionIndex, file) {
        this.props.deleteImage(pageIndex, categoryIndex, questionIndex, file);
    }

    handleNextTab() {
        let value = this.state.value;
        if (value !== (this.props.data.pages.length - 1)) {
            value = value + 1;
            this.setState({value: value});
        }
        this.forceUpdate();
    }

    handlePreviousTab() {
        let value = this.state.value;
        if (value !== 0) {
            value = value - 1;
            this.setState({value: value});
        }
        this.forceUpdate();
    }

    handleTabChange(value) {
        this.setState({
            value: value
        }, () => {
            if (this.props.activeTab) {
                this.props.resetActiveTab();
            }
        });
    };

    handleActive(value) {
        this.setState({value: value});
    }

    shouldShowPage(questionnairePage, type) {
        for (let k = 0; k < questionnairePage.elements.length; k++) {
            for (let j = 0; j < questionnairePage.elements[k].elements.length; j++) {
                if (questionnairePage.elements[k] &&
                    (!questionnairePage.elements[k].followUpCategoryData ||
                        (questionnairePage.elements[k].followUpCategoryData && questionnairePage.elements[k].followUpCategoryData.isShown))) {
                    if (type === 'fav') {
                        if (questionnairePage.elements[k].elements[j].markedAsImportant) {
                            return true;
                        }
                    } else if (type === 'noGo') {
                        if (questionnairePage.elements[k].elements[j].noGoQuestionData &&
                            questionnairePage.elements[k].elements[j].noGoQuestionData.isNoGoQuestion) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    render() {
        let myLang = this.props.loadedLang || DEFAULT_LANG;
        let isLTR = isLTRLanguage(myLang);

        if (this.props.data && this.props.data.pages) {

            let floatingCheckBox = classNames({
                'floatingRTL': isLTR,
                'floatingLTR': !isLTR
            });
            let tabStyle = {};

            let curQuestionnairePage = {};
            let value = this.state.value || 0;
            if(this.props.data.pages[value]){
                curQuestionnairePage = this.props.data.pages[value];
            }
            let tabs = this.props.data.pages.map((questionnairePage, i) => {
                if ((this.props.markedAsImportantFilter && !this.shouldShowPage(questionnairePage, 'fav')) ||
                    (this.props.markedAsNoGoFilter && !this.shouldShowPage(questionnairePage, 'noGo'))) {
                    return '';
                } else {
                    return (
                        <Tab isMultiLine={true}
                             onActive={this.handleActive.bind(this, i)}
                             label={questionnairePage.name}
                             value={i} key={i}>
                        </Tab>
                    );
                }
            });

            return (
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <div>
                        <div className={floatingCheckBox}>
                            <span style={{position: 'relative', top: 18, marginLeft: 20, marginRight: 20}} >{isLTR? 'Filter by: ' : 'סנן לפי: '}</span>
                            <Paper>
                            <DropDownMenu
                                value={this.props.dropDownFilterVal}
                                onChange={this.props.onChangeDropdown}
                                style={{width: 220, paddingBottom: 5}}
                                labelStyle={{color: 'black'}}
                                underlineStyle={{display: 'none'}}
                                selectedMenuItemStyle={{color: 'black', background: '#c5c2c2'}}
                                anchorOrigin={{ vertical: 'bottom', horizontal: 'middle'}}
                                autoWidth={false}>
                                <MenuItem value={'defaultQ'} primaryText={isLTR? "Default" : 'ברירת מחדל'}/>
                                <MenuItem value={'importantQ'} primaryText={isLTR? "Important Questions" : "שאלות חשובות"}/>
                                <MenuItem value={'noGoQ'} primaryText={isLTR? 'Critical Questions' : "שאלות קריטיות"}/>
                            </DropDownMenu>
                            </Paper>
                        </div>
                        <div style={{marginTop: 60}}>
                            <span style={{textAlign: 'left'}}>{this.props.surveyLogo}</span>
                            <span
                                style={{
                                    color: 'black',
                                    margin: '35px',
                                    marginBottom: '55px',
                                    fontSize: 26
                                }}>{this.props.data.name}</span>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <Tabs
                            value={this.props.activeTab || this.state.value}
                            onChange={value => this.setState({value: value})}
                            tabItemContainerStyle={tabStyle}
                            tabType={'scrollable-buttons'}>
                            {tabs}
                        </Tabs>
                        {((this.props.markedAsImportantFilter && !this.shouldShowPage(curQuestionnairePage, 'fav')) ||
                            (this.props.markedAsNoGoFilter && !this.shouldShowPage(curQuestionnairePage, 'noGo'))) ?
                            <div> </div>
                            :
                            <div>
                                <QuestionnaireTab page={curQuestionnairePage}
                                                  saveAnswer={this.saveAnswer.bind(this, value)}
                                                  saveComments={this.saveComments.bind(this, value)}
                                                  saveImages={this.saveImages.bind(this, value)}
                                                  deleteImage={this.deleteImage.bind(this, value)}
                                                  markedAsImportantFilter={this.props.markedAsImportantFilter}
                                                  markedAsNoGoFilter={this.props.markedAsNoGoFilter}
                                                  loadedLang={this.props.loadedLang}
                                />
                                <div style={{margin: 10}}>
                                    <FlatButton
                                        style={{boxShadow: "2px 2px 2px 2px #e0e0e0", radius: '2px', margin: 2.5}}
                                        primary={true}
                                        disabled={this.state.value === 0}
                                        labelPosition={"after"}
                                        icon={isLTR ? <LeftArrowIcon/> : <RightArrowIcon/>}
                                        label={t[myLang]['Survey_Back']}
                                        labelStyle={{textTransform: 'none'}}
                                        onClick={this.handlePreviousTab.bind(this)}/>
                                    <FlatButton
                                        style={{boxShadow: "2px 2px 2px 2px #e0e0e0", radius: '2px', margin: 2.5}}
                                        label={t[myLang]['Survey_Next']}
                                        labelPosition={"before"}
                                        icon={isLTR ? <RightArrowIcon/> : <LeftArrowIcon/>}
                                        labelStyle={{textTransform: 'none'}}
                                        primary={true}
                                        disabled={this.state.value === this.props.data.pages.length - 1}
                                        onClick={this.handleNextTab.bind(this)}/>
                                </div>
                            </div>
                        }
                    </div>
                </MuiThemeProvider>
            );
        } else {
            return (
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <Tabs onChange={this.handleTabChange}>
                    </Tabs>
                </MuiThemeProvider>
            );
        }
    }
}

class QuestionnaireTab extends React.Component {
    constructor(props) {
        super(props);
    }

    shouldShowCat(category, type) {
        for (let j = 0; j < category.elements.length; j++) {
            if (category &&
                (!category.followUpCategoryData ||
                    (category.followUpCategoryData && category.followUpCategoryData.isShown))) {
                if (type === 'fav') {
                    if (category.elements[j].markedAsImportant) {
                        return true;
                    }
                } else if (type === 'noGo') {
                    if (category.elements[j].noGoQuestionData &&
                        category.elements[j].noGoQuestionData.isNoGoQuestion) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    saveAnswer(categoryIndex, qustionIndex, answer) {
        this.props.saveAnswer(categoryIndex, qustionIndex, answer);
    }

    saveComments(categoryIndex, qustionIndex, comment) {
        this.props.saveComments(categoryIndex, qustionIndex, comment);
    }

    saveImages(categoryIndex, qustionIndex, files) {
        this.props.saveImages(categoryIndex, qustionIndex, files);
    }

    deleteImage(categoryIndex, qustionIndex, file) {
        this.props.deleteImage(categoryIndex, qustionIndex, file);
    }

    render() {
        let subCategories = this.props.page.elements.map((subCategory, i) => {
            let isFollowUpCategory = subCategory && subCategory.followUpCategoryData &&
                subCategory.followUpCategoryData.isFollowUpCategory && !!(!subCategory.followUpCategoryData.isShown);

            if (isFollowUpCategory || (this.props.markedAsImportantFilter && !this.shouldShowCat(subCategory, 'fav')) ||
                (this.props.markedAsNoGoFilter && !this.shouldShowCat(subCategory, 'noGo'))) {
                return '';
            } else {
                return (
                    <QuestionsSubCategory
                        questions={subCategory.elements}
                        name={subCategory.name}
                        key={i}
                        saveAnswer={this.saveAnswer.bind(this, i)}
                        saveComments={this.saveComments.bind(this, i)}
                        saveImages={this.saveImages.bind(this, i)}
                        deleteImage={this.deleteImage.bind(this, i)}
                        loadedLang={this.props.loadedLang}
                        markedAsImportantFilter={this.props.markedAsImportantFilter}
                        markedAsNoGoFilter={this.props.markedAsNoGoFilter}
                    />
                );
            }
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <form onSubmit={this.onSubmit}>
                        {subCategories}
                    </form>
                </div>
            </MuiThemeProvider>
        );
    }
}

class QuestionsSubCategory extends React.Component {
    constructor(props) {
        super(props);
    }

    saveAnswer(questionIndex, answer) {
        this.props.saveAnswer(questionIndex, answer);
    }

    saveComments(questionIndex, comment) {
        this.props.saveComments(questionIndex, comment);
    }

    saveImages(questionIndex, files) {
        this.props.saveImages(questionIndex, files);
    }

    deleteImage(questionIndex, file) {
        this.props.deleteImage(questionIndex, file);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            questions: nextProps.questions
        });
    }

    render() {
        let questions = [];
        if (this.props.questions && this.props.questions.length > 0) {
            questions = this.props.questions.map((question, i) => {
                return <QuestionListItem key={i + 1}
                                         index={i + 1}
                                         question={question}
                                         saveAnswer={this.saveAnswer.bind(this, i)}
                                         saveComments={this.saveComments.bind(this, i)}
                                         saveImages={this.saveImages.bind(this, i)}
                                         deleteImage={this.deleteImage.bind(this, i)}
                                         loadedLang={this.props.loadedLang}
                                         markedAsImportantFilter={this.props.markedAsImportantFilter}
                                         markedAsNoGoFilter={this.props.markedAsNoGoFilter}
                />;
            });
        }

        let style = {
            cardStyle: {
                margin: '25px 10px 25px 10px',
                padding: '0px 20px 0px 20px'
            },
            titleStyle: {
                'float': 'left'
            },
            iconStyle: {
                'float': 'left',
                'marginTop': '10px'
            }

        };

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <Card style={style.cardStyle}>
                    <div>
                        <CardTitle style={style.headerStyle} title={this.props.name}/>
                    </div>
                    <CardText>
                        {questions}
                    </CardText>
                </Card>
            </MuiThemeProvider>
        );
    }
}

class QuestionListItem extends React.Component {
    constructor(props) {
        super(props);

        let myLang = this.props.loadedLang || localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            value: '' || this.props.question.name,
            question: {} || this.props.question,
            commentValue: '' || this.props.question.comment,
            openUploadModal: false,
            files: this.props.question.files,
            lang: myLang
        };
    }

    onChange(index, e, answer) {
        e.preventDefault();
        if (answer === 'Other') {
            this.refs['comment_' + index].focus();
        }

        this.props.saveAnswer(answer);
    }

    handleBlur(event) {
        event.preventDefault();
        this.props.saveComments(event.target.value);
    }

    handleCommentsChange(event) {
        this.setState({
            commentValue: event.target.value
        });
    }

    saveImages(files) {
        this.setState({files: files, openUploadModal: false});
        this.props.saveImages(files);
    }

    handleOpenUpload() {
        this.setState({
            openUploadModal: true
        });
    }

    deleteImage(fileName) {
        this.props.deleteImage(fileName);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            if (nextProps.question && nextProps.question.files) {
                this.setState({
                    files: nextProps.question.files,
                });
            } else{
                this.setState({
                    files: []
                });
            }
            if (nextProps.question.comment && nextProps.question.comment) {
                this.setState({
                    commentValue: nextProps.question.comment
                });
            } else {
                this.setState({
                    commentValue: ''
                });
            }
        }
    }

    closeDialog() {
        this.setState({openUploadModal: false});
    }

    render() {
        let index = '';
        let files = this.state.files;
        let previews;

        let myLang = this.state.lang;

        let isLTR = isLTRLanguage(myLang);

        let middleBigPic = classNames({
            'middleBigPicLTR': isLTR,
            'middleBigPicRTL': !isLTR
        });

        let imageColClass = classNames({
            'col-md-2': true,
            'imageContainer': true,
            'fileIconImg': true
        });

        if (this.props.index !== 0) {
            index = this.props.index + '. ';
        }

        if (!files) {
            files = [];
        }

        previews = files.map((file, i) => {
            // Display the file only if there is no property called scanStatus or if there is one and its value is SAFE.
            let shouldDisplayFile = !!(!file.hasOwnProperty('scanStatus') || (file.scanStatus && file.scanStatus === WILDFIRE_STATUS_TYPES.SAFE));
            let img;
            let fileName = file.name || file.path;
            let path = file.preview || '/evidence' + file.path;

            if (isImage(file)) {
                img = <img className="smallPreviewImg" src={path} data-tip={fileName.replace('/', '')}/>;
            } else {
                img = <FileIcon className="smallPreviewImg" data-tip={fileName.replace('/', '')}/>;
            }

            return (shouldDisplayFile) ? <div className={imageColClass} key={i}>
                <div>
                    {img}
                </div>
                <div className={middleBigPic}>
                    <IconButton touch={true} tooltip={t[myLang]['Survey_Delete']} tooltipPosition="bottom-center">
                        <ActionDelete
                            className="removeBtn"
                            onTouchTap={this.deleteImage.bind(this, file)}
                        />
                    </IconButton>

                    <a href={path} download="Evidence">
                        <IconButton touch={true} tooltip={t[myLang]['Survey_Evidence_Download']}
                                    tooltipPosition="bottom-center">
                            <ActionDownload
                                className="downloadBtn"
                            />
                        </IconButton>
                    </a>

                </div>
                <ReactTooltip place="bottom"/>
            </div> : null;
        });

        let questionColClass = classNames({
            'col-md-4': true
        });
        let style = {
            comments: {
                margin: '10px',
                'marginBottom': '35px'
            },
            radioBtn: {
                'marginBottom': '10px'
            },
            radioBtnGrp: {
                'margin': '10px'
            },
            question: {
                'margin': '10px'
            },
            addImageBtn: {
                'marginTop': '15px'
            }
        };

        let user = app.getAuthUser();
        let hide = false;
        if (user && user.userRole === 'basic') {
            hide = true;
        }

        let hideClass = classNames({
            'hide': hide
        });

        let hideQuestion = classNames({
            'hide': ((this.props.markedAsImportantFilter && !this.props.question.markedAsImportant) ||
                (this.props.markedAsNoGoFilter && !(this.props.question.noGoQuestionData && this.props.question.noGoQuestionData.isNoGoQuestion)))
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>

                <div style={style.question} className={hideQuestion}>
                    <div className="row">
                        <div className={questionColClass}>
                            <br/>
                            {index}
                            {this.props.question.name}
                            <br/>
                            <br/>
                            <RadioButtonGroup style={style.radioBtnGrp}
                                              onChange={this.onChange.bind(this, this.props.index)}
                                              name="questionsRadioGroup"
                                              valueSelected={this.props.question.answer}>
                                <RadioButton
                                    value="Non Comply"
                                    label={t[myLang]['Template_Quest_Choice1']}
                                    disabled={hide}
                                    style={style.radioBtn}
                                />
                                <RadioButton
                                    value="Comply"
                                    label={t[myLang]['Template_Quest_Choice2']}
                                    disabled={hide}
                                    style={style.radioBtn}
                                />
                                <RadioButton
                                    value="Other"
                                    disabled={hide}
                                    label={t[myLang]['Template_Quest_Choice3']}
                                />
                            </RadioButtonGroup>
                            <RaisedButton className={hideClass} style={style.addImageBtn} disabled={hide}
                                          label={t[myLang]['Survey_Attach_Evidence']}
                                          onTouchTap={this.handleOpenUpload.bind(this)}/>
                            <br/>
                            <TextField disabled={hide}
                                       style={style.comments}
                                       floatingLabelText={t[myLang]['Template_Quest_Comments']}
                                       multiLine={true}
                                       ref={'comment_' + this.props.index}
                                       value={this.state.commentValue}
                                       onBlur={this.handleBlur.bind(this)}
                                       onChange={this.handleCommentsChange.bind(this)}
                                       rows={3}
                            />
                        </div>
                        {previews}
                    </div>
                    <UploadModal
                        open={this.state.openUploadModal}
                        saveImages={this.saveImages.bind(this)}
                        deleteImage={this.deleteImage.bind(this)}
                        files={files}
                        closeDialog={this.closeDialog.bind(this)}
                        loadedLang={this.props.loadedLang}
                    />
                    <Divider/>
                </div>
            </MuiThemeProvider>
        );
    }
}
