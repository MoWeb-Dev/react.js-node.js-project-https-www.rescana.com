import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Dropzone from 'react-dropzone';
import FontIcon from 'material-ui/FontIcon';
import classNames from 'classnames';
import TextField from 'material-ui/TextField';
import {LTR, RTL, MY_LANG, MY_DIR, DEFAULT_LANG, DEFAULT_DIR} from '../../../../config/consts';
import t from '../../../../config/i18n';
import {isLTRLanguage} from '../../common/AppHelpers';
import '../../../../public/css/survey.css';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class ExistingSurveyUploadModal extends React.Component {
    constructor(props) {
        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.uploadExcel = this.uploadExcel.bind(this);
        this.getAllComapniesByID = this.getAllComapniesByID.bind(this);

        /* Only .xlsx extension is supported with the ExcelParser. */
        this.state = {
            open: false,
            files: [],
            disabled: true,
            acceptedFiles: ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            surveyName: '',
            surveyNameError: t[myLang]['Survey_Field_Required'],
            surveyRespondent: '',
            surveyRespondentError: t[myLang]['Survey_Field_Required'],
            surveyCompanyId: '',
            companies: [],
            lang: myLang,
            dir: myDir
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open === true) {
            let myLang = this.state.lang;

            this.setState({
                open: nextProps.open,
                disabled: true,
                files: [],
                surveyName: '',
                surveyNameError: t[myLang]['Survey_Field_Required'],
                surveyRespondent: '',
                surveyRespondentError: t[myLang]['Survey_Field_Required'],
                surveyCompanyId: ''
            });
        }
    }

    componentDidMount() {
        this.getAllComapniesByID();
    }

    getAllComapniesByID() {
        $.ajax({
            type: 'GET',
            url: '/api/getAllCompaniesById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((companies) => {
            let companiesForDD = companies.map((item) => {
                return {companyName: item.companyName, id: item.id};
            });

            this.setState({
                companies: companiesForDD
            });
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load companies.');
        });
    }

    handleClose() {
        this.setState({open: false},
            this.props.closeDialog);
    };

    onDrop(files) {
        if (files.length !== 1) {
            app.addAlert('error', t[this.state.lang]['TemplateUploadDialog_ExcelsLimit']);
        } else {
            const MAX_FILENAME_LENGTH_POSSIBLE = 75;

            if (files[0] && files[0].name && files[0].name.length > MAX_FILENAME_LENGTH_POSSIBLE) {
                app.addAlert('error', t[this.state.lang]['SurveyUploadDialog_FileNameLimit']);
            } else {
                this.setState({
                    files: files
                }, this.changeButtonDisable);
            }
        }
    }

    changeButtonDisable() {
        if (this.state.files.length !== 0) {
            this.setState({
                disabled: false
            });
        } else {
            this.setState({
                disabled: true
            });
        }
    }

    uploadExcel() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            // Create the FormData - note that in this section they are valid (were checked in createSurvey function).
            let data = new FormData();
            let file = this.state.files[0];
            let surveyName = this.state.surveyName;
            let surveyRespondent = this.state.surveyRespondent;
            let surveyCompanyId = this.state.surveyCompanyId;
            let lang = this.state.lang;

            data.append('surveyExcelWithAnswersTemplate', file, surveyName + '_' + lang + '_' + (file.name || file.path));
            data.append('surveyName', surveyName);
            data.append('surveyLang', lang);
            data.append('surveyRespondent', surveyRespondent);
            data.append('surveyCompanyId', surveyCompanyId);

            $.ajax({
                url: '/api/surveyExcelWithAnswersUpload',
                dataType: 'json',
                data: data,
                enctype: 'multipart/form-data',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: (res) => {
                    if (res && res.result && res.result.said) {
                        let newSid = res.result.said;
                        this.handleClose();
                        this.props.openUploadedSurvey(newSid);
                    } else {
                        app.addAlert('error', 'Excel upload failed.');
                    }
                }
            });
        }
    }

    createSurvey() {
        // Make sure the user entered a name for the survey. (if not,an error is displayed)
        if (this.state.surveyName) {
            // Make sure the user entered a respondent for the survey. (if not,an error is displayed)
            if (this.state.surveyRespondent) {
                // Make sure the user entered the company being surveyed. (if not,an error is displayed)
                if (this.state.surveyCompanyId) {
                    // If an Excel is selected, load it.
                    if (this.state.files.length > 0) {
                        this.uploadExcel();
                    }
                } else {
                    app.addAlert('warning', t[this.state.lang]['Survey_NoCompaniesSelected']);
                }
            } else {
                this.respondentInput.focus();
            }
        } else {
            this.nameInput.focus();
        }
    }

    onDropRejected() {
        app.addAlert('error', t[this.state.lang]['TemplateUploadDialog_ExcelSizeLimit']);
    }

    handleChangeSurveyProperty(propertyName, propertyErrorName, event) {
        let value = event.target.value;
        let error;
        if (value) {
            error = '';
        } else {
            error = t[this.state.lang]['Survey_Field_Required'];
        }

        let stateObj = {};
        stateObj[propertyName] = value;
        stateObj[propertyErrorName] = error;
        this.setState(stateObj);
    };

    handleChangeCompany(event, index, value) {
        if (value && this.state.surveyCompanyId !== value) {
            this.setState({surveyCompanyId: value});
        }
    }

    render() {
        let dropZoneClass = classNames({
            'dropZone': true
        });
        let activeDropZoneClass = classNames({
            'stripes': true
        });
        let rejectDropZoneClass = classNames({
            'rejectStripes': true
        });
        let uploadCloudIcon = classNames({
            'material-icons': true,
            'uploadIconSize': true
        });

        let myLang = this.props.loadedLang || this.state.lang;

        let isLTR = isLTRLanguage(myLang);

        let myDir = (isLTR) ? LTR : RTL;

        let buttonsContainer = classNames({
            'uploadDialogButtonsLTR': isLTR,
            'uploadDialogButtonsRTL': !isLTR
        });

        let color;
        let excelText;

        if (this.state.files && this.state.files.length > 0) {
            color = '#00BCD4';
            excelText = t[myLang]['TemplateUploadDialog_ExcelSelected'] + this.state.files[0].name;
        } else {
            color = '#F44336';
            excelText = t[myLang]['TemplateUploadDialog_NoExcelSelected'];
        }

        const dropzoneLabelStyle = {color: color};

        let selectedCompanyLabelColor;
        let selectedCompanyText;

        if (this.state.surveyCompanyId) {
            selectedCompanyLabelColor = 'white';
            selectedCompanyText = '';
        } else {
            selectedCompanyLabelColor = '#F44336';
            selectedCompanyText = t[myLang]['Survey_NoCompaniesSelected'];
        }
        const selectedCompanyLabelStyle = {color: selectedCompanyLabelColor};

        const actions = [
            <FlatButton
                label={t[myLang]['TemplateUploadDialog_Submit']}
                primary={true}
                style={{
                    color: 'white',
                    marginBottom: '5px',
                    marginRight: '5px',
                    height: 40
                }}
                keyboardFocused={true}
                backgroundColor={'#0091ea'}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                disabled={this.state.disabled}
                onTouchTap={this.createSurvey.bind(this)}
            />,
            <FlatButton
                label={t[myLang]['TemplateUploadDialog_Cancel']}
                primary={true}
                backgroundColor={'#0091ea'}
                style={{color: 'white', marginRight: '5px', marginBottom: '5px', height: 40}}
                keyboardFocused={true}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                onTouchTap={this.handleClose.bind(this)}
            />
        ];

        let companies = [<MenuItem value={''} key={0} primaryText={t[myLang]['Survey_Company']} disabled={true}/>];
        if (this.state.companies) {
            for (let i = 0; i < this.state.companies.length; i++) {
                companies.push(<MenuItem value={this.state.companies[i].id} key={i + 1}
                                         primaryText={this.state.companies[i].companyName}/>);
            }
        }
        // if there are no available companies - show a message.
        if (companies.length === 1) {
            companies.push(<MenuItem value={undefined} key={1}
                                     primaryText={t[myLang]['Survey_NoCompaniesAvailable']}/>);
        }

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title={t[myLang]['ExistingSurveyUploadModal_Title']}
                        actions={actions}
                        actionsContainerClassName={buttonsContainer}
                        open={this.state.open}
                        onRequestClose={this.handleClose.bind(this)}
                        actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                        bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                        autoScrollBodyContent={true}
                        modal={true}
                        repositionOnUpdate={false}
                        contentStyle={{direction: myDir, maxWidth: '50%', borderRadius: '7px 7px 7px 7px'}}
                        titleStyle={{
                            fontSize: 18,
                            background: 'rgba(0,0,0,0.7)',
                            color: 'white', textAlign: 'center',
                            borderRadius: '2px 2px 0px 0px',
                            textTransform: 'uppercase',
                        }}
                    >
                        <div style={{paddingLeft: '10px',paddingRight: '10px'}}>
                            <TextField
                                id={'text-field-survey-name'}
                                hintText={t[myLang]['Survey_Name']}
                                floatingLabelText={t[myLang]['Survey_Name']}
                                onChange={this.handleChangeSurveyProperty.bind(this, 'surveyName', 'surveyNameError')}
                                errorText={this.state.surveyNameError}
                                ref={(input) => {
                                    this.nameInput = input;
                                }}
                            />
                            <br/>
                            <TextField
                                id={'text-field-survey-respondent'}
                                hintText={t[myLang]['Survey_Respondent_Hint']}
                                floatingLabelText={t[myLang]['Survey_Respondent']}
                                onChange={this.handleChangeSurveyProperty.bind(this, 'surveyRespondent', 'surveyRespondentError')}
                                errorText={this.state.surveyRespondentError}
                                ref={(input) => {
                                    this.respondentInput = input;
                                }}
                            />
                            <br/>
                            <div style={{display: 'inline-block'}}>
                                <DropDownMenu
                                    style={{display: 'inline-flex'}}
                                    maxHeight={300}
                                    value={this.state.surveyCompanyId}
                                    onChange={this.handleChangeCompany.bind(this)}
                                >
                                    {companies}
                                </DropDownMenu>
                                <label style={selectedCompanyLabelStyle}>{selectedCompanyText}</label>
                            </div>
                            <br/>
                            <br/>
                            <Dropzone
                                accept={this.state.acceptedFiles.join(',')}
                                onDrop={this.onDrop.bind(this)}
                                className={dropZoneClass}
                                activeClassName={activeDropZoneClass}
                                rejectClassName={rejectDropZoneClass}
                                onDropRejected={this.onDropRejected.bind(this)}
                                maxSize={3000000}
                            >
                                <div className="dropzoneTextStyle">
                                    <p className="dropzoneParagraph">{t[myLang]['TemplateUploadDialog_Dropzone_Text']}</p>
                                    <br/>
                                    <FontIcon
                                        className={uploadCloudIcon}
                                    >
                                        cloud_upload
                                    </FontIcon>
                                </div>
                            </Dropzone>
                            <br/>
                            <label style={dropzoneLabelStyle}>{excelText}</label>
                            <br/>
                            <label>{t[myLang]['TemplateUploadDialog_Download_Examples']}</label>
                            <a href="/Standard_Survey_With_Answers_Template_en.xlsx"
                               download="Survey With Answers Template.xlsx">
                                <FlatButton
                                    label={t[myLang]['TemplateUploadDialog_Download_English_Example']}
                                    primary={true}
                                />
                            </a>
                            <a href="/Standard_Survey_With_Answers_Template_he.xlsx"
                               download="תבנית לסקר עם תשובות.xlsx">
                                <FlatButton
                                    label={t[myLang]['TemplateUploadDialog_Download_Hebrew_Example']}
                                    primary={true}
                                />
                            </a>
                        </div>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
