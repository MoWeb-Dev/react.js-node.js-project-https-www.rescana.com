import React, {Component} from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardText, CardTitle} from 'material-ui/Card';
import MenuItem from 'material-ui/MenuItem';
import {browserHistory} from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {ValidatorForm, TextValidator, SelectValidator} from 'react-material-ui-form-validator';
import {LTR, MY_DIR, MY_LANG, DEFAULT_LANG, DEFAULT_DIR} from '../../../../config/consts';
import t from '../../../../config/i18n';
import NoTemplatesModal from './no-templates-modal.jsx';
import classNames from 'classnames';
import '../../../../public/css/survey.css';
import themeForFont from "../../app/themes/themeForFont";

const shortid = require('shortid');
const moment = require('moment');

export default class SurveyNew extends React.Component {
    constructor() {
        super();

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.state = {
            surveyName: '',
            fillerName: '',
            selectedCompanyId: '',
            selectedCompanyName: '',
            selectedTemplateSid: '',
            openNoTemplateModal: false,
            templates: [],
            companies: [],
            lang: myLang,
            dir: myDir
        };
        this.redirectToSurveyStart = this.redirectToSurveyStart.bind(this);
    }

    runSurvey(said) {
        browserHistory.push({
            pathname: '/survey',
            search: '?said=' + said
        });
    }

    closeDialog() {
        this.setState({openNoTemplateModal: false});
    }

    // When using this function, it is required to add an uid as an array.
    initQuestionnaireObjectWithoutUid() {
        let questionnaire = {};
        questionnaire.lastChangedDate = moment().format();
        questionnaire.sid = this.state.selectedTemplateSid;
        questionnaire.selectedCompanyId = this.state.selectedCompanyId;
        questionnaire.selectedCompanyName = this.state.selectedCompanyName;
        questionnaire.respondent = this.state.fillerName;
        questionnaire.name = this.state.surveyName;
        questionnaire.said = shortid.generate();
        questionnaire.preferences = this.state.templatePreferences;
        return questionnaire;
    }

    createNewSurvey(e) {
        e.preventDefault();
        let user = app.getAuthUser();
        if (!user) {
            app.routeAuthUser();
        } else {
            let questionnaire = this.initQuestionnaireObjectWithoutUid();
            questionnaire.uid = [];
            questionnaire.uid.push(user.id);
            if(user.logo){
                questionnaire.logo = user.logo;
            }
            $.ajax({
                type: 'POST',
                url: '/api/saveSurveyWithAnswers',
                data: JSON.stringify({questionnaire}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done(() => {
                this.redirectToSurveyStart();
            }).fail(() => {
                app.addAlert('error', t[this.state.lang]['Survey_createNewSurvey_Error']);
            });
        }
    }
    redirectToSurveyStart(){
        browserHistory.push({
            pathname: '/survey-start'
        });
    }

    handleChangeName(event) {
        this.setState({
            surveyName: event.target.value
        });
    };

    handleChangeFiller(event) {
        this.setState({
            fillerName: event.target.value
        });
    };

    filterTemplateNames(templateNames) {
        return templateNames.map((template) => {
            return {sid: template.sid, template: template.name, preferences: template.preferences};
        });
    }

    getAllComapniesByID(user) {
        $.ajax({
            type: 'GET',
            url: '/api/getAllCompaniesById',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((companies) => {
            let companiesForDD = companies.map((item) => {
                return {companyName: item.companyName, id: item.id};
            });

            this.setState({
                companies: companiesForDD
            });
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load companies.');
        });
    }

    getTemplates(user) {
        $.ajax({
            type: 'POST',
            url: '/api/getSurvey',
            data: JSON.stringify({
                uid: user.id
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            let templateNames = this.filterTemplateNames(data);
            if (templateNames.length > 0) {
                this.setState({templates: templateNames});
            } else {
                this.setState({openNoTemplateModal: true});
            }

            this.getAllComapniesByID(user);
        }).fail(() => {
            app.addAlert('error', t[this.state.lang]['Survey_getTemplates_Error']);
        });
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            this.getTemplates(user);
            app.setFlagVisibile(true);
        }
    }

    cancel() {
        browserHistory.push({
            pathname: '/survey-start'
        });
    }

    updateSelectedTemplate(selectedTemplateSid, templatePreferences) {
        if(templatePreferences){
            this.setState({templatePreferences: templatePreferences});
        }
        this.setState({selectedTemplateSid});
    }

    updateSelectedCompany(selectedCompanyId, selectedCompanyName) {
        this.setState({selectedCompanyId: selectedCompanyId, selectedCompanyName: selectedCompanyName});
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        let submitStyleBtn = {
            margin: 12
        };

        let myDir = this.state.dir;

        return (
            <div className={pageStyleClass} dir={myDir}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <ValidatorForm ref="form"
                                   onSubmit={this.createNewSurvey.bind(this)}
                                   onError={(errors) => console.log(errors)}>
                        <div style={{margin: '35px', marginBottom: '55px', fontSize: 24}}>{t[this.state.lang]['Survey_Description']}</div>
                        <Card style={{margin: 10, paddingLeft: 30}}>
                            <div>
                                <CardText>
                                    <TextValidator
                                        id="text-field-survey-name"
                                        hintText={t[this.state.lang]['Survey_Name']}
                                        name="surveyName"
                                        floatingLabelText={t[this.state.lang]['Survey_Name']}
                                        value={this.state.surveyName}
                                        onChange={this.handleChangeName.bind(this)}
                                        validators={['required']}
                                        errorMessages={t[this.state.lang]['Survey_Field_Required']}
                                    />
                                    <br/>
                                    <TextValidator
                                        id="text-field-survey-filler"
                                        hintText={t[this.state.lang]['Survey_Respondent_Hint']}
                                        name="surveyFiller"
                                        floatingLabelText={t[this.state.lang]['Survey_Respondent']}
                                        value={this.state.fillerName}
                                        onChange={this.handleChangeFiller.bind(this)}
                                        validators={['required']}
                                        errorMessages={t[this.state.lang]['Survey_Field_Required']}
                                    />
                                    <SelectFieldTemplate updateSelectedItem={this.updateSelectedTemplate.bind(this)}
                                                         templates={this.state.templates}
                                    />
                                    <SelectFieldCompany updateSelectedItem={this.updateSelectedCompany.bind(this)}
                                                        companies={this.state.companies}
                                    />
                                    <br/>
                                    <RaisedButton label={t[this.state.lang]['Survey_Next']} secondary={true}
                                                  type="submit"
                                                  style={submitStyleBtn}/>
                                    <FlatButton label={t[this.state.lang]['Survey_Back']}
                                                primary={true}
                                                style={submitStyleBtn}
                                                onTouchTap={this.cancel.bind(this)}
                                    />
                                </CardText>
                            </div>
                        </Card>
                    </ValidatorForm>
                </MuiThemeProvider>
                <NoTemplatesModal
                    open={this.state.openNoTemplateModal}
                />
            </div>
        );
    }
}

class SelectFieldTemplate extends Component {
    constructor() {
        super();

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            value: null,
            lang: myLang
        };
    }

    handleChange(ignore, index, value) {
        this.setState({value});
        this.props.updateSelectedItem(this.props.templates[index].sid, this.props.templates[index].preferences);
    }

    render() {
        let items = this.props.templates.map((item) => {
            return <MenuItem key={item.sid} value={item.template} primaryText={item.template}/>;
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <SelectValidator
                        value={this.state.value}
                        name="selectTemplate"
                        onChange={this.handleChange.bind(this)}
                        floatingLabelText={t[this.state.lang]['Survey_Template']}
                        validators={['required']}
                        style={{direction: LTR}}
                        errorMessages={t[this.state.lang]['Survey_Field_Required']}
                    >
                        {items}
                    </SelectValidator>
                </div>
            </MuiThemeProvider>
        );
    }
}

class SelectFieldCompany extends Component {
    constructor() {
        super();

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            value: null,
            lang: myLang
        };
    }

    handleChange(ignore, index, value) {
        this.setState({value});
        this.props.updateSelectedItem(this.props.companies[index].id, this.props.companies[index].companyName);
    }

    render() {
        let items = this.props.companies.map((item) => {
            return <MenuItem key={item.id} value={item.companyName} primaryText={item.companyName}/>;
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <SelectValidator
                        value={this.state.value}
                        name="selectCompany"
                        onChange={this.handleChange.bind(this)}
                        floatingLabelText={t[this.state.lang]['Survey_Company']}
                        validators={['required']}
                        style={{direction: LTR}}
                        errorMessages={t[this.state.lang]['Survey_Field_Required']}
                    >
                        {items}
                    </SelectValidator>
                </div>
            </MuiThemeProvider>
        );
    }
}
