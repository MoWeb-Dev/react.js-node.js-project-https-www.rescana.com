import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import {LTR, RTL, MY_LANG, MY_DIR, DEFAULT_LANG, DEFAULT_DIR} from '../../../../config/consts';
import t from '../../../../config/i18n';
import {isLTRLanguage} from '../../common/AppHelpers';
import {browserHistory} from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";


export default class NoTemplateModal extends React.Component {
    constructor(props) {
        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.state = {
            open: false,
            lang: myLang,
            dir: myDir
        };
    }

    handleClose() {
        this.props.closeDialog();
        this.setState({open: false});
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open,
            files: nextProps.files
        });
    }

    navToCreateTemplate() {
        browserHistory.push({
            pathname: '/survey-wizard'
        });
    }

    render() {
        let myLang = this.props.loadedLang || this.state.lang;

        let isLTR = isLTRLanguage(myLang);

        let myDir = (isLTR) ? LTR : RTL;

        let buttonsContainer = classNames({
            'uploadDialogButtonsLTR': isLTR,
            'uploadDialogButtonsRTL': !isLTR
        });

        const actions = [
            <FlatButton
                label={t[myLang]['NoTemplate_Create_Template']}
                primary={true}
                onTouchTap={this.navToCreateTemplate.bind(this)}
            />
        ];

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title={t[myLang]['NoTemplate_Title']}
                        actions={actions}
                        actionsContainerClassName={buttonsContainer}
                        modal={true}
                        open={this.state.open}
                        contentStyle={{direction: myDir}}
                        onRequestClose={this.handleClose.bind(this)}
                        autoScrollBodyContent={true}
                    >
                        <div>
                            {t[myLang]['NoTemplate_Text']}
                        </div>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
