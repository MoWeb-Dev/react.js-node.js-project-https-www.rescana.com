import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import t from '../../../../config/i18n';
import classNames from 'classnames';
import {LTR, RTL, MY_LANG, DEFAULT_LANG, ORGANIZATION_NAME} from '../../../../config/consts';
import {isLTRLanguage} from '../../common/AppHelpers';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class sendSurveyModal extends React.Component {
    constructor(props) {
        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            open: false,
            message: '',
            recipient: '',
            senderEmail: '',
            senderEmailExpiry: '',
            sender: '',
            lang: myLang
        };
    }

    handleChangeSender(e) {
        this.setState({
            sender: e.target.value
        });
    }

    handleChangeMessage(e) {
        this.setState({
            message: e.target.value
        });
    }

    handleChangeRecipient(event) {
        this.setState({
            recipient: event.target.value
        });
    };

    handleChangeSenderEmail(event) {
        this.setState({
            senderEmail: event.target.value
        });
    };

    handleChangeSenderEmailExpiry(event) {
        this.setState({
            senderEmailExpiry: event.target.value
        });
    };

    handleClose() {
        this.props.close();
    };

    handleSubmit(e) {
        e.preventDefault();
        let tokenCreationDate = new Date();
        let curOrgName = localStorage.getItem(ORGANIZATION_NAME) || "";

        let data = {
            message: this.state.message,
            recipient: this.state.recipient,
            sender: this.state.sender,
            senderEmail: this.state.senderEmail,
            surveyName: this.state.surveyName,
            senderEmailExpiry: this.state.senderEmailExpiry,
            tokenCreationTime: new Date().toISOString(),
            tokenExpirationTime: new Date(tokenCreationDate.setTime(tokenCreationDate.getTime() +
                this.state.senderEmailExpiry * 86400000 )).toISOString(),
            said: this.props.data.said,
            companyName: this.props.data.companyName,
            organizationName: curOrgName,
            companyId: this.props.data.companyId,
            isSentWithEmail: true
        };
        this.props.sendSurvey(data);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open,
            sender: nextProps.data && nextProps.data.sender,
            surveyName: nextProps.data && nextProps.data.surveyName
        });
    }

    render() {
        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = (isLTRLanguage(myLang)) ? LTR : RTL;
        let isLTR = (myDir === LTR);

        let sendSurveyTextField = classNames({
            'sendSurveyTextFieldLTR': isLTR,
            'sendSurveyTextFieldRTL': !isLTR
        });

        let senderEmailTextField = classNames({
            'senderEmailTextFieldLTR': isLTR,
            'senderEmailTextFieldRTL': !isLTR
        });

        let buttonsContainer = classNames({
            'uploadDialogButtonsLTR': isLTR,
            'uploadDialogButtonsRTL': !isLTR
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title="Send Survey"
                        open={this.state.open || false}
                        actionsContainerClassName={buttonsContainer}
                        actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                        bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                        autoScrollBodyContent={true}
                        modal={true}
                        repositionOnUpdate={false}
                        contentStyle={{direction: myDir,  borderRadius: '7px 7px 7px 7px'}}
                        titleStyle={{
                            fontSize: 18,
                            background: 'rgba(0,0,0,0.7)',
                            color: 'white', textAlign: 'center',
                            borderRadius: '2px 2px 0px 0px',
                            textTransform: 'uppercase',
                        }}
                    >
                        <br/>
                        <span style={{padding: '0px 30px 0px 30px'}}>{t[this.state.lang]['Survey_invite']}</span>
                        <ValidatorForm ref="form"
                                       className="commentForm"
                                       onSubmit={this.handleSubmit.bind(this)}
                                       onError={(errors) => console.log(errors)}>
                            <div style={{padding: '0px 30px 0px 30px'}} className="row">
                                <TextValidator
                                    floatingLabelText={t[this.state.lang]['Survey_Sender']}
                                    onChange={this.handleChangeSender.bind(this)}
                                    name="sender"
                                    hintText={t[this.state.lang]['Survey_Sender']}
                                    className={sendSurveyTextField}
                                    id="survey-name-field"
                                    value={this.state.sender}
                                    validators={['required']}
                                    errorMessages={t[this.state.lang]['Survey_Field_Required']}
                                />
                                <TextValidator
                                    name="email"
                                    validators={['required', 'isEmail']}
                                    errorMessages={[t[this.state.lang]['Survey_Field_Required'], t[this.state.lang]['Survey_Email_Not_Valid']]}
                                    id="recipient-name-field"
                                    hintText={t[this.state.lang]['Survey_To']}
                                    floatingLabelText={t[this.state.lang]['Survey_Recipient_Email']}
                                    value={this.state.recipient}
                                    onChange={this.handleChangeRecipient.bind(this)}
                                />
                            </div>
                            <div className="row" style={{padding: '0px 30px 20px 30px'}}>
                                <TextValidator
                                    name="senderEmail"
                                    validators={['required', 'isEmail']}
                                    errorMessages={[t[this.state.lang]['Survey_Field_Required'], t[this.state.lang]['Survey_Email_Not_Valid']]}
                                    id="sender-email-field"
                                    hintText={t[this.state.lang]['Survey_Sender_Email']}
                                    floatingLabelText={t[this.state.lang]['Survey_Sender_Email']}
                                    className={senderEmailTextField}
                                    value={this.state.senderEmail}
                                    onChange={this.handleChangeSenderEmail.bind(this)}
                                />
                                <TextField
                                    hintText={t[this.state.lang]['Survey_Comment']}
                                    floatingLabelText={t[this.state.lang]['Survey_Comment']}
                                    multiLine={true}
                                    rows={2}
                                    className={sendSurveyTextField}
                                    rowsMax={4}
                                    id="comment-name-field"
                                    value={this.state.message}
                                    onChange={this.handleChangeMessage.bind(this)}
                                />
                            </div>
                            <div className="row" style={{padding: '0px 30px 20px 30px'}}>
                                <TextValidator
                                    name="senderEmailExpiry"
                                    validators={['required', 'isNumber', 'minNumber:3', 'maxNumber:60']}
                                    errorMessages={[t[this.state.lang]['Survey_Field_Required'],
                                        t[this.state.lang]['Survey_Email_Days_Not_Valid'],
                                        t[this.state.lang]['Survey_Email_Days_Not_Fit'],
                                        t[this.state.lang]['Survey_Email_Days_Not_Fit']]}
                                    id="sender-email-expiry-field"
                                    hintText={'3-60'}
                                    floatingLabelText={t[this.state.lang]['Survey_Sender_Email_Expiry']}
                                    className={senderEmailTextField}
                                    value={this.state.senderEmailExpiry}
                                    onChange={this.handleChangeSenderEmailExpiry.bind(this)}
                                />
                            </div>
                            <div style={{float: 'right'}} className="row">
                                <RaisedButton
                                    label={t[this.state.lang]['Survey_Send']}
                                    primary={true}
                                    style={{
                                        color: 'white',
                                        marginBottom: '5px',
                                        marginRight: '5px',
                                        height: 40
                                    }}
                                    keyboardFocused={true}
                                    backgroundColor={'#0091ea'}
                                    hoverColor={'#12a4ff'}
                                    rippleColor={'white'}
                                    labelStyle={{fontSize: 10}}
                                    type="submit"
                                />
                                <FlatButton
                                    label={t[this.state.lang]['Survey_Cancel']}
                                    primary={true}
                                    backgroundColor={'#0091ea'}
                                    style={{color: 'white',marginRight: '5px', marginBottom: '5px', height: 40}}
                                    keyboardFocused={true}
                                    hoverColor={'#12a4ff'}
                                    rippleColor={'white'}
                                    labelStyle={{fontSize: 10}}
                                    onTouchTap={this.handleClose.bind(this)}
                                />
                            </div>
                        </ValidatorForm>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
