export function deleteIndexInArray(array, idx, objectName) {
    let isDeleted = false;

    // At least one {objectName} is required.
    if (array.length > 1) {
        const temp = [];

        for (let i = array.length - 1; i > idx; i--) {
            temp.push(array.pop());
        }

        array.pop();

        isDeleted = true;

        for (let i = temp.length; i > 0; i--) {
            array.push(temp.pop());
        }
    } else {
        app.addAlert('warning', 'At least one ' + objectName + ' is required.');
    }

    return (isDeleted);
}

export function deleteIDFromSurveys(srvsData, srvID) {
    if (srvsData.length >= 1) {
        const temp = [];
        let top;

        for (let i = srvsData.length - 1; i > 0; i--) {
            top = srvsData.pop();
            if (top.surveyIdentifier === srvID) {
                srvsData.push(top);
                break;
            } else {
                temp.push(top);
            }
        }

        srvsData.pop();

        for (let i = temp.length; i > 0; i--) {
            srvsData.push(temp.pop());
        }
    }
}

export function isImage(file) {
    const fileName = file.name || file.path;
    const suffix = fileName.substr(fileName.indexOf('.')+1).toLowerCase();
    if (suffix === 'jpg' || suffix === 'jpeg' || suffix === 'bmp' || suffix === 'png') {
        return true;
    }
}
