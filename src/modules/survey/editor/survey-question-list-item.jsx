import React from 'react';
import TextField from 'material-ui/TextField';
import Slider from 'material-ui/Slider';
import IconButton from 'material-ui/IconButton';
import Checkbox from 'material-ui/Checkbox';
import StarIcon from 'material-ui/svg-icons/toggle/star';
import StarBorderIcon from 'material-ui/svg-icons/toggle/star-border';
import ActionDelete from 'material-ui/svg-icons/action/delete-forever';
import {LTR, RTL, MY_LANG, DEFAULT_LANG} from '../../../../config/consts';
import t from '../../../../config/i18n';
import classNames from 'classnames';
import {isLTRLanguage} from '../../common/AppHelpers.js';
import '../../../../public/css/survey.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import NoGoOffIcon from 'material-ui/svg-icons/content/remove-circle-outline.js';
import NoGoOnIcon from 'material-ui/svg-icons/content/remove-circle.js';
import ReactTooltip from 'react-tooltip';
import InformationHelpText from "../../generalComponents/information-help-text.jsx";

export default class QuestionListItem extends React.Component {
    constructor(props) {
        super(props);

        let myLang = this.props.loadedLang || localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            value: this.props.question.name,
            question: {},
            slider: this.props.question.weight,
            markedAsImportant: this.props.question.markedAsImportant,
            noGoQuestionData: this.props.question.noGoQuestionData,
            isOpenNoGoDialog: false,
            lang: myLang
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleSlider(ignore, value) {
        this.setState({slider: value});
    };

    handleDragDrop(questionIndex) {
        this.props.saveScoreWeight(this.state.slider, questionIndex);
    }

    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    };

    handleBlur(questionIndex, event) {
        if (event.target.value) {
            this.props.saveQuestion(event.target.value, questionIndex);
        }
    }

    componentWillReceiveProps(nextProp) {
        this.setState({
            value: nextProp.question.name,
            slider: nextProp.question.weight,
            markedAsImportant: nextProp.question.markedAsImportant,
            noGoQuestionData: nextProp.question.noGoQuestionData,
        });
    }

    deleteQuestion() {
        this.props.deleteQuestion();
    }

    markQuestionForSummary(ignore, isInputChecked) {
        this.setState({markedAsImportant: isInputChecked},
            this.props.markQuestionForSummary(isInputChecked)
        );
    }

    markAsNoGoQuestion() {
        let noGoQuestionData = this.state.noGoQuestionData || {isNoGoQuestion: false, noGoCorrectAnswer: 1};
        noGoQuestionData.isNoGoQuestion = !noGoQuestionData.isNoGoQuestion;
        noGoQuestionData.noGoCorrectAnswer = noGoQuestionData.noGoCorrectAnswer || 1;
        this.setState({noGoQuestionData: noGoQuestionData},
            this.props.markAsNoGoQuestion(noGoQuestionData)
        );
        this.handleCloseNoGoDialog();
    }

    handleOpenNoGoDialog() {
        let noGoQuestionData = this.state.noGoQuestionData || {isNoGoQuestion: false, noGoCorrectAnswer: 1};
        if(noGoQuestionData && noGoQuestionData.hasOwnProperty('isNoGoQuestion') && noGoQuestionData.isNoGoQuestion === true){
            this.markAsNoGoQuestion();
        } else {
            this.setState({isOpenNoGoDialog: true});
        }
    }

    handleCloseNoGoDialog() {
        this.setState({isOpenNoGoDialog: false});
    }

    onChangeNoGoAnswerDropdown(event, index, value) {
        if (value && typeof value === "number") {
            let noGoQuestionData = this.state.noGoQuestionData || {isNoGoQuestion: false, noGoCorrectAnswer: 1};
            noGoQuestionData.noGoCorrectAnswer = value;
            this.setState({noGoQuestionData: noGoQuestionData});
        }
    }

    render() {
        let style = {
            textField: {
                marginTop: '15px',
                width: '80%'
            },
            indexField: {
                display: 'inline'
            },
            iconField: {
                margin: '0px',
                padding: '12px',
                height: '48px',
                width: '48px',
                cursor: 'pointer'
            }
        };

        let myLang = this.state.lang;

        let myDir = (isLTRLanguage(myLang)) ? LTR : RTL;

        let axisType;
        let isLTR;

        if (myDir === LTR) {
            axisType = 'x';
            isLTR = true;
        } else {
            axisType = 'x-reverse';
            isLTR = false;
        }

        let questDeleteClass = classNames({
            'questDeleteLTR': isLTR,
            'questDeleteRTL': !isLTR
        });

        let index = this.props.index + 1;
        index = index + '. ';

        let i18nKeyForCheckbox = (this.state.markedAsImportant) ? 'Survey_Editor_UnmarkAsImportant' : 'Survey_Editor_MarkAsImportant';

        let noGoQuestionData = this.state.noGoQuestionData || {isNoGoQuestion: false, noGoCorrectAnswer: 1};

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <div className="row">
                        <div className="col-md-5">
                            <div>
                                <div style={style.indexField}>
                                <span style={{marginRight: '5px', marginLeft: '5px', fontSize: '14px'}}>
                                    {index}
                                </span>
                                </div>
                                <TextField
                                    id="text-field-question-items"
                                    value={this.state.value || ''}
                                    style={style.textField}
                                    multiLine={true}
                                    onBlur={this.handleBlur.bind(this, this.props.index)}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col-md-2" style={{marginLeft: '40px', marginRight: '70px'}}>
                            <div>
                                <span>{t[this.state.lang]['Template_Quest_Weight']} {this.state.slider ? this.state.slider : 0}</span>
                                <Slider min={0}
                                        max={10}
                                        step={1}
                                        axis={axisType}
                                        value={this.state.slider}
                                        onChange={this.handleSlider.bind(this)}
                                        onDragStop={this.handleDragDrop.bind(this, this.props.index)}
                                />
                            </div>
                        </div>
                        <div className={questDeleteClass}>
                            <Checkbox
                                checked={!!this.state.markedAsImportant}
                                checkedIcon={<StarIcon/>}
                                uncheckedIcon={<StarBorderIcon/>}
                                style={style.iconField}
                                onCheck={this.markQuestionForSummary.bind(this)}
                                data-tip={t[myLang][i18nKeyForCheckbox]}
                                data-for={'Important'}
                            />
                            <Checkbox
                                uncheckedIcon={<NoGoOffIcon/>}
                                checkedIcon={<NoGoOnIcon/>}
                                checked={!!noGoQuestionData.isNoGoQuestion}
                                style={style.iconField}
                                onCheck={this.handleOpenNoGoDialog.bind(this)}
                                data-tip={t[myLang]['Survey_Editor_No_Go_Checkbox']}
                                data-for={'No_Go_Checkbox'}
                            />
                            <ReactTooltip id='No_Go_Checkbox' place={isLTR? "right": "left"} effect="solid" multiline={true}/>
                            <ReactTooltip id='Important' place={isLTR? "right": "left"} effect="solid"/>
                            <IconButton data-for={'Important'} data-tip={t[this.state.lang]['Template_Delete_Question']}>
                                <ActionDelete onTouchTap={this.deleteQuestion.bind(this)}/>
                            </IconButton>
                        </div>
                        <div dir={myDir}>
                            <Dialog
                                title={t[myLang]['No_Go_Question_Title']}
                                modal={true}
                                autoScrollBodyContent={true}
                                open={this.state.isOpenNoGoDialog || false}
                                contentStyle={{maxWidth: '38%', borderRadius: '7px 7px 7px 7px'}}
                                actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                                bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                                titleStyle={{
                                    fontSize: 18, background: 'rgba(0,0,0,0.7)', color: 'white', textAlign: 'center',
                                    borderRadius: '2px 2px 0px 0px', textTransform: 'uppercase',
                                }}
                                actions={[
                                    <FlatButton
                                        label={t[myLang]['Survey_Save']}
                                        primary={true}
                                        keyboardFocused={false}
                                        onTouchTap={this.markAsNoGoQuestion.bind(this)}
                                        style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                                        backgroundColor={'#0091ea'}
                                        hoverColor={'#12a4ff'}
                                        rippleColor={'white'}
                                        labelStyle={{fontSize: 10}}
                                    />,
                                    <FlatButton
                                        label={t[myLang]['Survey_Cancel']}
                                        primary={true}
                                        keyboardFocused={true}
                                        onTouchTap={this.handleCloseNoGoDialog.bind(this)}
                                        style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                                        backgroundColor={'#0091ea'}
                                        hoverColor={'#12a4ff'}
                                        rippleColor={'white'}
                                        labelStyle={{fontSize: 10}}
                                    />
                                ]}>
                                <br/>
                                <br/>
                                <div dir={myDir}>
                                    <div>{t[myLang]['No_Go_CheckBox_Dialog_text']}</div>
                                    <br/>
                                    <DropDownMenu
                                        value={noGoQuestionData.noGoCorrectAnswer || 1}
                                        onChange={this.onChangeNoGoAnswerDropdown.bind(this)}
                                        autoWidth={false}
                                        style={{width: '300px'}}>
                                        <MenuItem value={1} primaryText="Comply"/>
                                        <MenuItem value={2} primaryText="Non-Comply"/>
                                        <MenuItem value={3} primaryText="Other"/>
                                    </DropDownMenu>
                                </div>
                                <InformationHelpText
                                    dir={myDir}
                                    aboveSpace={'100px'}
                                    explanationHeader={t[myLang]['Survey_Editor_No_Go_Explanation_Header']}
                                    explanationText={t[myLang]['Survey_Editor_No_Go_Explanation']}/>
                            </Dialog>
                        </div>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}
