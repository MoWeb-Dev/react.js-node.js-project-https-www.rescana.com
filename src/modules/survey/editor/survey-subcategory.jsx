import React from 'react';
import QuestionListItem from './survey-question-list-item.jsx';
import {Card, CardText} from 'material-ui/Card';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import TextField from 'material-ui/TextField';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import ActionDelete from 'material-ui/svg-icons/action/delete-forever';
import {
    MY_LANG,
    DEFAULT_LANG,
    LTR,
    RTL
} from '../../../../config/consts';
import t from '../../../../config/i18n';
import {isLTRLanguage} from '../../common/AppHelpers.js';
import '../../../../public/css/survey.css';
import themeForFont from "../../app/themes/themeForFont";
import FollowUpQuestionIcon from 'material-ui/svg-icons/action/assignment-returned.js';
import DropDownMenu from 'material-ui/DropDownMenu';
import Dialog from 'material-ui/Dialog';
import MenuItem from 'material-ui/MenuItem';
const shortid = require('shortid');
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import DataTables from 'material-ui-datatables';
import DeleteDialog from "../../app/delete-dialog.jsx";
import ReactTooltip from 'react-tooltip';
import FuzzySearch from "fuzzy-search";
import InformationHelpText from "../../generalComponents/information-help-text.jsx";


export default class QuestionsSubCategory extends React.Component {
    constructor(props) {
        super(props);

        let myLang = this.props.loadedLang || localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            lang: myLang,
            categoryTitle: this.props.name,
            followUpCategoryData: this.props.followUpCategoryData,
            isOpenFollowUpDialog: false,
            curCategoryIndex: this.props.curCategoryIndex,
            curPageIndex: this.props.curPageIndex,
            fullQuestionnaire: this.props.fullQuestionnaire,
            openDeleteDialog: false,

            tableColumns: [],
            tableData: [],
            sortedTableData: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            rowSize: 10,
            page: 1,
        };
        this.handleOpenFollowUpDialog = this.handleOpenFollowUpDialog.bind(this);
        this.setDialogTableData = this.setDialogTableData.bind(this);
    }

    addQuestion(questionsData) {
        let question = {
            type: 'radiogroup',
            name: questionsData.text,
            choices: [
                t[this.state.lang]['Template_Quest_Choice1'],
                t[this.state.lang]['Template_Quest_Choice1'],
                t[this.state.lang]['Template_Quest_Choice3']
            ],
            withText: {
                name: t[this.state.lang]['Template_Quest_Comments']
            },
            weight: 5,
            markedAsImportant: false
        };
        this.props.addQuestion(question);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps) {
            let categoryTitle = '';
            if (nextProps.name) {
                categoryTitle = nextProps.name;
            }
            let curCategoryIndex = -1;
            if (nextProps.curCategoryIndex) {
                curCategoryIndex = nextProps.curCategoryIndex;
            }
            let curPageIndex = -1;
            if (nextProps.curPageIndex) {
                curPageIndex = nextProps.curPageIndex;
            }

            let followUpCategoryData = {isFollowUpCategory: false, followUpCategoryId: '', followUpCategoryAnswer: 1};
            if (nextProps.followUpCategoryData) {
                followUpCategoryData = nextProps.followUpCategoryData;
            }
            this.setState({followUpCategoryData: followUpCategoryData, categoryTitle: categoryTitle, curCategoryIndex: curCategoryIndex, curPageIndex: curPageIndex});
        }
    }

    handleOpenFollowUpDialog() {
        let followUpCategoryData = this.state.followUpCategoryData || {isFollowUpCategory: false, followUpCategoryId: '', followUpCategoryAnswer: 1};
        if(followUpCategoryData && followUpCategoryData.hasOwnProperty('isFollowUpCategory') && followUpCategoryData.isFollowUpCategory === true){
            this.markAsFollowUpCategory('toggleOff');
        } else {
            this.setDialogTableData();
            this.setState({isOpenFollowUpDialog: true});
        }
    }

    handleCloseFollowUpDialog() {
        this.setState({isOpenFollowUpDialog: false});
    }

    handleCloseDeleteDialog() {
        this.setState({openDeleteDialog: false});
    }

    onChangeFollowUpAnswerDropdown(event, index, value) {
        if (value && typeof value === "number") {
            let followUpCategoryData = this.state.followUpCategoryData || {isFollowUpCategory: false, followUpCategoryId: '', followUpCategoryAnswer: 1};
            followUpCategoryData.followUpCategoryAnswer = value;
            this.setState({followUpCategoryData: followUpCategoryData});
        }
    }

    markAsFollowUpCategory(action) {
        let followUpCategoryData = this.state.followUpCategoryData || {isFollowUpCategory: false, followUpCategoryId: null, followUpCategoryAnswer: 1};
        let chosenQuestion = this.state.chosenQuestionObjForFollowUp;
        let fullQuestionnaire = this.props.fullQuestionnaire;

        if(action && action === 'toggleOn'){
            //updating followUpCategoryData
            followUpCategoryData.followUpQuestionText = chosenQuestion.question || '';
            followUpCategoryData.isFollowUpCategory = !followUpCategoryData.isFollowUpCategory;
            followUpCategoryData.followUpCategoryAnswer = followUpCategoryData.followUpCategoryAnswer || 1;
            followUpCategoryData.followUpCategoryId = followUpCategoryData.followUpCategoryId || shortid.generate();

            //updating followUpQuestionDataArr specific in the questionnaire object
            if (chosenQuestion && fullQuestionnaire && fullQuestionnaire.pages) {
                let pages = fullQuestionnaire.pages;

                if (chosenQuestion &&
                    (chosenQuestion.pageIdx || chosenQuestion.pageIdx === 0) &&
                    (chosenQuestion.categoryIdx || chosenQuestion.categoryIdx === 0) &&
                    (chosenQuestion.questionIdx || chosenQuestion.questionIdx === 0)) {
                    let pIdx = chosenQuestion.pageIdx;
                    let cIdx = chosenQuestion.categoryIdx;
                    let qIdx = chosenQuestion.questionIdx;
                    if (pages[pIdx] && pages[pIdx].elements && pages[pIdx].elements[cIdx] &&
                        pages[pIdx].elements[cIdx].elements && pages[pIdx].elements[cIdx].elements[qIdx]) {

                        if (!pages[pIdx].elements[cIdx].elements[qIdx].followUpQuestionDataArr) {
                            pages[pIdx].elements[cIdx].elements[qIdx].followUpQuestionDataArr = [];
                        }
                        pages[pIdx].elements[cIdx].elements[qIdx].followUpQuestionDataArr.push({
                            isFollowUpQuestion: true,
                            followUpCategoryId: followUpCategoryData.followUpCategoryId,
                            followUpCategoryAnswer: followUpCategoryData.followUpCategoryAnswer,
                        });
                    }
                }
            }
            this.setState({followUpCategoryData: followUpCategoryData},
                this.props.markAsFollowUpCategory(followUpCategoryData));
            this.handleCloseFollowUpDialog();

        } else if(action && action === 'toggleOff'){

            if (fullQuestionnaire && fullQuestionnaire.pages) {
                let pages = fullQuestionnaire.pages;
                // Loop through pages.
                for (let k = 0; k < pages.length; k++) {
                    if(pages[k] && pages[k].elements){
                        // Loop through categories.
                        for (let i = 0; i < pages[k].elements.length; i++) {

                            if(pages[k].elements[i] && pages[k].elements[i].elements){
                                // Loop through questions in each category.
                                for (let j = 0; j < pages[k].elements[i].elements.length; j++) {

                                    if(pages[k].elements[i].elements[j] && pages[k].elements[i].elements[j].followUpQuestionDataArr
                                        && pages[k].elements[i].elements[j].followUpQuestionDataArr.length > 0){
                                        let followUpQuestionDataArr = pages[k].elements[i].elements[j].followUpQuestionDataArr;
                                        followUpQuestionDataArr.map((curQuestion, i)=>{
                                            if(curQuestion.followUpCategoryId && followUpCategoryData.followUpCategoryId &&
                                                curQuestion.followUpCategoryId === followUpCategoryData.followUpCategoryId){
                                                followUpQuestionDataArr.splice(i, 1);
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.setState({followUpCategoryData: undefined},
                this.props.markAsFollowUpCategory(undefined)
            );
        }
    }

    handleMenuAction(event, actionVal) {
        if (actionVal && typeof actionVal === 'number') {
            //toggle follow up
            if (actionVal === 1) {
                this.handleOpenFollowUpDialog();
            //delete category action
            } else if (actionVal === 2) {
                this.setState({openDeleteDialog: true});
            }
        }
    }

    handleNextPageClick() {
        let page = this.state.page + 1;
        let rowCount = this.state.rowSize;
        let curDataToShow = this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
            (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage);
        this.setState({
            page: page,
            sortedTableData: curDataToShow
        });

        this.updateUiSelectedRow(curDataToShow, rowCount);
    }

    handlePreviousPageClick() {
        let page = this.state.page - 1;
        let rowCount = this.state.rowSize;
        let curDataToShow = this.state.tableData.slice(page * this.state.rowCountInPage - this.state.rowCountInPage,
            page * this.state.rowCountInPage);
        this.setState({
            page: page,
            sortedTableData: curDataToShow
        });

        this.updateUiSelectedRow(curDataToShow, rowCount);
    }

    handleRowSizeChange(ignore, rowCount) {
        let page = this.state.page;
        if (page > 1 && page > this.state.tableData.length / rowCount) {
            page--;
        }
        let curDataToShow = this.state.tableData.slice((page - 1) * rowCount, (page - 1) * rowCount + rowCount);

        this.setState({
            rowCountInPage: rowCount,
            sortedTableData: curDataToShow,
            rowSize: rowCount,
            page: page
        });

        this.updateUiSelectedRow(curDataToShow, rowCount);
    }

    //curMarkedUiRow - this is the state that is actually show the row in the ui table.
    //curSelectedRow - use to identify the current selected row later when switching pages or amount of items in the table.
    handleRowSelection(rowNum) {
        if(rowNum){
            let sortedTableData = this.state.sortedTableData;
            if(sortedTableData[rowNum]){
                let curQuestionObj = sortedTableData[rowNum];
                this.setState({chosenQuestionObjForFollowUp: curQuestionObj,
                    curMarkedUiRow: rowNum ,
                    curSelectedRow: Number(rowNum[0]) + ((Number(this.state.page) - 1) * Number(this.state.rowSize))});
            }
        }
    }

    handleFilterValueChange(value) {
        // Get labels to search for when fuzzy searching.
        let tableColumns= this.state.tableColumns || [];
        const keys = tableColumns.map((el) => {
            return el['key'];
        });

        // Fuzzy search in tableData objects in every key.
        const searcher = new FuzzySearch(this.state.tableData, keys, {
            caseSensitive: false
        });

        const result = searcher.search(value);

        this.setState({
            sortedTableData: result,
            fuzzyString: value
        });
        let rowCount = this.state.rowSize;

        this.updateUiSelectedRow(result, rowCount,true);

    }

    updateUiSelectedRow(curDataToShow, rowCount, fromSearch = false){
        let curChosenQuestion = this.state.chosenQuestionObjForFollowUp;
        let curSelectedRow = this.state.curSelectedRow;


        if(curChosenQuestion && curDataToShow && curSelectedRow) {
            let isSelectedRowOnThisPage = false;
            let idx = null;
            curDataToShow.map((curQuestion, i)=>{
                if(curQuestion.identifier && curChosenQuestion.identifier && curChosenQuestion.identifier === curQuestion.identifier) {
                    isSelectedRowOnThisPage = true;
                    idx = i ;
                }
            });
            if (fromSearch) {
                this.setState({curMarkedUiRow: isSelectedRowOnThisPage? [idx] : null});
            } else {
                if (Array.isArray(curSelectedRow)) {
                    curSelectedRow = curSelectedRow[0];
                }

                let uiRow = curSelectedRow;
                while (rowCount < uiRow) {
                    uiRow = uiRow - rowCount;
                }

                if (!Array.isArray(uiRow)) {
                    uiRow = [uiRow];
                }
                this.setState({curMarkedUiRow: isSelectedRowOnThisPage? uiRow : null});
            }
        }
    }

    setDialogTableData() {
        let fullQuestionnaire = this.props.fullQuestionnaire;
        let myLang = this.state.lang;
        let isRight = !isLTRLanguage(myLang);
        let curCategoryIndex = this.props.curCategoryIndex;
        let curPageIndex = this.props.curPageIndex;

        const wrappableStyle = {
            whiteSpace: 'pre-wrap',
            wordBreak: 'break-word'
        };

        // Create table column headers
        let tableColumns = [{
            sortable: true,
            label: 'Question',
            key: 'question',
            alignRight: isRight,
            style: wrappableStyle
        }, {
            sortable: true,
            label: 'Category',
            key: 'category',
            alignRight: isRight,
            style: wrappableStyle
        }];

        if (fullQuestionnaire && fullQuestionnaire.pages) {
            let pages = fullQuestionnaire.pages;
            let tableData = [];
            // Loop through pages.
            for (let k = 0; k < pages.length; k++) {

                if(pages[k] && pages[k].elements){
                    // Loop through categories.
                    for (let i = 0; i < pages[k].elements.length; i++) {

                        //check if props exist and we are not showing the questions on the current category we just pressed
                        if(pages[k].elements[i] && pages[k].elements[i].elements && !(i === curCategoryIndex && k === curPageIndex)){
                            // Loop through questions in each category.
                            for (let j = 0; j < pages[k].elements[i].elements.length; j++) {
                                if(pages[k].elements[i].name && pages[k].elements[i].elements[j] && pages[k].elements[i].elements[j].name){
                                    //if we found the elements of category and question in the questionnaire object we add them to the table data
                                    tableData.push({category: pages[k].elements[i].name, question: pages[k].elements[i].elements[j].name,
                                        pageIdx: k, categoryIdx: i, questionIdx: j, identifier: shortid.generate()});
                                }
                            }
                        }
                    }
                }
            }

            this.setState({
                tableColumns: tableColumns,
                totalRowCount: tableData.length,
                tableData: tableData,
                sortedTableData: tableData.slice(0, this.state.rowCountInPage)
            });
        }
    }

    saveQuestion(question, i) {
        this.props.saveQuestion(question, i);
    }

    deleteQuestion(questIdx) {
        this.props.deleteQuestion(questIdx);
    }

    markQuestionForSummary(questIndex, isChecked) {
        this.props.markQuestionForSummary(questIndex, isChecked);
    }

    markAsNoGoQuestion(questIndex, noGoQuestionData) {
        this.props.markAsNoGoQuestion(questIndex, noGoQuestionData);
    }

    saveScoreWeight(score, i) {
        this.props.saveScoreWeight(score, i);
    }

    saveCategoryName(event) {
        this.props.saveCategoryName(event.target.value);
    }

    changeCategoryName(event) {
        this.setState({categoryTitle: event.target.value});
    }


    render() {
        let questions = this.props.questions.map((question, i) => {
            return <QuestionListItem key={i}
                index={i}
                question={question}
                weight={question.weight}
                questionCount={this.props.questionCount}
                saveQuestion={this.saveQuestion.bind(this, i)}
                deleteQuestion={this.deleteQuestion.bind(this, i)}
                markQuestionForSummary={this.markQuestionForSummary.bind(this, i)}
                markAsNoGoQuestion={this.markAsNoGoQuestion.bind(this, i)}
                saveScoreWeight={this.saveScoreWeight.bind(this, i)}
                loadedLang={this.props.loadedLang}
            />;
        });


        let myLang = this.state.lang;
        let myDir = (isLTRLanguage(myLang)) ? LTR : RTL;
        let isLTR = isLTRLanguage(myLang);

        let deleteAlign;
        if (isLTR) {
            deleteAlign = 'right';
        } else {
            deleteAlign = 'left';
        }

        let style = {
            cardStyle: {
                margin: '25px 10px 25px 10px',
                padding: '0px 20px 0px 20px'
            },
            categoryDelete: {
                'float': deleteAlign,
                'marginTop': '10px',
            },
            categoryFollowUp: {
                width: '48px',
                margin: '15px 17px 0 17px'
            },
            categoryTitle: {
                'fontSize': '24px',
                margin: '15px 10px 0 10px'
            },
            categoryTitleTextField: {
                'fontSize': '20px'
            },
            iconField: {
                margin: '0px',
                padding: '12px',
                height: '48px',
                width: '48px',
                cursor: 'pointer'
            },
            tableBodyStyle: {'overflowX': 'auto'}
        };

        let followUpQuestionTextForToolTip = this.state.followUpCategoryData && this.state.followUpCategoryData.followUpQuestionText?
            this.state.followUpCategoryData.followUpQuestionText : '';
        let followUpCategoryData = this.state.followUpCategoryData || {isFollowUpCategory: false, followUpCategoryId: '', followUpCategoryAnswer: 1};
        let sideMenuFollowUpText = followUpCategoryData && !followUpCategoryData.isFollowUpCategory? t[myLang]['Follow_Up_CheckBox_dropdown'] : t[myLang]['Follow_Up_CheckBox_dropdown_remove'] ;

        let followUpDialog = <div dir={myDir}><Dialog
            title={t[myLang]['Follow_Up_Category_title']}
            modal={true}
            autoScrollBodyContent={true}
            open={this.state.isOpenFollowUpDialog || false}
            contentStyle={{maxWidth: '60%', borderRadius: '7px 7px 7px 7px'}}
            actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
            bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
            //tooltip={t[myLang]['Survey_Editor_Follow_Up_Checkbox']}
            titleStyle={{
                fontSize: 18, background: 'rgba(0,0,0,0.7)', color: 'white', textAlign: 'center',
                borderRadius: '2px 2px 0px 0px', textTransform: 'uppercase',
            }}
            actions={[
                <FlatButton
                    label={t[myLang]['Survey_Save']}
                    primary={true}
                    keyboardFocused={false}
                    onTouchTap={this.markAsFollowUpCategory.bind(this, 'toggleOn')}
                    style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                    backgroundColor={'#0091ea'}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                />,
                <FlatButton
                    label={t[myLang]['Survey_Cancel']}
                    primary={true}
                    keyboardFocused={true}
                    onTouchTap={this.handleCloseFollowUpDialog.bind(this)}
                    style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                    backgroundColor={'#0091ea'}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                />
            ]}>
            <br/>
            <br/>
            <div  dir={myDir} style={{margin: 20}}>{t[myLang]['Follow_Up_Dialog_first_step']}</div>
            <Card>
            <DataTables
                height={'auto'}
                selectable={true}
                showRowHover={true}
                showHeaderToolbar={true}
                tableBodyStyle={style.tableBodyStyle}
                columns={this.state.tableColumns}
                data={this.state.sortedTableData}
                showCheckboxes={false}
                selectedRows={this.state.curMarkedUiRow || null}
                onRowSelection={this.handleRowSelection.bind(this)}
                onFilterValueChange={this.handleFilterValueChange.bind(this)}
                onNextPageClick={this.handleNextPageClick.bind(this)}
                onRowSizeChange={this.handleRowSizeChange.bind(this)}
                onPreviousPageClick={this.handlePreviousPageClick.bind(this)}
                headerToolbarMode={'filter'}
                page={this.state.page}
                rowSize={this.state.rowSize}
                count={this.state.totalRowCount}/>
            </Card>
            <br/>
            <br/>
            <div dir={myDir}>
                <div style={{margin: 20}}>{t[myLang]['Follow_Up_Dialog_second_step']}</div>
                <Card style={{padding: 20}}>
                <DropDownMenu
                    value={followUpCategoryData.followUpCategoryAnswer || 1}
                    onChange={this.onChangeFollowUpAnswerDropdown.bind(this)}
                    autoWidth={false}
                    style={{width: '300px'}}>
                    <MenuItem value={1} primaryText="Comply"/>
                    <MenuItem value={2} primaryText="Non-Comply"/>
                    <MenuItem value={3} primaryText="Other"/>
                </DropDownMenu>
                </Card>
            </div>
            <InformationHelpText
                dir={myDir}
                aboveSpace={'30px'}
                explanationHeader={t[myLang]['Survey_Editor_Follow_Up_Explanation_Header']}
                explanationText={t[myLang]['Survey_Editor_Follow_Up_Explanation']}/>
        </Dialog>
        </div>;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <Card style={style.cardStyle}>
                    <div>
                        <IconMenu
                            iconButtonElement={<IconButton><MoreVertIcon/></IconButton>}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            style={style.categoryDelete}
                            value={0}
                            onChange={this.handleMenuAction.bind(this)}>
                            <MenuItem leftIcon={<FollowUpQuestionIcon/>} value={1} primaryText={sideMenuFollowUpText}/>
                            <MenuItem leftIcon={<ActionDelete/>} value={2} primaryText={t[myLang]['Template_Delete_Category']}/>
                        </IconMenu>
                        <CardText style={{display: 'inline-flex'}}>
                            <span style={style.categoryTitle}>{t[myLang]['Template_Category_Title']}: </span>
                            <TextField
                                id="c-text-field-Header"
                                style={style.categoryTitleTextField}
                                value={this.state.categoryTitle}
                                onBlur={this.saveCategoryName.bind(this)}
                                onChange={this.changeCategoryName.bind(this)}
                            />
                            {!!followUpCategoryData.isFollowUpCategory ? <div style={style.categoryFollowUp}>
                                <img width={30} height={30} src={'/svg/nested_questions_icon.png'} alt=''
                                     data-tip={t[myLang]['Survey_Editor_Follow_Up_tooltip'] + followUpQuestionTextForToolTip}
                                     data-for={'Follow_Up_Category'}/>
                                <ReactTooltip id='Follow_Up_Category' place="bottom" effect="solid"/>

                            </div>: ''}
                        </CardText>
                    </div>
                    <CardText>
                        {questions}
                        <FlatButton label={t[this.state.lang]['Template_Add_Question']}
                            onTouchTap={this.addQuestion.bind(this, {text: t[this.state.lang]['Template_Init_Question']})}
                            secondary={true}/>
                    </CardText>
                    {followUpDialog}
                    <DeleteDialog
                        open={this.state.openDeleteDialog}
                        onRequestClose={this.handleCloseDeleteDialog.bind(this)}
                        onDelete={() => {
                            this.props.deleteCategory();
                            this.markAsFollowUpCategory.bind(this,'toggleOff');
                        }}/>
                </Card>
            </MuiThemeProvider>
        );
    }
}
