import React from 'react';
import QuestionsSubCategory from './survey-subcategory.jsx';
import FlatButton from 'material-ui/FlatButton';
import {Card, CardText} from 'material-ui/Card';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import TextField from 'material-ui/TextField';
import {MY_LANG, DEFAULT_LANG} from '../../../../config/consts';
import t from '../../../../config/i18n';
import classNames from 'classnames';
import {isLTRLanguage} from '../../common/AppHelpers';
import '../../../../public/css/survey.css';
import themeForFont from "../../app/themes/themeForFont";

export default class QuestionnaireTab extends React.Component {
    constructor(props) {
        super(props);

        let myLang = this.props.loadedLang || localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        this.state = {
            pageTitleValue: '',
            lang: myLang,
            fullQuestionnaire: this.props.fullQuestionnaire,
            pageIndex: this.props.pageIndex
        };

        this.handleTabChange = this.handleTabChange.bind(this);
        this.addQuestion = this.addQuestion.bind(this);
        this.saveQuestion = this.saveQuestion.bind(this);
    }

    handleTabChange(value) {
        this.setState({
            value: value,
            questionList: []
        });
    };

    addQuestion(i, questionData) {
        this.props.page.elements[i].elements.push(questionData);
        let page = this.props.page;
        this.props.addQuestion(page);
    };

    addCategory(i) {
        this.props.addCategory(i);
    }

    addPage() {
        this.props.addPage();
    }

    deletePage() {
        this.props.deletePage();
    }

    saveQuestion(categoryIndex, questionIndex, question) {
        this.props.page.elements[categoryIndex].elements[questionIndex].name = question;
        let page = this.props.page;

        this.props.saveQuestion(page);
    }

    saveScoreWeight(categoryIndex, questionIndex, newWeight) {
        this.props.saveScoreWeight(categoryIndex, questionIndex, newWeight);
    }

    deleteQuestion(ctgIdx, questIdx) {
        this.props.deleteQuestion(ctgIdx, questIdx);
    }

    markQuestionForSummary(categoryIndex, questIndex, isChecked) {
        this.props.markQuestionForSummary(categoryIndex, questIndex, isChecked);
    }

    markAsNoGoQuestion(categoryIndex, questIndex, noGoQuestionData) {
        this.props.markAsNoGoQuestion(categoryIndex, questIndex, noGoQuestionData);
    }

    markAsFollowUpCategory(categoryIndex, followUpCategoryData) {
        this.props.markAsFollowUpCategory(categoryIndex, followUpCategoryData);
    }

    saveCategoryName(categoryIndex, categoryName) {
        this.props.saveCategoryName(categoryIndex, categoryName);
    }

    deleteCategory(ctgIdx) {
        this.props.deleteCategory(ctgIdx);
    }

    handleTextFieldBlur(event) {
        this.props.saveTabName(event.target.value);
    }

    handleTextFieldChange(event) {
        this.setState({
            pageTitleValue: event.target.value
        });

        this.props.changeTabName(event.target.value);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps) {
            if (nextProps.page && nextProps.page.name) {
                this.setState({
                    pageTitleValue: nextProps.page.name,
                });
            } else{
                this.setState({
                    pageTitleValue: ''
                });
            }

            if (nextProps.pageIndex || nextProps.pageIndex === 0) {
                this.setState({
                    pageIndex: nextProps.pageIndex,
                });
            } else{
                this.setState({
                    pageIndex: 0
                });
            }
        }
    }

    render() {
        let isLTR = (isLTRLanguage(this.state.lang));

        let pageDeleteClass = classNames({
            'pageDeleteLTR': isLTR,
            'pageDeleteRTL': !isLTR
        });

        let style = {
            cardStyle: {
                margin: '25px 10px 25px 10px',
                padding: '0px 20px 0px 20px'
            },
            pageTitle: {
                'fontSize': '24px'
            },
            pageTitleTextField: {
                'fontSize': '20px'
            }

        };
        let subCategories = <div></div>;

        if (this.props.page && this.props.page.elements) {
            subCategories = this.props.page.elements.map((subCategory, categoryIndex) => {
                return (
                    <QuestionsSubCategory addQuestion={this.addQuestion.bind(this, categoryIndex)}
                                          questions={subCategory.elements}
                                          name={subCategory.name}
                                          key={categoryIndex}
                                          curCategoryIndex={categoryIndex}
                                          curPageIndex={this.state.pageIndex}
                                          questionCount={this.props.questionCount}
                                          saveQuestion={this.saveQuestion.bind(this, categoryIndex)}
                                          deleteQuestion={this.deleteQuestion.bind(this, categoryIndex)}
                                          markQuestionForSummary={this.markQuestionForSummary.bind(this, categoryIndex)}
                                          markAsNoGoQuestion={this.markAsNoGoQuestion.bind(this, categoryIndex)}
                                          saveScoreWeight={this.saveScoreWeight.bind(this, categoryIndex)}
                                          saveCategoryName={this.saveCategoryName.bind(this, categoryIndex)}
                                          deleteCategory={this.deleteCategory.bind(this, categoryIndex)}
                                          loadedLang={this.props.loadedLang}
                                          followUpCategoryData={subCategory.followUpCategoryData}
                                          markAsFollowUpCategory={this.markAsFollowUpCategory.bind(this, categoryIndex)}
                                          fullQuestionnaire={this.props.fullQuestionnaire}
                    />
                );
            });


        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <FlatButton
                        secondary={true}
                        label={t[this.state.lang]['Template_Add_Page']}
                        onTouchTap={this.addPage.bind(this)}
                    />
                    <FlatButton
                        secondary={true}
                        label={t[this.state.lang]['Template_Add_Category']}
                        onTouchTap={this.addCategory.bind(this)}
                    />
                    <FlatButton
                        secondary={true}
                        label={t[this.state.lang]['Template_Delete_Page']}
                        className={pageDeleteClass}
                        onTouchTap={this.deletePage.bind(this)}
                    />
                    <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)} >
                        <Card style={style.cardStyle}>
                            <CardText>
                                <span style={style.pageTitle}>{t[this.state.lang]['Template_Page_Title']}:  </span>
                                <TextField
                                    id="text-field-Header"
                                    style={style.pageTitleTextField}
                                    value={ this.state.pageTitleValue.toUpperCase() || this.props.page.name.toUpperCase()}
                                    onBlur={this.handleTextFieldBlur.bind(this)}
                                    onChange={this.handleTextFieldChange.bind(this)}
                                />
                            </CardText>
                        </Card>
                    </MuiThemeProvider>
                    {subCategories}
                </form>
            </div>
        );
        }else {
            return null;
        }
    }
}
