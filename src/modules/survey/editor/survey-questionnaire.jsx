import React from 'react';
import QuestionnaireTab from './survey-tab.jsx';
import Tabs from 'material-scrollable-tabs-build/build/Tabs/Tabs.js';
import Tab from 'material-scrollable-tabs-build/build/Tabs/Tab.js';
import TextField from 'material-ui/TextField';
import questionnaireInitObj from '../../../mock/init-survey';
import questionnaireInitHeObj from '../../../mock/init-survey-he';
import {deleteIndexInArray} from '../common/survey-helpers';
import {LTR, RTL, MY_DIR, MY_LANG, DEFAULT_LANG, DEFAULT_DIR, COMPANY_CONTACT_TYPES} from '../../../../config/consts';
import t from '../../../../config/i18n';
import {isLTRLanguage} from '../../common/AppHelpers.js';
import '../../../../public/css/survey.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import themeForFont from '../../app/themes/themeForFont';
import IconButton from 'material-ui/IconButton';
import RightArrowIcon from 'material-ui/svg-icons/navigation/chevron-right';
import LeftArrowIcon from 'material-ui/svg-icons/navigation/chevron-left';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import InformationHelpText from "../../generalComponents/information-help-text.jsx";


const moment = require('moment');

export default class QuestionnaireComponent extends React.Component {
    constructor(props) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }

        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.state = {
            questionnaire: {},
            questionCount: 0,
            noGoQuestionCount: 0,
            lang: myLang,
            dir: myDir,
            loadedLang: undefined,
            templateTitleText: '',
            value: 0,
            openPreferencesDialog: false,
            preferences: {minNoGoToAnswerNum: 0}
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.addCategory = this.addCategory.bind(this);
        this.addPage = this.addPage.bind(this);
        this.deletePage = this.deletePage.bind(this);
        this.saveSurvey = this.saveSurvey.bind(this);
        this.getSurvey = this.getSurvey.bind(this);
        this.handleNextTab = this.handleNextTab.bind(this);
        this.handlePreviousTab = this.handlePreviousTab.bind(this);
        this.handleActive = this.handleActive.bind(this);
    }

    componentWillMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }
        // If sid was received as query parameter then try to get survey from the server.
        if (this.props.sid) {
            this.getSurvey(user);
        }

        app.setFlagVisibile(true);
    }

    componentDidUpdate() {
        if (this.state && this.state.questionnaire && this.state.questionnaire.sid) {
            this.props.stopLoader();
        }
    }

    saveTitle(event) {
        let questionnaire = this.state.questionnaire;
        if (questionnaire.name !== event.target.value) {
            questionnaire.name = event.target.value;
            // setState was removed since no re-render is required. state is saved alone by upper rows.
            this.saveSurvey();
        }
    }

    changeTitle(event) {
        this.setState({templateTitleText: event.target.value});
    }

    getSurvey(user) {
        $.ajax({
            type: 'POST',
            url: '/api/getSurvey',
            data: JSON.stringify({
                uid: user.id,
                sid: this.props.sid
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data && data.length > 0 && data[0]) {
                let questionCount = this.countNumberOfQuestions(data[0], false);
                let noGoQuestionCount = this.countNumberOfQuestions(data[0], true);
                let loadedLang = data[0].lang;

                this.state.loadedLang = loadedLang;
                this.props.newLoadedLang(loadedLang);

                this.setState({
                    questionnaire: data[0],
                    questionCount: questionCount,
                    noGoQuestionCount: noGoQuestionCount || 0,
                    templateTitleText: data[0].name
                });
            } else {
                let questionnaireObj;

                if (isLTRLanguage(this.state.lang)) {
                    questionnaireObj = questionnaireInitObj;
                } else {
                    questionnaireObj = questionnaireInitHeObj;
                }

                questionnaireObj.sid = this.props.sid;
                questionnaireObj.createDate = moment().format();

                this.setState({
                    questionnaire: questionnaireObj,
                    templateTitleText: questionnaireObj.name
                });
            }
        }).fail(() => {
            app.addAlert('error', t[this.state.lang]['Template_getSurvey_Error']);
        });
    }

    saveSurvey(isUpdatingPreferences = false) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let survey = this.state.questionnaire;
            survey.lang = this.state.loadedLang || this.state.lang;

            $.ajax({
                type: 'POST',
                url: '/api/saveSurvey',
                data: JSON.stringify(survey),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done(() => {
                console.log('done');
                if(isUpdatingPreferences){
                    app.addAlert('success', t[this.state.lang]['Template_updatePreferences']);
                }
            }).fail((e, b, x) => {
                console.log(e, b, x);
                app.addAlert('error', t[this.state.lang]['Template_saveSurvey_Error']);
            });
        }
    }

    addCategory(i) {
        let category = this.state.questionnaire.pages[i].elements;
        category.push({
            type: 'panel',
            elements: [],
            name: t[this.state.lang]['Template_New_Category']
        });

        // forceUpdate is used here just for re-render since a new Component is added by code.
        this.forceUpdate(this.saveSurvey);
    }

    addPage() {
        let pages = this.state.questionnaire.pages;
        pages.push({
            elements: [],
            name: t[this.state.lang]['Template_New_Page']
        });

        let newPageValue = pages.length - 1;

        // setState is used here just for re-render since a new Component is added by code.
        this.setState({value: newPageValue}, this.saveSurvey);

        this.addCategory(newPageValue);
    }

    deletePage(idx) {
        let pages = this.state.questionnaire.pages;

        // At least one page is required. If the current page is deleted - change the tabIndex.
        if (deleteIndexInArray(pages, idx, 'page')) {
            if (idx !== 0) {
                idx = idx - 1;
            }

            this.setState({value: idx}, this.saveSurvey);
        }
    }

    saveQuestion(i, pageObject) {
        let pages = this.state.questionnaire.pages;
        pages[i] = pageObject;

        // Each question added saves the survey to the DB.
        this.forceUpdate(this.saveSurvey);
    }

    deleteQuestion(pageIndex, categoryIndex, questIdx) {
        let questions = this.state.questionnaire.pages[pageIndex].elements[categoryIndex].elements;

        // If the question is deleted - re-render and save to DB.
        if (deleteIndexInArray(questions, questIdx, 'question')) {
            this.forceUpdate(this.saveSurvey);
        }
    }

    markQuestionForSummary(pageIndex, categoryIndex, questIndex, isChecked) {
        let changedQuestion = this.state.questionnaire.pages[pageIndex].elements[categoryIndex].elements[questIndex];
        changedQuestion.markedAsImportant = isChecked;

        this.saveSurvey();
    }

    markAsNoGoQuestion(pageIndex, categoryIndex, questIndex, noGoQuestionData) {
        let questionnaire = this.state.questionnaire;
        let preferences = this.state.preferences;
        let changedQuestion = this.state.questionnaire.pages[pageIndex].elements[categoryIndex].elements[questIndex];
        changedQuestion.noGoQuestionData = noGoQuestionData;
        let noGoQuestionCount = this.countNumberOfQuestions(questionnaire, true) || 0;
        this.setState({noGoQuestionCount: noGoQuestionCount});

        if(preferences && preferences.minNoGoToAnswerNum > noGoQuestionCount){
            preferences.minNoGoToAnswerNum = noGoQuestionCount;
            this.setState({preferences: preferences});
            app.addAlert('warning', 'Minimum number of required critical questions has been updated!');
        }
        this.saveSurvey();
    }

    markAsFollowUpCategory(pageIndex, categoryIndex, followUpCategoryData) {
        let changedCategory = this.state.questionnaire.pages[pageIndex].elements[categoryIndex];
        changedCategory.followUpCategoryData = followUpCategoryData;
        this.saveSurvey();
    }

    saveCategoryName(pageIndex, categoryIndex, categoryName) {
        let category = this.state.questionnaire.pages[pageIndex].elements[categoryIndex];

        if (category.name !== categoryName) {
            category.name = categoryName;

            this.saveSurvey();
        }
    }

    deleteCategory(pageIdx, ctgIdx) {
        let pageElements = this.state.questionnaire.pages[pageIdx].elements;

        // At least one category is required. If the current category is deleted - re-render and save to DB.
        if (deleteIndexInArray(pageElements, ctgIdx, 'category')) {
            this.forceUpdate(this.saveSurvey);
        }
    }

    onSubmit(event) {
        event.preventDefault();
    }

    // This function is called for each char changed in TabName-TextField component.
    changeTabName(pageIndex, newPageName) {
        // Save the new pageName (tabName) on state.
        let page = this.state.questionnaire.pages[pageIndex];
        page.name = newPageName;
        // The forceUpdate is called for the tabName changing by the pageTitle.
        this.forceUpdate();
    }

    // This function is called only when focus is lost from TabName-TextField component.
    saveTabName() {
        // No need of mutating state again, changeTabName does that already on each change.
        // Just save to DB.
        this.saveSurvey();
    }

    saveScoreWeight(pageIndex, categoryIndex, questionIndex, newWeight) {
        let changedQuestion = this.state.questionnaire.pages[pageIndex].elements[categoryIndex].elements[questionIndex];
        changedQuestion.weight = newWeight;

        this.saveSurvey();
    }

    countNumberOfQuestions(data, isNoGoQuestionCount = false) {
        let questionsCount = 0;
        let pages;
        // If argument is survey object then count the # of questions in it, if not, use state.
        pages = !data ? this.state.questionnaire.pages : data.pages;

        // Loop through pages.
        for (let k = 0; k < pages.length; k++) {
            // Loop through categories.
            for (let i = 0; i < pages[k].elements.length; i++) {
                // Loop through questions in each category.
                for (let j = 0; j < pages[k].elements[i].elements.length; j++) {
                    if (isNoGoQuestionCount) {
                        if (pages[k].elements[i].elements[j].noGoQuestionData && pages[k].elements[i].elements[j].noGoQuestionData.isNoGoQuestion) {
                            questionsCount++;
                        }
                    } else {
                        questionsCount++;
                    }
                }
            }
        }

        return questionsCount;
    }

    handleActive(value) {
        this.setState({value: value});
    }

    handleNextTab() {
        let value = this.state.value;
        if (value !== (this.state.questionnaire.pages.length - 1)) {
            value = value + 1;
            this.setState({value: value});
        }
    }

    handlePreviousTab() {
        let value = this.state.value;
        if (value !== 0) {
            value = value - 1;
            this.setState({value: value});
        }
    }

    handleClosePreferencesDialog() {
        this.setState({openPreferencesDialog: false});
    }


    handleOpenPreferencesDialog() {
        let preferences = {minNoGoToAnswerNum: 0};
        let questionnaire = this.state.questionnaire;
        if (questionnaire && questionnaire.preferences) {
            preferences = questionnaire.preferences;
        }
        this.setState({preferences: preferences, openPreferencesDialog: true})
    }

    updatePreferences() {
        let questionCount = this.state.questionCount || 0;
        let preferences = this.state.preferences || {minNoGoToAnswerNum: 0};
        let questionnaire = this.state.questionnaire;
        let noGoQuestionCount = this.state.noGoQuestionCount || 0;

        if(preferences.minNoGoToAnswerNum){
            //in case 'minNoGoToAnswerNum' is string and have zeros on the beginning like -> 0002
            //we remove the zeros and convert to int
            preferences.minNoGoToAnswerNum = Number(preferences.minNoGoToAnswerNum.toString().replace(/^0+/, ''));
        }

        if (noGoQuestionCount === 0) {
            app.addAlert('error', 'No "No Go" questions was found on the survey. please add then and then choose.');
        } else if (noGoQuestionCount >= 0) {
            if (typeof preferences.minNoGoToAnswerNum === 'number') {
                if (preferences.minNoGoToAnswerNum >= 0 && preferences.minNoGoToAnswerNum <= noGoQuestionCount && preferences.minNoGoToAnswerNum <= questionCount) {
                    if (questionnaire) {
                        questionnaire.preferences = preferences;
                        this.setState({questionnaire: questionnaire});
                        this.saveSurvey(true);
                        this.handleClosePreferencesDialog();
                    }
                } else {
                    app.addAlert('error', 'Failed to update Preferences: Please enter minimum critical questions number between 0 to ' + noGoQuestionCount + '!');
                }
            } else {
                app.addAlert('error', 'Failed to update Preferences: Please enter minimum critical questions number between 0 to ' + noGoQuestionCount + '!');
            }
        }
    }

    handleChangeNoGoQuestionsNumField(event) {
        if(event && event.target && event.target.value){
            let preferences = this.state.preferences || {minNoGoToAnswerNum: 0};
            let noGoQuestionCount = this.state.noGoQuestionCount || 0;
            if(event.target.value <= noGoQuestionCount && event.target.value >= 0){
                preferences.minNoGoToAnswerNum = event.target.value;
                this.setState({preferences: preferences})
            } else {
                app.addAlert('error', 'Failed to update Preferences: Please enter minimum critical questions number between 0 to ' + noGoQuestionCount + '!');
            }
        } else {
            app.addAlert('error', 'Failed to update critical questions number!');
        }
    }

    render() {
        if (this.state.questionnaire && this.state.questionnaire.pages) {
            let preferences = this.state.preferences || {minNoGoToAnswerNum: 0};

            let curQuestionnairePage = {};
            let value = this.state.value || 0;
            if(this.state.questionnaire.pages[value]){
                curQuestionnairePage = this.state.questionnaire.pages[value];
            }

            let tabs = this.state.questionnaire.pages.map((questionnairePage, i) => {
                return (
                    <Tab isMultiLine={true} onActive={this.handleActive.bind(this, i)} label={questionnairePage.name}
                         value={i} key={i}>

                    </Tab>

                );
            });

            let myLang = this.state.loadedLang || this.state.lang;

            let myDir = (isLTRLanguage(myLang)) ? LTR : RTL;

            let templateTitleDiv = {
                padding: '16px',
                margin: '20px 0px 30px 0px'
            };
            let templateTitle = {
                fontSize: '26px',
                margin: 15
            };
            let templateTitleTextField = {
                fontSize: '22px'
            };

            let tabStyle = {};
            let PreferencesIconStyle = myDir === LTR? {float: 'right', right: 40, bottom: 10} : {float: 'left', left: 40, bottom: 10};

            return (
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <div dir={myDir}>
                        <IconButton
                            onTouchTap={this.handleOpenPreferencesDialog.bind(this)}
                            style={PreferencesIconStyle}>
                            <SettingsIcon/>
                        </IconButton>
                        <div style={templateTitleDiv}>
                            <span style={templateTitle}>{t[myLang]['Template_Title']}: </span>
                            <TextField
                                id="t-text-field-Header"
                                style={templateTitleTextField}
                                value={this.state.templateTitleText}
                                onBlur={this.saveTitle.bind(this)}
                                onChange={this.changeTitle.bind(this)}
                            />
                        </div>
                            <Tabs
                                value={this.state.value}
                            onChange={value => this.setState({value: value})}
                                tabItemContainerStyle={tabStyle}
                            tabType={'scrollable-buttons'}>
                            {tabs}
                        </Tabs>
                        <QuestionnaireTab saveQuestion={this.saveQuestion.bind(this, value)}
                                          addPage={this.addPage.bind(this, value)}
                                          deletePage={this.deletePage.bind(this, value)}
                                          addCategory={this.addCategory.bind(this, value)}
                                          addQuestion={this.saveQuestion.bind(this, value)}
                                          questionCount={this.state.questionCount}
                                          saveScoreWeight={this.saveScoreWeight.bind(this, value)}
                                          deleteQuestion={this.deleteQuestion.bind(this, value)}
                                          markQuestionForSummary={this.markQuestionForSummary.bind(this, value)}
                                          markAsNoGoQuestion={this.markAsNoGoQuestion.bind(this, value)}
                                          markAsFollowUpCategory={this.markAsFollowUpCategory.bind(this, value)}
                                          saveCategoryName={this.saveCategoryName.bind(this, value)}
                                          deleteCategory={this.deleteCategory.bind(this, value)}
                                          changeTabName={this.changeTabName.bind(this, value)}
                                          saveTabName={this.saveTabName.bind(this, value)}
                                          pageIndex={value}
                                          page={curQuestionnairePage}
                                          fullQuestionnaire={this.state.questionnaire}
                                          loadedLang={this.state.loadedLang}/>
                        <div style={{margin: 10}}>
                        <FlatButton style={{boxShadow: "2px 2px 2px 2px #e0e0e0", radius: '2px', margin: 2.5}}
                                    secondary={true}
                                    disabled={this.state.value === 0}
                                    labelPosition={"after"}
                                    icon={myDir === LTR ? <LeftArrowIcon/> : <RightArrowIcon/>}
                                    label={t[myLang]['Survey_Back']}
                                    labelStyle={{textTransform: 'none'}}
                                    onClick={this.handlePreviousTab.bind(this)}/>
                        <FlatButton style={{boxShadow: "2px 2px 2px 2px #e0e0e0",radius: '2px', margin: 2.5}}
                                    label={t[myLang]['Survey_Next']}
                                    labelPosition={"before"}
                                    icon={myDir === LTR ? <RightArrowIcon/> : <LeftArrowIcon/> }
                                    labelStyle={{textTransform: 'none'}}
                                    secondary={true}
                                    disabled={this.state.value === this.state.questionnaire.pages.length - 1}
                                    onClick={this.handleNextTab.bind(this)}/>
                        </div>
                        <div dir={myDir}>
                            <Dialog
                                title={myDir === LTR ? "Preferences" : 'הגדרות'}
                                modal={true}
                                autoScrollBodyContent={true}
                                open={this.state.openPreferencesDialog || false}
                                contentStyle={{maxWidth: '38%', borderRadius: '7px 7px 7px 7px'}}
                                actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                                bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                                titleStyle={{
                                    fontSize: 18, background: 'rgba(0,0,0,0.7)', color: 'white', textAlign: 'center',
                                    borderRadius: '2px 2px 0px 0px', textTransform: 'uppercase',
                                }}
                                actions={[
                                    <FlatButton
                                        label={t[myLang]['Survey_Save']}
                                        primary={true}
                                        keyboardFocused={false}
                                        onTouchTap={this.updatePreferences.bind(this)}
                                        style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                                        backgroundColor={'#0091ea'}
                                        hoverColor={'#12a4ff'}
                                        rippleColor={'white'}
                                        labelStyle={{fontSize: 10}}
                                    />,
                                    <FlatButton
                                        label={t[myLang]['Survey_Cancel']}
                                        primary={true}
                                        keyboardFocused={true}
                                        onTouchTap={this.handleClosePreferencesDialog.bind(this)}
                                        style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                                        backgroundColor={'#0091ea'}
                                        hoverColor={'#12a4ff'}
                                        rippleColor={'white'}
                                        labelStyle={{fontSize: 10}}
                                    />
                                ]}>
                                <br/>
                                <br/>
                                <div dir={myDir}>
                                    <div>{t[myLang]['No_Go_Questions_amount_text'] + this.state.noGoQuestionCount || 0}</div>
                                    <br/>
                                    <div>{t[myLang]['No_Go_Questions_minimum_text']}</div>
                                    <TextField
                                        id="minNoGoToAnswerNum"
                                        value={preferences.minNoGoToAnswerNum || 0}
                                        onChange={this.handleChangeNoGoQuestionsNumField.bind(this)}/>
                                </div>
                                <InformationHelpText
                                    dir={myDir}
                                    aboveSpace={'100px'}
                                    explanationHeader={t[myLang]['Survey_Editor_No_Go_Explanation_Header']}
                                    explanationText={t[myLang]['Survey_Editor_No_Go_Explanation']}/>
                            </Dialog>
                        </div>
                    </div>
                </MuiThemeProvider>
            );
        } else {
            return null;
        }
    }
}
