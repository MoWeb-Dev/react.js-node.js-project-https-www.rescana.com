import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import React from 'react';
import QuestionnaireComponent from './survey-questionnaire.jsx';
import {browserHistory} from 'react-router';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import DoneIcon from 'material-ui/svg-icons/action/done';
import classNames from 'classnames';
import ReactTooltip from 'react-tooltip';
import {LTR, RTL, MY_LANG, DEFAULT_LANG} from '../../../../config/consts';
import {isLTRLanguage} from '../../common/AppHelpers.js';
import t from '../../../../config/i18n';
import Loader from 'react-loader-advanced';
import '../../../../public/css/survey.css';
import {isImage} from "../common/survey-helpers.js";
import FileIcon from 'material-ui/svg-icons/editor/insert-drive-file';

export default class SurveyEditor extends React.Component {
    constructor() {
        super();

        this.state = {
            value: 10,
            loadedLang: undefined,
            showLoader: true
        };
        this.newLoadedLang = this.newLoadedLang.bind(this);
        this.render = this.render.bind(this);
    }

    newLoadedLang(newLang) {
        this.setState({loadedLang: newLang});
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }
    }

    finishSurvey() {
        let myLang = this.state.loadedLang || localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        app.addAlert('success', t[myLang]['Survey_Editor_Done_success']);
        browserHistory.push({
            pathname: '/survey-wizard'
        });
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    render() {
        let myLang = this.state.loadedLang || localStorage.getItem(MY_LANG) || DEFAULT_LANG;

        let isLTR = isLTRLanguage(myLang);

        let myDir = (isLTR) ? LTR : RTL;

        let floatingDoneBtn = classNames({
            'floatingDoneBtnLTR': isLTR,
            'floatingDoneBtnRTL': !isLTR
        });

        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForSurveys': true
        });

        const spinner = <div className={spinnerClass}/>;

        let sid = '';

        if (this.props.location && this.props.location.query && this.props.location.query.sid) {
            sid = this.props.location.query.sid;
        }

        let
            isRTLObj = {

        };

        if (myDir && myDir === RTL) {
            isRTLObj = {
                isRtl: true,
            };
        }
        return (
            <div className={pageStyleClass}>
                <Loader show={this.state.showLoader} message={spinner}>
                    <MuiThemeProvider muiTheme={getMuiTheme(isRTLObj)}>
                        <div>
                            <QuestionnaireComponent sid={sid} newLoadedLang={this.newLoadedLang}
                                stopLoader={this.stopLoader.bind(this)}
                            />
                            {(this.state.showLoader) ? null : (
                                <div className={floatingDoneBtn}>
                                    <FloatingActionButton
                                        secondary={true}
                                        data-tip={t[myLang]['Survey_Editor_Done_tooltip']}
                                        onTouchTap={this.finishSurvey.bind(this)}
                                        style={{margin: '50px 30px 30px 0px'}}
                                    >
                                        <DoneIcon/>
                                    </FloatingActionButton>
                                    <ReactTooltip place="top"/>
                                </div>
                            )}
                        </div>
                    </MuiThemeProvider>
                </Loader>
            </div>
        );
    }
}
