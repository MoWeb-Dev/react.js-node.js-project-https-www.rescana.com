import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardTitle} from 'material-ui/Card';
import EditButton from 'material-ui/svg-icons/editor/mode-edit';
import AllowedOrgButton from 'material-ui/svg-icons/action/work';
import DataTables from 'material-ui-datatables';
import FlatButton from 'material-ui/FlatButton';
import FuzzySearch from 'fuzzy-search';
import {browserHistory} from 'react-router';
import React from 'react';
import {RTL, MY_LANG, MY_DIR, DEFAULT_LANG, DEFAULT_DIR,ORGANIZATION_ID ,ORGANIZATION_NAME} from '../../../../config/consts';
import {deleteIDFromSurveys} from '../common/survey-helpers.js';
import NewTemplateModal from './new-template-dialog.jsx';
import t from '../../../../config/i18n';
import classNames from 'classnames';
import moment from 'moment';
import '../../../../public/css/survey.css';
import themeForFont from "../../app/themes/themeForFont";
import IconButton from 'material-ui/IconButton';
import Delete from 'material-ui/svg-icons/action/delete';
import DeleteDialog from "../../app/delete-dialog.jsx";
import Dialog from 'material-ui/Dialog';
import AutoComplete from "material-ui/AutoComplete";
import ChipInput from 'material-ui-chip-input';
import DynamicDialog from '../../app/dynamic-dialog.jsx';

const shortid = require('shortid');

export default class SurveyWizard extends React.Component {
    constructor() {
        super();

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.state = {
            tableColumns: [],
            tableData: [],
            sortedTableData: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            page: 1,
            openNewTemplateModal: false,
            lang: myLang,
            dir: myDir,
            openDeleteDialog: false,
            curRowIdToDelete: null,
            curOrgObj: {allowedOrganizations:[], surveyId: 0},
            openEditAllowedUsersDialog: false,
            isFeaturesExplDialogOpen: false
        };

        this.handleOpenDeleteDialog = this.handleOpenDeleteDialog.bind(this);
        this.handleCloseDeleteDialog = this.handleCloseDeleteDialog.bind(this);
        this.handleSortOrderChange = this.handleSortOrderChange.bind(this);
        this.handleFilterValueChange = this.handleFilterValueChange.bind(this);
        this.handleNextPageClick = this.handleNextPageClick.bind(this);
        this.handlePreviousPageClick = this.handlePreviousPageClick.bind(this);
        this.handleRowSizeChange = this.handleRowSizeChange.bind(this);
        this.openExplDialogOpen = this.openExplDialogOpen.bind(this);
        this.getSurveys = this.getSurveys.bind(this);
        this.deleteSurveys = this.deleteSurveys.bind(this);
        this.setResults = this.setResults.bind(this);
        this.deleteSurvey = this.deleteSurvey.bind(this);
    }

    componentDidMount() {
        let user = app.getAuthUser();
        let curOrgId = localStorage.getItem(ORGANIZATION_ID);
        if (!user || user.userRole === 'basic') {
            app.routeAuthUser();
        }
        if(user.userRole === 'admin'){
            this.getAllOrganizations();
        }
        this.getSurveys(user);
    }

    componentWillMount() {
        app.setFlagVisibile(true);
    }

    setResults(data) {
        let isRight = (this.state.dir === RTL);

        const wrappableStyle = {
            whiteSpace: 'pre-wrap',
            wordBreak: 'break-word'
        };

        // Create table column headers
        let tableColumns = [{
            sortable: true,
            label: t[this.state.lang]['Template_Name'],
            key: 'surveyName',
            alignRight: isRight,
            style: wrappableStyle
        }, {
            sortable: true,
            label: t[this.state.lang]['Template_Creation_Date'],
            key: 'createDate',
            alignRight: isRight,
            style: wrappableStyle
        }, {
            label: t[this.state.lang]['Template_Edit'],
            key: 'edit',
            alignRight: isRight
        }, {
            label: t[this.state.lang]['Template_Delete'],
            key: 'delete',
            alignRight: isRight
        }];

        let user = app.getAuthUser();

        if(user.userRole ==="admin"){
            tableColumns[3] = {
                label: t[this.state.lang]['Template_Allowed_Org'],
                key: 'allowedOrganizations',
                alignRight: isRight
            };
            tableColumns[4] = {
                label: t[this.state.lang]['Template_Delete'],
                key: 'delete',
                alignRight: isRight
            };
        }

        let editBtnStyle = {
            cursor: 'pointer'
        };

        let tableData = [];

        for (let i = 0; i < data.length; i++) {
            tableData[i] = {
                surveyName: data[i].name,
                createDate: moment(data[i].createDate).format('YYYY/MM/DD, HH:mm:ss'),
                edit: <EditButton onTouchTap={this.openExplDialogOpen.bind(this, data[i].sid)} style={editBtnStyle}/>,
                allowedOrganizations: <AllowedOrgButton onTouchTap={this.editAllowedOrg.bind(this, data[i])} style={editBtnStyle}/>,
                delete:  <IconButton style={editBtnStyle} onTouchTap={this.handleOpenDeleteDialog.bind(this, data[i].sid)}><Delete/></IconButton>,
                surveyIdentifier: data[i].sid
            };
        }

        this.setState({
            tableColumns: tableColumns,
            totalRowCount: tableData.length,
            tableData: tableData,
            sortedTableData: tableData.slice(0, this.state.rowCountInPage)
        });
    }

    handleOpenDeleteDialog(rowToDelete) {
        this.setState({openDeleteDialog: true, curRowIdToDelete: rowToDelete});
    };

    handleCloseDeleteDialog() {
        this.setState({openDeleteDialog: false, curRowIdToDelete: null});
    };

    handleSortOrderChange(key, order) {
        let tableData = this.state.tableData;
        if (order === 'asc') {
            tableData.sort((a, b) => {
                let textA = a[key].toUpperCase();
                let textB = b[key].toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else {
            tableData.sort((a, b) => {
                let textA = a[key].toUpperCase();
                let textB = b[key].toUpperCase();
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        }

        this.setState({
            sortedTableData: tableData.slice((this.state.page - 1) * this.state.rowCountInPage,
                (this.state.page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handleOpenNewTemplate() {
        this.setState({
            openNewTemplateModal: true
        });
    }

    closeTemplateDialog() {
        this.setState({openNewTemplateModal: false});
    }

    openExplDialogOpen(sid) {
        let newFeaturesFlag = false;
        if (newFeaturesFlag) {
            //save current sid and open new feature explanation dialog
            this.setState({isFeaturesExplDialogOpen: true, curPressedSid: sid});
        } else if (sid) {
            browserHistory.push({
                pathname: '/survey-editor',
                search: '?sid=' + sid
            });
        }
    }

    closeFeaturesExplDialogAndRunSurvey() {
        let sid = this.state.curPressedSid || null;

        let user = app.getAuthUser();
        if (!user) {
            app.routeAuthUser();
        } else {
            if(sid){
                browserHistory.push({
                    pathname: '/survey-editor',
                    search: '?sid=' + sid
                });
                this.setState({curPressedSid: null})
            }
            this.setState({isFeaturesExplDialogOpen: false});
        }
    };

    createSurveyTemplate(newSid, organizations, isUploadedExcel) {
        let sid;
        // If the parameter has value, meaning it's an existing survey template (from ExcelUpload).
        if (newSid) {
            sid = newSid;

        // If the parameter has no value, meaning a new template needs to be made (from manuallyTemplate).
        } else {
            sid = shortid.generate();
        }
        //create Default template so we can save the allowed organizations on creation
        this.createDefaultSurveyTemplate(sid, organizations, isUploadedExcel);

        let newFeaturesFlag = false;
        if(newFeaturesFlag){
            this.setState({isFeaturesExplDialogOpen: true, curPressedSid: sid});
        } else{
            browserHistory.push({
                pathname: '/survey-editor',
                search: '?sid=' + sid
            });
        }
    }

    removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    }

    createDefaultSurveyTemplate(sid, organizations, isUploadedExcel) {
        let user = app.getAuthUser();
        if (!user) {
            app.routeAuthUser();
        } else {
            let survey = {};
            let allUsers = [];
            if(user.userRole === 'admin' && organizations && Array.isArray(organizations) && organizations.length > 0){
                organizations.map((currOrg)=>{
                    allUsers = allUsers.concat(currOrg.orgUsers);
                });
            }
            if(!isUploadedExcel){
                survey.name = 'New Survey Template';
                survey.pages = [
                    {
                        "name" : "New Page",
                        "elements" : [
                            {
                                "type" : "panel",
                                "name" : "New Category",
                                "elements" : [
                                    {
                                        "type" : "radiogroup",
                                        "name" : "Enter a question here",
                                        "choices" : [
                                            "Non Comply",
                                            "Comply",
                                            "Other"
                                        ],
                                        "withText" : {
                                            "name" : "Comments"
                                        },
                                        "weight" : 5,
                                        "markedAsImportant" : false
                                    },
                                    {
                                        "type" : "radiogroup",
                                        "name" : "Enter a fg here",
                                        "choices" : [
                                            "Non Comply",
                                            "Non Comply",
                                            "Other"
                                        ],
                                        "withText" : {
                                            "name" : "Comments"
                                        },
                                        "weight" : 5,
                                        "markedAsImportant" : false
                                    }
                                ]
                            }
                        ]
                    }
                ];
            }
            allUsers.push(user);
            survey.uid = this.removeDuplicates(allUsers, 'id');
            survey.allowedOrganizations = organizations || [];
            survey.sid = sid;
            survey.lang = this.state.lang;
            survey.createDate = moment().format();

            let selectedOrgId = localStorage.getItem(ORGANIZATION_ID);
            if(selectedOrgId){
                survey.selectedOrgId = selectedOrgId;
            }

            $.ajax({
                type: 'POST',
                url: '/api/createDefaultSurvey',
                data: JSON.stringify(survey),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done(() => {
                console.log('done');
            }).fail((e, b, x) => {
                console.log(e, b, x);
                app.addAlert('error', t[this.state.lang]['Template_saveSurvey_Error']);
            });
        }
    }

    getSurveys(user) {
        $.ajax({
            type: 'POST',
            url: '/api/getSurvey',
            data: JSON.stringify({uid: user.id}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data[0]) {
                this.setResults(data);
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to fetch data');
        });
    }
    deleteSurvey(srvSid) {
        let tableData = this.state.tableData;
        deleteIDFromSurveys(tableData, srvSid);

        if (!this.state.fuzzyString) {
            let page = this.state.page;
            if (page > 1 && (page - 1) === tableData.length / this.state.rowCountInPage) {
                page--;
            }

            this.setState({
                tableData: tableData, totalRowCount: tableData.length, page: page,
                sortedTableData: tableData.slice((page - 1) * this.state.rowCountInPage,
                    (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
            });
        } else {
            this.setState({
                tableData: tableData
            }, this.handleFilterValueChange(this.state.fuzzyString));
        }
        this.deleteSurveys(srvSid);
    }

    deleteSurveys(srvSid) {
        $.ajax({
            type: 'POST',
            url: '/api/deleteSurvey',
            data: JSON.stringify({sid: srvSid}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'

        }).done(() => {
            console.log('deleted');
        }).fail(() => {
            app.addAlert('error', 'Error: failed to delete data');
        });
    }

    handleFilterValueChange(value) {
        // Get labels to search for when fuzzy searching.
        const keys = this.state.tableColumns.map((el) => {
            return el['key'];
        });

        // Fuzzy search in tableData objects in every key.
        const searcher = new FuzzySearch(this.state.tableData, keys, {
            caseSensitive: false
        });

        const result = searcher.search(value);

        this.setState({
            sortedTableData: result,
            fuzzyString: value
        });
    }

    handleNextPageClick() {
        let page = this.state.page + 1;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice((page - 1) * this.state.rowCountInPage,
                (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
        });
    }

    handlePreviousPageClick() {
        let page = this.state.page - 1;
        this.setState({
            page: page,
            sortedTableData: this.state.tableData.slice(page * this.state.rowCountInPage - this.state.rowCountInPage,
                page * this.state.rowCountInPage)
        });
    }

    handleRowSizeChange(ignore, rowCount) {
        let page = this.state.page;
        if (page > 1 && page > this.state.tableData.length / rowCount) {
            page--;
        }

        this.setState({
            rowCountInPage: rowCount,
            sortedTableData: this.state.tableData.slice((page - 1) * rowCount, (page - 1) * rowCount + rowCount),
            rowSize: rowCount,
            page: page
        });
    }

    getAllOrganizations() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'get',
                url: '/admin/getAllOrganizations',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.organizations) {
                    let organizations = data.organizations;
                    let organizationsInputList = [];
                    organizations.map((org) => {
                        let orgId = org._id;
                        if(orgId && typeof orgId === 'object'){
                            orgId = orgId.valueOf().toString();
                        }
                        organizationsInputList.push({organizationName: org.organizationName, id: orgId, orgUsers: org.allowedUsers})
                    });
                    this.setState({
                        organizationsInputList: organizationsInputList,
                    });
                } else {
                    if (data && data.error) {
                        console.log(data.error);
                    }
                    app.addAlert('error', 'Error: failed to load users.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load users.');
            });
        }
    }

    editAllowedOrg(data) {
        if (data) {
            let curOrgObj = this.state.curOrgObj;
            curOrgObj.surveyId = data.sid || 0;
            curOrgObj.allowedOrganizations = data.allowedOrganizations || [];
            this.setState({openEditAllowedUsersDialog: true,
                curOrgObj: curOrgObj});
        }
    }

    handleCloseEditAllowedOrgDialog() {
        this.setState({openEditAllowedUsersDialog: false, curOrgObj: {allowedOrganizations: [], surveyId: 0}});
    };

    saveSurveyEditedData() {
        let curOrgObj = this.state.curOrgObj;
        if (curOrgObj && curOrgObj.surveyId) {
            $.ajax({
                type: 'POST',
                url: '/admin/updateSurveyTemplateAllowedOrg',
                data: JSON.stringify(curOrgObj),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((res) => {
                if (res && res.ok) {
                    let organizationsInputList = this.state.organizationsInputList;
                    for (let i = 0; i < organizationsInputList.length; i++) {
                        if (organizationsInputList[i] && organizationsInputList[i].surveyId === curOrgObj.surveyId) {
                            organizationsInputList[i].allowedOrganizations = curOrgObj.allowedOrganizations;
                            break;
                        }
                    }
                    this.setState({
                        organizationsInputList: organizationsInputList
                    });
                    this.handleCloseEditAllowedOrgDialog();
                    app.addAlert('success', 'Updated Survey!');
                } else {
                    app.addAlert('error', 'Error: failed to update survey');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to update survey');
            });
        } else {
            app.addAlert('error', 'Error: failed to update survey');
        }
    }

    handleAddAllowedOrgField(input) {
        let curOrgObj = this.state.curOrgObj;
        let allowedOrganizations = this.state.curOrgObj.allowedOrganizations;
        allowedOrganizations.push(input);
        curOrgObj.allowedOrganizations = allowedOrganizations;
        this.setState({
            curOrgObj: curOrgObj
        });
    }

    handleDeleteAllowedOrgField(value, index) {
        let curOrgObj = this.state.curOrgObj;
        let allowedOrganizations = this.state.curOrgObj.allowedOrganizations;
        allowedOrganizations.splice(index, 1);
        curOrgObj.allowedOrganizations = allowedOrganizations;
        this.setState({
            curOrgObj: curOrgObj
        });
    }

    render() {
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;
        let isRight = (this.state.dir === RTL);
        let user = app.getAuthUser();
        let isAdmin = false;
        if(user && user.userRole){
            isAdmin = user.userRole === "admin";
        }

        let explText = 'Explanation Text Here.';

        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        let tableBodyStyle = {
            'overflowX': 'auto',
            'direction': this.state.dir
        };

        let tableStyle = {
            'width': '95%',
            'margin': 'auto'
        };

        return (
            <div className={pageStyleClass}>
                <div>
                    <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                        <div>
                            <div style={{margin: '40px 0px 40px 10px'}}>
                                <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                                <span style={{
                                    position: 'relative',
                                    top: '-6px',
                                    fontSize: 22
                                }}>{t[this.state.lang]['Templates_Dashboard']}</span>
                            </div>
                            <Card style={{margin: 10}}>
                                <div dir={this.state.dir}>
                                    <FlatButton
                                        style={{
                                            boxShadow: "-4px 6px 6px -4px #e0e0e0",
                                            borderRadius:5,
                                            margin: '35px 25px 25px 35px'}}
                                        secondary={true}
                                        label={t[this.state.lang]['Template_Create_New']}
                                        onTouchTap={this.handleOpenNewTemplate.bind(this)}
                                    />
                                    <NewTemplateModal
                                        open={this.state.openNewTemplateModal}
                                        closeDialog={this.closeTemplateDialog.bind(this)}
                                        loadedLang={this.state.lang}
                                        createSurveyTemplate={this.createSurveyTemplate.bind(this)}
                                        organizationsInputList={this.state.organizationsInputList}
                                    />
                                </div>
                                <DataTables
                                    height={'auto'}
                                    selectable={false}
                                    showRowHover={true}
                                    showHeaderToolbar={true}
                                    tableBodyStyle={tableBodyStyle}
                                    tableStyle={tableStyle}
                                    columns={this.state.tableColumns}
                                    data={this.state.sortedTableData}
                                    showCheckboxes={false}
                                    onCellClick={this.handleCellClick}
                                    onFilterValueChange={this.handleFilterValueChange}
                                    onSortOrderChange={this.handleSortOrderChange}
                                    onNextPageClick={this.handleNextPageClick}
                                    onRowSizeChange={this.handleRowSizeChange}
                                    onPreviousPageClick={this.handlePreviousPageClick}
                                    page={this.state.page}
                                    rowSize={this.state.rowSize}
                                    count={this.state.totalRowCount}
                                />
                                <DeleteDialog
                                    open={this.state.openDeleteDialog}
                                    onRequestClose={this.handleCloseDeleteDialog}
                                    onDelete={() => {
                                        this.deleteSurvey(this.state.curRowIdToDelete);
                                        this.handleCloseDeleteDialog();
                                    }}/>
                            </Card>
                            <Dialog
                                title={t[this.state.lang]['Template_Allowed_Org']}
                                style={{margin: 10}}
                                actions={[
                                    <FlatButton
                                        label={t[this.state.lang]['Survey_Save']}
                                        primary={true}
                                        style={{
                                            color: 'white',
                                            marginBottom: '5px',
                                            marginRight: '5px',
                                            height: 40
                                        }}
                                        keyboardFocused={true}
                                        backgroundColor={'#0091ea'}
                                        hoverColor={'#12a4ff'}
                                        rippleColor={'white'}
                                        labelStyle={{fontSize: 10}}
                                        onTouchTap={this.saveSurveyEditedData.bind(this)}
                                    />,
                                    <FlatButton
                                        label={t[this.state.lang]['Survey_Cancel']}
                                        primary={true}
                                        backgroundColor={'#0091ea'}
                                        style={{ marginRight: '5px',color: 'white', marginBottom: '5px', height: 40}}
                                        keyboardFocused={true}
                                        hoverColor={'#12a4ff'}
                                        rippleColor={'white'}
                                        labelStyle={{fontSize: 10}}
                                        onTouchTap={this.handleCloseEditAllowedOrgDialog.bind(this)}
                                    />
                                ]}
                                open={this.state.openEditAllowedUsersDialog}
                                onRequestClose={this.handleCloseEditAllowedOrgDialog.bind(this)}
                                actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                                bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                                autoScrollBodyContent={true}
                                modal={true}
                                repositionOnUpdate={false}
                                contentStyle={{direction: myDir, maxWidth: '30%', borderRadius: '7px 7px 7px 7px'}}
                                titleStyle={{
                                    fontSize: 18,
                                    background: 'rgba(0,0,0,0.7)',
                                    color: 'white', textAlign: 'center',
                                    borderRadius: '2px 2px 0px 0px',
                                    textTransform: 'uppercase',
                                }}>
                                <br/>
                                <div style={isRight ? {
                                    fontSize: 16,
                                } : {fontSize: 16}}>{t[this.state.lang]['Template_Allowed_Org']}</div>
                                {isAdmin && <ChipInput
                                    hintText={t[this.state.lang]['Template_Allowed_Org']}
                                    floatingLabelText={t[this.state.lang]['Template_Allowed_Org']}
                                    value={this.state.curOrgObj.allowedOrganizations}
                                    filter={AutoComplete.fuzzyFilter}
                                    dataSource={this.state.organizationsInputList}
                                    onRequestAdd={(chip) => this.handleAddAllowedOrgField(chip)}
                                    dataSourceConfig={{'text': 'organizationName', 'value': 'id'}}
                                    onRequestDelete={(chip, index) => this.handleDeleteAllowedOrgField(chip, index)}
                                    maxSearchResults={5}
                                    openOnFocus={true}
                                />
                                }
                                <br/>
                            </Dialog>
                            <DynamicDialog
                                open={this.state.isFeaturesExplDialogOpen}
                                title={t[this.state.lang]['Survey_New_Features_title']}
                                dir={myDir}
                                bodyText={explText}
                                btnOkLable={isRight? 'המשך' : 'Continue'}
                                cancelBtnEnabled={false}
                                handlebBtnOkClick={this.closeFeaturesExplDialogAndRunSurvey.bind(this)}/>
                        </div>
                    </MuiThemeProvider>
                </div>
            </div>
        );
    }
}
