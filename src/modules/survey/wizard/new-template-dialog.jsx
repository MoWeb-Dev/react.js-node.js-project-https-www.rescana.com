import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Dropzone from 'react-dropzone';
import FontIcon from 'material-ui/FontIcon';
import classNames from 'classnames';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import TextField from 'material-ui/TextField';
import {LTR, RTL, MY_LANG, MY_DIR, DEFAULT_LANG, DEFAULT_DIR} from '../../../../config/consts';
import t from '../../../../config/i18n';
import {isLTRLanguage} from '../../common/AppHelpers';
import '../../../../public/css/survey.css';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from "material-ui/AutoComplete";

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class TemplateUploadModal extends React.Component {
    constructor(props) {
        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.uploadExcel = this.uploadExcel.bind(this);

        /* Only .xlsx extension is supported with the ExcelParser. */
        this.state = {
            open: false,
            showDropzone: false,
            files: [],
            disabled: false,
            acceptedFiles: ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            templateNameError: t[myLang]['Survey_Field_Required'],
            lang: myLang,
            dir: myDir,
            orgInputVal: [],
            organizationsInputList: this.props.organizationsInputList || []
        };
        this.handleAddAllowedUsersField = this.handleAddAllowedUsersField.bind(this);
        this.handleDeleteAllowedUsersField = this.handleDeleteAllowedUsersField.bind(this);
    }

    handleClose() {
        this.props.closeDialog();
        this.setState({open: false, orgInputVal:[]});
    };

    onDrop(files) {
        if (files.length !== 1) {
            app.addAlert('error', t[this.state.lang]['TemplateUploadDialog_ExcelsLimit']);
        } else {
            this.setState({
                files: files
            }, this.changeButtonDisable);
        }
    }

    changeButtonDisable() {
        if (this.state.files.length !== 0) {
            this.setState({
                disabled: false
            });
        } else {
            this.setState({
                disabled: true
            });
        }
    }
    uploadExcel() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let data = new FormData();
            if (this.state.files && this.state.files.length > 0 && this.state.templateName) {
                let file = this.state.files[0];
                let templateName = this.state.templateName;
                let lang = this.state.lang;
                data.append('surveyExcelTemplate', file, templateName + '.' + lang + '.' + (file.name || file.path));
            }

            $.ajax({
                url: '/api/surveyExcelUpload',
                dataType: 'json',
                data: data,
                enctype: 'multipart/form-data',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: (res) => {
                    if (res && res.result && res.result.sid) {
                        let newSid = res.result.sid;
                        let isUploadedExcel = true;
                        this.props.createSurveyTemplate(newSid, this.state.orgInputVal || [], isUploadedExcel);
                    } else {
                        app.addAlert('error', 'Excel upload failed.');
                    }
                }
            });
        }
    }

    createTemplate() {
        // If showDropzone is true, meaning the second option is selected - load from Excel.
        if (this.state.showDropzone) {
            // Make sure the user entered a name for the template. (if not,an error is displayed)
            if (this.state.templateName) {
                // If an Excel is selected, load it.
                if (this.state.files.length > 0) {
                    this.uploadExcel();
                }
            } else {
                this.nameInput.focus();
            }

        // else - If showDropzone is false, meaning the first option is selected - create manually.
        } else {
            this.handleClose();
            // Empty string "" means that a new sid needs to be generated.
            this.props.createSurveyTemplate('', this.state.orgInputVal || []);
        }
    }

    onDropRejected() {
        return app.addAlert('error', t[this.state.lang]['TemplateUploadDialog_ExcelSizeLimit']);
    }

    changeSelection(ignore, value) {
        if (value && value === 'createNew') {
            this.setState({showDropzone: false, disabled: false});
        } else {
            let disableSubmit = !(this.state.files.length > 0);
            this.setState({showDropzone: true, disabled: disableSubmit});
        }
    }

    handleChangeTemplateName(event) {
        let templateNameError;
        if (event.target.value) {
            templateNameError = '';
        } else {
            templateNameError = t[this.state.lang]['Survey_Field_Required'];
        }
        this.setState({
            templateName: event.target.value,
            templateNameError: templateNameError
        });
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.open === true) {
            this.setState({
                open: nextProps.open,
                showDropzone: false,
                disabled: false,
                files: [],
                allUsers: nextProps.allUsers || [],
                organizationsInputList: nextProps.organizationsInputList || []
            });
        }
    }

    handleAddAllowedUsersField(input) {
        let orgInputVal = this.state.orgInputVal;
        orgInputVal.push(input);
        this.setState({
            orgInputVal: orgInputVal
        });
    }

    handleDeleteAllowedUsersField(value, index) {
        let orgInputVal = this.state.orgInputVal;
        orgInputVal.splice(index, 1);
        this.setState({
            orgInputVal: orgInputVal
        });
    }

    render() {
        let user = app.getAuthUser();
        let isAdmin = false;
            if(user && user.userRole){
            isAdmin = user.userRole === "admin";
        }
        let dropZoneClass = classNames({
            'dropZone': true
        });
        let activeDropZoneClass = classNames({
            'stripes': true
        });
        let rejectDropZoneClass = classNames({
            'rejectStripes': true
        });
        let uploadCloudIcon = classNames({
            'material-icons': true,
            'uploadIconSize': true
        });

        let myLang = this.props.loadedLang || this.state.lang;

        let isLTR = isLTRLanguage(myLang);

        let myDir = (isLTR) ? LTR : RTL;

        let buttonsContainer = classNames({
            'uploadDialogButtonsLTR': isLTR,
            'uploadDialogButtonsRTL': !isLTR
        });

        let color;
        let excelText;

        if (this.state.files && this.state.files.length > 0) {
            color = '#00BCD4';
            excelText = t[myLang]['TemplateUploadDialog_ExcelSelected'] + this.state.files[0].name;
        } else {
            color = '#F44336';
            excelText = t[myLang]['TemplateUploadDialog_NoExcelSelected'];
        }

        const dropzoneLabelStyle = {color: color};

        const actions = [
            <FlatButton
                label={t[myLang]['TemplateUploadDialog_Cancel']}
                primary={true}
                backgroundColor={'#0091ea'}
                style={{ marginRight: '5px',color: 'white', marginBottom: '5px', height: 40}}
                keyboardFocused={true}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                onTouchTap={this.handleClose.bind(this)}
            />,
            <FlatButton
                label={t[myLang]['TemplateUploadDialog_Submit']}
                primary={true}
                style={{
                    color: 'white',
                    marginBottom: '5px',
                    marginRight: '5px',
                    height: 40
                }}
                keyboardFocused={true}
                backgroundColor={'#0091ea'}
                hoverColor={'#12a4ff'}
                rippleColor={'white'}
                labelStyle={{fontSize: 10}}
                disabled={this.state.disabled}
                onTouchTap={this.createTemplate.bind(this)}
            />
        ];

        let dropzone = (this.state.showDropzone) ?
            <div>
                <TextField
                    hintText={t[myLang]['TemplateUploadDialog_TemplateName']}
                    floatingLabelText={t[myLang]['TemplateUploadDialog_TemplateName']}
                    onChange={this.handleChangeTemplateName.bind(this)}
                    errorText={this.state.templateNameError}
                    ref={(input) => {
                        this.nameInput = input;
                    }}
                />
                <Dropzone
                    accept={this.state.acceptedFiles.join(',')}
                    onDrop={this.onDrop.bind(this)}
                    className={dropZoneClass}
                    activeClassName={activeDropZoneClass}
                    rejectClassName={rejectDropZoneClass}
                    onDropRejected={this.onDropRejected.bind(this)}
                    maxSize={3000000}
                >
                    <div className="dropzoneTextStyle">
                        <p className="dropzoneParagraph">{t[myLang]['TemplateUploadDialog_Dropzone_Text']}</p>
                        <br/>
                        <FontIcon
                            className={uploadCloudIcon}
                        >
                            cloud_upload
                        </FontIcon>
                    </div>
                </Dropzone>
                <br/>
                <label style={dropzoneLabelStyle}>{excelText}</label>
                <br/>
                <label>{t[myLang]['TemplateUploadDialog_Download_Examples']}</label>
                <a href="/Standard_Survey_Template_en.xlsx" download="Survey Template.xlsx">
                    <FlatButton
                        label={t[myLang]['TemplateUploadDialog_Download_English_Example']}
                        primary={true}
                    />
                </a>
                <a href="/Standard_Survey_Template_he.xlsx" download="תבנית לסקר.xlsx">
                    <FlatButton
                        label={t[myLang]['TemplateUploadDialog_Download_Hebrew_Example']}
                        primary={true}
                    />
                </a>
            </div> : '';

        return (
            <div>
                <Dialog
                    actions={actions}
                    actionsContainerClassName={buttonsContainer}
                    open={this.state.open}
                    title={t[myLang]['TemplateUploadDialog_Title']}
                    contentStyle={{direction: myDir}}
                    onRequestClose={this.handleClose.bind(this)}
                    actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                    bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                    autoScrollBodyContent={true}
                    modal={true}
                    repositionOnUpdate={false}
                    titleStyle={{
                        fontSize: 18,
                        background: 'rgba(0,0,0,0.7)',
                        color: 'white', textAlign: 'center',
                        borderRadius: '2px 2px 0px 0px',
                        textTransform: 'uppercase',
                    }}>
                    <br/>
                    <br/>
                    <RadioButtonGroup name="templateOptions" defaultSelected="createNew" onChange={this.changeSelection.bind(this)}>
                        <RadioButton
                            value="createNew"
                            label={t[myLang]['TemplateUploadDialog_Create_Manually']}
                            style={{marginBottom: 16, marginRight: '15px'}}
                        />
                        <RadioButton
                            value="loadFromExcel"
                            label={t[myLang]['TemplateUploadDialog_Load_From_Excel']}
                            style={{marginBottom: 16, marginRight: '15px'}}
                        />
                    </RadioButtonGroup>
                    {isAdmin && <ChipInput
                        hintText={t[this.state.lang]['Template_Allowed_Org']}
                        floatingLabelText={t[this.state.lang]['Template_Allowed_Org']}
                        value={this.state.orgInputVal}
                        filter={AutoComplete.fuzzyFilter}
                        dataSource={this.state.organizationsInputList}
                        onRequestAdd={(chip) => this.handleAddAllowedUsersField(chip)}
                        dataSourceConfig={{'text': 'organizationName', 'value': 'id'}}
                        onRequestDelete={(chip, index) => this.handleDeleteAllowedUsersField(chip, index)}
                        maxSearchResults={5}
                        openOnFocus={true}
                        style={{marginLeft: 5}}
                    />
                    }
                    <br/>
                    {dropzone}
                </Dialog>
            </div>
        );
    }
};
