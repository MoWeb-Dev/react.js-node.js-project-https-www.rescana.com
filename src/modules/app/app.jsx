'use strict';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
import ChooseOrganizationDialog from '../login/choose-organization-dialog.jsx';
import themeForFont from './themes/themeForFont.js';

injectTapEventPlugin();
import LanguageIcon from 'material-ui/svg-icons/action/language';
import ProjectsIcon from 'material-ui/svg-icons/action/assignment';
import Dialog from 'material-ui/Dialog';
import Loadable from 'react-loadable';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import '../../../public/css/main.css';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import IconButton from 'material-ui/IconButton/IconButton';
import {ToastContainer, ToastMessage} from 'react-toastr';
import Icon_Flag_US from './Flag-US.jsx';
import Icon_Flag_IL from './Flag-IL.jsx';
import {
    LANG_EN,
    LANG_HE,
    LTR,
    RTL,
    MY_LANG,
    MY_DIR,
    DEFAULT_LANG,
    DEFAULT_DIR,
    AUTH_USER,
    PROJECT_NAME,
    PROJECT_ID,
    PROJECT_ISSELFASSESSMENT,
    ORGANIZATION_ID,
    ORGANIZATION_NAME,
    SESSION_COMPANIES,
    SCORE_TYPES, SESSION_CVE_SEVERITY, SESSION_CVE_SEVERITY_PANEL, SESSION_FIRST_CVE_TABLE
    , SESSION_ADD_NEW_COMP_BY_USER,
} from '../../../config/consts';
import t from '../../../config/i18n';
import {isAuth} from '../common/AppHelpers';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import {cyanA200} from 'material-ui/styles/colors';
import {List, ListItem} from 'material-ui/List';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import EditorInsertChart from 'material-ui/svg-icons/editor/insert-chart';
import ActionDashboard from 'material-ui/svg-icons/action/dashboard';
import ActionTimeline from 'material-ui/svg-icons/action/timeline';
import AlertWarning from 'material-ui/svg-icons/alert/warning';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import MenuArrowLeft from 'material-ui/svg-icons/navigation/chevron-left';
import Subheader from 'material-ui/Subheader';
import '../../../public/css/main.css';
import '../../../public/css/survey.css';
import sideBarTheme from './themes/sideBarTheme';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';

// application instance
// TODO: This is horrific, must be changed.
window.app = null;

const ToastMessageFactory = React.createFactory(ToastMessage.animation);


class App extends React.Component {
    constructor(props) {
        super(props);

        let myLang = localStorage.getItem(MY_LANG) || DEFAULT_LANG;
        let myDir = localStorage.getItem(MY_DIR) || DEFAULT_DIR;

        this.state = {
            user: this.getAuthUser(),
            currentUserName: '',
            openConfirmModal: false,
            flagVisibility: '0',
            projects: [],
            flagPointerEvents: 'none',
            lang: myLang,
            dir: myDir,
            projectIdx: '',
            open: true,
            orgArrForUserInput: [],
            openChooseOrgDialog: false,
            curOrgIdx: 0
        };

        this.handleCloseConfirmModal = this.handleCloseConfirmModal.bind(this);
        this.handleOpenConfirmModal = this.handleOpenConfirmModal.bind(this);
        this.handleSignOut = this.handleSignOut.bind(this);
        this.handleAdminLink = this.handleAdminLink.bind(this);
        this.handleOverviewLink = this.handleOverviewLink.bind(this);
        this.handleDataleaksLink = this.handleDataleaksLink.bind(this);
        this.handleBlacklistsLink = this.handleBlacklistsLink.bind(this);
        this.handleBrandLink = this.handleBrandLink.bind(this);
        this.handleDashboardLink = this.handleDashboardLink.bind(this);
        this.handleAccountSettingsLink = this.handleAccountSettingsLink.bind(this);
        this.handleRunSurveyLink = this.handleRunSurveyLink.bind(this);
        this.handleEditSurveyLink = this.handleEditSurveyLink.bind(this);
        this.handleExportIntelReportLink = this.handleExportIntelReportLink.bind(this);
        this.handleExportCVEReportLink = this.handleExportCVEReportLink.bind(this);
        this.handleExportSurveyReportLink = this.handleExportSurveyReportLink.bind(this);
        this.handleLangChange = this.handleLangChange.bind(this);
        this.handleLeftIconTap = this.handleLeftIconTap.bind(this);
        this.handleManageProjectsLink = this.handleManageProjectsLink.bind(this);
        this.handleCreateNewProjectLink = this.handleCreateNewProjectLink.bind(this);
        this.handleManageCompaniesLink = this.handleManageCompaniesLink.bind(this);
        this.addAlert = this.addAlert.bind(this);
        this.handleProjectChange = this.handleProjectChange.bind(this);
        this.handleOpenMenu = this.handleOpenMenu.bind(this);
        this.handleCloseMenu = this.handleCloseMenu.bind(this);
        this.handleIntelPortsLink = this.handleIntelPortsLink.bind(this);
        this.handleIntelDMARCLink = this.handleIntelDMARCLink.bind(this);
        this.handleIntelSPFLink = this.handleIntelSPFLink.bind(this);
        this.handleOpenOrgDialog = this.handleOpenOrgDialog.bind(this);
        this.handleOrgCloseDialog = this.handleOrgCloseDialog.bind(this);
        app = this;
    }

    addAlert(type, message, title) {
        if (this.refs.container[type] === undefined) {
            return;
        }

        this.refs.container[type](message, title, {
            closeButton: true
        });
    }

    componentDidMount() {
        this.routeAuthUser();
        this.initUserDataDisplay();
        if(this.state.user && this.state.user.userRole === 'admin'){
            this.refs.ChooseOrganizationDialog.getOrganizationsByUserId(true)
        }
    }

    rerender() {
        this.initUserDataDisplay();
        if (this.state.user && this.state.user.userRole === 'admin') {
            if (localStorage.getItem(ORGANIZATION_ID) === null) {
                this.refs.ChooseOrganizationDialog.getOrganizationsByUserId(true)
            }
        }
    }

    initUserDataDisplay() {
        let authUser = this.state.user;

        if (authUser) {
            let userName = authUser.name;
            if (userName) {
                let firstN = userName.firstName;
                let lastN = userName.lastName;

                if (firstN && lastN && firstN.trim() && lastN.trim()) {
                    this.setState({currentUserName: firstN + ' ' + lastN});
                } else {
                    let authEmail = authUser.email;

                    if (authEmail) {
                        this.setState({currentUserName: authEmail});
                    }
                }
            } else {
                let authEmail = authUser.email;

                if (authEmail) {
                    this.setState({currentUserName: authEmail});
                }
            }
            this.getProjects();
        }
    }

    getProjects() {
        let authUser = this.state.user;
        let loadedOrg = localStorage.getItem(ORGANIZATION_ID);

        let data = {};
        let url = '';
        let type = '';

        if (loadedOrg && authUser.userRole !== 'admin') {
            url = '/api/getProjectsByOrgId';
            type = 'POST';
            data.orgId = loadedOrg;
        } else {
            url = '/api/getAllProjectsById';
            type = 'GET';
        }
        $.ajax({
            type: type,
            url: url,
            async: true,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projects) => {
            if (projects && projects.length !== 0) {
                let loadedProject = localStorage.getItem(PROJECT_NAME);
                let pId;
                let pIndex;

                if (loadedProject) {
                    let curProj = projects.find((project) => {return project.projectName === loadedProject? project : null;});
                    pId = curProj.id;
                    pIndex = curProj.projectName;

                } else {
                    pId = projects[0].id;
                    pIndex = projects[0].projectName;
                }

                localStorage.setItem(PROJECT_ID, pId);
                localStorage.setItem(PROJECT_NAME, pIndex);

                projects.find((project) => {
                    if (project.projectName === pIndex) {
                        this.checkIsSelfAssessmentProject(project);
                    }
                });

                if(this.state.user && this.state.user.userRole === 'admin' && pId && typeof pId === 'string'){
                    this.updateOrgOnLocalStorageByCurProj(pId);
                }

                this.setState({
                    projects: projects,
                    projectIdx: pIndex
                });
            } else {
                this.setState({
                    projects: projects,
                    projectIdx: 'No Projects Available'
                });
            }
        }).fail((jqXHR, textStatus) => {
            if (textStatus === 'parsererror') {
                this.removeAuthUser();
                this.handleSignOut();
            }
        });
    }

    checkIsSelfAssessmentProject(project) {
        if (project && project.isSelfAssessmentProject) {
            if (project.selectedCompanies && project.selectedCompanies.length === 1) {
                localStorage.setItem(PROJECT_ISSELFASSESSMENT, 'true with one company');
            } else {
                localStorage.setItem(PROJECT_ISSELFASSESSMENT, 'true');
            }
        } else {
            localStorage.setItem(PROJECT_ISSELFASSESSMENT, 'false');
        }
    }

    handleLeftIconTap() {
        if (this.state.user) {
            browserHistory.push('/dashboard');
        } else {
            this.routeAuthUser();
        }
    }

    handleOpenConfirmModal() {
        this.setState({
            openConfirmModal: true
        });
    }

    handleCloseConfirmModal() {
        this.setState({
            openConfirmModal: false
        });
    }

    handleOrgCloseDialog() {
        this.setState({
            openChooseOrgDialog: false
        });
    }

    handleLoginLink(e) {
        e.preventDefault();
        browserHistory.push('/login');
    }

    handleAccountSettingsLink(e) {
        e.preventDefault();
        browserHistory.push('/account-settings');
    }

    handleSignOut() {
        this.removeAuthUser();
        this.setState({
            open: true,
            user: undefined,
            projects: [],
            projectIdx: '',
            currentUserName: ''
        }, () => {
            browserHistory.push('/login');
        });
    }

    handleAddNewCompanyByUser() {
        sessionStorage.setItem(SESSION_ADD_NEW_COMP_BY_USER, 'true');
        browserHistory.push('/manage-companies');
    }

    handleAdminLink() {
        browserHistory.push('/admin');
    }

    handleOverviewLink() {
        browserHistory.push('/overview');
    }

    handleDataleaksLink() {
        this.refreshSessionSettings();
        browserHistory.push('/dataleaks');
    }

    handleBlacklistsLink() {
        this.refreshSessionSettings();
        browserHistory.push('/blacklists');
    }

    handleBrandLink() {
        browserHistory.push('/brand');
    }

    handleDashboardLink() {
        browserHistory.push('/dashboard');
    }

    handleIntelIPLink(e) {
        e.preventDefault();
        browserHistory.push('/intel/IP');
    }

    handleIntelPortsLink(e) {
        e.preventDefault();
        e.stopPropagation();
        this.refreshSessionSettings();
        browserHistory.push('/intel/ports');
    }

    handleIntelCVELink(e) {
        e.preventDefault();
        e.stopPropagation();
        //set cve page to show first table on enter
        sessionStorage.setItem(SESSION_FIRST_CVE_TABLE, 'true');
        //when coming from menu to cve page we dont need the cve severity identifier(only when we come from dashboard/company dashboard
        this.refreshSessionSettings();
        browserHistory.push('/intel/cve');
    }

    handleIntelSPFLink(e) {
        e.preventDefault();
        e.stopPropagation();
        this.refreshSessionSettings();
        browserHistory.push('/intel/spf');
    }

    handleIntelDMARCLink(e) {
        e.preventDefault();
        e.stopPropagation();
        this.refreshSessionSettings();
        browserHistory.push('/intel/dmarc');
    }

    handleBreachedLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/breaches');
    }

    handleIntelVPNLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/intel/VPN');
    }

    handleIntelBucketsLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/intel/buckets');
    }

    handleIntelProductLink(e) {
        e.preventDefault();
        e.stopPropagation();
        this.refreshSessionSettings();
        browserHistory.push('/intel/product');
    }

    handleIntelISPLink(e) {
        e.preventDefault();
        e.stopPropagation();
        this.refreshSessionSettings();
        browserHistory.push('/intel/isp');
    }

    handleBotnetsLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/botnets');
    }

    handleValidSSLCertificatesLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/ssl-certs-discovery');
    }

    handleNotValidSSLCertificatesLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/ssl-certs-vulnerability');
    }


    handleWifiNetworksLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/wifi-networks');
    }

    handleGDPRComplianceLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/gdpr-compliance');
    }

    handleIntelASNLink(e) {
        e.preventDefault();
        e.stopPropagation();
        browserHistory.push('/intel/asn');
    }

    handleRunSurveyLink() {
        browserHistory.push('/survey-start');
    }

    handleEditSurveyLink() {
        browserHistory.push('/survey-wizard');
    }

    handleExportIntelReportLink() {
        browserHistory.push('/report-new');
    }

    handleExportCVEReportLink() {
        browserHistory.push('/cve-report-new');
    }

    handleExportSurveyReportLink() {
        browserHistory.push('/survey-report-new');
    }

    handleManageProjectsLink() {
        browserHistory.push('/manage-projects');
    }

    handleCreateNewProjectLink() {
        browserHistory.push('/create-new-project');
    }

    handleManageCompaniesLink() {
        sessionStorage.setItem(SESSION_ADD_NEW_COMP_BY_USER, 'false');
        browserHistory.push('/manage-companies');
    }

    refreshSessionSettings() {
        sessionStorage.removeItem(SESSION_CVE_SEVERITY);
        sessionStorage.removeItem(SESSION_CVE_SEVERITY_PANEL);
    }

    routeAuthUser(toHome) {
        if (!this.state.user) {
            browserHistory.push('/login');
        } else if (toHome) {
            app.rerender();
            browserHistory.push('/dashboard');

            // In case there is a state with user, and the localStorage was somehow deleted - recreate it.
        } else if (!localStorage.getItem(AUTH_USER)) {
            localStorage.setItem(AUTH_USER, JSON.stringify(this.state.user));
            console.log('localStorage was refreshed.');
        } else if (this.state.user && window.location.pathname === '/login' || window.location.pathname === '/') {
            browserHistory.push('/dashboard');
        } else {
            app.rerender();
        }
    }

    getAuthUser() {
        let storedUser = localStorage.getItem(AUTH_USER);

        try {
            storedUser = JSON.parse(storedUser);
        } catch (e) {
            storedUser = null;
        }

        return storedUser;
    }

    clearLocalStorage() {
        if (localStorage.getItem(AUTH_USER)) {
            localStorage.removeItem(AUTH_USER);
        }
        if (localStorage.getItem(PROJECT_NAME)) {
            localStorage.removeItem(PROJECT_NAME);
        }
        // Also clean SessionStorage if exists.
        if (sessionStorage.getItem(SESSION_COMPANIES)) {
            sessionStorage.removeItem(SESSION_COMPANIES);
        }
    }

    setAuthUser(user) {
        let storedUser = JSON.stringify(user);

        this.setState({user: user});
        localStorage.setItem(AUTH_USER, storedUser);
    }

    removeAuthUser() {
        localStorage.removeItem(AUTH_USER);
        this.setState({user: null});
    }

    setFlagVisibile(isVisible) {
        let visible;
        let pointerEvents;

        if (isVisible) {
            visible = '1';
            pointerEvents = 'all';
        } else {
            visible = '0';
            pointerEvents = 'none';
        }
        if (this.state.flagVisibility !== visible) {
            this.setState({flagVisibility: visible, flagPointerEvents: pointerEvents});
        }
    }

    // This function checks whether the user settings are set to display the scores backwards.
    shouldDisplayScoreBackwards(user) {
        return !!(user && user.userPreferencesConfig && user.userPreferencesConfig.displayScoreBackwards);
    }

    // This function returns the backwards score by the score's type.
    switchScoreBackwards(score, scoreType = SCORE_TYPES.COMPANY) {
        let completeNumberForSwitch;

        if (scoreType === SCORE_TYPES.INTEL) {
            completeNumberForSwitch = 10;
        } else if (scoreType === SCORE_TYPES.SURVEY) {
            completeNumberForSwitch = 1;
        } else {
            // For COMPANY type.
            completeNumberForSwitch = 100;
        }

        return completeNumberForSwitch - score;
    }

    handleLangChange(ignore, value) {
        if (localStorage.getItem(MY_LANG) !== value) {
            let dir = (value === LANG_EN) ? LTR : RTL;

            localStorage.setItem(MY_LANG, value);
            localStorage.setItem(MY_DIR, dir);

            this.setState({lang: value, dir: dir});

            let href = window.location.href;
            let prefix = 'survey-editor?sid=';

            // Save the language to DB only from survey-editor.
            if (href.indexOf(prefix) !== -1) {
                let desiredSid = href.substring(href.indexOf(prefix) + prefix.length, href.length);
                $.ajax({
                    type: 'POST',
                    url: '/api/updateSurveyLanguage',
                    data: JSON.stringify({sid: desiredSid, lang: value}),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: true
                }).done(() => {
                    console.log('Updated survey language');
                    window.location = window.location;
                }).fail(() => {
                    app.addAlert('error', t[this.state.lang]['App_Survey_changeLanguageToDB_Error']);
                });
            } else {
                window.location = window.location;
            }
        }
    }

    handleProjectChange(event, value) {
        event.preventDefault();
        this.setState({projectIdx: value});
        localStorage.setItem(PROJECT_NAME, value);
    }

    handleOpenMenu(event) {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            openMenu: true,
            anchorEl: event.currentTarget
        });
    }

    handleCloseMenu() {
        this.setState({
            openMenu: false
        });
    }


    handleProjectSelect(idx, e) {
        e.preventDefault();

        this.setState({projectIdx: this.state.projects[idx].projectName, openMenu: false});
        localStorage.setItem(PROJECT_NAME, this.state.projects[idx].projectName);
        browserHistory.push('/dashboard/' + this.state.projects[idx].projectName);
        // Also clean SessionStorage if exists.
        if (sessionStorage.getItem(SESSION_COMPANIES)) {
            sessionStorage.removeItem(SESSION_COMPANIES);
        }
        this.checkIsSelfAssessmentProject(this.state.projects[idx]);
        if(this.state.user && this.state.user.userRole === 'admin'){
            this.updateOrgOnLocalStorageByCurProj(this.state.projects[idx].id);
        }
    }

    updateOrgOnLocalStorageByCurProj(curProjectId) {
        if(curProjectId && typeof curProjectId === 'string'){
            $.ajax({
                type: 'POST',
                url: '/admin/getOrganizationByProject',
                data: JSON.stringify({projectId: curProjectId}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done((data) => {
                if (data && data.ok) {
                    if(data.curOrg && data.curOrg.id && data.curOrg.name){
                        this.setState({
                            chosenOrg: { _id: data.curOrg.id, organizationName: data.curOrg.name}
                        });
                        this.handleAfterOrgChoose(false);
                    } else if(data.message) {
                        app.addAlert('error', 'Error: ' + data.message);
                    } else {
                        app.addAlert('error', 'Error: failed to load organization by current project.');
                    }
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to load organization by current project.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load organization by current project.');
            });
        } else {
            app.addAlert('error', 'Error: failed to load organization by current project.');
        }
    }

    handleToggle() {
        //when toggle menu on cve(network) page the table will not change because of the rerender caused by the toggle
        sessionStorage.setItem(SESSION_FIRST_CVE_TABLE, 'noChangeToTable');
        this.setState({open: !this.state.open});
    }

    handleAfterOrgChoose(isFromDialog = true) {
        if (this.state.chosenOrg && this.state.chosenOrg._id) {
            localStorage.setItem(ORGANIZATION_ID, this.state.chosenOrg._id);
            localStorage.setItem(ORGANIZATION_NAME, this.state.chosenOrg.organizationName);
            if(isFromDialog){
                this.setState({openChooseOrgDialog: false})
            }
        } else {
            if(isFromDialog) {
                app.addAlert('error', 'Error: Failed to choose Organization!');
            } else {
                app.addAlert('error', 'Error: Failed to choose Organization!');
            }
        }
    }

    handleOrgChange(event, index, value) {
        this.setState({curOrgIdx: value})
    }

    handleOrgSelect(idx, e) {
        e.preventDefault();
        this.setState({
            chosenOrg: this.state.orgArrForUserInput[idx]
        });
        localStorage.setItem(ORGANIZATION_ID, this.state.chosenOrg._id);
        localStorage.setItem(ORGANIZATION_NAME, this.state.chosenOrg.organizationName);
    }

    handleOrgResults(orgArrForUserInput) {
        orgArrForUserInput.splice(0, 0, {_id: 'No Id', organizationName: 'No Organization'});
        if (orgArrForUserInput && orgArrForUserInput.length > 0) {
            this.setState({
                orgArrForUserInput: orgArrForUserInput
            });
        } else {
            app.addAlert('error', 'Error: No Organizations Related To The User Found.');
        }
    }

    handleOpenOrgDialog(){
        this.setState({ openChooseOrgDialog: true})
    }

    render() {
        const styles = {
            title: {
                cursor: 'pointer',
                color: cyanA200
            },
            listItems: {
                fontSize: 14,
                marginLeft: '55px',
            },
            subListItems: {
                paddingLeft: '40px',
                fontSize: 13,
                background: 'rgba(0,0,0,0.3)',
            },
            mediumIcon: {
                position: 'relative',
                top: '-11px',
                height: 64,
                width: 64
            },
            smallIcon: {
                width: 16,
                height: 26,
                marginLeft: 0,
            },
            toastStyle: {
                marginRight: '100px',
                marginBottom: '16px'
            },
            signInLogoStyle: {
                cursor: 'pointer',
                paddingLeft: '60px',
                paddingRight: '60px',
                marginTop: '-21px',
                borderBottomRightRadius: 100,
                marginLeft: '-10px',
                background: '#444444'
            }
        };
        const curOrgName = this.state.chosenOrg && this.state.chosenOrg.organizationName ? this.state.chosenOrg.organizationName : '';
        const containerStyleByDrawer = this.state.open ? {marginLeft: 255} : {};

        let hideFlag = false;
        if (this.state.user && this.state.user.userRole === 'basic') {
            hideFlag = true;
        }

        let hide = classNames({
            'hide': hideFlag
        });

        let flagIconBtn = <IconButton
            tooltip={t[this.state.lang]['App_Survey_changeLanguage']}
            tooltipPosition="bottom-center"><LanguageIcon color={'secondary'}/></IconButton>;

        let projects = this.state.projects
            .sort((a, b) => {
                let textA = a.projectName;
                let textB = b.projectName;
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            })
            .map((project, idx) => {
                return <MenuItem value={project.projectName}
                                 key={idx}
                                 style={{color: 'white', fontSize: '13px'}}
                                 primaryText={project.projectName}
                                 onTouchTap={this.handleProjectSelect.bind(this, idx)}
                />
            });
        let toggleExpander = false;
        let iconElementRight;
        let navBarElements;
        let isAdmin = (this.state.user && this.state.user.admin);
        let isUserAllowedToAddVendors = (this.state.user && this.state.user.isUserAllowedToAddVendors);
        let isAllowedToExportCVE = (this.state.user && this.state.user.canExportCVEReports);
        let iconElementLeft =
            <div>
                {this.state.user && <IconButton
                    style={{
                        position: 'absolute', top: '3px', left: '3px',
                    }}

                    onClick={this.handleToggle.bind(this)}>
                    <MenuIcon color={'primary'}/>
                </IconButton>}
                {isAdmin && <span style={{
                    position: 'relative',
                    color: 'black',
                    fontSize: 18,
                    top: '-5.0px',
                    left: this.state.open? 272 : 55,
                    marginLeft: '5px',
                    marginRight: '5px'
                }}>{curOrgName}</span>
                }
                {!this.state.user && <img
                    src='/images/logo_rescana.png'
                    width={'91px'}
                    height={'53px'}
                    onClick={this.handleLeftIconTap}
                    style={styles.signInLogoStyle}/>}
            </div>;

        if (!location.pathname.startsWith('/login')) {
            if (this.state.user) {
                iconElementRight =
                    <div style={{marginTop: '-16px', marginRight: '16px'}}>
                        <IconMenu
                            iconButtonElement={flagIconBtn}
                            targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                            onChange={this.handleLangChange}
                            style={{
                                fillOpacity: this.state.flagVisibility,
                                pointerEvents: this.state.flagPointerEvents,
                                position: 'relative',
                                top: '-10px',
                                marginRight: '5px',
                                right: '0px',
                            }}>
                            <MenuItem style={{position: 'relative', right: '7px', top: '-5px'}}
                                      value={LANG_EN} key={11}><IconButton><Icon_Flag_US width={15}
                                                                                         height={15}/></IconButton></MenuItem>
                            <MenuItem style={{position: 'relative', right: '7px', top: '-5px'}}
                                      value={LANG_HE} key={12}><IconButton><Icon_Flag_IL width={15}
                                                                                         height={15}/></IconButton></MenuItem>
                        </IconMenu>
                        <FlatButton
                            label={this.state.projectIdx}
                            labelPosition="before"
                            labelStyle={{textTransform: 'capitalize', bottom: '2px', marginRight: 5}}
                            icon={<ProjectsIcon
                                color='#212121' style={{height: 25, width: 25, top: -2}}/>}
                            onTouchTap={this.handleOpenMenu}
                            style={{height: '50px', color: 'black', position: 'relative', top: '-6px'}}
                        />
                        <Popover
                            tooltip='Projects'
                            tooltipPosition="bottom-center"
                            open={this.state.openMenu}
                            anchorEl={this.state.anchorEl}
                            anchorOrigin={{horizontal: 'middle', vertical: 'top'}}
                            targetOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                            onRequestClose={this.handleCloseMenu}>
                            <Menu>
                                {projects}
                            </Menu>
                        </Popover>
                        <IconMenu
                            iconButtonElement={
                                <IconButton
                                    tooltip='User'
                                    tooltipPosition="bottom-center"
                                    name="avatarButton"
                                    style={styles.mediumIcon}>
                                    <img src={'/images/user_icon.png'} width={37} height={37} alt={'no pic'}/>
                                </IconButton>
                            }
                            targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                            key={13}>
                            {(isUserAllowedToAddVendors || isAdmin) &&
                            <MenuItem key={14} innerDivStyle={{fontSize: "13px"}}
                                      primaryText="Add New Company"
                                      onTouchTap={() => {
                                          if (isAdmin) {
                                              app.addAlert('warning', 'You are admin user! please manage companies from admin page.');
                                          } else {
                                              this.handleAddNewCompanyByUser();
                                          }
                                      }}/>
                            }
                            {
                                isAdmin && <MenuItem key={15} innerDivStyle={{fontSize: "13px"}}
                                                     onTouchTap={this.handleOpenOrgDialog}>Select Organization</MenuItem>
                            }
                            {
                                isAdmin && <MenuItem key={19} innerDivStyle={{fontSize: "13px"}}
                                                     onTouchTap={this.handleAdminLink}>Admin</MenuItem>
                            }
                            <MenuItem
                                key={16}
                                innerDivStyle={{fontSize: "13px"}}
                                primaryText="Account"
                                onTouchTap={this.handleAccountSettingsLink}/>
                            <MenuItem key={17} innerDivStyle={{fontSize: "13px"}}
                                      primaryText="Sign out"
                                      onTouchTap={this.handleSignOut}/>
                            <Subheader key={18} innerDivStyle={{color: '0091ea'}}
                                       disabled={true}/>
                        </IconMenu>
                        <span style={{
                            position: 'relative',
                            color: 'black',
                            top: '-5.0px',
                            marginLeft: '5px',
                            marginRight: '5px'
                        }}>
                            {this.state.user.name.firstName ? "Hi " + this.state.user.name.firstName : "Hi " + ''}</span>
                    </div>;

            } else {
                iconElementRight =
                    <FlatButton
                        primary={true}
                        label="Log in"
                        style={{
                            bottom: 19.92,
                            color: 'white',
                            borderRadius: 6,
                            height: 40
                        }}
                        backgroundColor={'rgba(0,0,0,0.7'}
                        hoverColor={'#0091ea'}
                        labelStyle={{fontSize: 13, textTransform: 'capitalize', marginRight: 5}}
                        onClick={this.handleLoginLink.bind(this)}
                    />;
            }
        }

        if (this.state.user) {
            navBarElements =
                <Drawer open={this.state.open} className="drawer" width={255}>
                    {<IconButton
                        style={{
                            position: 'absolute', top: '3px', left: '3px',
                            background: '#444444',
                        }}
                        onClick={this.handleToggle.bind(this)}>
                        <MenuArrowLeft/>
                    </IconButton>}
                    {this.state.user && this.state.user.eyPartner ?
                        <img
                            src='/images/EY powered by rescana.png'
                            width={'91px'}
                            height={'53px'}
                            onClick={this.handleLeftIconTap}
                            style={{cursor: 'pointer', marginLeft: '80px'}}/> :
                        <img
                            src='/images/logo_rescana.png'
                            width={'91px'}
                            height={'53px'}
                            onClick={this.handleLeftIconTap}
                            style={{cursor: 'pointer', marginLeft: '80px'}}/>}
                    <List className="listMenu">
                        <MenuItem
                            primaryText="Dashboard" key={1} innerDivStyle={styles.listItems}
                            leftIcon={<ActionDashboard color='white' style={styles.smallIcon}/>}
                            initiallyOpen={toggleExpander} primaryTogglesNestedList={true}
                            nestedItems={[
                                <MenuItem
                                    key={11} primaryText="Overview" innerDivStyle={styles.subListItems}
                                    onTouchTap={this.handleDashboardLink}/>,
                                <MenuItem
                                    key={12} primaryText="Company Analysis" innerDivStyle={styles.subListItems}
                                    onTouchTap={this.handleOverviewLink}/>
                            ]}/>
                        {/* onTouchTap={this.handleDashboardLink}/>*/}
                        <MenuItem className={hide} primaryText="Discovery" key={2} innerDivStyle={styles.listItems}
                                  leftIcon={<ActionTimeline color='white' style={styles.smallIcon}/>}
                                  initiallyOpen={toggleExpander} primaryTogglesNestedList={true} nestedItems={[
                            <MenuItem key={21} primaryText="Domains\IP" innerDivStyle={styles.subListItems}
                                      onTouchTap={this.handleIntelIPLink.bind(this)}/>,
                            <MenuItem key={22} innerDivStyle={styles.subListItems} primaryText="Open Ports"
                                      onTouchTap={this.handleIntelPortsLink.bind(this)}/>,
                            /*<MenuItem key={23} innerDivStyle={styles.subListItems} primaryText="VPN"
                                      onTouchTap={this.handleIntelVPNLink.bind(this)}/>,*/
                            <MenuItem key={27} innerDivStyle={styles.subListItems} primaryText="S3 Buckets"
                                      onTouchTap={this.handleIntelBucketsLink.bind(this)}/>,
                            <MenuItem key={24} innerDivStyle={styles.subListItems} primaryText="Discovered Systems"
                                      onTouchTap={this.handleIntelProductLink.bind(this)}/>,
                            /*<MenuItem key={25} innerDivStyle={styles.subListItems} primaryText="ASN"
                                      onTouchTap={this.handleIntelASNLink.bind(this)}/>,*/
                            <MenuItem key={26} innerDivStyle={styles.subListItems} primaryText="ISP/Hosting Provider"
                                      onTouchTap={this.handleIntelISPLink.bind(this)}/>,
                            <MenuItem key={28} innerDivStyle={styles.subListItems} primaryText="SSL Certificates"
                                      onTouchTap={this.handleValidSSLCertificatesLink.bind(this)}/>
      /*                      <MenuItem key={29} innerDivStyle={styles.subListItems} primaryText="Wifi Networks"
                                      onTouchTap={this.handleWifiNetworksLink.bind(this)}/>*/
                        ]}/>
                        <MenuItem
                            className={hide} primaryText="Risks" innerDivStyle={styles.listItems}
                            key={3}
                            leftIcon={<AlertWarning color='white' style={styles.smallIcon}/>}
                            initiallyOpen={toggleExpander} primaryTogglesNestedList={true}
                            nestedItems={[
                                <MenuItem
                                    key={31} innerDivStyle={styles.subListItems} primaryText="Network"
                                    onTouchTap={this.handleIntelCVELink.bind(this)}/>,
                                <MenuItem
                                    key={32} innerDivStyle={styles.subListItems} primaryText="Dataleaks"
                                    onTouchTap={this.handleDataleaksLink}/>,
                                <MenuItem
                                    key={38} innerDivStyle={styles.subListItems} primaryText="Blacklists"
                                    onTouchTap={this.handleBlacklistsLink}/>,
                                <MenuItem
                                    key={39} innerDivStyle={styles.subListItems} primaryText="Botnets & Malware"
                                    onTouchTap={this.handleBotnetsLink.bind(this)}/>,
                                <MenuItem
                                    key={391} innerDivStyle={styles.subListItems} primaryText="No SSL"
                                    onTouchTap={this.handleNotValidSSLCertificatesLink.bind(this)}/>,
                                <MenuItem
                                    key={392} innerDivStyle={styles.subListItems} primaryText="Web Privacy Policy"
                                    onTouchTap={this.handleGDPRComplianceLink.bind(this)}/>,
                                <MenuItem
                                    key={33} innerDivStyle={styles.subListItems} primaryText="Social Engineering"
                                    onTouchTap={this.handleBrandLink}/>,
                                <MenuItem
                                    key={35} innerDivStyle={styles.subListItems}
                                    primaryText="SPF Records" onTouchTap={this.handleIntelSPFLink}/>,
                                <MenuItem
                                    key={36} innerDivStyle={styles.subListItems}
                                    primaryText="DMARC Records" onTouchTap={this.handleIntelDMARCLink}/>,
                                <MenuItem
                                    key={37} innerDivStyle={styles.subListItems}
                                    primaryText="Emails Found In Breaches" onTouchTap={this.handleBreachedLink}/>
                            ]}/>
                        <MenuItem
                            primaryText="Surveys"
                            key={4}
                            innerDivStyle={styles.listItems}
                            leftIcon={<ActionAssignment color='white' style={styles.smallIcon}/>}
                            initiallyOpen={toggleExpander}
                            primaryTogglesNestedList={true}
                            nestedItems={[
                                <MenuItem
                                    className={hide}
                                    key={42}
                                    primaryText={t[this.state.lang]['NavMenu_Edit_Survey_Templates']}
                                    innerDivStyle={{
                                        direction: this.state.dir,
                                        paddingLeft: '40px',
                                        fontSize: 13,
                                        background: 'rgba(0,0,0,0.3)'
                                    }}
                                    onTouchTap={this.handleEditSurveyLink}/>,
                                <MenuItem
                                    key={41}
                                    primaryText={t[this.state.lang]['NavMenu_Run_Surveys']}
                                    innerDivStyle={{
                                        direction: this.state.dir,
                                        paddingLeft: '40px',
                                        fontSize: 13,
                                        background: 'rgba(0,0,0,0.3)'
                                    }}
                                    onTouchTap={this.handleRunSurveyLink}/>
                            ]}/>
                        <MenuItem
                            primaryText="Reports"
                            key={6}
                            innerDivStyle={styles.listItems}
                            leftIcon={<EditorInsertChart color='white' style={styles.smallIcon}/>}
                            initiallyOpen={toggleExpander}
                            primaryTogglesNestedList={true}
                            nestedItems={[
                                <MenuItem
                                    key={61}
                                    primaryText="Export Intel Report"
                                    innerDivStyle={styles.subListItems}
                                    onTouchTap={this.handleExportIntelReportLink}/>,
                                (isAdmin) ?
                                    <MenuItem
                                        key={63}
                                        primaryText="Export Survey Report"
                                        innerDivStyle={styles.subListItems}
                                        onTouchTap={this.handleExportSurveyReportLink}/>
                                    : null,
                                (isAdmin || isAllowedToExportCVE) ?
                                    <MenuItem
                                        key={62}
                                        primaryText="Export CVE Report"
                                        innerDivStyle={styles.subListItems}
                                        onTouchTap={this.handleExportCVEReportLink}/>
                                    : null
                            ]}/>
                        {(isUserAllowedToAddVendors || isAdmin) &&
                        <MenuItem
                            primaryText="Preferences" key={7} className={hide} innerDivStyle={styles.listItems}
                            leftIcon={<SettingsIcon color='white' style={styles.smallIcon}/>}
                            initiallyOpen={toggleExpander} primaryTogglesNestedList={true}
                            nestedItems={[
                                <MenuItem
                                    key={71} primaryText="Manage Projects"
                                    innerDivStyle={styles.subListItems}
                                    onTouchTap={this.handleManageProjectsLink}/>,
                                <MenuItem
                                    key={72} primaryText="Create New Project"
                                    innerDivStyle={styles.subListItems}
                                    onTouchTap={this.handleCreateNewProjectLink}/>,
                                <MenuItem
                                    key={73} primaryText="Manage Companies"
                                    innerDivStyle={styles.subListItems}
                                    onTouchTap={this.handleManageCompaniesLink}/>
                            ]}/>
                        }
                    </List>
                </Drawer>;
        }

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(sideBarTheme)}>
                <div>
                    <MuiThemeProvider muiTheme={getMuiTheme(sideBarTheme)}>
                        <div>
                            <AppBar
                                iconElementLeft={iconElementLeft}
                                iconElementRight={iconElementRight}
                                showMenuIconButton={true}
                                className="appBar"/>
                            {navBarElements}
                        </div>
                    </MuiThemeProvider>
                    <ToastContainer
                        toastMessageFactory={ToastMessageFactory}
                        ref="container"
                        className="toast-bottom-right"
                        style={styles.toastStyle}
                    />
                    <div style={containerStyleByDrawer}>{this.props.children}</div>
                    <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                        <div>
                        <Dialog
                            title="Registration success!"
                            modal={false}
                            open={this.state.openConfirmModal}
                            onRequestClose={this.handleCloseConfirmModal}/>
                        <ChooseOrganizationDialog
                            ref="ChooseOrganizationDialog"
                            orgArrForUserInput={this.state.orgArrForUserInput}
                            openChooseOrgDialog={this.state.openChooseOrgDialog}
                            curOrgIdx={this.state.curOrgIdx}
                            handleAfterOrgChoose={this.handleAfterOrgChoose.bind(this)}
                            handleOrgChange={this.handleOrgChange.bind(this)}
                            handleOrgSelect={this.handleOrgSelect.bind(this)}
                            handleOrgResults={this.handleOrgResults.bind(this)}
                            //handleOrgCloseDialog={this.handleOrgCloseDialog.bind(this)}
                            closeBtnEnabled={false}/>
                        </div>
                    </MuiThemeProvider>
                </div>
            </MuiThemeProvider>

        );
    }
}

export default App;

const Main = Loadable({
    loader: () => import('./app.jsx'),
    loading: () => null
});

const Dashboard = Loadable({
    loader: () => import('../dashboard/main.jsx'),
    loading: () => null
});

const Brand = Loadable({
    loader: () => import('../brandProtection/brand-main.jsx'),
    loading: () => null

});

const Overview = Loadable({
    loader: () => import('../dashboard/overview/overview-main.jsx'),
    loading: () => null
});

const Dataleaks = Loadable({
    loader: () => import('../dataleaks/dataleaks-main.jsx'),
    loading: () => null
});

const Blacklists = Loadable({
    loader: () => import('../blacklists/blacklists-main.jsx'),
    loading: () => null
});

const Botnets = Loadable({
    loader: () => import('../botnets/botnets-main.jsx'),
    loading: () => null
});

const ValidSSLCertificates = Loadable({
    loader: () => import('../sslCertificates/ssl-certs-valid.jsx'),
    loading: () => null
});

const NotValidSSLCertificates = Loadable({
    loader: () => import('../sslCertificates/ssl-certs-not-valid.jsx'),
    loading: () => null
});

const WifiNetworks = Loadable({
    loader: () => import('../wifiNetworks/wifi-networks-main.jsx'),
    loading: () => null
});

const GDPRCompliance = Loadable({
    loader: () => import('../gdprCompliance/gdpr-comply-main.jsx'),
    loading: () => null
});

const AccountSettingsPage = Loadable({
    loader: () => import('../accountSettings/account-settings-page.jsx'),
    loading: () => null
});

const UserPage = Loadable({
    loader: () => import('../app/user-page.jsx'),
    loading: () => null
});

const SetupLogin2fa = Loadable({
    loader: () => import('../login/setup-login-2fa.jsx'),
    loading: () => null
});

const Login = Loadable({
    loader: () => import('../login/login.jsx'),
    loading: () => null
});

const Login2fa = Loadable({
    loader: () => import('../login/login2fa.jsx'),
    loading: () => null
});

const Breaches = Loadable({
    loader: () => import('../breaches/breaches-main.jsx'),
    loading: () => null
});

const ManageProjects = Loadable({
    loader: () => import('../projects/manage-projects.jsx'),
    loading: () => null
});

const ManageCompanies = Loadable({
    loader: () => import('../projects/user-manage-forms.jsx'),
    loading: () => null
});

const NewProject = Loadable({
    loader: () => import('../projects/create-new-project.jsx'),
    loading: () => null
});

const SurveyContinue = Loadable({
    loader: () => import('../survey/run/survey-continue.jsx'),
    loading: () => null
});

const Survey = Loadable({
    loader: () => import('../survey/run/survey.jsx'),
    loading: () => null
});

const SurveyEditor = Loadable({
    loader: () => import('../survey/editor/survey-editor.jsx'),
    loading: () => null
});

const SurveyStart = Loadable({
    loader: () => import('../survey/run/survey-start-screen.jsx'),
    loading: () => null
});

const SurveyNew = Loadable({
    loader: () => import('../survey/run/survey-new.jsx'),
    loading: () => null
});

const SurveyWizard = Loadable({
    loader: () => import('../survey/wizard/survey-wizard.jsx'),
    loading: () => null
});

const Admin = Loadable({
    loader: () => import('../admin/admin-main.jsx'),
    loading: () => null
});

const CompanyDashboard = Loadable({
    loader: () => import('../companyDashboard/company-main.jsx'),
    loading: () => null
});

const Intelligence = Loadable({
    loader: () => import('../intelligence/intel-main.jsx'),
    loading: () => null
});

const ExportIntelReportSettings = Loadable({
    loader: () => import('../reports/intelReports/intel-report-main.jsx'),
    loading: () => null
});

const ExportCVEReportSettings = Loadable({
    loader: () => import('../reports/cveReports/cve-report-main.jsx'),
    loading: () => null
});

const ExportSurveyReportSettings = Loadable({
    loader: () => import('../reports/surveyReports/survey-report-main.jsx'),
    loading: () => null
});
ReactDOM.render((

    <Router history={browserHistory}>
        <Route path="/survey-continue" component={SurveyContinue}/>
        <Route path="/" component={Main}>
            <IndexRoute component={Login}/>
            <Route path="/brand" component={Brand}/>
            <Route path="/overview" component={Overview}/>
            <Route path="/dataleaks" component={Dataleaks}/>
            <Route path="/blacklists" component={Blacklists}/>
            <Route path="/botnets" component={Botnets}/>
            <Route path="/ssl-certs-discovery" component={ValidSSLCertificates}/>
            <Route path="/ssl-certs-vulnerability" component={NotValidSSLCertificates}/>
            <Route path="/wifi-networks" component={WifiNetworks}/>
            <Route path="/gdpr-compliance" component={GDPRCompliance}/>
            <Route path="/login" component={Login}/>
            <Route path="/login2fa" component={Login2fa}/>
            <Route path="/setupLogin2fa" component={SetupLogin2fa}/>
            <Route path="/account-settings" component={AccountSettingsPage}/>
            <Route path="/users-:userId" component={UserPage}/>
            <Route path="/admin" component={Admin} onEnter={isAuth}/>
            {/* <Route path="/intel" component={Intelligence}/>*/}
            <Route path="/intel/:filter" component={Intelligence}/>
            <Route path="/intel/:cat/:type/:filter" component={Intelligence}/>
            <Route path="/breaches" component={Breaches}/>
            <Route path="/dashboard(/:project)" component={Dashboard}/>
            <Route path="/company-dashboard/:companyID" component={CompanyDashboard}/>
            <Route path="/survey-wizard" component={SurveyWizard}/>
            <Route path="/survey" component={Survey}/>
            <Route path="/survey-editor" component={SurveyEditor}/>
            <Route path="/survey-start" component={SurveyStart}/>
            <Route path="/survey-new" component={SurveyNew}/>
            <Route path="/report-new" component={ExportIntelReportSettings}/>
            <Route path="/cve-report-new" component={ExportCVEReportSettings}/>
            <Route path="/survey-report-new" component={ExportSurveyReportSettings}/>
            <Route path="/manage-projects" component={ManageProjects}/>
            <Route path="/manage-companies" component={ManageCompanies}/>
            <Route path="/create-new-project" component={NewProject}/>
        </Route>
    </Router>

), document.getElementById('container'));
