'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

let _colors = require('./colors');

let _spacing = require('./spacing');

let _spacing2 = _interopRequireDefault(_spacing);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  spacing: _spacing2.default,
  fontFamily: 'Barlow, sans-serif',
  stepper: {
    iconColor: '#0091ea',
    color: '#0091ea',
  },
  palette: {
    primary1Color: '#0091ea',
    primary2Color: '#0091ea',
    primary3Color: '#bfbfbf',
    accent1Color: '#0091ea',
    accent2Color: '#f5f5f5',
    accent3Color: '#757575',
    secondaryTextColor: '#444444',
    alternateTextColor: 'rgb(255, 255, 255)',
    disabledColor: '#9e9e9e',
    pickerHeaderColor: '#0091ea',
    clockCircleColor: '#00b0ff'
  }
};
