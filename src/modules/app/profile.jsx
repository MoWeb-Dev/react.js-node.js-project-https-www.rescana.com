import {Card, CardHeader, CardText} from 'material-ui/Card';
import React from 'react';
import {List, ListItem} from 'material-ui/List';
import FlatButton from 'material-ui/FlatButton';
import {browserHistory} from 'react-router';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import themeForFont from "../app/themes/themeForFont";

class Profile extends React.Component {
    constructor() {
        super();
        this.state = {
            apps: {},
            whiteList: [
                'google.com',
                'yandex.ru',
                'amazon.com',
                'facebook.com'
            ],
            user: app.getAuthUser()
        };
    }

    componentDidMount() {
        if (!this.state.user) {
            app.routeAuthUser();
            return;
        }

        this.getAppsData();
        this.getWhiteListData();
    }

    getAppsData() {
        $.ajax({
            type: 'get',
            url: '/api/getAppNames',
            data: {user_id: this.state.user.id}
        }).done((data) => {
            this.setState({apps: data});
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    getWhiteListData() {
        $.ajax({
            type: 'get',
            url: '/api/getAppNames',
            data: {user_id: this.state.user.id}
        }).done((data) => {
            this.setState({whiteList: data});
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    handleBack() {
        browserHistory.goBack();
    }

    render() {
        let appsData = this.state.apps;
        let whiteListData = this.state.whiteList;
        let userName = null;
        let userImage;
        if (this.state.user) {
            userName = this.state.user.username;
            userImage = this.state.user.image;
        }
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div style={{
                    backgroundColor: 'grey200',
                    margin: '0 180px'
                }}>
                    <Card style={{margin: 10}}>
                        <CardHeader
                            title={userName}
                            avatar={userImage}
                        >
                            <FlatButton
                                label="Back"
                                style={{
                                    float: 'right',
                                    backgroundColor: '#00bcd4'
                                }}
                                onClick={this.handleBack}
                            />
                        </CardHeader>

                        <CardText>
                            Contacts:
                        </CardText>
                        <CardText>
                            ?
                        </CardText>
                    </Card>
                    <Card style={{margin: 10}}>
                        <CardHeader
                            title="Sites"
                        />
                        <List>
                            {
                                Object.keys(appsData).map((key) => {
                                    return (
                                        <ListItem
                                            key={key}
                                            primaryText={appsData[key]}
                                        />
                                    );
                                })
                            }
                        </List>
                    </Card>
                    <Card style={{margin: 10}}>
                        <CardHeader
                            title="White List"
                        />
                        <List>
                            {
                                whiteListData.map((item, key) => {
                                    return (
                                        <ListItem
                                            key={key}
                                            primaryText={item}
                                        />
                                    );
                                })
                            }
                        </List>
                    </Card>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default Profile;
