import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Menu from 'react-mfb/build/menu';
import MainButton from 'react-mfb/build/main-button';
import ChildButton from 'react-mfb/build/child-button';
import TextField from 'material-ui/TextField';
import '../../../node_modules/react-mfb/mfb.css';

export default class DialogWithAddNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            openAddNewModal: false,
            openAddWatchModal: false,
            appUrl: null,
            watchUrl: null,
            aid: null
        };

        this.handleCloseAddNewModal = this.handleCloseAddNewModal.bind(this);
        this.handleOpenAddNewModal = this.handleOpenAddNewModal.bind(this);
        this.handleCloseAddWatchModal = this.handleCloseAddWatchModal.bind(this);
        this.handleOpenAddWatchModal = this.handleOpenAddWatchModal.bind(this);

        this.saveApp = this.saveApp.bind(this);
        this.saveToWatchList = this.saveToWatchList.bind(this);

        this.handleChangeAppUrlField = this.handleChangeAppUrlField.bind(this);
        this.handleChangeWatchUrlField = this.handleChangeWatchUrlField.bind(this);
    }

    handleCloseAddNewModal() {
        this.setState({openAddNewModal: false});
    }

    handleOpenAddNewModal() {
        this.setState({openAddNewModal: true});
    }

    handleCloseAddWatchModal() {
        this.setState({openAddWatchModal: false});
    }

    handleOpenAddWatchModal() {
        this.setState({openAddWatchModal: true});
    }

    handleChangeAppUrlField(e) {
        this.setState({appUrl: e.target.value});
    }

    handleChangeWatchUrlField(e) {
        this.setState({watchUrl: e.target.value});
    }

    saveApp() {
        let data = {
            url: this.state.appUrl,
            pName: localStorage.getItem('pn'),
	        timestamp: new Date().getTime()
        };
        $.ajax({
            type: 'get',
            url: '/api/enrich',
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8'
        }).done((data) => {
            this.handleCloseAddNewModal();
            if (data) {
                this.props.brandComponent.appList.getData();
            }
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    saveToWatchList() {
        let data = {
            aid: this.state.aid,
            pName: localStorage.getItem('pn'),
            url: this.state.watchUrl
        };

        $.ajax({
            type: 'get',
            url: '/api/watch',
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8'
        }).done((data) => {
            this.handleCloseAddWatchModal();
            if (data) {
                this.props.brandComponent.appList.getFeed(this.state.aid);
            }
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    render() {
        this.props.brandComponent.mfbCommands = this;

        // const styles = {
        //     ButtonStyle: {
        //         position: 'fixed',
        //         bottom: 0,
        //         right: 0,
        //         marginBottom: 20,
        //         marginRight: 20
        //     },
        //     radioButton: {
        //         marginBottom: 16
        //     },
        //     btnGroup: {
        //         marginTop: 15
        //     }
        // };

        return (
            <div>
                <Dialog
                    title="Domain Protection"
                    actions={[
                        <FlatButton
                            label="Ok"
                            primary={true}
                            keyboardFocused={true}
                            onTouchTap={this.saveApp}
                        />
                    ]}
                    modal={false}
                    open={this.state.openAddNewModal}
                    onRequestClose={this.handleCloseAddNewModal}
                >
                    Please enter the domain you wish to protect.<br/>
                    Results will appear shortly.<br/>
                    <TextField
                        hintText="http://"
                        onChange={this.handleChangeAppUrlField}
                    />
                </Dialog>
                <Dialog
                    title="Add To WatchList"
                    actions={[
                        <FlatButton
                            label="Ok"
                            primary={true}
                            keyboardFocused={true}
                            onTouchTap={this.saveToWatchList}
                        />
                    ]}
                    modal={false}
                    open={this.state.openAddWatchModal}
                    onRequestClose={this.handleCloseAddWatchModal}
                >
                    <TextField
                        hintText="App Name"
                        onChange={this.handleChangeWatchUrlField}
                    />
                </Dialog>
                <Menu effect="zoomin" method="hover" position="br">
                    <MainButton iconResting="ion-plus-round" iconActive="ion-close-round" />
                    <ChildButton
                        icon="ion-plus-round"
                        label="Add new app"
                        onClick={this.handleOpenAddNewModal} />
                    <ChildButton
                        icon="ion-plus-round"
                        label="Add to watchlist"
                        onClick={this.handleOpenAddWatchModal}
                        disabled={!this.state.aid} />
                </Menu>
            </div>
        );
    }
}
