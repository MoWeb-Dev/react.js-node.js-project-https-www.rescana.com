import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';


export default class DeleteDialog extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return <Dialog
            title={'Delete'}
            anchorOrigin={{horizontal: 'middle', vertical: 'center'}}
            open={this.props.open}
            modal={false}
            onRequestClose={this.props.onRequestClose}
            actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
            bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
            autoScrollBodyContent={true}
            repositionOnUpdate={false}
            contentStyle={{maxWidth: '30%', borderRadius: '7px 7px 7px 7px'}}
            titleStyle={{
                fontSize: 18,
                background: 'rgba(0,0,0,0.7)',
                color: 'white', textAlign: 'center',
                borderRadius: '2px 2px 0px 0px',
                textTransform: 'uppercase',
            }}
            actions={[
                <FlatButton
                    label="Ok"
                    primary={true}
                    style={{
                        color: 'white',
                        marginBottom: '5px',
                        marginRight: '5px',
                        height: 40
                    }}
                    keyboardFocused={true}
                    backgroundColor={'#0091ea'}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    onTouchTap={this.props.onDelete}
                />,
                <FlatButton
                    label="Cancel"
                    primary={true}
                    backgroundColor={'#0091ea'}
                    style={{marginRight: '5px', color: 'white', marginBottom: '5px', height: 40}}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    keyboardFocused={false}
                    onTouchTap={this.props.onRequestClose}
                />
            ]}>
            <br/>
            Are you sure you wish to delete?
            <br/>
        </Dialog>;
    }
}
