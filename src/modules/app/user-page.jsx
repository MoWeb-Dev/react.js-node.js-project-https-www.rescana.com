import {Card, CardHeader, CardText} from 'material-ui/Card';
import React from 'react';
import {List, ListItem} from 'material-ui/List';
import Delete from 'material-ui/svg-icons/action/delete';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import themeForFont from "../app/themes/themeForFont";


class UserPage extends React.Component {
    constructor() {
        super();
        this.state = {
            watchList: [],
            whiteList: [{id: 1, url: 'example.com'}, {id: 2, url: 'example.org'}],
            user: null,
            authUser: app.getAuthUser()
        };
    }

    componentDidMount() {
        if (!(this.state.authUser && this.state.user.username.indexOf('a1') !== -1)) {
            app.routeAuthUser();
            return;
        }

        this.getUserData();
        this.getWhiteListData();
        this.getWatchListData();
    }

    getUserData() {
        $.ajax({
            type: 'get',
            url: '/api/users',
            data: {id: this.props.params.userId},
            dataType: 'json'
        }).done((data) => {
            this.setState({user: data});
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    getWatchListData() {
        $.ajax({
            type: 'get',
            url: '/api/watchList',
            data: {user_id: this.props.params.userId},
            dataType: 'json'
        }).done((data) => {
            this.setState({watchList: data});
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    getWhiteListData() {
        $.ajax({
            type: 'get',
            url: '/api/whiteList',
            data: {user_id: this.props.params.userId},
            dataType: 'json'
        }).done((data) => {
            this.setState({whiteList: data});
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    removeFromWatchList(id) {
        $.ajax({
            type: 'delete',
            url: '/api/whiteList/' + id,
            dataType: 'json'
        }).done(() => {
            this.getWatchListData();
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    removeFromWhiteList(id) {
        $.ajax({
            type: 'delete',
            url: '/api/whiteList/' + id,
            dataType: 'json'
        }).done(() => {
            this.getWhiteListData();
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
    }

    editWhiteList() {}

    editWatchList() {}

    render() {
        let watchListData = this.state.watchList;
        let whiteListData = this.state.whiteList;
        let userName = null;
        let userImage;
        if (this.state.user) {
            userName = this.state.user.username;
            userImage = this.state.user.image;
        }
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div style={{
                    backgroundColor: 'grey200',
                    margin: '0 180px'
                }}>
                    <Card style={{margin: 10}}>
                        <CardHeader
                            title={userName}
                            avatar={userImage}
                        >
                        </CardHeader>

                        <CardText>
                            Contacts:
                        </CardText>
                        <CardText>
                            ?
                        </CardText>
                    </Card>
                    <Card style={{margin: 10}}>
                        <CardHeader
                            title="Watch List"
                        />
                        <List>
                            {
                                watchListData.map((rec) => {
                                    return (
                                        <ListItem
                                            key={rec.id}
                                            primaryText={rec.url}
                                            rightIcon={
                                                <div style={{width: 'initial'}}>
                                                    <Edit onClick={this.editWatchList.bind(this, rec.id)} />
                                                    <Delete onClick={this.removeFromWatchList.bind(this, rec.id)} />
                                                </div>
                                            }
                                        />
                                    );
                                })
                            }
                        </List>
                    </Card>
                    <Card style={{margin: 10}}>
                        <CardHeader
                            title="White List"
                        />
                        <List>
                            {
                                whiteListData.map((rec) => {
                                    return (
                                        <ListItem
                                            key={rec.id}
                                            primaryText={rec.url}
                                            rightIcon={
                                                <div style={{width: 'initial'}}>
                                                    <Edit onClick={this.editWhiteList.bind(this, rec.id)} />
                                                    <Delete onClick={this.removeFromWhiteList.bind(this, rec.id)} />
                                                </div>
                                            }
                                        />
                                    );
                                })
                            }
                        </List>
                    </Card>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default UserPage;
