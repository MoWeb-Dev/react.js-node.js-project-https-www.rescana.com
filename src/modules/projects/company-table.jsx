import React from 'react';
import {Table, TableHeaderColumn, TableRow, TableHeader, TableRowColumn, TableBody} from 'material-ui/Table';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import IconButton from 'material-ui/IconButton';
import Chip from 'material-ui/Chip';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';

export default class companyTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            companies: []
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.companies) {
            this.setState({
                companies: nextProps.companies
            });
        }
    }

    renderSensitivity(sensitivity) {
        if (sensitivity) {
            let sensStr = '';
            let color = '';
            if (sensitivity === 1) {
                sensStr = 'Low Sensitivity';
                color = '#0091ea';
            } else if (sensitivity === 2) {
                sensStr = 'Moderate Sensitivity';
                color = '#ffa000';
            } else if (sensitivity === 3) {
                sensStr = 'High Sensitivity';
                color = '#f17200';
            } else if (sensitivity === 4) {
                sensStr = 'Very High Sensitivity';
                color = '#D92E2E';
            }
            return <Chip backgroundColor={color} labelColor={'#FFFFFF'}>{sensStr}</Chip>;
        }
    }


    render() {
        let user = app.getAuthUser();
        let isUserAllowedToAddVendors = false;
        if(user && user.isUserAllowedToAddVendors){
            isUserAllowedToAddVendors =  user.isUserAllowedToAddVendors
        }
        if(user.userRole && user.userRole === "admin"){
            isUserAllowedToAddVendors = true
        }

        const styles = {
            tableStyle: {
                paddingTop: '8px',
                paddingBottom: '8px'
            }
        };

        return (
            <Table
                fixedHeader={true}
                selectable={false}
                onRowSelection={this.handleRowSelection}
            >
                <TableHeader
                    style={{marginLeft: '5px'}}
                    displaySelectAll={false}
                    adjustForCheckbox={false}
                >
                    <TableRow>
                        <TableHeaderColumn style={{marginLeft: '5px'}}>Company Name</TableHeaderColumn>
                        <TableHeaderColumn>Create Date</TableHeaderColumn>
                        <TableHeaderColumn>Domains</TableHeaderColumn>
                        <TableHeaderColumn>Sensitivity</TableHeaderColumn>
                        <TableHeaderColumn></TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody
                    showRowHover={true}
                    stripedRows={false}
                    displayRowCheckbox={false}
                >
                    {this.state.companies && this.state.companies.map((row) => (
                        <TableRow style={{marginLeft: '5px'}} style={styles.tableStyle} key={row.id}>
                            <TableRowColumn style={styles.tableStyle}>{row.companyName}</TableRowColumn>
                            <TableRowColumn style={styles.tableStyle}>{row.createDate}</TableRowColumn>
                            <TableRowColumn style={styles.tableStyle}>
                                {showExpandedDataChip(row.selectedDomains, 4, 5)}
                            </TableRowColumn>
                            <TableRowColumn style={styles.tableStyle}>{this.renderSensitivity(row.sensitivity)}</TableRowColumn>
                            <TableRowColumn style={styles.tableStyle}>
                                {isUserAllowedToAddVendors && row.edit ? <IconButton onClick={(e) => this.props.editCompanyByUser(row.id, e)} name="editCompany">
                                    <Edit/>
                                </IconButton> : ''}
                            </TableRowColumn>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        );
    }
}
