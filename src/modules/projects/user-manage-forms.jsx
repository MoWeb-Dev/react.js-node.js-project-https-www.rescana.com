import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import ManageCompanyForm from './manage-companies.jsx';
import moment from 'moment';
import themeForFont from "../app/themes/themeForFont";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import {ORGANIZATION_ID, ORGANIZATION_NAME, SESSION_ADD_NEW_COMP_BY_USER} from "../../../config/consts";
import ActionAdd from 'material-ui/svg-icons/content/add-circle';

class UserManageForms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            curOrg: {},
            projects: [],
            users: [],
            companies: [],
            projectsArrForUserInput: [],
            inCompanyEditMode: false
        };

        this.addCompany = this.addCompany.bind(this);
    }

    componentWillMount() {
        app.setFlagVisibile(false);
        let loadedOrgId = localStorage.getItem(ORGANIZATION_ID);
        if (loadedOrgId) {
            this.getOrgById(loadedOrgId);
        }
        this.getProjects(loadedOrgId);
        this.getAllCompaniesById();

        let isFromAddNewCompanyButton = sessionStorage.getItem(SESSION_ADD_NEW_COMP_BY_USER);
        if (isFromAddNewCompanyButton === 'true') {
            this.setState({openAddNewCompanyModal: true, inCompanyEditMode: false})
        }
    }

    getOrgById(loadedOrgId) {
        let user = app.getAuthUser();
        if (!user) {
            app.routeAuthUser();
        } else {
            const data = {orgId: loadedOrgId};
            $.ajax({
                type: 'POST',
                url: '/api/getOrgById',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.org) {
                    this.setState({
                        curOrg: data.org
                    });
                } else {
                    app.addAlert('error', 'Error: failed to load Organization.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load Organization.');
            });
        }
    }

    getAllCompaniesById() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'GET',
                url: '/api/getAllCompaniesPartialById',
                data: {isInUserCompanyManagement: true},
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((companies) => {
                this.setState({
                    companies: companies
                });
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load companies.');
            });
        }
    }

    getProjects(orgId) {
        let data = {};
        let url = '';
        let type = '';

        if (orgId) {
            url = '/api/getProjectsByOrgId';
            type = 'POST';
            data.orgId = orgId;
        } else {
            url = '/api/getProjectsByUserId';
            type = 'get';
        }

        $.ajax({
            type: type,
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projectsArrForUserInput) => {
            if (projectsArrForUserInput) {
                this.setState({
                    projectsArrForUserInput: projectsArrForUserInput
                });
            } else {
                app.addAlert('error', 'Error: failed to load projects.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
        });
    }


    addCompany(companyFields, deletedChipArray, addedChipArray, newProjectsCreatedByUser, inCompanyEditMode) {
        let user = app.getAuthUser();
        if (!user) {
            app.routeAuthUser();
        } else if (user.userRole === "admin") {
            app.addAlert('warning', 'You are admin user! please manage companies from admin page.');
        } else {
            let data = companyFields;
            let extraData = {};

            let orgId = localStorage.getItem(ORGANIZATION_ID);
            let orgName = localStorage.getItem(ORGANIZATION_NAME);

            if (orgId && orgName) {
                companyFields.chosenOrg.id = orgId;
                companyFields.chosenOrg.organizationName = orgName;
            }
            if (inCompanyEditMode) {
                extraData.isInUserEditMode = inCompanyEditMode;
            }
            companyFields.allowedUsers = [{email: user.email, id: user.id}];
            companyFields.allowedEditUsers = [{email: user.email, id: user.id}];

            if (this.state.curOrg && this.state.curOrg[0]) {
                if (this.state.curOrg[0].allowedUsers) {
                    companyFields.allowedUsers = this.state.curOrg[0].allowedUsers.map((item) => {
                        return {email: item.email, id: item.id};
                    });
                    companyFields.allowedEditUsers = companyFields.allowedUsers;
                }
            }

            if (deletedChipArray) {
                extraData.detachArrayToAddFromNeo = addedChipArray;
            }
            if (deletedChipArray) {
                extraData.detachArrayToRemoveFromNeo = deletedChipArray;
            }
            if (!companyFields.sensitivity) {
                companyFields.sensitivity = 2;
            }
            data.createDate = moment().format();
            if (!data.ratios.surveyWeight) {
                data.ratios.surveyWeight = 0;
            }

            if (!data.ratios.intelWeight) {
                data.ratios.intelWeight = 0;
            }

            if(newProjectsCreatedByUser || companyFields.chosenProj){
                extraData.createdProjects = newProjectsCreatedByUser || companyFields.chosenProj;
            }

            $.ajax({
                type: 'POST',
                url: '/api/addCompany',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({data: data, extraData: extraData}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.newCompany) {
                    let companies = this.state.companies;
                    let companiesAfterAdd = companies;
                    let found = false;
                    for (let i = 0; i < companies.length; i++) {
                        if (companies[i].id === data.newCompany.id) {
                            companiesAfterAdd[i] = data.newCompany;
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        companiesAfterAdd.push(data.newCompany);
                    }
                    this.setState({companies: companiesAfterAdd});
                    app.addAlert('success', 'Done');
                } else if (data && data.error) {
                    app.addAlert('error', data.error);
                } else {
                    app.addAlert('error', 'Error: failed to Edit company.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add company.');
            });
        }
        this.setState({inCompanyEditMode: false})
    }

    render() {
        let user = app.getAuthUser();
        let isUserAllowedToAddVendors = false;
        if (user && user.isUserAllowedToAddVendors) {
            isUserAllowedToAddVendors = user.isUserAllowedToAddVendors
        }
        else if(user && user.admin) {
            isUserAllowedToAddVendors = true;
        }

        const styles = {
            header: {}
        };

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    {isUserAllowedToAddVendors && <FlatButton
                        label="Add New Company"
                        primary={true}
                        labelStyle={{fontSize: 13}}
                        style={{
                            top: 100,
                            right: 50,
                            float: 'right',
                            borderRadius: 6,
                            height: 40,
                            color: 'white',
                        }}
                        icon={<ActionAdd/>}
                        backgroundColor={'rgba(0,0,0,0.7'}
                        hoverColor={'#0091ea'}
                        onTouchTap={() => {
                            if (user.userRole === "admin") {
                                app.addAlert('warning', 'You are admin user! please manage companies from admin page.');
                            } else {
                                this.refs.companyManagement.handleOpenAddNewCompanyModal()
                            }
                        }}
                    />
                    }
                    <div style={styles.tabContent}>
                        <ManageCompanyForm
                            openAddNewCompanyModal={this.state.openAddNewCompanyModal}
                            ref="companyManagement"
                            identifier="companies"
                            curOrg={this.state.curOrg}
                            projectsArrForUserInput={this.state.projectsArrForUserInput}
                            projects={this.state.projects}
                            companies={this.state.companies}
                            addCompany={this.addCompany.bind(this)}
                            inCompanyEditMode={this.state.inCompanyEditMode}
                        />
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default UserManageForms;
