import React from 'react';
import {Card, CardHeader, CardTitle} from 'material-ui/Card';
import CompaniesTable from './company-table.jsx';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import classNames from 'classnames';
import {DEFAULT_INTEL_SCORE_RATIOS} from '../../../config/consts.js';
import themeForFont from "../app/themes/themeForFont";
import AddNewCompanyByUserModal from "../admin/manageCompanies/add-company-modal.jsx";
import {SESSION_ADD_NEW_COMP_BY_USER} from "../../../config/consts";


class ManageCompanyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openAddNewCompanyModal: this.props.openAddNewCompanyModal || false,
            companies: this.props.companies,
            projectsArrForUserInput: this.props.projectsArrForUserInput,
            users: this.props.users,
            companyFields: {
                companyName: '',
                allowedUsers: [],
                allowedEditUsers: [], // Users that are allowed to change company details.
                selectedDomains: [],
                ratios: {
                    surveyWeight: 0,
                    intelWeight: 100
                },
                intelScoreRatios: DEFAULT_INTEL_SCORE_RATIOS,
                sensitivity: 2,
                keywords: [],
                createDate: '',
                companyInfo: {},
                chosenOrg: {
                    id: '',
                    organizationName: ''
                }
            },
            curProjectIdx: 0,
            inCompanyEditMode: this.props.inCompanyEditMode || false
        };

        this.handleOpenAddNewCompanyModal = this.handleOpenAddNewCompanyModal.bind(this);
    }

    handleOpenAddNewCompanyModal() {

        this.clearAllModalFields();
        this.setState({openAddNewCompanyModal: true});
    }

    componentWillReceiveProps(nextProps, nextContext) {
        let isFromAddNewCompanyButton = sessionStorage.getItem(SESSION_ADD_NEW_COMP_BY_USER);
        if (isFromAddNewCompanyButton === 'true') {
            this.setState({
                openAddNewCompanyModal: nextProps.openAddNewCompanyModal || true,
                inCompanyEditMode: this.props.inCompanyEditMode
            });
            sessionStorage.setItem(SESSION_ADD_NEW_COMP_BY_USER, 'false');
        }
        if (nextProps.companies) {
            this.setState({
                companies: nextProps.companies
            });
        }

        if (nextProps.projectsArrForUserInput) {
            this.setState({
                projectsArrForUserInput: nextProps.projectsArrForUserInput
            });
        }
    }

    clearAllModalFields() {
        let user = app.getAuthUser();
        let companyFields = this.state.companyFields;
        companyFields.allowedUsers = [];
        companyFields.allowedEditUsers = [];
        if (user) {
            companyFields.allowedUsers.push(user);
            companyFields.allowedEditUsers.push(user);
        }
        companyFields.companyName = '';
        companyFields.selectedDomains = [];
        companyFields.keywords = [];
        companyFields.ratios = {
            surveyWeight: 0,
            intelWeight: 100
        };
        companyFields.intelScoreRatios = DEFAULT_INTEL_SCORE_RATIOS;
        companyFields.sensitivity = '';
        companyFields.createDate = '';
        companyFields.id = '';
        companyFields.companyInfo = {};
        companyFields.chosenProj = {};

        this.setState({
            curProjectIdx: 0,
            openAddNewCompanyModal: false,
            companyFields: companyFields,
            inCompanyEditMode: false
        });
    }

    editCompanyByUser(companyId, e) {
        e.preventDefault();
        let companyFields = this.state.companyFields;

        function matchesEl(el) {
            return el.id === companyId;
        }

        let company = this.props.companies.filter((companyId) => {
            return matchesEl(companyId);
        });

        companyFields.companyName = company[0].companyName;
        companyFields.allowedUsers = company[0].allowedUsers;
        companyFields.allowedEditUsers = company[0].allowedEditUsers;
        companyFields.selectedDomains = company[0].selectedDomains;
        companyFields.sensitivity = company[0].sensitivity;
        companyFields.ratios = company[0].ratios;
        companyFields.intelScoreRatios = company[0].intelScoreRatios;
        companyFields.id = company[0].id;
        companyFields.companyInfo = company[0].extraData || {};

        let projObj = this.findRelatedCompanyProject(company[0].id);
        let curProjectIdx = 0;
        if (projObj && projObj.chosenProj && (projObj.projectIdx || projObj.projectIdx === 0)) {
            companyFields.chosenProj = projObj.chosenProj;
            curProjectIdx = projObj.projectIdx;
        }

        this.setState({
            curProjectIdx: curProjectIdx,
            openAddNewCompanyModal: true,
            companyFields: companyFields,
            inCompanyEditMode: true
        });
    }

    findRelatedCompanyProject(compId) {
        let projObj = {};
        if (this.state.projectsArrForUserInput) {
            for (let i = 0; i < this.state.projectsArrForUserInput.length; i++) {
                if (this.state.projectsArrForUserInput[i].selectedCompanies) {
                    for (let j = 0; j < this.state.projectsArrForUserInput[i].selectedCompanies.length; j++) {
                        if (this.state.projectsArrForUserInput[i].selectedCompanies[j] &&
                            this.state.projectsArrForUserInput[i].selectedCompanies[j].id &&
                            this.state.projectsArrForUserInput[i].selectedCompanies[j].id === compId) {
                            projObj.chosenProj = this.state.projectsArrForUserInput[i];
                            projObj.projectIdx = i;
                            return projObj;
                        }
                    }
                }
            }
        }
        return projObj;
    }


    componentWillMount() {
        app.setFlagVisibile(false);
        let isFromAddNewCompanyButton = sessionStorage.getItem(SESSION_ADD_NEW_COMP_BY_USER);
        if (isFromAddNewCompanyButton === 'true') {
            this.handleOpenAddNewCompanyModal();
            sessionStorage.setItem(SESSION_ADD_NEW_COMP_BY_USER, 'false');
        }
    }

    addCompany(companyFields, deletedChipArray, addedChipArray, newProjectsCreatedByUser) {
        this.setState({openAddNewCompanyModal: false});
        this.props.addCompany(companyFields, deletedChipArray, addedChipArray, newProjectsCreatedByUser, this.state.inCompanyEditMode);
    }


    render() {
        let pageMainClass = classNames({
            'containerLTR': true
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div className={pageMainClass} style={{
                    backgroundColor: 'grey200'
                }}>
                    <CardTitle style={{margin: '42px 0px 10px 10px'}}>
                        <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                        <span style={{position: 'relative', top: '-6px', fontSize: 22}}>COMPANIES</span>
                    </CardTitle>
                    <Card style={{margin: '25px'}}>
                        <div>
                            <CompaniesTable
                                companies={this.props.companies}
                                deleteCompany={this.props.deleteCompany}
                                editCompanyByUser={this.editCompanyByUser.bind(this)}
                            />
                        </div>
                    </Card>
                    <AddNewCompanyByUserModal
                        addCompany={this.addCompany.bind(this)}
                        cancel={this.cancel}
                        curOrg={this.props.curOrg}
                        companyFields={this.state.companyFields}
                        companies={this.state.companies}
                        projects={this.props.projects}
                        projectsArrForUserInput={this.state.projectsArrForUserInput}
                        clearAllModalFields={this.clearAllModalFields.bind(this)}
                        allUsers={this.state.users}
                        openAddNewCompanyModal={this.state.openAddNewCompanyModal}
                        inCompanyEditMode={this.state.inCompanyEditMode}
                        curProjectIdx={this.state.curProjectIdx}
                    />
                </div>
            </MuiThemeProvider>
        );
    }
}

export default ManageCompanyForm;
