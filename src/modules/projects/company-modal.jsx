import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import React from 'react';
import {
    Step,
    Stepper,
    StepButton,
    StepContent
} from 'material-ui/Stepper';

import FlatButton from 'material-ui/FlatButton';
import ActionAdd from 'material-ui/svg-icons/content/add-circle';
import ReactTooltip from 'react-tooltip';
import {
    DEFAULT_INTEL_SCORE_RATIOS,
    COMPANY_CONTACT_TYPES, EMPTY_CONTACT,
    EMPTY_DATE_RANGE_FIELD,
    YES_NO_MODAL_TYPES,
    COMPANY_INFO_CLASSIFICATION_TYPES,
    COMPANY_INFO_SECTOR_TYPES
} from '../../../config/consts.js';
import EditIcon from 'material-ui/svg-icons/image/edit';
import EditContactsModal from '../common/companyEdit/edit-contacts-modal.jsx';
import EditInformationModal from '../common/companyEdit/edit-information-modal.jsx';
import YesNoModal from '../common/companyEdit/yes-no-modal.jsx';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete/index';
import Checkbox from 'material-ui/Checkbox';
import DatePicker from 'material-ui/DatePicker';
import DatePickerMain from '../common/datePicker/date-picker-main.jsx';
import {fixTimezoneRange} from '../common/CommonHelpers';
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../app/themes/themeForFont.js";
const moment = require('moment');
const NUM_OF_LAST_STEP = 4;
const ENTER_KEY = 'Enter';
import SelectField from 'material-ui/SelectField';
import SensitivityQuestionnaire from "../admin/manageCompanies/sensitivity-questionnaire.jsx";
import IconButton from 'material-ui/IconButton';
import '../../../public/css/customScrollbar.css';
import QuestionnaireIcon from 'material-ui/svg-icons/action/chrome-reader-mode.js';

export default class AddCompanyModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            openAddNewCompanyModal: false,
            openEditContactsModal: false,
            openEditInformationModal: false,
            companyFields: this.props.companyFields,
            usersForDD: this.props.allUsers,
            domainsInputVal: [],
            usersInputVal: [],
            usersEditInputVal: [],
            ratios: {
                surveyWeight: 50,
                intelWeight: 50
            },
            intelScoreRatios: DEFAULT_INTEL_SCORE_RATIOS,
            sensitivity: 2,
            keywordsInputVal: [],
            stepIndex: 0,
            companyResponsibles: [],
            companyContacts: [],
            companyResponsibleToAdd: JSON.parse(JSON.stringify(EMPTY_CONTACT)),
            companyContactToAdd: JSON.parse(JSON.stringify(EMPTY_CONTACT)),
            openYesNoModal: false,
            titleYesNoModal: '',
            messageYesNoModal: '',
            paramObjectYesNoModal: {},
            companyDescription: '',
            companyClassifications: [],
            companySectors: [],
            companyDateOfContact: JSON.parse(JSON.stringify(EMPTY_DATE_RANGE_FIELD)),
            companyDateOfSurvey: JSON.parse(JSON.stringify(EMPTY_DATE_RANGE_FIELD)),
            companyInformation: [],
            companyInformationToAdd: '',
            isQuestionnaireActive: false,
            isQuestionnaireStepVisible: false
        };

        this.addContactField = this.addContactField.bind(this);
        this.checkContactFieldsValid = this.checkContactFieldsValid.bind(this);
        this.addContactFieldsToArray = this.addContactFieldsToArray.bind(this);
        this.handleOpenEditContactsArray = this.handleOpenEditContactsArray.bind(this);
        this.handleCloseEditContactsArray = this.handleCloseEditContactsArray.bind(this);
        this.handleOpenEditInformationArray = this.handleOpenEditInformationArray.bind(this);
        this.handleCloseEditInformationArray = this.handleCloseEditInformationArray.bind(this);
        this.addInfoField = this.addInfoField.bind(this);
        this.removeInfoFieldArrayItem = this.removeInfoFieldArrayItem.bind(this);
        this.handleAddInformation = this.handleAddInformation.bind(this);
        this.handleAddContact = this.handleAddContact.bind(this);
        this.handleQuestionnaireBtn = this.handleQuestionnaireBtn.bind(this);
        this.handleSensitivityQuestionnaireFinish = this.handleSensitivityQuestionnaireFinish.bind(this);
        this.handleSensitivityQuestionnaireCancel = this.handleSensitivityQuestionnaireCancel.bind(this);
    }

    handleNext() {
        let stepIndex = this.state.stepIndex;
        if (stepIndex < NUM_OF_LAST_STEP) {
            this.setState({stepIndex: stepIndex + 1});
        }
    };

    handlePrev() {
        let stepIndex = this.state.stepIndex;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };


    handleQuestionnaireBtn() {
        let isQuestionnaireStepVisible = !this.state.isQuestionnaireStepVisible;
        if(this.state.isQuestionnaireActive){
            isQuestionnaireStepVisible = false;
        }
        this.setState({isQuestionnaireActive: !this.state.isQuestionnaireActive,
            isQuestionnaireStepVisible: isQuestionnaireStepVisible, finalSensitivityLevelText: ''});
    }


    handleChangeCompanyNameField(e) {
        let companyFields = this.state.companyFields;
        companyFields.companyName = e.target.value;
        this.setState(companyFields);
    }

    // This generic function assigns the proper contactField's value to matching contactType array.
    addContactField(contactType, contactField, value) {
        let stateFieldToAdd;
        if (contactType === COMPANY_CONTACT_TYPES.RESPONSIBLE) {
            stateFieldToAdd = 'companyResponsibleToAdd';
        } else {
            // this means type is regular 'CONTACT'.
            stateFieldToAdd = 'companyContactToAdd';
        }
        if (this.state.hasOwnProperty(stateFieldToAdd) && this.state[stateFieldToAdd].hasOwnProperty(contactField)) {
            let fieldToUpdate = this.state[stateFieldToAdd];
            fieldToUpdate[contactField] = value;
            let stateToUpdate = {};
            stateToUpdate[stateFieldToAdd] = fieldToUpdate;
            this.setState(stateToUpdate);
        }
    }

    // This generic function sets the infoField with the value (in case of array - pushing value as new item).
    addInfoField(infoField, value) {
        let stateToUpdate = {};
        if (this.state.hasOwnProperty(infoField) && Array.isArray(this.state[infoField])) {
            stateToUpdate[infoField] = this.state[infoField];
            stateToUpdate[infoField].push(value);
        } else {
            stateToUpdate[infoField] = value;
        }
        this.setState(stateToUpdate);
    }

    // This generic function removes the itemToDelete from infoField only in case it is an array.
    removeInfoFieldArrayItem(infoField, itemToDelete) {
        let stateToUpdate = {};
        if (this.state.hasOwnProperty(infoField) && Array.isArray(this.state[infoField])) {
            stateToUpdate[infoField] = this.state[infoField];
            let itemIndex = stateToUpdate[infoField].indexOf(itemToDelete);
            if (itemIndex > -1) {
                stateToUpdate[infoField].splice(itemIndex, 1);
                this.setState(stateToUpdate);
            }
        }
    }

    checkContactFieldsValid(contactType) {
        let isValid = false;
        let stateFieldNameToCheck;
        if (contactType === COMPANY_CONTACT_TYPES.RESPONSIBLE) {
            stateFieldNameToCheck = 'companyResponsibleToAdd';
        } else {
            // this means type is regular 'CONTACT'.
            stateFieldNameToCheck = 'companyContactToAdd';
        }
        let stateField = this.state[stateFieldNameToCheck];
        if (stateField) {
            let objKeys = Object.keys(stateField);
            if (objKeys && objKeys.length > 0) {
                for (let i = 0; i < objKeys.length; i++) {
                    // Check if at least one field has any value.
                    if (stateField[objKeys[i]]) {
                        isValid = true;
                        break;
                    }
                }
            }
        }
        return isValid;
    }

    checkInformationFieldValid() {
        return !!this.state.companyInformationToAdd;
    }

    addContactFieldsToArray(contactType) {
        let stateFieldToAdd;
        let stateArrayToAdd;
        let notifyType;

        if (contactType === COMPANY_CONTACT_TYPES.RESPONSIBLE) {
            stateFieldToAdd = 'companyResponsibleToAdd';
            stateArrayToAdd = 'companyResponsibles';
            notifyType = 'Auditor';
        } else {
            // this means type is regular 'CONTACT'.
            stateFieldToAdd = 'companyContactToAdd';
            stateArrayToAdd = 'companyContacts';
            notifyType = 'Contact';
        }

        let fieldToUpdate = this.state[stateArrayToAdd];
        fieldToUpdate.push(this.state[stateFieldToAdd]);
        let stateToUpdate = {};
        stateToUpdate[stateArrayToAdd] = fieldToUpdate;
        // Clear the field for future use.
        stateToUpdate[stateFieldToAdd] = JSON.parse(JSON.stringify(EMPTY_CONTACT));
        this.setState(stateToUpdate, () => {
            app.addAlert('success', 'Added ' + notifyType + ' Successfully');
        });
    }

    handleChangeContactEmailField(contactType, e) {
        this.addContactField(contactType, 'email', e.target.value);
    }

    handleChangeContactNameField(contactType, e) {
        this.addContactField(contactType, 'name', e.target.value);
    }

    handleChangeContactPhoneField(contactType, e) {
        this.addContactField(contactType, 'phone', e.target.value);
    }

    handleChangeContactPositionField(contactType, e) {
        this.addContactField(contactType, 'position', e.target.value);
    }

    handleAddContact(contactType, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        if (this.checkContactFieldsValid(contactType)) {
            this.addContactFieldsToArray(contactType);
        } else {
            app.addAlert('warning', 'No data was inserted');
        }
    }

    getCompanyInformationObject() {
        return {
            checked: false,
            label: this.state.companyInformationToAdd
        };
    }

    handleAddInformation(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        if (this.checkInformationFieldValid()) {
            this.addInfoField('companyInformation', this.getCompanyInformationObject());
            this.setState({companyInformationToAdd: ''}, () => {
                app.addAlert('success', 'Added Information Successfully');
            });
        } else {
            app.addAlert('warning', 'No data was inserted');
        }
    }

    handleChangeDescriptionField(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.addInfoField('companyDescription', e.target.value);
    }

    handleAddChipInfoField(infoFieldName, chip) {
        if (chip) {
            let dataSource;
            if (infoFieldName === 'companyClassifications') {
                dataSource = COMPANY_INFO_CLASSIFICATION_TYPES;
            } else {
                // Means infoFieldName is "companySectors"
                dataSource = COMPANY_INFO_SECTOR_TYPES;
            }
            let matchedType = dataSource.find((t) => {
                let lowerCaseType = t;
                if (lowerCaseType) {
                    lowerCaseType = lowerCaseType.toString().toLowerCase();
                }
                return lowerCaseType === chip.toString().toLowerCase();
            });

            if (matchedType && this.state[infoFieldName] && !this.state[infoFieldName].includes(matchedType)) {
                this.addInfoField(infoFieldName, matchedType);
            }
        }
    }

    handleDeleteChipItemInfoField(infoFieldName, chip) {
        if (chip && this.state[infoFieldName] && this.state[infoFieldName].includes(chip)) {
            this.removeInfoFieldArrayItem(infoFieldName, chip);
        }
    }

    handleChangeInformationField(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.addInfoField('companyInformationToAdd', e.target.value);
    }

    handleOpenEditContactsArray(contactType, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.setState({openEditContactsModal: true});
    }

    handleCloseEditContactsArray(contactType, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.setState({openEditContactsModal: false});
    }

    handleOpenEditInformationArray(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.setState({openEditInformationModal: true});
    }

    handleCloseEditInformationArray(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.setState({openEditInformationModal: false});
    }

    handleSaveContactsChanges(companyResponsibles, companyContacts) {
        let stateToUpdate = {openEditContactsModal: false};
        if (companyResponsibles && Array.isArray(companyResponsibles)) {
            stateToUpdate.companyResponsibles = companyResponsibles;
        }
        if (companyContacts && Array.isArray(companyContacts)) {
            stateToUpdate.companyContacts = companyContacts;
        }
        this.setState(stateToUpdate);
    }

    handleSaveInformationChanges(companyInformation) {
        let stateToUpdate = {openEditInformationModal: false};
        if (companyInformation && Array.isArray(companyInformation)) {
            stateToUpdate.companyInformation = companyInformation;
        }
        this.setState(stateToUpdate);
    }

    handleChangeInfoDateField(dateInfoField, startDate, endDate) {
        if (this.state[dateInfoField] && startDate && endDate) {
            this.state[dateInfoField].startDate = startDate;
            this.state[dateInfoField].endDate = endDate;
        }
    }

    handleChangeInfoSingleDateField(dateInfoField, ignore, newDate) {
        let fixedDate = fixTimezoneRange(newDate, true).format('LL');
        this.handleChangeInfoDateField(dateInfoField, fixedDate, fixedDate);
        // Force a rerender.
        this.forceUpdate();
    }

    handleChangeSurveyRatioField(e) {
        let companyFields;
        let val = parseInt(e.target.value);
        if (100 >= val && val >= 0) {
            companyFields = this.props.companyFields;
            companyFields.ratios = {};
            companyFields.ratios.surveyWeight = val;
            companyFields.ratios.intelWeight = 100 - val;
            this.setState({companyFields: companyFields});
        } else {
            app.addAlert('error', 'Error: ratio should be between 0-100.');
        }
    }

    handleChangeIntelRatioField(e) {
        let companyFields;
        let val = parseInt(e.target.value);
        if (100 >= val && val >= 0) {
            companyFields = this.props.companyFields;
            companyFields.ratios = {};
            companyFields.ratios.intelWeight = val;
            companyFields.ratios.surveyWeight = 100 - val;
            this.setState({companyFields: companyFields});
        } else {
            app.addAlert('error', 'Error: ratio should be between 0-100.');
        }
    }

    handleChangeIntelScoreRatioField(type, e) {
        let intelScoreRatios;
        let val = parseInt(e.target.value);
        if (100 >= val && val >= 0) {
            intelScoreRatios = this.state.intelScoreRatios;
            intelScoreRatios[type] = val;
            this.setState({intelScoreRatios: intelScoreRatios});
        } else {
            app.addAlert('error', 'Error: ratio should be between 0-100.');
        }
    }

    validateIntelScoreRatioField() {
        let isValid = true;
        if (this.state.intelScoreRatios) {
            let sum = 0;
            Object.values(this.state.intelScoreRatios).map((currVal) => {
                sum += currVal;
            });
            if (sum !== 100) {
                isValid = false;
                app.addAlert('error', 'intel score ratios must sum up to 100% (Current is ' + sum + '%)');
                // Navigate user to fix the error.
                if (this.state.stepIndex !== 1) {
                    this.setState({stepIndex: 1}, () => {
                        this.cveInput.focus();
                    });
                }
            } else {
                // If the results are valid - save on props component.
                this.props.companyFields.intelScoreRatios = this.state.intelScoreRatios;
            }
        }
        return isValid;
    }

    validateNoForgottenContacts() {
        let checkIfForgotten = (dataObject) => {
            let isForgotten = false;
            Object.keys(dataObject).map((currKey) => {
                if (currKey && dataObject[currKey]) {
                    isForgotten = true;
                }
            });
            return isForgotten;
        };
        let isResponsibleForgotten = !!(this.state.companyResponsibleToAdd && checkIfForgotten(this.state.companyResponsibleToAdd));
        let isContactForgotten = !!(this.state.companyContactToAdd && checkIfForgotten(this.state.companyContactToAdd));
        let isInformationForgotten = this.checkInformationFieldValid();
        return {
            isResponsibleForgotten: isResponsibleForgotten,
            isContactForgotten: isContactForgotten,
            isInformationForgotten: isInformationForgotten
        };
    }

    getYesNoModalResult(result, paramObject) {
        let stateObj = {
            openYesNoModal: false,
            titleYesNoModal: '',
            messageYesNoModal: '',
            paramObjectYesNoModal: {}
        };
        if (result === YES_NO_MODAL_TYPES.CANCEL) {
            this.setState(stateObj);
        } else {
            if (result === YES_NO_MODAL_TYPES.OK) {
                let forgotten = this.validateNoForgottenContacts();
                if (forgotten.isResponsibleForgotten) {
                    stateObj.companyResponsibles = this.state.companyResponsibles;
                    stateObj.companyResponsibles.push(this.state.companyResponsibleToAdd);
                    stateObj.companyResponsibleToAdd = JSON.parse(JSON.stringify(EMPTY_CONTACT));
                }
                if (forgotten.isContactForgotten) {
                    stateObj.companyContacts = this.state.companyContacts;
                    stateObj.companyContacts.push(this.state.companyContactToAdd);
                    stateObj.companyContactToAdd = JSON.parse(JSON.stringify(EMPTY_CONTACT));
                }
                if (forgotten.isInformationForgotten) {
                    stateObj.companyInformation = this.state.companyInformation;
                    stateObj.companyInformation.push(this.getCompanyInformationObject());
                    stateObj.companyInformationToAdd = '';
                }
            }
            this.setState(stateObj, () => {
                this.addValidCompany(paramObject);
            });
        }
    }

    addValidCompany(companyFields) {
        // this is a fix for companyInfo, since companyFields are loaded from props and not from state.
        if (this.state.companyContacts || this.state.companyResponsibles || this.state.companyInformation
            || this.state.companyDescription || this.state.companyClassifications) {
            companyFields.companyInfo = {
                companyContacts: this.state.companyContacts || [],
                companyResponsibles: this.state.companyResponsibles || [],
                companyInformation: this.state.companyInformation || [],
                companyDescription: this.state.companyDescription || '',
                companyClassifications: this.state.companyClassifications || [],
                companySectors: this.state.companySectors || []
            };

            let rangeDatesNotEmpty = (datesObj) => {
                return !!(datesObj && (datesObj.startDate || datesObj.endDate));
            };

            if (rangeDatesNotEmpty(this.state.companyDateOfContact)) {
                companyFields.companyInfo.companyDateOfContact = this.state.companyDateOfContact;
            }
            if (rangeDatesNotEmpty(this.state.companyDateOfSurvey)) {
                companyFields.companyInfo.companyDateOfSurvey = this.state.companyDateOfSurvey;
            }
        }
        this.props.addCompany(companyFields);
        this.setState({
            isQuestionnaireActive:false,
            isQuestionnaireStepVisible:false,
            finalSensitivityLevelText: ''
        })
    }

    handleSensitivityQuestionnaireFinish(finalSensitivityLevel, finalSensitivityLevelText) {
        this.onChangeSensitivityDropdown(null, null, finalSensitivityLevel);
        this.setState({finalSensitivityLevelText: finalSensitivityLevelText, isQuestionnaireStepVisible: false});
    }

    handleSensitivityQuestionnaireCancel() {
        this.setState({isQuestionnaireStepVisible: false, isQuestionnaireActive: false, finalSensitivityLevelText: ''});
    }

    addCompany(companyFields) {
        if (this.validateIntelScoreRatioField()) {
            let forgotten = this.validateNoForgottenContacts();
            if (forgotten.isResponsibleForgotten || forgotten.isContactForgotten || forgotten.isInformationForgotten) {
                // Open modal with matching message.
                let title = 'Forgotten ';
                let msg = 'It looks like ';
                let isBothForgotten = false;

                if (forgotten.isResponsibleForgotten) {
                    title += 'Auditor ';
                    msg += 'an Auditor ';
                    if (forgotten.isContactForgotten || forgotten.isInformationForgotten) {
                        title += '& ';
                        if (forgotten.isContactForgotten && forgotten.isInformationForgotten) {
                            msg += ', ';
                        } else {
                            msg += 'and ';
                        }
                        isBothForgotten = true;
                    }
                }
                if (forgotten.isContactForgotten) {
                    title += 'Company Contact ';
                    msg += 'a Company Contact ';
                    if (forgotten.isInformationForgotten) {
                        title += '& Company Information ';
                        msg += 'and a Company Information ';
                    }
                } else if (forgotten.isInformationForgotten) {
                    title += 'Company Information ';
                    msg += 'a Company Information ';
                }
                if (isBothForgotten) {
                    msg += 'were ';
                } else {
                    msg += 'was ';
                }
                msg += 'not added.\nDo you wish to add it now?';
                this.setState({
                    openYesNoModal: true,
                    titleYesNoModal: title,
                    messageYesNoModal: msg,
                    paramObjectYesNoModal: companyFields
                });
            } else {
                this.addValidCompany(companyFields);
            }
        }
    }

    onChangeSensitivityDropdown(event, index, value) {
        let companyFields = this.props.companyFields;
        companyFields.sensitivity = value;
        this.setState(companyFields);
    }

    handleChangeScanPriority(e, newScanPriority) {
        let companyFields = this.props.companyFields;
        companyFields.scanPriority = newScanPriority || this.state.companyFields.scanPriority;
        this.setState(companyFields);
    }

    cancel() {
        this.setState({openAddNewCompanyModal: false , isQuestionnaireActive:false, isQuestionnaireStepVisible:false, finalSensitivityLevelText: ''});
        this.props.clearAllModalFields();
    }

    componentWillReceiveProps(nextProps) {
        let stateObj = {open: nextProps.open};

        stateObj.openAddNewCompanyModal = nextProps.openAddNewCompanyModal;

        if (nextProps.companyFields.scanPriority) {
            stateObj.companyFields.scanPriority = nextProps.companyFields.scanPriority;
        }

        if (nextProps.companyFields.allowedUsers) {
            stateObj.usersInputVal = nextProps.companyFields.allowedUsers;
        }

        if (nextProps.companyFields.allowedEditUsers) {
            stateObj.usersEditInputVal = nextProps.companyFields.allowedEditUsers;
        }

        if (nextProps.companyFields.sensitivity) {
            stateObj.sensitivity = nextProps.companyFields.sensitivity;
        }

        if (nextProps.companyFields.ratios) {
            stateObj.ratios = {};
            stateObj.ratios.intelWeight = nextProps.companyFields.ratios.intelWeight;
            stateObj.ratios.surveyWeight = nextProps.companyFields.ratios.surveyWeight;
        }

        if (nextProps.companyFields.intelScoreRatios) {
            stateObj.intelScoreRatios = nextProps.companyFields.intelScoreRatios;

            // Iterate on all supposed-to-exist intelScoreRatios to verify there is a value for each one.
            Object.keys(DEFAULT_INTEL_SCORE_RATIOS).map((currKey) => {
                if (currKey && stateObj.intelScoreRatios[currKey] == null) {
                    stateObj.intelScoreRatios[currKey] = 0;
                }
            });
        } else {
            stateObj.intelScoreRatios = DEFAULT_INTEL_SCORE_RATIOS;
        }

        if (nextProps.companyFields.selectedDomains) {
            stateObj.domainsInputVal = nextProps.companyFields.selectedDomains;
        }

        if (nextProps.companyFields.companyResponsibles) {
            stateObj.companyResponsibles = nextProps.companyFields.companyResponsibles;
        } else {
            stateObj.companyResponsibles = [];
        }

        if (nextProps.companyFields.companyContacts) {
            stateObj.companyContacts = nextProps.companyFields.companyContacts;
        } else {
            stateObj.companyContacts = [];
        }

        if (nextProps.companyFields.companyInformation) {
            stateObj.companyInformation = nextProps.companyFields.companyInformation;
        } else {
            stateObj.companyInformation = [];
        }

        if (nextProps.companyFields.companyDescription) {
            stateObj.companyDescription = nextProps.companyFields.companyDescription;
        } else {
            stateObj.companyDescription = '';
        }

        if (nextProps.companyFields.companyClassifications) {
            stateObj.companyClassifications = nextProps.companyFields.companyClassifications;
        } else {
            stateObj.companyClassifications = [];
        }

        if (nextProps.companyFields.companySectors) {
            stateObj.companySectors = nextProps.companyFields.companySectors;
        } else {
            stateObj.companySectors = [];
        }

        if (nextProps.companyFields.companyDateOfContact) {
            stateObj.companyDateOfContact = nextProps.companyFields.companyDateOfContact;
        } else {
            stateObj.companyDateOfContact = JSON.parse(JSON.stringify(EMPTY_DATE_RANGE_FIELD));
        }

        if (nextProps.companyFields.companyDateOfSurvey) {
            stateObj.companyDateOfSurvey = nextProps.companyFields.companyDateOfSurvey;
        } else {
            stateObj.companyDateOfSurvey = JSON.parse(JSON.stringify(EMPTY_DATE_RANGE_FIELD));
        }

        if (nextProps.companyFields.companyInfo) {
            if (nextProps.companyFields.companyInfo.responsibles) {
                stateObj.companyResponsibles = nextProps.companyFields.companyInfo.responsibles;
            }
            if (nextProps.companyFields.companyInfo.contacts) {
                stateObj.companyContacts = nextProps.companyFields.companyInfo.contacts;
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('informations')
                && Array.isArray(nextProps.companyFields.companyInfo.informations)) {
                stateObj.companyInformation = [];
                nextProps.companyFields.companyInfo.informations.map((currInfo) => {
                    if (currInfo && currInfo.label) {
                        // If no 'checked' property exists - set as false.
                        currInfo.checked = !!currInfo.checked;
                        stateObj.companyInformation.push(currInfo);
                    }
                });
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('classifications')
                && Array.isArray(nextProps.companyFields.companyInfo.classifications)) {
                stateObj.companyClassifications = [];
                nextProps.companyFields.companyInfo.classifications.map((currClass) => {
                    if (currClass && currClass.label) {
                        stateObj.companyClassifications.push(currClass.label);
                    }
                });
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('sectors')
                && Array.isArray(nextProps.companyFields.companyInfo.sectors)) {
                stateObj.companySectors = [];
                nextProps.companyFields.companyInfo.sectors.map((currSector) => {
                    if (currSector && currSector.label) {
                        stateObj.companySectors.push(currSector.label);
                    }
                });
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('date_of_contacts')
                && Array.isArray(nextProps.companyFields.companyInfo['date_of_contacts'])
                && nextProps.companyFields.companyInfo['date_of_contacts'].length === 1) {
                let rangeDates = nextProps.companyFields.companyInfo['date_of_contacts'][0];
                if (rangeDates && rangeDates.startDate && rangeDates.endDate) {
                    stateObj.companyDateOfContact = rangeDates;
                }
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('date_of_surveys')
                && Array.isArray(nextProps.companyFields.companyInfo['date_of_surveys'])
                && nextProps.companyFields.companyInfo['date_of_surveys'].length === 1) {
                let rangeDates = nextProps.companyFields.companyInfo['date_of_surveys'][0];
                if (rangeDates && rangeDates.startDate && rangeDates.endDate) {
                    stateObj.companyDateOfSurvey = rangeDates;
                }
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('descriptions')
                && Array.isArray(nextProps.companyFields.companyInfo.descriptions)
                && nextProps.companyFields.companyInfo.descriptions.length === 1) {
                let desc = nextProps.companyFields.companyInfo.descriptions[0];
                if (desc && desc.label) {
                    stateObj.companyDescription = desc.label;
                }
            }
        }

        this.setState(stateObj);
    }

    render() {
        const {stepIndex} = this.state;
        let user = app.getAuthUser();
        let isAdmin = false;
        if (user && user.userRole) {
            isAdmin = user.userRole === "admin";
        }

        let isQuestionnaireActive = this.state.isQuestionnaireActive !== null ? this.state.isQuestionnaireActive  : false;
        let isQuestionnaireStepVisible = this.state.isQuestionnaireStepVisible !== null ? this.state.isQuestionnaireStepVisible  : false;

        let questionnaireBtnText = isQuestionnaireActive ? "Determine sensitivity by yourself" : "Use questionnaire to determine sensitivity";
        let questionnaireIconColor = isQuestionnaireActive ? '#0091ea' : 'rgba(0,0,0,0.7)';

        let items = [];

        let style = {
            domainsBtn: {
                'margin': '38px 35px 6px'
            },
            SurveyScoreField: {
                'width': '25%',
                'marginLeft': '20px'
            },
            intelScoreField: {
                'width': '18%'
            },
            intelPartScoreField: {
                'width': '23%'
            },
            intelPartScoreWideField: {
                'width': '25%'
            },
            underlineStyle: {
                'width': '25px'
            },
            formRow: {
                'marginTop': '45px',
                'marginBottom': '45px'
            },
            companyContactTextField: {
                marginLeft: 5,
                marginBottom: 5,
                float: 'left',
                fontSize: 14,
                'width': '20%'
            },
            companyInfoTextField: {
                'width': '85%'
            },
            limitedSizeDiv: {
                maxHeight: 400,
                overflow: 'auto'
            },
            limitedSizeFlexDiv: {
                maxHeight: 250,
                overflow: 'auto',
                display: 'grid'
            },
            autoCompleteStyle: {
                overflowY: 'auto',
                height: '200px',
                maxHeight: '200px'
            },
            infoHintCheckbox: {
                cursor: 'text'
            },
            infoHintCheckboxLabel: {
                width: '100%'
            },
            singleDatePicker: {
                display: 'inline-block',
                marginLeft: '5px'
            },
            stepButtonStyle: {
                borderRadius: 30
            },
            stepContentStyle: {
                'overflow': 'auto',
                'maxHeight': '800px',
                background: '#efefef',
                borderRadius: 10,
                border: '1px solid',
                borderColor: '#f5f5f5'

            },
        };

        let cancelBtn = {};
        let doneBtn = {};
        if(isQuestionnaireStepVisible){
            cancelBtn = {color: 'white', marginBottom: '5px', height: 40, visibility:'hidden'};
            doneBtn = {color: 'white', marginBottom: '5px', marginRight: '5px', height: 40, visibility:'hidden'};
        } else {
            cancelBtn = {color: 'white', marginBottom: '5px', height: 40, visibility:'visible'};
            doneBtn = {color: 'white', marginBottom: '5px', marginRight: '5px', height: 40,visibility:'visible'};
        }

        let lastDateOfSurvey;
        if (this.state.companyDateOfSurvey.endDate) {
            lastDateOfSurvey = new Date(moment(this.state.companyDateOfSurvey.endDate));
        } else {
            lastDateOfSurvey = null;
        }

        for (let i = 0; i < 10; i++ ) {
            items.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
        }

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
            <Dialog
                title="Edit Company"
                actions={[
                    <FlatButton
                        label="Done"
                        primary={true}
                        style={doneBtn}
                        keyboardFocused={true}
                        backgroundColor={'#0091ea'}
                        hoverColor={'#12a4ff'}
                        rippleColor={'white'}
                        labelStyle={{fontSize: 10}}
                        onTouchTap={this.addCompany.bind(this, this.props.companyFields)}
                    />,
                    <FlatButton
                        label="Cancel"
                        primary={true}
                        backgroundColor={'#0091ea'}
                        style={cancelBtn}
                        keyboardFocused={true}
                        hoverColor={'#12a4ff'}
                        rippleColor={'white'}
                        labelStyle={{fontSize: 10}}
                        onTouchTap={this.cancel.bind(this)}
                    />
                ]}
                actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                autoScrollBodyContent={true}
                modal={true}
                open={this.state.openAddNewCompanyModal}
                onRequestClose={this.cancel.bind(this)}
                repositionOnUpdate={false}
                contentStyle={{maxWidth: '60%', borderRadius: '7px 7px 7px 7px'}}
                titleStyle={{
                    fontSize: 18,
                    background: 'rgba(0,0,0,0.7)',
                    color: 'white', textAlign: 'center',
                    borderRadius: '2px 2px 0px 0px',
                    textTransform: 'uppercase',
                }}>
                <div>
                    {!isQuestionnaireStepVisible ?
                        <Stepper
                            color={'#0091ea'}
                            style={{marginTop: 10}}
                            activeStep={stepIndex}
                            linear={false}
                            orientation="vertical">
                            <Step>
                                <StepButton style={style.stepButtonStyle} onClick={() => this.setState({stepIndex: 0})}>
                                    Company
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <br/>
                                    <span style={{width: '100%'}}>Company name:</span>
                                    <br/>
                                    <TextField
                                        hintText="Company Name"
                                        onChange={this.handleChangeCompanyNameField.bind(this)}
                                        value={this.state.companyFields.companyName}
                                    />
                                    <br/>
                                    <br/>
                                    <TextField
                                        floatingLabelText="Company Description"
                                        hintText="Enter description"
                                        onChange={this.handleChangeDescriptionField.bind(this)}
                                        value={this.state.companyDescription}
                                        style={style.companyInfoTextField}
                                        multiLine={true}
                                        rows={1}
                                        rowsMax={2}
                                    />
                                    <br/>
                                    <br/>
                                    <span style={{width: '100%', position: 'relative', top: 15}}>Who are the company contacts?</span>
                                    <div>
                                        <ValidatorForm
                                            ref="form"
                                            onSubmit={this.handleAddContact.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                            onError={errors => console.log(errors)}>
                                            <TextValidator
                                                id={"15"}
                                                name="email"
                                                floatingLabelText="Email"
                                                hintText="Contact Email"
                                                validators={['required', 'isEmail']}
                                                errorMessages={['this field is required', 'email is not valid']}
                                                onChange={this.handleChangeContactEmailField.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.CONTACT, ev);
                                                    }
                                                }}
                                                value={this.state.companyContactToAdd.email}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"16"}
                                                name="name"
                                                floatingLabelText="Name"
                                                hintText="Contact name"
                                                validators={['required']}
                                                errorMessages={['this field is required']}
                                                onChange={this.handleChangeContactNameField.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.CONTACT, ev);
                                                    }
                                                }}
                                                value={this.state.companyContactToAdd.name}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"17"}
                                                name="phone"
                                                floatingLabelText="Phone"
                                                hintText="Contact phone"
                                                validators={['isNumber']}
                                                errorMessages={['numeric characters only']}
                                                onChange={this.handleChangeContactPhoneField.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.CONTACT, ev);
                                                    }
                                                }}
                                                value={this.state.companyContactToAdd.phone}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"18"}
                                                name="position"
                                                floatingLabelText="Position"
                                                hintText="Contact position"
                                                onChange={this.handleChangeContactPositionField.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.CONTACT, ev);
                                                    }
                                                }}
                                                value={this.state.companyContactToAdd.position}
                                                style={style.companyContactTextField}
                                            />
                                            <div style={{
                                                float: 'right',
                                                left: 15,
                                                bottom: 40,
                                                position: 'relative'
                                            }}>
                                                {(this.state.companyContacts && this.state.companyContacts.length > 0) ? (() => {
                                                    return <FlatButton
                                                        label="Edit"
                                                        icon={<EditIcon/>}
                                                        onClick={this.handleOpenEditContactsArray.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                        style={{
                                                            color: 'white',
                                                            borderRadius: 6,
                                                            height: 40,
                                                            top: 13
                                                        }}
                                                        labelStyle={{fontSize: 13}}
                                                        backgroundColor={'rgba(0,0,0,0.7'}
                                                        hoverColor={'#0091ea'}

                                                    />;
                                                })() : null}
                                                <br/>
                                                <FlatButton
                                                    type={"submit"}
                                                    label="Add"
                                                    labelStyle={{fontSize: 13}}
                                                    icon={<ActionAdd/>}
                                                    style={{top: 18, color: 'white', borderRadius: 6, height: 40}}
                                                    backgroundColor={'rgba(0,0,0,0.7'}
                                                    hoverColor={'#0091ea'}/>
                                                <br/>
                                                <span style={{
                                                    marginRight: '27px',
                                                    lineHeight: '36px',
                                                    float: 'right',
                                                    marginTop: 20,
                                                }}>Total Contacts: {this.state.companyContacts.length}</span>
                                            </div>
                                        </ValidatorForm>
                                        <EditContactsModal
                                            open={this.state.openEditContactsModal}
                                            closeDialog={this.handleCloseEditContactsArray.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                            modalTitle={'Edit Company Contacts'}
                                            companyContacts={this.state.companyContacts}
                                            saveChanges={this.handleSaveContactsChanges.bind(this)}
                                        />
                                    </div>
                                </StepContent>
                            </Step>
                            <Step>
                                <StepButton style={style.stepButtonStyle} onClick={() => this.setState({stepIndex: 1})}>
                                    Additional Information
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <div style={style.limitedSizeDiv}>
                                        <ChipInput
                                            hintText="Search Classification"
                                            floatingLabelText="Company Classification"
                                            menuStyle={style.autoCompleteStyle}
                                            filter={AutoComplete.caseInsensitiveFilter}
                                            value={this.state.companyClassifications}
                                            dataSource={COMPANY_INFO_CLASSIFICATION_TYPES}
                                            onRequestAdd={(chip) => this.handleAddChipInfoField('companyClassifications', chip)}
                                            onRequestDelete={(chip) => this.handleDeleteChipItemInfoField('companyClassifications', chip)}
                                            maxSearchResults={50}
                                            style={style.companyInfoTextField}
                                            openOnFocus={true}
                                            fullWidth={false}
                                            fullWidthInput={true}
                                        />
                                        <br/>
                                        <br/>
                                        <div>
                                            <TextField
                                                floatingLabelText="Company Condition"
                                                hintText={<Checkbox
                                                    label="Example: This company signed an NDA"
                                                    checked={true}
                                                    disabled={true}
                                                    style={style.infoHintCheckbox}
                                                    labelStyle={style.infoHintCheckboxLabel}
                                                />}
                                                onChange={this.handleChangeInformationField.bind(this)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddInformation(ev);
                                                    }
                                                }}
                                                value={this.state.companyInformationToAdd}
                                                style={style.companyInfoTextField}
                                            />
                                            <div style={{float: 'right', bottom: 20, position: 'relative'}}>
                                                {(this.state.companyInformation && this.state.companyInformation.length > 0) ? (() => {
                                                    return <FlatButton
                                                        label="Edit"
                                                        icon={<EditIcon/>}
                                                        onClick={this.handleOpenEditInformationArray.bind(this)}
                                                        style={{color: 'white', borderRadius: 6, height: 40, top: 13}}
                                                        backgroundColor={'rgba(0,0,0,0.7'}
                                                        labelStyle={{fontSize: 13}}
                                                        hoverColor={'#0091ea'}
                                                    />;
                                                })() : null}
                                                <br/>
                                                <FlatButton
                                                    label="Add"
                                                    icon={<ActionAdd/>}
                                                    onClick={this.handleAddInformation.bind(this)}
                                                    style={{top: 18, color: 'white', borderRadius: 6, height: 40}}
                                                    labelStyle={{fontSize: 13}}
                                                    backgroundColor={'rgba(0,0,0,0.7'}
                                                    hoverColor={'#0091ea'}
                                                />
                                            </div>
                                            <br/>
                                            <br/>
                                            <span
                                                style={{float: 'right'}}>Total company conditions: {this.state.companyInformation.length}</span>
                                            <EditInformationModal
                                                open={this.state.openEditInformationModal}
                                                closeDialog={this.handleCloseEditInformationArray.bind(this)}
                                                companyInformation={this.state.companyInformation}
                                                saveChanges={this.handleSaveInformationChanges.bind(this)}
                                            />
                                        </div>
                                        <ChipInput
                                            hintText="Search Sector"
                                            floatingLabelText="Company Sector"
                                            menuStyle={style.autoCompleteStyle}
                                            filter={AutoComplete.caseInsensitiveFilter}
                                            value={this.state.companySectors}
                                            dataSource={COMPANY_INFO_SECTOR_TYPES}
                                            onRequestAdd={(chip) => this.handleAddChipInfoField('companySectors', chip)}
                                            onRequestDelete={(chip) => this.handleDeleteChipItemInfoField('companySectors', chip)}
                                            maxSearchResults={50}
                                            style={style.companyInfoTextField}
                                            openOnFocus={true}
                                            fullWidth={false}
                                            fullWidthInput={true}
                                        />
                                        <br/>
                                        <div>
                                        <span style={{
                                            marginLeft: '15px',
                                            lineHeight: '36px'
                                        }}>Date of Contact: </span>
                                            <DatePickerMain
                                                filterDataByDates={this.handleChangeInfoDateField.bind(this, 'companyDateOfContact')}
                                                disableDefaultDates={true}
                                                replaceDefaultDates={this.state.companyDateOfContact}
                                                buttonTooltip="Choose Dates"
                                            />
                                        </div>
                                        <br/>
                                        <div>
                                        <span style={{
                                            marginLeft: '15px',
                                            lineHeight: '36px'
                                        }}>Date of Last Survey: </span>
                                            <DatePicker
                                                hintText="Choose Last Survey Date"
                                                container="inline"
                                                value={lastDateOfSurvey}
                                                onChange={this.handleChangeInfoSingleDateField.bind(this, 'companyDateOfSurvey')}
                                                style={style.singleDatePicker}
                                            />
                                        </div>
                                    </div>
                                    <br/>
                                </StepContent>
                            </Step>
                            <Step>
                                <StepButton style={style.stepButtonStyle} onClick={() => this.setState({stepIndex: 2})}>
                                    Audit
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <br/>
                                    <span style={{width: '100%'}}>Who is auditing the company?</span>
                                    <br/>
                                    <div>
                                        <ValidatorForm
                                            ref="form"
                                            onSubmit={this.handleAddContact.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                            onError={errors => console.log(errors)}
                                            style={{marginBottom: 30}}
                                        >
                                            <TextValidator
                                                id={"11"}
                                                name={"email"}
                                                floatingLabelText="Email"
                                                hintText="Enter Email"
                                                validators={['required', 'isEmail']}
                                                errorMessages={['this field is required', 'email is not valid']}
                                                onChange={this.handleChangeContactEmailField.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.RESPONSIBLE, ev);
                                                    }
                                                }}
                                                value={this.state.companyResponsibleToAdd.email}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"12"}
                                                name={"name"}
                                                floatingLabelText="Name"
                                                hintText="Enter name"
                                                validators={['required']}
                                                errorMessages={['this field is required']}
                                                onChange={this.handleChangeContactNameField.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.RESPONSIBLE, ev);
                                                    }
                                                }}
                                                value={this.state.companyResponsibleToAdd.name}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"13"}
                                                name={"phone"}
                                                floatingLabelText="Phone"
                                                hintText="Enter phone"
                                                validators={['isNumber']}
                                                errorMessages={['numeric characters only']}
                                                onChange={this.handleChangeContactPhoneField.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.RESPONSIBLE, ev);
                                                    }
                                                }}
                                                value={this.state.companyResponsibleToAdd.phone}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"14"}
                                                name={"position"}
                                                floatingLabelText="Position"
                                                hintText="Enter position"
                                                onChange={this.handleChangeContactPositionField.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.RESPONSIBLE, ev);
                                                    }
                                                }}
                                                value={this.state.companyResponsibleToAdd.position}
                                                style={style.companyContactTextField}
                                            />
                                            <div style={{float: 'right', left: 15, bottom: 20, position: 'relative'}}>
                                                {(this.state.companyResponsibles && this.state.companyResponsibles.length > 0) ? (() => {
                                                    return <FlatButton
                                                        label="Edit"
                                                        icon={<EditIcon/>}
                                                        onClick={this.handleOpenEditContactsArray.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                        style={{color: 'white', borderRadius: 6, height: 40, top: 13}}
                                                        backgroundColor={'rgba(0,0,0,0.7'}
                                                        labelStyle={{fontSize: 13}}
                                                        hoverColor={'#0091ea'}
                                                    />;
                                                })() : null}
                                                <br/>
                                                <FlatButton
                                                    type="submit"
                                                    label="Add"
                                                    icon={<ActionAdd/>}
                                                    style={{top: 18, color: 'white', borderRadius: 6, height: 40}}
                                                    backgroundColor={'rgba(0,0,0,0.7'}
                                                    labelStyle={{fontSize: 13}}
                                                    hoverColor={'#0091ea'}
                                                />
                                                <br/>
                                                <span style={{
                                                    marginRight: '27px',
                                                    lineHeight: '36px',
                                                    float: 'right',
                                                    marginTop: 20,
                                                }}>Total Auditors: {this.state.companyResponsibles.length}</span>
                                            </div>
                                        </ValidatorForm>
                                        <EditContactsModal
                                            open={this.state.openEditContactsModal}
                                            closeDialog={this.handleCloseEditContactsArray.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                            companyResponsibles={this.state.companyResponsibles}
                                            saveChanges={this.handleSaveContactsChanges.bind(this)}
                                            modalTitle={'Edit Company Auditors'}

                                        />
                                    </div>
                                    <br/>
                                </StepContent>
                            </Step>
                            <Step>
                                <StepButton style={style.stepButtonStyle} onClick={() => this.setState({stepIndex: 3})}>
                                    Preferences
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <div style={style.limitedSizeDiv}>
                                        <br/>
                                        <span
                                            style={{width: '100%'}}>What is the weight ratio between the intel and survey scores?</span>
                                        <br/>
                                        <TextField
                                            floatingLabelText="Intel Weight(%)"
                                            onChange={this.handleChangeIntelRatioField.bind(this)}
                                            style={style.intelScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={(this.state.companyFields.ratios
                                                && this.state.companyFields.ratios.intelWeight != null) ?
                                                this.state.companyFields.ratios.intelWeight : '50'}
                                        />
                                        <TextField
                                            floatingLabelText="Survey Weight(%)"
                                            onChange={this.handleChangeSurveyRatioField.bind(this)}
                                            style={style.SurveyScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={(this.state.companyFields.ratios
                                                && this.state.companyFields.ratios.surveyWeight != null) ?
                                                this.state.companyFields.ratios.surveyWeight : '50'}
                                        />
                                        <br/><br/>
                                        <span
                                            style={{width: '100%'}}>What is the weight ratio between the following intel score categories?</span>
                                        <br/>
                                        <TextField
                                            floatingLabelText="CVE Weight(%)"
                                            onChange={this.handleChangeIntelScoreRatioField.bind(this, 'cveWeight')}
                                            style={style.intelPartScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={this.state.intelScoreRatios.cveWeight}
                                            ref={(input) => {
                                                this.cveInput = input;
                                            }}
                                        />
                                        <TextField
                                            floatingLabelText="DNS Weight(%)"
                                            data-tip="SPF + DMARC"
                                            onChange={this.handleChangeIntelScoreRatioField.bind(this, 'dnsWeight')}
                                            style={style.intelPartScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={this.state.intelScoreRatios.dnsWeight}
                                        />
                                        <TextField
                                            floatingLabelText="S3 Buckets Weight(%)"
                                            onChange={this.handleChangeIntelScoreRatioField.bind(this, 'bucketsWeight')}
                                            style={style.intelPartScoreWideField}
                                            underlineStyle={style.underlineStyle}
                                            value={this.state.intelScoreRatios.bucketsWeight}
                                        />
                                        <TextField
                                            floatingLabelText="Breaches Weight(%)"
                                            onChange={this.handleChangeIntelScoreRatioField.bind(this, 'breachesWeight')}
                                            style={style.intelPartScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={this.state.intelScoreRatios.breachesWeight}
                                        />
                                        <TextField
                                            floatingLabelText="Dataleaks Weight(%)"
                                            onChange={this.handleChangeIntelScoreRatioField.bind(this, 'dataleaksWeight')}
                                            style={style.intelPartScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={this.state.intelScoreRatios.dataleaksWeight}
                                        />
                                        <TextField
                                            floatingLabelText="Blacklists Weight(%)"
                                            onChange={this.handleChangeIntelScoreRatioField.bind(this, 'blacklistsWeight')}
                                            style={style.intelPartScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={this.state.intelScoreRatios.blacklistsWeight}
                                        />
                                        <TextField
                                            floatingLabelText="Botnets Weight(%)"
                                            onChange={this.handleChangeIntelScoreRatioField.bind(this, 'botnetsWeight')}
                                            style={style.intelPartScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={this.state.intelScoreRatios.botnetsWeight}
                                        />
                                        <TextField
                                            floatingLabelText="Https Weight(%)"
                                            onChange={this.handleChangeIntelScoreRatioField.bind(this, 'sslCertsWeight')}
                                            style={style.intelPartScoreField}
                                            underlineStyle={style.underlineStyle}
                                            value={this.state.intelScoreRatios.sslCertsWeight}
                                        />
                                        <br/>
                                        <br/>
                                        <span
                                            style={{width: '100%'}}>How sensitive is the company being reviewed?</span>
                                        <br/>
                                        <DropDownMenu
                                            disabled={this.state.isQuestionnaireActive}
                                            value={this.props.companyFields.sensitivity || 2}
                                            onChange={this.onChangeSensitivityDropdown.bind(this)}
                                            style={{marginLeft: -20, width: 310}}
                                            autoWidth={false}
                                        >
                                            <MenuItem value={1} primaryText="Low Sensitivity"/>
                                            <MenuItem value={2} primaryText="Moderate Sensitivity"/>
                                            <MenuItem value={3} primaryText="High Sensitivity"/>
                                            <MenuItem value={4} primaryText="Very High Sensitivity"/>
                                        </DropDownMenu>
                                        <IconButton style={{position:'relative', bottom: 6}} touch={true}
                                                    tooltip={questionnaireBtnText} tooltipPosition="bottom-right">
                                            <QuestionnaireIcon  color={questionnaireIconColor}
                                                                onTouchTap={this.handleQuestionnaireBtn.bind(this)}
                                            />
                                        </IconButton>
                                        <span style={{position:'relative', left: 40}}>{this.state.finalSensitivityLevelText}</span>
                                        <br/>
                                        <br/>
                                        <ReactTooltip place="bottom"/>
                                        <br/>
                                        <span style={{width: '100%'}}>What is the company scan priority?</span>
                                        <br/>
                                        {isAdmin && <SelectField
                                            value={this.props.companyFields.scanPriority || 5}
                                            onChange={this.handleChangeScanPriority}
                                            maxHeight={200}
                                        >
                                            {items}
                                        </SelectField>}
                                    </div>
                                    <br/>
                                </StepContent>
                            </Step>
                        </Stepper> :
                        <div className="scrollableDiv" style={{
                            'overflow': 'auto',
                            'maxHeight': '800px',
                            background: '#efefef',
                            borderRadius: 10,
                            border: '1px solid',
                            borderColor: '#f5f5f5',
                            padding: '20px 20px 0px 25px',
                            marginTop: 30,
                        }}>
                            <SensitivityQuestionnaire
                                updateSensitivity={this.handleSensitivityQuestionnaireFinish.bind(this)}
                                handleCancel={this.handleSensitivityQuestionnaireCancel.bind(this)}/>
                        </div>
                    }

                    <YesNoModal
                        open={this.state.openYesNoModal}
                        title={this.state.titleYesNoModal}
                        message={this.state.messageYesNoModal}
                        paramObject={this.state.paramObjectYesNoModal}
                        getUserResult={this.getYesNoModalResult.bind(this)}
                    />
                </div>
            </Dialog>
            </MuiThemeProvider>

        );
    }
}
