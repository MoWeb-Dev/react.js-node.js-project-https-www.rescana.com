import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardText, CardTitle} from 'material-ui/Card';
import AutoComplete from 'material-ui/AutoComplete';
import {browserHistory} from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import ChipInput from 'material-ui-chip-input';
import classNames from 'classnames';
import themeForFont from "../app/themes/themeForFont";
import {ORGANIZATION_ID} from "../../../config/consts";


const moment = require('moment');

export default class NewProject extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projectName: '',
            projects: [],
            companies: [],
            companiesInputVal: [],
            projectFields: {
                projectName: '',
                selectedCompanies: []
            },
            curOrg: {}
        };
    }

    componentWillMount() {
        app.setFlagVisibile(false);
        let loadedOrgId = localStorage.getItem(ORGANIZATION_ID);
        if (loadedOrgId) {
            this.getOrgById(loadedOrgId);
        }
        this.getProjects(loadedOrgId);
        this.getAllCompaniesById();
    }

    getOrgById(loadedOrgId) {
        let user = app.getAuthUser();
        if (!user) {
            app.routeAuthUser();
        } else {
            const data = {orgId: loadedOrgId};
            $.ajax({
                type: 'POST',
                url: '/api/getOrgById',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.org) {
                    this.setState({
                        curOrg: data.org
                    });
                } else {
                    app.addAlert('error', 'Error: failed to load Organization.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load Organization.');
            });
        }
    }

    getProjects(orgId) {
        let data = {};
        let url = '';
        let type = '';

        if (orgId) {
            url = '/api/getProjectsByOrgId';
            type = 'POST';
            data.orgId = orgId;
        } else {
            url = '/api/getAllProjectsById';
            type = 'GET';
        }
        $.ajax({
            type: type,
            url: url,
            async: true,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projects) => {
            this.setState({
                projects: projects
            });
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
        });
    }

    cancel() {
        browserHistory.push({
            pathname: '/manage-projects'
        });
    }

    createNewProject() {
        let data = this.state.projectFields;
        let user = app.getAuthUser();
        if (user.userRole === "admin") {
            app.addAlert('warning', 'You are admin user! please manage projects from admin page.');
        } else {
            let curOrg = {};
            data.uid = user.id;
            data.createDate = moment().format();
            data.allowedUsers = [{email: user.email, id: user.id}];
            if (this.state.curOrg) {
                curOrg = this.state.curOrg;
            }
            $.ajax({
                type: 'post',
                url: '/api/createNewProjectByUser',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({data: data, curOrg: curOrg}),
                dataType: 'json'
            }).done((data) => {
                if (data) {
                    app.addAlert('success', 'Project Added.');
                    browserHistory.push({
                        pathname: '/manage-projects'
                    });
                } else {
                    app.addAlert('error', 'Error: failed to add project.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add project.');
            });
        }
    }

    handleAddCompanyToList(chip) {
        let companiesInputVal = this.state.companiesInputVal;
        let projectFields = this.state.projectFields;
        companiesInputVal.push(chip);
        projectFields.selectedCompanies = companiesInputVal;
        this.setState({projectFields: projectFields, companiesInputVal: companiesInputVal});
    }

    handleDeleteCompanyFromList(value, index) {
        let companiesInputVal = this.state.companiesInputVal;
        let projectFields = this.state.projectFields;
        companiesInputVal.splice(index, 1);

        projectFields.selectedCompanies = projectFields.selectedCompanies.filter(function (obj) {
            return obj.id !== value;
        });

        this.setState({companiesInputVal: companiesInputVal, projectFields: projectFields});
    }

    getAllCompaniesById() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'GET',
                url: '/api/getAllCompaniesById',
                async: true,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((companies) => {
                let alterdCompanies = companies.map((company) => {
                    return {name: company.companyName, id: company.id};
                });

                this.setState({
                    companies: alterdCompanies
                });
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load companies.');
            });
        }
    }

    handleChangeName(event) {
        let projectFields = this.state.projectFields;
        projectFields.projectName = event.target.value;
        this.setState({
            projectName: event.target.value,
            projectFields: projectFields
        });
    }

    render() {
        let pageMainClass = classNames({
            'containerLTR': true
        });

        let submitStyleBtn = {

            margin: '5px 0px 10px 170px'
        };

        return (
            <div className={pageMainClass}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <ValidatorForm ref="form"
                                   onSubmit={this.createNewProject.bind(this)}
                                   onError={(errors) => console.log(errors)}>
                        <CardTitle style={{margin: '42px 0px -5px 10px'}}>
                            <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                            <span style={{position: 'relative', top: '-6px', fontSize: 22}}>CREATE NEW PROJECT</span>
                        </CardTitle>
                        <Card style={{margin: '25px'}}>
                            <div>
                                <CardText style={{margin: 30}}>
                                    <TextValidator
                                        id="text-field-project-name"
                                        hintText={'Enter Project Name'}
                                        name="projectName"
                                        floatingLabelText={'Project Name'}
                                        value={this.state.projectName}
                                        onChange={this.handleChangeName.bind(this)}
                                        validators={['required']}
                                        errorMessages={['this field is required']}
                                    />
                                    <br/>
                                    <ChipInput
                                        hintText="Companies to include in project"
                                        floatingLabelText="Please choose from list"
                                        newChipKeyCodes={[]}
                                        value={this.state.companiesInputVal}
                                        filter={AutoComplete.fuzzyFilter}
                                        onRequestAdd={(chip) => this.handleAddCompanyToList(chip)}
                                        onRequestDelete={(chip, index) => this.handleDeleteCompanyFromList(chip, index)}
                                        dataSource={this.state.companies}
                                        dataSourceConfig={{'text': 'name', 'value': 'id'}}
                                        maxSearchResults={5}
                                    />
                                    <br/>
                                    <FlatButton label={'Done'} secondary={true}
                                                type="submit"
                                                style={submitStyleBtn}/>
                                    {/*<FlatButton label={'Cancel'}*/}
                                    {/*primary={true}*/}
                                    {/*style={submitStyleBtn}*/}
                                    {/*onTouchTap={this.cancel.bind(this)}*/}
                                    {/*/>*/}
                                </CardText>
                            </div>
                        </Card>
                    </ValidatorForm>
                </MuiThemeProvider>
            </div>
        );
    }
}
