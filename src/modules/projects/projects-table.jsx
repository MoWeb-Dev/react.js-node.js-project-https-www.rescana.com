import React from 'react';
import {Table, TableHeaderColumn, TableRow, TableHeader, TableRowColumn, TableBody} from 'material-ui/Table';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import IconButton from 'material-ui/IconButton';
import Delete from 'material-ui/svg-icons/action/delete';
import Chip from 'material-ui/Chip';
import DeleteDialog from '../app/delete-dialog.jsx';


export default class ProjectsTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projects: this.props.projects || [],
            openDeleteDialog: false,
            curRowIdToDelete: null
        };

        this.handleOpenDeleteDialog = this.handleOpenDeleteDialog.bind(this);
        this.handleCloseDeleteDialog = this.handleCloseDeleteDialog.bind(this);
    }

    handleOpenDeleteDialog(rowToDelete) {
        this.setState({openDeleteDialog: true, curRowIdToDelete: rowToDelete});
    };

    handleCloseDeleteDialog() {
        this.setState({openDeleteDialog: false, curRowIdToDelete: null});
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            projects: nextProps.projects,
            companies: nextProps.companies
        });
    }

    render() {
        let projects = this.state.projects;
        let user = app.getAuthUser();
        let isUserAllowedToAddVendors = false;
        if(user && user.isUserAllowedToAddVendors){
            isUserAllowedToAddVendors =  user.isUserAllowedToAddVendors
        }
        if(user.userRole && user.userRole === "admin"){
            isUserAllowedToAddVendors = true
        }
        const styles = {
            chip: {
                margin: 4
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap'
            },
            tableStyle: {
                paddingTop: '8px',
                paddingBottom: '8px'
            }
        };

        return (
            <div>
                <Table
                    fixedHeader={true}
                    selectable={false}
                    onRowSelection={this.handleRowSelection}
                >
                    <TableHeader
                        style={{marginLeft: '5px'}}
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                    >
                        <TableRow>
                            <TableHeaderColumn style={{marginLeft: '5px'}}>Project Name</TableHeaderColumn>
                            <TableHeaderColumn>Create Date</TableHeaderColumn>
                            <TableHeaderColumn>Companies</TableHeaderColumn>
                            <TableHeaderColumn></TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        showRowHover={true}
                        stripedRows={false}
                        displayRowCheckbox={false}
                    >
                        {projects.map((row, i) => (
                            <TableRow style={{marginLeft: '5px'}} key={i}>
                                <TableRowColumn style={styles.tableStyle}>{row.projectName}</TableRowColumn>
                                <TableRowColumn style={styles.tableStyle}>{row.createDate}</TableRowColumn>
                                <TableRowColumn style={styles.tableStyle}>
                                    {row && row.selectedCompanies && row.selectedCompanies.map((company) => (
                                        <Chip key={company.id} style={styles.chip}>{company.name}</Chip>))}
                                </TableRowColumn>
                                <TableRowColumn style={styles.tableStyle}>
                                    {isUserAllowedToAddVendors ?
                                        <IconButton onClick={(e) => this.props.editProject(row.id, e)}
                                                    name="editProject">
                                            <Edit/>
                                        </IconButton> : ''}
                                    {isUserAllowedToAddVendors ?
                                        <IconButton
                                            name="deleteProject"
                                            style={{cursor: 'pointer'}}
                                            onTouchTap={this.handleOpenDeleteDialog.bind(this, row.id)}>
                                            <Delete/>
                                        </IconButton> : ''}
                                </TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <DeleteDialog
                    open={this.state.openDeleteDialog}
                    onRequestClose={this.handleCloseDeleteDialog}
                    onDelete={(e) => {
                        this.props.deleteProject(this.state.curRowIdToDelete, e);
                        this.handleCloseDeleteDialog();
                    }}/>
            </div>
        );
    }
}
