import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';

export default class EditProjectModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            openAddNewProjectModal: false,
            projectFields: this.props.projectFields || {},
            companiesInputVal: this.props.companies,
            usersInputVal: [],
            companies: [],
            addedChipArray: [],
            deletedChipArray: [],
        };

        this.chipArrCheckWhenAdded = this.chipArrCheckWhenAdded.bind(this);
        this.chipArrCheckWhenDeleted = this.chipArrCheckWhenDeleted.bind(this);
        this.clearAllProjectModalFields = this.clearAllProjectModalFields.bind(this);
        this.handleDone = this.handleDone.bind(this);
    }

    chipArrCheckWhenAdded(chipId) {
        let addedChipArray = this.state.addedChipArray;

        if (this.state.deletedChipArray.includes(chipId)) {
            let deletedChipArray = this.state.deletedChipArray;
            const index = deletedChipArray.indexOf(chipId);

            if (index > -1) {
                deletedChipArray.splice(index, 1);
                this.setState({deletedChipArray: deletedChipArray})
            }
        } else {
            addedChipArray.push(chipId);
            this.setState({addedChipArray: addedChipArray})
        }
    }
    chipArrCheckWhenDeleted(chipId) {
        let deletedChipArray = this.state.deletedChipArray;

        if (this.state.addedChipArray.includes(chipId)) {
            let addedChipArray = this.state.addedChipArray;
            const index = addedChipArray.indexOf(chipId);

            if (index > -1) {
                addedChipArray.splice(index, 1);
                this.setState({addedChipArray: addedChipArray})
            }
        } else {
            deletedChipArray.push(chipId);
            this.setState({deletedChipArray: deletedChipArray})
        }
    }

    handleChangeProjectField(e) {
        let projectFields = this.props.projectFields;
        projectFields.projectName = e.target.value;
        this.setState(projectFields);
    }

    handleAddCompanyToList(chip) {
        let companiesInputVal = this.state.companiesInputVal;
        let projectFields = this.state.projectFields;
        companiesInputVal.push(chip);
        projectFields.selectedCompanies = companiesInputVal;
        this.chipArrCheckWhenAdded(chip.id);
        this.setState({projectFields: projectFields, companiesInputVal: companiesInputVal});
    }

    handleDeleteCompanyFromList(value, index) {
        let companiesInputVal = this.state.companiesInputVal;
        let projectFields = this.state.projectFields;
        companiesInputVal.splice(index, 1);

        projectFields.selectedCompanies = projectFields.selectedCompanies.filter(function(obj) {
            return obj.id !== value;
        });
        this.chipArrCheckWhenDeleted(value);
        this.setState({companiesInputVal: companiesInputVal, projectFields: projectFields});
    }

    cancel() {
        this.clearAllProjectModalFields()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            openAddNewProjectModal: nextProps.openAddNewProjectModal,
            companiesInputVal: nextProps.projectFields.selectedCompanies,
            companies: nextProps.companies,
            projectFields: nextProps.projectFields

        });
    }
    clearAllProjectModalFields() {
        let projectFields = this.state.projectFields;
        projectFields.projectName = '';
        projectFields.selectedCompanies = [];
        projectFields.allowedUsers = [];
        projectFields.id = '';

        this.setState({
            openAddNewProjectModal: false,
            projectFields: projectFields
        });
    }

    handleDone() {
        if(this.state.projectFields && this.state.projectFields.projectName && this.state.projectFields.projectName !== ""){
            this.props.submitProject(this.state.projectFields, this.state.addedChipArray, this.state.deletedChipArray);
            this.clearAllProjectModalFields();
            this.setState({addedChipArray: [], deletedChipArray: []});
        } else {
            app.addAlert('error', 'Must enter project name!');
        }
    }



    render() {
        let companiesForDD = this.state.companies.map((item) => {
            return {name: item.companyName, id: item.id};
        });

        return (
            <Dialog
                title={this.props.title}
                actions={[
                    <FlatButton
                        label="Done"
                        primary={true}
                        style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                        backgroundColor={'#0091ea'}
                        hoverColor={'#12a4ff'}
                        rippleColor={'white'}
                        labelStyle={{fontSize: 10}}
                        keyboardFocused={true}
                        onTouchTap={this.handleDone.bind(this)}
                    />,
                    <FlatButton
                        label="Cancel"
                        primary={true}
                        backgroundColor={'#0091ea'}
                        style={{color: 'white', marginBottom: '5px', height: 40}}
                        keyboardFocused={true}
                        hoverColor={'#12a4ff'}
                        rippleColor={'white'}
                        labelStyle={{fontSize: 10}}
                        onTouchTap={()=>{this.cancel.bind(this);
                            this.props.closeProjectModal();
                        }}
                    />
                ]}
                actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                contentStyle={{maxWidth: '33%', borderRadius: '7px 7px 7px 7px'}}
                titleStyle={{
                    fontSize: 18,
                    background: 'rgba(0,0,0,0.7)',
                    color: 'white', textAlign: 'center',
                    borderRadius: '2px 2px 0px 0px',
                    textTransform: 'uppercase',
                }}
                autoScrollBodyContent={true}
                modal={true}
                open={this.state.openAddNewProjectModal}
                onRequestClose={this.cancel.bind(this)}
            >
                <form style={{marginLeft: '30px'}}>
                    <br/>
                    <br/>
                    <br/>
                    <TextField
                        hintText="Project Name"
                        onChange={this.handleChangeProjectField.bind(this)}
                        value={this.state.projectFields.projectName}
                    />
                    <br/>
                    <br/>
                    <ChipInput
                        hintText="Companies to include in project"
                        floatingLabelText="Companies to include in project"
                        value={this.state.companiesInputVal}
                        newChipKeyCodes={[]}
                        filter={AutoComplete.fuzzyFilter}
                        onRequestAdd={(chip) => this.handleAddCompanyToList(chip)}
                        onRequestDelete={(chip, index) => this.handleDeleteCompanyFromList(chip, index)}
                        dataSource={companiesForDD}
                        dataSourceConfig={{'text': 'name', 'value': 'id'}}
                        maxSearchResults={5}
                    />
                    <br/>
                    <br/>
                    <br/>
                </form>
            </Dialog>
        );
    }
}
