import React from 'react';
import {Card, CardHeader, CardTitle} from 'material-ui/Card';
import ProjectsTable from './projects-table.jsx';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import EditProjectModal from './edit-project-modal.jsx';
import classNames from 'classnames';
import themeForFont from "../app/themes/themeForFont";
import {ORGANIZATION_ID, PROJECT_NAME} from "../../../config/consts";


const moment = require('moment');

export default class manageProjects extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projects: [],
            projectFields: {
                projectName: '',
                allowedUsers: [],
                selectedCompanies: [],
                createDate: ''
            },
            companies: [],
            openAddNewProjectModal: false
        };
    }

    componentWillMount() {
        app.setFlagVisibile(false);
        let loadedOrgId = localStorage.getItem(ORGANIZATION_ID);
        this.getProjects(loadedOrgId);
        this.getAllCompaniesById();
    }

    getProjects(orgId) {
        let data = {};
        let url = '';
        let type = '';

        if (orgId) {
            url = '/api/getProjectsByOrgId';
            type = 'POST';
            data.orgId = orgId;
        } else {
            url = '/api/getAllProjectsById';
            type = 'GET';
        }
        $.ajax({
            type: type,
            url: url,
            async: true,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projects) => {
            this.setState({
                projects: projects
            });
            //this.getAllCompanies(projects);
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
        });
    }

    getAllCompaniesById() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'GET',
                url: '/api/getAllCompaniesById',
                async: true,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((companies) => {
                this.setState({
                    companies: companies
                });
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load projects.');
            });
        }
    }

    deleteProject(projectId, e) {
        let user = app.getAuthUser();
        if (user.userRole === "admin") {
            app.addAlert('warning', 'You are admin user! please manage projects from admin page.');
        } else {
            $.ajax({
                type: 'post',
                url: '/api/deleteProject',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({id: projectId}),
                dataType: 'json'
            }).done((data) => {
                if (data.ok) {
                    function matchesEl(el) {
                        return el.id === projectId;
                    }

                    let ProjectsAfterDelete = this.state.projects.filter((projectId) => {
                        return !matchesEl(projectId);
                    });
                    this.setState({
                        projects: ProjectsAfterDelete
                    });
                    app.addAlert('success', 'Record deleted.');
                } else {
                    app.addAlert('error', 'Error: failed to delete project.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to delete project.');
            });
        }
    }

    editProject(projectId, e) {
        e.preventDefault();
        let projectFields = this.state.projectFields;
        this.setState({openAddNewProjectModal: true});

        function matchesEl(el) {
            return el.id === projectId;
        }

        let project = this.state.projects.filter((projectId) => {
            return matchesEl(projectId);
        });

        projectFields.projectName = project[0].projectName;
        projectFields.selectedCompanies = project[0].selectedCompanies;
        projectFields.allowedUsers = project[0].allowedUsers;
        projectFields.id = project[0].id;

        this.setState({
            projectFields: projectFields
        });
    }

    submitProject(projectFields, addedChipArray, deletedChipArray) {
        let data = projectFields;
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else if (user.userRole === "admin") {
            app.addAlert('warning', 'You are admin user! please manage projects from admin page.');
        } else {
            let uid = user.id;
            let extraData = {
                detachArrayToAddToNeo: addedChipArray,
                detachArrayToRemoveFromNeo: deletedChipArray,
            };
            data.createDate = moment().format();

            $.ajax({
                type: 'post',
                url: '/api/addProject',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({uid: uid, data: data, extraData: extraData}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.newProject) {
                    let projects = this.state.projects;
                    let newProjectsAfterAdd = projects;
                    let found = false;
                    for (let i = 0; i < projects.length; i++) {
                        if (projects[i].id === data.newProject.id) {
                            newProjectsAfterAdd[i] = data.newProject;
                            found = true;
                        }
                    }

                    if (!found) {
                        newProjectsAfterAdd.push(data.newProject);
                    }

                    app.addAlert('success', 'Project Added.');
                    this.setState({openAddNewProjectModal: false, projects: newProjectsAfterAdd});
                } else {
                    app.addAlert('error', 'Error: failed to add project.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add project.');
            });
        }
    }

    render() {
        let pageMainClass = classNames({
            'containerLTR': true
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div className={pageMainClass}>
                    <CardTitle style={{margin: '42px 0px 10px 10px'}}>
                        <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                        <span style={{position: 'relative', top: '-6px', fontSize: 22}}>PROJECTS</span>
                    </CardTitle>
                    <Card style={{margin: '25px'}}>
                        <div>
                            <ProjectsTable
                                projects={this.state.projects}
                                deleteProject={this.deleteProject.bind(this)}
                                editProject={this.editProject.bind(this)}
                            />
                        </div>
                    </Card>
                    <EditProjectModal
                        title={"Edit Project"}
                        submitProject={this.submitProject.bind(this)}
                        cancel={this.cancel}
                        projects={this.state.projects}
                        projectFields={this.state.projectFields}
                        companies={this.state.companies}
                        allUsers={this.state.users}
                        openAddNewProjectModal={this.state.openAddNewProjectModal}
                    />
                </div>
            </MuiThemeProvider>
        );
    }
}
