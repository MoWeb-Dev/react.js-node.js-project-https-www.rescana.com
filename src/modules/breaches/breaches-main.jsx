import React from 'react';
import {
    EMAIL_BREACHES_TYPE,
    ORGANIZATION_ID
} from '../../../config/consts';
import classNames from 'classnames';
import moment from 'moment';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import DataTableTemplate from '../common/datatableTemplate/datatable-template-main.jsx';
import {getLoadedCompaniesObject} from '../common/datatableTemplate/datatable-template-helpers.js';
import {clearHTMLTags} from '../common/CommonHelpers.js';
import '../../../public/css/survey.css';
import Loader from 'react-loader-advanced';
import FlatButton from 'material-ui/FlatButton';
import InfoIcon from 'material-ui/svg-icons/action/info-outline';
import EnterIcon from 'material-ui/svg-icons/navigation/arrow-forward';
import generalFunctions from '../generalComponents/general-functions.js';
import themeForFont from "../app/themes/themeForFont";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import textForTitleExpl from "../appGeneralFiles/text-for-title-explanations";
import TitleExplanationComponent from '../appGeneralFiles/title-explanation-component.jsx';

const shortid = require('shortid');

export default class Breaches extends React.Component {
    constructor() {
        super();

        const clickableStyle = {'cursor': 'pointer'};
        let wrapClickableStyle = clickableStyle;
        wrapClickableStyle.whiteSpace = 'pre-wrap';

        this.state = {
            showLoader: false,
            tableData: [],
            sortedTableData: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            companies: [],
            companiesInputVal: [],
            companiesDomains: [],
            assetUserInputAndMitigatedData: [],
            fullData: [],
            /* Create table column headers */
            tableColumns: [
                {
                    sortable: true,
                    label: 'Email',
                    key: 'email',
                    style: wrapClickableStyle
                }, {
                    sortable: true,
                    label: '',
                    key: 'breach',
                    style: wrapClickableStyle
                }, {
                    sortable: true,
                    label: '',
                    key: 'breach_date',
                    style: wrapClickableStyle
                }, {
                    label: '',
                    key: 'breach_classes',
                    style: wrapClickableStyle
                }, {
                    label: 'Related Companies',
                    key: 'relatedCompanies',
                    style: clickableStyle,
                    noneSearchable: true
                }, {
                    label: 'More Info',
                    key: 'moreInfo',
                    style: clickableStyle,
                    noneSearchable: true
                }
            ],
            tableColumnsToDownload: [
                {label: 'Email', key: 'email'},
                {label: 'Appeared in Breach', key: 'breach'},
                {label: 'Breach Date', key: 'breach_date'},
                {label: 'Compromised Data', key: 'breach_classes'},
                {label: 'Related Companies', key: 'relatedCompanies'},
                {label: 'More Info', key: 'moreInfo'},
                {label: 'Importance', key: 'Importance'},
                {label: 'Comments', key: 'Comments'},
                {label: 'Mitigated', key: 'Mitigated'},
            ],
            extraInfoNamesMapper: {
                'Description': 'Breach Description',
                'BreachDate': 'Breach Date',
                'DataClasses': 'Compromised Data',
                'IsSpamList': 'Breach is Considered a Spam List'
            },
            isOnFirstTable: true,
            openInfoBox: false
        };

        this.setResults = this.setResults.bind(this);
        this.updateState = this.updateState.bind(this);
        this.getEmailBreaches = this.getEmailBreaches.bind(this);
        this.setDefaultCompanies = this.setDefaultCompanies.bind(this);
        this.saveLoadedCompanies = this.saveLoadedCompanies.bind(this);
        this.arrangeExtraInfo = this.arrangeExtraInfo.bind(this);
        this.changeTableState = this.changeTableState.bind(this);
        this.updateStateForAMData = this.updateStateForAMData.bind(this);

    }

    //AMData = assetUserInput and mitigated Data
    updateStateForAMData(assetUserInputAndMitigatedData) {
        this.setState({assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
    }

    componentDidMount() {
        let user = app.getAuthUser();
        if (user) {
            this.setState({user: user});
        }
    }

    componentWillMount() {
        app.setFlagVisibile(false);
        this.setState({isInFirstTable: true});
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    setResults(isOnFirstTable, rowEmailObj, needsToAddColumns = true) {
        let tableData = [];
        let tableColumns = this.state.tableColumns;
        let tableDataToDownload = [];
        let dataToDisplay = [];
        let data = this.state.fullData || [];
        let idx = 0;
        let demoIdx = 0;
        let findingStatusColumnStyle = {width: 140, 'cursor': 'pointer'};
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;

        if(tableColumns && isOnFirstTable && needsToAddColumns){
            tableColumns.push({
                sortable: true,
                label: 'Importance',
                key: 'Importance',
                style: findingStatusColumnStyle,
                noneSearchable: true
            },{
                sortable: false,
                label: 'Comments',
                key: 'Comments',
                style: findingStatusColumnStyle,
                noneSearchable: true
            },{
                sortable: true,
                label: 'Mitigated',
                key: 'Mitigated',
                style: findingStatusColumnStyle,
                noneSearchable: true,
            });
        }

        let assetUserInputAndMitigatedData = this.state.assetUserInputAndMitigatedData || [];

        //in case we are on the first table and want to show all the unique emails without duplications
        if (isOnFirstTable) {
            let idx = 0;
            for (let i = 0; i < data.length; i++) {
                if (data && data[i].id && data[i].id.low) {

                    let exists = dataToDisplay.some(dataOfDisplayArr => dataOfDisplayArr.id && dataOfDisplayArr.id.low && dataOfDisplayArr.id.low.toString() === data[i].id.low.toString());
                    if (!exists) {
                        let id = data[i].id && data[i].id.low? data[i].id.low.toString() : null;
                        dataToDisplay.push(data[i]);
                        tableData[idx] = {
                            id: id,
                            email: isInDemoMode? 'EmailExample'+ demoIdx +'@mail.com' : data[i].email,
                            relatedCompanies: showExpandedDataChip(isInDemoMode? ['Company Name'] : data[i].relatedCompanies, 3, 5),
                            moreInfo: <div style={{position:'relative' ,left:20}}><EnterIcon/></div>,
                        };

                        let importance = 3;
                        let mitigatedBackground = 'black';
                        let comments = [];
                        let assetUserInputFlag = false;
                        let mitigatedBackgroundFlag = false;
                        let commentsFlag = false;

                        for(let j=0; j<assetUserInputAndMitigatedData.length; j++){
                            if(this.refs.dataTableTemplate.isCurRowNeedToBeUpdateCurNode(tableData[idx], assetUserInputAndMitigatedData[j])){
                                if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'assetUserInput'){
                                    importance = assetUserInputAndMitigatedData[j].importance? Number(assetUserInputAndMitigatedData[j].importance) : 3;
                                    assetUserInputFlag = true;
                                }
                                if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'mitigated'){
                                    mitigatedBackground = '#5bd25e';
                                    mitigatedBackgroundFlag = true;
                                }
                                if(assetUserInputAndMitigatedData[j].comments){
                                    comments = assetUserInputAndMitigatedData[j].comments? assetUserInputAndMitigatedData[j].comments : [];
                                    commentsFlag = true;
                                }
                                if(mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag){
                                    break;
                                }
                            }
                        }
                        let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';
                        tableData[idx].Importance = this.refs.dataTableTemplate.getImportanceComponent(tableData[idx], importance);
                        tableData[idx].Comments =  this.refs.dataTableTemplate.getCommentsComponent(tableData[idx], comments.length, comments, badgeColor);
                        tableData[idx].Mitigated =  this.refs.dataTableTemplate.getMitigatedComponent(tableData[idx], mitigatedBackground);

                        tableDataToDownload[idx] = {};
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[0].key] = isInDemoMode? 'EmailExample'+ demoIdx +'@mail.com' : data[i].email;
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[4].key] = isInDemoMode? 'Company Name' : data[i].relatedCompanies.join(',');
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[6].key] = generalFunctions.getImportanceStr(importance);
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[7].key] = generalFunctions.allCommentsToOneStr(comments);
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[8].key] = mitigatedBackgroundFlag ? 'Yes' : 'No';
                        idx++;
                        demoIdx++;
                    }
                }
            }

            //Removing the columns names for the first table
            let tableColumns = this.state.tableColumns;
            tableColumns[1].label = '';
            tableColumns[2].label = '';
            tableColumns[3].label = '';

            this.setState({
                tableColumns: tableColumns
            });

            //in case we are on the second table and want to show all the email breaches of the specific email we choose
        } else if (rowEmailObj) {
            for (let i = 0; i < data.length; i++) {
                if (data && data[i].id && data[i].id.low &&  rowEmailObj.id && rowEmailObj.id) {
                    let needsToBeShownOnTable = data[i].id.low.toString() === rowEmailObj.id;
                    if (needsToBeShownOnTable) {
                        let id = data[i].id && data[i].id.low? data[i].id.low.toString() : null;
                        tableData[idx] = {
                            id: id,
                            email: isInDemoMode? 'EmailExample'+ demoIdx +'@gmail.com' : data[i].email,
                            breach: data[i].breach,
                            breach_date: moment(data[i].breach_date).format('YYYY/MM/DD'),
                            breach_classes: data[i].breach_classes,
                            relatedCompanies: isInDemoMode? ['Company Name'] : showExpandedDataChip(data[i].relatedCompanies, 3, 5),
                            breach_nodeID: data[i].breach_nodeID,
                            moreInfo: <div style={{position:'relative' ,left:20}}><InfoIcon style={{textAlign:'center'}}/></div>,
                            /* markAsImportant: <FavoriteButton onTouchTap={this.markAsImportant.bind(this, data[i].lid)}
                                                             style={deleteBtnStyle}/>,*/
                            surveyIdentifier: shortid.generate()
                        };

                        tableDataToDownload[idx] = {};
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[0].key] = isInDemoMode? 'EmailExample'+ demoIdx +'@gmail.com' : data[i].email;
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[1].key] = data[i].breach;
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[2].key] = data[i].breach_date;
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[3].key] = data[i].breach_classes;
                        tableDataToDownload[idx][this.state.tableColumnsToDownload[4].key] =  isInDemoMode? 'Company Name' : data[i].relatedCompanies.join(',');
                        idx++;
                        demoIdx++;
                    }
                }
            }
            //Adding the columns names for the second table
            let tableColumns = this.state.tableColumns;
            tableColumns[1].label = 'Appeared in Breach';
            tableColumns[2].label = 'Breach Date';
            tableColumns[3].label = 'Compromised Data';
            if(tableColumns[6] && tableColumns[6].label){
                tableColumns.splice(6,3);
            }
            this.setState({
                tableColumns: tableColumns
            });
        }

        this.setState({
            totalRowCount: tableData.length,
            tableData: tableData,
            sortedTableData: tableData.slice(0, this.state.rowCountInPage),
            tableDataToDownload: tableDataToDownload
        });

        if(this.refs && this.refs.dataTableTemplate){
            this.refs.dataTableTemplate.doAfterSetResult();
        }
    }

    backToFirstTable() {
        this.changeTableState(true);
        this.getEmailBreaches()
    }

    changeTableState(wantedState) {
        this.setState({isInFirstTable: wantedState});
    }

    updateState(stateObj, cb) {
        if (stateObj) {
            if (cb) {
                this.setState(stateObj, cb);
            } else {
                this.setState(stateObj);
            }
        }
    }

    getEmailBreaches() {
        let orgId = localStorage.getItem(ORGANIZATION_ID);

        const data = {
            companies: this.state.companiesInputVal,
            domains: this.state.companiesDomains,
            orgId: orgId
        };

        this.startLoader();

        $.ajax({
            type: 'POST',
            url: '/api/getEmailBreachesData',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            this.stopLoader();
            let assetUserInputAndMitigatedData = [];

            if (data && data.emailBreachesData) {
                if(data.assetUserInputAndMitigatedData && Array.isArray(data.assetUserInputAndMitigatedData)){
                    assetUserInputAndMitigatedData = data.assetUserInputAndMitigatedData
                }
                this.setState({fullData: data.emailBreachesData , isOnFirstTable: true, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData}, () =>{
                    this.setResults(true, null);
                });
            }
        }).fail(() => {
            this.stopLoader();

            app.addAlert('error', 'Error: failed to fetch data');
        });
    }

    setDefaultCompanies(result) {
        if (result && result.companies && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: result.companies,
                companiesInputVal: result.companiesInputVal,
                companiesDomains: result.companiesPropArray
            }, this.getEmailBreaches);
        }
    }

    saveLoadedCompanies(companies, selectedCompanies) {
        let result = getLoadedCompaniesObject(companies, selectedCompanies, 'selectedDomains');

        if (result && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: companies,
                companiesInputVal: result.companiesInputVal,
                companiesDomains: result.companiesPropArray
            }, this.getEmailBreaches);
        }
    }

    arrangeExtraInfo(data) {
        let resultData = [];
        let namesMapper = this.state.extraInfoNamesMapper;

        if (namesMapper && data && Array.isArray(data)) {
            data.map((currData) => {
                if (currData && currData.hasOwnProperty('name') && currData.hasOwnProperty('finding') && namesMapper.hasOwnProperty(currData.name)) {
                    let currDataName = namesMapper[currData.name];
                    let currDataFinding;

                    if (currData.name === 'IsSpamList') {
                        // finding for 'IsSpamList' is true or false.
                        currDataFinding = (currData.finding) ? 'Yes' : 'No';
                    } else if (currData.name === 'Description') {
                        currDataFinding = clearHTMLTags(currData.finding);
                    } else {
                        currDataFinding = currData.finding;
                    }

                    resultData.push({name: currDataName, finding: currDataFinding});
                }
            });
        }

        return resultData;
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;
        let isDisabled = this.state.isInFirstTable;

        let buttonStyle = {color: '#212121', boxShadow: "2px 2px 2px 2px #e0e0e0", borderRadius: 5};
        if (isDisabled) {
            buttonStyle = {color: "grey", boxShadow: "2px 2px 2px 2px #e0e0e0", borderRadius: 5};
        }

        let infoText = textForTitleExpl.breaches.expl;
        let shorterInfoText = textForTitleExpl.breaches.explShort;


        return (
            <div className={pageStyleClass}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <Loader show={this.state.showLoader} message={spinner}>
                        <div style={{margin: '40px 0px 40px 10px'}}>
                            <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                            <span style={{
                                position: 'relative',
                                top: '-6px',
                                fontSize: 22
                            }}>EMAILS FOUND IN BREACHES</span>
                        </div>
                        <TitleExplanationComponent
                            openInfoBox={this.state.openInfoBox}
                            anchorEl={this.state.anchorEl}
                            shorterInfoText={shorterInfoText}
                            infoText={infoText}
                            handleCloseInfoBox={this.handleCloseInfoBox.bind(this)}
                            handleOpenInfoBox={this.handleOpenInfoBox.bind(this)}
                        />
                        <div style={{margin: '0px 10px 10px 10px', float: 'right'}}>
                            <FlatButton label="Back"
                                        style={buttonStyle}
                                        disabled={isDisabled}
                                        labelStyle={{bottom: 2}}
                                        backgroundColor={isDisabled ? '#e2e2e2' : 'white'}
                                        onTouchTap={this.backToFirstTable.bind(this)}/>
                        </div>
                        <br/>
                        <br/>
                        <DataTableTemplate
                            ref="dataTableTemplate"
                            myDataType={EMAIL_BREACHES_TYPE}
                            tableColumns={this.state.tableColumns}
                            tableData={this.state.tableData}
                            sortedTableData={this.state.sortedTableData}
                            tableColumnsToDownload={this.state.tableColumnsToDownload}
                            tableDataToDownload={this.state.tableDataToDownload}
                            totalRowCount={this.state.totalRowCount}
                            companies={this.state.companies}
                            companiesInputVal={this.state.companiesInputVal}
                            companiesDomains={this.state.companiesDomains}
                            arrangeExtraInfo={this.arrangeExtraInfo}
                            updateParentState={this.updateState}
                            setResults={this.setResults}
                            getData={this.getEmailBreaches}
                            setDefaultCompanies={this.setDefaultCompanies}
                            saveLoadedCompanies={this.saveLoadedCompanies}
                            enableRowClick={true}
                            isOnFirstTable={this.state.isOnFirstTable}
                            changeTableState={this.changeTableState}
                            assetUserInputAndMitigatedData={this.state.assetUserInputAndMitigatedData}
                            updateStateForAMData={this.updateStateForAMData}
                        />
                    </Loader>
                </MuiThemeProvider>
            </div>
        );
    }
}
