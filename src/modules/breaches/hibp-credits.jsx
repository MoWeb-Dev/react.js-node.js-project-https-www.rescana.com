import React from 'react';

/**
 * A component showing credits to HaveIBeenPwned?
 */
export default class HIBP_Credits extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let creditsStyle = {margin: '10px'};

        if (this.props && this.props.style && typeof this.props.style === 'object') {
            /* let propStyle = this.props.style;
            Object.keys(propStyle).forEach((key, ignore) => {
                // key: the name of the object key
                if(propStyle.hasOwnProperty(key)) {
                    creditsStyle[key] = propStyle[key];
                }
            });*/
            creditsStyle = Object.assign(creditsStyle, this.props.style);
        }

        return (
            <span style={creditsStyle}>Data received from <a href="https://haveibeenpwned.com"
                    target="_blank">Have I been pwned?</a>
            </span>
        );
    }
};
