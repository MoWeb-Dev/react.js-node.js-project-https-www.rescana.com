export function findCompanyIndexByID(companies, companyID) {
    let index = -1;
    if (companies) {
        for (let i=0; i<companies.length; i++) {
            if (companies[i].id && companies[i].id === companyID) {
                index = i;
                break;
            }
        }
    }
    return index;
}
export function findCompanyIndexByName(companies, companyName) {
    let index = -1;
    if (companies) {
        for (let i=0; i<companies.length; i++) {
            if (companies[i].companyName && companies[i].companyName === companyName) {
                index = i;
                break;
            }
        }
    }
    return index;
}
