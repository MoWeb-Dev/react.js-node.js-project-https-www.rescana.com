import React from 'react';
import {
    BLACKLISTS_TYPE,
    DATALEAKS_PASTEBIN,
    DATALEAKS_TYPE, EMAIL_BREACHES_TYPE,
    GRAPH_COMPANIES_FILTER_LIMIT, ORGANIZATION_ID,
    SESSION_COMPANIES
} from '../../../config/consts';
import DeleteButton from 'material-ui/svg-icons/action/delete-forever';
import {deleteIDFromSurveys} from '../survey/common/survey-helpers';
import classNames from 'classnames';
import moment from 'moment';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import {getLoadedCompaniesObject, getDefaultCompanies} from '../common/datatableTemplate/datatable-template-helpers.js';
import DataTableTemplate from '../common/datatableTemplate/datatable-template-main.jsx';
import {FuzzySearchInTable} from '../common/CommonHelpers.js';
import '../../../public/css/survey.css';
import Loader from 'react-loader-advanced';
import generalFunctions from "../generalComponents/general-functions.js";
import themeForFont from "../app/themes/themeForFont";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import textForTitleExpl from '../appGeneralFiles/text-for-title-explanations.js'
import TitleExplanationComponent from '../appGeneralFiles/title-explanation-component.jsx';


export default class DataLeaks extends React.Component {
    constructor() {
        super();

        const wrappableStyle = {
            whiteSpace: 'pre-wrap',
            wordBreak: 'break-word'
        };

        this.state = {
            showLoader: false,
            tableData: [],
            sortedTableData: [],
            tableDataToDownload: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            page: 1,
            companies: [],
            companiesInputVal: [],
            companiesKeywords: [],
            assetUserInputAndMitigatedData: [],
            fullData: [],
            /* Create table column headers */
            tableColumns: [
                {
                    sortable: true,
                    label: 'Source',
                    key: 'source',
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Title',
                    key: 'title',
                    style: wrappableStyle
                }, {
                    label: 'Url',
                    key: 'url',
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Created date',
                    key: 'createDate',
                    style: wrappableStyle
                }, {
                    label: 'Related companies',
                    key: 'relatedCompanies',
                    noneSearchable: true
                }, {
                    label: 'Delete',
                    key: 'delete',
                    noneSearchable: true
                },{
                    sortable: true,
                    label: 'Importance',
                    key: 'Importance',
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: false,
                    label: 'Comments',
                    key: 'Comments',
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: true,
                    label: 'Mitigated',
                    key: 'Mitigated',
                    style: wrappableStyle,
                    noneSearchable: true
                }
            ],
            tableColumnsToDownload: [
                {label: 'Source', key: 'source'},
                {label: 'URL', key: 'url'},
                {label: 'Title', key: 'title'},
                {label: 'Create Date', key: 'createDate'},
                {label: 'Related Companies', key: 'relatedCompanies'},
                {label: 'Importance', key: 'Importance'},
                {label: 'Comments', key: 'Comments'},
                {label: 'Mitigated', key: 'Mitigated'},
            ],
            openInfoBox: false
        };

        this.setResults = this.setResults.bind(this);
        this.updateState = this.updateState.bind(this);
        this.getDataleaks = this.getDataleaks.bind(this);
        this.setDefaultCompanies = this.setDefaultCompanies.bind(this);
        this.saveLoadedCompanies = this.saveLoadedCompanies.bind(this);

        this.handleFilterValueChange = this.handleFilterValueChange.bind(this);
        this.deletePastes = this.deletePastes.bind(this);
        this.deleteDataleak = this.deleteDataleak.bind(this);
        this.add3Dots = this.add3Dots.bind(this);
        this.updateStateForAMData = this.updateStateForAMData.bind(this);
    }

    //AMData = assetUserInput and mitigated Data
    updateStateForAMData(assetUserInputAndMitigatedData) {
        this.setState({assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
    }

    componentDidMount() {
        let user = app.getAuthUser();
        if (user) {
            this.setState({user: user});
        }
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    setResults() {
        let data = this.state.fullData;
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;
        let deleteBtnStyle = {
            cursor: 'pointer'
        };

        let tableData = [];
        let tableDataToDownload = [];
        let assetUserInputAndMitigatedData = this.state.assetUserInputAndMitigatedData || [];

        for (let i = 0; i < data.length; i++) {
            tableData[i] = {
                id: data[i]._id || data[i].id,
                source: data[i].source,
                url: <a href={'http://' + data[i].url} target="_blank"
                    style={{textDecoration: 'none'}}>{this.add3Dots(data[i].url, 25)}</a>,
                title: this.add3Dots(data[i].title, 25),
                time: data[i].time,
                createDate: moment(data[i].time).format('YYYY/MM/DD, hh:mm:ss a'),
                relatedCompanies: showExpandedDataChip(isInDemoMode? ['Company Name'] : data[i].relatedCompanies, 3, 5),
                delete: <DeleteButton onTouchTap={this.deleteDataleak.bind(this, data[i].lid, data[i].source)}
                    style={deleteBtnStyle}/>,
                /* markAsImportant: <FavoriteButton onTouchTap={this.markAsImportant.bind(this, data[i].lid)}
                                      style={deleteBtnStyle}/>,*/
                surveyIdentifier: data[i].lid,
                lid: data[i].lid
            };

            let importance = 3;
            let mitigatedBackground = 'black';
            let comments = [];
            let assetUserInputFlag = false;
            let mitigatedBackgroundFlag = false;
            let commentsFlag = false;

            for(let j=0; j<assetUserInputAndMitigatedData.length; j++){
                if(this.refs.dataTableTemplate.isCurRowNeedToBeUpdateCurNode(tableData[i], assetUserInputAndMitigatedData[j])){
                    if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'assetUserInput'){
                        importance = assetUserInputAndMitigatedData[j].importance? Number(assetUserInputAndMitigatedData[j].importance) : 3;
                        assetUserInputFlag = true;
                    }
                    if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'mitigated'){
                        mitigatedBackground = '#5bd25e';
                        mitigatedBackgroundFlag = true;
                    }
                    if(assetUserInputAndMitigatedData[j].comments){
                        comments = assetUserInputAndMitigatedData[j].comments? assetUserInputAndMitigatedData[j].comments : [];
                        commentsFlag = true;
                    }
                    if(mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag){
                        break;
                    }
                }
            }
            let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';
            tableData[i].Importance = this.refs.dataTableTemplate.getImportanceComponent(tableData[i], importance);
            tableData[i].Comments =  this.refs.dataTableTemplate.getCommentsComponent(tableData[i], comments.length, comments, badgeColor);
            tableData[i].Mitigated =  this.refs.dataTableTemplate.getMitigatedComponent(tableData[i], mitigatedBackground);

            tableDataToDownload[i] = {};
            tableDataToDownload[i][this.state.tableColumnsToDownload[0].key] = data[i].source;
            tableDataToDownload[i][this.state.tableColumnsToDownload[1].key] = 'http://' + data[i].url;
            tableDataToDownload[i][this.state.tableColumnsToDownload[2].key] = data[i].title;
            tableDataToDownload[i][this.state.tableColumnsToDownload[3].key] = tableData[i].createDate;
            tableDataToDownload[i][this.state.tableColumnsToDownload[4].key] = isInDemoMode? ['Company Name'] : data[i].relatedCompanies.join(',');
            tableDataToDownload[i][this.state.tableColumnsToDownload[5].key] = generalFunctions.getImportanceStr(importance);
            tableDataToDownload[i][this.state.tableColumnsToDownload[6].key] = generalFunctions.allCommentsToOneStr(comments);
            tableDataToDownload[i][this.state.tableColumnsToDownload[7].key] = mitigatedBackgroundFlag ? 'Yes' : 'No';
            tableDataToDownload[i].surveyIdentifier = data[i].lid;
        }

        this.setState({
            totalRowCount: tableData.length,
            tableData: tableData,
            sortedTableData: tableData.slice(0, this.state.rowCountInPage),
            tableDataToDownload: tableDataToDownload
        });

        if(this.refs && this.refs.dataTableTemplate){
            this.refs.dataTableTemplate.doAfterSetResult();
        }
    }
    updateState(stateObj, cb) {
        if (stateObj) {
            if (cb) {
                this.setState(stateObj, cb);
            } else {
                this.setState(stateObj);
            }
        }
    }

    add3Dots(str, limit) {
        let dots = '...';
        if (str.length > limit) {
            str = str.substring(0, limit) + dots;
        }

        return str;
    }

    getDataleaks() {
        let orgId = localStorage.getItem(ORGANIZATION_ID);
        const data = {
            companiesKeywords: this.state.companiesKeywords,
            companies: this.state.companiesInputVal,
            orgId: orgId
        };

        this.startLoader();

        $.ajax({
            type: 'POST',
            url: '/api/getDataleaks',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            this.stopLoader();
            let assetUserInputAndMitigatedData = [];

            if (data && data.dataleaksData) {
                this.setState({});
                if(data.assetUserInputAndMitigatedData){
                    assetUserInputAndMitigatedData =  data.assetUserInputAndMitigatedData;
                }

                this.setState({fullData: data.dataleaksData, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
                this.setResults();
            }
        }).fail(() => {
            this.stopLoader();
            app.addAlert('error', 'Error: failed to fetch data');
        });
    }

    deleteDataleak(leakID, leakSource) {
        let tableData = this.state.tableData;
        let tableDataToDownload = this.state.tableDataToDownload;
        // Same code from surveys will work fine for dataleaks too.
        deleteIDFromSurveys(tableData, leakID);
        deleteIDFromSurveys(tableDataToDownload, leakID);

        if (!this.state.fuzzyString) {
            let page = this.state.page;
            if (page > 1 && page > tableData.length / this.state.rowCountInPage) {
                page--;
            }

            this.setState({
                tableData: tableData, totalRowCount: tableData.length, page: page,
                tableDataToDownload: tableDataToDownload,
                sortedTableData: tableData.slice((page - 1) * this.state.rowCountInPage,
                    (page - 1) * this.state.rowCountInPage + this.state.rowCountInPage)
            });
        } else {
            this.setState({
                tableData: tableData,
                tableDataToDownload: tableDataToDownload
            }, this.handleFilterValueChange(this.state.fuzzyString));
        }

        if (leakSource === DATALEAKS_PASTEBIN) {
            this.deletePastes(leakID);
        }
    }

    deletePastes(leakID) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'POST',
                url: '/api/deletePasteFromUser',
                data: JSON.stringify({lid: leakID}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'

            }).done(() => {
                app.addAlert('success', 'Deleted successfully');
            }).fail(() => {
                app.addAlert('error', 'Error: failed to delete data');
            });
        }
    }

    handleFilterValueChange(value) {
        let stateObj = FuzzySearchInTable(this.state.tableColumns, this.state.tableData, value);

        this.setState(stateObj);
    }

    setDefaultCompanies(result) {
        if (result && result.companies && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: result.companies,
                companiesInputVal: result.companiesInputVal,
                companiesKeywords: result.companiesPropArray
            }, this.getDataleaks);
        }
    }

    saveLoadedCompanies(companies, selectedCompanies) {
        let result = getLoadedCompaniesObject(companies, selectedCompanies, 'keywords');

        if (result && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: companies,
                companiesInputVal: result.companiesInputVal,
                companiesKeywords: result.companiesPropArray
            }, this.getDataleaks);
        }
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        let infoText = textForTitleExpl.dataleaks.expl;
        let shorterInfoText = textForTitleExpl.dataleaks.explShort;

        return (
            <div className={pageStyleClass}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <Loader show={this.state.showLoader} message={spinner}>
                        <div style={{margin: '40px 0px 40px 10px'}}>
                            <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                            <span style={{position: 'relative', top: '-6px', fontSize: 22}}>DATALEAKS</span>
                        </div>
                        <TitleExplanationComponent
                            openInfoBox={this.state.openInfoBox}
                            anchorEl={this.state.anchorEl}
                            shorterInfoText={shorterInfoText}
                            infoText={infoText}
                            handleCloseInfoBox={this.handleCloseInfoBox.bind(this)}
                            handleOpenInfoBox={this.handleOpenInfoBox.bind(this)}
                        />
                        <DataTableTemplate
                            ref="dataTableTemplate"
                            myDataType={DATALEAKS_TYPE}
                            tableColumns={this.state.tableColumns}
                            tableData={this.state.tableData}
                            sortedTableData={this.state.sortedTableData}
                            tableColumnsToDownload={this.state.tableColumnsToDownload}
                            tableDataToDownload={this.state.tableDataToDownload}
                            totalRowCount={this.state.totalRowCount}
                            companies={this.state.companies}
                            companiesInputVal={this.state.companiesInputVal}
                            companiesKeywords={this.state.companiesKeywords}
                            updateParentState={this.updateState}
                            setResults={this.setResults}
                            getData={this.getDataleaks}
                            setDefaultCompanies={this.setDefaultCompanies}
                            saveLoadedCompanies={this.saveLoadedCompanies}
                            enableRowClick={false}
                            assetUserInputAndMitigatedData={this.state.assetUserInputAndMitigatedData}
                            changeTableState={() => {
                            }}
                            updateStateForAMData={this.updateStateForAMData}
                        />
                    </Loader>
                </MuiThemeProvider>
            </div>
        );
    }
}
