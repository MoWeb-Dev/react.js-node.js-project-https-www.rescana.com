import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete';
import {findCompanyIndexByID, findCompanyIndexByName} from './dataleaks-helpers.js';

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class DataleaksUploadModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            companies: [],
            companiesInputVal: [],
            companiesInputKeywords: []
        };
    }

    handleClose() {
        this.setState({open: false},
            this.props.closeDialog());
    };

    handleAddRequestedCompanyField(chip) {
        if (!Array.isArray(chip)) {
            chip = [chip];
        }

        let companiesOptions = this.state.companies;
        let companiesInputVal = [];
        let companiesInputKeywords = [];

        companiesInputVal = companiesInputVal.concat(this.state.companiesInputVal);
        companiesInputKeywords = companiesInputKeywords.concat(this.state.companiesInputKeywords);

        let input = '';
        if (chip && chip[0] && chip[0].companyName) {
            input = chip[0].companyName;
        }
        // Check if the companies contain the input.
        let inputIndex = findCompanyIndexByName(companiesOptions, input);
        if (inputIndex > -1) {
            let companyObj = companiesOptions[inputIndex];
            let isExist = false;
            for (let i = 0; i < companiesInputVal.length; i++) {
                if (companiesInputVal[i] && companiesInputVal[i].companyName && companiesInputVal[i].companyName === input) {
                    isExist = true;
                    break;
                }
            }
            // Check if input was not already selected.
            if (!isExist) {
                // Add to the ChipSelect.
                companiesInputVal = companiesInputVal.concat(companyObj);
                // Add keywords to state.
                let addedKeywords = companyObj.keywords;
                if (addedKeywords) {
                    addedKeywords.map((keyword) => {
                        if (companiesInputKeywords.indexOf(keyword) === -1) {
                            companiesInputKeywords.push(keyword);
                        }
                    });
                }
                this.setState({companiesInputVal: companiesInputVal, companiesInputKeywords: companiesInputKeywords});
            }
        } else {
            app.addAlert('error', 'No such company.');
        }
    }

    handleDeleteRequestedCompanyField(value, index) {
        let companiesInputVal = [];
        let companiesInputKeywords = [];
        companiesInputVal = companiesInputVal.concat(this.state.companiesInputVal);
        let companies = this.state.companies;
        companiesInputKeywords = companiesInputKeywords.concat(this.state.companiesInputKeywords);
        let selectedIndex = findCompanyIndexByID(companies, value);
        if (selectedIndex > -1) {
            let keywordsToDelete = companies[selectedIndex].keywords;

            companiesInputVal.splice(index, 1);

            if (keywordsToDelete) {
                keywordsToDelete.map((keyword) => {
                    let isFoundInOtherCompanies = false;
                    for (let i = 0; i < companiesInputVal.length; i++) {
                        if (companiesInputVal[i].keywords && companiesInputVal[i].keywords.indexOf(keyword) !== -1) {
                            isFoundInOtherCompanies = true;
                            break;
                        }
                    }
                    if (!isFoundInOtherCompanies) {
                        companiesInputKeywords.splice(companiesInputKeywords.indexOf(keyword), 1);
                    }
                });
            }


            this.setState({companiesInputVal: companiesInputVal, companiesInputKeywords: companiesInputKeywords});
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open === true) {
            let stateObj={open: nextProps.open};
            if (nextProps.companies) {
                stateObj.companies = nextProps.companies;
            }
            if (nextProps.companiesInputVal) {
                stateObj.companiesInputVal = nextProps.companiesInputVal;
            }
            if (nextProps.companiesKeywords) {
                stateObj.companiesInputKeywords = nextProps.companiesKeywords;
            }
            // Set all companies as selected by default, so user can only delete or re-add them.
            this.setState(stateObj);
        }
    }

    ApplySettings() {
        this.handleClose();
        this.props.ApplyChangesFromSettings(this.state.companiesInputVal, this.state.companiesInputKeywords);
    }

    render() {
        let buttonsContainer = classNames({
            'uploadDialogButtonsLTR': true
        });

        const actions = [
            <FlatButton
                label="Cancel"
                secondary={true}
                onTouchTap={this.handleClose.bind(this)}
            />,
            <FlatButton
                label="Apply"
                secondary={true}
                onTouchTap={this.ApplySettings.bind(this)}
            />
        ];

        return (
            <div>
                <Dialog
                    title="Dataleaks Filter By Companies"
                    actions={actions}
                    actionsContainerClassName={buttonsContainer}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose.bind(this)}
                    autoScrollBodyContent={true}
                >
                    <ChipInput
                        hintText="Requested companies"
                        floatingLabelText="Requested companies"
                        value={this.state.companiesInputVal}
                        filter={AutoComplete.fuzzyFilter}
                        dataSource={this.props.companies}
                        dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                        onRequestAdd={(chip) => this.handleAddRequestedCompanyField(chip)}
                        onRequestDelete={(chip, index) => this.handleDeleteRequestedCompanyField(chip, index)}
                        maxSearchResults={5}
                        openOnFocus={true}
                    />
                </Dialog>
            </div>
        );
    }
};
