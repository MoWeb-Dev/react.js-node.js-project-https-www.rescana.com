import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

export default class UpdateAllDialog extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return <Dialog
            title="Update All Related Entities?"
            anchorOrigin={{horizontal: 'middle', vertical: 'center'}}
            open={this.props.open}
            modal={true}
            actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
            bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
            contentStyle={{width: '40%', borderRadius: '7px 7px 7px 7px'}}
            titleStyle={{
                fontSize: 18,
                background: 'rgba(0,0,0,0.7)',
                color: 'white', textAlign: 'center',
                borderRadius: '2px 2px 0px 0px',
                textTransform: 'uppercase',
            }}
            onRequestClose={this.props.onRequestClose}
            actions={[
                <FlatButton
                    label="Yes"
                    style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                    keyboardFocused={true}
                    backgroundColor={'#0091ea'}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    onTouchTap={this.props.updateDialogText.includes('Do you wish to add') ?
                        this.props.addToAll : this.props.removeFromAll}
                />,
                <FlatButton
                    label="No"
                    primary={true}
                    backgroundColor={'#0091ea'}
                    style={{color: 'white', marginBottom: '5px', height: 40}}
                    keyboardFocused={true}
                    hoverColor={'#12a4ff'}
                    rippleColor={'white'}
                    labelStyle={{fontSize: 10}}
                    onTouchTap={this.props.onRequestClose}
                />
            ]}
        >
            <br/>
            <br/>
            {this.props.updateDialogText}
            <br/>
            <br/>
            <br/>
        </Dialog>;
    }
}
