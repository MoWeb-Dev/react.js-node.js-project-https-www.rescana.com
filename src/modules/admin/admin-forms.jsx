import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import ManageUsersForm from './manageUsers/manage-users.jsx';
import ManageCompanyForm from './manageCompanies/manage-companies.jsx';
import ManageProjectsForm from './manageProjects/manage-projects.jsx';
import ManageTokensForm from './manageTokens/manage-tokens.jsx';
import ManageApiKeysForm from './manageApiKeys/manage-api-keys.jsx';
import ManualApis from './manualApis/manual-apis.jsx';
import moment from 'moment';
import themeForFont from "../app/themes/themeForFont";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import ManageOrganizationForm from "./manageOrganizations/manage-organizations.jsx";
import {API_KEY_STATUS} from '../../../config/consts.js';
import DisplayNewApiKeyModal from './manageApiKeys/display-new-api-key-modal.jsx';
import FlatButton from 'material-ui/FlatButton';

class AdminForms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'users',
            usersForDD: [],
            projects: [],
            totalProjectsCount: 0,
            users: [],
            companies: [],
            totalCompaniesCount: 0,
            tokens: [],
            organizations: [],
            totalOrganizationsCount: 0,
            apiKeys: [],
            newApiKeyToDisplay: '',
            surveyTemplatesForDD: [],
            organizationsForDD: [],
            projectsForDD: [],
            companiesForDD: [],
            showLoaderForOrganizations: true,
            showLoaderForProjects: true,
            showLoaderForCompanies: true,
            showLoaderForUsers: true,
            showLoaderForTokens: true,
            showLoaderForApiKeys: true
        };

        this.addCompany = this.addCompany.bind(this);
        this.deleteCompany = this.deleteCompany.bind(this);
        this.addProject = this.addProject.bind(this);
        this.deleteProject = this.deleteProject.bind(this);
        this.addOrganization = this.addOrganization.bind(this);
        this.getAllOrganizationsForDD = this.getAllOrganizationsForDD.bind(this);
        this.getAllSurveyTemplatesForDD = this.getAllSurveyTemplatesForDD.bind(this);
        this.getAllProjectsForDD = this.getAllProjectsForDD.bind(this);
        this.getAllCompaniesForDD = this.getAllCompaniesForDD.bind(this);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    handleScroll() {
        if (
            document.body.scrollTop > 20 ||
            document.documentElement.scrollTop > 20
        ) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

    topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    getAllOrganizations(pageIndex = 0, countInPage = 0, searchQuery = null, sortMethod = null) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            const dataToSend = {};
            if(pageIndex && countInPage) {
                dataToSend.sliceStartIndex = pageIndex;
                dataToSend.sliceMaxLength = countInPage;
                this.setState({showLoaderForOrganizations: true});
            }
            else {
                dataToSend.sliceStartIndex = 1;
                dataToSend.sliceMaxLength = 10;
            }
            if(searchQuery != null && typeof searchQuery === 'string') {
                dataToSend.searchQuery = searchQuery;
            }
            if(sortMethod && typeof sortMethod === 'string') {
                dataToSend.sortMethod = sortMethod;
            }

            $.ajax({
                type: 'get',
                url: '/admin/getAllOrganizationsWithPagination',
                data: dataToSend,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.organizations) {
                    const stateToUpdate = {
                        organizations: data.organizations,
                        showLoaderForOrganizations: false
                    };
                    if(data.totalOrganizationsCount != null) {
                        stateToUpdate.totalOrganizationsCount = data.totalOrganizationsCount;
                    }
                    this.setState(stateToUpdate, () => {
                        // Get all projects only if this is the first time loading the component. (Meaning this is not a pagination refresh)
                        if(!pageIndex && !countInPage) {
                            this.getAllProjects();
                        }
                    });
                } else {
                    if (data && data.error) {
                        console.log(data.error);
                    }
                    app.addAlert('error', 'Error: failed to load organizations.');

                    this.setState({showLoaderForOrganizations: false});
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load organizations.');

                this.setState({showLoaderForOrganizations: false});
            });
        }
    }

    getAllOrganizationsForDD() {
        $.ajax({
            type: 'get',
            url: '/admin/getAllOrganizationsForDD',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok && data.organizationsForDD) {
                this.setState({
                    organizationsForDD: data.organizationsForDD
                });
            } else {
                if (data && data.error) {
                    console.log(data.error);
                }
                app.addAlert('error', 'Error: failed to load organizations.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load organizations.');
        });
    }

    getAllSurveyTemplatesForDD() {
        $.ajax({
            type: 'get',
            url: '/admin/getAllSurveyTemplatesForDD',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok && data.surveyTemplatesForDD) {
                this.setState({
                    surveyTemplatesForDD: data.surveyTemplatesForDD
                });
            } else {
                if (data && data.error) {
                    console.log(data.error);
                }
                app.addAlert('error', 'Error: failed to load Survey Templates.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load Survey Templates.');
        });
    }

    getAllProjectsForDD() {
        $.ajax({
            type: 'get',
            url: '/admin/getAllProjectsForDD',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok && data.projectsForDD) {
                this.setState({
                    projectsForDD: data.projectsForDD
                });
            } else {
                if (data && data.error) {
                    console.log(data.error);
                }
                app.addAlert('error', 'Error: failed to load projects.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
        });
    }

    getAllCompaniesForDD() {
        $.ajax({
            type: 'get',
            url: '/admin/getAllCompaniesForDD',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok && data.companiesForDD) {
                this.setState({
                    companiesForDD: data.companiesForDD
                });
            } else {
                if (data && data.error) {
                    console.log(data.error);
                }
                app.addAlert('error', 'Error: failed to load projects.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
        });
    }

    getAllProjects(pageIndex = 0, countInPage = 0, searchQuery = null, sortMethod = null) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            const dataToSend = {};
            if(pageIndex && countInPage) {
                dataToSend.sliceStartIndex = pageIndex;
                dataToSend.sliceMaxLength = countInPage;
                this.setState({showLoaderForProjects: true});
            }
            else {
                dataToSend.sliceStartIndex = 1;
                dataToSend.sliceMaxLength = 10;
            }
            if(searchQuery != null && typeof searchQuery === 'string') {
                dataToSend.searchQuery = searchQuery;
            }
            if(sortMethod && typeof sortMethod === 'string') {
                dataToSend.sortMethod = sortMethod;
            }

            $.ajax({
                type: 'get',
                url: '/admin/getAllProjectsWithPagination',
                data: dataToSend,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.projects) {
                    const stateToUpdate = {
                        projects: data.projects,
                        showLoaderForProjects: false
                    };
                    if(data.totalProjectsCount != null) {
                        stateToUpdate.totalProjectsCount = data.totalProjectsCount;
                    }
                    this.setState(stateToUpdate, () => {
                        // Get all companies only if this is the first time loading the component. (Meaning this is not a pagination refresh)
                        if(!pageIndex && !countInPage) {
                            this.getAllCompanies();
                        }
                    });
                } else {
                    if (data && data.error) {
                        console.log(data.error);
                    }
                    app.addAlert('error', 'Error: failed to load projects.');

                    this.setState({showLoaderForProjects: false});
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load projects.');

                this.setState({showLoaderForProjects: false});
            });
        }
    }

    getAllTokens() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'get',
                url: '/admin/getAllTokens',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.tokens) {
                    this.setState({
                        tokens: data.tokens,
                        showLoaderForTokens: false
                    });
                } else {
                    if (data && data.error) {
                        console.log(data.error);
                    }
                    app.addAlert('error', 'Error: failed to load tokens.');

                    this.setState({showLoaderForTokens: false});
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load tokens.');

                this.setState({showLoaderForTokens: false});
            });
        }
    }

    getAllApiKeys() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'get',
                url: '/admin/getAllApiKeys',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.apiKeys && Array.isArray(data.apiKeys)) {
                    this.setState({
                        apiKeys: data.apiKeys,
                        showLoaderForApiKeys: false
                    });
                } else {
                    if (data && data.error) {
                        console.log('Error in getAllApiKeys: ', data.error);
                    }
                    app.addAlert('error', 'Error: failed to load API Keys.');

                    this.setState({showLoaderForApiKeys: false});
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load API Keys.');

                this.setState({showLoaderForApiKeys: false});
            });
        }
    }

    getAllCompanies(pageIndex = 0, countInPage = 0, searchQuery = null, sortMethod = null) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            const dataToSend = {};
            if(pageIndex && countInPage) {
                dataToSend.sliceStartIndex = pageIndex;
                dataToSend.sliceMaxLength = countInPage;
                this.setState({showLoaderForCompanies: true});
            }
            else {
                dataToSend.sliceStartIndex = 1;
                dataToSend.sliceMaxLength = 10;
            }
            if(searchQuery != null && typeof searchQuery === 'string') {
                dataToSend.searchQuery = searchQuery;
            }
            if(sortMethod && typeof sortMethod === 'string') {
                dataToSend.sortMethod = sortMethod;
            }
            $.ajax({
                type: 'get',
                url: '/admin/getAllCompaniesWithPagination',
                data: dataToSend,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.companies) {
                    const stateToUpdate = {
                        companies: data.companies,
                        showLoaderForCompanies: false
                    };
                    if(data.totalCompaniesCount != null) {
                        stateToUpdate.totalCompaniesCount = data.totalCompaniesCount;
                    }
                    this.setState(stateToUpdate);
                } else {
                    if (data && data.error) {
                        console.log(data.error);
                    }
                    app.addAlert('error', 'Error: failed to load companies.');

                    this.setState({showLoaderForCompanies: false});
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load companies.');

                this.setState({showLoaderForCompanies: false});
            });
        }
    }

    getAllUsers() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'get',
                url: '/admin/getAllUsers',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.users) {
                    let users = data.users;
                    let usersForDD = users.map((item) => {
                        return {email: item.email, id: item.id};
                    });

                    this.setState({
                        usersForDD: usersForDD,
                        users: users,
                        showLoaderForUsers: false
                    });
                } else {
                    if (data && data.error) {
                        console.log(data.error);
                    }
                    app.addAlert('error', 'Error: failed to load users.');

                    this.setState({showLoaderForUsers: false});
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load users.');

                this.setState({showLoaderForUsers: false});
            });
        }
    }

    saveProjects(projects) {
        this.setState({
            projects: projects
        });
    }

    addCompany(companyFields, deletedChipArray, addedChipArray, deletedIPChipArray, addedIPChipArray) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let data = companyFields;
            let extraData = {
                detachArrayToAddFromNeo: addedChipArray,
                detachArrayToRemoveFromNeo: deletedChipArray,
                detachIPArrayToAddFromNeo: addedIPChipArray,
                detachIPArrayToRemoveFromNeo: deletedIPChipArray,
            };

            data.createDate = moment().format();
            if (!data.ratios.surveyWeight) {
                data.ratios.surveyWeight = 0;
            }

            if (!data.ratios.intelWeight) {
                data.ratios.intelWeight = 0;
            }

            $.ajax({
                type: 'post',
                url: '/admin/addCompany',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({data: data, extraData: extraData}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.newCompany) {
                    let companies = this.state.companies;
                    let companiesAfterAdd = companies;
                    let companiesForDDAfterAdd = this.state.companiesForDD;
                    let found = false;

                    for (let i = 0; i < companies.length; i++) {
                        if (companies[i].id === data.newCompany.id) {
                            companiesAfterAdd[i] = data.newCompany;
                            found = true;

                            for (let j = 0; j < companiesForDDAfterAdd.length; j++) {
                                if (companiesForDDAfterAdd[j].id === data.newCompany.id) {
                                    companiesForDDAfterAdd[j].name = data.newCompany.companyName;
                                    break;
                                }
                            }

                            break;
                        }
                    }

                    if (!found) {
                        companiesAfterAdd.push(data.newCompany);

                        companiesForDDAfterAdd.push({
                            id: data.newCompany.id,
                            companyName: data.newCompany.companyName
                        });
                    }

                    app.addAlert('success', 'Companies Added.');

                    this.setState({companies: companiesAfterAdd, companiesForDD: companiesForDDAfterAdd});
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to add company.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add company.');
            });
        }
    }

    addProject(projectFields, addedChipArray, deletedChipArray) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;
            let data = projectFields;

            data.createDate = moment().format();
            let extraData = {
                detachArrayToAddToNeo: addedChipArray,
                detachArrayToRemoveFromNeo: deletedChipArray,
            };

            $.ajax({
                type: 'post',
                url: '/admin/addProject',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({uid: uid, data: data, extraData: extraData}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.newProject) {
                    let projects = this.state.projects;
                    let newProjectsAfterAdd = projects;
                    let newProjectsForDDAfterAdd = this.state.projectsForDD;
                    let found = false;

                    for (let i = 0; i < projects.length; i++) {
                        if (projects[i].id === data.newProject.id) {
                            newProjectsAfterAdd[i] = data.newProject;
                            found = true;

                            for (let j = 0; j < newProjectsForDDAfterAdd.length; j++) {
                                if (newProjectsForDDAfterAdd[j].id === data.newProject._id) {
                                    newProjectsForDDAfterAdd[j].name = data.newProject.projectName;
                                    break;
                                }
                            }

                            break;
                        }
                    }

                    if (!found) {
                        newProjectsAfterAdd.push(data.newProject);

                        newProjectsForDDAfterAdd.push({
                            id: data.newProject._id,
                            name: data.newProject.projectName
                        });
                    }

                    app.addAlert('success', 'Project Added.');

                    this.setState({openAddNewProjectModal: false, projects: newProjectsAfterAdd, projectsForDD: newProjectsForDDAfterAdd});
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to add project.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add project.');
            });
        }
    }


    addOrganization(organizationFields, projToAddToNeo, projToDeleteFromNeo) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;
            let orgData = organizationFields;

            orgData.createDate = moment().format();
            orgData.projToAddToNeo = projToAddToNeo;
            orgData.projToDeleteFromNeo = projToDeleteFromNeo;
            $.ajax({
                type: 'post',
                url: '/admin/addOrganization',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    uid: uid,
                    orgData: orgData,
                }),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.newOrganization) {
                    let organizations = this.state.organizations;
                    let newOrganizationsAfterAdd = organizations;
                    let newOrganizationsForDDAfterAdd = this.state.organizationsForDD;
                    let found = false;

                    for (let i = 0; i < organizations.length; i++) {
                        if (organizations[i]._id === data.newOrganization._id) {
                            newOrganizationsAfterAdd[i] = data.newOrganization;
                            found = true;

                            for (let j = 0; j < newOrganizationsForDDAfterAdd.length; j++) {
                                if (newOrganizationsForDDAfterAdd[j].id === data.newOrganization._id) {
                                    newOrganizationsForDDAfterAdd[j].name = data.newOrganization.organizationName;
                                    break;
                                }
                            }

                            break;
                        }
                    }

                    if (!found) {
                        newOrganizationsAfterAdd.push(data.newOrganization);

                        newOrganizationsForDDAfterAdd.push({
                            id: data.newOrganization._id,
                            name: data.newOrganization.organizationName
                        });
                    }

                    app.addAlert('success', 'Organization Added.');

                    this.setState({openAddNewProjectModal: false, organizations: newOrganizationsAfterAdd, organizationsForDD: newOrganizationsForDDAfterAdd});
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to add organization.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add organization.');
            });
        }
    }

    deleteCompany(companyId, selectedDomains, e) {
        e.preventDefault();

        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;

            $.ajax({
                type: 'post',
                url: '/admin/deleteCompany',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({uid: uid, id: companyId, selectedDomains: selectedDomains}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    function matchesEl(el) {
                        return el.id === companyId;
                    }

                    let CompaniesAfterDelete = this.state.companies.filter((companyId) => {
                        return !matchesEl(companyId);
                    });

                    const CompaniesForDDAfterDelete = this.state.companiesForDD.filter((companyId) => {
                        return !matchesEl(companyId);
                    });

                    this.setState({
                        companies: CompaniesAfterDelete,
                        companiesForDD: CompaniesForDDAfterDelete
                    });

                    app.addAlert('success', 'Record deleted.');
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to delete companies.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to delete company.');
            });
        }
    }


    deleteOrganization(organizationId, e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/admin/deleteOrganization',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({id: organizationId}),
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok) {
                function matchesEl(el) {
                    return el._id === organizationId;
                }

                function matchesEl2(el) {
                    return el.id === organizationId;
                }

                let organizationsAfterDelete = this.state.organizations.filter((organizationId) => {
                    return !matchesEl(organizationId);
                });

                const organizationsForDDAfterDelete = this.state.organizationsForDD.filter((organizationId) => {
                    return !matchesEl2(organizationId);
                });

                this.setState({
                    organizations: organizationsAfterDelete,
                    organizationsForDD: organizationsForDDAfterDelete
                });

                app.addAlert('success', 'Record deleted.');
            } else {
                app.addAlert('error', 'Error: failed to delete organization.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to delete organization.');
        });
    }

    deleteProject(projectId, e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/admin/deleteProject',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({id: projectId}),
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok) {
                function matchesEl(el) {
                    return el.id === projectId;
                }

                let ProjectsAfterDelete = this.state.projects.filter((projectId) => {
                    return !matchesEl(projectId);
                });

                const ProjectsforDDAfterDelete = this.state.projectsForDD.filter((projectId) => {
                    return !matchesEl(projectId);
                });

                this.setState({
                    projects: ProjectsAfterDelete,
                    projectsForDD: ProjectsforDDAfterDelete
                });

                app.addAlert('success', 'Record deleted.');
            } else {
                app.addAlert('error', 'Error: failed to delete project.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to delete project.');
        });
    }

    runCompanyQuery(companyId, e) {
        e.preventDefault();

        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'post',
                url: '/admin/runCompanyQuery',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({id: companyId}),
                dataType: 'json'
            }).done((data) => {
                if (data && !data.ok && data.error) {
                    console.log(data.error);
                }
            }).fail(() => {
                app.addAlert('error', 'This might take a few minutes.');
            });
        }
    }

    runDiscoveryQuery(companyId, e) {
        e.preventDefault();

        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'post',
                url: '/admin/runDiscoveryQuery',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({id: companyId}),
                dataType: 'json'
            }).done((data) => {
                if (data && !data.ok && data.error) {
                    console.log(data.error);
                }
            }).fail(() => {
                app.addAlert('error', 'This might take a few minutes.');
            });
        }
    }

    runProjectQuery(projectId, e) {
        e.preventDefault();

        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'post',
                url: '/admin/runProjectQuery',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({id: projectId}),
                dataType: 'json'
            }).done((data) => {
                if (data && !data.ok && data.error) {
                    console.log(data.error);
                }
            }).fail(() => {
                app.addAlert('error', 'This might take a few minutes.');
            });
        }
    }

    addApiKey(organizationsInput, isScoreBackwardsEnabled = false, e) {
        if(e && e.preventDefault) {
            e.preventDefault();
        }

        if(organizationsInput && Array.isArray(organizationsInput)) {
            let user = app.getAuthUser();

            if (!user) {
                app.routeAuthUser();
            } else {
                $.ajax({
                    type: 'post',
                    url: '/admin/addApiKey',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        organizations: organizationsInput,
                        isScoreBackwardsEnabled: isScoreBackwardsEnabled
                    }),
                    dataType: 'json'
                }).done((res) => {
                    if(res && res.ok && res.data) {
                        app.addAlert('success', 'API Key inserted successfully');

                        // Update state.
                        const stateToUpdate = {
                            apiKeys: this.state.apiKeys
                        };

                        stateToUpdate.apiKeys.push(res.data);

                        if(res.originalApiKey) {
                            stateToUpdate.newApiKeyToDisplay = res.originalApiKey;
                        }

                        this.setState(stateToUpdate);
                    }
                    else {
                        if (res && res.error) {
                            console.log('Error in addApiKey: ', res.error);
                        }
                        app.addAlert('error', 'Failed to add API Key');
                    }
                }).fail(() => {
                    app.addAlert('error', 'Failed to add API Key');
                });
            }
        }
    }

    handleCloseNewApiKeyModal() {
        // When newApiKeyToDisplay has a value - the modal will open, otherwise will be closed.
        this.setState({newApiKeyToDisplay: ''});
    }

    handleDoneEditKey(keyInput, organizationsInput, isScoreBackwardsEnabled = false, e) {
        if(e && e.preventDefault) {
            e.preventDefault();
        }

        if(keyInput && organizationsInput && Array.isArray(organizationsInput)) {
            let user = app.getAuthUser();

            if (!user) {
                app.routeAuthUser();
            } else {
                $.ajax({
                    type: 'post',
                    url: '/admin/editApiKey',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        key: keyInput,
                        organizations: organizationsInput,
                        isScoreBackwardsEnabled: isScoreBackwardsEnabled
                    }),
                    dataType: 'json'
                }).done((res) => {
                    if(res && res.ok && res.data) {
                        app.addAlert('success', 'API Key updated successfully');

                        let allApiKeys = this.state.apiKeys;
                        if(allApiKeys && Array.isArray(allApiKeys) && allApiKeys.length > 0){
                            allApiKeys.map((curApi)=>{
                                if(curApi && res.data && curApi.key && res.data.key && curApi.key === res.data.key){
                                    curApi.isScoreBackwardsEnabled = res.data.isScoreBackwardsEnabled || false;
                                }
                            });
                        }
                        // Update state.
                        const stateToUpdate = {
                            apiKeys: allApiKeys
                        };

                        const matchedApiKey = stateToUpdate.apiKeys.find((k) => {return (k && k.key && k.key === keyInput);});

                        if(matchedApiKey) {
                            matchedApiKey.organizations = organizationsInput;

                            this.setState(stateToUpdate);
                        }
                    }
                    else {
                        if (res && res.error) {
                            console.log('Error in editApiKey: ', res.error);
                        }
                        app.addAlert('error', 'Failed to edit API Key');
                    }
                }).fail(() => {
                    app.addAlert('error', 'Failed to edit API Key');
                });
            }
        }
    }

    deleteApiKey(keyToDelete, e) {
        if(e && e.preventDefault) {
            e.preventDefault();
        }

        if(keyToDelete) {
            let user = app.getAuthUser();

            if (!user) {
                app.routeAuthUser();
            } else {
                $.ajax({
                    type: 'post',
                    url: '/admin/deleteApiKey',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        keyToDelete: keyToDelete
                    }),
                    dataType: 'json'
                }).done((res) => {
                    if(res && res.ok) {
                        app.addAlert('success', 'API Key was successfully deleted');

                        // Update state.
                        const stateToUpdate = {
                            apiKeys: this.state.apiKeys
                        };

                        let indexToDelete = -1;
                        let currKey;
                        for(let i=0; i< stateToUpdate.apiKeys.length ; i++){
                            currKey = stateToUpdate.apiKeys[i];
                            if(currKey && currKey.key && currKey.key === keyToDelete) {
                                indexToDelete = i;
                                break;
                            }
                        }

                        if(indexToDelete > -1) {
                            stateToUpdate.apiKeys.splice(indexToDelete, 1);
                            this.setState(stateToUpdate);
                        }
                    }
                    else {
                        if (res && res.error) {
                            console.log('Error in addApiKey: ', res.error);
                        }
                        app.addAlert('error', 'Failed to add API Key');
                    }
                }).fail(() => {
                    app.addAlert('error', 'Failed to add API Key');
                });
            }
        }
    }

    updateApiKeyStatus(keyData, newStatus, e) {
        if(e && e.preventDefault) {
            e.preventDefault();
        }

        if(keyData && keyData.key && newStatus) {
            let user = app.getAuthUser();

            if (!user) {
                app.routeAuthUser();
            } else {
                $.ajax({
                    type: 'post',
                    url: '/admin/editApiKeyStatus',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        key: keyData.key,
                        newStatus: newStatus
                    }),
                    dataType: 'json'
                }).done((res) => {
                    if(res && res.ok) {
                        const status = (newStatus === API_KEY_STATUS.ACTIVE) ? 'activated' : 'suspended';
                        app.addAlert('success', 'API Key ' + status + ' successfully');

                        // Update state.
                        const stateToUpdate = {
                            apiKeys: this.state.apiKeys
                        };

                        const matchedApiKey = stateToUpdate.apiKeys.find((k) => {return (k && k.key && k.key === keyData.key);});

                        if(matchedApiKey) {
                            matchedApiKey.status = newStatus;

                            this.setState(stateToUpdate);
                        }
                    }
                    else {
                        if (res && res.error) {
                            console.log('Error in updateApiKeyStatus: ', res.error);
                        }
                        app.addAlert('error', 'Failed to change API Key status');
                    }
                }).fail(() => {
                    app.addAlert('error', 'Failed to change API Key status');
                });
            }
        }
    }

    componentDidMount() {
        let user = app.getAuthUser();
        window.addEventListener("scroll", this.handleScroll);
        document.getElementById("myBtn").style.display = "none";

        if (!user) {
            app.routeAuthUser();
        } else {
            this.getAllUsers();
            this.getAllOrganizations();
            this.getAllTokens();
            this.getAllApiKeys();
            this.getAllOrganizationsForDD();
            this.getAllSurveyTemplatesForDD();
            this.getAllProjectsForDD();
            this.getAllCompaniesForDD();
        }
    }


    render() {

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleChange}>
                        <Tab label="Companies" value="companies">
                            <div>
                                <ManageCompanyForm
                                    ref="companyManagment"
                                    identifier="companies"
                                    users={this.state.usersForDD}
                                    projects={this.state.projects}
                                    companies={this.state.companies}
                                    totalCompaniesCount={this.state.totalCompaniesCount}
                                    deleteCompany={this.deleteCompany.bind(this)}
                                    addCompany={this.addCompany.bind(this)}
                                    runCompanyQuery={this.runCompanyQuery.bind(this)}
                                    runDiscoveryQuery={this.runDiscoveryQuery.bind(this)}
                                    showLoaderForCompanies={this.state.showLoaderForCompanies}
                                    handleChangeDataViewClick={this.getAllCompanies.bind(this)}
                                />
                            </div>
                        </Tab>
                        <Tab label="Projects" value="Projects">
                            <div>
                                <ManageProjectsForm
                                    ref="projectManagment"
                                    identifier="Projects"
                                    users={this.state.usersForDD}
                                    projects={this.state.projects}
                                    totalProjectsCount={this.state.totalProjectsCount}
                                    companies={this.state.companiesForDD}
                                    saveProjects={this.saveProjects.bind(this)}
                                    deleteProject={this.deleteProject.bind(this)}
                                    addProject={this.addProject.bind(this)}
                                    runProjectQuery={this.runProjectQuery.bind(this)}
                                    showLoaderForProjects={this.state.showLoaderForProjects}
                                    handleChangeDataViewClick={this.getAllProjects.bind(this)}
                                />
                            </div>
                        </Tab>
                        <Tab label="Organizations" value="Organizations">
                            <div>
                                <ManageOrganizationForm
                                    ref="OrganizationManagement"
                                    identifier="Organizations"
                                    users={this.state.usersForDD}
                                    organizations={this.state.organizations}
                                    totalOrganizationsCount={this.state.totalOrganizationsCount}
                                    projects={this.state.projectsForDD}
                                    companies={this.state.companies}
                                    surveyTemplatesForDD={this.state.surveyTemplatesForDD}
                                    deleteOrganization={this.deleteOrganization.bind(this)}
                                    addOrganization={this.addOrganization.bind(this)}
                                    showLoaderForOrganizations={this.state.showLoaderForOrganizations}
                                    handleChangeDataViewClick={this.getAllOrganizations.bind(this)}
                                />
                            </div>
                        </Tab>
                        <Tab label="User Management" value="users">
                            <div>
                                <ManageUsersForm
                                    ref="userManagment"
                                    identifier="users"
                                    users={this.state.users}
                                    showLoaderForUsers={this.state.showLoaderForUsers}
                                />
                            </div>
                        </Tab>
                        <Tab label="Tokens" value="tokens">
                            <div>
                                <ManageTokensForm
                                    ref="tokensManagement"
                                    identifier="Tokens"
                                    tokens={this.state.tokens}
                                    showLoaderForTokens={this.state.showLoaderForTokens}
                                />
                            </div>
                        </Tab>
                        <Tab label="API Keys" value="apiKeys">
                            <div>
                                <ManageApiKeysForm
                                    ref="apiKeysManagement"
                                    identifier="apiKeys"
                                    apiKeys={this.state.apiKeys}
                                    organizations={this.state.organizationsForDD}
                                    addApiKey={this.addApiKey.bind(this)}
                                    handleDoneEditKey={this.handleDoneEditKey.bind(this)}
                                    deleteApiKey={this.deleteApiKey.bind(this)}
                                    updateApiKeyStatus={this.updateApiKeyStatus.bind(this)}
                                    showLoaderForApiKeys={this.state.showLoaderForApiKeys}
                                />
                            </div>
                        </Tab>
                        <Tab label="Manual API" value="apis">
                            <div>
                                <ManualApis
                                    ref="manualApis"
                                    identifier="apis"
                                    companies={this.state.companiesForDD}
                                />
                            </div>
                        </Tab>
                    </Tabs>
                    <DisplayNewApiKeyModal
                        newApiKeyToDisplay={this.state.newApiKeyToDisplay}
                        cancel={this.handleCloseNewApiKeyModal.bind(this)}
                    />
                    <FlatButton id="myBtn"
                                style={{
                                    transform: 'rotate(90deg)',
                                    backgroundColor: "rgba(0,0,0,0.1)",
                                    borderRadius: 100,
                                    height: 40, width: 3,
                                    position: 'fixed',
                                    bottom: -20, right: 130}}
                                label={"<"}
                                labelStyle={{paddingRight: "70px",position: "relative", bottom: "1.5px"}}
                                onClick={this.topFunction}>
                    </FlatButton>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default AdminForms;
