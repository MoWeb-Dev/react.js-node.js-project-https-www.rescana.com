import React from 'react';
import DataTableViewTemplate from '../../common/datatableTemplate/datatable-template.jsx';

const moment = require('moment');
import FlatButton from 'material-ui/FlatButton';

export default class tokensTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tokens: this.props.tokens || [],
            isRevokedArr: []
        };
        this.handleTokenRevoke = this.handleTokenRevoke.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let tokens = nextProps.tokens;
        let revokedArrForUpdate = [];

        if (tokens) {
            tokens.map((item) => {
                revokedArrForUpdate.push(item.isTokenRevoked);
            });
        }

        this.setState({
            tokens: nextProps.tokens,
            isRevokedArr: revokedArrForUpdate
        });
    }

    handleTokenRevoke(data, idx) {
        $.ajax({
            type: 'POST',
            url: '/api/addRevokedTokenToList',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(() => {
            let revokedArrForUpdate = this.state.isRevokedArr;
            if (revokedArrForUpdate) {
                for (let i = 0; i < revokedArrForUpdate.length; i++) {
                    if (idx === i) {
                        revokedArrForUpdate[i] = true;
                        break;
                    }
                }
                this.setState({
                    isRevokedArr: revokedArrForUpdate
                });
            }
            app.addAlert('success', 'Revoked Token Success!');

        }).fail(() => {
            app.addAlert('error', 'Error: Failed to Revoked Token');
        });
    }

    render() {
        const styles = {
            chip: {
                margin: 4
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap'
            },
            tableStyle: {
                paddingTop: '8px',
                paddingBottom: '8px'
            },
            wrappableStyle: {
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-word'
            }
        };

        const tableColumns = [
            {
                sortable: true,
                label: 'User',
                key: 'userEmail'
            },
            {
                sortable: true,
                label: 'Sender',
                key: 'senderEmail',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Receiver',
                key: 'receiverEmail',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Company',
                key: 'companyName',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Survey Name',
                key: 'surveyName',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Token Creation',
                key: 'tokenCreationTime',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Token Expiration',
                key: 'tokenExpirationTime',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Type',
                key: 'isSentWithEmail',
                style: styles.wrappableStyle
            },
            {
                label: 'Revoke Token',
                key: 'isRevoked',
                noneSearchable: true
            }
        ];

        const tableData = [];
        this.state.tokens.map((currToken, i) => {
            tableData.push({
                userEmail: currToken.userEmail,
                senderEmail: currToken.senderEmail,
                receiverEmail: currToken.receiverEmail,
                companyName: currToken.companyName,
                surveyName: currToken.surveyName,
                tokenCreationTime: moment(currToken.tokenCreationTime).calendar(null, {
                    sameDay: 'YYYY/MM/DD, HH:mm',
                    nextDay: 'YYYY/MM/DD, HH:mm',
                    nextWeek: 'YYYY/MM/DD, HH:mm',
                    lastDay: 'YYYY/MM/DD, HH:mm',
                    lastWeek: 'YYYY/MM/DD, HH:mm',
                    sameElse: 'YYYY/MM/DD, HH:mm'
                }),
                tokenExpirationTime: moment(currToken.tokenExpirationTime).calendar(null, {
                    sameDay: 'YYYY/MM/DD, HH:mm',
                    nextDay: 'YYYY/MM/DD, HH:mm',
                    nextWeek: 'YYYY/MM/DD, HH:mm',
                    lastDay: 'YYYY/MM/DD, HH:mm',
                    lastWeek: 'YYYY/MM/DD, HH:mm',
                    sameElse: 'YYYY/MM/DD, HH:mm'
                }),
                isSentWithEmail: currToken.isSentWithEmail ? 'Email' : 'Link',
                isRevoked: <FlatButton
                    disabled={this.state.isRevokedArr[i]}
                    onClick={this.handleTokenRevoke.bind(this, currToken, i)}>Revoke
                </FlatButton>
            });
        });

        return (
            <div>
                <DataTableViewTemplate
                    myDataType={'adminTokens'}
                    tableColumns={tableColumns}
                    tableData={tableData}
                    hideDownloadExcel={true}
                    hideCardBackground={true}
                />
            </div>
        );
    }
}
