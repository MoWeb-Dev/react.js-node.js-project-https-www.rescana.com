import React from 'react';
import {Card} from 'material-ui/Card';
import TokensTable from './tokens-table.jsx';
import Loader from 'react-loader-advanced';
import classNames from "classnames";

class ManageTokensForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tokens: this.props.tokens,
        };
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            tokens: nextProps.tokens,
        });
    }

    render() {
        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        return (
            <div style={{backgroundColor: 'grey200'}}>
                <div style={{margin: '30px 30px 0px 30px'}}>
                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                    <span style={{position: 'relative', top: '-6px', fontSize: 18}}>Tokens</span>
                </div>
                <Card style={{margin: 30}}>
                    <Loader show={this.props.showLoaderForTokens} message={spinner}>
                        <div>
                            <TokensTable
                                tokens={this.state.tokens}
                            />
                        </div>
                    </Loader>
                </Card>
            </div>
        );
    }
}

export default ManageTokensForm;
