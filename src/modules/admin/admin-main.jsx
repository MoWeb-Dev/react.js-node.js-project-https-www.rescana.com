import React from 'react';
import {browserHistory} from 'react-router';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AdminForms from './admin-forms.jsx';
import classNames from 'classnames';
import '../../../node_modules/react-mfb/mfb.css';
import themeForFont from "../app/themes/themeForFont";
import IconButton from 'material-ui/IconButton';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import AddIcon from 'material-ui/svg-icons/content/add-circle';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from "material-ui/AutoComplete";
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import AddPersonIcon from 'material-ui/svg-icons/social/person-add';
import AddCompanyIcon from 'material-ui/svg-icons/social/public';
import AddProjectIcon from 'material-ui/svg-icons/hardware/device-hub';
import AddOrganizationIcon from 'material-ui/svg-icons/action/work';
import AddApiKeyIcon from 'material-ui/svg-icons/communication/vpn-key';


class Admin extends React.Component {
    constructor() {
        super();

        this.state = {
            users: {},
            fixedHeader: true,
            stripedRows: false,
            showRowHover: false,
            height: '100%',
            openDialog: false,
            user: app.getAuthUser(),
            openAdminSettingsDialog: false,
            emailsToBeNotifiedInputVal: [],
            emailsForCancelInputVal: [],
            adminUsers: [],
            addedChipArray: [],
            deletedChipArray: [],
        };
        this.handleRowSelection = this.handleRowSelection.bind(this);
        this.handleOpenAdminSettingsDialog = this.handleOpenAdminSettingsDialog.bind(this);
        this.updateEmailsToBeNotified = this.updateEmailsToBeNotified.bind(this);
        this.handleAddAdminToList = this.handleAddAdminToList.bind(this);
        this.handleDeleteAdminFromList = this.handleDeleteAdminFromList.bind(this);
        this.chipArrCheckWhenAdded = this.chipArrCheckWhenAdded.bind(this);
        this.chipArrCheckWhenDeleted = this.chipArrCheckWhenDeleted.bind(this);
        this.clearAdminSettingFields = this.clearAdminSettingFields.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    chipArrCheckWhenAdded(chipId) {
        let addedChipArray = this.state.addedChipArray;

        if (this.state.deletedChipArray.includes(chipId)) {
            let deletedChipArray = this.state.deletedChipArray;
            const index = deletedChipArray.indexOf(chipId);

            if (index > -1) {
                deletedChipArray.splice(index, 1);
                this.setState({deletedChipArray: deletedChipArray})
            }
        } else {
            addedChipArray.push(chipId);
            this.setState({addedChipArray: addedChipArray})
        }
    }

    chipArrCheckWhenDeleted(chipId) {
        let deletedChipArray = this.state.deletedChipArray;

        if (this.state.addedChipArray.includes(chipId)) {
            let addedChipArray = this.state.addedChipArray;
            const index = addedChipArray.indexOf(chipId);

            if (index > -1) {
                addedChipArray.splice(index, 1);
                this.setState({addedChipArray: addedChipArray})
            }
        } else {
            deletedChipArray.push(chipId);
            this.setState({deletedChipArray: deletedChipArray})
        }
    }

    componentWillMount() {
        app.setFlagVisibile(false);
        this.getAllEmailsToBeNotifiedOnVendorCreation();
    }

    componentDidMount() {
        if (!this.state.user) {
            app.routeAuthUser(true);
        }
    }

    handleRowSelection(selected) {
        let id = this.state.data[selected[0]].id;

        browserHistory.push(`/users-${id}`);
    }

    getAllEmailsToBeNotifiedOnVendorCreation() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            $.ajax({
                type: 'get',
                url: '/admin/getAllEmailsToBeNotifiedOnVendorCreation',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.emailsToBeNotified) {
                    let emailsToBeNotified = data.emailsToBeNotified;
                    let emailsToBeNotifiedInputVal = [];
                    emailsToBeNotified.map((email) => {

                        if (email.email) {
                            emailsToBeNotifiedInputVal.push(email.email);
                        }
                    });
                    this.setState({
                        emailsToBeNotifiedInputVal: emailsToBeNotifiedInputVal,
                        adminUsersEmails: emailsToBeNotified
                    });

                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to load admin Users.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load admin Users.');
            });
        }
    }

    updateEmailsToBeNotified() {
        let user = app.getAuthUser();
        if (!user) {
            app.routeAuthUser();
        } else {
            let data = {};
            data.emailsToInclude = [];
            data.emailsToRemove = [];

            if (this.state.addedChipArray) {
                data.emailsToInclude = this.state.addedChipArray;
            }
            if (this.state.deletedChipArray) {
                data.emailsToRemove = this.state.deletedChipArray;
            }
            $.ajax({
                type: 'post',
                url: '/admin/updateEmailsToBeNotifiedOnVendorCreationOnMongo',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({data: data}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    app.addAlert('success', 'Updated Admin Users Setting.');
                } else {
                    app.addAlert('error', 'Error: Failed To Updated Admin Users Setting.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: Failed To Updated Admin Users Setting.');
            });
        }
    }

    cancel() {
        let savedChipVal = this.state.emailsForCancelInputVal;
        this.setState({emailsToBeNotifiedInputVal:savedChipVal, openAdminSettingsDialog: false});
    }


    handleOpenAdminSettingsDialog() {
        let savedChipVal = [];
        this.state.emailsToBeNotifiedInputVal.map((chip)=>{
            savedChipVal.push(chip);
        });
        this.setState({emailsForCancelInputVal: savedChipVal ,openAdminSettingsDialog: true});
    }

    handleAddAdminToList(chip) {
        const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if(chip.match(mailFormat)){
            let emailsToBeNotifiedInputVal = this.state.emailsToBeNotifiedInputVal;
            emailsToBeNotifiedInputVal.push(chip);
            this.chipArrCheckWhenAdded(chip);
            this.setState({emailsToBeNotifiedInputVal: emailsToBeNotifiedInputVal});
        }else {
            app.addAlert('error','Invalid Email');
        }
    }

    handleDeleteAdminFromList(value, index) {
        let emailsToBeNotifiedInputVal = this.state.emailsToBeNotifiedInputVal;
        emailsToBeNotifiedInputVal.splice(index, 1);
        this.chipArrCheckWhenDeleted(value);
        this.setState({emailsToBeNotifiedInputVal: emailsToBeNotifiedInputVal});
    }

    clearAdminSettingFields() {
        this.setState({openAdminSettingsDialog: false, addedChipArray: [], deletedChipArray: []});
    }

    render() {
        let pageMainClass = classNames({
            'containerLTR': true
        });

        let adminSettingsContent = <div style={{padding: '50px 20px 50px 20px'}}>
            <div style={{fontSize: 18, marginBottom: 5}}>Notifications:</div>
            <div style={{fontSize: 14}}>Which admin users will receive email notifications when new entity is created by
                non admin
                user:
            </div>
            <ChipInput
                hintText="Emails to include"
                floatingLabelText="Emails to include"
                value={this.state.emailsToBeNotifiedInputVal}
                filter={AutoComplete.fuzzyFilter}
                onRequestAdd={(chip) => this.handleAddAdminToList(chip)}
                onRequestDelete={(chip, index) => this.handleDeleteAdminFromList(chip, index)}
                maxSearchResults={5}
                style={{bottom: 10}}
            />
        </div>;

        const styles = {
            menuItem: {
                color: '#ffffff',
                fontSize: '13px'
            },
            menuItemIcon: {
                fill: '#ffffff'
            }
        };

        return (
            <div className={pageMainClass}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <div>
                        <IconButton
                            tooltip="settings"
                            touch={true}
                            tooltipPosition="bottom-right"
                            onTouchTap={this.handleOpenAdminSettingsDialog}
                            style={{float: 'right', right: 40, bottom: 10}}>
                            <SettingsIcon/>
                        </IconButton>
                        <IconMenu
                            iconButtonElement={<IconButton
                                tooltip="add"
                                touch={true}
                                tooltipPosition="bottom-center"
                            ><AddIcon /></IconButton>}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            style={{float: 'right', right: 40, bottom: 10}}
                            menuStyle={{backgroundColor: '#444444'}}
                        >
                            <MenuItem primaryText="Add User" rightIcon={<AddPersonIcon style={styles.menuItemIcon}/>} style={styles.menuItem}
                                      onClick={() => this.refs.adminForms.refs.userManagment.handleOpenAddNewUsersModal()}
                            />
                            <MenuItem primaryText="Add Company" rightIcon={<AddCompanyIcon style={styles.menuItemIcon}/>} style={styles.menuItem}
                                      onClick={() => this.refs.adminForms.refs.companyManagment.handleOpenAddNewCompanyModal()}
                            />
                            <MenuItem primaryText="Add Project" rightIcon={<AddProjectIcon style={styles.menuItemIcon}/>} style={styles.menuItem}
                                      onClick={() => this.refs.adminForms.refs.projectManagment.handleOpenAddNewProjectModal()}
                            />
                            <MenuItem primaryText="Add Organization" rightIcon={<AddOrganizationIcon style={styles.menuItemIcon}/>} style={styles.menuItem}
                                      onClick={() => this.refs.adminForms.refs.OrganizationManagement.handleOpenAddNewOrganizationModal()}
                            />
                            <MenuItem primaryText="Add API Key" rightIcon={<AddApiKeyIcon style={styles.menuItemIcon}/>} style={styles.menuItem}
                                      onClick={() => this.refs.adminForms.refs.apiKeysManagement.handleOpenAddKey()}
                            />
                        </IconMenu>
                        <Dialog
                            title="Admin Settings"
                            modal={true}
                            autoScrollBodyContent={true}
                            open={this.state.openAdminSettingsDialog || false}
                            contentStyle={{maxWidth: '45%', borderRadius: '7px 7px 7px 7px'}}
                            actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                            bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                            titleStyle={{
                                fontSize: 18, background: 'rgba(0,0,0,0.7)', color: 'white', textAlign: 'center',
                                borderRadius: '2px 2px 0px 0px', textTransform: 'uppercase',
                            }}
                            actions={[
                                <FlatButton
                                    label="Save"
                                    primary={true}
                                    keyboardFocused={false}
                                    onTouchTap={() => {
                                        this.updateEmailsToBeNotified();
                                        this.clearAdminSettingFields()
                                    }}
                                    style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                                    backgroundColor={'#0091ea'}
                                    hoverColor={'#12a4ff'}
                                    rippleColor={'white'}
                                    labelStyle={{fontSize: 10}}
                                />,
                                <FlatButton
                                    label="Cancel"
                                    primary={true}
                                    keyboardFocused={true}
                                    onTouchTap={this.cancel}
                                    style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                                    backgroundColor={'#0091ea'}
                                    hoverColor={'#12a4ff'}
                                    rippleColor={'white'}
                                    labelStyle={{fontSize: 10}}
                                />
                            ]}
                        >
                            {adminSettingsContent}
                        </Dialog>
                        <div style={{margin: '53px', marginBottom: '55px', fontSize: 24}}>ADMIN</div>
                        <AdminForms
                            ref="adminForms"
                            onSave={this.handleSaveAccountSettings}
                            onChange={this.setAccountSettingsState}
                            errors={this.state.errors}/>
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}

export default Admin;
