import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import UpdateAllDialog from '../update-all-dialog.jsx';
import Checkbox from 'material-ui/Checkbox';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

export default class AddOrganizationModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            openAddNewOrganizationModal: false,
            organizationFields: this.props.organizationFields,
            usersForDD: this.props.allUsers,
            projectsInputVal: this.props.projects,
            usersInputVal: [],
            projects: [],
            openUpdateAllDialog: false,
            currUserChipInput: '',
            updateDialogText: '',
            addedProjectsChipArray: [],
            deletedProjectsChipArray: [],
            isSendAutoSurveyFromApi: false,
            showScanDate: false,
            apiAuditorEmail: '',
            apiSelectedTemplate: [],
            surveyTemplatesForDD: this.props.surveyTemplatesForDD || []
        };

        this.handleDeleteAllowedUsersField = this.handleDeleteAllowedUsersField.bind(this);
        this.handleOpenUpdateAllDialog = this.handleOpenUpdateAllDialog.bind(this);
        this.handleCloseUpdateAllDialog = this.handleCloseUpdateAllDialog.bind(this);
        this.chipArrCheckWhenAdded = this.chipArrCheckWhenAdded.bind(this);
        this.chipArrCheckWhenDeleted = this.chipArrCheckWhenDeleted.bind(this);
    }

    handleOpenUpdateAllDialog() {
        this.setState({openUpdateAllDialog: true});
    };

    handleCloseUpdateAllDialog() {
        this.setState({openUpdateAllDialog: false});
    };

    handleRemoveFromAll() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;
            let orgData = this.props.organizationFields;
            let userIdToRemove = this.state.currUserChipInput;

            $.ajax({
                type: 'post',
                url: '/admin/removeAllowedUsersFromOrgProjects',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    uid: uid,
                    orgData: orgData,
                    userIdToRemove: userIdToRemove
                }),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    app.addAlert('warning', 'Please Note! In case you updated user list don\'t forget to update all surveys templates as well!');
                    app.addAlert('success', 'removed.');
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to update.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to update.');
            });
        }
    }

    handleAddToAll() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;
            let orgData = this.props.organizationFields;
            let userToAdd = this.state.currUserChipInput;

            $.ajax({
                type: 'post',
                url: '/admin/addAllowedUsersToOrgProjects',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    uid: uid,
                    orgData: orgData,
                    userToAdd: userToAdd
                }),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    app.addAlert('success', 'updated.');
                    app.addAlert('warning', 'Please Note! In case you updated user list don\'t forget to update all surveys templates as well!');
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to update.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to update.');
            });
        }
    }


    handleChangeOrganizationField(e) {
        let organizationFields = this.props.organizationFields;
        organizationFields.organizationName = e.target.value;
        this.setState(organizationFields);
    }

    handleAddProjectToList(chip) {
        let projectsInputVal = this.state.projectsInputVal;
        let organizationFields = this.state.organizationFields;
        projectsInputVal.push(chip);
        organizationFields.selectedProjects = projectsInputVal;
        this.chipArrCheckWhenAdded(chip.id);
        this.setState({organizationFields: organizationFields, projectsInputVal: projectsInputVal});
    }

    chipArrCheckWhenAdded(chipId) {
        let addedProjectsChipArray = this.state.addedProjectsChipArray;

        if (this.state.deletedProjectsChipArray.includes(chipId)) {
            let deletedProjectsChipArray = this.state.deletedProjectsChipArray;
            const index = deletedProjectsChipArray.indexOf(chipId);

            if (index > -1) {
                deletedProjectsChipArray.splice(index, 1);
                this.setState({deletedProjectsChipArray: deletedProjectsChipArray});
            }
        } else {
            addedProjectsChipArray.push(chipId);
            this.setState({addedProjectsChipArray: addedProjectsChipArray});
        }
    }

    chipArrCheckWhenDeleted(chipId) {
        let deletedProjectsChipArray = this.state.deletedProjectsChipArray;

        if (this.state.addedProjectsChipArray.includes(chipId)) {
            let addedProjectsChipArray = this.state.addedProjectsChipArray;
            const index = addedProjectsChipArray.indexOf(chipId);

            if (index > -1) {
                addedProjectsChipArray.splice(index, 1);
                this.setState({addedProjectsChipArray: addedProjectsChipArray});
            }
        } else {
            deletedProjectsChipArray.push(chipId);
            this.setState({deletedProjectsChipArray: deletedProjectsChipArray});
        }
    }

    handleDeleteProjectFromList(value, index) {
        let projectsInputVal = this.state.projectsInputVal;
        let organizationFields = this.state.organizationFields;
        projectsInputVal.splice(index, 1);

        organizationFields.selectedProjects = organizationFields.selectedProjects.filter(function(obj) {
            return obj.id !== value;
        });
        this.chipArrCheckWhenDeleted(value);
        this.setState({projectsInputVal: projectsInputVal, organizationFields: organizationFields});
    }

    handleAddAllowedUsersField(input) {
        let organizationFields = this.state.organizationFields;
        let usersInputVal = this.state.usersInputVal;
        usersInputVal.push(input);
        organizationFields.allowedUsers = usersInputVal;
        this.setState({
            organizationFields: organizationFields, usersInputVal: usersInputVal,
            currUserChipInput: input,
            updateDialogText: 'Do you wish to add the user to all related projects and companies?'
        });
        this.handleOpenUpdateAllDialog(this);
    }

    handleDeleteAllowedUsersField(value, index) {
        let organizationFields = this.state.organizationFields;
        let usersInputVal = this.state.usersInputVal;
        usersInputVal.splice(index, 1);

        organizationFields.allowedUsers = organizationFields.allowedUsers.filter(function(obj) {
            return obj !== value;
        });

        this.setState({
            usersInputVal: usersInputVal, organizationFields: organizationFields,
            currUserChipInput: value,
            updateDialogText: 'Do you wish to remove the user from all related projects and companies?'
        });
        this.handleOpenUpdateAllDialog(this);
    }

    cancel() {
        this.setState({
            addedProjectsChipArray: [],
            deletedProjectsChipArray: [],
            openAddNewOrganizationModal: false
        });
        this.props.clearAllModalFields();
    }

    handleChangeSendAutoSurveyCheckBox(e) {
        if (e && e.target && e.target.checked !== this.state.isSendAutoSurveyFromApi) {
            this.setState({isSendAutoSurveyFromApi: e.target.checked});
        }
    }

    handleChangeShowScanDate(e) {
        if (e && e.target && e.target.checked !== this.state.showScanDate) {
            this.setState({showScanDate: e.target.checked});
        }
    }

    handleChangeAuditorEmail(e) {
        if (e && e.target && e.target.hasOwnProperty('value')) {
            this.setState({apiAuditorEmail: e.target.value});
        }
    }

    handleAddRequestedTemplateField(chip) {
        let allData = this.state.surveyTemplatesForDD;
        let inputData = this.state.apiSelectedTemplate;

        if (inputData.length > 0) {
            app.addAlert('warning', 'Only one template is required.');
        } else {
            let input;

            if (!Array.isArray(chip)) {
                input = chip;
            } else {
                input = chip[0];
            }

            // Check if allData contains the input.
            const inputIndex = allData.indexOf(input);

            if (inputIndex > -1) {
                const inputObjToAdd = allData[inputIndex];

                // Add to the ChipSelect.
                inputData.push(inputObjToAdd);

                this.setState({apiSelectedTemplate: inputData});
            } else {
                app.addAlert('error', 'No such template.');
            }
        }
    }

    handleDeleteRequestedTemplateField(value) {
        let inputData = this.state.apiSelectedTemplate;

        let index = -1;

        if (inputData && Array.isArray(inputData)) {
            // Check the index of the value of selectedChip in inputData.
            inputData.map((currData, currIndex) => {
                if (currData && currData.sid && currData.sid === value) {
                    index = currIndex;
                }
            });
        }

        if (index > -1) {
            // Update the ChipSelect.
            inputData.splice(index, 1);

            this.setState({apiSelectedTemplate: inputData});
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            openAddNewOrganizationModal: nextProps.openAddNewOrganizationModal,
            usersInputVal: nextProps.organizationFields.allowedUsers,
            projectsInputVal: nextProps.organizationFields.selectedProjects,
            usersForDD: nextProps.allUsers,
            projects: nextProps.projects,
            apiAuditorEmail: nextProps.organizationFields.apiAuditorEmail || '',
            apiSelectedTemplate: nextProps.organizationFields.apiSelectedTemplate || [],
            isSendAutoSurveyFromApi: nextProps.organizationFields.isSendAutoSurveyFromApi || false,
            showScanDate: nextProps.organizationFields.showScanDate || false,
            surveyTemplatesForDD: nextProps.surveyTemplatesForDD || []
        });
    }

    render() {
        let projectsForDD = this.state.projects.map((item) => {
            return {name: item.projectName, id: item.id};
        });

        const autoCompleteStyle = {overflowY: 'auto', height: '200px', maxHeight: '200px'};

        return (
            <Dialog
                title="Add New Organization"
                actions={[
                    <FlatButton
                        label="Done"
                        primary={true}
                        style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                        keyboardFocused={true}
                        backgroundColor={'#0091ea'}
                        hoverColor={'#12a4ff'}
                        rippleColor={'white'}
                        labelStyle={{fontSize: 10}}
                        onTouchTap={() => {
                            if (this.state.organizationFields && !this.state.organizationFields.organizationName) {
                                app.addAlert('error', 'Error: organization Name must be assigned.');
                            } else if (this.state.isSendAutoSurveyFromApi && (!this.state.apiAuditorEmail || this.state.apiSelectedTemplate.length === 0)) {
                                app.addAlert('error', 'Error: API fields must be assigned.');
                            } else {
                                this.state.organizationFields.isSendAutoSurveyFromApi = this.state.isSendAutoSurveyFromApi;

                                if (this.state.isSendAutoSurveyFromApi) {
                                    this.state.organizationFields.apiAuditorEmail = this.state.apiAuditorEmail;
                                    this.state.organizationFields.apiSelectedTemplate = this.state.apiSelectedTemplate;
                                }

                                this.state.organizationFields.showScanDate = this.state.showScanDate;

                                this.props.addOrganization(
                                    this.state.organizationFields,
                                    this.state.addedProjectsChipArray,
                                    this.state.deletedProjectsChipArray);
                                this.setState({
                                    addedProjectsChipArray: [],
                                    deletedProjectsChipArray: []
                                });
                            }
                        }}
                    />,
                    <FlatButton
                        label="Cancel"
                        primary={true}
                        backgroundColor={'#0091ea'}
                        style={{color: 'white', marginBottom: '5px', height: 40}}
                        keyboardFocused={true}
                        hoverColor={'#12a4ff'}
                        rippleColor={'white'}
                        labelStyle={{fontSize: 10}}
                        onTouchTap={this.cancel.bind(this)}
                    />
                ]}
                actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                modal={true}
                contentStyle={{borderRadius: '7px 7px 7px 7px'}}
                titleStyle={{
                    fontSize: 18,
                    background: 'rgba(0,0,0,0.7)',
                    color: 'white', textAlign: 'center',
                    borderRadius: '2px 2px 0px 0px',
                    textTransform: 'uppercase'
                }}
                autoScrollBodyContent={true}
                open={this.state.openAddNewOrganizationModal}
                onRequestClose={this.cancel.bind(this)}
            >
                <form>
                    <br/>
                    <TextField
                        floatingLabelText={'Organization Name'}
                        hintText="Organization Name"
                        onChange={this.handleChangeOrganizationField.bind(this)}
                        value={this.state.organizationFields.organizationName}
                    />
                    <br/>
                    <br/>
                    <ChipInput
                        hintText="Projects to include in Organization"
                        floatingLabelText="Projects to include in Organization"
                        value={this.state.projectsInputVal}
                        newChipKeyCodes={[]}
                        filter={AutoComplete.fuzzyFilter}
                        onRequestAdd={(chip) => this.handleAddProjectToList(chip)}
                        onRequestDelete={(chip, index) => this.handleDeleteProjectFromList(chip, index)}
                        dataSource={projectsForDD}
                        dataSourceConfig={{'text': 'name', 'value': 'id'}}
                        maxSearchResults={5}
                        openOnFocus={true}
                        style={{width: 600}}
                    />
                    <br/>
                    <br/>
                    <ChipInput
                        hintText="Allowed users (emails)"
                        floatingLabelText="Allowed users (emails)"
                        value={this.state.usersInputVal}
                        filter={AutoComplete.fuzzyFilter}
                        dataSource={this.state.usersForDD}
                        onRequestAdd={(chip) => this.handleAddAllowedUsersField(chip)}
                        dataSourceConfig={{'text': 'email', 'value': 'id'}}
                        onRequestDelete={(chip, index) => this.handleDeleteAllowedUsersField(chip, index)}
                        maxSearchResults={5}
                    />
                    <br/>
                    <br/>
                    <span>Show Scan Dates</span>
                    <br/>
                    <Checkbox
                        label="Show Scan Dates in data tables"
                        checked={this.state.showScanDate}
                        onCheck={this.handleChangeShowScanDate.bind(this)}
                    />
                    <br/>
                    <br/>
                    <span>API Fields</span>
                    <br/>
                    <Checkbox
                        label="Send auto survey through API for very sensitive companies"
                        checked={this.state.isSendAutoSurveyFromApi}
                        onCheck={this.handleChangeSendAutoSurveyCheckBox.bind(this)}
                    />
                    {(this.state.isSendAutoSurveyFromApi) ? (() => {
                        return <div>
                            <br/>
                            <ValidatorForm>
                                <TextValidator
                                    name="auditorEmail"
                                    floatingLabelText="Auditor Email"
                                    hintText="Enter Auditor Email"
                                    onChange={this.handleChangeAuditorEmail.bind(this)}
                                    value={this.state.apiAuditorEmail}
                                    validators={['required', 'isEmail']}
                                    errorMessages={['this field is required', 'email is not valid']}
                                />
                            </ValidatorForm>
                            <br/>
                            <ChipInput
                                hintText="Search Template"
                                floatingLabelText="Survey Template"
                                menuStyle={autoCompleteStyle}
                                filter={AutoComplete.caseInsensitiveFilter}
                                value={this.state.apiSelectedTemplate}
                                dataSource={this.state.surveyTemplatesForDD}
                                dataSourceConfig={{'text': 'name', 'value': 'sid'}}
                                onRequestAdd={(chip) => this.handleAddRequestedTemplateField(chip)}
                                onRequestDelete={(chip) => this.handleDeleteRequestedTemplateField(chip)}
                                maxSearchResults={50}
                                openOnFocus={true}
                            />
                        </div>;
                    })() : null}
                    <br/>
                </form>
                <UpdateAllDialog
                    updateDialogText={this.state.updateDialogText}
                    open={this.state.openUpdateAllDialog}
                    onRequestClose={this.handleCloseUpdateAllDialog}
                    removeFromAll={() => {
                        this.handleRemoveFromAll();
                        this.handleCloseUpdateAllDialog();
                    }}

                    addToAll={() => {
                        this.handleAddToAll();
                        this.handleCloseUpdateAllDialog();
                    }}/>
            </Dialog>
        );
    }
}
