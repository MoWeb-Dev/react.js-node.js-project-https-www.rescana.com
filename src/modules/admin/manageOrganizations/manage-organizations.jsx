import React from 'react';
import {Card} from 'material-ui/Card';
import AddOrganizationModal from './add-organization-modal.jsx';
import OrganizationsTable from './organizations-table.jsx';
import Loader from 'react-loader-advanced';
import classNames from "classnames";

class ManageOrganizationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openAddNewOrganizationModal: false,
            companies: this.props.companies,
            surveyTemplatesForDD: this.props.surveyTemplatesForDD,
            projectsInputVal: this.props.projects,
            organizations: this.props.organizations,
            totalOrganizationsCount: this.props.totalOrganizationsCount,
            users: this.props.users,
            organizationFields: {
                organizationName: '',
                allowedUsers: [],
                selectedProjects: [],
                createDate: '',
                apiAuditorEmail: '',
                apiSelectedTemplate: [],
                isSendAutoSurveyFromApi: false,
                showScanDate : false
            }
        };

        this.handleOpenAddNewOrganizationModal = this.handleOpenAddNewOrganizationModal.bind(this);
    }

    handleOpenAddNewOrganizationModal() {
        this.clearAllModalFields();
        this.setState({openAddNewOrganizationModal: true});

        let user = app.getAuthUser();
        let organizationFields = this.state.organizationFields;
        if (user && user.admin) {
            organizationFields.allowedUsers.push(user);
            this.setState({organizationFields: organizationFields});
        }
    }

    clearAllModalFields() {
        let organizationFields = this.state.organizationFields;
        organizationFields.organizationName = '';
        organizationFields.allowedUsers = [];
        organizationFields.selectedProjects = [];
        organizationFields.createDate = '';
        organizationFields._id = '';
        organizationFields.apiAuditorEmail = '';
        organizationFields.apiSelectedTemplate = [];
        organizationFields.isSendAutoSurveyFromApi = false;
        organizationFields.showScanDate = false;

        this.setState({
            openAddNewOrganizationModal: false,
            organizationFields: organizationFields
        });
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }
    }

    editOrganization(organizationId, e) {
        e.preventDefault();
        let organizationFields = this.state.organizationFields;
        this.setState({openAddNewOrganizationModal: true});

        function matchesEl(el) {
            return el._id === organizationId;
        }

        let organization = this.state.organizations.filter((organizationId) => {
            return matchesEl(organizationId);
        });

        organizationFields.organizationName = organization[0].organizationName;
        organizationFields.selectedProjects = organization[0].selectedProjects;
        organizationFields.allowedUsers = organization[0].allowedUsers;
        organizationFields._id = organization[0]._id;
        organizationFields.apiAuditorEmail = organization[0].apiAuditorEmail || '';
        organizationFields.apiSelectedTemplate = organization[0].apiSelectedTemplate || [];
        organizationFields.isSendAutoSurveyFromApi = organization[0].isSendAutoSurveyFromApi || false;
        organizationFields.showScanDate = organization[0].showScanDate || false;

        this.setState({
            organizationFields: organizationFields
        });
    }

    addOrganization(organizationFields, addedProjectsChipArray, deletedProjectsChipArray) {
        this.setState({openAddNewOrganizationModal: false});
        this.props.addOrganization(organizationFields, addedProjectsChipArray, deletedProjectsChipArray);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            users: nextProps.users,
            organizations: nextProps.organizations,
            totalOrganizationsCount: nextProps.totalOrganizationsCount,
            projects: nextProps.projects,
            companies: nextProps.companies,
            surveyTemplatesForDD: nextProps.surveyTemplatesForDD
        });
    }

    render() {
        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        return (
            <div style={{backgroundColor: 'grey200'}}>
                <div style={{margin: '30px 30px 0px 30px'}}>
                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                    <span style={{position: 'relative', top: '-6px', fontSize: 18}}>Organizations</span>
                </div>
                <Card style={{margin: 30}}>
                    <Loader show={this.props.showLoaderForOrganizations} message={spinner}>
                        <div>
                            <OrganizationsTable
                                organizations={this.state.organizations}
                                totalOrganizationsCount={this.state.totalOrganizationsCount}
                                deleteOrganization={this.props.deleteOrganization}
                                editOrganization={this.editOrganization.bind(this)}
                                handleChangeDataViewClick={this.props.handleChangeDataViewClick}
                            />
                        </div>
                    </Loader>
                </Card>
                <AddOrganizationModal
                    addOrganization={this.addOrganization.bind(this)}
                    cancel={this.cancel}
                    organizationFields={this.state.organizationFields}
                    projects={this.state.projects}
                    companies={this.state.companies}
                    surveyTemplatesForDD={this.state.surveyTemplatesForDD}
                    clearAllModalFields={this.clearAllModalFields.bind(this)}
                    allUsers={this.state.users}
                    openAddNewOrganizationModal={this.state.openAddNewOrganizationModal}
                />
            </div>
        );
    }
}

export default ManageOrganizationForm;
