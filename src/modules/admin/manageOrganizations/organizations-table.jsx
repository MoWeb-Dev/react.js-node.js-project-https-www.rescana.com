import React from 'react';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import IconButton from 'material-ui/IconButton';
import Delete from 'material-ui/svg-icons/action/delete';
import Chip from 'material-ui/Chip';
import {showExpandedDataChip} from '../../common/UI-Helpers.jsx';
import DeleteDialog from "../../app/delete-dialog.jsx";
import DataTableViewTemplate from '../../common/datatableTemplate/datatable-template.jsx';
import DefaultIcon from 'material-ui/svg-icons/navigation/menu';
import ArrowUpIcon from 'material-ui/svg-icons/navigation/arrow-upward';
import ArrowDownIcon from 'material-ui/svg-icons/navigation/arrow-downward';

export default class OrganizationsTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            organizations: this.props.organizations || [],
            totalOrganizationsCount: this.props.totalOrganizationsCount || 0,
            openDeleteDialog: false,
            curRowIdToDelete: null
        };

        this.handleOpenDeleteDialog = this.handleOpenDeleteDialog.bind(this);
        this.handleCloseDeleteDialog = this.handleCloseDeleteDialog.bind(this);
    }


    handleOpenDeleteDialog(rowToDelete) {
        this.setState({openDeleteDialog: true, curRowIdToDelete: rowToDelete});
    };

    handleCloseDeleteDialog() {
        this.setState({openDeleteDialog: false, curRowIdToDelete: null});
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            organizations: nextProps.organizations,
            totalOrganizationsCount: nextProps.totalOrganizationsCount
        });
    }

    render() {
        const styles = {
            chip: {
                margin: 4
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap'
            },
            tableStyle: {
                paddingTop: '8px',
                paddingBottom: '8px'

            },
            wrappableStyle: {
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-word'
            },
            menuItem: {
                color: '#ffffff',
                fontSize: '13px'
            },
            menuItemIcon: {
                fill: '#ffffff'
            }
        };

        const tableColumns = [
            {
                label: 'ID',
                key: 'id'
            },
            {
                label: 'Organization Name',
                key: 'organizationName',
                style: styles.wrappableStyle
            },
            {
                label: 'Create Date',
                key: 'createDate',
                style: styles.wrappableStyle
            },
            {
                label: 'Projects',
                key: 'selectedProjects',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                label: 'Allowed Users',
                key: 'allowedUsers',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                label: '',
                key: 'editDelete',
                noneSearchable: true
            }
        ];

        const tableData = [];
        this.state.organizations.map((currOrg) => {
            tableData.push({
                id: currOrg._id,
                organizationName: currOrg.organizationName,
                createDate: currOrg.createDate,
                selectedProjects: currOrg && currOrg.selectedProjects && currOrg.selectedProjects.map((project) => (
                    <Chip key={project.id} style={styles.chip}>{project.name}</Chip>)),
                allowedUsers: showExpandedDataChip(currOrg.allowedUsers && currOrg.allowedUsers.map((user) => {
                    return (user.email);
                }), 4, 5),
                editDelete: <div>
                    <IconButton
                        onClick={(e) => this.props.editOrganization(currOrg._id, e)} name="editOrganization">
                        <Edit/>
                    </IconButton>
                    <IconButton
                        name="deleteOrganization"
                        style={{cursor: 'pointer'}}
                        onTouchTap={this.handleOpenDeleteDialog.bind(this, currOrg._id)}>
                        <Delete/>
                    </IconButton>
                </div>
            });
        });

        const sortOptions = [
            {
                primaryText: 'Default',
                value: 'default',
                icon: <DefaultIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'organization Name ASC',
                value: 'organizationNameAsc',
                icon: <ArrowUpIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'organization Name DESC',
                value: 'organizationNameDesc',
                icon: <ArrowDownIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Create Date ASC',
                value: 'createDateAsc',
                icon: <ArrowUpIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Create Date DESC',
                value: 'createDateDesc',
                icon: <ArrowDownIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            }
        ];

        return (
            <div>
                <DataTableViewTemplate
                    myDataType={'adminOrganizations'}
                    tableColumns={tableColumns}
                    tableData={tableData}
                    totalRowCount={this.state.totalOrganizationsCount}
                    hideDownloadExcel={true}
                    hideCardBackground={true}
                    hideHeaderToolbar={true}
                    searchHintText='Enter organization name'
                    sortOptions={sortOptions}
                    handleChangeDataViewClick={this.props.handleChangeDataViewClick}
                />
                <DeleteDialog
                    open={this.state.openDeleteDialog}
                    onRequestClose={this.handleCloseDeleteDialog}
                    onDelete={(e) => {
                        this.props.deleteOrganization(this.state.curRowIdToDelete, e);
                        this.handleCloseDeleteDialog();
                    }}/>
            </div>
        );
    }
}
