import React from 'react';
import {Card} from 'material-ui/Card';
import AddCompanyModal from './add-company-modal.jsx';
import CompaniesTable from './company-table.jsx';
import {DEFAULT_INTEL_SCORE_RATIOS} from '../../../../config/consts.js';
import ManageDiscoveredDomainsModal from './manage-discovered-domains.jsx';
import {deleteIndexInArray} from '../../common/CommonHelpers';
import Loader from 'react-loader-advanced';
import classNames from 'classnames';
import FlatButton from 'material-ui/FlatButton';

class ManageCompanyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openAddNewCompanyModal: false,
            openManageDiscoveredDomainsModal: false,
            companies: this.props.companies,
            totalCompaniesCount: this.props.totalCompaniesCount,
            users: this.props.users,
            companyFields: {
                companyName: '',
                allowedUsers: [],
                allowedEditUsers: [], // Users that are allowed to change company details.
                selectedDomains: [],
                ratios: {
                    surveyWeight: 0,
                    intelWeight: 100
                },
                intelScoreRatios: DEFAULT_INTEL_SCORE_RATIOS,
                sensitivity: '',
                keywords: [],
                IPList: [],
                createDate: '',
                companyInfo: {}
            },
            isDisabledNeo4jBtn: false
        };
        // TODO: add ',projectsNames: []' to state.

        this.handleOpenAddNewCompanyModal = this.handleOpenAddNewCompanyModal.bind(this);
        this.closeManageDiscoveredDomains = this.closeManageDiscoveredDomains.bind(this);
    }

    handleOpenAddNewCompanyModal() {
        this.clearAllModalFields();
        this.setState({openAddNewCompanyModal: true});

        let user = app.getAuthUser();
        let companyFields = this.state.companyFields;
        if (user && user.admin) {
            companyFields.allowedUsers.push(user);
            companyFields.allowedEditUsers.push(user);
            this.setState({companyFields: companyFields});
        }
    }

    clearAllModalFields() {
        let companyFields = this.state.companyFields;
        companyFields.companyName = '';
        companyFields.allowedUsers = [];
        companyFields.allowedEditUsers = [];
        companyFields.selectedDomains = [];
        companyFields.keywords = [];
        companyFields.IPList = [];
        companyFields.ratios = {
            surveyWeight: 0,
            intelWeight: 100
        };
        companyFields.intelScoreRatios = DEFAULT_INTEL_SCORE_RATIOS;
        companyFields.sensitivity = '';
        companyFields.createDate = '';
        companyFields.id = '';
        companyFields.companyInfo = {};

        this.setState({
            openAddNewCompanyModal: false,
            companyFields: companyFields
        });
    }

    addAllCompToNeo4j() {
        $.ajax({
            type: 'GET',
            url: '/admin/addAllCompToNeo4j',
            async: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok) {
                app.addAlert('success', 'success.');
                this.setState({isDisabledNeo4jBtn: true});
            }
        }).fail((e) => {
            app.addAlert('error', 'Error: failed to load get companies.');
        });
    }

    editCompany(companyId, e) {
        e.preventDefault();
        let companyFields = this.state.companyFields;
        this.setState({openAddNewCompanyModal: true});

        function matchesEl(el) {
            return el.id === companyId;
        }

        let company = this.state.companies.filter((companyId) => {
            return matchesEl(companyId);
        });

        companyFields.companyName = company[0].companyName;
        companyFields.allowedUsers = company[0].allowedUsers;
        companyFields.allowedEditUsers = company[0].allowedEditUsers;
        companyFields.selectedDomains = company[0].selectedDomains;
        companyFields.IPList = company[0].IPList;
        companyFields.sensitivity = company[0].sensitivity;
        companyFields.ratios = company[0].ratios;
        companyFields.intelScoreRatios = company[0].intelScoreRatios;
        companyFields.keywords = company[0].keywords;
        companyFields.id = company[0].id;
        companyFields.companyInfo = company[0].extraData || {};

        this.setState({
            companyFields: companyFields
        });
    }

    openManageDiscoveredDomains(company, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        let discoveredDomain = [];

        $.ajax({
            type: 'GET',
            url: '/admin/getDiscoveredDomains',
            contentType: 'application/json; charset=utf-8',
            data: {cid: company.id},
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                discoveredDomain = res.discoveredDomains;
                this.setState({
                    openManageDiscoveredDomainsModal: true,
                    manageDiscovered_company: company,
                    manageDiscovered_Domain: discoveredDomain
                });
            }
        });
    }

    validateDomain(domain, strength, currIndex){
        $.ajax({
            type: 'POST',
            url: '/admin/validateDomain',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({domain: domain, strength: strength}),
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'Validated Domain!');
                if(strength === 100){
                    if(this.state.manageDiscovered_company.selectedDomains.indexOf(domain) === -1){
                        this.state.manageDiscovered_company.selectedDomains.push(domain);
                    }
                    this.state.manageDiscovered_Domain[currIndex].connectionStrength = strength;
                    this.state.manageDiscovered_Domain[currIndex].validated = true;
                    this.forceUpdate();
                }else if(strength === 0){
                    if(this.state.manageDiscovered_company.selectedDomains.indexOf(domain) > -1){
                        const indexOfSelectedDomain = this.state.manageDiscovered_company.selectedDomains.indexOf(domain);
                        if (indexOfSelectedDomain > -1) {
                            deleteIndexInArray(this.state.manageDiscovered_company.selectedDomains, indexOfSelectedDomain);
                        }
                    }
                    this.state.manageDiscovered_Domain[currIndex].connectionStrength = strength;
                    this.state.manageDiscovered_Domain[currIndex].validated = false;
                    // Force a rerender to show results.
                    this.forceUpdate();
                }
            }else{
                app.addAlert('error', 'Validation Failed!');
            }
        });
    }

    closeManageDiscoveredDomains(e) {
        if (e && e.preventDefault) {
            e.preventDefault();
        }
        let stateObjToUpdate = {
            openManageDiscoveredDomainsModal: false,
            manageDiscovered_company: undefined,
            manageDiscovered_Domain: undefined
        };

        this.setState(stateObjToUpdate);
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }
    }

    addCompany(companyFields, deletedChipArray, addedChipArray, deletedIPChipArray, addedIPChipArray) {
        this.setState({openAddNewCompanyModal: false});
        this.props.addCompany(companyFields, deletedChipArray, addedChipArray, deletedIPChipArray, addedIPChipArray);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            users: nextProps.users,
            companies: nextProps.companies,
            totalCompaniesCount: nextProps.totalCompaniesCount
        });
    }

    render() {
        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        return (
            <div style={{backgroundColor: 'grey200'}}>
                <div style={{margin: '30px 30px 0px 30px'}}>
                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                    <span style={{position: 'relative', top: '-6px', fontSize: 18}}>Companies</span>
                </div>
                <Card style={{margin: 30}}>
                    <FlatButton
                        key={1}
                        label="Add All Companies To Neo4j"
                        onTouchTap={this.addAllCompToNeo4j.bind(this)}
                        disabled={this.state.isDisabledNeo4jBtn}
                        style={{backgroundColor: 'red', color: 'white', float: 'right'}}
                    />
                    <Loader show={this.props.showLoaderForCompanies} message={spinner}>
                        <div>
                            <CompaniesTable
                                companies={this.state.companies}
                                totalCompaniesCount={this.state.totalCompaniesCount}
                                deleteCompany={this.props.deleteCompany}
                                editCompany={this.editCompany.bind(this)}
                                openManageDiscoveredDomains={this.openManageDiscoveredDomains.bind(this)}
                                runCompanyQuery={this.props.runCompanyQuery}
                                runDiscoveryQuery={this.props.runDiscoveryQuery}
                                handleChangeDataViewClick={this.props.handleChangeDataViewClick}
                            />
                        </div>
                    </Loader>
                </Card>
                <AddCompanyModal
                    addCompany={this.addCompany.bind(this)}
                    cancel={this.cancel}
                    companyFields={this.state.companyFields}
                    companies={this.state.companies}
                    projects={this.props.projects}
                    clearAllModalFields={this.clearAllModalFields.bind(this)}
                    allUsers={this.state.users}
                    openAddNewCompanyModal={this.state.openAddNewCompanyModal}
                />
                <ManageDiscoveredDomainsModal
                    openManageDiscoveredDomainsModal={this.state.openManageDiscoveredDomainsModal}
                    cancel={this.closeManageDiscoveredDomains}
                    manageCompany={this.state.manageDiscovered_company}
                    validateDomain={this.validateDomain.bind(this)}
                    manageDomain={this.state.manageDiscovered_Domain}
                />
            </div>
        );
    }
}

export default ManageCompanyForm;
