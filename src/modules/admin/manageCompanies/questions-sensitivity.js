module.exports.questionsArr = [
    { //Impact
        question: "How much data could be disclosed and how sensitive is it?",
        answers: ['Minimal "internal" data disclosed', 'Minimal "internal" data disclosed, minimal "sensitive" data disclosed', 'Minimal "sensitive" data disclosed, extensive "internal" data disclosed', 'Extensive "sensitive" data disclosed', 'All data disclosed']
    },
    {
        question: "How much data could be corrupted and how damaged is it?",
        answers: ['Minimal slightly corrupt non critical data', 'Minimal slightly corrupt critical data', 'Minimal seriously corrupt critical data', 'Extensive seriously corrupt critical data', 'All data totally corrupt']
    },
    {
        question: "How much service could be lost and how critical is it?",
        answers: ['Minimal support process interrupted', 'Minimal primary process interrupted, minimal support process interrupted', 'Minimal primary process interrupted, extensive support process interrupted', 'Extensive primary process interrupted', 'All process completely lost']
    },
    {//Probability
        question: "What is the type of the engagement with the vendor?",
        answers: ['Continuous. Vendor only', 'One time. Vendor only', 'Continuous. Vendor and his sub contractors and his 3rd parties (4th party to the organization)', 'One time. Vendor and his sub contractors and his 3rd parties (4th party to the organization)']
    },
    {
        question: "Does the organization share data with the vendor?",
        answers: ['No', 'Yes. Physical', 'Yes. Electronic', 'Yes. Physical and Electronic']
    },
    {
        question: "What is the Data Classification of the information shared with the vendor?",
        answers: ['Non Sensitive - Public', 'Non Sensitive - Internal', 'Sensitive - Confidencial', 'Sensitive - Secret']
    },
    {
        question: "Does the vendor stores\\process\\transmits the organization's data?",
        answers: ['No', 'Stores only', 'Process, and transmits without storing', 'Stores, process and transmits']
    },
    {
        question: "What is the nature of the interfaces with the vendor?",
        answers: ['Intra-organizational interfaces.', 'External secured interfaces: encrypted communication and secured file transfer.', 'External partially secured interfaces: encrypted communication and non- secured file transfer.', 'External non secured interfaces']
    },
    {
        question: "How does the vendor support the service?",
        answers: ['Remotely with secured and monitored connection to the supported resource in the organization\'s network Physicaly (on-site)', 'Physicaly (accompanied on site)', 'Remotely with no connection to the organization\'s network (e.g. Cloud service etc.)', 'Remotely using commercial takeover software']
    }
];
