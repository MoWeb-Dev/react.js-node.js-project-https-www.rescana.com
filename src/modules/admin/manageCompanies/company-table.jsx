import React from 'react';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import DomainsIcon from 'material-ui/svg-icons/av/web';
import Play from 'material-ui/svg-icons/av/play-arrow';
import IconButton from 'material-ui/IconButton';
import {showExpandedDataChip} from '../../common/UI-Helpers.jsx';
import Delete from 'material-ui/svg-icons/action/delete';
import DiscoveryIcon from 'material-ui/svg-icons/device/wifi-tethering';
import Chip from 'material-ui/Chip';
import DeleteDialog from '../../app/delete-dialog.jsx';
import DataTableViewTemplate from '../../common/datatableTemplate/datatable-template.jsx';
import DefaultIcon from 'material-ui/svg-icons/navigation/menu';
import ArrowUpIcon from 'material-ui/svg-icons/navigation/arrow-upward';
import ArrowDownIcon from 'material-ui/svg-icons/navigation/arrow-downward';


export default class companyTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            companies: this.props.companies || [],
            totalCompaniesCount: this.props.totalCompaniesCount || 0,
            openDeleteDialog: false,
            curRowIdToDelete: null,
            curSelectedDomains: []
        };

        this.handleOpenDeleteDialog = this.handleOpenDeleteDialog.bind(this);
        this.handleCloseDeleteDialog = this.handleCloseDeleteDialog.bind(this);
    }

    handleOpenDeleteDialog(rowToDelete, curSelectedDomains) {
        this.setState({openDeleteDialog: true, curRowIdToDelete: rowToDelete, curSelectedDomains: curSelectedDomains});
    };

    handleCloseDeleteDialog() {
        this.setState({openDeleteDialog: false, curRowIdToDelete: null});
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            companies: nextProps.companies,
            totalCompaniesCount: nextProps.totalCompaniesCount
        });
    }

    renderSensitivity(sensitivity) {
        if (sensitivity) {
            let sensStr = '';
            let color = '';

            if (sensitivity === 1) {
                sensStr = 'Low Sensitivity';
                color = '#0091ea';
            } else if (sensitivity === 2) {
                sensStr = 'Moderate Sensitivity';
                color = '#ffa000';
            } else if (sensitivity === 3) {
                sensStr = 'High Sensitivity';
                color = '#f17200';
            } else if (sensitivity === 4) {
                sensStr = 'Very High Sensitivity';
                color = '#D92E2E';
            }
            return <Chip backgroundColor={color} labelColor={'#FFFFFF'}>{sensStr}</Chip>;
        }
    }

    render() {
        const styles = {
            tableOldStyle: {
                paddingTop: '8px',
                paddingBottom: '8px'

            },
            tableStyleWrapped: {
                width: '60px',
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-word'
            },
            tableBodyStyle: {
                'overflowX': 'auto'
            },
            tableStyle: {
                'width': '95%',
                'margin': 'auto'
            },
            wrappableStyle: {
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-word'
            },
            menuItem: {
                color: '#ffffff',
                fontSize: '13px'
            },
            menuItemIcon: {
                fill: '#ffffff'
            }
        };

        const tableColumns = [
            {
                label: 'Company Name',
                key: 'companyName'
            },
            {
                label: 'Create Date',
                key: 'createDate',
                style: styles.wrappableStyle
            },
            {
                label: 'Domains',
                key: 'selectedDomains',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                label: 'Dataleaks Keywords',
                key: 'keywords',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                label: 'Allowed Users',
                key: 'allowedUsers',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                label: 'Sensitivity',
                key: 'sensitivity',
                style: styles.wrappableStyle
            },
            {
                label: 'Latest Scan',
                key: 'lastScanDate',
                style: styles.wrappableStyle
            },
            {
                label: 'Scan Progress',
                key: 'percentageDone',
                style: styles.wrappableStyle
            },
            {
                label: 'Discovered Domains',
                key: 'discoveredDomainsData',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                label: '',
                key: 'runEditDelete',
                noneSearchable: true
            }
        ];

        const tableData = [];
        this.state.companies.map((currComp) => {
            tableData.push({
                companyName: currComp.companyName,
                createDate: currComp.createDate,
                selectedDomains: showExpandedDataChip(currComp.selectedDomains, 4, 5, 'domains'),
                keywords: showExpandedDataChip(currComp.keywords, 4, 5),
                allowedUsers: showExpandedDataChip(currComp.allowedUsers
                    && currComp.allowedUsers.map((user) => {
                        return (user.email);
                    }), 4, 5),
                sensitivity: this.renderSensitivity(currComp.sensitivity),
                lastScanDate: currComp.lastScanDate,
                percentageDone: (currComp.percentageDone || 0) + '%',
                discoveredDomainsData:
                    <IconButton
                        onClick={(e) => this.props.openManageDiscoveredDomains(currComp, e)}
                        name="manageDiscoveredDomains">
                        <DomainsIcon/>
                    </IconButton>,
                runEditDelete: <div>
                    <IconButton style={{marginLeft: '-30px'}}
                                onClick={(e) => this.props.runDiscoveryQuery(currComp.id, e)}
                                name="runDiscovery">
                        <DiscoveryIcon/>
                    </IconButton>
                    <IconButton style={{marginLeft: '-20px'}}
                                onClick={(e) => this.props.runCompanyQuery(currComp.id, e)}
                                name="runCompanyQuery">
                        <Play/>
                    </IconButton>
                    <IconButton
                        onClick={(e) => this.props.editCompany(currComp.id, e)} name="editCompany">
                        <Edit/>
                    </IconButton>
                    <IconButton
                        name="deleteCompany"
                        style={{cursor: 'pointer'}}
                        onTouchTap={this.handleOpenDeleteDialog.bind(this, currComp.id, currComp.selectedDomains)}>
                        <Delete/>
                    </IconButton>
                </div>
            });
        });

        const sortOptions = [
            {
                primaryText: 'Default',
                value: 'default',
                icon: <DefaultIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Company Name ASC',
                value: 'companyNameAsc',
                icon: <ArrowUpIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Company Name DESC',
                value: 'companyNameDesc',
                icon: <ArrowDownIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Create Date ASC',
                value: 'createDateAsc',
                icon: <ArrowUpIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Create Date DESC',
                value: 'createDateDesc',
                icon: <ArrowDownIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Latest Scan ASC',
                value: 'latestScanAsc',
                icon: <ArrowUpIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Latest Scan DESC',
                value: 'latestScanDesc',
                icon: <ArrowDownIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            }
        ];

        return (
            <div>
                <DataTableViewTemplate
                    myDataType={'adminCompanies'}
                    tableColumns={tableColumns}
                    tableData={tableData}
                    totalRowCount={this.state.totalCompaniesCount}
                    hideDownloadExcel={true}
                    hideCardBackground={true}
                    hideHeaderToolbar={true}
                    searchHintText='Enter company name'
                    sortOptions={sortOptions}
                    handleChangeDataViewClick={this.props.handleChangeDataViewClick}
                />
                <DeleteDialog
                    open={this.state.openDeleteDialog}
                    onRequestClose={this.handleCloseDeleteDialog}
                    onDelete={(e) => {
                        this.props.deleteCompany(this.state.curRowIdToDelete, this.state.curSelectedDomains, e);
                        this.handleCloseDeleteDialog();
                    }}/>
            </div>);
    }
}
