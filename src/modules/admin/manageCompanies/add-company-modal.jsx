import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import React from 'react';
import {
    Step,
    Stepper,
    StepButton,
    StepContent
} from 'material-ui/Stepper';
import ReactTooltip from 'react-tooltip';
import {
    DEFAULT_INTEL_SCORE_RATIOS, COMPANY_CONTACT_TYPES, EMPTY_CONTACT, EMPTY_DATE_RANGE_FIELD, YES_NO_MODAL_TYPES,
    COMPANY_INFO_CLASSIFICATION_TYPES, COMPANY_INFO_SECTOR_TYPES
} from '../../../../config/consts.js';
import EditContactsModal from '../../common/companyEdit/edit-contacts-modal.jsx';
import EditInformationModal from '../../common/companyEdit/edit-information-modal.jsx';
import YesNoModal from '../../common/companyEdit/yes-no-modal.jsx';
import Checkbox from 'material-ui/Checkbox';
import ActionAdd from 'material-ui/svg-icons/content/add-circle';
import EditIcon from 'material-ui/svg-icons/image/edit';
import QuestionnaireIcon from 'material-ui/svg-icons/action/chrome-reader-mode.js';
import DatePicker from 'material-ui/DatePicker';
import DatePickerMain from '../../common/datePicker/date-picker-main.jsx';
import {fixTimezoneRange} from "../../common/CommonHelpers";
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import themeForFont from "../../app/themes/themeForFont";
import EditProjectModal from "../../projects/edit-project-modal.jsx";
import parseDomain from 'parse-domain'
import SelectField from 'material-ui/SelectField';
import SensitivityQuestionnaire from './sensitivity-questionnaire.jsx';
import IconButton from 'material-ui/IconButton';
const moment = require('moment');
import '../../../../public/css/customScrollbar.css';

const ENTER_KEY = 'Enter';

const DEFAULT_COMPANY_FIELDS = {
    companyName: '',
    allowedUsers: [],
    allowedEditUsers: [],
    sensitivity: 2,
    ratios: {
        surveyWeight: 0,
        intelWeight: 100
    },
    intelScoreRatios: DEFAULT_INTEL_SCORE_RATIOS,
    keywords: [],
    selectedDomains: [],
    IPList: [],
    scanPriority: 3
};

export default class AddCompanyModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            openAddNewCompanyModal: false,
            openEditContactsModal: false,
            openEditInformationModal: false,
            openNewProjectModal: false,
            newProjectName: '',
            companyFields: DEFAULT_COMPANY_FIELDS,
            allCompanies: [],
            usersForDD: this.props.allUsers,
            stepIndex: 0,
            companyResponsibles: [],
            companyContacts: [],
            companyResponsibleToAdd: JSON.parse(JSON.stringify(EMPTY_CONTACT)),
            companyContactToAdd: JSON.parse(JSON.stringify(EMPTY_CONTACT)),
            openYesNoModal: false,
            titleYesNoModal: '',
            messageYesNoModal: '',
            paramObjectYesNoModal: {},
            companyDescription: '',
            companyClassifications: [],
            companySectors: [],
            companyDateOfContact: JSON.parse(JSON.stringify(EMPTY_DATE_RANGE_FIELD)),
            companyDateOfSurvey: JSON.parse(JSON.stringify(EMPTY_DATE_RANGE_FIELD)),
            companyInformation: [],
            companyInformationToAdd: '',
            addedChipArray: [],
            deletedChipArray: [],
            addedIPChipArray: [],
            deletedIPChipArray: [],
            chosenOrg: {},
            chosenProj: {},
            curOrgIdx: 0,
            curProjIdx: this.props.companyFields.curProjIdx || 0,
            projectsForDD: [],
            openAddNewProjectModal: false,
            projectFields: {
                projectName: '',
                allowedUsers: [],
                selectedCompanies: [],
                createDate: ''
            },
            newProjectsCreatedByUser: [],
            isQuestionnaireActive: false,
            isQuestionnaireStepVisible: false
        };

        this.addContactField = this.addContactField.bind(this);
        this.checkContactFieldsValid = this.checkContactFieldsValid.bind(this);
        this.addContactFieldsToArray = this.addContactFieldsToArray.bind(this);
        this.handleOpenEditContactsArray = this.handleOpenEditContactsArray.bind(this);
        this.handleCloseEditContactsArray = this.handleCloseEditContactsArray.bind(this);
        this.handleOpenEditInformationArray = this.handleOpenEditInformationArray.bind(this);
        this.handleCloseEditInformationArray = this.handleCloseEditInformationArray.bind(this);
        this.addInfoField = this.addInfoField.bind(this);
        this.removeInfoFieldArrayItem = this.removeInfoFieldArrayItem.bind(this);
        this.handleAddInformation = this.handleAddInformation.bind(this);
        this.handleAddContact = this.handleAddContact.bind(this);
        this.chipArrCheckWhenAdded = this.chipArrCheckWhenAdded.bind(this);
        this.chipArrCheckWhenDeleted = this.chipArrCheckWhenDeleted.bind(this);
        this.chipArrCheckIPWhenAdded = this.chipArrCheckIPWhenAdded.bind(this);
        this.chipArrCheckIPWhenDeleted = this.chipArrCheckIPWhenDeleted.bind(this);
        this.handleChangeScanPriority = this.handleChangeScanPriority.bind(this);
        this.handleQuestionnaireBtn = this.handleQuestionnaireBtn.bind(this);
        this.handleSensitivityQuestionnaireFinish = this.handleSensitivityQuestionnaireFinish.bind(this);
        this.handleSensitivityQuestionnaireCancel = this.handleSensitivityQuestionnaireCancel.bind(this);
    }

    //This function sets two arrays, one for deleted chips and another for added chips.
    chipArrCheckIPWhenAdded(chip) {
        let addedIPChipArray = this.state.addedIPChipArray;

        if (this.state.deletedIPChipArray.includes(chip)) {
            let deletedIPChipArray = this.state.deletedIPChipArray;
            const index = deletedIPChipArray.indexOf(chip);

            if (index > -1) {
                deletedIPChipArray.splice(index, 1);
                this.setState({deletedIPChipArray: deletedIPChipArray})
            }
        } else {
            addedIPChipArray.push(chip);
            this.setState({addedIPChipArray: addedIPChipArray})
        }
    }

    handleQuestionnaireBtn() {
        let isQuestionnaireStepVisible = !this.state.isQuestionnaireStepVisible;
        if(this.state.isQuestionnaireActive){
            isQuestionnaireStepVisible = false;
        }
        this.setState({isQuestionnaireActive: !this.state.isQuestionnaireActive,
            isQuestionnaireStepVisible: isQuestionnaireStepVisible, finalSensitivityLevelText: ''});
    }

    handleChangeScanPriority(e, newScanPriority) {
        let companyFields = this.state.companyFields;
        if(!newScanPriority && newScanPriority !== 0){
            newScanPriority =  this.state.companyFields.scanPriority;
        }
        companyFields.scanPriority = newScanPriority;
        this.setState({companyFields: companyFields});
    }


    //This function sets two arrays, one for deleted chips and another for added chips.
    chipArrCheckIPWhenDeleted(chip) {
        let deletedIPChipArray = this.state.deletedIPChipArray;

        if (this.state.addedIPChipArray.includes(chip)) {
            let addedIPChipArray = this.state.addedIPChipArray;
            const index = addedIPChipArray.indexOf(chip);

            if (index > -1) {
                addedIPChipArray.splice(index, 1);
                this.setState({addedIPChipArray: addedIPChipArray})
            }
        } else {
            deletedIPChipArray.push(chip);
            this.setState({deletedIPChipArray: deletedIPChipArray})
        }
    }

    validateIPAddress(ipAddress)
    {
        if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipAddress))
        {
            return true;
        }
        app.addAlert('error', 'One or more of the IP addresses you entered is not valid!');
        return false;
    }

    handleAddIPToList(chips) {
        if (!Array.isArray(chips)) {
            chips = [chips];
        }

        let result = [];
        chips.map((chip) => {
            //TODO: Add IP validation
            if (this.validateIPAddress(chip)) {
                result.push(chip);
            }
        });

        for (let i = 0; i < result.length; i++) {
            result[i] = result[i].replace(/(\r\n|\n|\r)/gm, '');
        }

        let companyFields = this.state.companyFields;
        result.map((chip) => {
            // Prevent domains duplicates
            if (chip && !companyFields.IPList.includes(chip)) {
                companyFields.IPList.push(chip);
            }
        });

        this.chipArrCheckIPWhenAdded(result);
        this.setState(companyFields);
    }

    handleDeleteIPFromList(value, index) {
        let companyFields = this.state.companyFields;
        companyFields.IPList.splice(index, 1);

        this.chipArrCheckIPWhenDeleted(value);
        this.setState(companyFields);
    }

    //This function sets two arrays, one for deleted chips and another for added chips.
    chipArrCheckWhenAdded(chipId) {
        let addedChipArray = this.state.addedChipArray;

        if (this.state.deletedChipArray.includes(chipId)) {
            let deletedChipArray = this.state.deletedChipArray;
            const index = deletedChipArray.indexOf(chipId);

            if (index > -1) {
                deletedChipArray.splice(index, 1);
                this.setState({deletedChipArray: deletedChipArray})
            }
        } else {
            addedChipArray.push(chipId);
            this.setState({addedChipArray: addedChipArray})
        }
    }

    //This function sets two arrays, one for deleted chips and another for added chips.
    chipArrCheckWhenDeleted(chipId) {
        let deletedChipArray = this.state.deletedChipArray;

        if (this.state.addedChipArray.includes(chipId)) {
            let addedChipArray = this.state.addedChipArray;
            const index = addedChipArray.indexOf(chipId);

            if (index > -1) {
                addedChipArray.splice(index, 1);
                this.setState({addedChipArray: addedChipArray})
            }
        } else {
            deletedChipArray.push(chipId);
            this.setState({deletedChipArray: deletedChipArray})
        }
    }

    handleChangeCompanyNameField(e) {
        let companyFields = this.state.companyFields;
        companyFields.companyName = e.target.value;
        this.setState(companyFields);
    }

    // This generic function assigns the proper contactField's value to matching contactType array.
    addContactField(contactType, contactField, value) {
        let stateFieldToAdd;

        if (contactType === COMPANY_CONTACT_TYPES.RESPONSIBLE) {
            stateFieldToAdd = 'companyResponsibleToAdd';
        } else {
            // this means type is regular 'CONTACT'.
            stateFieldToAdd = 'companyContactToAdd';
        }
        if (this.state.hasOwnProperty(stateFieldToAdd) && this.state[stateFieldToAdd].hasOwnProperty(contactField)) {
            let fieldToUpdate = this.state[stateFieldToAdd];

            fieldToUpdate[contactField] = value;

            let stateToUpdate = {};

            stateToUpdate[stateFieldToAdd] = fieldToUpdate;
            this.setState(stateToUpdate);
        }
    }

    // This generic function sets the infoField with the value (in case of array - pushing value as new item).
    addInfoField(infoField, value) {
        let stateToUpdate = {};

        if (this.state.hasOwnProperty(infoField) && Array.isArray(this.state[infoField])) {
            stateToUpdate[infoField] = this.state[infoField];
            stateToUpdate[infoField].push(value);
        } else {
            stateToUpdate[infoField] = value;
        }
        this.setState(stateToUpdate);
    }

    // This generic function removes the itemToDelete from infoField only in case it is an array.
    removeInfoFieldArrayItem(infoField, itemToDelete) {
        let stateToUpdate = {};

        if (this.state.hasOwnProperty(infoField) && Array.isArray(this.state[infoField])) {
            stateToUpdate[infoField] = this.state[infoField];

            let itemIndex = stateToUpdate[infoField].indexOf(itemToDelete);

            if (itemIndex > -1) {
                stateToUpdate[infoField].splice(itemIndex, 1);
                this.setState(stateToUpdate);
            }
        }
    }

    checkContactFieldsValid(contactType) {
        let isValid = false;
        let stateFieldNameToCheck;

        if (contactType === COMPANY_CONTACT_TYPES.RESPONSIBLE) {
            stateFieldNameToCheck = 'companyResponsibleToAdd';
        } else {
            // this means type is regular 'CONTACT'.
            stateFieldNameToCheck = 'companyContactToAdd';
        }
        let stateField = this.state[stateFieldNameToCheck];

        if (stateField) {
            let objKeys = Object.keys(stateField);
            if (objKeys && objKeys.length > 0) {
                for (let i = 0; i < objKeys.length; i++) {
                    // Check if at least one field has any value.
                    if (stateField[objKeys[i]]) {
                        isValid = true;
                        break;
                    }
                }
            }
        }
        return isValid;
    }

    checkInformationFieldValid() {
        return !!this.state.companyInformationToAdd;
    }

    addContactFieldsToArray(contactType) {
        let stateFieldToAdd;
        let stateArrayToAdd;
        let notifyType;

        if (contactType === COMPANY_CONTACT_TYPES.RESPONSIBLE) {
            stateFieldToAdd = 'companyResponsibleToAdd';
            stateArrayToAdd = 'companyResponsibles';
            notifyType = 'Auditor';
        } else {
            // this means type is regular 'CONTACT'.
            stateFieldToAdd = 'companyContactToAdd';
            stateArrayToAdd = 'companyContacts';
            notifyType = 'Contact';
        }

        let fieldToUpdate = this.state[stateArrayToAdd];

        fieldToUpdate.push(this.state[stateFieldToAdd]);

        let stateToUpdate = {};

        stateToUpdate[stateArrayToAdd] = fieldToUpdate;
        // Clear the field for future use.
        stateToUpdate[stateFieldToAdd] = JSON.parse(JSON.stringify(EMPTY_CONTACT));
        this.setState(stateToUpdate);
        this.setState(stateToUpdate, () => {
            app.addAlert('success', 'Added ' + notifyType + ' Successfully');
        });
    }

    handleChangeContactEmailField(contactType, e) {
        this.addContactField(contactType, 'email', e.target.value);
    }

    handleChangeContactNameField(contactType, e) {
        this.addContactField(contactType, 'name', e.target.value);
    }

    handleChangeContactPhoneField(contactType, e) {
        this.addContactField(contactType, 'phone', e.target.value);
    }

    handleChangeContactPositionField(contactType, e) {
        this.addContactField(contactType, 'position', e.target.value);
    }

    handleAddContact(contactType, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        if (this.checkContactFieldsValid(contactType)) {
            this.addContactFieldsToArray(contactType);
        } else {
            app.addAlert('warning', 'No data was inserted');
        }
    }

    getCompanyInformationObject() {
        return {
            checked: false,
            label: this.state.companyInformationToAdd
        };
    }

    handleAddInformation(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        if (this.checkInformationFieldValid()) {
            this.addInfoField('companyInformation', this.getCompanyInformationObject());
            this.setState({companyInformationToAdd: ''}, () => {
                app.addAlert('success', 'Added Information Successfully');
            });
        } else {
            app.addAlert('warning', 'No data was inserted');
        }
    }

    handleChangeDescriptionField(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.addInfoField('companyDescription', e.target.value);
    }

    handleAddChipInfoField(infoFieldName, chip) {
        if (chip) {
            let dataSource;

            if (infoFieldName === 'companyClassifications') {
                dataSource = COMPANY_INFO_CLASSIFICATION_TYPES;
            } else {
                // Means infoFieldName is "companySectors"
                dataSource = COMPANY_INFO_SECTOR_TYPES;
            }
            let matchedType = dataSource.find((t) => {
                let lowerCaseType = t;

                if (lowerCaseType) {
                    lowerCaseType = lowerCaseType.toString().toLowerCase();
                }
                return lowerCaseType === chip.toString().toLowerCase();
            });

            if (matchedType && this.state[infoFieldName] && !this.state[infoFieldName].includes(matchedType)) {
                this.addInfoField(infoFieldName, matchedType);
            }
        }
    }

    handleDeleteChipItemInfoField(infoFieldName, chip) {
        if (chip && this.state[infoFieldName] && this.state[infoFieldName].includes(chip)) {
            this.removeInfoFieldArrayItem(infoFieldName, chip);
        }
    }

    handleChangeInformationField(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.addInfoField('companyInformationToAdd', e.target.value);
    }

    handleOpenEditContactsArray(contactType, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.setState({openEditContactsModal: true});
    }

    handleCloseEditContactsArray(contactType, e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.setState({openEditContactsModal: false});
    }

    handleOpenEditInformationArray(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.setState({openEditInformationModal: true});
    }

    handleCloseEditInformationArray(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        this.setState({openEditInformationModal: false});
    }

    handleSaveContactsChanges(companyResponsibles, companyContacts) {
        let stateToUpdate = {openEditContactsModal: false};

        if (companyResponsibles && Array.isArray(companyResponsibles)) {
            stateToUpdate.companyResponsibles = companyResponsibles;
        }
        if (companyContacts && Array.isArray(companyContacts)) {
            stateToUpdate.companyContacts = companyContacts;
        }
        this.setState(stateToUpdate);
    }

    handleSaveInformationChanges(companyInformation) {
        let stateToUpdate = {openEditInformationModal: false};

        if (companyInformation && Array.isArray(companyInformation)) {
            stateToUpdate.companyInformation = companyInformation;
        }
        this.setState(stateToUpdate);
    }

    handleChangeInfoDateField(dateInfoField, startDate, endDate) {
        if (this.state[dateInfoField] && startDate && endDate) {
            this.state[dateInfoField].startDate = startDate;
            this.state[dateInfoField].endDate = endDate;
        }
    }

    handleChangeInfoSingleDateField(dateInfoField, ignore, newDate) {
        let fixedDate = fixTimezoneRange(newDate, true).format('LL');
        this.handleChangeInfoDateField(dateInfoField, fixedDate, fixedDate);
        // Force a rerender.
        this.forceUpdate();
    }

    handleChangeSurveyRatioField(e) {
        let companyFields;
        let val = e.target.value;

        val = val || val === 0 ? parseInt(val) : '';

        if (100 >= val && val >= 0) {
            companyFields = this.state.companyFields;
            companyFields.ratios = {};
            companyFields.ratios.surveyWeight = val;
            companyFields.ratios.intelWeight = 100 - val;
            this.setState({companyFields: companyFields});
        } else {
            app.addAlert('error', 'Error: ratio should be between 0-100.');
        }
    }

    handleChangeIntelRatioField(e) {
        let companyFields;
        let val = e.target.value;

        val = val || val === 0 ? parseInt(val) : '';

        if (100 >= val && val >= 0) {
            companyFields = this.state.companyFields;
            companyFields.ratios = {};
            companyFields.ratios.intelWeight = val;
            companyFields.ratios.surveyWeight = 100 - val;
            this.setState({companyFields: companyFields});
        } else {
            app.addAlert('error', 'Error: ratio should be between 0-100.');
        }
    }

    handleChangeIntelScoreRatioField(type, e) {
        let val = parseInt(e.target.value);

        if (100 >= val && val >= 0) {
            let companyFields = this.state.companyFields;
            companyFields.intelScoreRatios[type] = val;
            this.setState({companyFields: companyFields});
        } else {
            app.addAlert('error', 'Error: ratio should be between 0-100.');
        }
    }

    validateIntelScoreRatioField() {
        let isValid = true;

        if (this.state.companyFields.intelScoreRatios) {
            let sum = 0;
            Object.values(this.state.companyFields.intelScoreRatios).map((currVal) => {
                sum += currVal;
            });

            if (sum !== 100) {
                isValid = false;
                app.addAlert('error', 'intel score ratios must sum up to 100% (Current is ' + sum + '%)');
                // Navigate user to fix the error.
                if (this.state.stepIndex !== 2) {
                    this.setState({stepIndex: 2}, () => {
                        this.cveInput.focus();
                    });
                }
            }
        }
        return isValid;
    }

    validateNoForgottenContacts() {
        let checkIfForgotten = (dataObject) => {
            let isForgotten = false;
            Object.keys(dataObject).map((currKey) => {
                if (currKey && dataObject[currKey]) {
                    isForgotten = true;
                }
            });
            return isForgotten;
        };
        let isResponsibleForgotten = !!(this.state.companyResponsibleToAdd && checkIfForgotten(this.state.companyResponsibleToAdd));
        let isContactForgotten = !!(this.state.companyContactToAdd && checkIfForgotten(this.state.companyContactToAdd));
        let isInformationForgotten = this.checkInformationFieldValid();
        return {
            isResponsibleForgotten: isResponsibleForgotten,
            isContactForgotten: isContactForgotten,
            isInformationForgotten: isInformationForgotten
        };
    }

    getYesNoModalResult(result, paramObject) {
        let stateObj = {
            openYesNoModal: false,
            titleYesNoModal: '',
            messageYesNoModal: '',
            paramObjectYesNoModal: {}
        };

        if (result === YES_NO_MODAL_TYPES.CANCEL) {
            this.setState(stateObj);
        } else {
            if (result === YES_NO_MODAL_TYPES.OK) {
                let forgotten = this.validateNoForgottenContacts();

                if (forgotten.isResponsibleForgotten) {
                    stateObj.companyResponsibles = this.state.companyResponsibles;
                    stateObj.companyResponsibles.push(this.state.companyResponsibleToAdd);
                    stateObj.companyResponsibleToAdd = JSON.parse(JSON.stringify(EMPTY_CONTACT));
                }
                if (forgotten.isContactForgotten) {
                    stateObj.companyContacts = this.state.companyContacts;
                    stateObj.companyContacts.push(this.state.companyContactToAdd);
                    stateObj.companyContactToAdd = JSON.parse(JSON.stringify(EMPTY_CONTACT));
                }
                if (forgotten.isInformationForgotten) {
                    stateObj.companyInformation = this.state.companyInformation;
                    stateObj.companyInformation.push(this.getCompanyInformationObject());
                    stateObj.companyInformationToAdd = '';
                }
            }
            this.setState(stateObj, () => {
                this.addValidCompany(paramObject);
            });
        }
    }

    addValidCompany(companyFields, deletedChipArray, addedChipArray, deletedIPChipArray, addedIPChipArray) {
        // this is a fix for companyInfo, since companyFields are loaded from props and not from state.
        if (this.state.companyContacts || this.state.companyResponsibles || this.state.companyInformation
            || this.state.companyDescription || this.state.companyClassifications) {
            companyFields.companyInfo = {
                companyContacts: this.state.companyContacts || [],
                companyResponsibles: this.state.companyResponsibles || [],
                companyInformation: this.state.companyInformation || [],
                companyDescription: this.state.companyDescription || '',
                companyClassifications: this.state.companyClassifications || [],
                companySectors: this.state.companySectors || []
            };

            let rangeDatesNotEmpty = (datesObj) => {
                return !!(datesObj && (datesObj.startDate || datesObj.endDate));
            };

            if (rangeDatesNotEmpty(this.state.companyDateOfContact)) {
                companyFields.companyInfo.companyDateOfContact = this.state.companyDateOfContact;
            }
            if (rangeDatesNotEmpty(this.state.companyDateOfSurvey)) {
                companyFields.companyInfo.companyDateOfSurvey = this.state.companyDateOfSurvey;
            }
        }

        this.props.addCompany(companyFields, deletedChipArray, addedChipArray, deletedIPChipArray, addedIPChipArray, this.state.newProjectsCreatedByUser);
        this.setState({
            addedChipArray: [],
            deletedChipArray: [],
            addedIPChipArray: [],
            deletedIPChipArray: [],
            newProjectsCreatedByUser: [],
            curProjIdx: 0,
            chosenOrg: {},
            chosenProj: {},
            isQuestionnaireActive:false,
            isQuestionnaireStepVisible:false,
            finalSensitivityLevelText: ''
        })
    }

    onChangeSensitivityDropdown(event, index, value) {
        let companyFields = this.state.companyFields;
        companyFields.sensitivity = value;
        this.setState({companyFields: companyFields});
    }

    handleSensitivityQuestionnaireFinish(finalSensitivityLevel, finalSensitivityLevelText) {
        this.handleStepActionsClick(4);
        this.onChangeSensitivityDropdown(null, null, finalSensitivityLevel);
        this.setState({finalSensitivityLevelText: finalSensitivityLevelText, isQuestionnaireStepVisible: false});
    }
    handleSensitivityQuestionnaireCancel() {
        this.handleStepActionsClick(4);
        this.setState({isQuestionnaireStepVisible: false, isQuestionnaireActive: false, finalSensitivityLevelText: ''});
    }

    handleAddDomainToList(chips) {
        if (!Array.isArray(chips)) {
            chips = [chips];
        }

        let result = [];
        chips.map((chip) => {
            if (parseDomain(chip) === null) {
                app.addAlert('error', 'One or more of the domains you entered is not valid!');
            } else {
                result.push(parseDomain(chip).domain + '.' + parseDomain(chip).tld);
            }
        });

        for (let i = 0; i < result.length; i++) {
            result[i] = result[i].replace(/(\r\n|\n|\r)/gm, '');
        }

        let companyFields = this.state.companyFields;
        result.map((chip) => {
            // Prevent domains duplicates
            if (chip && !companyFields.selectedDomains.includes(chip)) {
                companyFields.selectedDomains.push(chip);
            }
        });
        let newKeywords = result;

        for (let j = 0; j < newKeywords.length; j++) {
            // Prevent keywords duplicates
            if (companyFields.keywords.indexOf(newKeywords[j]) === -1) {
                companyFields.keywords.push(newKeywords[j]);
            }
        }
        this.chipArrCheckWhenAdded(result.id);
        this.setState({
            companyFields: companyFields
        });
    }

    handleDeleteAllowedUsersField(value) {
        let companyFields = this.state.companyFields;

        companyFields.allowedUsers = companyFields.allowedUsers.filter(function (obj) {
            // This is a fix for bug when couldn't remove an allowedUser.
            let compareObj = (obj && obj.id) ? obj.id : obj;
            return compareObj !== value;
        });

        this.setState({companyFields: companyFields});
    }

    handleDeleteDomainFromList(value, index) {
        let companyFields = this.state.companyFields;
        companyFields.selectedDomains.splice(index, 1);

        if (companyFields.keywords) {
            let keywordIndex = companyFields.keywords.indexOf(value.split('.')[0]);
            if (keywordIndex > -1) {
                companyFields.keywords.splice(keywordIndex, 1);
            }
            keywordIndex = companyFields.keywords.indexOf(value);
            if (keywordIndex > -1) {
                companyFields.keywords.splice(keywordIndex, 1);
            }
        }
        this.chipArrCheckWhenDeleted(value);
        this.setState({companyFields: companyFields});
    }

    handleAddKeywordToList(chip) {
        if (!Array.isArray(chip)) {
            chip = [chip];
        }
        let companyFields = this.state.companyFields;
        companyFields.keywords = companyFields.keywords.concat(chip);
        this.setState({companyFields: companyFields});
    }

    handleDeleteKeywordFromList(value, index) {
        let companyFields = this.state.companyFields;
        companyFields.keywords.splice(index, 1);
        this.setState({companyFields: companyFields});
    }

    handleAddAllowedUsersField(input) {
        let companyFields = this.state.companyFields;
        companyFields.allowedUsers.push(input);
        this.setState({companyFields: companyFields});
    }

    handleAddAllowedEditUsersField(input) {
        let companyFields = this.state.companyFields;
        companyFields.allowedEditUsers.push(input);
        this.setState({companyFields: companyFields});
    }

    handleDeleteAllowedUsersEditField(value) {
        let companyFields = this.state.companyFields;

        companyFields.allowedEditUsers = companyFields.allowedEditUsers.filter(function (obj) {
            // This is a fix for bug when couldn't remove an allowedEditUser.
            let compareObj = (obj && obj.id) ? obj.id : obj;
            return compareObj !== value;
        });

        this.setState({companyFields: companyFields});
    }

    cancel() {
        this.setState({openAddNewCompanyModal: false, openAddNewProjectModal: false,
            isQuestionnaireActive:false, isQuestionnaireStepVisible:false, finalSensitivityLevelText: ''});
        this.props.clearAllModalFields();
        app.getProjects();
    }

    componentWillReceiveProps(nextProps) {
        let stateObj = {open: nextProps.open, stepIndex: 0, openNewProjectModal: false, newProjectName: ''};

        stateObj.openAddNewCompanyModal = nextProps.openAddNewCompanyModal;

        stateObj.companyFields = nextProps.companyFields || DEFAULT_COMPANY_FIELDS;

        if (nextProps.companyFields.scanPriority || nextProps.companyFields.scanPriority === 0) {
            stateObj.companyFields.scanPriority = nextProps.companyFields.scanPriority;
        } else {
            stateObj.companyFields.scanPriority = DEFAULT_COMPANY_FIELDS.scanPriority;
        }

        if (nextProps.companyFields.companyName) {
            stateObj.companyFields.companyName = nextProps.companyFields.companyName;
        } else {
            stateObj.companyFields.companyName = DEFAULT_COMPANY_FIELDS.companyName;
        }

        if (nextProps.companyFields.allowedUsers) {
            stateObj.companyFields.allowedUsers = nextProps.companyFields.allowedUsers;
        } else {
            stateObj.companyFields.allowedUsers = DEFAULT_COMPANY_FIELDS.allowedUsers;
        }

        if (nextProps.companyFields.allowedEditUsers) {
            stateObj.companyFields.allowedEditUsers = nextProps.companyFields.allowedEditUsers;
        } else {
            stateObj.companyFields.allowedEditUsers = DEFAULT_COMPANY_FIELDS.allowedEditUsers;
        }

        if (nextProps.allUsers) {
            stateObj.usersForDD = nextProps.allUsers;
        }


        if (nextProps.projectsArrForUserInput && nextProps.projectsArrForUserInput[this.state.curProjIdx]) {
            stateObj.chosenProj = nextProps.projectsArrForUserInput[this.state.curProjIdx];
            stateObj.companyFields.chosenProj = nextProps.projectsArrForUserInput[this.state.curProjIdx];
            stateObj.projectsForDD = nextProps.projectsArrForUserInput.map((item) => {
                return {name: item.projectName, uid: item.uid};
            });
        }

        if (nextProps.curProjectIdx === 0 || nextProps.curProjectIdx) {
            stateObj.curProjectIdx = nextProps.curProjectIdx;
            stateObj.chosenProj = nextProps.projectsArrForUserInput[nextProps.curProjectIdx];
            stateObj.companyFields.chosenProj = nextProps.projectsArrForUserInput[nextProps.curProjectIdx];
            this.setState({curProjIdx: nextProps.curProjectIdx});
        }

        if (nextProps.companyFields.sensitivity) {
            stateObj.companyFields.sensitivity = nextProps.companyFields.sensitivity;
        }

        if (nextProps.companyFields.ratios &&
            nextProps.companyFields.ratios.intelWeight != null && nextProps.companyFields.ratios.surveyWeight != null) {
            stateObj.companyFields.ratios = {
                intelWeight: nextProps.companyFields.ratios.intelWeight,
                surveyWeight: nextProps.companyFields.ratios.surveyWeight
            };
        } else {
            stateObj.companyFields.ratios = DEFAULT_COMPANY_FIELDS.ratios;
        }

        if (nextProps.companyFields.intelScoreRatios) {
            stateObj.companyFields.intelScoreRatios = nextProps.companyFields.intelScoreRatios;

            // Iterate on all supposed-to-exist intelScoreRatios to verify there is a value for each one.
            Object.keys(DEFAULT_INTEL_SCORE_RATIOS).map((currKey) => {
                if (currKey && stateObj.companyFields.intelScoreRatios[currKey] == null) {
                    stateObj.companyFields.intelScoreRatios[currKey] = 0;
                }
            });
        } else {
            stateObj.companyFields.intelScoreRatios = DEFAULT_COMPANY_FIELDS.intelScoreRatios;
        }

        if (nextProps.companyFields.keywords) {
            stateObj.companyFields.keywords = nextProps.companyFields.keywords;
        } else {
            stateObj.companyFields.keywords = DEFAULT_COMPANY_FIELDS.keywords;
        }

        if (nextProps.companyFields.IPList) {
            stateObj.companyFields.IPList = nextProps.companyFields.IPList;
        } else {
            stateObj.companyFields.IPList = DEFAULT_COMPANY_FIELDS.IPList;
        }

        if (nextProps.companyFields.selectedDomains) {
            stateObj.companyFields.selectedDomains = nextProps.companyFields.selectedDomains;
        } else {
            stateObj.companyFields.selectedDomains = DEFAULT_COMPANY_FIELDS.selectedDomains;
        }

        if (nextProps.allUsers) {
            stateObj.usersForDD = nextProps.allUsers;
        }

        if (nextProps.companyFields.companyResponsibles) {
            stateObj.companyResponsibles = nextProps.companyFields.companyResponsibles;
        } else {
            stateObj.companyResponsibles = [];
        }

        if (nextProps.companyFields.companyContacts) {
            stateObj.companyContacts = nextProps.companyFields.companyContacts;
        } else {
            stateObj.companyContacts = [];
        }

        if (nextProps.companyFields.companyInformation) {
            stateObj.companyInformation = nextProps.companyFields.companyInformation;
        } else {
            stateObj.companyInformation = [];
        }

        if (nextProps.companyFields.companyDescription) {
            stateObj.companyDescription = nextProps.companyFields.companyDescription;
        } else {
            stateObj.companyDescription = '';
        }

        if (nextProps.companyFields.companyClassifications) {
            stateObj.companyClassifications = nextProps.companyFields.companyClassifications;
        } else {
            stateObj.companyClassifications = [];
        }

        if (nextProps.companyFields.companySectors) {
            stateObj.companySectors = nextProps.companyFields.companySectors;
        } else {
            stateObj.companySectors = [];
        }

        if (nextProps.companyFields.companyDateOfContact) {
            stateObj.companyDateOfContact = nextProps.companyFields.companyDateOfContact;
        } else {
            stateObj.companyDateOfContact = JSON.parse(JSON.stringify(EMPTY_DATE_RANGE_FIELD));
        }

        if (nextProps.companyFields.companyDateOfSurvey) {
            stateObj.companyDateOfSurvey = nextProps.companyFields.companyDateOfSurvey;
        } else {
            stateObj.companyDateOfSurvey = JSON.parse(JSON.stringify(EMPTY_DATE_RANGE_FIELD));
        }

        if (nextProps.companyFields.companyInfo) {
            if (nextProps.companyFields.companyInfo.responsibles) {
                stateObj.companyResponsibles = nextProps.companyFields.companyInfo.responsibles;
            }
            if (nextProps.companyFields.companyInfo.contacts) {
                stateObj.companyContacts = nextProps.companyFields.companyInfo.contacts;
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('informations')
                && Array.isArray(nextProps.companyFields.companyInfo.informations)) {
                stateObj.companyInformation = [];
                nextProps.companyFields.companyInfo.informations.map((currInfo) => {
                    if (currInfo && currInfo.label) {
                        // If no 'checked' property exists - set as false.
                        currInfo.checked = !!currInfo.checked;
                        stateObj.companyInformation.push(currInfo);
                    }
                });
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('classifications')
                && Array.isArray(nextProps.companyFields.companyInfo.classifications)) {
                stateObj.companyClassifications = [];
                nextProps.companyFields.companyInfo.classifications.map((currClass) => {
                    if (currClass && currClass.label) {
                        stateObj.companyClassifications.push(currClass.label);
                    }
                });
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('sectors')
                && Array.isArray(nextProps.companyFields.companyInfo.sectors)) {
                stateObj.companySectors = [];
                nextProps.companyFields.companyInfo.sectors.map((currSector) => {
                    if (currSector && currSector.label) {
                        stateObj.companySectors.push(currSector.label);
                    }
                });
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('date_of_contacts')
                && Array.isArray(nextProps.companyFields.companyInfo['date_of_contacts'])
                && nextProps.companyFields.companyInfo['date_of_contacts'].length === 1) {
                let rangeDates = nextProps.companyFields.companyInfo['date_of_contacts'][0];
                if (rangeDates && rangeDates.startDate && rangeDates.endDate) {
                    stateObj.companyDateOfContact = rangeDates;
                }
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('date_of_surveys')
                && Array.isArray(nextProps.companyFields.companyInfo['date_of_surveys'])
                && nextProps.companyFields.companyInfo['date_of_surveys'].length === 1) {
                let rangeDates = nextProps.companyFields.companyInfo['date_of_surveys'][0];
                if (rangeDates && rangeDates.startDate && rangeDates.endDate) {
                    stateObj.companyDateOfSurvey = rangeDates;
                }
            }
            if (nextProps.companyFields.companyInfo.hasOwnProperty('descriptions')
                && Array.isArray(nextProps.companyFields.companyInfo.descriptions)
                && nextProps.companyFields.companyInfo.descriptions.length === 1) {
                let desc = nextProps.companyFields.companyInfo.descriptions[0];
                if (desc && desc.label) {
                    stateObj.companyDescription = desc.label;
                }
            }
        }

        // Set all companies as selected by default, so user can only delete or re-add them.
        this.setState(stateObj);
    }

    handleStepActionsClick(step) {
        if (this.state.stepIndex !== step) {
            this.setState({stepIndex: step});
        }
    }

    validateNewCompany(companyFields, deletedChipArray, addedChipArray, deletedIPChipArray, addedIPChipArray) {
        if (companyFields && !companyFields.companyName) {
            app.addAlert('error', 'Error: Company Name must be assigned.');

            this.setState({stepIndex: 0});
        } else if (this.validateIntelScoreRatioField()) {
            let forgotten = this.validateNoForgottenContacts();
            if (forgotten.isResponsibleForgotten || forgotten.isContactForgotten || forgotten.isInformationForgotten) {
                // Open modal with matching message.
                let title = 'Forgotten ';
                let msg = 'It looks like ';
                let isBothForgotten = false;

                if (forgotten.isResponsibleForgotten) {
                    title += 'Auditor ';
                    msg += 'an Auditor ';
                    if (forgotten.isContactForgotten || forgotten.isInformationForgotten) {
                        title += '& ';
                        if (forgotten.isContactForgotten && forgotten.isInformationForgotten) {
                            msg += ', ';
                        } else {
                            msg += 'and ';
                        }
                        isBothForgotten = true;
                    }
                }
                if (forgotten.isContactForgotten) {
                    title += 'Company Contact ';
                    msg += 'a Company Contact ';
                    if (forgotten.isInformationForgotten) {
                        title += '& Company Information ';
                        msg += 'and a Company Information ';
                    }
                } else if (forgotten.isInformationForgotten) {
                    title += 'Company Information ';
                    msg += 'a Company Information ';
                }
                if (isBothForgotten) {
                    msg += 'were ';
                } else {
                    msg += 'was ';
                }
                msg += 'not added.\nDo you wish to add it now?';
                this.setState({
                    openYesNoModal: true,
                    titleYesNoModal: title,
                    messageYesNoModal: msg,
                    paramObjectYesNoModal: companyFields
                });
            } else {
                this.addValidCompany(companyFields, deletedChipArray, addedChipArray, deletedIPChipArray, addedIPChipArray);
            }
        }
    }

    handleProjChange(event, index, value) {
        this.setState({curProjIdx: value})
    }

    handleProjSelect(idx, e) {
        if (this.props.projectsArrForUserInput && this.props.projectsArrForUserInput[idx]) {
            let companyFields = this.state.companyFields;
            companyFields.chosenProj = this.props.projectsArrForUserInput[idx];
            companyFields.chosenProj.projIdx = idx;

            this.setState({
                companyFields: companyFields,
                projectsForDD: this.props.projectsArrForUserInput.map((item) => {
                    return {name: item.projectName};
                })
            });
        }
    }

    createNewProject(projectFields) {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let data = projectFields;
            data.createDate = moment().format();
            data.allowedUsers = [{email: user.email, id: user.id}];

            let curOrg = {};
            data.uid = user.id;
            if (this.props.curOrg) {
                curOrg = this.props.curOrg;
            }
            $.ajax({
                type: 'post',
                url: '/api/createNewProjectByUser',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({data: data, curOrg: curOrg}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.newProject) {
                    if (!data.newProject._id || data.newProject._id === "") {
                        data.newProject._id = data.newProject.id;
                    }
                    let newProjectsCreatedByUser = this.state.newProjectsCreatedByUser;
                    newProjectsCreatedByUser.push(data.newProject);
                    this.props.projectsArrForUserInput.push(data.newProject);
                    this.setState({newProjectsCreatedByUser: newProjectsCreatedByUser});
                    this.handleProjSelect(this.props.projectsArrForUserInput.length - 1);
                    this.handleProjChange([], this.props.projectsArrForUserInput.length - 1, this.props.projectsArrForUserInput.length - 1);
                    this.clearAllProjectModalFields();
                    app.addAlert('success', 'Project Added.');
                } else {
                    app.addAlert('error', 'Error: failed to add project.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add project.');
            });
        }
    }

    submitProject(projectFields, addedChipArray, deletedChipArray) {
        let data = projectFields;
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;
            let extraData = {
                detachArrayToAddToNeo: addedChipArray,
                detachArrayToRemoveFromNeo: deletedChipArray,
            };
            data.createDate = moment().format();

            $.ajax({
                type: 'post',
                url: '/api/addProject',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({uid: uid, data: data, extraData: extraData}),
                dataType: 'json'
            }).done((data) => {
                if (data) {
                    let projects = this.state.projects;
                    let newProjectsAfterAdd = projects;
                    let found = false;
                    for (let i = 0; i < projects.length; i++) {
                        if (projects[i].id === data.newProject.id) {
                            newProjectsAfterAdd[i] = data.newProject;
                            found = true;
                        }
                    }

                    if (!found) {
                        newProjectsAfterAdd.push(data.newProject);
                    }

                    app.addAlert('success', 'Project Added.');
                    this.setState({openAddNewProjectModal: false, projects: newProjectsAfterAdd});
                } else {
                    app.addAlert('error', 'Error: failed to add project.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to add project.');
            });
        }
    }

    handleOpenNewProjectModal() {
        this.setState({openAddNewProjectModal: true});
    }

    clearAllProjectModalFields() {
        let projectFields = this.state.projectFields;
        projectFields.projectName = '';
        projectFields.selectedCompanies = [];
        projectFields.allowedUsers = [];
        projectFields.id = '';
        this.setState({
            openAddNewProjectModal: false,
            projectFields: projectFields
        });
    }

    closeAddNewProjectModal() {
        this.setState({
            openAddNewProjectModal: false,
        });
    }


    render() {
        let activeStep = this.state.stepIndex;
        let user = app.getAuthUser();
        let isAdmin = false;

        let isQuestionnaireActive = this.state.isQuestionnaireActive !== null ? this.state.isQuestionnaireActive  : false;
        let isQuestionnaireStepVisible = this.state.isQuestionnaireStepVisible !== null ? this.state.isQuestionnaireStepVisible  : false;

        let questionnaireBtnText = isQuestionnaireActive ? "Determine sensitivity by yourself" : "Use questionnaire to determine sensitivity";
        let questionnaireIconColor = isQuestionnaireActive ? '#0091ea' : 'rgba(0,0,0,0.7)';

        let isUserAllowedToAddVendors = false;
        if (user && user.userRole) {
            isAdmin = user.userRole === "admin";
        }
        if (user && user.isUserAllowedToAddVendors) {
            isUserAllowedToAddVendors = user.isUserAllowedToAddVendors;
        }
        let items = [];

        for (let i = 0; i < 6; i++ ) {
            items.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
        }

        const style = {
            domainsBtn: {
                color: '#0091ea',
                'margin': '38px 35px 6px'
            },
            SurveyScoreField: {
                'width': '25%',
                'marginLeft': '20px'
            },
            intelScoreField: {
                'width': '170px'
            },
            intelPartScoreField: {
                'width': '23%'
            },
            intelPartScoreWideField: {
                'width': '25%'
            },
            underlineStyle: {
                'width': '25px'
            },
            formRow: {
                'marginTop': '45px',
                'marginBottom': '45px'
            },
            newProjectContentStyle: {
                width: '25%',
                maxWidth: 'none'
            },
            companyContactTextField: {
                marginLeft: 5,
                marginBottom: 5,
                float: 'left',
                fontSize: 14,
                'width': '20%'
            },
            companyInfoTextField: {
                'width': '85%'
            },
            limitedSizeDiv: {
                maxHeight: 400,
                overflow: 'auto'
            },
            limitedSizeFlexDiv: {
                maxHeight: 250,
                overflow: 'auto',
                display: 'grid'
            },
            autoCompleteStyle: {
                overflowY: 'auto',
                height: '200px',
                maxHeight: '200px'
            },
            infoHintCheckbox: {
                cursor: 'text'
            },
            infoHintCheckboxLabel: {
                width: '100%'
            },
            singleDatePicker: {
                display: 'inline-block',
                marginLeft: '5px'
            },
            stepContentStyle: {
                'overflow': 'auto',
                'maxHeight': '800px',
                background: '#efefef',
                borderRadius: 10,
                border: '1px solid',
                borderColor: '#f5f5f5'

            },
            stepButtonStyle: {
                borderRadius: 30
            },
            stepButtonStyleLabel: {},
            stepStyle: {}
        };
        let cancelBtn = {};
        let doneBtn = {};
        if(isQuestionnaireStepVisible){
             cancelBtn = {color: 'white', marginBottom: '5px', height: 40, visibility:'hidden'};
             doneBtn = {color: 'white', marginBottom: '5px', marginRight: '5px', height: 40, visibility:'hidden'};
        } else {
            cancelBtn = {color: 'white', marginBottom: '5px', height: 40, visibility:'visible'};
            doneBtn = {color: 'white', marginBottom: '5px', marginRight: '5px', height: 40,visibility:'visible'};
        }

        let projects;
        if (this.props.projectsArrForUserInput) {
            projects = this.props.projectsArrForUserInput.map((project, idx) => {
                return <MenuItem value={idx}
                                 key={idx}
                    //style={{color: 'white', fontSize: '13px'}}
                                 primaryText={project.projectName}
                                 onTouchTap={this.handleProjSelect.bind(this, idx)}
                />
            });
        } else {
            projects = [];
        }

        let lastDateOfSurvey;
        if (this.state.companyDateOfSurvey.endDate) {
            lastDateOfSurvey = new Date(moment(this.state.companyDateOfSurvey.endDate));
        } else {
            lastDateOfSurvey = null;
        }
        let scanPriority = 3;

        if (this.state.companyFields.scanPriority || this.state.companyFields.scanPriority === 0){
            scanPriority = this.state.companyFields.scanPriority;
        }

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <Dialog
                    title={this.props.inCompanyEditMode ? "Edit Company" : "Add New Company"}
                    actions={[
                        <FlatButton
                            label="Done"
                            primary={true}
                            style={doneBtn}
                            keyboardFocused={true}
                            backgroundColor={'#0091ea'}
                            hoverColor={'#12a4ff'}
                            rippleColor={'white'}
                            labelStyle={{fontSize: 10}}
                            onTouchTap={this.validateNewCompany.bind(this, this.state.companyFields,
                                this.state.deletedChipArray,
                                this.state.addedChipArray,
                                this.state.deletedIPChipArray,
                                this.state.addedIPChipArray)}
                        />,
                        <FlatButton
                            label="Cancel"
                            primary={true}
                            backgroundColor={'#0091ea'}
                            style={cancelBtn}
                            keyboardFocused={true}
                            hoverColor={'#12a4ff'}
                            rippleColor={'white'}
                            labelStyle={{fontSize: 10}}
                            onTouchTap={this.cancel.bind(this)}
                        />
                    ]}
                    actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                    bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                    autoScrollBodyContent={true}
                    modal={true}
                    open={this.state.openAddNewCompanyModal}
                    onRequestClose={this.cancel.bind(this)}
                    repositionOnUpdate={false}
                    contentStyle={{maxWidth: '60%', borderRadius: '7px 7px 7px 7px'}}
                    titleStyle={{
                        fontSize: 18,
                        background: 'rgba(0,0,0,0.7)',
                        color: 'white', textAlign: 'center',
                        borderRadius: '2px 2px 0px 0px',
                        textTransform: 'uppercase',
                    }}>
                    <div>
                        {!isQuestionnaireStepVisible ?
                        <Stepper
                            color={'#0091ea'}
                            activeStep={activeStep}
                            linear={false}
                            orientation="vertical"
                            style={{marginTop: 10}}>
                            <Step style={style.stepStyle}>
                                <StepButton style={style.stepButtonStyle}
                                            onClick={this.handleStepActionsClick.bind(this, 0)}>
                                    <span style={style.stepButtonStyleLabel}>Company</span>
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <div style={style.limitedSizeDiv}>
                                        <br/>
                                        <br/>
                                        <div>
                                            <span style={{width: '100%'}}>Enter the company name:</span>
                                            <br/>
                                            <TextField
                                                id={'1'}
                                                hintText="Company Name"
                                                onChange={this.handleChangeCompanyNameField.bind(this)}
                                                value={this.state.companyFields.companyName}
                                            />
                                        </div>
                                        {
                                            this.props.projectsArrForUserInput &&
                                            <div>
                                                <br/>
                                                <div style={{width: '100%'}}>Project:</div>
                                                <DropDownMenu
                                                    labelStyle={{left: 12}}
                                                    style={{right: 35, width: '300px'}}
                                                    anchorOrigin={{
                                                        horizontal: 'left',
                                                        vertical: 'bottom'
                                                    }}
                                                    value={this.state.curProjIdx}
                                                    autoWidth={false}
                                                    onChange={this.handleProjChange.bind(this)}>
                                                    {projects}
                                                </DropDownMenu>
                                                <FlatButton
                                                    label="Create New Project"
                                                    onClick={this.handleOpenNewProjectModal.bind(this)}
                                                    icon={<ActionAdd/>}
                                                    style={{
                                                        right: 25,
                                                        bottom: 3,
                                                        color: 'white',
                                                        borderRadius: 6,
                                                        height: 40
                                                    }}
                                                    labelStyle={{fontSize: 13}}
                                                    backgroundColor={'rgba(0,0,0,0.7'}
                                                    hoverColor={'#0091ea'}
                                                />
                                                <EditProjectModal
                                                    title={"Create New Project"}
                                                    submitProject={this.createNewProject.bind(this)}
                                                    cancel={this.cancel}
                                                    projectFields={this.state.projectFields}
                                                    companies={this.props.companies}
                                                    allUsers={this.props.users}
                                                    openAddNewProjectModal={this.state.openAddNewProjectModal}
                                                    closeProjectModal={this.closeAddNewProjectModal.bind(this)}
                                                />
                                            </div>
                                        }
                                        <br/>
                                        <div style={{width: '100%', position: 'relative', top: 15}}>Add company
                                            description:
                                        </div>
                                        <TextField
                                            floatingLabelText="Company Description"
                                            hintText="Enter description"
                                            onChange={this.handleChangeDescriptionField.bind(this)}
                                            value={this.state.companyDescription}
                                            style={style.companyInfoTextField}
                                            multiLine={true}
                                            rows={1}
                                            rowsMax={2}
                                        />
                                        <br/>
                                        <br/>
                                        <span style={{width: '100%', position: 'relative', top: 15}}>Who are the company contacts?</span>
                                        <div>
                                            <ValidatorForm
                                                ref="form"
                                                onSubmit={this.handleAddContact.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                onError={errors => console.log(errors)}>
                                                <TextValidator
                                                    id={"15"}
                                                    name="email"
                                                    floatingLabelText="Email"
                                                    hintText="Contact Email"
                                                    validators={['required', 'isEmail']}
                                                    errorMessages={['this field is required', 'email is not valid']}
                                                    onChange={this.handleChangeContactEmailField.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                    onKeyPress={(ev) => {
                                                        if (ev && ev.key === ENTER_KEY) {
                                                            this.handleAddContact(COMPANY_CONTACT_TYPES.CONTACT, ev);
                                                        }
                                                    }}
                                                    value={this.state.companyContactToAdd.email}
                                                    style={style.companyContactTextField}
                                                />
                                                &nbsp;&nbsp;
                                                <TextValidator
                                                    id={"16"}
                                                    name="name"
                                                    floatingLabelText="Name"
                                                    hintText="Contact name"
                                                    validators={['required']}
                                                    errorMessages={['this field is required']}
                                                    onChange={this.handleChangeContactNameField.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                    onKeyPress={(ev) => {
                                                        if (ev && ev.key === ENTER_KEY) {
                                                            this.handleAddContact(COMPANY_CONTACT_TYPES.CONTACT, ev);
                                                        }
                                                    }}
                                                    value={this.state.companyContactToAdd.name}
                                                    style={style.companyContactTextField}
                                                />
                                                &nbsp;&nbsp;
                                                <TextValidator
                                                    id={"17"}
                                                    name="phone"
                                                    floatingLabelText="Phone"
                                                    hintText="Contact phone"
                                                    validators={['isNumber']}
                                                    errorMessages={['numeric characters only']}
                                                    onChange={this.handleChangeContactPhoneField.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                    onKeyPress={(ev) => {
                                                        if (ev && ev.key === ENTER_KEY) {
                                                            this.handleAddContact(COMPANY_CONTACT_TYPES.CONTACT, ev);
                                                        }
                                                    }}
                                                    value={this.state.companyContactToAdd.phone}
                                                    style={style.companyContactTextField}
                                                />
                                                &nbsp;&nbsp;
                                                <TextValidator
                                                    id={"18"}
                                                    name="position"
                                                    floatingLabelText="Position"
                                                    hintText="Contact position"
                                                    onChange={this.handleChangeContactPositionField.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                    onKeyPress={(ev) => {
                                                        if (ev && ev.key === ENTER_KEY) {
                                                            this.handleAddContact(COMPANY_CONTACT_TYPES.CONTACT, ev);
                                                        }
                                                    }}
                                                    value={this.state.companyContactToAdd.position}
                                                    style={style.companyContactTextField}
                                                />
                                                <div style={{
                                                    float: 'right',
                                                    left: 15,
                                                    bottom: 40,
                                                    position: 'relative'
                                                }}>
                                                    {(this.state.companyContacts && this.state.companyContacts.length > 0) ? (() => {
                                                        return <FlatButton
                                                            label="Edit"
                                                            icon={<EditIcon/>}
                                                            onClick={this.handleOpenEditContactsArray.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                            style={{
                                                                color: 'white',
                                                                borderRadius: 6,
                                                                height: 40,
                                                                top: 13
                                                            }}
                                                            labelStyle={{fontSize: 13}}
                                                            backgroundColor={'rgba(0,0,0,0.7'}
                                                            hoverColor={'#0091ea'}

                                                        />;
                                                    })() : null}
                                                    <br/>
                                                    <FlatButton
                                                        type={"submit"}
                                                        label="Add"
                                                        labelStyle={{fontSize: 13}}
                                                        icon={<ActionAdd/>}
                                                        style={{top: 18, color: 'white', borderRadius: 6, height: 40}}
                                                        backgroundColor={'rgba(0,0,0,0.7'}
                                                        hoverColor={'#0091ea'}/>
                                                    <br/>
                                                    <span style={{
                                                        marginRight: '27px',
                                                        lineHeight: '36px',
                                                        float: 'right',
                                                        marginTop: 20,
                                                    }}>Total Contacts: {this.state.companyContacts.length}</span>
                                                </div>
                                            </ValidatorForm>
                                            <EditContactsModal
                                                open={this.state.openEditContactsModal}
                                                closeDialog={this.handleCloseEditContactsArray.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                                modalTitle={'Edit Company Contacts'}
                                                companyContacts={this.state.companyContacts}
                                                saveChanges={this.handleSaveContactsChanges.bind(this)}
                                            />
                                        </div>
                                    </div>
                                </StepContent>
                            </Step>
                            <Step style={style.stepStyle}>
                                <StepButton style={style.stepButtonStyle}
                                            onClick={this.handleStepActionsClick.bind(this, 1)}>
                                    <span style={style.stepButtonStyleLabel}>Domains</span>
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <div style={style.limitedSizeDiv}>
                                        <br/>
                                        <br/>
                                        <span style={{width: '100%'}}>Enter the domains you wish to review (that are related to this company).</span>
                                        <br/>

                                        <ChipInput
                                            hintText="Connected Domains"
                                            floatingLabelText="Connected Domains"
                                            value={this.state.companyFields.selectedDomains}
                                            filter={AutoComplete.fuzzyFilter}
                                            onPaste={(event) => {
                                                const clipboardText = event.clipboardData.getData('Text');

                                                event.preventDefault();
                                                let domainsArr = clipboardText.split('\n').filter((t) => t.length > 0);
                                                this.handleAddDomainToList(domainsArr);

                                                if (this.props.onPaste) {
                                                    this.props.onPaste(event);
                                                }
                                            }}
                                            style={{float: 'left', marginBottom: 40}}
                                            onBlur={(e) => {
                                                if (e.target.value) this.handleAddDomainToList(e.target.value);
                                            }}
                                            onRequestAdd={(chip) => this.handleAddDomainToList(chip)}
                                            onRequestDelete={(chip, index) => this.handleDeleteDomainFromList(chip, index)}
                                            maxSearchResults={5}
                                            fullWidth={true}
                                            fullWidthInput={true}/>
                                    </div>
                                    <div>
                                    <span style={{width: '100%', marginBottom: 20}}>What keywords would you like to use when searching for leaks?</span>
                                    <br/>
                                    <ChipInput
                                        hintText="Dataleaks Keywords"
                                        floatingLabelText="Dataleaks Keywords"
                                        value={this.state.companyFields.keywords}
                                        onBlur={(e) => {
                                            if (e.target.value) this.handleAddKeywordToList(e.target.value);
                                        }}
                                        onRequestAdd={(chip) => this.handleAddKeywordToList(chip)}
                                        onRequestDelete={(chip, index) => this.handleDeleteKeywordFromList(chip, index)}
                                        chipContainerStyle={{marginTop: 20}}
                                        fullWidth={true}
                                        fullWidthInput={true}
                                    />
                                </div>
                                    <br/>
                                    {isAdmin && <div>
                                        <span style={{width: '100%', marginBottom: 20}}>Add IP addresses related to the company.
                                            <br/>
                                            This is useful when you have specific IP addresses you want to assess.</span>
                                        <br/>
                                        <ChipInput
                                            hintText="IP addresses"
                                            floatingLabelText="IP addresses related to the company"
                                            value={this.state.companyFields.IPList}
                                            onBlur={(e) => {if (e.target.value) {this.handleAddIPToList(e.target.value)}}}
                                            onRequestAdd={(chip) => this.handleAddIPToList(chip)}
                                            onRequestDelete={(chip, index) => this.handleDeleteIPFromList(chip, index)}
                                            chipContainerStyle={{marginTop: 20}}
                                            fullWidth={true}
                                            fullWidthInput={true}
                                        />
                                    </div>
                                    }
                                    <br/>
                                </StepContent>
                            </Step>
                            <Step style={style.stepStyle}>
                                <StepButton style={style.stepButtonStyle}
                                            onClick={this.handleStepActionsClick.bind(this, 2)}>
                                    <span style={style.stepButtonStyleLabel}>Additional Information</span>
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <ChipInput
                                        hintText="Search Classification"
                                        floatingLabelText="Company Classification"
                                        menuStyle={style.autoCompleteStyle}
                                        filter={AutoComplete.caseInsensitiveFilter}
                                        value={this.state.companyClassifications}
                                        dataSource={COMPANY_INFO_CLASSIFICATION_TYPES}
                                        onRequestAdd={(chip) => this.handleAddChipInfoField('companyClassifications', chip)}
                                        onRequestDelete={(chip) => this.handleDeleteChipItemInfoField('companyClassifications', chip)}
                                        maxSearchResults={50}
                                        style={style.companyInfoTextField}
                                        openOnFocus={true}
                                        fullWidth={false}
                                        fullWidthInput={true}
                                    />
                                    <br/>
                                    <br/>
                                    <div>
                                        <TextField
                                            floatingLabelText="Company Condition"
                                            hintText={<Checkbox
                                                label="Example: This company signed an NDA"
                                                checked={true}
                                                disabled={true}
                                                style={style.infoHintCheckbox}
                                                labelStyle={style.infoHintCheckboxLabel}
                                            />}
                                            onChange={this.handleChangeInformationField.bind(this)}
                                            onKeyPress={(ev) => {
                                                if (ev && ev.key === ENTER_KEY) {
                                                    this.handleAddInformation(ev);
                                                }
                                            }}
                                            value={this.state.companyInformationToAdd}
                                            style={style.companyInfoTextField}
                                        />
                                        <div style={{float: 'right', bottom: 20, position: 'relative'}}>
                                            {(this.state.companyInformation && this.state.companyInformation.length > 0) ? (() => {
                                                return <FlatButton
                                                    label="Edit"
                                                    icon={<EditIcon/>}
                                                    onClick={this.handleOpenEditInformationArray.bind(this)}
                                                    style={{color: 'white', borderRadius: 6, height: 40, top: 13}}
                                                    backgroundColor={'rgba(0,0,0,0.7'}
                                                    labelStyle={{fontSize: 13}}
                                                    hoverColor={'#0091ea'}
                                                />;
                                            })() : null}
                                            <br/>
                                            <FlatButton
                                                label="Add"
                                                icon={<ActionAdd/>}
                                                onClick={this.handleAddInformation.bind(this)}
                                                style={{top: 18, color: 'white', borderRadius: 6, height: 40}}
                                                labelStyle={{fontSize: 13}}
                                                backgroundColor={'rgba(0,0,0,0.7'}
                                                hoverColor={'#0091ea'}
                                            />
                                        </div>
                                        <br/>
                                        <br/>
                                        <span
                                            style={{float: 'right'}}>Total company conditions: {this.state.companyInformation.length}</span>
                                        <EditInformationModal
                                            open={this.state.openEditInformationModal}
                                            closeDialog={this.handleCloseEditInformationArray.bind(this)}
                                            companyInformation={this.state.companyInformation}
                                            saveChanges={this.handleSaveInformationChanges.bind(this)}
                                        />
                                    </div>

                                    <ChipInput
                                        hintText="Search Sector"
                                        floatingLabelText="Company Sector"
                                        menuStyle={style.autoCompleteStyle}
                                        filter={AutoComplete.caseInsensitiveFilter}
                                        value={this.state.companySectors}
                                        dataSource={COMPANY_INFO_SECTOR_TYPES}
                                        onRequestAdd={(chip) => this.handleAddChipInfoField('companySectors', chip)}
                                        onRequestDelete={(chip) => this.handleDeleteChipItemInfoField('companySectors', chip)}
                                        maxSearchResults={50}
                                        style={{position: 'relative'}}
                                        openOnFocus={true}
                                        fullWidth={false}
                                        fullWidthInput={true}
                                    />
                                    <br/>
                                    <br/>
                                    <div>
                                        <span style={{}}>Date of contact: </span>
                                        <span style={{position: 'relative', bottom: 6}}>
                                        <DatePickerMain
                                            filterDataByDates={this.handleChangeInfoDateField.bind(this, 'companyDateOfContact')}
                                            disableDefaultDates={true}
                                            replaceDefaultDates={this.state.companyDateOfContact}
                                            buttonTooltip="Choose Dates"/>
                                        </span>
                                    </div>
                                    <span style={{}}>Date of Last Survey: </span>
                                    <DatePicker
                                        hintText="Choose Last Survey Date"
                                        container="inline"
                                        value={lastDateOfSurvey}
                                        onChange={this.handleChangeInfoSingleDateField.bind(this, 'companyDateOfSurvey')}
                                        style={style.singleDatePicker}
                                    />
                                    <br/>
                                    <br/>
                                </StepContent>
                            </Step>
                            <Step style={style.stepStyle}>
                                <StepButton style={style.stepButtonStyle}
                                            onClick={this.handleStepActionsClick.bind(this, 3)}>
                                    <span style={style.stepButtonStyleLabel}>{isAdmin ? 'Permissions' : 'Audit'}</span>
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <div>
                                        <br/>
                                        <br/>
                                        {isAdmin && <div>
                                            <span
                                                style={{width: '100%'}}>What users are allowed to view this company?</span>
                                            <br/>
                                            <ChipInput
                                                hintText="Allowed users (view)"
                                                floatingLabelText="Allowed users (view)"
                                                value={this.state.companyFields.allowedUsers}
                                                filter={AutoComplete.fuzzyFilter}
                                                dataSource={this.state.usersForDD}
                                                dataSourceConfig={{'text': 'email', 'value': 'id'}}
                                                openOnFocus={true}
                                                onRequestAdd={(chip) => this.handleAddAllowedUsersField(chip)}
                                                onRequestDelete={(chip, index) => this.handleDeleteAllowedUsersField(chip, index)}
                                                maxSearchResults={5}
                                            />
                                            <br/>
                                            <span
                                                style={{width: '100%'}}>What users are allowed to edit company details?</span>
                                            <br/>
                                            <ChipInput
                                                hintText="Allowed users (edit)"
                                                floatingLabelText="Allowed users (edit)"
                                                value={this.state.companyFields.allowedEditUsers}
                                                filter={AutoComplete.fuzzyFilter}
                                                dataSource={this.state.usersForDD}
                                                dataSourceConfig={{'text': 'email', 'value': 'id'}}
                                                openOnFocus={true}
                                                onRequestAdd={(chip) => this.handleAddAllowedEditUsersField(chip)}
                                                onRequestDelete={(chip, index) => this.handleDeleteAllowedUsersEditField(chip, index)}
                                                maxSearchResults={5}
                                            />
                                            <br/>
                                            <br/>
                                        </div>
                                        }
                                        <span style={{width: '100%'}}>Who is auditing the company?</span>
                                        <br/>
                                        <ValidatorForm
                                            ref="form"
                                            onSubmit={this.handleAddContact.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                            onError={errors => console.log(errors)}
                                            style={{marginBottom: 30}}
                                        >
                                            <TextValidator
                                                id={"11"}
                                                name={"email"}
                                                floatingLabelText="Email"
                                                hintText="Enter Email"
                                                validators={['required', 'isEmail']}
                                                errorMessages={['this field is required', 'email is not valid']}
                                                onChange={this.handleChangeContactEmailField.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.RESPONSIBLE, ev);
                                                    }
                                                }}
                                                value={this.state.companyResponsibleToAdd.email}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"12"}
                                                name={"name"}
                                                floatingLabelText="Name"
                                                hintText="Enter name"
                                                validators={['required']}
                                                errorMessages={['this field is required']}
                                                onChange={this.handleChangeContactNameField.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.RESPONSIBLE, ev);
                                                    }
                                                }}
                                                value={this.state.companyResponsibleToAdd.name}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"13"}
                                                name={"phone"}
                                                floatingLabelText="Phone"
                                                hintText="Enter phone"
                                                validators={['isNumber']}
                                                errorMessages={['numeric characters only']}
                                                onChange={this.handleChangeContactPhoneField.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.RESPONSIBLE, ev);
                                                    }
                                                }}
                                                value={this.state.companyResponsibleToAdd.phone}
                                                style={style.companyContactTextField}
                                            />
                                            &nbsp;&nbsp;
                                            <TextValidator
                                                id={"14"}
                                                name={"position"}
                                                floatingLabelText="Position"
                                                hintText="Enter position"
                                                onChange={this.handleChangeContactPositionField.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                onKeyPress={(ev) => {
                                                    if (ev && ev.key === ENTER_KEY) {
                                                        this.handleAddContact(COMPANY_CONTACT_TYPES.RESPONSIBLE, ev);
                                                    }
                                                }}
                                                value={this.state.companyResponsibleToAdd.position}
                                                style={style.companyContactTextField}
                                            />
                                            <div style={{float: 'right', left: 15, bottom: 20, position: 'relative'}}>
                                                {(this.state.companyResponsibles && this.state.companyResponsibles.length > 0) ? (() => {
                                                    return <FlatButton
                                                        label="Edit"
                                                        icon={<EditIcon/>}
                                                        onClick={this.handleOpenEditContactsArray.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                        style={{color: 'white', borderRadius: 6, height: 40, top: 13}}
                                                        backgroundColor={'rgba(0,0,0,0.7'}
                                                        labelStyle={{fontSize: 13}}
                                                        hoverColor={'#0091ea'}
                                                    />;
                                                })() : null}
                                                <br/>
                                                <FlatButton
                                                    type="submit"
                                                    label="Add"
                                                    icon={<ActionAdd/>}
                                                    //onClick={this.handleAddContact.bind(this, COMPANY_CONTACT_TYPES.RESPONSIBLE)}
                                                    style={{top: 18, color: 'white', borderRadius: 6, height: 40}}
                                                    backgroundColor={'rgba(0,0,0,0.7'}
                                                    labelStyle={{fontSize: 13}}
                                                    hoverColor={'#0091ea'}
                                                />
                                                <br/>
                                                <span style={{
                                                    marginRight: '27px',
                                                    lineHeight: '36px',
                                                    float: 'right',
                                                    marginTop: 20,
                                                }}>Total Auditors: {this.state.companyResponsibles.length}</span>
                                            </div>
                                        </ValidatorForm>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <EditContactsModal
                                            open={this.state.openEditContactsModal}
                                            closeDialog={this.handleCloseEditContactsArray.bind(this, COMPANY_CONTACT_TYPES.CONTACT)}
                                            companyResponsibles={this.state.companyResponsibles}
                                            //companyContacts={this.state.companyContacts}
                                            saveChanges={this.handleSaveContactsChanges.bind(this)}
                                            modalTitle={'Edit Company Auditors'}

                                        />
                                    </div>
                                </StepContent>
                            </Step>
                            <Step style={style.stepStyle}>
                                <StepButton style={style.stepButtonStyle}
                                            onClick={this.handleStepActionsClick.bind(this, 4)}>
                                    <span style={style.stepButtonStyleLabel}>Preferences</span>
                                </StepButton>
                                <StepContent style={style.stepContentStyle}>
                                    <br/>
                                    <span style={{width: '100%'}}>What is the ratio between the intel score to the survey score?</span>
                                    <br/>
                                    <TextField
                                        id={'2'}
                                        floatingLabelText="Intel Score Weight"
                                        onChange={this.handleChangeIntelRatioField.bind(this)}
                                        style={style.intelScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={(this.state.companyFields.ratios && this.state.companyFields.ratios.intelWeight != null) ?
                                            this.state.companyFields.ratios.intelWeight : '50'}
                                    />
                                    <TextField
                                        id={'3'}
                                        floatingLabelText="Survey Score Weight"
                                        onChange={this.handleChangeSurveyRatioField.bind(this)}
                                        style={style.SurveyScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={(this.state.companyFields.ratios && this.state.companyFields.ratios.surveyWeight != null) ?
                                            this.state.companyFields.ratios.surveyWeight : '50'}
                                    />
                                    <br/><br/>
                                    <span
                                        style={{width: '100%'}}>What is the weight ratio between the following intel score categories?</span>
                                    <br/>
                                    <TextField
                                        id={'4'}
                                        floatingLabelText="CVE Weight(%)"
                                        onChange={this.handleChangeIntelScoreRatioField.bind(this, 'cveWeight')}
                                        style={style.intelPartScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={this.state.companyFields.intelScoreRatios.cveWeight}
                                        ref={(input) => {
                                            this.cveInput = input;
                                        }}
                                    />
                                    <TextField
                                        id={'5'}
                                        floatingLabelText="DNS Weight(%)"
                                        data-tip="SPF + DMARC"
                                        onChange={this.handleChangeIntelScoreRatioField.bind(this, 'dnsWeight')}
                                        style={style.intelPartScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={this.state.companyFields.intelScoreRatios.dnsWeight}
                                    />
                                    <TextField
                                        id={'6'}
                                        floatingLabelText="S3 Buckets Weight(%)"
                                        onChange={this.handleChangeIntelScoreRatioField.bind(this, 'bucketsWeight')}
                                        style={style.intelPartScoreWideField}
                                        underlineStyle={style.underlineStyle}
                                        value={this.state.companyFields.intelScoreRatios.bucketsWeight}
                                    />
                                    <TextField
                                        id={'7'}
                                        floatingLabelText="Breaches Weight(%)"
                                        onChange={this.handleChangeIntelScoreRatioField.bind(this, 'breachesWeight')}
                                        style={style.intelPartScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={this.state.companyFields.intelScoreRatios.breachesWeight}
                                    />
                                    <TextField
                                        id={'7'}
                                        floatingLabelText="Dataleaks Weight(%)"
                                        onChange={this.handleChangeIntelScoreRatioField.bind(this, 'dataleaksWeight')}
                                        style={style.intelPartScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={this.state.companyFields.intelScoreRatios.dataleaksWeight}
                                    />
                                    <TextField
                                        id={'8'}
                                        floatingLabelText="Blacklists Weight(%)"
                                        onChange={this.handleChangeIntelScoreRatioField.bind(this, 'blacklistsWeight')}
                                        style={style.intelPartScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={this.state.companyFields.intelScoreRatios.blacklistsWeight}
                                    />
                                    <TextField
                                        id={'9'}
                                        floatingLabelText="Botnets Weight(%)"
                                        onChange={this.handleChangeIntelScoreRatioField.bind(this, 'botnetsWeight')}
                                        style={style.intelPartScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={this.state.companyFields.intelScoreRatios.botnetsWeight}
                                    />
                                    <TextField
                                        id={'10'}
                                        floatingLabelText="Https Weight(%)"
                                        onChange={this.handleChangeIntelScoreRatioField.bind(this, 'sslCertsWeight')}
                                        style={style.intelPartScoreField}
                                        underlineStyle={style.underlineStyle}
                                        value={this.state.companyFields.intelScoreRatios.sslCertsWeight}
                                    />
                                    <ReactTooltip place="bottom"/>
                                    <br/>
                                    <br/>
                                    <span style={{width: '100%'}}>How sensitive is the company being reviewed?</span>
                                    <br/>
                                    <DropDownMenu
                                        disabled={this.state.isQuestionnaireActive}
                                        value={this.state.companyFields.sensitivity || 2}
                                        onChange={this.onChangeSensitivityDropdown.bind(this)}
                                        autoWidth={false}
                                        labelStyle={{left: 14}}
                                        style={{right: 35, width: 310}}
                                        anchorOrigin={{
                                            horizontal: 'left',
                                            vertical: 'bottom'
                                        }}>
                                        <MenuItem value={1} primaryText="Low Sensitivity"/>
                                        <MenuItem value={2} primaryText="Moderate Sensitivity"/>
                                        <MenuItem value={3} primaryText="High Sensitivity"/>
                                        <MenuItem value={4} primaryText="Very High Sensitivity"/>
                                    </DropDownMenu>
                                    <IconButton style={{position:'relative', bottom: 6}} touch={true}
                                                tooltip={questionnaireBtnText} tooltipPosition="bottom-right">
                                        <QuestionnaireIcon  color={questionnaireIconColor}
                                            onTouchTap={this.handleQuestionnaireBtn.bind(this)}
                                        />
                                    </IconButton>
                                    <span style={{position:'relative', left: 40}}>{this.state.finalSensitivityLevelText}</span>
                                    <br/>
                                    <br/>
                                    {isAdmin && <span style={{width: '100%'}}>What is the company scan priority?</span>}
                                    <br/>
                                    {isAdmin && <SelectField
                                        value={scanPriority}
                                        onChange={this.handleChangeScanPriority}
                                        maxHeight={200}
                                    >
                                        {items}
                                    </SelectField>}
                                </StepContent>
                            </Step>
                        </Stepper> :
                                <div className="scrollableDiv" style={{
                                    'overflow': 'auto',
                                    'maxHeight': '800px',
                                    background: '#efefef',
                                    borderRadius: 10,
                                    border: '1px solid',
                                    borderColor: '#f5f5f5',
                                    padding: '20px 20px 0px 25px',
                                    marginTop: 30,
                                }}>
                                    <SensitivityQuestionnaire
                                        updateSensitivity={this.handleSensitivityQuestionnaireFinish.bind(this)}
                                        handleCancel={this.handleSensitivityQuestionnaireCancel.bind(this)}/>
                                </div>
                        }
                        <YesNoModal
                            open={this.state.openYesNoModal}
                            title={this.state.titleYesNoModal}
                            message={this.state.messageYesNoModal}
                            paramObject={this.state.paramObjectYesNoModal}
                            getUserResult={this.getYesNoModalResult.bind(this)}
                        />
                    </div>
                </Dialog>
            </MuiThemeProvider>
        );
    }
};
