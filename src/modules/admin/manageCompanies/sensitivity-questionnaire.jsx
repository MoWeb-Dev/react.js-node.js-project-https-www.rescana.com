import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Divider from 'material-ui/Divider';
import '../../../../public/css/survey.css';
import themeForFont from '../../app/themes/themeForFont';
import FlatButton from 'material-ui/FlatButton';
import questions from './questions-sensitivity.js';

export default class SensitivityQuestionnaire extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            questionnaireResults: [],
            finalSensitivityLevel: '',
            questionAmount: 0
        };

        this.handleFinish = this.handleFinish.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.updateResults = this.updateResults.bind(this);
        this.getSensitivityLevelByResults = this.getSensitivityLevelByResults.bind(this);
    }

    componentWillMount() {
        let questionnaireResults = ['notAnswered', 'notAnswered', 'notAnswered', 'notAnswered',
            'notAnswered', 'notAnswered', 'notAnswered', 'notAnswered', 'notAnswered'];
        let questionAmount = questionnaireResults.length;
        this.setState({questionnaireResults: questionnaireResults, questionAmount: questionAmount})

    }

    updateResults(questionnaireResults) {
        this.setState({questionnaireResults: questionnaireResults});

    }

    handleFinish() {
        let isAnsweredAllFlag = true;
        let questionnaireResults = this.state.questionnaireResults;
        let questionAmount = this.state.questionAmount;

        if (questionnaireResults && questionAmount > 0) {
            questionnaireResults.map((res) => {
                if (res === 'notAnswered') {
                    isAnsweredAllFlag = false;
                }
            });
            if (!isAnsweredAllFlag) {
                app.addAlert('error', 'Please answer all the questions to determine sensitivity level!');
            } else {
                this.getSensitivityLevelByResults(questionnaireResults, questionAmount);
            }
        }
    }

    handleCancel() {
        this.props.handleCancel();
    }


    getSensitivityLevelByResults(questionnaireResults, questionAmount) {
        if (questionnaireResults && Array.isArray(questionnaireResults) && questionnaireResults.length > 0 &&
            questionAmount && questionAmount > 0) {

            let impactScore = 1;
            for (let i = 0; i < 3; i++) {
                if (questionnaireResults[i] >= impactScore) {
                    impactScore = questionnaireResults[i];
                }
            }

            let sum = 0;
            for (let j = 3; j < 9; j++) {
                sum += questionnaireResults[j];
            }

            let probabilityScore = sum / 6;

            let riskScore = Math.floor((impactScore * 3) + probabilityScore);

            let low = [3, 4, 8], moderate = [9, 10, 11, 12], high = [6, 7, 13, 14, 15, 16], veryHigh = [17, 18, 19];

            let finalSensitivityLevel = low.includes(riskScore) ? 1 : moderate.includes(riskScore) ? 2 :
                high.includes(riskScore) ? 3 : veryHigh.includes(riskScore) ? 4 : null;

            if (finalSensitivityLevel > 0 && finalSensitivityLevel < 5) {
                this.props.updateSensitivity(finalSensitivityLevel, this.getSensitivityLevelString(finalSensitivityLevel));
            } else {
                app.addAlert('error', 'Failed to determine sensitivity level!');
            }
        }
    }

    getSensitivityLevelString(finalSensitivityLevel) {
        let text = '';
        if (finalSensitivityLevel && typeof finalSensitivityLevel === 'number' && finalSensitivityLevel > 0 && finalSensitivityLevel < 5) {
            if (finalSensitivityLevel === 1) {
                text = 'Questionnaire Result: Low Sensitivity';
            } else if (finalSensitivityLevel === 2) {
                text = 'Questionnaire Result: Moderate Sensitivity';
            } else if (finalSensitivityLevel === 3) {
                text = 'Questionnaire Result: High Sensitivity';
            } else if (finalSensitivityLevel === 4) {
                text = 'Questionnaire Result: Very High Sensitivity';
            }
            return text;
        }
    }


    render() {

        const questionnaireText = questions.questionsArr;
        const questionnaire = questionnaireText.map((curItem, index) => {
            return (
                <QuestionItem
                    question={curItem.question}
                    answers={curItem.answers}
                    index={index}
                    key={index}
                    questionnaireResults={this.state.questionnaireResults}
                    updateResults={this.updateResults.bind(this)}
                />
            )
        });


        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <br/>
                    <div style={{textAlign: 'center' , color:'black', fontSize: 22, padding: 15}}>Risk Analysis Questionnaire</div><br/><br/>
                    <div style={{'margin': '10px'}}>Define the initial score of the vendor by answering the following questionnaire:</div><br/>
                    {questionnaire}
                    <Divider/><br/><br/>
                    <FlatButton
                        label="Cancel"
                        onClick={this.handleCancel.bind(this)}
                        style={{float: 'right', bottom: 20, right: 30, color: 'white', borderRadius: 4, height: 40}}
                        labelStyle={{textTransform: 'capitalize', fontSize: 13}}
                        backgroundColor={'rgba(0,0,0,0.7'}
                        hoverColor={'#0091ea'}/>
                    <FlatButton
                        label="Done"
                        onClick={this.handleFinish.bind(this)}
                        style={{float: 'right', bottom: 20, right: 30, color: 'white', borderRadius: 4, height: 40, marginRight: 5}}
                        labelStyle={{textTransform: 'capitalize', fontSize: 13}}
                        backgroundColor={'rgba(0,0,0,0.7'}
                        hoverColor={'#0091ea'}/>
                </div>
            </MuiThemeProvider>
        );
    }


}


class QuestionItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            questionnaireResults: this.props.questionnaireResults,
        };

        this.onChange = this.onChange.bind(this);
        this.handleAnswerBtnClick = this.handleAnswerBtnClick.bind(this);
    }


    onChange(questionIdx, e) {
        e.preventDefault();
        let questionnaireResults = this.props.questionnaireResults;
        questionnaireResults[questionIdx] = this.state.answerIdx + 1;
        this.props.updateResults(questionnaireResults);

    }

    handleAnswerBtnClick(answerIdx, e) {
        e.preventDefault();
        this.setState({answerIdx: answerIdx})
    }

    render() {
        let style = {
            radioBtn: {'marginBottom': '10px'},
            radioBtnGrp: {'margin': '10px'},
            question: {'margin': '10px'}
        };


        let answers = this.props.answers.map((curAnswer, index) => {
            return (
                <RadioButton
                    key={index}
                    value={index}
                    label={curAnswer}
                    style={style.radioBtn}
                    onTouchTap={this.handleAnswerBtnClick.bind(this, index)}
                />
            )
        });

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div style={style.question}>
                    <br/>
                    <div>{this.props.question}</div>
                    <RadioButtonGroup style={style.radioBtnGrp}
                                      onChange={this.onChange.bind(this, this.props.index)}
                                      name="questionsRadioGroup">
                        {answers}
                    </RadioButtonGroup>
                    <br/>
                </div>
            </MuiThemeProvider>
        );
    }
}
