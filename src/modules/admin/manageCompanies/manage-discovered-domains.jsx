import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import themeForFont from '../../app/themes/themeForFont';
import DataTableTemplate from '../../common/datatableTemplate/datatable-template.jsx';
import {DISCOVERED_DOMAINS_TYPE} from '../../../../config/consts.js';
import Language from 'material-ui/svg-icons/action/language';
import Toggle from 'material-ui/Toggle';
import ReactTooltip from 'react-tooltip';

export default class ManageDiscoveredDomainsModal extends React.Component {
    constructor(props) {
        super(props);

        const wrappableStyle = {
            whiteSpace: 'pre-wrap',
            wordBreak: 'break-word'
        };

        this.state = {
            tableColumns: [
                {
                    sortable: true,
                    label: 'Discovered Domain',
                    key: 'domain',
                    style: wrappableStyle,
                    noneSearchable: true
                }, {
                    sortable: true,
                    label: 'Source',
                    key: 'source',
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Connection Level',
                    key: 'Connection_Level',
                    style: wrappableStyle,
                    noneSearchable: true
                }, {
                    sortable: true,
                    label: 'Connection Strength',
                    key: 'Connection_Strength',
                    style: wrappableStyle,
                    noneSearchable: true
                }, {
                    sortable: false,
                    label: 'whois',
                    key: 'whois',
                    style: wrappableStyle,
                    noneSearchable: true
                }, {
                    label: 'Validated',
                    key: 'validate',
                    style: wrappableStyle,
                    noneSearchable: true
                }
            ],
            tableColumnsToDownload: [
                {
                    sortable: true,
                    label: 'Discovered Domain',
                    key: 'domain',
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Source',
                    key: 'source',
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Connection Level',
                    key: 'Connection_Level',
                    style: wrappableStyle
                }, {
                    sortable: true,
                    label: 'Connection Strength',
                    key: 'Connection_Strength',
                    style: wrappableStyle
                }
            ],
            openWhoisModal: false
        };

        this.setResults = this.setResults.bind(this);
    }

    setResults() {
        const tableData = [];
        if (this.props.manageDomain && Array.isArray(this.props.manageDomain)) {
            this.props.manageDomain.map((currDiscoveredData, currIndex) => {
                if (currDiscoveredData && currDiscoveredData.domain && currDiscoveredData.source) {
                    let connectionStrength = 0;
                    if (currDiscoveredData && currDiscoveredData.connectionStrength) {
                        if (currDiscoveredData.connectionStrength.low || currDiscoveredData.connectionStrength.low === 0) {
                            connectionStrength = currDiscoveredData.connectionStrength.low;
                        } else if (currDiscoveredData.connectionStrength || currDiscoveredData.connectionStrength === 0) {
                            connectionStrength = currDiscoveredData.connectionStrength;
                        }
                    }
                    let ConnectionLevel = 0;
                    if (currDiscoveredData && currDiscoveredData.Connection_Level) {
                        if (currDiscoveredData.Connection_Level.low || currDiscoveredData.Connection_Level.low === 0) {
                            ConnectionLevel = currDiscoveredData.Connection_Level.low;
                        } else if (currDiscoveredData.Connection_Level || currDiscoveredData.Connection_Level === 0) {
                            ConnectionLevel = currDiscoveredData.Connection_Level;
                        }
                    }

                    tableData.push({
                        source: currDiscoveredData && currDiscoveredData.source,
                        domain: currDiscoveredData && <a href={'http://' + currDiscoveredData.domain} style={{textDecoration: 'none'}}
                                                         target="_blank">{currDiscoveredData.domain}</a>,
                        Connection_Strength: connectionStrength,
                        Connection_Level: ConnectionLevel,
                        whois: <span><a data-tip="Who.is" style={{marginRight:'10px'}} href={'https://who.is/whois/' + currDiscoveredData.domain} target="_blank" ><Language/></a>
                            <a data-tip="Godaddy" href={'https://il.godaddy.com/en/whois/results.aspx?domain=' + currDiscoveredData.domain} target="_blank"><Language/></a><ReactTooltip place="right"/></span>,
                        validate: <Toggle
                            labelPosition="right"
                            onToggle={this.toggleValidation.bind(this, currDiscoveredData.domain, currDiscoveredData.validated, currIndex)}
                            toggled={currDiscoveredData.validated}
                        />
                    });
                }
            });
            return tableData;
        }
    }

    toggleValidation(domain, validated, currIndex) {
        let strength;
        if (validated) {
            strength = 0;
        } else {
            strength = 100;
        }

        this.props.validateDomain(domain, strength, currIndex);
    }

    openWhoisModal(whois) {
        this.setState({openWhoisModal: true, currentWhois: whois});
    }

    closeWhoisModal() {
        this.setState({openWhoisModal: false});
    }

    render() {
        let tableData = [];
        let noDataToShowFlag = false;
        if (this.props && this.props.openManageDiscoveredDomainsModal) {
            tableData = this.setResults();

            // If all discoveredDomains were deleted - close the modal.
            if (tableData.length === 0) {
                noDataToShowFlag = true;
            }
        }

        const customContentStyle = {
            width: '80%',
            maxWidth: 'none',
            borderRadius: '7px 7px 7px 7px'
        };

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                    <Dialog
                        title="Manage Discovered Domains"
                        actions={[
                            <FlatButton
                                label="Done"
                                primary={true}
                                style={{color: '#0091ea', marginBottom: '5px'}}
                                keyboardFocused={true}
                                onTouchTap={this.props.cancel.bind(this)}
                            />
                        ]}
                        contentStyle={customContentStyle}
                        open={this.props.openManageDiscoveredDomainsModal}
                        onRequestClose={this.props.cancel.bind(this)}
                        repositionOnUpdate={false}
                        actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                        bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                        autoScrollBodyContent={true}
                        modal={true}
                        titleStyle={{
                            fontSize: 18,
                            background: 'rgba(0,0,0,0.7)',
                            color: 'white', textAlign: 'center',
                            borderRadius: '2px 2px 0px 0px',
                            textTransform: 'uppercase',
                        }}
                    >
                        <div>
                            {noDataToShowFlag?
                                <div style={{margin: '30px 0 30px 0', textAlign: 'center'}}>No Data Found</div>
                                :
                                <DataTableTemplate
                                    myDataType={DISCOVERED_DOMAINS_TYPE}
                                    tableColumns={this.state.tableColumns}
                                    tableData={tableData}
                                    tableDataToDownload={tableData}
                                    tableColumnsToDownload={this.state.tableColumnsToDownload}
                            />}
                        </div>
                    </Dialog>
                    <Dialog
                        title="WHOIS"
                        open={this.state.openWhoisModal}
                        style={{"white-space": "pre-wrap"}}
                        autoScrollBodyContent={true}
                        actions={[
                            <FlatButton
                                label="Done"
                                primary={true}
                                style={{color: '#0091ea', marginBottom: '5px'}}
                                keyboardFocused={true}
                                onTouchTap={this.closeWhoisModal.bind(this)}
                            />
                        ]}>
                        <div>
                            {this.state.currentWhois}
                        </div>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
};
