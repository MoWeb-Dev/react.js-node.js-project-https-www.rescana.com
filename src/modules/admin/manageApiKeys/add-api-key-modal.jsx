import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import Checkbox from 'material-ui/Checkbox';

export default class AddApiKeyModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            keyInput: '',
            organizationsInput: [],
            organizations: this.props.organizations,
            isScoreBackwardsEnabled: false
        };
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps) {
            const stateToUpdate = {
                keyInput: '',
                organizationsInput: []
            };
            if(nextProps.hasOwnProperty('open')) {
                stateToUpdate.open = nextProps.open;
            }
            if(nextProps.hasOwnProperty('organizations')) {
                stateToUpdate.organizations = nextProps.organizations;
            }
            if(nextProps.editKeyData && nextProps.editKeyData.key) {
                stateToUpdate.keyInput = '' + nextProps.editKeyData.key;
                stateToUpdate.isEditMode = true;
                stateToUpdate.isScoreBackwardsEnabled = nextProps.editKeyData.isScoreBackwardsEnabled || false;
                if(nextProps.editKeyData.organizations && Array.isArray(nextProps.editKeyData.organizations)) {
                    // This is to duplicate data so changed results won't be displayed on UI while canceled.
                    stateToUpdate.organizationsInput = [].concat(nextProps.editKeyData.organizations);
                }
            }
            this.setState(stateToUpdate);
        }
    }

    handleUpdateBackwardsCheckBox() {
        this.setState({isScoreBackwardsEnabled: !this.state.isScoreBackwardsEnabled});

    }

    handleChangeKeyField(e) {
        if(e && e.target && e.target.hasOwnProperty("value")) {
            this.setState({keyInput: e.target.value});
        }
    }

    handleAddOrganizationToArray(chip, dataSource) {
        if (chip && chip.organizationName && dataSource && Array.isArray(dataSource)) {
            if(this.state.organizationsInput.length >= 1) {
                app.addAlert('error', 'Only one organization is allowed per API Key');
            }
            else {
                let matchedInput = dataSource.find((t) => {
                    let lowerCaseName = t;
                    if (lowerCaseName && lowerCaseName.organizationName) {
                        lowerCaseName = lowerCaseName.organizationName.toString().toLowerCase();
                    }
                    return lowerCaseName === chip.organizationName.toString().toLowerCase();
                });

                if (matchedInput && !this.state.organizationsInput.find((o) => {
                    return (o && o.organizationName && o.organizationName === matchedInput.organizationName);
                })) {
                    const stateToUpdate = {};
                    stateToUpdate.organizationsInput = this.state.organizationsInput;
                    stateToUpdate.organizationsInput.push(matchedInput);
                    this.setState(stateToUpdate);
                }
            }
        }
    }

    handleDeleteOrganizationFromArray(chipToDelete, chipIndexToDelete) {
        if(chipIndexToDelete != null && chipIndexToDelete > -1) {
            const stateToUpdate = {};
            stateToUpdate.organizationsInput = this.state.organizationsInput;
            stateToUpdate.organizationsInput.splice(chipIndexToDelete, 1);
            this.setState(stateToUpdate);
        }
    }

    render() {
        const styles = {
            autoCompleteStyle: {
                overflowY: 'auto',
                height: '200px',
                maxHeight: '200px'
            },
            wideTextBox: {
                width: '65%'
            }
        };

        const organizationsForDD = this.state.organizations.map((item) => {
            return {organizationName: item.organizationName, id: item._id};
        });

        const title = (this.state.isEditMode) ? 'Editing API Key' : 'Add New API Key';

        return (
            <Dialog
                title={title}
                actions={[
                    <FlatButton
                        label="Done"
                        primary={true}
                        keyboardFocused={true}
                        onTouchTap={this.props.done.bind(this, this.state.keyInput, this.state.organizationsInput, this.state.isScoreBackwardsEnabled)}
                    />,
                    <FlatButton
                        label="Cancel"
                        primary={true}
                        keyboardFocused={true}
                        onTouchTap={this.props.cancel.bind(this)}
                    />
                ]}
                autoScrollBodyContent={true}
                modal={false}
                open={this.state.open}
                onRequestClose={this.props.cancel.bind(this)}
            >
                <form>
                    {(this.state.isEditMode) ? (
                        <TextField
                            floatingLabelText="API Key"
                            onChange={this.handleChangeKeyField.bind(this)}
                            value={this.state.keyInput}
                            disabled={this.state.isEditMode}
                            style={styles.wideTextBox}
                        />
                    ) : null
                    }
                    <br/>
                    <br/>
                    <ChipInput
                        floatingLabelText="The Organization related to this key"
                        hintText="Search here for an organization"
                        value={this.state.organizationsInput}
                        menuStyle={styles.autoCompleteStyle}
                        filter={AutoComplete.fuzzyFilter}
                        onRequestAdd={(chip) => this.handleAddOrganizationToArray(chip, organizationsForDD)}
                        onRequestDelete={(chip, index) => this.handleDeleteOrganizationFromArray(chip, index)}
                        dataSource={organizationsForDD}
                        dataSourceConfig={{'text': 'organizationName', 'value': 'id'}}
                        maxSearchResults={35}
                        openOnFocus={true}
                    />
                    <br/>
                    <br/>
                    <Checkbox
                        label="Display Score Backwards (Higher score means lower risk)"
                        checked={this.state.isScoreBackwardsEnabled}
                        onCheck={this.handleUpdateBackwardsCheckBox.bind(this)}
                        style={styles.space}
                    />
                </form>
            </Dialog>
        );
    }
}
