import React from 'react';
import IconButton from 'material-ui/IconButton';
import PlayIcon from 'material-ui/svg-icons/av/play-circle-filled';
import PauseIcon from 'material-ui/svg-icons/av/pause-circle-filled';
import DeleteIcon from 'material-ui/svg-icons/action/delete-forever';
import EditIcon from 'material-ui/svg-icons/image/edit';
import {showExpandedDataChip} from '../../common/UI-Helpers.jsx';
import {API_KEY_STATUS} from '../../../../config/consts.js';
import DeleteDialog from '../../app/delete-dialog.jsx';
import AddApiKeyModal from './add-api-key-modal.jsx';
import DataTableViewTemplate from '../../common/datatableTemplate/datatable-template.jsx';

export default class apiKeysTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            apiKeys: this.props.apiKeys || [],
            organizations: this.props.organizations || [],
            openDeleteDialog: false,
            openEditApiKeyModal: false
        };

        this.displayOrganizations = this.displayOrganizations.bind(this);
        this.IsKeyActive = this.IsKeyActive.bind(this);
        this.handleOpenDeleteDialog = this.handleOpenDeleteDialog.bind(this);
        this.handleCloseDeleteDialog = this.handleCloseDeleteDialog.bind(this);
        this.handleDeleteApiKey = this.handleDeleteApiKey.bind(this);
        this.handleOpenEditApiKeyDialog = this.handleOpenEditApiKeyDialog.bind(this);
        this.handleCloseEditApiKeyDialog = this.handleCloseEditApiKeyDialog.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps) {
            const stateToUpdate = {};
            if(nextProps.apiKeys) {
                stateToUpdate.apiKeys = nextProps.apiKeys;
            }
            if(nextProps.organizations) {
                stateToUpdate.organizations = nextProps.organizations;
            }
            this.setState(stateToUpdate);
        }
    }

    displayOrganizations(keyData) {
        let result;
        if(keyData && keyData.organizations && Array.isArray(keyData.organizations)) {
            const orgNames = [];
            keyData.organizations.map((o) => {
                if (o && o.organizationName && !orgNames.includes(o.organizationName)) {
                    orgNames.push(o.organizationName);
                }
            });
            if(orgNames.length > 0) {
                result = showExpandedDataChip(orgNames, 5, 5, 'organizations');
            }
            else {
                result = '';
            }
        }
        else {
            result = '';
        }
        return result;
    }

    IsKeyActive(keyData) {
        return !!(keyData && keyData.status && keyData.status === API_KEY_STATUS.ACTIVE);
    }

    handleOpenDeleteDialog(keyData) {
        if(!this.state.openDeleteDialog && keyData && keyData.key) {
            this.setState({openDeleteDialog: true, currKeyToDelete: keyData.key});
        }
    };

    handleCloseDeleteDialog() {
        if(this.state.openDeleteDialog) {
            this.setState({openDeleteDialog: false, currKeyToDelete: null});
        }
    };

    handleOpenEditApiKeyDialog(keyData) {
        if(!this.state.openEditApiKeyModal && keyData && keyData.key) {
            this.setState({openEditApiKeyModal: true, editKeyData: keyData});
        }
    };

    handleCloseEditApiKeyDialog() {
        if(this.state.openEditApiKeyModal) {
            this.setState({openEditApiKeyModal: false, editKeyData: null});
        }
    };

    handleDeleteApiKey(keyToDelete, e) {
        if(e && e.preventDefault) {
            e.preventDefault();
        }
        this.handleCloseDeleteDialog();
        if(keyToDelete) {
            this.props.deleteApiKey(keyToDelete);
        }
    }

    handleDoneEditKey(keyInput, organizationsInput, isScoreBackwardsEnabled = false, e) {
        if(e && e.preventDefault) {
            e.preventDefault();
        }
        if(keyInput && organizationsInput && Array.isArray(organizationsInput) && organizationsInput.length > 0) {
            this.props.handleDoneEditKey(keyInput, organizationsInput, isScoreBackwardsEnabled, e);
            this.handleCloseEditApiKeyDialog();
        }
        else {
            app.addAlert('warning', 'Some fields are missing');
        }
    }

    render() {
        const styles = {
            chip: {
                margin: 4
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap'
            },
            tableStyle: {
                paddingTop: '8px',
                paddingBottom: '8px'
            },
            wrappedTableStyle: {
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-all',
                paddingTop: '8px',
                paddingBottom: '8px'
            },
            smallTableStyle: {
                paddingTop: '8px',
                paddingBottom: '8px',
                width: '15%'
            },
            wrappableStyle: {
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-word'
            }
        };

        const tableColumns = [
            {
                sortable: true,
                label: 'Key',
                key: 'key'
            },
            {
                label: 'Organization',
                key: 'organization',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                sortable: true,
                label: 'Status',
                key: 'status',
                style: styles.wrappableStyle
            },
            {
                label: 'Suspend / Activate',
                key: 'suspendOrActivate',
                noneSearchable: true
            },
            {
                label: 'Edit / Delete',
                key: 'editOrDelete',
                noneSearchable: true
            }
        ];

        const tableData = [];
        this.state.apiKeys.map((currApiKey) => {
            tableData.push({
                key: (currApiKey && currApiKey.key) || '',
                organization: this.displayOrganizations(currApiKey) || '',
                status: (currApiKey && currApiKey.status) || '',
                suspendOrActivate: <div>
                    <IconButton style={{marginLeft: '-20px'}}
                                disabled={!this.IsKeyActive(currApiKey)}
                                onClick={this.props.updateApiKeyStatus.bind(this, currApiKey, API_KEY_STATUS.SUSPENDED)}
                                data-tip="Suspend" touch={true}
                                name="suspendApiKey">
                        <PauseIcon/>
                    </IconButton>
                    <IconButton style={{marginLeft: '0px'}}
                                disabled={this.IsKeyActive(currApiKey)}
                                onClick={this.props.updateApiKeyStatus.bind(this, currApiKey, API_KEY_STATUS.ACTIVE)}
                                data-tip="Activate" touch={true}
                                name="activateApiKey">
                        <PlayIcon/>
                    </IconButton>
                </div>,
                editOrDelete: <div>
                    <IconButton style={{marginLeft: '-20px'}}
                                onClick={this.handleOpenEditApiKeyDialog.bind(this, currApiKey)}
                                data-tip="Edit" touch={true}
                                name="editApiKey">
                        <EditIcon/>
                    </IconButton>
                    <IconButton style={{marginLeft: '0px'}}
                                onClick={this.handleOpenDeleteDialog.bind(this, currApiKey)}
                                data-tip="Delete" touch={true}
                                name="deleteApiKey">
                        <DeleteIcon/>
                    </IconButton>
                </div>
            });
        });

        return (
            <div>
                <DataTableViewTemplate
                    myDataType={'adminApiKeys'}
                    tableColumns={tableColumns}
                    tableData={tableData}
                    hideDownloadExcel={true}
                    hideCardBackground={true}
                />
                <DeleteDialog
                    open={this.state.openDeleteDialog}
                    onRequestClose={this.handleCloseDeleteDialog.bind(this)}
                    onDelete={this.handleDeleteApiKey.bind(this, this.state.currKeyToDelete)}
                />
                <AddApiKeyModal
                    open={this.state.openEditApiKeyModal}
                    cancel={this.handleCloseEditApiKeyDialog.bind(this)}
                    done={this.handleDoneEditKey.bind(this)}
                    organizations={this.state.organizations}
                    editKeyData={this.state.editKeyData}
                />
            </div>
        );
    }
}
