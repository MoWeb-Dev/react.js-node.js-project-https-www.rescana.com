import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import copy from 'copy-to-clipboard';

export default class DisplayNewApiKeyModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            keyToDisplay: ''
        };
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps) {
            const stateToUpdate = {};
            if(nextProps.newApiKeyToDisplay) {
                stateToUpdate.open = true;
                stateToUpdate.keyToDisplay = nextProps.newApiKeyToDisplay;
            }
            else {
                stateToUpdate.open = false;
                stateToUpdate.keyToDisplay = '';
            }
            this.setState(stateToUpdate);
        }
    }

    handleCopyToClipboard() {
        copy(this.state.keyToDisplay);
        app.addAlert('info', 'Copied Key to clipboard');
    };

    render() {
        return (
            <Dialog
                title='New API Key Created'
                actions={[
                    <FlatButton
                        label="Close"
                        primary={true}
                        onTouchTap={this.props.cancel.bind(this)}
                    />
                ]}
                autoScrollBodyContent={true}
                modal={true}
                open={this.state.open}
                onRequestClose={this.props.cancel.bind(this)}
            >
                <div>

                    <p>
                        New API Key created and <b>it will be displayed only now</b>
                    </p>
                    <TextField
                        floatingLabelText="API Key"
                        value={this.state.keyToDisplay}
                        fullWidth={true}
                        disabled={true}
                    />
                    <FlatButton
                        label={'Copy To Clipboard'}
                        primary={true}
                        onTouchTap={this.handleCopyToClipboard.bind(this)}
                        keyboardFocused={true}
                    />
                    <p>
                        Please store it somewhere safe because as soon as you navigate away from this page,<br/>
                        you will not be able to retrieve or restore this key.
                    </p>
                </div>
            </Dialog>
        );
    }
}
