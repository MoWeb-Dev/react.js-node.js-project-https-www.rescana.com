import React from 'react';
import {Card} from 'material-ui/Card';
import ApiKeysTable from './api-keys-table.jsx';
import AddApiKeyModal from './add-api-key-modal.jsx';
import Loader from 'react-loader-advanced';
import classNames from "classnames";

export default class ManageApiKeysForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openAddNewApiKeyModal: false,
            apiKeys: this.props.apiKeys,
            organizations: this.props.organizations
        };
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            const stateToUpdate = {};
            if(nextProps.apiKeys) {
                stateToUpdate.apiKeys = nextProps.apiKeys;
            }
            if(nextProps.organizations) {
                stateToUpdate.organizations = nextProps.organizations;
            }
            this.setState(stateToUpdate);
        }
    }

    handleOpenAddKey() {
        if(!this.state.openAddNewApiKeyModal) {
            this.setState({openAddNewApiKeyModal: true});
        }
    }

    handleCloseAddKey() {
        if(this.state.openAddNewApiKeyModal) {
            this.setState({openAddNewApiKeyModal: false});
        }
    }

    handleDoneAddKey(keyInput, organizationsInput, isScoreBackwardsEnabled = false, e) {
        // Do not delete keyInput parameter. It is not used here, but used in another component (when editing API Key).
        if(organizationsInput && Array.isArray(organizationsInput) && organizationsInput.length > 0) {
            this.props.addApiKey(organizationsInput, isScoreBackwardsEnabled,  e);
            this.handleCloseAddKey();
        }
        else {
            app.addAlert('warning', 'Some fields are missing');
        }
    }

    render() {
        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        return (
            <div style={{backgroundColor: 'grey200'}}>
                <div style={{margin: '30px 30px 0px 30px'}}>
                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                    <span style={{position: 'relative', top: '-6px', fontSize: 18}}>API Keys</span>
                </div>
                <div style={{margin: '30px 30px 0px 30px'}}>
                    <AddApiKeyModal
                        open={this.state.openAddNewApiKeyModal}
                        cancel={this.handleCloseAddKey.bind(this)}
                        done={this.handleDoneAddKey.bind(this)}
                        organizations={this.state.organizations}
                    />
                </div>
                <Card style={{margin: 30}}>
                    <Loader show={this.props.showLoaderForApiKeys} message={spinner}>
                        <div>
                            <ApiKeysTable
                                apiKeys={this.state.apiKeys}
                                organizations={this.state.organizations}
                                deleteApiKey={this.props.deleteApiKey}
                                updateApiKeyStatus={this.props.updateApiKeyStatus}
                                handleDoneEditKey={this.props.handleDoneEditKey}
                            />
                        </div>
                    </Loader>
                </Card>
            </div>
        );
    }
}
