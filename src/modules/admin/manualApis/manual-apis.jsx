import React from 'react';
import {Card} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete/index';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';

class ManualApis extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            domainForEmail: '',
            domainBreachCompanies: [],
            isAllCompaniesBotnetsChecked: true,
            botnetsCompanies: [],
            isAllCompaniesSslCertsChecked: true,
            sslCertsCompanies: [],
            isAllCompaniesGDPRDataChecked: true,
            gdprDataCompanies: [],
            isAllCompaniesCalculateChecked: true,
            calcDataCompanies: [],
            wifiNetworksCompanies: [],
            wifiNetworksAddress: '',
            wifiNetworksSsid: '',
            isSearchWifiBySsidChecked: false
        };

        this.findBreachesByCompany = this.findBreachesByCompany.bind(this);
        this.handleDomainForEmail = this.handleDomainForEmail.bind(this);
        this.harvestEmail = this.harvestEmail.bind(this);
        this.searchBlacklists = this.searchBlacklists.bind(this);
        this.searchBotnets = this.searchBotnets.bind(this);
        this.searchSSLCertificates = this.searchSSLCertificates.bind(this);
        this.searchGDPRCompliance = this.searchGDPRCompliance.bind(this);
        this.calculateScore = this.calculateScore.bind(this);
        this.searchWifiNetworks = this.searchWifiNetworks.bind(this);
    }

    findBreachesByCompany() {
        let data = {};
        if (this.state.domainBreachCompanies.length > 0) {
            data.companies = [];
            this.state.domainBreachCompanies.map((currComp) => {
                if (currComp && currComp.id && !data.companies.includes(currComp.id)) {
                    data.companies.push(currComp);
                }
            });
        }
        $.ajax({
            type: 'post',
            url: '/admin/runManHibp',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'Breached Emails Search has begun');
            } else {
                app.addAlert('Error', 'Error starting Breached Emails Search');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to search Breached Emails');
        });
    }


    handleDomainForEmail(e) {
        this.setState({domainForEmail: e.target.value});
    }

    handleWifiNetworksSearchField(searchField, e) {
        if(e && e.target && e.target.hasOwnProperty('value')) {
            if(searchField && searchField === 'ssid') {
                this.setState({wifiNetworksSsid: e.target.value});
            }
            else {
                // Meaning this is address.
                this.setState({wifiNetworksAddress: e.target.value});
            }
        }
    }

    harvestEmail() {
        $.ajax({
            type: 'post',
            url: '/admin/harvestEmails',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({domainForEmail: this.state.domainForEmail}),
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok) {
                app.addAlert('success', 'Added emails and breaches');
                this.setState({domainForEmail: ''});
            } else {
                app.addAlert('Error', 'No data found');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to get emails');
        });
    }

    searchBlacklists() {
        $.ajax({
            type: 'post',
            url: '/admin/searchBlacklists',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({}),
            dataType: 'json'
        }).done((data) => {
            if (data && data.ok) {
                app.addAlert('success', 'Blacklists Search has began');
            } else {
                app.addAlert('Error', 'Error starting Blacklists Search');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to search blacklists');
        });
    }

    searchBotnets() {
        let data = {};

        if (!this.state.isAllCompaniesBotnetsChecked && this.state.botnetsCompanies.length > 0) {
            data.companyIDs = [];
            this.state.botnetsCompanies.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        $.ajax({
            type: 'post',
            url: '/admin/searchBotnets',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'Botnets Search has began');
            } else {
                app.addAlert('Error', 'Error starting Botnets Search');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to search Botnets');
        });
    }

    searchSSLCertificates() {
        let data = {};

        if (!this.state.isAllCompaniesSslCertsChecked && this.state.sslCertsCompanies.length > 0) {
            data.companyIDs = [];
            this.state.sslCertsCompanies.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        $.ajax({
            type: 'post',
            url: '/admin/searchSSLCertificates',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'SSL Certificates Search has began');
            } else {
                app.addAlert('Error', 'Error starting SSL Certificates Search');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to search SSL Certificates');
        });
    }

    searchGDPRCompliance() {
        let data = {};

        if (!this.state.isAllCompaniesGDPRDataChecked && this.state.gdprDataCompanies.length > 0) {
            data.companyIDs = [];
            this.state.gdprDataCompanies.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        $.ajax({
            type: 'post',
            url: '/admin/searchGDPRCompliance',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'Web Privacy Policy Search has began');
            } else {
                app.addAlert('Error', 'Error starting Web Privacy Policy Search');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to search Web Privacy Policy');
        });
    }

    calculateScore() {
        let data = {};

        if (!this.state.isAllCompaniesCalculateChecked && this.state.calcDataCompanies.length > 0) {
            data.companyIDs = [];
            this.state.calcDataCompanies.map((currComp) => {
                if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                    data.companyIDs.push(currComp.id);
                }
            });
        }
        data.saveScoresOnDB = true;
        data.culcFromAdminMenual = true;
        $.ajax({
            type: 'POST',
            url: '/api/getMaxCVSScore',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((res) => {
            if (res && res.ok) {
                app.addAlert('success', 'CalculateScores');
            } else {
                app.addAlert('Error', 'Error calculating scores');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to calculate scores');
        });
    }

    searchWifiNetworks() {
        if(this.state.wifiNetworksCompanies && this.state.wifiNetworksCompanies.length > 0
            && this.state.wifiNetworksCompanies[0] && this.state.wifiNetworksCompanies[0].id) {
            const data = {
                companyID: this.state.wifiNetworksCompanies[0].id
            };

            if(this.state.isSearchWifiBySsidChecked) {
                if(this.state.wifiNetworksSsid) {
                    data.ssid = this.state.wifiNetworksSsid;
                }
                else {
                    app.addAlert('warning', 'Please fill a SSID');
                }
            }
            else {
                if(this.state.wifiNetworksAddress) {
                    data.address = this.state.wifiNetworksAddress;
                }
                else {
                    app.addAlert('warning', 'Please fill an address');
                }
            }

            if(data.address || data.ssid) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/collectWifiNetworks',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                }).done((res) => {
                    if (res && res.ok) {
                        app.addAlert('success', 'Wifi networks search has began');
                    } else {
                        app.addAlert('Error', 'Error starting wifi networks search');
                    }
                }).fail(() => {
                    app.addAlert('error', 'Error: Failed to search wifi networks');
                });
            }
        }
        else {
            app.addAlert('warning', 'Please choose a company first');
        }
    }

    updateAllCompaniesCheck(type) {
        let stateObj;
        if (type) {
            if (type === 'gdprData') {
                stateObj = {isAllCompaniesGDPRDataChecked: !this.state.isAllCompaniesGDPRDataChecked};
            } else if (type === 'sslCerts') {
                stateObj = {isAllCompaniesSslCertsChecked: !this.state.isAllCompaniesSslCertsChecked};
            } else if (type === 'botnets') {
                stateObj = {isAllCompaniesBotnetsChecked: !this.state.isAllCompaniesBotnetsChecked};
            } else {
                stateObj = {isAllCompaniesCalculateChecked: !this.state.isAllCompaniesCalculateChecked};
            }

            this.setState(stateObj);
        }
    }

    handleAddRequestedField(chip, type) {
        let allData = this.props.companies || [];

        let inputData = this.setInputData(type);

        let input;

        if (!Array.isArray(chip)) {
            input = chip;
        } else {
            input = chip[0];
        }

        if(type && type === 'wifiNetworks' && inputData && inputData.length > 0) {
            app.addAlert('warning', 'One company already selected.');
        }
        else {

            // Check if allData contains the input.
            let inputIndex = allData.map((c) => {
                return c.companyName;
            }).indexOf(input.companyName);

            if (inputIndex > -1) {
                let inputObjToAdd = allData[inputIndex];

                // Check if inputData not already contains the input.
                if (inputData.indexOf(inputObjToAdd) === -1) {
                    // Add to the ChipSelect.
                    inputData.push(inputObjToAdd);

                    let stateObj = ManualApis.setStateObj(type, inputData);

                    this.setState(stateObj);
                } else {
                    app.addAlert('warning', 'Company already selected.');
                }
            } else {
                app.addAlert('error', 'No such company.');
            }
        }
    }

    handleDeleteRequestedField(value, type) {
        let inputData = this.setInputData(type);

        // Check the index of the value of selectedChip in inputData.
        let index = inputData.map((c) => {
            return c.id;
        }).indexOf(value);
        if (index > -1) {
            // Update the ChipSelect.
            inputData.splice(index, 1);

            let stateObj = ManualApis.setStateObj(type, inputData);

            this.setState(stateObj);
        }
    }

    setInputData(type){
        let inputData;
        if (type) {
            if (type === 'gdprData') {
                inputData = this.state.gdprDataCompanies;
            } else if (type === 'sslCerts') {
                inputData = this.state.sslCertsCompanies;
            } else if (type === 'botnets') {
                inputData = this.state.botnetsCompanies;
            } else if (type === 'calcData') {
                inputData = this.state.calcDataCompanies;
            } else if (type === 'wifiNetworks') {
                inputData = this.state.wifiNetworksCompanies;
            } else {
                inputData = this.state.domainBreachCompanies;
            }
        }
        return inputData;
    }

    static setStateObj(type, inputData){
        let stateObj;

        if (type === 'sslCerts') {
            stateObj = {sslCertsCompanies: inputData};
        } else if (type === 'gdprData') {
            stateObj = {gdprDataCompanies: inputData};
        } else if (type === 'botnets') {
            stateObj = {botnetsCompanies: inputData};
        } else if (type === 'calcData') {
            stateObj = {calcDataCompanies: inputData};
        } else if (type === 'wifiNetworks') {
            stateObj = {wifiNetworksCompanies: inputData};
        } else {
            stateObj = {domainBreachCompanies: inputData};
        }

        return stateObj;
    }

    changeWifiSearchType(ignore, isSearchWifiBySsidChecked) {
        if(this.state.isSearchWifiBySsidChecked !== isSearchWifiBySsidChecked) {
            this.setState({isSearchWifiBySsidChecked: isSearchWifiBySsidChecked});
        }
    }

    render() {
        const autoCompleteStyle = {overflowY: 'auto', maxHeight: '200px'};

        const styles = {
            cardsStyle: {
                margin: 30, padding: '30px 30px 30px 30px'
            },
            spacingBottom: {
                padding: '30px 30px 30px 30px'
            },
            space: {
                marginRight: 20
            },
            spaceFlex: {
                marginRight: 20,
                display: 'flex'
            },
            block: {
                maxWidth: 250,
            },
            toggle: {
                marginBottom: 16,
            },
            thumbOff: {
                backgroundColor: '#0091ea',
            },
            trackOff: {
                backgroundColor: '#b3d4fc',
            },
            switchBlock: {
                maxWidth: 200,
                display: 'flex'
            },
            switchAddress: {
                whiteSpace: 'nowrap',
                marginLeft: '10px'
            },
            switchSsid: {
                marginLeft: '10px'
            }
        };
        return (
            <div>
                <div style={{margin: '30px 30px 0px 30px'}}>
                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                    <span style={{position: 'relative', top: '-6px', fontSize: 18}}>Manual APIs</span>
                </div>
                <Card style={styles.cardsStyle}>
                    <div style={styles.space}>Harvest Email by domain:</div>
                    <pre/>
                    <TextField
                        style={styles.space}
                        value={this.state.domainForEmail}
                        hintText={'Enter Domain Here..'}
                        onChange={this.handleDomainForEmail}/>
                    <pre/>
                    <RaisedButton
                        label="Harvest"
                        secondary={true}
                        keyboardFocused={true}
                        onTouchTap={this.harvestEmail}
                    />
                </Card>

                <Card style={styles.cardsStyle}>
                    <div style={styles.space}>
                        Find Breaches by Company:
                    </div>
                    <pre/>
                    <ChipInput
                        style={styles.space}
                        hintText="Search Companies"
                        floatingLabelText="Requested companies"
                        menuStyle={autoCompleteStyle}
                        filter={AutoComplete.caseInsensitiveFilter}
                        value={this.state.domainBreachCompanies}
                        dataSource={this.props.companies || []}
                        dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                        onRequestAdd={(chip) => this.handleAddRequestedField(chip, 'domainBreachs')}
                        onRequestDelete={(chip) => this.handleDeleteRequestedField(chip, 'domainBreachs')}
                        maxSearchResults={50}
                        openOnFocus={true}
                        chipContainerStyle={autoCompleteStyle}
                    />
                    <pre/>
                    <RaisedButton
                        label="Find"
                        secondary={true}
                        keyboardFocused={true}
                        onTouchTap={this.findBreachesByCompany}
                    />
                </Card>

                <Card style={styles.cardsStyle}>
                    <div style={styles.space}>
                        Search all domains in Blacklists:
                    </div>
                    <pre/>
                    <RaisedButton
                        label="Search Blacklists"
                        secondary={true}
                        keyboardFocused={true}
                        onTouchTap={this.searchBlacklists}
                    />
                </Card>

                <Card style={styles.cardsStyle}>
                    <div style={styles.space}>
                        Search IPs in Botnets RSS feeds:
                    </div>
                    <Checkbox
                        label="For all companies"
                        checked={this.state.isAllCompaniesBotnetsChecked}
                        onCheck={this.updateAllCompaniesCheck.bind(this, 'botnets')}
                        style={styles.space}
                    />
                    <pre/>
                    {(!this.state.isAllCompaniesBotnetsChecked) ? (
                        <div style={styles.space}>
                            <ChipInput
                                hintText="Search Companies"
                                floatingLabelText="Requested companies"
                                menuStyle={autoCompleteStyle}
                                filter={AutoComplete.caseInsensitiveFilter}
                                value={this.state.botnetsCompanies}
                                dataSource={this.props.companies || []}
                                dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                                onRequestAdd={(chip) => this.handleAddRequestedField(chip, 'botnets')}
                                onRequestDelete={(chip) => this.handleDeleteRequestedField(chip, 'botnets')}
                                maxSearchResults={50}
                                style={{marginLeft: '14px'}}
                                openOnFocus={true}
                                chipContainerStyle={autoCompleteStyle}
                            />
                        </div>
                    ) : null
                    }
                    <RaisedButton
                        label="Search Botnets"
                        secondary={true}
                        keyboardFocused={true}
                        onTouchTap={this.searchBotnets}
                    />
                </Card>

                <Card style={styles.cardsStyle}>
                    <div style={styles.space}>
                        Search domains for SSL Certificates:
                    </div>
                    <Checkbox
                        style={styles.space}
                        label="For all companies"
                        checked={this.state.isAllCompaniesSslCertsChecked}
                        onCheck={this.updateAllCompaniesCheck.bind(this, 'sslCerts')}
                    />
                    <pre/>
                    {(!this.state.isAllCompaniesSslCertsChecked) ? (
                        <div>
                            <ChipInput
                                hintText="Search Companies"
                                floatingLabelText="Requested companies"
                                menuStyle={autoCompleteStyle}
                                filter={AutoComplete.caseInsensitiveFilter}
                                value={this.state.sslCertsCompanies}
                                dataSource={this.props.companies || []}
                                dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                                onRequestAdd={(chip) => this.handleAddRequestedField(chip, 'sslCerts')}
                                onRequestDelete={(chip) => this.handleDeleteRequestedField(chip, 'sslCerts')}
                                maxSearchResults={50}
                                openOnFocus={true}
                                chipContainerStyle={autoCompleteStyle}
                            />
                        </div>
                    ) : null
                    }
                    <RaisedButton
                        label="Search SSL Certificates"
                        secondary={true}
                        keyboardFocused={true}
                        onTouchTap={this.searchSSLCertificates}
                    />
                </Card>

                <Card style={styles.cardsStyle}>
                    <div style={styles.space}>
                        Search domains for Web Privacy Policy:
                    </div>
                    <pre/>
                    <Checkbox
                        label="For all companies"
                        checked={this.state.isAllCompaniesGDPRDataChecked}
                        onCheck={this.updateAllCompaniesCheck.bind(this, 'gdprData')}
                        style={styles.space}
                    />
                    <pre/>
                    {(!this.state.isAllCompaniesGDPRDataChecked) ? (
                        <div>
                            <ChipInput
                                hintText="Search Companies"
                                floatingLabelText="Requested companies"
                                menuStyle={autoCompleteStyle}
                                filter={AutoComplete.caseInsensitiveFilter}
                                value={this.state.gdprDataCompanies}
                                dataSource={this.props.companies || []}
                                dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                                onRequestAdd={(chip) => this.handleAddRequestedField(chip, 'gdprData')}
                                onRequestDelete={(chip) => this.handleDeleteRequestedField(chip, 'gdprData')}
                                maxSearchResults={50}
                                openOnFocus={true}
                                chipContainerStyle={autoCompleteStyle}
                                style={styles.space}
                            />
                        </div>
                    ) : null
                    }
                    <RaisedButton
                        label="Search Web Privacy Policy"
                        secondary={true}
                        keyboardFocused={true}
                        onTouchTap={this.searchGDPRCompliance}
                    />
                </Card>

                <Card style={styles.cardsStyle}>
                    <div style={styles.space}>
                        Re-Calculate Score:
                    </div>
                    <pre/>
                    <Checkbox
                        label="For all companies"
                        checked={this.state.isAllCompaniesCalculateChecked}
                        onCheck={this.updateAllCompaniesCheck.bind(this, 'calcData')}
                        style={styles.space}
                    />
                    <pre/>
                    {(!this.state.isAllCompaniesCalculateChecked) ? (
                        <div>
                            <ChipInput
                                hintText="Search Companies"
                                floatingLabelText="Requested companies"
                                menuStyle={autoCompleteStyle}
                                filter={AutoComplete.caseInsensitiveFilter}
                                value={this.state.calcDataCompanies}
                                dataSource={this.props.companies || []}
                                dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                                onRequestAdd={(chip) => this.handleAddRequestedField(chip, 'calcData')}
                                onRequestDelete={(chip) => this.handleDeleteRequestedField(chip, 'calcData')}
                                maxSearchResults={50}
                                openOnFocus={true}
                                chipContainerStyle={autoCompleteStyle}
                                style={styles.space}
                            />
                        </div>
                    ) : null
                    }
                    <RaisedButton
                        label="Calculate"
                        secondary={true}
                        keyboardFocused={true}
                        onTouchTap={this.calculateScore}
                    />
                </Card>

                <Card style={styles.cardsStyle}>
                    <div style={styles.spaceFlex}>
                        Find Open Wifi:
                        <div style={styles.switchBlock}>
                            <span style={styles.switchAddress}>By Address</span>
                            <Toggle
                                label="By SSID"
                                labelPosition="right"
                                style={styles.switchSsid}
                                thumbStyle={styles.thumbOff}
                                trackStyle={styles.trackOff}
                                onToggle={this.changeWifiSearchType.bind(this)}
                                toggled={this.state.isSearchWifiBySsidChecked}
                            />
                        </div>
                    </div>
                        <div>
                            <ChipInput
                                hintText="Search Company"
                                floatingLabelText="Requested company"
                                menuStyle={autoCompleteStyle}
                                filter={AutoComplete.caseInsensitiveFilter}
                                value={this.state.wifiNetworksCompanies}
                                dataSource={this.props.companies || []}
                                dataSourceConfig={{'text': 'companyName', 'value': 'id'}}
                                onRequestAdd={(chip) => this.handleAddRequestedField(chip, 'wifiNetworks')}
                                onRequestDelete={(chip) => this.handleDeleteRequestedField(chip, 'wifiNetworks')}
                                maxSearchResults={50}
                                openOnFocus={true}
                                chipContainerStyle={autoCompleteStyle}
                                style={styles.space}
                            />
                            <br/>
                            {(this.state.isSearchWifiBySsidChecked) ?
                                <TextField
                                    style={styles.space}
                                    value={this.state.wifiNetworksSsid}
                                    floatingLabelText="Enter SSID"
                                    hintText={'Write SSID Here..'}
                                    onChange={this.handleWifiNetworksSearchField.bind(this, 'ssid')}
                                /> :
                                <TextField
                                    style={styles.space}
                                    value={this.state.wifiNetworksAddress}
                                    floatingLabelText="Enter Address"
                                    hintText={'Write Street, City, Country Here..'}
                                    onChange={this.handleWifiNetworksSearchField.bind(this, 'address')}
                                />
                            }
                        </div>
                    <RaisedButton
                        label="Find Wifi Networks"
                        secondary={true}
                        keyboardFocused={true}
                        onTouchTap={this.searchWifiNetworks}
                    />
                </Card>
                <div style={styles.spacingBottom}/>
            </div>
        );
    }
}

export default ManualApis;
