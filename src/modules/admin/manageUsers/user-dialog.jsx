import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import Dropzone from 'react-dropzone';
import classNames from "classnames";
import FontIcon from 'material-ui/FontIcon';
import {isImage} from "../../survey/common/survey-helpers.js";
import FileIcon from 'material-ui/svg-icons/editor/insert-drive-file';



export default class UserDialog extends React.Component {
    constructor(props) {
        super(props);
        /* Only .xlsx extension is supported with the ExcelParser. */
        this.state = {
            acceptedFiles: ['image/png'],
        };
    }

    onDrop(logoFile) {
        if (logoFile.length !== 1) {
            app.addAlert('error', "You can only select a single file");
        } else {
            this.props.handleChangeLogo(logoFile[0]);
        }
    }

    onDropRejected() {
        return app.addAlert('error', "File too big, max size is 3MB");
    }

    render() {
        const styles = {
            customWidth: {
                width: 200
            },
            textStyle: {
                marginLeft: '24px'
            },
            submitButtonStyle: {
                float: 'right'
            }
        };

        let dropZoneClass = classNames({
            'dropZone': true
        });
        let activeDropZoneClass = classNames({
            'stripes': true
        });
        let rejectDropZoneClass = classNames({
            'rejectStripes': true
        });
        let uploadCloudIcon = classNames({
            'material-icons': true,
            'uploadIconSize': true
        });
        let logoFile = this.props.userFields.logo;
        if(!logoFile){
            logoFile = {};
        }
        let img;
        let path = '';

        if(logoFile && logoFile.name ||  logoFile.path){
            path = logoFile.preview || '/orgLogo' + logoFile.path;
            if (isImage(logoFile)) {
                img = <img alt='no image' style={{position: 'relative', left: 30}} className="smallPreviewImg" src={path}/>;
            } else {
                img = <FileIcon className="smallPreviewImg" style={{position: 'relative', left: 30}}/>;
            }
        }

        let dropzone = <div>
            <span style={{color: 'black',position: 'relative', left: 30}}>Logo: <span>{img? img : ''}</span></span>
            <br/>
            <br/>
            <Dropzone
                accept={this.state.acceptedFiles.join(',')}
                onDrop={this.onDrop.bind(this)}
                style={{left:30, width: 200, height: 200}}
                className={dropZoneClass}
                activeClassName={activeDropZoneClass}
                rejectClassName={rejectDropZoneClass}
                onDropRejected={this.onDropRejected.bind(this)}
                maxSize={3000000}>
                <div className="dropzoneTextStyle">
                    <p className="dropzoneParagraph">Drop logo</p>
                    <br/>
                    <FontIcon
                        className={uploadCloudIcon}
                    >
                        cloud_upload
                    </FontIcon>
                </div>
            </Dropzone>
        </div>;

        return <Dialog
            title={this.props.title}
            autoScrollBodyContent={true}
            modal={false}
            open={this.props.open}
            onRequestClose={this.props.onRequestClose}>
            <ValidatorForm
                ref="form"
                onSubmit={this.props.submit}
                onError={(errors) => console.log(errors)}>
                <div style={styles.textStyle}>
                    <TextValidator
                        name="userEmail"
                        hintText="email"
                        onChange={this.props.onChangeEmail}
                        value={this.props.userFields.username}
                        validators={['required', 'isEmail']}
                        errorMessages={['this field is required', 'email is not valid']}
                    />
                    <br/>
                    <br/>
                    {
                        this.props.title === 'Add New User' && < TextValidator
                            name="userPassword"
                            hintText="Password"
                            type="password"
                            onChange={this.props.onChangePassword}
                            value={this.props.userFields.password}
                            validators={['required', 'matchRegexp:(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])']}
                            errorMessages={['this field is required', 'password must contain at least ' +
                            'one uppercase character, ' +
                            'one lowercase character ' +
                            'and one numeric character']}
                        />
                    }
                </div>
                {dropzone}
                <br/>
                <br/>
                <Checkbox
                    label="Enable vendor assessment"
                    checked={this.props.userFields.isVendorAssessmentEnabled}
                    onCheck={this.props.changeVendorAssessmentCheckBox}
                />
                <br/>
                <Checkbox
                    label="Enable Gauge Display"
                    checked={this.props.userFields.userPreferencesConfig.gaugeDisplay}
                    onCheck={this.props.changeGaugeDisplayCheckBox}
                />
                <br/>
                <Checkbox
                    label="Must setup two factor authentication on first login"
                    checked={this.props.userFields.twoFactorOnLogin}
                    onCheck={this.props.onSetTwoFactor}
                />
                <br/>
                <Checkbox
                    label="Allowed to export CVE reports"
                    checked={this.props.userFields.canExportCVEReports}
                    onCheck={this.props.onSetCanExportCVE}
                />
                <br/>
                {this.props.userFields.userRole === "user" &&
                <div>
                    <Checkbox
                        label="Allowed to add vendors"
                        checked={this.props.userFields.isUserAllowedToAddVendors}
                        onCheck={this.props.updateUserAllowedToAddVendors}
                        style={{}}
                    />
                </div>
                }
                <br/>
                <Checkbox
                    label="Enable demo mode"
                    checked={this.props.userFields.isInDemoMode}
                    onCheck={this.props.updateUserDemoModeCheckBox}
                    style={{}}
                />
                <br/>
                <span>
                    Role:
                </span>
                <br/>
                <br/>
                <DropDownMenu
                    value={this.props.userFields.userRole || 'basic'}
                    style={styles.customWidth}
                    onChange={this.props.onChangeDropdown}
                    autoWidth={true}
                >
                    <MenuItem value={'basic'} primaryText="Basic"/>
                    <MenuItem value={'user'} primaryText="Advanced"/>
                    <MenuItem value={'admin'} primaryText="Administrator"/>
                </DropDownMenu>
                <br/>
                <br/>
                <FlatButton
                    label="Cancel"
                    primary={true}
                    keyboardFocused={true}
                    style={styles.submitButtonStyle}
                    onTouchTap={this.props.onRequestClose}
                />
                <FlatButton
                    type="submit"
                    label="Done"
                    style={styles.submitButtonStyle}
                    primary={true}
                    keyboardFocused={true}
                />
            </ValidatorForm>
        </Dialog>;
    }
}
