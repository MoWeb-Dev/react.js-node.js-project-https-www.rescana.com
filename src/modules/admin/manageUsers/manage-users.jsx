import React from 'react';
import {Card} from 'material-ui/Card';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import IconButton from 'material-ui/IconButton';
import Delete from 'material-ui/svg-icons/action/delete';
import UserDialog from './user-dialog.jsx';
import DeleteDialog from "../../app/delete-dialog.jsx";
import DataTableViewTemplate from '../../common/datatableTemplate/datatable-template.jsx';
import Loader from 'react-loader-advanced';
import classNames from "classnames";

class ManageUsersForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openAddNewUsersModal: false,
            openEditUsersModal: false,
            users: this.props.users,
            userFields: {
                username: '',
                password: '',
                userRole: '',
                twoFactorOnLogin: false,
                canExportCVEReports: false,
                logo: {},
                openDeleteDialog: false,
                curRowIdToDelete: null,
                isUserAllowedToAddVendors: false,
                isInDemoMode: false,
                isVendorAssessmentEnabled: false,
                userPreferencesConfig:{gaugeDisplay: false}
            }
        };

        this.handleOpenDeleteDialog = this.handleOpenDeleteDialog.bind(this);
        this.handleCloseDeleteDialog = this.handleCloseDeleteDialog.bind(this);
        this.handleOpenAddNewUsersModal = this.handleOpenAddNewUsersModal.bind(this);
        this.handleOpenEditUsersModal = this.handleOpenEditUsersModal.bind(this);
        this.handleCloseAddNewUsersModal = this.handleCloseAddNewUsersModal.bind(this);
        this.handleRoleDDChange = this.handleRoleDDChange.bind(this);
        this.handleCloseEditUsersModal = this.handleCloseEditUsersModal.bind(this);
        this.handleChangeEmailField = this.handleChangeEmailField.bind(this);
        this.handleChangePassField = this.handleChangePassField.bind(this);
        this.handleUpdate2faOnFirstLogin = this.handleUpdate2faOnFirstLogin.bind(this);
        this.handleUpdateUserCanExportCVEReport = this.handleUpdateUserCanExportCVEReport.bind(this);
        this.addUser = this.addUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.setUserForEdit = this.setUserForEdit.bind(this);
        this.updateUserAllowedToAddVendorsCheckBox = this.updateUserAllowedToAddVendorsCheckBox.bind(this);
        this.updateDemoModeCheckBox = this.updateDemoModeCheckBox.bind(this);
        this.handleChangeLogo = this.handleChangeLogo.bind(this);
        this.handleChangeVendorAssessmentCheckBox = this.handleChangeVendorAssessmentCheckBox.bind(this);
        this.handleChangeGaugeDisplayCheckBox = this.handleChangeGaugeDisplayCheckBox.bind(this);
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
            return;
        }
    }

    updateUserAllowedToAddVendorsCheckBox(e) {
        let userFields = this.state.userFields;
        if (!userFields.isUserAllowedToAddVendors) {
            this.isUserRelatedToOrg(userFields.id);
        }
        userFields.isUserAllowedToAddVendors = !userFields.isUserAllowedToAddVendors;
        this.setState({
            userFields: userFields,
        })
    }

    updateDemoModeCheckBox(e) {
        let userFields = this.state.userFields;
        userFields.isInDemoMode = !userFields.isInDemoMode;
        this.setState({
            userFields: userFields,
        })
    }

    handleChangeVendorAssessmentCheckBox(e) {
        let userFields = this.state.userFields;
        userFields.isVendorAssessmentEnabled = !userFields.isVendorAssessmentEnabled;
        this.setState({
            userFields: userFields,
        });
    }

    handleChangeGaugeDisplayCheckBox(e) {
        let userFields = this.state.userFields;
        if(userFields.userPreferencesConfig === null || userFields.userPreferencesConfig === undefined){
            userFields.userPreferencesConfig = {};
        }
        userFields.userPreferencesConfig.gaugeDisplay = !userFields.userPreferencesConfig.gaugeDisplay;
        this.setState({
            userFields: userFields
        });
    }

    isUserRelatedToOrg(userId) {
        if (userId) {
            const data = {userId: userId, checkAdmin: false};
            $.ajax({
                type: 'POST',
                url: '/api/getOrganizationsByUserId',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok && data.orgArrForUserInput) {
                    let orgArrForUserInput = data.orgArrForUserInput;
                    if (orgArrForUserInput.length === 0) {
                        app.addAlert('warning', 'User is not related to any of the organizations! \n Please add him to one!');

                    } else if (orgArrForUserInput.length > 0) {
                        app.addAlert('info', 'User is already related to one or more of the organizations!');
                    }
                } else {
                    app.addAlert('error', 'Error: failed to load organizations.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to load organizations.');
            });
        } else {
            app.addAlert('warning', 'User is not related to any of the organizations! \n Please add him to one!');
        }
    }

    handleOpenDeleteDialog(rowToDelete) {
        this.setState({openDeleteDialog: true, curRowIdToDelete: rowToDelete});
    };

    handleCloseDeleteDialog() {
        this.setState({openDeleteDialog: false, curRowIdToDelete: null});
    };


    handleChangeEmailField(e) {
        let userFields = this.state.userFields;
        userFields.username = e.target.value;
        this.setState(userFields);
    }

    handleChangeLogo(iconFile) {
        let userFields = this.state.userFields;
        userFields.logo = iconFile;
        this.setState(userFields);
    };


    handleChangePassField(e) {
        let userFields = this.state.userFields;
        userFields.password = e.target.value;
        this.setState(userFields);
    }

    handleOpenAddNewUsersModal() {
        this.setState({openAddNewUsersModal: true});
    }

    handleCloseAddNewUsersModal() {
        this.setState({
            openAddNewUsersModal: false,
            userFields: {
                username: '',
                password: '',
                userRole: '',
                twoFactorOnLogin: false,
                canExportCVEReports: false,
                logo: {},
                isUserAllowedToAddVendors: false,
                isInDemoMode: false,
                isVendorAssessmentEnabled: false,
                userPreferencesConfig:{gaugeDisplay: false}
            }
        });
    }

    handleOpenEditUsersModal() {
        this.setState({openEditUsersModal: true});
    }

    handleCloseEditUsersModal() {
        this.setState({
            openEditUsersModal: false,
            userFields: {
                username: '',
                password: '',
                userRole: '',
                twoFactorOnLogin: false,
                canExportCVEReports: false,
                isUserAllowedToAddVendors: false,
                isInDemoMode: false,
                isVendorAssessmentEnabled: false,
                logo: {},
                userPreferencesConfig:{gaugeDisplay: false}
            }
        });
    }

    handleRoleDDChange(event, index, value) {
        let userFields = this.state.userFields;
        userFields.userRole = value;
        this.setState(userFields);
    }

    handleUpdate2faOnFirstLogin() {
        let userFields = this.state.userFields;
        userFields.twoFactorOnLogin = !userFields.twoFactorOnLogin;
        userFields.mfaEnabled = userFields.twoFactorOnLogin;
        this.setState({userFields: userFields});
    }

    handleUpdateUserCanExportCVEReport() {
        let userFields = this.state.userFields;
        userFields.canExportCVEReports = !userFields.canExportCVEReports;
        this.setState({userFields: userFields});
    }

    deleteUser(UserId, e) {
        e.preventDefault();

        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;

            $.ajax({
                type: 'post',
                url: '/admin/deleteUser',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({uid: uid, id: UserId}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    function matchesEl(el) {
                        return el.id === UserId;
                    }

                    let usersAfterDelete = this.state.users.filter((UserId) => {
                        return !matchesEl(UserId);
                    });

                    this.setState({
                        users: usersAfterDelete
                    });
                    app.addAlert('success', 'Record deleted.');
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to delete user.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to delete user.');
            });
        }
    }

    setUserForEdit(userId, e) {
        e.preventDefault();

        let userFields = this.state.userFields;

        this.setState({openEditUsersModal: true});

        function matchesEl(el) {
            return el.id === userId;
        }

        let user = this.state.users.filter((userId) => {
            return matchesEl(userId);
        });

        userFields.username = user[0].email;
        userFields.id = userId;
        userFields.userRole = user[0].userRole;
        userFields.twoFactorOnLogin = user[0].twoFactorOnLogin;
        userFields.canExportCVEReports = user[0].canExportCVEReports;
        userFields.logo = user[0].logo;
        userFields.isUserAllowedToAddVendors = user[0].isUserAllowedToAddVendors;
        userFields.isInDemoMode = user[0].isInDemoMode;
        userFields.isVendorAssessmentEnabled = user[0].isVendorAssessmentEnabled;
        userFields.userPreferencesConfig.gaugeDisplay = user[0].userPreferencesConfig.gaugeDisplay;

        this.setState({userFields: userFields});
    }

    editUser(data) {
        $.ajax({
            type: 'POST',
            url: '/aut/update_user',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            if (data.ok && data && data.user) {
                let usersAfterAdd = this.state.users;

                // Keep all users except the old user we just edited.
                usersAfterAdd = usersAfterAdd.filter((element) => {
                    return element.id !== data.user.id;
                });
                usersAfterAdd.push(data.user);

                this.setState({
                    users: usersAfterAdd
                });

                app.addAlert('success', 'Changes Saved.');

                // this.setState({openEditUsersModal: false});
                this.handleCloseEditUsersModal();
            } else {
                let defaultMsg = 'Failed to add user';

                if (data && data.error) {
                    let errMsg = data.error === 'nick-exists' ? 'User already exists in the system' : defaultMsg;
                    app.addAlert('error', 'Error: ' + errMsg);
                } else {
                    app.addAlert('error', 'Error: ' + defaultMsg);
                }
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to save changes' );
        });
    }

    submitUser(userAction) {
        let userFields = this.state.userFields;

        if (!this.state.userFields.userRole) {
            this.state.userFields.userRole = 'basic';
        }

        if (userFields.logo && userFields.logo.preview) {
            let data = new FormData();
            data.append('logoForOrg', userFields.logo, userFields.logo.name);

            $.ajax({
                url: '/api/saveLogoForOrg',
                dataType: 'json',
                data: data,
                enctype: 'multipart/form-data',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: (res) => {
                    if (res && res.result && res.result[0]) {
                        app.addAlert('success', 'logo Saved.');
                        userFields.logo = res.result[0];
                    } else {
                        app.addAlert('error', 'failed to save logo.');
                    }
                    if (userAction === 'addUser') {
                        this.addUser(userFields);
                    } else if (userAction === 'editUser') {
                        this.editUser(userFields);
                    }
                }
            });
        } else {
            if (userAction === 'addUser') {
                this.addUser(userFields);
            } else if (userAction === 'editUser') {
                this.editUser(userFields);
            }
        }
    }

    addUser(data) {
        $.ajax({
            type: 'post',
            url: '/aut/register',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            dataType: 'json'
        }).done((data) => {
            if (data && data.user) {
                let usersAfterAdd = this.state.users;

                usersAfterAdd.push(data.user);
                this.setState({
                    users: usersAfterAdd
                });
                app.addAlert('success', 'User Added.');
                this.handleCloseAddNewUsersModal();
            } else {
                let defaultMsg = 'Failed to add user';

                if (data && data.error) {
                    let errMsg = data.error === 'nick-exists' ? 'User already exists in the system' : defaultMsg;
                    app.addAlert('error', 'Error: ' + errMsg);
                } else {
                    app.addAlert('error', 'Error: ' + defaultMsg);
                }
            }
        }).fail(() => {
            app.addAlert('error', 'Error: Failed to add user');
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            users: nextProps.users
        });
    }

    render() {
        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        const styles = {
            wrappableStyle: {
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-word'
            }
        };

        const tableColumns = [
            {
                sortable: true,
                label: 'ID',
                key: 'id'
            },
            {
                sortable: true,
                label: 'Name',
                key: 'name',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Email',
                key: 'email',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Active',
                key: 'active',
                style: styles.wrappableStyle
            },
            {
                sortable: true,
                label: 'Create Date',
                key: 'created',
                style: styles.wrappableStyle
            },
            {
                label: 'Edit User',
                key: 'editUser',
                noneSearchable: true
            },
            {
                label: 'Remove',
                key: 'removeUser',
                noneSearchable: true
            }
        ];

        const tableData = [];
        this.state.users.map((currUser) => {
            tableData.push({
                id: currUser.id,
                name: currUser.name,
                email: currUser.email,
                active: currUser.active,
                created: currUser.created,
                editUser: <IconButton
                    onClick={(e) => this.setUserForEdit(currUser.id, e)}
                    disabled={currUser.admin || false}>
                    <Edit/>
                </IconButton>,
                removeUser: <IconButton
                    style={{cursor: 'pointer'}}
                    disabled={currUser.admin || false}
                    onTouchTap={this.handleOpenDeleteDialog.bind(this, currUser.id)}>
                    <Delete/>
                </IconButton>
            });
        });

        return (
            <div style={{
                backgroundColor: 'grey200'
            }}>
                <div style={{margin: '30px 30px 0px 30px'}}>
                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                    <span style={{position: 'relative', top: '-6px', fontSize: 18}}>Users</span>
                </div>
                <Card style={{margin: 30}}>
                    <Loader show={this.props.showLoaderForUsers} message={spinner}>
                        <div>
                            <DataTableViewTemplate
                                myDataType={'adminUsers'}
                                tableColumns={tableColumns}
                                tableData={tableData}
                                hideDownloadExcel={true}
                                hideCardBackground={true}
                            />
                        </div>
                    </Loader>
                </Card>

                <UserDialog
                    title={'Add New User'}
                    open={this.state.openAddNewUsersModal}
                    onRequestClose={this.handleCloseAddNewUsersModal}
                    onChangeEmail={this.handleChangeEmailField}
                    onChangePassword={this.handleChangePassField}
                    onChangeDropdown={this.handleRoleDDChange}
                    onSetTwoFactor={this.handleUpdate2faOnFirstLogin}
                    onSetCanExportCVE={this.handleUpdateUserCanExportCVEReport}
                    userFields={this.state.userFields}
                    submit={this.submitUser.bind(this, 'addUser')}
                    updateUserAllowedToAddVendors={this.updateUserAllowedToAddVendorsCheckBox}
                    updateUserDemoModeCheckBox={this.updateDemoModeCheckBox}
                    handleChangeLogo={this.handleChangeLogo}
                    changeVendorAssessmentCheckBox={this.handleChangeVendorAssessmentCheckBox}
                    changeGaugeDisplayCheckBox={this.handleChangeGaugeDisplayCheckBox}
                />

                <UserDialog
                    title={'Edit User'}
                    open={this.state.openEditUsersModal}
                    onRequestClose={this.handleCloseEditUsersModal}
                    onChangeEmail={this.handleChangeEmailField}
                    onChangePassword={this.handleChangePassField}
                    onChangeDropdown={this.handleRoleDDChange}
                    onSetTwoFactor={this.handleUpdate2faOnFirstLogin}
                    onSetCanExportCVE={this.handleUpdateUserCanExportCVEReport}
                    userFields={this.state.userFields}
                    submit={this.submitUser.bind(this, 'editUser')}
                    updateUserAllowedToAddVendors={this.updateUserAllowedToAddVendorsCheckBox}
                    updateUserDemoModeCheckBox={this.updateDemoModeCheckBox}
                    handleChangeLogo={this.handleChangeLogo}
                    changeVendorAssessmentCheckBox={this.handleChangeVendorAssessmentCheckBox}
                    changeGaugeDisplayCheckBox={this.handleChangeGaugeDisplayCheckBox}
                />
                <DeleteDialog
                    open={this.state.openDeleteDialog}
                    onRequestClose={this.handleCloseDeleteDialog}
                    onDelete={(e) => {
                        this.deleteUser(this.state.curRowIdToDelete, e);
                        this.handleCloseDeleteDialog();
                    }}/>
            </div>
        );
    }
}

export default ManageUsersForm;
