import ChipInput from 'material-ui-chip-input';
import AutoComplete from 'material-ui/AutoComplete';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import UpdateAllDialog from "../update-all-dialog.jsx";
import Checkbox from 'material-ui/Checkbox';


export default class AddProjectModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            openAddNewProjectModal: false,
            projectFields: this.props.projectFields,
            usersForDD: this.props.allUsers,
            companiesInputVal: this.props.companies,
            usersInputVal: [],
            companies: [],
            openUpdateAllDialog: false,
            currUserChipInput: '',
            updateDialogText: '',
            addedChipArray: [],
            deletedChipArray: [],
        };

        this.handleDeleteAllowedUsersField = this.handleDeleteAllowedUsersField.bind(this);
        this.handleOpenUpdateAllDialog = this.handleOpenUpdateAllDialog.bind(this);
        this.handleCloseUpdateAllDialog = this.handleCloseUpdateAllDialog.bind(this);

        this.chipArrCheckWhenAdded = this.chipArrCheckWhenAdded.bind(this);
        this.chipArrCheckWhenDeleted = this.chipArrCheckWhenDeleted.bind(this);
        this.handleChangeSelfAssessmentCheckBox = this.handleChangeSelfAssessmentCheckBox.bind(this);
    }

    handleChangeSelfAssessmentCheckBox(e) {
        let projectFields = this.props.projectFields;
        projectFields.isSelfAssessmentProject = !this.props.projectFields.isSelfAssessmentProject;
        this.setState(projectFields);
    }

    chipArrCheckWhenAdded(chipId) {
        let addedChipArray = this.state.addedChipArray;

        if (this.state.deletedChipArray.includes(chipId)) {
            let deletedChipArray = this.state.deletedChipArray;
            const index = deletedChipArray.indexOf(chipId);

            if (index > -1) {
                deletedChipArray.splice(index, 1);
                this.setState({deletedChipArray: deletedChipArray})
            }
        } else {
            addedChipArray.push(chipId);
            this.setState({addedChipArray: addedChipArray})
        }
    }

    chipArrCheckWhenDeleted(chipId) {
        let deletedChipArray = this.state.deletedChipArray;

        if (this.state.addedChipArray.includes(chipId)) {
            let addedChipArray = this.state.addedChipArray;
            const index = addedChipArray.indexOf(chipId);

            if (index > -1) {
                addedChipArray.splice(index, 1);
                this.setState({addedChipArray: addedChipArray})
            }
        } else {
            deletedChipArray.push(chipId);
            this.setState({deletedChipArray: deletedChipArray})
        }
    }

    handleOpenUpdateAllDialog() {
        this.setState({openUpdateAllDialog: true});
    };

    handleCloseUpdateAllDialog() {
        this.setState({openUpdateAllDialog: false});
    };

    handleAddToAll() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;
            let userToAdd = this.state.currUserChipInput;
            let projData = this.props.projectFields;

            $.ajax({
                type: 'post',
                url: '/admin/addAllowedUsersToProjCompanies',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({uid: uid, projData: projData, userToAdd: userToAdd}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    app.addAlert('success', 'updated');
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to update.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to update.');
            });
        }
    }

    handleRemoveFromAll() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        } else {
            let uid = user.id;
            let userToRemove = this.state.currUserChipInput;
            let projData = this.props.projectFields;

            $.ajax({
                type: 'post',
                url: '/admin/removeAllowedUsersFromProjCompanies',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({uid: uid, projData: projData, userToRemove: userToRemove}),
                dataType: 'json'
            }).done((data) => {
                if (data && data.ok) {
                    app.addAlert('success', 'removed');
                } else if (data && data.error) {
                    console.log(data.error);
                } else {
                    app.addAlert('error', 'Error: failed to update.');
                }
            }).fail(() => {
                app.addAlert('error', 'Error: failed to update.');
            });
        }
    }

    handleChangeProjectField(e) {
        let projectFields = this.props.projectFields;
        projectFields.projectName = e.target.value;
        this.setState(projectFields);
    }

    handleAddCompanyToList(chip) {
        let companiesInputVal = this.state.companiesInputVal;
        let projectFields = this.state.projectFields;
        companiesInputVal.push(chip);
        projectFields.selectedCompanies = companiesInputVal;
        this.chipArrCheckWhenAdded(chip.id);
        this.setState({projectFields: projectFields, companiesInputVal: companiesInputVal});
    }

    handleDeleteCompanyFromList(value, index) {
        let companiesInputVal = this.state.companiesInputVal;
        let projectFields = this.state.projectFields;
        companiesInputVal.splice(index, 1);

        projectFields.selectedCompanies = projectFields.selectedCompanies.filter(function (obj) {
            return obj.id !== value;
        });
        this.chipArrCheckWhenDeleted(value);
        this.setState({companiesInputVal: companiesInputVal, projectFields: projectFields});
    }

    handleAddAllowedUsersField(input) {
        let projectFields = this.state.projectFields;
        let usersInputVal = this.state.usersInputVal;
        usersInputVal.push(input);
        projectFields.allowedUsers = usersInputVal;
        this.setState({
            projectFields: projectFields, usersInputVal: usersInputVal,
            currUserChipInput: input,
            updateDialogText: 'Do you wish to add the user to all related companies?'
        });
        this.handleOpenUpdateAllDialog(this);
    }

    handleDeleteAllowedUsersField(value, index) {
        let projectFields = this.state.projectFields;
        let usersInputVal = this.state.usersInputVal;
        usersInputVal.splice(index, 1);

        projectFields.allowedUsers = projectFields.allowedUsers.filter(function (obj) {
            return obj !== value;
        });

        this.setState({
            usersInputVal: usersInputVal, projectFields: projectFields,
            currUserChipInput: value,
            updateDialogText: 'Do you wish to remove the user from all related companies?'
        });
        this.handleOpenUpdateAllDialog(this);
    }

    cancel() {
        this.setState({openAddNewProjectModal: false});
        this.props.clearAllModalFields();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            openAddNewProjectModal: nextProps.openAddNewProjectModal,
            usersInputVal: nextProps.projectFields.allowedUsers,
            companiesInputVal: nextProps.projectFields.selectedCompanies,
            usersForDD: nextProps.allUsers,
            companies: nextProps.companies
        });
    }

    render() {
        let companiesForDD = this.state.companies.map((item) => {
            return {name: item.companyName, id: item.id};
        });

        return (
            <Dialog
                title="Add New Project"
                actions={[
                    <FlatButton
                        label="Done"
                        primary={true}
                        style={{color: 'white', marginBottom: '5px', marginRight: '5px', height: 40}}
                        keyboardFocused={true}
                        backgroundColor={'#0091ea'}
                        hoverColor={'#12a4ff'}
                        rippleColor={'white'}
                        labelStyle={{fontSize: 10}}
                        onTouchTap={() => {
                            this.props.addProject(this.state.projectFields, this.state.addedChipArray, this.state.deletedChipArray);
                            this.setState({addedChipArray: [], deletedChipArray: []})

                        }}
                    />,
                    <FlatButton
                        label="Cancel"
                        primary={true}
                        backgroundColor={'#0091ea'}
                        style={{color: 'white', marginBottom: '5px', height: 40}}
                        keyboardFocused={true}
                        hoverColor={'#12a4ff'}
                        rippleColor={'white'}
                        labelStyle={{fontSize: 10}}
                        onTouchTap={this.cancel.bind(this)}
                    />
                ]}
                actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
                bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
                modal={true}
                contentStyle={{borderRadius: '7px 7px 7px 7px'}}
                titleStyle={{
                    fontSize: 18,
                    background: 'rgba(0,0,0,0.7)',
                    color: 'white', textAlign: 'center',
                    borderRadius: '2px 2px 0px 0px',
                    textTransform: 'uppercase',
                }}
                autoScrollBodyContent={true}
                open={this.state.openAddNewProjectModal}
                onRequestClose={this.cancel.bind(this)}
            >
                <form>
                    <br/>
                    <TextField
                        floatingLabelText={"Project Name"}
                        hintText="Project Name"
                        onChange={this.handleChangeProjectField.bind(this)}
                        value={this.state.projectFields.projectName}
                    />
                    <br/>
                    <br/>
                    <ChipInput
                        hintText="Companies to include in project"
                        floatingLabelText="Companies to include in project"
                        value={this.state.companiesInputVal}
                        newChipKeyCodes={[]}
                        filter={AutoComplete.fuzzyFilter}
                        onRequestAdd={(chip) => this.handleAddCompanyToList(chip)}
                        onRequestDelete={(chip, index) => this.handleDeleteCompanyFromList(chip, index)}
                        dataSource={companiesForDD}
                        dataSourceConfig={{'text': 'name', 'value': 'id'}}
                        maxSearchResults={5}
                        openOnFocus={true}
                    />
                    <br/>
                    <br/>
                    <ChipInput
                        hintText="Allowed users (emails)"
                        floatingLabelText="Allowed users (emails)"
                        value={this.state.usersInputVal}
                        filter={AutoComplete.fuzzyFilter}
                        dataSource={this.state.usersForDD}
                        onRequestAdd={(chip) => this.handleAddAllowedUsersField(chip)}
                        dataSourceConfig={{'text': 'email', 'value': 'id'}}
                        onRequestDelete={(chip, index) => this.handleDeleteAllowedUsersField(chip, index)}
                        maxSearchResults={5}
                    />
                    <br/>
                    <br/>
                    <Checkbox
                        label="Self assessment project"
                        checked={this.props.projectFields.isSelfAssessmentProject}
                        onCheck={this.handleChangeSelfAssessmentCheckBox.bind(this)}
                    />
                    <br/>
                </form>
                <UpdateAllDialog
                    updateDialogText={this.state.updateDialogText}
                    open={this.state.openUpdateAllDialog}
                    onRequestClose={this.handleCloseUpdateAllDialog}
                    removeFromAll={() => {
                        this.handleRemoveFromAll();
                        this.handleCloseUpdateAllDialog()
                    }}

                    addToAll={() => {
                        this.handleAddToAll();
                        this.handleCloseUpdateAllDialog()
                    }}/>
            </Dialog>
        );
    }
}
