import React from 'react';
import {Card} from 'material-ui/Card';
import AddProjectModal from './add-project-modal.jsx';
import ProjectsTable from './projects-table.jsx';
import Loader from 'react-loader-advanced';
import classNames from "classnames";

class ManageProjectsForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openAddNewProjectModal: false,
            companiesInputVal: this.props.companies,
            projects: this.props.projects,
            totalProjectsCount: this.props.totalProjectsCount,
            users: this.props.users,
            projectFields: {
                projectName: '',
                allowedUsers: [],
                selectedCompanies: [],
                createDate: '',
                id: '',
                isSelfAssessmentProject: false,
            }
        };

        this.handleOpenAddNewProjectModal = this.handleOpenAddNewProjectModal.bind(this);
    }

    handleOpenAddNewProjectModal() {
        this.clearAllModalFields();
        this.setState({openAddNewProjectModal: true});

        let user = app.getAuthUser();
        let projectFields = this.state.projectFields;
        if (user && user.admin) {
            projectFields.allowedUsers.push(user);
            this.setState({projectFields: projectFields});
        }
    }

    clearAllModalFields() {
        let projectFields = this.state.projectFields;
        projectFields.projectName = '';
        projectFields.allowedUsers = [];
        projectFields.selectedCompanies = [];
        projectFields.createDate = '';
        projectFields.id = '';

        this.setState({
            openAddNewProjectModal: false,
            projectFields: projectFields
        });
    }

    componentDidMount() {
        let user = app.getAuthUser();

        if (!user) {
            app.routeAuthUser();
        }
    }

    editProject(projectId, e) {
        e.preventDefault();
        let projectFields = this.state.projectFields;
        this.setState({openAddNewProjectModal: true});

        function matchesEl(el) {
            return el.id === projectId;
        }

        let project = this.state.projects.filter((projectId) => {
            return matchesEl(projectId);
        });

        projectFields.projectName = project[0].projectName;
        projectFields.selectedCompanies = project[0].selectedCompanies;
        projectFields.allowedUsers = project[0].allowedUsers;
        projectFields.id = project[0].id;
        projectFields.isSelfAssessmentProject = project[0].isSelfAssessmentProject;

        this.setState({
            projectFields: projectFields
        });
    }

    addProject(projectFields, addedChipArray, deletedChipArray) {
        this.setState({openAddNewProjectModal: false});
        this.props.addProject(projectFields, addedChipArray, deletedChipArray);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            users: nextProps.users,
            projects: nextProps.projects,
            companies: nextProps.companies,
            totalProjectsCount: nextProps.totalProjectsCount
        });
    }

    render() {
        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        return (
            <div style={{backgroundColor: 'grey200'}}>
                <div style={{margin: '30px 30px 0px 30px'}}>
                    <span style={{marginRight: '15px', fontSize: '35px', color: '#0091ea'}}>|</span>
                    <span style={{position: 'relative', top: '-6px', fontSize: 18}}>Projects</span>
                </div>
                <Card style={{margin: 30}}>
                    <Loader show={this.props.showLoaderForProjects} message={spinner}>
                        <div>
                            <ProjectsTable
                                projects={this.state.projects}
                                totalProjectsCount={this.state.totalProjectsCount}
                                deleteProject={this.props.deleteProject}
                                editProject={this.editProject.bind(this)}
                                runProjectQuery={this.props.runProjectQuery}
                                handleChangeDataViewClick={this.props.handleChangeDataViewClick}
                            />
                        </div>
                    </Loader>
                </Card>
                <AddProjectModal
                    addProject={this.addProject.bind(this)}
                    cancel={this.cancel}
                    projectFields={this.state.projectFields}
                    companies={this.state.companies}
                    clearAllModalFields={this.clearAllModalFields.bind(this)}
                    allUsers={this.state.users}
                    openAddNewProjectModal={this.state.openAddNewProjectModal}
                />
            </div>
        );
    }
}

export default ManageProjectsForm;
