import React from 'react';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import Play from 'material-ui/svg-icons/av/play-arrow';
import IconButton from 'material-ui/IconButton';
import Delete from 'material-ui/svg-icons/action/delete';
import Chip from 'material-ui/Chip';
import {showExpandedDataChip} from '../../common/UI-Helpers.jsx';
import DeleteDialog from "../../app/delete-dialog.jsx";
import DataTableViewTemplate from '../../common/datatableTemplate/datatable-template.jsx';
import DefaultIcon from 'material-ui/svg-icons/navigation/menu';
import ArrowUpIcon from 'material-ui/svg-icons/navigation/arrow-upward';
import ArrowDownIcon from 'material-ui/svg-icons/navigation/arrow-downward';

export default class ProjectsTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projects: this.props.projects || [],
            totalProjectsCount: this.props.totalProjectsCount || 0,
            openDeleteDialog: false,
            curRowIdToDelete: null
        };

        this.handleOpenDeleteDialog = this.handleOpenDeleteDialog.bind(this);
        this.handleCloseDeleteDialog = this.handleCloseDeleteDialog.bind(this);
    }

    removeUserFromList() {
    }

    handleOpenDeleteDialog(rowToDelete) {
        this.setState({openDeleteDialog: true, curRowIdToDelete: rowToDelete});
    };

    handleCloseDeleteDialog() {
        this.setState({openDeleteDialog: false, curRowIdToDelete: null});
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            projects: nextProps.projects,
            totalProjectsCount: nextProps.totalProjectsCount
        });
    }

    render() {
        const styles = {
            chip: {
                margin: 4
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap'
            },
            tableStyle: {
                paddingTop: '8px',
                paddingBottom: '8px'

            },
            wrappableStyle: {
                whiteSpace: 'pre-wrap',
                wordBreak: 'break-word'
            },
            menuItem: {
                color: '#ffffff',
                fontSize: '13px'
            },
            menuItemIcon: {
                fill: '#ffffff'
            }
        };

        const tableColumns = [
            {
                label: 'ID',
                key: 'id'
            },
            {
                label: 'Project Name',
                key: 'projectName',
                style: styles.wrappableStyle
            },
            {
                label: 'Create Date',
                key: 'createDate',
                style: styles.wrappableStyle
            },
            {
                label: 'Companies',
                key: 'selectedCompanies',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                label: 'Allowed Users',
                key: 'allowedUsers',
                style: styles.wrappableStyle,
                noneSearchable: true
            },
            {
                label: '',
                key: 'runEditDelete',
                noneSearchable: true
            }
        ];

        const tableData = [];
        this.state.projects.map((currProj) => {
            tableData.push({
                id: currProj.id,
                projectName: currProj.projectName,
                createDate: currProj.createDate,
                selectedCompanies: currProj && currProj.selectedCompanies && currProj.selectedCompanies.map((company) => (
                    <Chip key={company.id} style={styles.chip}>{company.name}</Chip>)),
                allowedUsers: showExpandedDataChip(currProj.allowedUsers && currProj.allowedUsers.map((user) => {
                    return (user.email);
                }), 4, 5),
                runEditDelete: <div>
                    <IconButton
                        onClick={(e) => {e.preventDefault();e.stopPropagation(); this.props.runProjectQuery(currProj.id, e)}}
                        name="runProjectQuery">
                        <Play/>
                    </IconButton>
                    <IconButton
                        onClick={(e) => this.props.editProject(currProj.id, e)} name="editProject">
                        <Edit/>
                    </IconButton>
                    <IconButton
                        name="deleteProject"
                        style={{cursor: 'pointer'}}
                        onTouchTap={this.handleOpenDeleteDialog.bind(this, currProj.id)}>
                        <Delete/>
                    </IconButton>
                </div>
            });
        });

        const sortOptions = [
            {
                primaryText: 'Default',
                value: 'default',
                icon: <DefaultIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'project Name ASC',
                value: 'projectNameAsc',
                icon: <ArrowUpIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'project Name DESC',
                value: 'projectNameDesc',
                icon: <ArrowDownIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Create Date ASC',
                value: 'createDateAsc',
                icon: <ArrowUpIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            },
            {
                primaryText: 'Create Date DESC',
                value: 'createDateDesc',
                icon: <ArrowDownIcon style={styles.menuItemIcon}/>,
                style: styles.menuItem
            }
        ];

        return (
            <div>
                <DataTableViewTemplate
                    myDataType={'adminProjects'}
                    tableColumns={tableColumns}
                    tableData={tableData}
                    totalRowCount={this.state.totalProjectsCount}
                    hideDownloadExcel={true}
                    hideCardBackground={true}
                    hideHeaderToolbar={true}
                    searchHintText='Enter project name'
                    sortOptions={sortOptions}
                    handleChangeDataViewClick={this.props.handleChangeDataViewClick}
                />
                <DeleteDialog
                    open={this.state.openDeleteDialog}
                    onRequestClose={this.handleCloseDeleteDialog}
                    onDelete={(e) => {
                        this.props.deleteProject(this.state.curRowIdToDelete, e);
                        this.handleCloseDeleteDialog();
                    }}/>
            </div>
        );
    }
}
