import React from 'react';
import {BLACKLISTS_TYPE, ORGANIZATION_ID} from '../../../config/consts';
import classNames from 'classnames';
import {showExpandedDataChip} from '../common/UI-Helpers.jsx';
import DataTableTemplate from '../common/datatableTemplate/datatable-template-main.jsx';
import {getLoadedCompaniesObject, getDefaultCompanies} from '../common/datatableTemplate/datatable-template-helpers.js';
import Loader from 'react-loader-advanced';
import generalFunctions from '../generalComponents/general-functions.js';
import themeForFont from "../app/themes/themeForFont";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import textForTitleExpl from "../appGeneralFiles/text-for-title-explanations";
import TitleExplanationComponent from '../appGeneralFiles/title-explanation-component.jsx';

const shortid = require('shortid');

const NO_RESULT = 'None';

const JOIN_SEPERATOR = ', ';

export default class Blacklists extends React.Component {
    constructor() {
        super();

        const wrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word'
        };
        const clickableWrappableStyle = {
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-word',
            'cursor': 'pointer'
        };

        this.state = {
            showLoader: false,
            tableData: [],
            sortedTableData: [],
            totalRowCount: 0,
            rowCountInPage: 10,
            companies: [],
            companiesInputVal: [],
            companiesDomains: [],
            assetUserInputAndMitigatedData: [],
            fullData: [],
            /* Create table column headers */
            tableColumns: [
                {
                    sortable: true,
                    label: 'Domain',
                    key: 'domain',
                    style: clickableWrappableStyle
                }, {
                    sortable: true,
                    label: 'IP Address',
                    key: 'address',
                    style: clickableWrappableStyle
                }, {
                    label: 'Domain Blacklist',
                    key: 'blacklist_domain',
                    style: wrappableStyle,
                    noneSearchable: true
                }, {
                    label: 'IP Address Blacklist',
                    key: 'blacklist_ip',
                    style: wrappableStyle,
                    noneSearchable: true
                }, {
                    label: 'MX Records Blacklist',
                    key: 'blacklist_mx',
                    style: wrappableStyle,
                    noneSearchable: true
                }, {
                    label: 'NS Records Blacklist',
                    key: 'blacklist_ns',
                    style: wrappableStyle,
                    noneSearchable: true
                }, {
                    label: 'Related Companies',
                    key: 'relatedCompanies',
                    style: clickableWrappableStyle,
                    noneSearchable: true
                },{
                    sortable: true,
                    label: 'Importance',
                    key: 'Importance',
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: false,
                    label: 'Comments',
                    key: 'Comments',
                    style: wrappableStyle,
                    noneSearchable: true
                },{
                    sortable: true,
                    label: 'Mitigated',
                    key: 'Mitigated',
                    style: wrappableStyle,
                    noneSearchable: true
                }
            ],
            tableColumnsToDownload: [
                {label: 'Domain', key: 'domain'},
                {label: 'IP Address', key: 'address'},
                {label: 'Blacklists for Domain', key: 'blacklist_domain'},
                {label: 'Blacklists for IP Address', key: 'blacklist_ip'},
                {label: 'MX Records', key: 'mx'},
                {label: 'Blacklist for MX Records', key: 'blacklist_mx'},
                {label: 'NS Records', key: 'ns'},
                {label: 'Blacklists for NS Records', key: 'blacklist_ns'},
                {label: 'Related Companies', key: 'relatedCompanies'},
                {label: 'Importance', key: 'Importance'},
                {label: 'Comments', key: 'Comments'},
                {label: 'Mitigated', key: 'Mitigated'}
            ],
            openInfoBox: false
        };

        this.setResults = this.setResults.bind(this);
        this.updateState = this.updateState.bind(this);
        this.getBlacklistsFindings = this.getBlacklistsFindings.bind(this);
        this.setDefaultCompanies = this.setDefaultCompanies.bind(this);
        this.saveLoadedCompanies = this.saveLoadedCompanies.bind(this);
        this.arrangeExtraInfo = this.arrangeExtraInfo.bind(this);
        this.updateStateForAMData = this.updateStateForAMData.bind(this);
    }

    //AMData = assetUserInput and mitigated Data
    updateStateForAMData(assetUserInputAndMitigatedData) {
        this.setState({assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});
    }

    componentDidMount() {
        let user = app.getAuthUser();
        if (user) {
            this.setState({user: user});
        }
    }

    componentWillMount() {
        app.setFlagVisibile(false);
    }

    handleOpenInfoBox(event) {
        // This prevents ghost click.
        event.preventDefault();

        if (!this.state.openInfoBox) {
            this.setState({
                openInfoBox: true,
                anchorEl: event.currentTarget
            });
        }
    }

    handleCloseInfoBox() {
        if (this.state.openInfoBox) {
            this.setState({openInfoBox: false});
        }
    }

    setResults() {
        let data = this.state.fullData;
        let tableData = [];
        let tableDataToDownload = [];
        let assetUserInputAndMitigatedData = this.state.assetUserInputAndMitigatedData || [];
        let isInDemoMode = this.state.user && this.state.user.isInDemoMode;
        let demoIdx=0;
        for (let i = 0; i < data.length; i++) {
            if (data[i] && data[i].data) {
                let findings = data[i].data;

                let findingsDomain = this.setDefaultIfEmpty(findings['blacklist_domain']);
                let findingsMX = this.setDefaultIfEmpty(findings['blacklist_mx']);
                let findingsNS = this.setDefaultIfEmpty(findings['blacklist_ns']);
                let findingsIP = this.setDefaultIfEmpty(findings['blacklist_ip']['blacklist']);
                let findingsAddress = findings['blacklist_ip']['address'];

                let tableObj = {
                    id: data[i]._id || data[i].id,
                    domain: isInDemoMode? 'domain-example-'+ demoIdx +'.com' : data[i].domain,
                    address: isInDemoMode? generalFunctions.createIpStrForDemoMode(findingsAddress, demoIdx) : findingsAddress,
                    blacklist_domain: this.addLinksToBlackslists(findingsDomain, true),
                    blacklist_mx: this.addLinksToBlackslists(findingsMX, true),
                    blacklist_ns: this.addLinksToBlackslists(findingsNS, true),
                    blacklist_ip: this.addLinksToBlackslists(findingsIP, false),
                    mx: this.setDefaultIfEmpty(findings.mx).join(JOIN_SEPERATOR),
                    ns: this.setDefaultIfEmpty(findings.ns).join(JOIN_SEPERATOR),
                    relatedCompanies: showExpandedDataChip(isInDemoMode? ['Company Name'] : data[i].relatedCompanies, 3, 5),
                    surveyIdentifier: shortid.generate()
                };


                let importance = 3;
                let mitigatedBackground = 'black';
                let comments = [];
                let assetUserInputFlag = false;
                let mitigatedBackgroundFlag = false;
                let commentsFlag = false;

                for(let j=0; j<assetUserInputAndMitigatedData.length; j++){
                    if(this.refs.dataTableTemplate.isCurRowNeedToBeUpdateCurNode(tableObj, assetUserInputAndMitigatedData[j])){
                        if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'assetUserInput'){
                            importance = assetUserInputAndMitigatedData[j].importance? Number(assetUserInputAndMitigatedData[j].importance) : 3;
                            assetUserInputFlag = true;
                        }
                        if(assetUserInputAndMitigatedData[j].type && assetUserInputAndMitigatedData[j].type === 'mitigated'){
                            mitigatedBackground = '#5bd25e';
                            mitigatedBackgroundFlag = true;
                        }
                        if(assetUserInputAndMitigatedData[j].comments){
                            comments = assetUserInputAndMitigatedData[j].comments? assetUserInputAndMitigatedData[j].comments : [];
                            commentsFlag = true;
                        }
                        if(mitigatedBackgroundFlag && assetUserInputFlag && commentsFlag){
                            break;
                        }
                    }
                }
                let badgeColor = comments && comments.length > 5 ? '#d24e37' : comments && comments.length > 0 ? '#e0882c' : '#82abea';
                tableObj.Importance = this.refs.dataTableTemplate.getImportanceComponent(tableObj, importance);
                tableObj.Comments =  this.refs.dataTableTemplate.getCommentsComponent(tableObj, comments.length, comments, badgeColor);
                tableObj.Mitigated =  this.refs.dataTableTemplate.getMitigatedComponent(tableObj, mitigatedBackground);


                tableData.push(tableObj);

                let tableDataToDownloadObj = {};
                tableDataToDownloadObj[this.state.tableColumnsToDownload[0].key] = isInDemoMode? 'domain-example-'+demoIdx+'.com' : tableObj.domain;
                tableDataToDownloadObj[this.state.tableColumnsToDownload[1].key] = isInDemoMode? generalFunctions.createIpStrForDemoMode(tableObj.address, demoIdx) : tableObj.address;
                tableDataToDownloadObj[this.state.tableColumnsToDownload[2].key] = findingsDomain.join(JOIN_SEPERATOR);
                tableDataToDownloadObj[this.state.tableColumnsToDownload[3].key] = findingsIP.join(JOIN_SEPERATOR);
                tableDataToDownloadObj[this.state.tableColumnsToDownload[4].key] = tableObj.mx;
                tableDataToDownloadObj[this.state.tableColumnsToDownload[5].key] = findingsMX.join(JOIN_SEPERATOR);
                tableDataToDownloadObj[this.state.tableColumnsToDownload[6].key] = tableObj.ns;
                tableDataToDownloadObj[this.state.tableColumnsToDownload[7].key] = findingsNS.join(JOIN_SEPERATOR);
                tableDataToDownloadObj[this.state.tableColumnsToDownload[8].key] = isInDemoMode? ['Company Name'] : data[i].relatedCompanies.join(JOIN_SEPERATOR);
                tableDataToDownloadObj[this.state.tableColumnsToDownload[9].key] = generalFunctions.getImportanceStr(importance);
                tableDataToDownloadObj[this.state.tableColumnsToDownload[10].key] = generalFunctions.allCommentsToOneStr(comments);
                tableDataToDownloadObj[this.state.tableColumnsToDownload[11].key] = mitigatedBackgroundFlag ? 'Yes' : 'No';
                tableDataToDownload.push(tableDataToDownloadObj);
            }
            demoIdx++;
        }

        this.setState({
            totalRowCount: tableData.length,
            tableData: tableData,
            sortedTableData: tableData.slice(0, this.state.rowCountInPage),
            tableDataToDownload: tableDataToDownload
        });

        if(this.refs && this.refs.dataTableTemplate){
            this.refs.dataTableTemplate.doAfterSetResult();
        }
    }

    addLinksToBlackslists(blArray, isDomainNotIP) {
        if (blArray && blArray[0] && blArray[0] !== NO_RESULT) {
            let arrayWithLinks = [];

            for (let i = 0; i < blArray.length; i++) {
                let fixedURL = 'https://apility.io/list/' + blArray[i] + '/type/';
                if (isDomainNotIP) {
                    fixedURL += 'baddomain';
                } else {
                    fixedURL += 'badip';
                }
                let link = <div key={i}><a href={fixedURL} target='_blank' style={{textDecoration: 'none'}}>{this.add3Dots(blArray[i], 20)}</a></div>;

                arrayWithLinks.push(link);
            }
            return arrayWithLinks;
        } else {
            return blArray;
        }
    }

    setDefaultIfEmpty(arr) {
        if (Array.isArray(arr)) {
            if (arr.length === 0) {
                arr.push(NO_RESULT);
            }
        } else {
            arr = [NO_RESULT];
        }

        return arr;
    }

    add3Dots(str, limit) {
        let dots = '...';

        if (str && limit && str.length > limit) {
            str = str.substring(0, limit) + dots;
        }

        return str;
    }

    updateState(stateObj, cb) {
        if (stateObj) {
            if (cb) {
                this.setState(stateObj, cb);
            } else {
                this.setState(stateObj);
            }
        }
    }

    getBlacklistsFindings() {
        let orgId = localStorage.getItem(ORGANIZATION_ID);

        const data = {
            companies: this.state.companiesInputVal,
            domains: this.state.companiesDomains,
            orgId: orgId
        };

        this.startLoader();

        $.ajax({
            type: 'POST',
            url: '/api/getBlacklists',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((data) => {
            this.stopLoader();
            let assetUserInputAndMitigatedData = [];

            if (data && data.blacklistsData) {
                this.setState({});
                if(data.assetUserInputAndMitigatedData){
                    assetUserInputAndMitigatedData = data.assetUserInputAndMitigatedData;
                }
                this.setState({fullData: data.blacklistsData, assetUserInputAndMitigatedData: assetUserInputAndMitigatedData});

                this.setResults();
            }
        }).fail(() => {
            this.stopLoader();

            app.addAlert('error', 'Error: failed to fetch data');
        });
    }

    startLoader() {
        if (!this.state.showLoader) {
            this.setState({showLoader: true});
        }
    }

    stopLoader() {
        if (this.state.showLoader) {
            this.setState({showLoader: false});
        }
    }

    setDefaultCompanies(result) {
        if (result && result.companies && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: result.companies,
                companiesInputVal: result.companiesInputVal,
                companiesDomains: result.companiesPropArray
            }, this.getBlacklistsFindings);
        }
    }

    saveLoadedCompanies(companies, selectedCompanies) {
        let result = getLoadedCompaniesObject(companies, selectedCompanies, 'selectedDomains');

        if (result && result.companiesInputVal && result.companiesPropArray) {
            this.setState({
                companies: companies,
                companiesInputVal: result.companiesInputVal,
                companiesDomains: result.companiesPropArray
            }, this.getBlacklistsFindings);
        }
    }

    arrangeExtraInfo(data) {
        let resultData = [];

        if (data) {
            if (data.address) {
                resultData.push({name: 'IP Address', finding: data.address});
            }

            if (data.mx && data.mx !== NO_RESULT) {
                resultData.push({name: 'MX Records', finding: data.mx});
            }

            if (data.ns && data.ns !== NO_RESULT) {
                resultData.push({name: 'NS Records', finding: data.ns});
            }
        }

        return resultData;
    }

    render() {
        let pageStyleClass = classNames({
            'surveyContainerLTR': true
        });

        // When adding a spinner - 2 classes are required: spinner(always) and matching position.
        let spinnerClass = classNames({
            'spinner': true,
            'spinnerPositionForReports': true
        });

        const spinner = <div className={spinnerClass}/>;

        let infoText = textForTitleExpl.blacklists.expl;
        let shorterInfoText = textForTitleExpl.blacklists.explShort;

        return (
            <div className={pageStyleClass}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <Loader show={this.state.showLoader} message={spinner}>
                        <div style={{margin: '40px 0px 40px 10px'}}>
                            <span style={{marginRight: '15px', fontSize: '45px', color: '#0091ea'}}>|</span>
                            <span style={{position: 'relative', top: '-6px', fontSize: 22}}>BLACKLISTS</span>
                        </div>
                        <TitleExplanationComponent
                            openInfoBox={this.state.openInfoBox}
                            anchorEl={this.state.anchorEl}
                            shorterInfoText={shorterInfoText}
                            infoText={infoText}
                            handleCloseInfoBox={this.handleCloseInfoBox.bind(this)}
                            handleOpenInfoBox={this.handleOpenInfoBox.bind(this)}
                        />
                        <DataTableTemplate
                            ref="dataTableTemplate"
                            myDataType={BLACKLISTS_TYPE}
                            tableColumns={this.state.tableColumns}
                            tableData={this.state.tableData}
                            sortedTableData={this.state.sortedTableData}
                            tableColumnsToDownload={this.state.tableColumnsToDownload}
                            tableDataToDownload={this.state.tableDataToDownload}
                            totalRowCount={this.state.totalRowCount}
                            companies={this.state.companies}
                            companiesInputVal={this.state.companiesInputVal}
                            companiesDomains={this.state.companiesDomains}
                            updateParentState={this.updateState}
                            setResults={this.setResults}
                            getData={this.getBlacklistsFindings}
                            setDefaultCompanies={this.setDefaultCompanies}
                            saveLoadedCompanies={this.saveLoadedCompanies}
                            arrangeExtraInfo={this.arrangeExtraInfo}
                            enableRowClick={false}
                            assetUserInputAndMitigatedData={this.state.assetUserInputAndMitigatedData}
                            changeTableState={() => {}}
                            updateStateForAMData={this.updateStateForAMData}
                        />
                    </Loader>
                </MuiThemeProvider>
            </div>
        );
    }
}
