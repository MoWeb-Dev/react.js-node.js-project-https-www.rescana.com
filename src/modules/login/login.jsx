'use strict';
import React from 'react';
import MainContainer from './login-main-container.jsx';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import themeForFont from "../app/themes/themeForFont";


export default class Login extends React.Component {
    render() {
        return (
            <div>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <MainContainer/>
                </MuiThemeProvider>
            </div>
        );
    }
}
