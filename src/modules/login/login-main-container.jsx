import {Card} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import {browserHistory} from 'react-router';
import {PROJECT_NAME, ORGANIZATION_ID, ORGANIZATION_NAME, PROJECT_ID} from '../../../config/consts';
import ChooseOrganizationDialog from './choose-organization-dialog.jsx';

const cardStyle = {
    textAlign: 'center',
    width: '320px',
    backgroundColor: 'white',
    margin: 'auto',
    position: 'relative',
    right: 140
};
const fieldStyle = {
    margin: '14px auto'
};

export default class MainContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            username: null,
            password: null,
            error: '',
            orgArrForUserInput: [],
            openChooseOrgDialog: false,
            curOrgIdx: 0
        };
    }

    handleChangeUsername(e) {
        this.setState({username: e.target.value});
    }

    handleChangePassword(e) {
        this.setState({password: e.target.value});
    }

    loginIsValid() {
        let loginIsValid = true;

        if (!this.state.password || !this.state.username) {
            this.state.error = 'Both fields are required.';
            loginIsValid = false;
        }

        if ((this.state.username && this.state.username.indexOf(' ') === 0) || (this.state.password && this.state.password.indexOf(' ') === 0)) {
            this.state.error = 'Email or password are not valid';
            loginIsValid = false;
        }

        return loginIsValid;
    }

    getfailMessage(data) {
        if (data.why === 'user-not-found') {
            return 'User not found';
        } else if (data.why === 'locked-out') {
            return 'Your account has been locked. Please try again in a few minutes';
        } else {
            return 'Incorrect email or password';
        }
    }

    handleAfterOrgChoose() {
        if (this.state.chosenOrg && this.state.chosenOrg._id) {
            localStorage.setItem(ORGANIZATION_ID, this.state.chosenOrg._id);
            localStorage.setItem(ORGANIZATION_NAME, this.state.chosenOrg.organizationName);
            this.getProjects(this.state.chosenOrg._id);
        } else {
            app.addAlert('error', 'Error: Organization Failed To Load!');
        }
    }

    getProjects(orgId) {
        let data = {};
        let url = '';
        let type = '';

        if (orgId) {
            url = '/api/getProjectsByOrgId';
            type = 'POST';
            data.orgId = orgId;
        } else {
            url = '/api/getAllProjectsById';
            type = 'GET';
        }
        $.ajax({
            type: type,
            url: url,
            async: true,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projects) => {
            if (projects && projects.length !== 0) {
                localStorage.setItem(PROJECT_NAME, projects[0].projectName);
                if(projects[0] && (projects[0].id || projects[0]._id)){
                    localStorage.setItem(PROJECT_ID, projects[0].id || projects[0]._id.valueOf().toString());
                }
            }
            app.routeAuthUser(true);
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
            app.routeAuthUser(true);
        });
    }

    setUser(data) {
        app.clearLocalStorage();
        app.setAuthUser({
            id: data.user.id,
            email: data.user.email,
            name: data.user.name,
            mfaEnabled: data.user.mfaEnabled,
            userRole: data.user.userRole,
            eyPartner: data.user.eyPartner,
            admin: data.user.userRole === 'admin',
            canExportCVEReports: data.user.canExportCVEReports,
            logo: data.user.logo,
            personalInfo: data.user.personalInfo,
            isUserAllowedToAddVendors: data.user.isUserAllowedToAddVendors,
            isInDemoMode: data.user.isInDemoMode,
            isVendorAssessmentEnabled: data.user.isVendorAssessmentEnabled,
            alertSystemConfig: data.user.alertSystemConfig,
            userPreferencesConfig: data.user.userPreferencesConfig
        });
    }

    handleLogin(e) {
        e.preventDefault();
        if (this.loginIsValid()) {
            const userNameInLowerCase = this.state.username.toLowerCase();
            $.post('/aut/login', {username: userNameInLowerCase, password: this.state.password})
                .done((data) => {
                    if (data.ok) {
                        if (data.user && data.user.why) {
                            let failMsg = this.getfailMessage(data.user);
                            app.addAlert('error', 'Error: ' + failMsg);
                        } else {
                            this.setUser(data);
                            localStorage.removeItem(ORGANIZATION_ID);
                            localStorage.removeItem(ORGANIZATION_NAME);

                            if (data.user.isUserAllowedToAddVendors && data.user.userRole !== 'admin') {
                                this.refs.ChooseOrganizationDialog.getOrganizationsByUserId(false)
                            } else {
                                this.getProjects(null);
                            }
                        }
                    } else if (!data.ok && data.firstLogin && data.mfaEnabled) {
                        browserHistory.push(`/setupLogin2fa`);
                    } else if (!data.ok && data.mfaEnabled) {
                        browserHistory.push(`/login2fa`);
                    } else {
                        let failMsg = this.getfailMessage(data);
                        app.addAlert('error', 'Error: ' + failMsg);
                    }
                });
        } else {
            app.addAlert('error', 'Error: ' + this.state.error);
        }
    }

    handleOrgChange(event, index, value) {
        this.setState({curOrgIdx: value})
    }

    handleOrgSelect(idx, e) {
        e.preventDefault();
        this.setState({
            chosenOrg: this.state.orgArrForUserInput[idx]
        });
    }

    handleOrgResults(orgArrForUserInput) {
        if (orgArrForUserInput.length > 1) {
            this.setState({
                orgArrForUserInput: orgArrForUserInput,
                openChooseOrgDialog: true,
                chosenOrg: orgArrForUserInput[0]
            });
        } else if (orgArrForUserInput.length === 1) {
            this.setState({chosenOrg: orgArrForUserInput[0]});
            this.handleAfterOrgChoose();
        } else {
            app.addAlert('error', 'Error: No Organizations Related To The User Found.');
        }
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleLogin.bind(this)} style={{'paddingTop': '85px'}}>
                    <div style={{
                        margin: '40px auto', textAlign: 'center', fontSize: '20px', position: 'relative',
                        right: 140
                    }}>SIGN IN
                    </div>
                    <Card style={cardStyle}>
                        <TextField
                            hintText="Email"
                            style={fieldStyle}
                            underlineShow={true}
                            onChange={this.handleChangeUsername.bind(this)}
                        />
                        <TextField
                            hintText="Password"
                            type="password"
                            style={fieldStyle}
                            underlineShow={true}
                            onChange={this.handleChangePassword.bind(this)}
                        />
                        <FlatButton
                            label="Sign in"
                            secondary={true}
                            backgroundColor={'rgba(0,0,0,0.7)'}
                            hoverColor={'#0091ea'}
                            style={{margin: '14px 0', color: 'white',
                                borderRadius: 5,
                                height: 40,
                            bottom: 10}}
                            onClick={this.handleLogin.bind(this)}
                            type="submit"
                        />
                    </Card>
                </form>
                <ChooseOrganizationDialog
                    ref="ChooseOrganizationDialog"
                    orgArrForUserInput={this.state.orgArrForUserInput}
                    openChooseOrgDialog={this.state.openChooseOrgDialog}
                    curOrgIdx={this.state.curOrgIdx}
                    handleAfterOrgChoose={this.handleAfterOrgChoose.bind(this)}
                    handleOrgChange={this.handleOrgChange.bind(this)}
                    handleOrgSelect={this.handleOrgSelect.bind(this)}
                    handleOrgResults={this.handleOrgResults.bind(this)}
                />
            </div>
        );
    }
}
