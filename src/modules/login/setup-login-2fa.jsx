'use strict';
import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TwoFactorForm from '../accountSettings/two-factor-form.jsx';
import {ORGANIZATION_ID, ORGANIZATION_NAME, PROJECT_ID, PROJECT_NAME} from '../../../config/consts';
import ChooseOrganizationDialog from './choose-organization-dialog.jsx';
import themeForFont from "../app/themes/themeForFont";

export default class SetupLogin2fa extends React.Component {
    constructor() {
        super();
        this.state = {
            error: '',
            orgArrForUserInput: [],
            openChooseOrgDialog: false,
            curOrgIdx: 0
        };
    }

    handleLogin(code) {
        $.post('/aut/login2fa', {mfaCode: code})
            .done((data) => {
                if (data.ok) {
                    app.clearLocalStorage();
                    app.setAuthUser({
                        id: data.user.id,
                        email: data.user.email,
                        name: data.user.name,
                        mfaEnabled: data.user.mfaEnabled,
                        userRole: data.user.userRole,
                        eyPartner: data.user.eyPartner,
                        login: data.login,
                        admin: data.user.userRole === 'admin',
                        personalInfo: data.user.personalInfo,
                        alertSystemConfig: data.user.alertSystemConfig,
                        isUserAllowedToAddVendors: data.user.isUserAllowedToAddVendors,
                        isInDemoMode: data.user.isInDemoMode,
                        isVendorAssessmentEnabled: data.user.isVendorAssessmentEnabled,
                        userPreferencesConfig: data.user.userPreferencesConfig
                    });
                    localStorage.removeItem(ORGANIZATION_ID);
                    localStorage.removeItem(ORGANIZATION_NAME);

                    if (data.user.isUserAllowedToAddVendors && data.user.userRole !== 'admin') {
                        this.refs.ChooseOrganizationDialog.getOrganizationsByUserId(false)
                    } else {
                        this.getProjects(null);
                    }
                } else {
                    app.addAlert('error', 'Incorrect code, please try again');
                }
            })
            .fail(() => {
                app.addAlert('error', 'Error has occurred, please try again');
            });
    }


    handleAfterOrgChoose() {
        if (this.state.chosenOrg && this.state.chosenOrg._id) {
            localStorage.setItem(ORGANIZATION_ID, this.state.chosenOrg._id);
            localStorage.setItem(ORGANIZATION_NAME, this.state.chosenOrg.organizationName);
            this.getProjects(this.state.chosenOrg._id);
        } else {
            app.addAlert('error', 'Error: Organization Failed To Load!');
        }
    }

    getProjects(orgId) {
        let data = {};
        let url = '';
        let type = '';

        if (orgId) {
            url = '/api/getProjectsByOrgId';
            type = 'POST';
            data.orgId = orgId;
        } else {
            url = '/api/getAllProjectsById';
            type = 'GET';
        }
        $.ajax({
            type: type,
            url: url,
            async: true,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projects) => {
            if (projects && projects.length !== 0) {
                localStorage.setItem(PROJECT_NAME, projects[0].projectName);
                if(projects[0] && (projects[0].id || projects[0]._id)){
                    localStorage.setItem(PROJECT_ID, projects[0].id || projects[0]._id.valueOf().toString());
                }
            }
            app.routeAuthUser(true);
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
            app.routeAuthUser(true);
        });
    }

    handleOrgResults(orgArrForUserInput) {
        if (orgArrForUserInput.length > 1) {
            this.setState({
                orgArrForUserInput: orgArrForUserInput,
                openChooseOrgDialog: true,
                chosenOrg: orgArrForUserInput[0]
            });
        } else if (orgArrForUserInput.length === 1) {
            this.setState({chosenOrg: orgArrForUserInput[0]});
            this.handleAfterOrgChoose();
        } else {
            app.addAlert('error', 'Error: No Organizations Related To The User Found.');
        }
    }

    handleOrgChange(event, index, value) {
        this.setState({curOrgIdx: value})
    }

    handleOrgSelect(idx, e) {
        e.preventDefault();
        this.setState({
            chosenOrg: this.state.orgArrForUserInput[idx]
        });
    }

    render() {
        const cardStyle = {
            textAlign: 'left',
            width: '50%',
            backgroundColor: 'white',
        };

        const containerStyle = {
            paddingTop: '5%',
            position:'relative',
            right: 100,
            paddingBottom: '3%',
            width: '50%',
            margin: 'auto'
        };


        return (
            <div style={containerStyle}>
                <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                    <div>
                        <TwoFactorForm
                            style={cardStyle}
                            setup2fFromLogin={true}
                            login={this.handleLogin.bind(this)}
                        />
                        <ChooseOrganizationDialog
                            ref="ChooseOrganizationDialog"
                            orgArrForUserInput={this.state.orgArrForUserInput}
                            openChooseOrgDialog={this.state.openChooseOrgDialog}
                            curOrgIdx={this.state.curOrgIdx}
                            handleAfterOrgChoose={this.handleAfterOrgChoose.bind(this)}
                            handleOrgChange={this.handleOrgChange.bind(this)}
                            handleOrgSelect={this.handleOrgSelect.bind(this)}
                            handleOrgResults={this.handleOrgResults.bind(this)}
                        />
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}
