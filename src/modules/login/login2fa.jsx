import {Card, CardText, CardHeader} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import {ORGANIZATION_ID, ORGANIZATION_NAME, PROJECT_NAME, PROJECT_ID} from '../../../config/consts';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import themeForFont from "../app/themes/themeForFont";
import ChooseOrganizationDialog from './choose-organization-dialog.jsx';

const cardStyle = {
    textAlign: 'center',
    width: '320px',
    backgroundColor: 'white',
    margin: 'auto',
    position: 'relative',
    right: 140
};

const fieldStyle = {
    margin: '14px auto',
    fontSize: 13
};

export default class Login2fa extends React.Component {
    constructor() {
        super();
        this.state = {
            code: '',
            error: '',
            user: {},
            orgArrForUserInput: [],
            openChooseOrgDialog: false,
            curOrgIdx: 0
        };
    }

    componentDidMount() {
        let user = app.getAuthUser();
        this.setState({user: user});
    }


    handleCode(e) {
        this.setState({code: e.target.value});
    }

    loginIsValid() {
        let loginIsValid = true;

        if (!this.state.code) {
            this.state.error = 'Code is required.';
            loginIsValid = false;
        }
        return loginIsValid;
    }

    handleLogin(e) {
        e.preventDefault();
        if (this.loginIsValid()) {
            $.post('/aut/login2fa', {mfaCode: this.state.code})
                .done((data) => {
                    if (data.ok) {
                        app.clearLocalStorage();
                        app.setAuthUser({
                            id: data.user.id,
                            email: data.user.email,
                            name: data.user.name,
                            mfaEnabled: data.user.mfaEnabled,
                            userRole: data.user.userRole,
                            eyPartner: data.user.eyPartner,
                            login: data.login,
                            admin: data.user.userRole === 'admin',
                            personalInfo: data.user.personalInfo,
                            isUserAllowedToAddVendors: data.user.isUserAllowedToAddVendors,
                            isInDemoMode: data.user.isInDemoMode,
                            isVendorAssessmentEnabled: data.user.isVendorAssessmentEnabled,
                            alertSystemConfig: data.user.alertSystemConfig,
                            userPreferencesConfig: data.user.userPreferencesConfig
                        });

                        localStorage.removeItem(ORGANIZATION_ID);
                        localStorage.removeItem(ORGANIZATION_NAME);

                        if (data.user.isUserAllowedToAddVendors && data.user.userRole !== 'admin') {
                            this.refs.ChooseOrganizationDialog.getOrganizationsByUserId(false)
                        } else {
                            this.getProjects(null);
                        }
                    } else {
                        app.addAlert('error', 'Incorrect code, please try again');
                    }
                })
                .fail(() => {
                    app.addAlert('error', 'Error has occurred, please try again');
                });
        } else {
            app.addAlert('error', 'Error: ' + this.state.error);
        }
    }

    handleAfterOrgChoose() {
        if (this.state.chosenOrg && this.state.chosenOrg._id) {
            localStorage.setItem(ORGANIZATION_ID, this.state.chosenOrg._id);
            localStorage.setItem(ORGANIZATION_NAME, this.state.chosenOrg.organizationName);
            this.getProjects(this.state.chosenOrg._id);
        } else {
            app.addAlert('error', 'Error: Organization Failed To Load!');
        }
    }

    getProjects(orgId) {
        let data = {};
        let url = '';
        let type = '';

        if (orgId) {
            url = '/api/getProjectsByOrgId';
            type = 'POST';
            data.orgId = orgId;
        } else {
            url = '/api/getAllProjectsById';
            type = 'GET';
        }
        $.ajax({
            type: type,
            url: url,
            async: true,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done((projects) => {
            if (projects && projects.length !== 0) {
                localStorage.setItem(PROJECT_NAME, projects[0].projectName);
                if(projects[0] && (projects[0].id || projects[0]._id)){
                    localStorage.setItem(PROJECT_ID, projects[0].id || projects[0]._id.valueOf().toString());
                }
            }
            app.routeAuthUser(true);
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load projects.');
            app.routeAuthUser(true);
        });
    }

    handleOrgResults(orgArrForUserInput) {
        if (orgArrForUserInput.length > 1) {
            this.setState({
                orgArrForUserInput: orgArrForUserInput,
                openChooseOrgDialog: true,
                chosenOrg: orgArrForUserInput[0]
            });
        } else if (orgArrForUserInput.length === 1) {
            this.setState({chosenOrg: orgArrForUserInput[0]});
            this.handleAfterOrgChoose();
        } else {
            app.addAlert('error', 'Error: No Organizations Related To The User Found.');
        }
    }

    handleOrgChange(event, index, value) {
        this.setState({curOrgIdx: value})
    }

    handleOrgSelect(idx, e) {
        e.preventDefault();
        this.setState({
            chosenOrg: this.state.orgArrForUserInput[idx]
        });
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(themeForFont)}>
                <div>
                <form onSubmit={this.handleLogin.bind(this)} style={{'paddingTop': '85px'}}>
                    <div style={{
                        margin: '40px auto', textAlign: 'center', fontSize: '20px', position: 'relative',
                        right: 140
                    }}>SECOND AUTHENTICATION
                    </div>
                    <Card style={cardStyle}>
                        <Divider/>
                        <br/>
                        <TextField
                            hintText="PIN code"
                            style={fieldStyle}
                            underlineShow={true}
                            onChange={this.handleCode.bind(this)}
                            autoFocus={true}
                        />
                        <FlatButton
                            label="Sign in"
                            secondary={true}
                            backgroundColor={'#0091ea'}
                            hoverColor={'#009ff7'}
                            style={{margin: '14px 0', color: 'white',
                                borderRadius: 5,
                                height: 40,
                                bottom: -5}}
                            onClick={this.handleLogin.bind(this)}
                            type="submit"
                        />
                        <CardText style={{color: '#6d7070', fontSize: 11}}>
                            Two factor Authentication is enabled for this user.
                            <br/>
                            Please enter PIN code.
                        </CardText>
                    </Card>
                </form>
                <ChooseOrganizationDialog
                    ref="ChooseOrganizationDialog"
                    orgArrForUserInput={this.state.orgArrForUserInput}
                    openChooseOrgDialog={this.state.openChooseOrgDialog}
                    curOrgIdx={this.state.curOrgIdx}
                    handleAfterOrgChoose={this.handleAfterOrgChoose.bind(this)}
                    handleOrgChange={this.handleOrgChange.bind(this)}
                    handleOrgSelect={this.handleOrgSelect.bind(this)}
                    handleOrgResults={this.handleOrgResults.bind(this)}
                />
                </div>
            </MuiThemeProvider>
        );
    }
}
