import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

export default class ChooseOrganizationDialog extends React.Component {
    constructor(props) {
        super(props);
    }

    getOrganizationsByUserId(checkAdmin) {
        let route = 'admin';
        if(!checkAdmin){
            route = 'api';
        }

        $.ajax({
            type: 'POST',
            url: '/'+ route +'/getOrganizationsByUserId',
            data: JSON.stringify({checkAdmin: checkAdmin}),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true
        }).done((data) => {
            if (data && data.ok && data.orgArrForUserInput) {
                this.props.handleOrgResults(data.orgArrForUserInput)
            } else if (data && data.error) {
                console.log(data.error);
            } else {
                app.addAlert('error', 'Error: failed to load organizations.');
            }
        }).fail(() => {
            app.addAlert('error', 'Error: failed to load organizations.');
        });
    }

    render() {
        let organizations;
        if (this.props.orgArrForUserInput) {
            organizations = this.props.orgArrForUserInput.map((org, idx) => {
                return <MenuItem value={idx}
                                 key={idx}
                                 primaryText={org.organizationName}
                                 onTouchTap={this.props.handleOrgSelect.bind(this, idx)}
                />
            });
        } else {
            organizations = [];
        }

        let actions = [
            <FlatButton
                label="Ok"
                onClick={this.props.handleAfterOrgChoose.bind(this)}
                style={{
                    right: 20,
                    bottom: 15,
                    color: 'white',
                    borderRadius: 4,
                    height: 40
                }}
                labelStyle={{fontSize: 13}}
                backgroundColor={'rgba(0,0,0,0.7'}
                hoverColor={'#0091ea'}
            />
        ];

        if(this.props.closeBtnEnabled){
            actions.push(
                <FlatButton
                    label="Cancel"
                    onClick={this.props.handleOrgCloseDialog.bind(this)}
                    style={{
                        right: 20,
                        bottom: 15,
                        color: 'white',
                        borderRadius: 4,
                        height: 40,
                        marginLeft: 4
                    }}
                    labelStyle={{fontSize: 13}}
                    backgroundColor={'rgba(0,0,0,0.7'}
                    hoverColor={'#0091ea'}
                />
            )
        }
        return <Dialog
            title={'Choose Organization'}
            anchorOrigin={{horizontal: 'middle', vertical: 'center'}}
            open={this.props.openChooseOrgDialog}
            modal={false}
            actionsContainerStyle={{backgroundColor: 'rgba(0,0,0,0.04)'}}
            bodyStyle={{background: 'rgba(0,0,0,0.04)'}}
            autoScrollBodyContent={true}
            contentStyle={{maxWidth: '33%', borderRadius: '7px 7px 7px 7px'}}
            titleStyle={{
                fontSize: 18,
                background: 'rgba(0,0,0,0.7)',
                color: 'white', textAlign: 'center',
                borderRadius: '2px 2px 0px 0px',
                textTransform: 'uppercase',
            }}
            actions={actions}
        >
            <div style={{marginLeft: '30px'}}>
                <br/>
                <br/>
                <br/>
                <br/>
                <div style={{width: '100%'}}>Choose organization you wish to work on:</div>
                <br/>
                <DropDownMenu
                    labelStyle={{left: 12}}
                    style={{right: 35, width: '300px'}}
                    anchorOrigin={{
                        horizontal: 'left',
                        vertical: 'bottom'
                    }}
                    value={this.props.curOrgIdx}
                    autoWidth={false}
                    onChange={this.props.handleOrgChange.bind(this)}>
                    {organizations}
                </DropDownMenu>
                <br/>
                <br/>
                <br/>
            </div>
        </Dialog>
    }
}
