
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/intel/domain-finder/site-validator.js')
    .listen({port: 12122, host: '0.0.0.0', pin: 'role:domain-validator', timeout: 999999})
