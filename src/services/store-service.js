const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/store/app-stores')
    .listen({port: 10904, host: '0.0.0.0', timeout: 999999})
    // whois-service.
    .client({port: 10202, host: 'localhost', pin: 'role:whois', timeout: 999999})
    .use('mongo-store', {
        name: 'darkvision',
        host: config.db.hostname,
        port: config.db.port,
        username: config.db.username,
        password: config.db.password
    });


