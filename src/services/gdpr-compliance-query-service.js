const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/gdpr-compliance/gdpr-compliance-query.js')
    .listen({port: 10599, host: 'localhost', pin: 'role:gdprComplianceQuery', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999});
