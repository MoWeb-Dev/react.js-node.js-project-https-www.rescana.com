const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/data-sources/pastebin-scraper')
    .listen({port: 23336, host: '0.0.0.0', pin: 'role:pastebin', timeout: 999999})
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client({port: 11112, host: '0.0.0.0', pin: 'role:mailer', timeout: 999999});
