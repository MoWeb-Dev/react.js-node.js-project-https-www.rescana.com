
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/intel/domain-finder/main.js')
    .listen({port: 12121, host: '0.0.0.0', pin: 'role:domain-finder', timeout: 999999})
