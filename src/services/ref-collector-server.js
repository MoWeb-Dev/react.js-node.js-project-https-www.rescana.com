'use strict';
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const Promise = require('bluebird');
const seneca = require('seneca')();
const helpers = require('../server_modules/utils/utils-helpers');
const path = require('path');
const config = require('app-config');
const app = express();
// app.enable('trust proxy');

module.exports = (function server() {
    let referredLinksArr = [], aid;
    const promiseAct = Promise.promisify(seneca.act, {context: seneca});

    // Every hour, send list of links to DB.
    setInterval(saveToDB, 30000);

    // rfs - Referrer send
    seneca.add('role:dv,cmd:rfs', saveReferrerData);

    seneca.act('role:web', {
        use: {
            // define some routes that start with /dv
            prefix: '/dv',
            // use action patterns where role has the value 'dv' and cmd has some defined value
            pin: {role: 'dv', cmd: '*'},
            map: {
                rfs: {GET: true} // explicitly accepting GETs
            }
        }
    });

    app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        // Get cid from query string of integration.
        if (req.query.aid) {
            aid = req.query.aid;
        }
        next();
    });

    // This is how you integrate Seneca with Express
    app.use(seneca.export('web'));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, '../../../dist/js')));
    app.use(express.query());
    app.use(bodyParser.urlencoded({extended: true, limit: '10mb'}));
    app.use(bodyParser.json({limit: '10mb'}));

    app.listen(3003, function() {
        console.log('Rescana listening on port 3003');
    });

    function saveReferrerData(arg, done) {
        let inList = false;
        let data;

        seneca.client({
            host: config.addresses.storeServiceAddress,
            port: 10904
        }).client({
            host: 'localhost',
            port: 12222
        });

        data = helpers.cleanObject(arg.req$.query);
        data.aid = aid;

        if (!inWhitelist(data.url)) {
            // Create a cache and Check if link is already in list.
            for (let i = 0; i < referredLinksArr.length; i++) {
                if (referredLinksArr[i].url === data.url) {
                    inList = true;
                    break;
                }
            }

            // if not, add to list.
            if (!inList) {
                promiseAct('role:compareApps,compare:app', {
                    customerAppId: data.aid,
                    publicAppURL: data.url
                }).then(function() {
                    console.log('done');
                });
                referredLinksArr.push(data);
            }
        }
        done();
    }

    function saveToDB() {
        const promArr = [];

        // Save each link to DB.
        for (let i = 0; i < referredLinksArr.length; i++) {
            const prom = promiseAct('role:publicstore, set:app', {data: referredLinksArr[i]});
            promArr.push(prom);
        }

        // After all items are saved, Empty list.
        Promise.all(promArr).then(() => {
            // Empty array.
            referredLinksArr.length = 0;
        });
    }

    // Checks if domain exists in predefined whitelist.
    function inWhitelist(url) {
        // TO DO : get whitelist from account.
        const whiteList = ['http://ynet.co.il', 'http://walla.co.il'];
        const domain = helpers.getDomainFromURL(url);
        return whiteList.indexOf(domain) !== -1;
    }
}());
