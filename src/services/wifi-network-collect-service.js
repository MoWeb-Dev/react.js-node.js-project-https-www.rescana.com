require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/open-wifi/wifi-network-collector.js')
    .listen({port: 10399, host: 'localhost', pin: 'role:wifiNetworksCollector', timeout: 999999});
