const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/blacklists/blacklists-searcher.js')
    .listen({port: 10588, host: '0.0.0.0', pin: 'role:blacklistsSearcher', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
;
