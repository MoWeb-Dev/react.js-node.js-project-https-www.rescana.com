module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://localhost:27017/darkvision',
    jobConnectionString: 'mongodb://localhost:27017/jobs',
    storeServiceAddress: 'localhost',//'10.0.0.14,
    schedulerAddress: 'localhost',//'10.0.0.14',
    mailerServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',
    cveImporterServiceAddress: 'localhost',
    website : 'https://qa.rescana.com',
    alertServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.240.0.2',
    circllu: 'http://cve.circl.lu/api/',//'http://10.0.0.18:5000/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
