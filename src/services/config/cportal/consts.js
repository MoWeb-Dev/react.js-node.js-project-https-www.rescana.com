module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: process.env.VT_KEY,
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    blacklistsCollectCronTime: '0 0 * * 3', /* Run every Wednesday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA'
};
