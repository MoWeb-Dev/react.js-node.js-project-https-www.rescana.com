const path = require('path');
module.exports = {
    evidence: path.join(__dirname, '../..', 'evidence/'),
    excelTemplates: path.join(__dirname, '../..', 'excelTemplates/'),
    excelTemplatesWithAnswers: path.join(__dirname, '../..', 'excelTemplatesWithAnswers/'),
    cveReports: path.join(__dirname, '../..', 'cveReports/'),
    intelligenceReports: path.join(__dirname, '../..', 'intelReports/'),
    surveyReports: path.join(__dirname, '../..', 'surveyReports/'),
    intelligenceManualFindings: path.join(__dirname, '../..', 'intelManuals')
};


