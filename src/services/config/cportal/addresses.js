module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://localhost:27017/darkvision',
    jobConnectionString: 'mongodb://localhost:27017/jobs',
    website : 'https://cportal.rescana.com',
    storeServiceAddress: 'localhost', //'10.0.0.40',
    schedulerAddress: 'localhost', //'10.0.0.40',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost', //'10.0.0.30',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://localhost',
    circllu: 'http://cve.circl.lu/api/',
    intelServiceAddresses: ['0.0.0.0'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
