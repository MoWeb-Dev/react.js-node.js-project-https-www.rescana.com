module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: '6040f9e981718a72f8680dfb044cd08971beb1f0702fc3cffe37c5773d9fbe02',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    blacklistsCollectCronTime: '0 0 * * 2', /* Run every Tuesday (weekly)*/
    sessionMaxAge: null,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA'
};
