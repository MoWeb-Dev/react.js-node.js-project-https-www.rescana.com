const path = require('path');
module.exports = {
    mock : false,
    twistPath: path.join(__dirname, '../..', 'dnstwist/dnstwist.py'),
    dnstwistJson:'-j'
};