module.exports = {
    evidence: './evidence',
    excelTemplates: './excelTemplates',
    excelTemplatesWithAnswers: './excelTemplatesWithAnswers',
    cveReports: './cveReports',
    intelligenceReports: './intelReports',
    surveyReports: './surveyReports',
    intelligenceManualFindings: './intelManuals',
    gcloudKeyFile: '/opt/darkvision/rescana-qa-19d99de072c3.json'
};
