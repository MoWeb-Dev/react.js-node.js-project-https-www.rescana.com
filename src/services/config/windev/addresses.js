module.exports = {
    circllu: 'http://cve.circl.lu/api/', //'http://10.0.0.18:5000/api/',
    mongoConnectionString: 'mongodb://localhost:27017/darkvision',
    jobConnectionString: 'mongodb://localhost:27017/jobs',
    website : 'http://localhost:3002',
    dnsTwistAddress: 'localhost',
    storeServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://localhost',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
