module.exports = {
    html: '',
    vtAPI: '6040f9e981718a72f8680dfb044cd08971beb1f0702fc3cffe37c5773d9fbe02',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    blacklistsCollectCronTime: '0 0 * * 4', /* Run every Thursday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA'
};
