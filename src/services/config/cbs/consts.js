module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: 'ede4eeb6d377d4c33718d99ac004797d2e3ab5bc89d8d0f1e8c24b4af8971fd0',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    blacklistsCollectCronTime: '0 0 * * 0', /* Run every Sunday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7
};
