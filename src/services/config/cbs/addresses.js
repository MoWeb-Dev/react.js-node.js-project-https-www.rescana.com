module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://localhost:27017/darkvision',
    jobConnectionString: 'mongodb://localhost:27017/jobs',
    website : 'https://poc.rescana.com',
    storeServiceAddress: 'localhost',//'10.0.0.39',
    schedulerAddress: 'localhost',//'10.0.0.39',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',//'10.0.0.30',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.240.0.2', //'bolt://10.0.0.39',
    circllu: 'http://cve.circl.lu/api/', //'http://10.0.0.18:5000/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddresses: ['localhost']
};
