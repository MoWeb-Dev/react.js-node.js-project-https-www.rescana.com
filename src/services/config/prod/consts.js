module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: '75d28d28d66f8f8b5e8de17faae9d30cff5351cacbeb88802d12e68c5e915080',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    blacklistsCollectCronTime: '0 0 * * 0', /* Run every Sunday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA'
};
