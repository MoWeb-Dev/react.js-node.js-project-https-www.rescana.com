module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://' + process.env.mongoUser + ':' + process.env.mongoPass + '@localhost:27017/darkvision?authMechanism=DEFAULT&authSource=db',
    jobConnectionString: 'mongodb://localhost:27017/jobs',
    website : 'https://portal.rescana.com',
    storeServiceAddress: 'localhost',//'10.0.0.39',
    schedulerAddress: 'localhost',//'10.0.0.39',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',//'10.0.0.30',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.142.0.14', //'bolt://10.0.0.39',
    circllu: 'http://cve.circl.lu/api/', //'http://10.0.0.18:5000/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
