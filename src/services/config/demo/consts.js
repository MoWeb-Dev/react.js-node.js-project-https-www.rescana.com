module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: '535476c1cd97a6d5b8c494fdf8f0456edd1705ea4cb5c280870532b781fef345',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    blacklistsCollectCronTime: '0 0 * * 1', /* Run every Monday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA'
};
