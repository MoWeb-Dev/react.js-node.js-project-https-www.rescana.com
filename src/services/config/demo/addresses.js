module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://localhost:27017/darkvision',
    jobConnectionString: 'mongodb://localhost:27017/jobs',
    website : 'https://demo.rescana.com',
    storeServiceAddress: 'localhost',//'10.0.0.21',
    schedulerAddress: 'localhost',//'10.0.0.21',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    hibpServiceAddress: 'localhost',//'10.0.0.42',
    harvesterServiceAddress: 'localhost',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.142.0.9',
    circllu: 'http://cve.circl.lu/api/', //'http://10.0.0.18:5000/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddresses: ['localhost']//['10.0.0.42']
};
