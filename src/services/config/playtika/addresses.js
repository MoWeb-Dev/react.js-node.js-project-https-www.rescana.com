module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://localhost:27017/darkvision',
    jobConnectionString: 'mongodb://localhost:27017/jobs',
    website : 'https://pl.rescana.com',
    storeServiceAddress: 'localhost', //'10.0.0.41',
    schedulerAddress: 'localhost', //'10.0.0.41',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.142.0.11',
    circllu: 'http://cve.circl.lu/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
