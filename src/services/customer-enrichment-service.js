const config = require('app-config');
require('seneca')({timeout: 900000})
    .use('entity')
    .use('../server_modules/customer-enrich')
    .listen({port: 11104, host: '0.0.0.0', timeout: 900000})
    // whois-service.
    .client({port: 10202, host: 'localhost', pin: 'role:whois', timeout: 900000})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 900000})
    // scraper-service
    .client({port: 10500, host: 'localhost', pin: 'role:scraper', timeout: 900000})
    // screenshot-service
    .client({port: 30500, host: 'localhost', pin: 'role:screenshot', timeout: 900000});

