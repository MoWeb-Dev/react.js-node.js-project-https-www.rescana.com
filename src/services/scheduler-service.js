const config = require('app-config');
const seneca = require('seneca')({timeout: 90000000})
    .use('entity')
    .use('balance-client')
    .use('../server_modules/scheduler.js')
    .listen({port: 11110, host: '0.0.0.0', timeout: 999999})
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    //.client({port: 11104, host: 'localhost', timeout: 999999})
    .client({port: 11113, host: config.addresses.alertServiceAddress, pin: 'role:alertSystem', timeout: 999999})
    .client({port: 10501, host: 'localhost', pin: 'role:phishtank', timeout: 999999})
    .client({port: 10888, host: 'localhost', pin: 'role:bucketCollector', timeout: 999999})
    .client({port: 11889, host: config.addresses.harvesterServiceAddress, pin: 'role:harvester', timeout: 999999})
    .client({port: 10777, host: config.addresses.hibpServiceAddress, pin: 'role:hibp', timeout: 999999})
    .client({port: 10588, host: 'localhost', pin: 'role:blacklistsSearcher', timeout: 999999})
    .client({port: 10587, host: 'localhost', pin: 'role:rssFeeds', timeout: 999999})
    .client({port: 10589, host: 'localhost', pin: 'role:sslCertificatesCollect', timeout: 999999})
    .client({port: 10598, host: 'localhost', pin: 'role:gdprComplianceCollect', timeout: 999999})
    .client({port: 22228, host: 'localhost', pin: 'role:intelQuery', timeout: 999999})
    .client({type: 'balance', pin: 'role:intel', model: 'consume'});

// collect intel service
for (let i = 0; i < config.addresses.intelServiceAddresses.length; i++) {
    seneca.client({
        port: 22226,
        host: config.addresses.intelServiceAddresses[i],
        pin: 'role:intel',
        timeout: 999999
    });
}


