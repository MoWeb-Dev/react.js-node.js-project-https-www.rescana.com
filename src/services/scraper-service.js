const config = require('app-config');
require('seneca')({timeout: 900000})
    .use('entity')
    .use('../server_modules/utils/scraper')
    .listen({port: 10500, host: '0.0.0.0', pin: 'role:scraper', timeout: 900000})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 900000});
