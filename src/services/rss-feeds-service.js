const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/rss-feeds/rss-feeds-collector.js')
    .listen({port: 10587, host: 'localhost', pin: 'role:rssFeeds', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client({port: 22228, host: '0.0.0.0', pin: 'role:intelQuery', timeout: 999999});
