const config = require('app-config');
require('seneca')({timeout: 999999999})
    .use('entity')
    .use('../server_modules/intel/collect-intel')
    .listen({port: 22226, host: '0.0.0.0', pin: 'role:intel', timeout: 999999})
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client({port: 11110, host: config.addresses.schedulerAddress, timeout: 999999})
    .client({port: 11113, host: config.addresses.alertServiceAddress, pin: 'role:alertSystem', timeout: 999999})
    .client({port: 22228, host: '0.0.0.0', pin: 'role:intelQuery', timeout: 999999});

