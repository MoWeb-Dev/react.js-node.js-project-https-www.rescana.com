const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/survey/survey.js')
    .listen({port: 10701, host: '0.0.0.0', pin: 'role:survey', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .use('mongo-store', {
        name: 'darkvision',
        host: config.db.hostname,
        port: config.db.port,
        username: config.db.username,
        password: config.db.password
    });
