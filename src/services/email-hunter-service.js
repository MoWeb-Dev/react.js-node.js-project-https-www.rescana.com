const config = require('app-config');
const seneca = require('seneca')({timeout: 900000})
    .use('entity')
    .use('balance-client')
    .use('../server_modules/data-sources/email-hunter.js')
    .listen({port: 10890, host: '0.0.0.0', pin: 'role:emailHunter', timeout: 900000})
    .client({type: 'balance', pin: 'role:hibp', model: 'consume'});

for (let i = 0; i < config.addresses.hibpServiceAddresses.length; i++) {
    seneca.client({
        port: 10777,
        host: config.addresses.hibpServiceAddresses[i],
        pin: 'role:hibp',
        timeout: 999999
    });
}
