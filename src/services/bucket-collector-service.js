const config = require('app-config');
require('seneca')({timeout: 900000})
    .use('entity')
    .use('../server_modules/data-sources/bucket-collector.js')
    .listen({port: 10888, host: '0.0.0.0', pin: 'role:bucketCollector', timeout: 900000})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 900000});
