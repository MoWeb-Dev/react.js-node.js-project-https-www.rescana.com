const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/ssl-certificates/ssl-certificates-collector.js')
    .listen({port: 10589, host: 'localhost', pin: 'role:sslCertificatesCollect', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client({port: 22228, host: 'localhost', pin: 'role:intelQuery', timeout: 999999});
