const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/open-wifi/wifi-network-query.js')
    .listen({port: 10398, host: 'localhost', pin: 'role:wifiNetworksQuery', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999});
