const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/data-sources/whois-engine')
    .listen({port: 10202, host: '0.0.0.0', pin: 'role:whois', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    // customer-enrichment-service
    .client({port: 11104, host: 'localhost', timeout: 999999});
