const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/ssl-certificates/ssl-certificates-query.js')
    .listen({port: 10590, host: 'localhost', pin: 'role:sslCertificatesQuery', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999});
