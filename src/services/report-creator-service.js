const config = require('app-config');
require('seneca')({
    timeout: 999999
})
    .use('entity')
    .use('../server_modules/reports/report-creator.js')
    .listen({port: 10388, host: '0.0.0.0', pin: 'role:reportCreator', timeout: 999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client({port: 22228, host: '0.0.0.0', pin: 'role:intelQuery', timeout: 999999})
    .client({port: 10587, host: 'localhost', pin: 'role:rssFeeds', timeout: 999999})
    .client({port: 10590, host: 'localhost', pin: 'role:sslCertificatesQuery', timeout: 999999})
    .client({port: 11112, host: '0.0.0.0', pin: 'role:mailer', timeout: 999999})
    .use('mongo-store', {
        name: 'darkvision',
        host: config.db.hostname,
        port: config.db.port,
        username: config.db.username,
        password: config.db.password
    });
