require('seneca')({timeout: 999999})
    .use('../server_modules/utils/mailer')
    .listen({port: 11112, host: '0.0.0.0', pin: 'role:mailer', timeout: 999999});
