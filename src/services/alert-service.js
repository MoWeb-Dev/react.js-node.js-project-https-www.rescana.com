const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/alert-system')
    .listen({port: 11113, host: '0.0.0.0', pin: 'role:alertSystem', timeout: 999999})
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client({port: 11112, host: config.addresses.mailerServiceAddress, pin: 'role:mailer', timeout: 999999});
