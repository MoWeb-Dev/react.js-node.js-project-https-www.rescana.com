const config = require('app-config');
require('seneca')({timeout: 2147483647})
    .use('entity')
    .use('seneca-amqp-transport')
    .use('../server_modules/compare/compare-engine')
    .listen(config.rabbitmq.compareQueue)
    // whois-service.
    .client({port: 10202, host: 'localhost', pin: 'role:whois', timeout: 900000})
    // compare images.
    .client({port: 22223, host: 'localhost', pin: 'role:compareimages', timeout: 900000})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 900000})
    // scraper-service
    .client({port: 10500, host: 'localhost', pin: 'role:scraper', timeout: 900000})
    // screenshot-service
    .client({port: 30500, host: 'localhost', pin: 'role:screenshot, capture:app', timeout: 900000});

