const config = require('app-config');
require('seneca')({timeout: 900000})
    .use('entity')
    .use('../server_modules/compare/compare-images')
    .listen({port: 22223, host: '0.0.0.0', pin: 'role:compareimages', timeout: 900000})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 900000});

