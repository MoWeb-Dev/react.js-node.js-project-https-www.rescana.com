const config = require('app-config');
require('seneca')({
    timeout: 999999
})
    .use('entity')
    .use('../server_modules/intel/query-intel')
    .listen({port: 22228, host: '0.0.0.0', pin: 'role:intelQuery', timeout: 999999})
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client({port: 10587, host: 'localhost', pin: 'role:rssFeeds', timeout: 999999})
    .client({port: 10590, host: 'localhost', pin: 'role:sslCertificatesQuery', timeout: 999999});
