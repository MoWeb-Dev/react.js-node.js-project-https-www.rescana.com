const config = require('app-config');
require('seneca')({timeout: 999999})
    .use('entity')
    .use('../server_modules/data-sources/dnstwist-wrapper')
    .use('seneca-amqp-transport')
    .listen({port: 11111, host: '0.0.0.0', pin: 'role:dnstwist', timeout: 999999})
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999})
    .client(config.rabbitmq.compareQueue)
    // compare images.
    .client({port: 22223, host: 'localhost', pin: 'role:compareimages', timeout: 900000})
    .client({port: 11113, host: config.addresses.alertServiceAddress, pin: 'role:alertSystem', timeout: 999999});


