const config = require('app-config');
require('seneca')({timeout: 999999999})
    .use('entity')
    .use('../server_modules/utils/screenshot')
    .listen({port: 30500, host: '0.0.0.0', pin: 'role:screenshot', timeout: 999999999})
    // store-service.
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 900000});
