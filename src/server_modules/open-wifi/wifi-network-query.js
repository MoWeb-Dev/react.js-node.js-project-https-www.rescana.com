'use strict';
const PromiseB = require('bluebird');
const wifiHelpers = require('./wifi-network-helpers.js');


module.exports = function wifiNetworksQuery() {
    const act = PromiseB.promisify(this.act, {context: this});

    const getWifiNetworksByCompanyIDs = async (msg, respond) => {

        if(msg && msg.companyIDs && Array.isArray(msg.companyIDs) && msg.companyIDs.length > 0) {
            console.log('In getWifiNetworksByCompanyIDs - starting to fetch data');

            const wifiResults = await wifiHelpers.getWifiFindingsFromDB(msg.companyIDs);

            let dataToReturn = wifiResults.data;
            // Add relatedDomains with company name for displaying in UI.
            if(wifiHelpers.isNotEmptyArray(dataToReturn)) {

                const dataWithRelatedCompanies = await wifiHelpers.getWifiFindingsWithRelatedCompanies(msg.companyIDs, dataToReturn, act);

                if(wifiHelpers.isNotEmptyArray(dataWithRelatedCompanies)) {

                    dataToReturn = dataWithRelatedCompanies;
                }
            }

            respond({ok: true, data: dataToReturn});
        }
        else {
            console.log('In getWifiNetworksByCompanyIDs - companyIDs array parameter is missing');

            respond({ok: false});
        }
    };

    this.add('role:wifiNetworksQuery, cmd:getWifiNetworks', getWifiNetworksByCompanyIDs);
};
