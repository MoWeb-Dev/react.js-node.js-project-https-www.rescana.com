'use strict';
const wifiHelpers = require('./wifi-network-helpers.js');


module.exports = function wifiNetworkCollector() {

    const collectWifiByGeoAddress = async (msg, respond) => {

        if(msg && msg.companyID && msg.address) {
            console.log('In collectWifiByGeoAddress - starting to collect data');

            respond({ok: true});

            const geoResult = await wifiHelpers.getWifiNetworksByAddress(msg.address);

            if(wifiHelpers.isNotEmptyArray(geoResult)) {
                console.log('In collectWifiByGeoAddress - starting to save found data');
                const objectToSave = {
                    companyID: msg.companyID,
                    address: msg.address,
                    findings: geoResult
                };

                // Save findings to DB.
                const mongoResult = await wifiHelpers.saveWifiFindingsToDB(objectToSave, 'address');

                if(mongoResult && mongoResult.ok) {
                    console.log('In collectWifiByGeoAddress - Successfully saved wifi findings to DB');
                }
                else {
                    console.log('In collectWifiByGeoAddress - Failed to save wifi findings to DB');
                }
            }
            else {
                console.log('In collectWifiByGeoAddress - No data was found for address: ', msg.address);
            }
        }
        else {
            console.log('In collectWifiByGeoAddress - Some parameters are missing');

            respond({ok: false});
        }
    };

    const collectWifiBySsid = async (msg, respond) => {

        if(msg && msg.companyID && msg.ssid) {
            console.log('In collectWifiBySsid - starting to collect data');

            respond({ok: true});

            const ssidResult = await wifiHelpers.getWifiNetworksBySsid(msg.ssid);

            if(wifiHelpers.isNotEmptyArray(ssidResult)) {
                console.log('In collectWifiBySsid - starting to save found data');
                const objectToSave = {
                    companyID: msg.companyID,
                    ssid: msg.ssid,
                    findings: ssidResult
                };

                // Save findings to DB.
                const mongoResult = await wifiHelpers.saveWifiFindingsToDB(objectToSave, 'ssid');

                if(mongoResult && mongoResult.ok) {
                    console.log('In collectWifiBySsid - Successfully saved wifi findings to DB');
                }
                else {
                    console.log('In collectWifiBySsid - Failed to save wifi findings to DB');
                }
            }
            else {
                console.log('In collectWifiBySsid - No data was found for ssid: ', msg.ssid);
            }
        }
        else {
            console.log('In collectWifiBySsid - Some parameters are missing');

            respond({ok: false});
        }
    };

    this.add('role:wifiNetworksCollector, cmd:collectWifiNetworksByAddress', collectWifiByGeoAddress);
    this.add('role:wifiNetworksCollector, cmd:collectWifiNetworksBySsid', collectWifiBySsid);
};
