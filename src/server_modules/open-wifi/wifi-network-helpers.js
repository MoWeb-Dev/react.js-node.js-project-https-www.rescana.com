'use strict';
const unirest = require('unirest');
const moment = require('moment');
const PromiseB = require('bluebird');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const config = require('app-config');
const {WIFI_WIGLE_API_KEY} = require('../../../config/consts.js');
const {listToArray} = require('../../server/api/api-helpers.js');

const DB_NAME = 'darkvision';
const WIFI_COLLECTION = 'wifi_networks_map';


module.exports.callWigleAPI = (requestType, requestParams) => {
    return new Promise((resolve) => {
        console.log("in callWigleAPI");
        try {
            if(requestType && requestParams) {
                const API_CONFIG = {};
                if(requestType === 'geo' && Array.isArray(requestParams) && requestParams.length === 4) {
                    API_CONFIG.reqRoute = 'network/search';
                    API_CONFIG.reqParams = '?latrange1=' + requestParams[0] + '&latrange2=' + requestParams[1] + '&longrange1=' + requestParams[2] + '&longrange2=' + requestParams[3];
                }
                else if (requestType === 'address' && typeof requestParams === 'string') {
                    API_CONFIG.reqRoute = 'network/geocode';
                    API_CONFIG.reqParams = '?addresscode=' + requestParams;
                }
                else if (requestType === 'ssid' && typeof requestParams === 'string') {
                    API_CONFIG.reqRoute = 'network/search';
                    API_CONFIG.reqParams = '?ssidlike=' + requestParams;
                }
                if(API_CONFIG.reqRoute && API_CONFIG.reqParams) {

                    const req = unirest('GET', // unirest' method
                        'https://api.wigle.net/api/v2/' + API_CONFIG.reqRoute + API_CONFIG.reqParams, // unirest' uri
                        { // unirest' headers
                            'Authorization': WIFI_WIGLE_API_KEY,
                            'accept': 'application/json'
                        },
                        { // unirest' body (for POST method)
                        }
                    );

                    req.end(function (res) {
                        if(res && res.body) {
                            //console.log('Response Result:\n', res.body);
                            resolve(res.body);
                        }
                        else {
                            if(res && res.error) {
                                console.log('Error using Wigle API: ', res.error);
                            }
                            else {
                                console.log('No result returned from Wigle API.');
                            }
                            resolve();
                        }
                    });
                }
                else {
                    console.log('Error in callWigleAPI - invalid parameters were inserted');
                    resolve();
                }
            }
            else {
                console.log('Error in callWigleAPI - some parameters are missing');
                resolve();
            }
        }
        catch (e) {
            console.error('Error calling Wigle API: ', e);
            resolve();
        }
    });
};

module.exports.getWifiNetworksByAddress = (addressForAPI) => {
    return new Promise(async (resolve) => {
        if(addressForAPI) {
            const encodedAddress = encodeURIComponent(addressForAPI);

            console.log("In getWifiNetworksByAddress - starting with encoded param: ", encodedAddress);

            try {
                const getDataByAddress = await module.exports.callWigleAPI('address', encodedAddress);
                if (getDataByAddress && getDataByAddress.results && Array.isArray(getDataByAddress.results)) {
                    console.log('Fetched successfully data by address');
                    const getSpecificGeoLocation = getDataByAddress.results.find((d) => {
                        return (d && d.boundingbox && Array.isArray(d.boundingbox) && d.boundingbox.length === 4);
                    });
                    if (getSpecificGeoLocation) {
                        console.log('Found valid geo data in result: ', getSpecificGeoLocation.boundingbox);
                        try {
                            const getDataByGeo = await module.exports.callWigleAPI('geo', getSpecificGeoLocation.boundingbox);
                            if (getDataByGeo && getDataByGeo.results && Array.isArray(getDataByGeo.results)) {
                                console.log('Fetched successfully data by geo');
                                resolve(getDataByGeo.results);
                            }
                            else {
                                console.log('Fetched empty data by geo');
                                resolve();
                            }
                        }
                        catch (e) {
                            console.error('Error in callWigleAPI-geo: ', e);
                            resolve();
                        }
                    }
                    else {
                        console.log('Did not find any valid geo data in result');
                        resolve();
                    }
                }
                else {
                    console.log('Fetched empty data by address');
                    resolve();
                }
            }
            catch (e) {
                console.error('Error in callWigleAPI-address: ', e);
                resolve();
            }
        }
        else {
            console.log('No address parameter inserted to getWifiNetworksByAddress');
            resolve();
        }
    });
};

module.exports.getWifiNetworksBySsid = (ssidForAPI) => {
    return new Promise(async (resolve) => {
        if(ssidForAPI) {
            const encodedSsid = encodeURIComponent(ssidForAPI);

            console.log("In getWifiNetworksBySsid - starting with encoded param: ", encodedSsid);

            try {
                const getDataBySsid = await module.exports.callWigleAPI('ssid', encodedSsid);
                if (getDataBySsid && getDataBySsid.results && Array.isArray(getDataBySsid.results)) {
                    console.log('Fetched successfully data by ssid');
                    resolve(getDataBySsid.results);
                }
                else {
                    console.log('Fetched empty data by ssid');
                    resolve();
                }
            }
            catch (e) {
                console.error('Error in callWigleAPI-ssid: ', e);
                resolve();
            }
        }
        else {
            console.log('No ssid parameter inserted to getWifiNetworksBySsid');
            resolve();
        }
    });
};

module.exports.saveWifiFindingsToDB = (findingsObj, dataSource) => {
    return new Promise(async (resolve) => {
        if(findingsObj && typeof findingsObj === 'object' && findingsObj.companyID
            && dataSource && ((dataSource === 'address' && findingsObj.address) ||
                (dataSource === 'ssid' && findingsObj.ssid))) {
            findingsObj.createDate = moment().format();

            try {
                const mongoUrl = config.addresses.mongoConnectionString;

                const pConnect = PromiseB.promisify(MongoClient.connect, {context: MongoClient});

                const client = await pConnect(mongoUrl, {useNewUrlParser: true});

                const db = client.db(DB_NAME);

                const wifiCollection = db.collection(WIFI_COLLECTION);

                const pUpdateWifiFindings = PromiseB.promisify(wifiCollection.update, {context: wifiCollection});

                const findQuery = {
                    'companyID': findingsObj.companyID
                };

                if(dataSource === 'address') {
                    findingsObj.address = findingsObj.address.toString().toLowerCase();
                    findQuery.address = findingsObj.address;
                }
                else if(dataSource === 'ssid') {
                    findingsObj.ssid = findingsObj.ssid.toString().toLowerCase();
                    findQuery.ssid = findingsObj.ssid;
                }

                await pUpdateWifiFindings(findQuery,
                    findingsObj,
                    {upsert: true});

                // Close DB connection.
                client.close();

                resolve({ok: true});
            }
            catch (e) {
                console.error('Error in saveWifiFindingsToDB: ', e);
                resolve({ok: false});
            }
        }
        else {
            console.log('Wrong parameters inserted to saveWifiFindingsToDB');
            resolve({ok: false});
        }
    });
};

module.exports.getWifiFindingsFromDB = (companyIDs) => {
    return new Promise(async (resolve) => {
        let findings = [];
        if(isNotEmptyArray(companyIDs)) {
            try {
                const mongoUrl = config.addresses.mongoConnectionString;

                const pConnect = PromiseB.promisify(MongoClient.connect, {context: MongoClient});

                const client = await pConnect(mongoUrl, {useNewUrlParser: true});

                const db = client.db(DB_NAME);

                const wifiCollection = db.collection(WIFI_COLLECTION);

                const pfindWifiFindings = PromiseB.promisify(wifiCollection.find, {context: wifiCollection});

                const findResults = await pfindWifiFindings({
                    'companyID': {
                        $in: companyIDs
                    }
                });

                if(findResults) {
                    const data = await listToArray(findResults);
                    if(data) {
                        findings = data;
                    }
                }

                // Close DB connection.
                client.close();

                resolve({ok: true, data: findings});
            }
            catch (e) {
                console.error('Error in getWifiFindingsFromDB: ', e);
                resolve({ok: false, data: findings});
            }
        }
        else {
            console.log('Wrong parameters inserted to getWifiFindingsFromDB');
            resolve({ok: false, data: findings});
        }
    });
};

module.exports.getWifiFindingsWithRelatedCompanies = (companyIDs, wifiFindings, act) => {
    return new Promise(async (resolve) => {
        if(isNotEmptyArray(companyIDs)) {
            try {
                const companies = await act('role:companies, listByObjectID:company', {
                    query: companyIDs
                });

                if(isNotEmptyArray(companies) && isNotEmptyArray(wifiFindings)) {
                    wifiFindings.map((currFinding) => {
                        if(currFinding && currFinding.companyID) {
                            const matchedCompany = companies.find((c) => {
                                return (c && c.id && c.id === currFinding.companyID);
                            });
                            if(matchedCompany && matchedCompany.companyName) {
                                currFinding.relatedCompanies = [matchedCompany.companyName];
                            }
                        }
                    });
                }

                resolve(wifiFindings);
            }
            catch (e) {
                console.error('Error in getWifiFindingsWithRelatedCompanies: ', e);
                resolve(wifiFindings);
            }
        }
        else {
            console.log('Wrong parameters inserted to getWifiFindingsWithRelatedCompanies');
            resolve(wifiFindings);
        }
    });
};

module.exports.testWigleAPI = async (addressForAPI, expectedResultSsid) => {
    return new Promise(async (resolve) => {
        const getDataByGeo = await module.exports.getWifiNetworksByAddress(addressForAPI);

        if (getDataByGeo && Array.isArray(getDataByGeo)) {
            console.log('Fetched successfully data by geo');
            const getSpecificWifi = getDataByGeo.find((d) => {
                return (d && d.ssid && typeof d.ssid === 'string' && d.ssid === expectedResultSsid);
            });
            if (getSpecificWifi) {
                console.log('Found specific wifi data in result: ', getSpecificWifi);
                resolve(true);
            }
            else {
                console.log('Did not find the specific wifi data in result called "', expectedResultSsid, '"');
                resolve(false);
            }
        }
        else {
            console.log('Fetched empty data by geo');
            resolve(false);
        }
    });
};

// This function checks whether the input is an array or not.
const isExistAndArray = function(data) {
    return !!(data && Array.isArray(data));
};
module.exports.isExistAndArray = isExistAndArray;

// This function checks whether the input is an array with at least 1 item or not.
const isNotEmptyArray = function(data) {
    return (isExistAndArray(data) && data.length > 0);
};
module.exports.isNotEmptyArray = isNotEmptyArray;
