const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const sinon = require('sinon');
const wifiHelpers = require('../wifi-network-helpers.js');

describe('Open Wifi API', function() {
    describe('Check API', function() {
        const addressForAPI = 'Shlabim, Tel Aviv';
        const dataByAddress_boundingbox = [32.048757, 32.049693, 34.7637229, 34.7643393];
        const expectedResultSsid = 'falafelgina';

        beforeEach(() => {
            // In Scheduler - run all companies once in 3 days and collect wifi networks by main domain names. Also, run on company will call wifi collect.
            const callWigleAPIStub = sinon.stub(wifiHelpers, 'callWigleAPI');
            callWigleAPIStub.withArgs('address', encodeURIComponent(addressForAPI)).resolves({results: [{boundingbox: dataByAddress_boundingbox}]});
            callWigleAPIStub.withArgs('geo', dataByAddress_boundingbox).resolves({results: [{ssid: expectedResultSsid}]});
        });

        afterEach(() => {
            wifiHelpers.callWigleAPI.restore();
        });

        it('Get Wifi by Address', async function() {
            this.timeout(30000);

            const testing = await wifiHelpers.testWigleAPI(addressForAPI, expectedResultSsid);

            return expect(testing).to.equals(true);
        });
    });
});
