'use strict';
const request = require('request-promise');
const helpers = require('../utils/utils-helpers');
const Promise = require('bluebird');
const options = {
    method: 'GET',
    uri: 'http://data.phishtank.com/data/caa8fd6b6fbe770e8fabdd60a92c1b0ca436b1a735acbac7f9cb8dd3410513a8/online-valid.json',
    json: true
};

module.exports = function Phishtank() {
    const act = Promise.promisify(this.act, {context: this});
    const phishAppURLs = [];
    const getPhishingSites = (msg, respond) => {
        console.log('In getPhishingSites');
        // respond to Scheduler that Phishtank fetch has started.
        respond();
        // let brand = helpers.getBrandName(msg.app.url);
        request(options)
            .then((apps) => {
                console.log('Got results from Phishtank API. Now saving to DB.');
                return act('role:publicstore, insertPhishtankSites:app', {apps: apps});
            })
            .catch((err) => {
                console.error('Error in getPhishingSites - request GET from Phishtank API: ', err);
            })
            .then((res) => {
                if (res && res.ok) {
                    console.log('Succesfuly fetched Phishing Sites and inserted to DB.');
                } else if (res && res.err) {
                    console.error('Error in getPhishingSites - insertPhishtankSites to DB: ', res.err);
                } else {
                    console.log('Error in getPhishingSites - insertPhishtankSites to DB failed.');
                }
            })
            .catch((e) => {
                console.error('Error in getPhishingSites - insertPhishtankSites: ', e);
            });
    };

    const setCustomerAppFromPT = (msg, respond) => {
        let brand = '';
        act('role:customerstore, list:app', {query: {}})
            .then((apps) => {
                return Promise.map(apps, (app) => {
                    if (app && app.url) {
                        brand = helpers.getBrandName(app.url);
                        // Check if brand name exists in phishtank.
                        return act('role:publicstore, listFromPhish:app', {query: {brandName: brand, aid: app.aid}});
                    }
                });
            })
            .then((results) => {
                return Promise.each(results, (apps) => {
                    return Promise.each(apps, (app) => {
                        // check if publicapp exists.
                        return act('role:publicstore, list:app', {query: {url: app.url}})
                            .then((publicApps) => {
                                // If exists, update
                                if (publicApps[0]) {
                                    publicApps[0].modifiedTime = new Date().getTime();
                                    return act('role:publicstore, update:app', {
                                        data: publicApps[0]
                                    });
                                } else {
                                    // else, create new.
                                    const newApp = app;
                                    delete newApp.id;
                                    delete newApp._id;
                                    phishAppURLs.push(newApp);
                                    return act('role:publicstore, update:app', {
                                        data: newApp
                                    });
                                }
                            });
                    });
                });
            })
            .then(() => {
                sendNotification(phishAppURLs, this);
                respond();
            });
    };

    this.add('role:phishtank, get:phishingSites', getPhishingSites);
    this.add('role:phishtank, set:customerAppFromPT', setCustomerAppFromPT);
};

function sendNotification(newApps, context) {
    const act = Promise.promisify(context.act, {context: context});
    let text = '<!DOCTYPE html><html><head></head><body><div>';
    text += '<p>Phishing sites were found:</p><br/>';
    for (let i = 0; i < newApps.length; i++) {
        text += newApps[i].url + '<br/>';
    }
    text += '<br/><b>Rescana</b>';
    text += '</div></body></html>';
    const msgObj = {
        to: 'yuval@rescana.com',
        subject: 'Phishing website found',
        text: text
    };

    if (newApps && newApps.length > 0) {
        act('role:mailer, cmd:send', msgObj)
            .then(() => {
                console.log('Phishing attacker websites Email has been sent');
            });
    }
}
