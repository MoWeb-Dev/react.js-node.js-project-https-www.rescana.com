'use strict';
const request = require('request-promise');
const config = require('app-config');
const Promise = require('bluebird');
const consts = require('../../../config/consts');
const neo4jDriver = require('neo4j-driver').v1;
const neo4jRetried = require('@ambassify/neo4j-retried');
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass),
    {maxConnectionLifetime: 20 * 60 * 1000,
        connectionTimeout: 1000 * 45,
        connectionAcquisitionTimeout: 600000,
        maxTransactionRetryTime: 10000,
        connectionPoolSize: 1000}), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});
const neoHelpers = require('helpers');
const moment = require('moment');

module.exports = function emailHunter() {
    const act = Promise.promisify(this.act, {context: this});

    const findAndSaveEmails = (msg, respond) => {
        const relArr = [];
        let foundEmails;

        const options = {
            uri: 'https://api.hunter.io/v2/domain-search',
            qs: {
                domain: msg.query && msg.query.domain || '',
                api_key: consts.HUNTER_KEY,
                limit: 1000,
                offset: 0
            },
            json: true // Automatically parses the JSON string in the response
        };

        const date = moment().format();

        const metaData = {
            resolve_time: date, // Set once and can't change in query
            scanTime: date // Set every scan.
        };

        return request.get(options)
            .then((emails) => {
                foundEmails = emails;
                console.log(emails.data.domain, ' has ', emails.data.emails.length, ' emails');
                for (let i = 0; i < emails.data.emails.length; i++) {
                    const relData = {
                        fromNodeLabelName: 'domain',
                        fromNodeProps: {hostname: emails.data.domain},
                        newNodeLabelName: 'email',
                        relationshipLabel: 'email',
                        data: metaData,
                        toNodeLabel: 'email',
                        toNodeType: 'email',
                        toNodeProps: {email: emails.data.emails[i].value}
                    };

                    relArr.push(relData);
                }

                return neoHelpers.createNodeAndRelationship(relArr);
            })
            .then(() => {
                const emailsOnly = [];
                for (let i = 0; i < foundEmails.data.emails.length; i++) {
                    emailsOnly.push({email: foundEmails.data.emails[i].value});
                }

                if (emailsOnly) {
                    return act('role:hibp,cmd:runMultiHibp', {query: {emails: emailsOnly}});
                } else {
                    return Promise.resolve();
                }
            })
            .catch((err) => {
                console.log(err);
                respond(null, {ok: false});
            })
            .then(() => {
                respond(null, {ok: true});
            });
    };

    this.add('role:emailHunter,cmd:findEmails', findAndSaveEmails);
};

