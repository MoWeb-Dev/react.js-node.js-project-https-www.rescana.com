'use strict';
const cheerio = require('cheerio');
const request = require('request');
const Repeat = require('repeat');
const helpers = require('../utils/utils-helpers');
const shortid = require('shortid');
const config = require('app-config');
// const sleep = require('sleep');

let expressions = [];

const frequency = {
    wait: 5, // second to wait before each request
    every: {
        unit: 'minutes',
        quantity: 3 // how often load recent pastes and scrape them
    },
    start: {
        unit: 'sec',
        quantity: 5 // how long until scraper starts
    }
};

const logging = true; // Log the Pastebin ID each time before scraping contents.
const log = [];

module.exports = function Pastebin() {
    const callback = (url, html, raw, firstKeywordFound, time, title, companyName) => { // define callback when paste found that matches expressions.
        const msg = 'Found keyword: ' + firstKeywordFound + ', posted: ' + time + ', at ' + url;

        let text = '<!DOCTYPE html><html><head></head><body><div>';
        text += '<p>A new pastebin has been found that could contain infromtaion related to : ' + companyName + '</p>';
        text += '<p>Keyword found : ' + firstKeywordFound + '</p>';
        text += '<ul>';
        text += '<li><a href="' + url + '" target="_blank">' + url + '</a></li>';
        text += '</ul>';

        text += '<br/><a href="' + config.addresses.website + '" target="_blank">' + config.addresses.website + '</a>';
        text += '</div></body></html>';

        const msgObj = {
            to: 'yuval@rescana.com',
            subject: 'New Pastebin url found',
            text: text
        };

        let keywordFound = firstKeywordFound.toString();
        keywordFound = keywordFound.replace('/i', '').replace('\\.', '.').replace('/', '');
        this.act('role:pastes, set:paste', {
            data: {
                'lid': shortid.generate(),
                'url': url,
                'title': title,
                'keyword': keywordFound,
                'time': new Date(),
                'hideFrom': []
            }
        }, () => {
            console.log('Saved to DB');
        });

/*        this.act('role:mailer, cmd:send', msgObj, () => {
            console.log(msg);
            console.log('Pastebin URL has been sent');
        });*/
        log.push(msg);
    };

    // --- End params
    const known = [];
    let keyword = ''; // you can ignore this.
    let companyName = ''; // you can ignore this.
    const match = function(string) {
        let size = expressions.length, i = 0;
        for (; i < size; i++) {
            if (string.match(expressions[i].keyword)) {
                keyword = expressions[i].keyword;
                companyName = expressions[i].company;
                return true;
            }
        }
        return false;
    };

    const search = function(url) {
        return url === this;
    };

    const requestScraping = () => {
        request.get({url: 'http://pastebin.com/archive'}, (err, httpResponse, body) => {
            const $ = cheerio.load(body);
            const parent = $('#content_left').find('table').children('tr');
            const size = parent.length;
            let url = {}, time = '', title = '';
            for (let i = 1; i < size; i++) {
                if (parent.eq(i).children('td').eq(0).find('a').attr('href')) {
                    url = parent.eq(i).children('td').eq(0).find('a').attr('href').split('/')[1];
                    title = parent.eq(i).children('td').eq(0).find('a').text();
                    time = parent.eq(i).children('td').eq(1).html();
                    if (!known.find(search, url)) {
                        known.push(url);
                        (logging ? console.log('Found URL: ' + url) : '');
                        // sleep.sleep(frequency.wait);
                        request.get({
                            url: 'http://pastebin.com/' + url + '/',
                            qs: {time: time, title: title}
                        }, (err, response, body) => {
                            if (err) {
                                console.log('Error:', err);
                            }
                            const filter = (body ? (match(body)) : false);
                            if (filter && response.req) {
                                const paste = response.req.path.split('/')[1];
                                const time = response.req.path.split('?time=')[1].split('&amp;title=')[0];
                                const title = response.req.path.split('&amp;title=')[1] || 'Pastebin finding';
                                callback('pastebin.com/' + paste + '/', body, $('#paste_code').eq(0).html(), keyword, time, title, companyName);
                            }
                            // sleep.sleep(1);
                        });
                    } else {
                        console.log('Skipped URL: ' + url);
                    }
                }
            }
            console.log('Restarting scraper...');
        });
    };

    const init = () => {
        //expressions = helpers.fetchKeywords();
        helpers.loadKeywords(this)
            .then((loadedKeywords) => {
                expressions = expressions.concat(loadedKeywords);
                requestScraping();
            })
            .catch((e) => {
                console.log('init: ', e);
                requestScraping();
            });
    };

    this.ready(() => {
        Repeat(init).every(frequency.every.quantity, frequency.every.unit).start.in(frequency.start.quantity, frequency.start.unit);
        console.log('Running Pastebin Scraper...');
    });
};
