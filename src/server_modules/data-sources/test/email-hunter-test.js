const config = require('app-config');
const Seneca = require('seneca');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(require('chai-things'));
chai.use(chaiAsPromised);
const expect = chai.expect;
const sinon = require('sinon');
const request = require('request-promise');
const intelHelpers = require('../../utils/intel-helpers');
const neoHelpers = require('helpers');

describe('Email hunter', function(fin) {
    const seneca = test_seneca(fin);

    beforeEach(() => {
        sinon.stub(request, 'get').resolves({
            'data': {
                'domain': 'example.com',
                'webmail': false,
                'pattern': null,
                'organization': null,
                'emails': [{
                    'value': 'moshe@example.com',
                    'type': 'personal',
                    'confidence': 99,
                    'sources': [
                        {
                            'domain': 'dev.stackstack.com',
                            'uri': 'http://dev.stackstack.com/bla',
                            'extracted_on': '2017-11-19',
                            'last_seen_on': '2017-11-21',
                            'still_on_page': true
                        },
                        {
                            'domain': 'milab.idc.ac.il',
                            'uri': 'http://milab.idc.ac.il/team/mosh2',
                            'extracted_on': '2017-10-03',
                            'last_seen_on': '2017-10-31',
                            'still_on_page': true
                        }
                    ],
                    'first_name': 'Moshe',
                    'last_name': 'Bla',
                    'position': 'Product Design',
                    'linkedin': 'https://www.linkedin.com/in/mosh2/',
                    'twitter': null,
                    'phone_number': null
                }, {
                    'value': 'moshe2@example.com',
                    'type': 'personal',
                    'confidence': 99,
                    'sources': [
                        {
                            'domain': 'dev.stackstack.com',
                            'uri': 'http://dev.stackstack.com/bla',
                            'extracted_on': '2017-11-19',
                            'last_seen_on': '2017-11-21',
                            'still_on_page': true
                        },
                        {
                            'domain': 'milab.idc.ac.il',
                            'uri': 'http://milab.idc.ac.il/team/stam',
                            'extracted_on': '2017-10-03',
                            'last_seen_on': '2017-10-31',
                            'still_on_page': true
                        }
                    ],
                    'first_name': 'Moshe2',
                    'last_name': 'bla2',
                    'position': 'Product Design',
                    'linkedin': 'https://www.linkedin.com/in/moshe/',
                    'twitter': null,
                    'phone_number': null
                }]
            },
            'meta': {
                'results': 0,
                'limit': 100,
                'offset': 0,
                'params': {
                    'domain': 'example.com',
                    'company': null,
                    'type': null,
                    'offset': 0,
                    'seniority': null,
                    'department': null
                }
            }
        });

        sinon.stub(neoHelpers, 'createNodeAndRelationship');
    });

    afterEach(() => {
        request.get.restore();
        neoHelpers.createNodeAndRelationship.restore();
    });

    it('role:emailHunter,cmd:findEmails - Get all emails from email hunter', function(fin) {
        setTimeout(() => {
            // Create a Seneca instance for testing.
            seneca
                .gate()
                // Send an action, and validate the response.
                .act({
                    role: 'emailHunter',
                    cmd: 'findEmails',
                    query: {
                        domain: 'example.com'
                    }
                }, function(ignore, result) {
                    return expect(result).to.deep.equal({ok: true});
                })

                // Under gating, `ready` will wait until all actions have completed.
                .ready(fin);
        }, 300);
    });
});

// Construct a Seneca instance suitable for unit testing
function test_seneca(fin) {
    return Seneca({log: 'test'})

    // activate unit test mode. Errors provide additional stack tracing context.
    // The fin callback is called when an error occurs anywhere.
        .test(fin)

        .use('basic')

        .use('entity')

        // Load the microservice business logic
        .use(require('../email-hunter'))

        .add('role:hibp,cmd:runMultiHibp', function(msg, reply) {
            reply();
        });
}

