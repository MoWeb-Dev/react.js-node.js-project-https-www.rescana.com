const config = require('app-config');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(require('chai-things'));
chai.use(chaiAsPromised);
// const pastescraper = require('../pastebin-scraper');
const helpers = require('../../utils/utils-helpers');
const expect = chai.expect;


describe('paste bin scraper', function() {
    it('fetchKewords() - should return all keyword to look for in scraped pages', function() {
        // this.timeout(15000);
        const keywordsArr = helpers.fetchKeywords();
        expect(keywordsArr).to.be.an('array');
    });
});


