'use strict';
const helpers = require('../utils/utils-helpers');
const neo4jDriver = require('neo4j-driver').v1;
const parseString = require('xml2js').parseString;
const request = require('request-promise');
const config = require('app-config');
const Promise = require('bluebird');
const shortid = require('shortid');
const moment = require('moment');
const neoHelpers = require('helpers');
const neo4jRetried = require('@ambassify/neo4j-retried');
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass),
    {maxConnectionLifetime: 20 * 60 * 1000,
        connectionTimeout: 1000 * 45,
        connectionAcquisitionTimeout: 600000,
        maxTransactionRetryTime: 10000,
        connectionPoolSize: 1000}), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});
const PromiseThrottle = require('promise-throttle');

module.exports = function bucketCollector() {
    const act = Promise.promisify(this.act, {context: this});

    const findAndSaveBuckets = (msg, respond) => {
        act('role:bucketCollector,cmd:getAllPotentialHostnames')
            .then((domains) => {
                let subsArr = [];
                let domainArr = [];
                const scanId = shortid.generate();

                if (respond) {
                    // To prevent Timeout we respond on entry.
                    respond();
                }

                const promiseBucketsThrottle = new PromiseThrottle({
                    requestsPerSecond: 0.5, // half requests per second
                    promiseImplementation: Promise
                });

                domains = removeDuplicatesBy((domain) => domain.hostname, domains);

                Promise.each(domains, (domain) => {
                    domainArr = domainArr.concat(createDomainOptions(domain.hostname));
                }).then(() => {
                    subsArr = [...new Set(domainArr)];
                    const data = {
                        resolve_time: moment().format(), // Set once and can't change in query
                        scanTime: moment().format(), // Set every scan.
                        scanId: scanId
                    };

                    return Promise.each(subsArr, (sub) => {
                        return promiseBucketsThrottle.add(() => {
                            return new Promise((resolve) => {
                                const opts = {};
                                opts.uri = 'http://' + sub.s3;
                                return request(opts)
                                    .then((response) => {
                                        return response;
                                    })
                                    .then((xml) => {
                                        return parseXML(xml);
                                    })
                                    .catch((err) => {
                                        const xml = err.error;
                                        return parseXML(xml);
                                    })
                                    .then((result) => {
                                        const relData = {
                                            fromNodeProps: {hostname: sub.domain},
                                            data: data,
                                            toNodeLabel: 'bucket',
                                            toNodeType: 'bucket',
                                            relationshipLabel: 'bucket',
                                            toNodeProps: {bucket: sub.s3}
                                        };

                                        if (result && result.ListBucketResult) {
                                            console.log(sub.s3, ': ', result.ListBucketResult);
                                            // relData.toNodeProps = {result: result.ListBucketResult, domain: sub.s3};
                                            return neoHelpers.createNodeAndRelationship(relData).then(() => {
                                                return {data: 'Public', domain: sub.s3};
                                            });
                                        }
                                        if (result && result.Error && result.Error.Code[0] === 'NoSuchBucket') {
                                            console.log(sub.s3, ': ', 'NoSuchBucket');
                                            return Promise.resolve();
                                        } else if (result && result.Error && result.Error.Code[0]) {
                                            console.log(sub.s3, ': ', result.Error.Code[0]);
                                            // relData.toNodeProps = {result: result.Error.Code[0], domain: sub.s3};
                                            return neoHelpers.createNodeAndRelationship(relData)
                                                .then(() => {
                                                    return {data: result.Error.Code[0], domain: sub.s3};
                                                });
                                        } else {
                                            console.log(sub, ': ', result);
                                            return Promise.resolve();
                                        }
                                    })
                                    .then((result) => {
                                        if (result) {
                                            const relData = {
                                                fromNodeLabelName: 'bucket',
                                                fromNodeProps: {bucket: result.domain},
                                                data: data,
                                                toNodeLabel: 'bucket_status',
                                                toNodeType: 'bucket_status',
                                                relationshipLabel: 'bucket_status',
                                                toNodeProps: {bucket_status: result.data, s3: result.domain}
                                            };

                                            return neoHelpers.createNodeAndRelationship(relData)
                                                .then(()=> {
                                                    return resolve();
                                                })
                                                .catch(() => {
                                                    return resolve();
                                                });
                                        } else {
                                            return resolve();
                                        }
                                    });
                            });
                        });
                    });
                }).then(() => {
                    console.log('Done Collecting buckets');
                }).catch((e) => {
                    console.log('Error with bucketCollector: ', e);
                    respond();
                });
            });
    };


    this.add('role:bucketCollector,cmd:getAllPotentialHostnames', getAllPotentialHostnames);
    this.add('role:bucketCollector,cmd:findBuckets', findAndSaveBuckets);
    findAndSaveBuckets();
};


const parseXML = (xml) => {
    return new Promise((resolve) => {
        parseString(xml, function(err, result) {
            resolve(result);
        });
    });
};

const getAllPotentialHostnames = (msg, respond) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();
        const query = 'MATCH path = ((n)-[p:IP]->(IP:IP)-[i:isp]->(isp:isp)) WHERE n.hostname <> "null" AND isp.finding="Amazon.com" RETURN n.hostname as hostname';
        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(query);
        });

        return readTxResultPromise
            .then((result) => {
                session.close();

                const data = result.records.map((record) => {
                    const dataObj = {};

                    for (let i = 0; i < record._fields.length; i++) {
                        dataObj[record.keys[i]] = record._fields[i];
                    }

                    return dataObj;
                });

                return resolve(respond(data));
            })
            .catch((error) => {
                console.error('createIndex: ', error);
                return reject(respond());
            });
    });
};

const createDomainOptions = (domain) => {
    const optionsArr = [];
    let newStr = '';

    const newDomain = helpers.getDomainFromURL(domain);
    optionsArr.push({domain: domain, s3: newDomain + '.s3.amazonaws.com'});
    newStr = newDomain.replace('-', '');
    if (newStr !== newDomain) {
        optionsArr.push({domain: domain, s3: newStr + '.s3.amazonaws.com'});
    }

    const domainWithoutTld = helpers.getDomainFromURLWithoutTld(domain);
    optionsArr.push({domain: domain, s3: domainWithoutTld + '.s3.amazonaws.com'});
    newStr = domainWithoutTld.replace('-', '');
    if (newStr !== domainWithoutTld) {
        optionsArr.push({domain: domain, s3: newStr + '.s3.amazonaws.com'});
    }

    const domainAndSub = helpers.getDomainAndSubFromURL(domain);
    if (domainAndSub) {
        optionsArr.push({domain: domain, s3: domainAndSub + '.s3.amazonaws.com'});
        newStr = domainAndSub.replace('-', '');
        if (newStr !== domainAndSub) {
            optionsArr.push({domain: domain, s3: newStr + '.s3.amazonaws.com'});
        }
    }

    return optionsArr;
};

const removeDuplicatesBy = (keyFn, array) => {
    const mySet = new Set();
    return array.filter(function(x) {
        let key = keyFn(x), isNew = !mySet.has(key);
        if (isNew) mySet.add(key);
        return isNew;
    });
};
