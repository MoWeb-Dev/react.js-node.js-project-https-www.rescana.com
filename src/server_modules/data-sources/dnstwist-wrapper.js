'use strict';
require('shelljs/global');
const helpers = require('../utils/utils-helpers');
const Promise = require('bluebird');
const config = require('app-config');
const exec = require('child_process').exec;
const async = require('async');
const dnstwistMock = require('../../mock/dnstwist-result');
const timeConfig = require('../../../config/time-config.json');
const threadNumber = 3;
// const deepDiff = require('deep-diff');
// const _ = require('lodash');

// dnstwist wrapper - will drop to shell and run dnstwist for analysis
/* example json result

 [
 {
 "dns-a": "212.143.2.227",
 "dns-mx": "mr51-iz.poalim.co.il",
 "dns-ns": "dns.netvision.net.il",
 "domain-name": "poalim.co.il",
 "fuzzer": "Original*",
 "geoip-country": "Israel",
 "mx-spy": true
 },
 {
 "dns-a": "209.15.13.134",
 "dns-mx": "mail.b-io.co",
 "dns-ns": "ns1.dnslink.com",
 "domain-name": "poalim.co-il.com",
 "fuzzer": "Various",
 "geoip-country": "United States",
 "mx-spy": true
 }
 ]


 */

module.exports = function DnsTwist() {
    let seneca = this;
    let customerDomain;
    let running = false;
    const act = Promise.promisify(seneca.act, {context: this});

    let coolOffTime = timeConfig.coolOffTime; // The time the app must wait until we can run dnsTwist on it.
    if (!coolOffTime) {
        coolOffTime = 21600000;
    }
    let twistIntervals = timeConfig.twistIntervals;
    if (!twistIntervals) {
        twistIntervals = 60000;
    }

    seneca.options({timeout: 999999999});
    this.ready(function() {
        // if coolOffTime is smaller then intervals, twist will run on same app every time.
        if (coolOffTime < twistIntervals) {
            coolOffTime = twistIntervals + 100;
        }
        async.forever(
            function(next) {
                runDnsTwistJobs(next);
            },
            function(err) {
            }
        );
    });

    async function runDnsTwistJobs(next) {
        console.log('In runDnsTwistJobs, running:', running);
        let res;
        if (!running) {
            running = true;
            try {
                res = await act('role:customerstore, getOldestDnsTwistCustomerApps:app', {coolOffTime: coolOffTime});
            } catch (e) {
                console.log('Error in role:customerstore, getOldestDnsTwistCustomerApps:app:', e);
                running = false;
                next();
            }

            try{
                if (res && res.data && Object.keys(res.data).length !== 0) {
                    console.log('Found ', Object.keys(res.data).length, 'results');
                    await act('role:dnstwist, twist:customerApp', {data: res.data});
                    next();
                } else {
                    running = false;
                    setTimeout(() => {
                        next();
                    }, 30000);
                }
            }catch(e){
                console.log('Error in role:dnstwist, twist:customerApp:', e);
                running = false;
                next();
            }
        }
    }

    // first publish function that accepts an app id for work
    seneca.add('role:dnstwist, twist:customerApp', async (msg, respond) => {
        console.log('In dnsTwist, customer data:', msg.data);
        const customerApp = msg.data;

        if (!customerApp || !customerApp.url) {
            respond();
        }

        try {
            // Give public app an id and mark it as being worked on.
            await act('role:customerstore, update:app', {data: {id: customerApp.id, markedForDnsTwist: true}});
            customerDomain = helpers.getDomainFromURL(customerApp.url);

            if (config.dnstwist.mock) {
                console.log('Running mock data');
                runMockTwist(customerApp, respond);
            } else {
                console.log('Running twist with real data');
                runTwist(customerDomain, false, threadNumber, customerApp, respond);
            }
        } catch (e) {
            console.error('Error role:dnstwist, twist:customerApp: ', e);
        }
    });

    seneca.add('role:dnstwist, twist:publicApp', (msg, respond) => {
        console.log('In dnsTwist, public data:', msg.data);
        const publicApp = msg.data;

        if (!publicApp) {
            respond();
        }

        const publicDomain = helpers.getDomainFromURL(publicApp.url);
        console.log('Entering runTwist');

        runTwist(publicDomain, true, threadNumber, publicApp, respond);
    });

    function runMockTwist(customerApp, respond) {
        const ts = new Date().getTime();

        dnstwistMock.aid = customerApp.aid;
        dnstwistMock.timestamp = ts;

        saveApp(dnstwistMock, respond);
    }

    // run dns twist
    async function runTwist(appDomain, isWatchedPublicApp, threadNum, app, respond) {
        if (isWatchedPublicApp) {
            // TODO: run dnstwist.py without permutations
            console.log('dnstwist-wrapper.js, runTwist, INFO: Running dnstwist.py without permutations');

            const ts = new Date().getTime();
            const result = {
                'aid': app.aid,
                'dnstwistGeoIPCountry': '',
                'url': appDomain,
                'dnstwistDnsARecord': '',
                'dnstwistDns-mx': '',
                'dnstwistDns-ns': '',
                'dnstwistFuzzer': '',
                'dnstwistscore': '',
                'dnstwistMxSpy': '',
                'dnstwistSsdeep': '',
                'timestamp': ts,
                'isWatched': isWatchedPublicApp
            };

            console.log('dnstwist-wrapper.js, runTwist, INFO: Saving public app...');

            await saveApp(result, respond);
        } else {
            // Async call to exec()
            /* TODO: need to add this : ' --dictionary /home/ubuntu/darkvision/dnstwist/database/effective_tld_names.dat */
            let resultArr = [];

            let commandStr = config.dnstwist.twistPath + ' -r -t ' + threadNum + ' --format json --registered ' + appDomain + ' --ssdeep';
            let stdout;
            try {
                console.log('In exec');
                stdout = await execute(commandStr);
                console.log('Program output:', stdout);
            } catch (e) {
                console.error(`exec error: ${e}`);
                return;
            }
            // store the suspicious domains and their data in the public store
            // aid, url, dnstwistScore
            const output = JSON.parse(stdout);

            for (let i = 0; i < output.length; i++) {
                (function(i) {
                    if (output[i]['domain-name'] && output[i]['domain-name'].indexOf(appDomain) !== -1) {
                        return;
                    }
                    // score calc - base is 30, add 10 for mx, GeoIP?, mxspy adds 10, ssdeep ?
                    let dnstwistscore = 30;

                    if (output[i]['dns-mx']) {
                        dnstwistscore = +10;
                    }
                    if (output[i]['mx-spy']) {
                        dnstwistscore = +10;
                    }
                    const ts = new Date().getTime();
                    const result = {
                        'aid': app.aid,
                        'url': output[i]['domain-name'],
                        'dnstwistDnsARecord': output[i]['dns-a'],
                        'dnstwistDns-mx': output[i]['dns-mx'],
                        'dnstwistDns-ns': output[i]['dns-ns'],
                        'dnstwistFuzzer': output[i]['fuzzer'],
                        'dnstwistGeoIpCountry': output[i]['geoip-country'],
                        'dnstwistscore': dnstwistscore,
                        'dnstwistMxSpy': output[i]['mx-spy'],
                        'dnstwistSsdeep': output[i]['ssdeep-score'],
                        'timestamp': ts,
                        'isWatched': isWatchedPublicApp
                    };

                    resultArr.push(result);
                })(i);
            }

            const newPublicApps = [];
            let apps;
            try {
                apps = await act('role:publicstore, list:app', {query: {}});
            } catch (e) {
                console.error('Error in role:publicstore, list:app: ', e);
            }
            const updatePublicAppsArr = [];

            await asyncForEach(resultArr, async (result) => {
                let addToUpdateList = false;
                if (result.url) {
                    console.log('Adding ', result.url);
                    await asyncForEach(apps, async (appItem) => {
                        if (appItem) {
                            if (appItem.dnsTwistTimeStamp > new Date().getTime() + 6000) {
                                await act('role:publicstore, update:app', {
                                    data: {
                                        id: appItem.id,
                                        markedForDnsTwist: false,
                                        modifiedTime: new Date().getTime()
                                    }
                                });
                            }

                            if (result.url === appItem.url) {
                                console.log('App exists in public list.');
                                result.id = appItem.id;
                                updatePublicAppsArr.push(result);
                                addToUpdateList = true;
                            }
                        } else {
                            addToUpdateList = true;
                        }
                    });

                    if (!addToUpdateList) {
                        console.log('App does not exist in public list.');
                        updatePublicAppsArr.push(result);
                        newPublicApps.push(result);
                    }
                }
            });

            const msgObj = {customerApp: app, newPublicApps: newPublicApps};

            if (newPublicApps.length) {
                await act('role:alertSystem, alert:newPublicAppsFound', {data: msgObj});
            }

            await act('role:publicstore, update:app', {data: {dataArr: updatePublicAppsArr}});
            console.log('Updated found apps to public apps');
            const date = new Date().getTime();
            let publicApps;
            await act('role:customerstore, update:app', {
                data: {
                    id: app.id,
                    dnsTwistTimeStamp: date,
                    lastModified: date,
                    markedForDnsTwist: false
                }
            });

            publicApps = await act('role:publicstore, list:app', {query: {'aid': app.aid}});

            await asyncForEach(publicApps, async (publicApp) => {
                console.log('Comparing! aid:', app.aid);
                console.log(app.id);
                console.log(publicApp.url);
                try {
                    await act('role:compareApps,compareNow:app', {
                        'customerAppId': app.id,
                        'publicAppURL': publicApp.url
                    });
                    console.log('done comparing');
                } catch (e) {
                    console.log('CHANGING TO NOT RUNNING****');
                    running = false;
                    console.log('Updated Customer app');
                    respond();
                }
            });
        }
    }

    async function saveApp(appData, respond) {
        try {
            let res = await act('role:publicstore, list:app', {query: {url: appData.url}});
            if (res.length !== 0) {
                console.log('Updating app to publicstore');
                appData.id = res[0].id;
                await act('role:publicstore, update:app', {data: appData});
                respond();
            } else {
                console.log('Setting new app to publicstore');
                await act('role:publicstore, set:app', {data: appData});
            }
        } catch (e) {
            console.log('Error in saveApp: ', e);
            respond();
        }
    }

    function execute(cmd) {
        return new Promise((resolve, reject) => {
            console.log("Running command: ", cmd);
            exec(cmd, (error, stdout) => {
                if (error) {
                    console.log("Error in execute: ", error);
                    reject(error);
                } else {
                    console.log("Executed successfuly");
                    resolve(stdout);
                }
            });
        });
    }

    async function asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }
};
