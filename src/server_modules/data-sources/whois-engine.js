'use strict';
const whois = require('node-whois');
const helpers = require('../utils/utils-helpers');
const request = require('request-promise');
const Promise = require('bluebird');
const consts = require('../../../config/consts');

module.exports = function Whois() {
    const seneca = this;

    seneca.add('role:whois, cmd:parseResponse', parseResponseForPhishStore);
    seneca.add('role:whois, cmd:sendQuery', sendQuery);
    seneca.add('role:whois, cmd:reverseWhois', reverseWhois);

    function reverseWhois(msg, respond) {
        let domains = msg.query.domains;
        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        return Promise.map(domains, (domain) => {
            // Uses viewDNS API
            return extendedWhois(domain)
                .then((whoisData) => {
                    if (whoisData.response.organisation || whoisData.response.registrant) {
                        const firstOptions = {
                            uri: 'https://api.viewdns.info/reversewhois/',
                            qs: {
                                q: whoisData.response.organisation || whoisData.response.registrant,
                                apikey: consts.VIEWDNS_KEY,
                                output: 'json'
                            },
                            json: true // Automatically parses the JSON string in the response
                        };
                        return [request(firstOptions), whoisData.response.organisation, firstOptions.qs.q];
                    } else {
                        return Promise.resolve();
                    }
                })
                .spread((result, q) => {
                    const promiseArr = [];
                    if (result) {
                        if (parseInt(result.response.total_pages) > 1 /*&& parseInt(result.response.total_pages) < 3*/) {
                            for (let i = 1; i <= result.response.total_pages; i++) {
                                const secondOptions = {
                                    uri: 'https://api.viewdns.info/reversewhois/',
                                    qs: {
                                        q: q,
                                        apikey: consts.VIEWDNS_KEY,
                                        output: 'json',
                                        page: i
                                    },
                                    json: true // Automatically parses the JSON string in the response
                                };

                                promiseArr.push(request(secondOptions));
                            }
                            return Promise.all(promiseArr);
                            /*} else if (parseInt(result.response.total_pages) > 3) {
                                respond(null, {
                                    ok: false,
                                    error: 'No Domains loaded : This domain is registered by a managed domain registrar and will probably return bad results.'
                                });

                                return Promise.resolve([]);
                            }*/
                        } else {
                            return Promise.resolve([result]);
                        }
                    } else {
                        return Promise.resolve();
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        }).then((results) => {
            const filterdResults = [];
            for (let i = 0; i < results[0].length; i++) {
                for (let j = 0; j < results[0][i].response.matches.length; j++) {
                    const domain = results[0][i].response.matches[j].domain;
                    filterdResults.push(domain);
                }
            }
            respond(null, {ok: true, filterdResults: filterdResults});
            return Promise.resolve();
        }).catch((e) => {
            console.log('Reverse Whois Error: ', e);
            respond();
        });
    }

    function parseWhois(whoisStr) {
        const lines = whoisStr.split('\n');
        const filterdLines = [];
        const whoisObj = {};
        for (const line of lines) {
            if (line.indexOf('%') !== 0 && line.length !== 0) {
                filterdLines.push(line);
            }
        }

        for (const kv of filterdLines) {
            let key = kv.substr(0, kv.indexOf(':'));
            if (key.indexOf('.') !== -1) {
                console.log(key);
                key = key.replace(/./g, '');
            }
            let val = kv.substr(kv.indexOf(':') + 1);
            let i = 0;
            val = val.replace(/ +(?= )|^(\s*)/g, '');
            if (val.indexOf(' ') === 0) {
                val = val.substr(1);
            }
            if (key || key !== '' || val || val !== '') {
                if (whoisObj.hasOwnProperty(key)) {
                    whoisObj[key + i] = val;
                    i++;
                } else {
                    whoisObj[key] = val;
                }
            }else{
                i++;
            }
        }

        return whoisObj;
    }

    function parseResponseForPhishStore(msg, respond) {
        let store = 'publicstore';
        console.log('In parseResponse');
        if (msg.data.whoisStr) {
            const whoisObj = parseWhois(msg.data.whoisStr);
            if (msg.data.customer) {
                store = 'customerstore';
            }
            seneca.act('role:' + store + ', update:app', {data: {id: msg.data.id, whois: whoisObj}}, () => {
                console.log('Updating whois to publicApp');
                respond();
            });
        } else {
            console.log('No data for whois');
            respond();
        }
    }

    function sendQuery(msg, respond) {
        console.log('in sendQuery - whois');
        let queryResult = msg.app.url,
            url,
            data;
        queryResult = helpers.getDomainFromURL(queryResult);

        url = queryResult;
        if (url) {
            whois.lookup(url, (err, whoisStr) => {
                console.log('looking up:', url);
                if (err || !whoisStr) {
                    console.log(err);
                    respond();
                } else {
                    data = {
                        id: msg.app.id,
                        whoisStr: whoisStr,
                        customer: msg.customer
                    };

                    seneca.act('role:whois, cmd:parseResponse', {'data': data}, respond);
                }
            });
        }
        respond();
    }

    function extendedWhois(domain) {
        const options = {
            uri: 'https://api.viewdns.info/whois/',
            qs: {
                domain: domain,
                apikey: consts.VIEWDNS_KEY,
                output: 'json'
            },
            json: true // Automatically parses the JSON string in the response
        };
        return request(options)
            .then((result) => {
                return result;
            })
            .catch((err) => {
                console.log(err);
                return err;
            });
    }
};

