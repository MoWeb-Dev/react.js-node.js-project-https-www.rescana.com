// this service manages putting keys on the PUBSUB Queue
// gets keys that have timestamps older then 30sec from mongo and put them in the pubsub apikey topics (for vt x2 and shodan)

const config = require('app-config');
// Imports the Google Cloud client library
const pubsub = require('@google-cloud/pubsub');
const client = new pubsub.PubSub();
process.env.GOOGLE_APPLICATION_CREDENTIALS = config.paths.googlePubSubCredentials;
//  mongodb
const MongoClient = require('mongodb').MongoClient;
// setup timers
const stalenessMins = 1;
//connect to DB
const mongoUrl = config.addresses.mongoConnectionString;
// Database Name
const dbName = 'api_keys';
const timeout = 20000;

// Find all the old records for a specific collection in mongo
const findDocuments = (db, pcollection) => {
    return new Promise((resolve) => {
        // Get the documents collection
        const collection = db.collection(pcollection);
        // Find some documents
        let d = new Date();
        d.setMinutes(d.getMinutes() - stalenessMins);
        collection.find({'timeStamp': {$lt: d.toISOString()}}, (err, res) => {
            res.toArray((err, docs) => {
                console.log("Found the following records");
                console.log(JSON.stringify(docs));
                resolve(docs);
            });
        });
    })
};

// Connect to Mongodb
MongoClient.connect(mongoUrl, async (err, client) => {
    //infinite looop
    while (true) {
        console.log('waiting ' + timeout / 1000 + ' seconds');
        await new Promise(r => setTimeout(r, timeout));
        console.log("Connected correctly to server");
        const db = client.db(dbName);
        let vtKeys = await findDocuments(db, process.env['NODE_ENV'] + '-vtKeys');
        let shodanKeys = await findDocuments(db, process.env['NODE_ENV'] + '-shodanKeys');
        await createAndPublish(process.env['NODE_ENV'] + '-vtKeys', vtKeys);
        await createAndPublish(process.env['NODE_ENV'] + '-shodanKeys', shodanKeys);
    }
});

// todo: add topic check before creating a new one
async function createAndPublish(topicName, keys) {
    // Instantiates a client
    try {
        // Creates the new topic
        const [topic] = await client.createTopic(topicName);
        console.log(`Topic ${topic.name} created.`);
    }
    catch (err) {
        if (err.code === 409 || err.code === 6) {
            console.log('topic "' + topicName + '" already exists.');
        }
    }

    if (keys.length > 0) {
        for (let i = 0; i < keys.length; i++) {
            if (keys[i] && keys[i].key) {
                try {
                    const dataBuffer = Buffer.from(keys[i].key);
                    const messageId = await client.topic(topicName).publish(dataBuffer, {});
                    console.log(`Message ${messageId} published.`);
                } catch (e) {
                    console.log("Error in createAndPublish: " + e);
                }
            }
        }
    }
}