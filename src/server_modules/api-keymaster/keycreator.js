
// The utility is to be ran manually after modification, to insert api keys to mongo until there is a GUI for it :)
const MongoClient = require('mongodb').MongoClient;
const config = require('app-config');

let db;
console.log("Connected correctly to server");

// Connection URL
const url = config.addresses.mongoConnectionString;

const vt_apikeys =  config.consts.vt_apikeys;

const shodan_apikeys =  config.consts.shodan_apikeys;

const insertDocuments = async (db, pcollection, apikeys) => {
    const collection = db.collection(pcollection);
    // Insert some documents
    await collection.insertMany(apikeys, (err, result) => {
        console.log("Inserted documents into the collection");
    });
};

const findDocuments =  async (db, pcollection) => {
    const collection = db.collection(pcollection);
    // Find some documents
    await collection.find({}).toArray((err, docs) => {
        console.log("Found the following records");
        console.log(JSON.stringify(docs))
    });
};


// Use connect method to connect to the server
MongoClient.connect(url, async (err, client) => {
    // Database Name
    const dbName = 'api_keys';
    db = client.db(dbName);
    await insertDocuments(db, process.env['NODE_ENV']+'-vtKeys', vt_apikeys);
    await findDocuments(db, process.env['NODE_ENV']+'-vtKeys');
    await insertDocuments(db, process.env['NODE_ENV']+'-shodanKeys', shodan_apikeys);
    await findDocuments(db, process.env['NODE_ENV']+'-shodanKeys');
});
