'use strict';
const Promise = require('bluebird');
const helpers = require('./utils/utils-helpers');
const config = require('app-config');
const neo4jDriver = require('neo4j-driver').v1;
const neo4jRetried = require('@ambassify/neo4j-retried');
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass),
    {maxConnectionLifetime: 20 * 60 * 1000,
        connectionTimeout: 1000 * 45,
        connectionAcquisitionTimeout: 600000,
        maxTransactionRetryTime: 10000,
        connectionPoolSize: 1000}), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});


module.exports = function AlertSystem() {
    const seneca = this;
    const act = Promise.promisify(seneca.act, {context: this});

    seneca.add('role:alertSystem, alert:newPublicAppsFound', (msg, respond) => {
        act('role:userdata, get:user', {id: msg.data.customerApp.uid[0]}).then((user) => {
            if (user.email !== undefined &&
                user.alertSystemConfig !== undefined &&
                user.email.length &&
                user.alertSystemConfig.sendPhishingAlerts) {
                const customerAppDomain = msg.data.customerApp.url.replace('http://', '');
                const firstName = user.name.firstName.length ? user.name.firstName : 'Customer';
                let text = '<!DOCTYPE html><html><head></head><body><div>';
                text += '<p>Dear ' + firstName + ',</p>';

                if (msg.data.newPublicApps.length > 1) {
                    text += '<p>' +
                        'There were ' + msg.data.newPublicApps.length + ' new suspicious domains found ' +
                        'for <b>' + customerAppDomain + '</b>:' +
                        '</p>';
                } else {
                    text += '<p>A new suspicious domain has been found: </p>';
                }

                text += '<ul>';
                msg.data.newPublicApps.map(function(app) {
                    const url = 'http://' + app.url;
                    text += '<li><a href="' + url + '" target="_blank">' + url + '</a></li>';
                });
                text += '</ul>';

                const addressPath = config.addresses.website;

                text += '<br/>You can find more details <a href="' + addressPath + '/brand" target="_blank">here</a>';
                text += '<br/><b>Rescana</b>';
                text += '</div></body></html>';

                const subject = 'New suspicious domains were found for: ' + customerAppDomain;
                console.log(subject);

                const msgObj = {
                    to: user.email,
                    subject: subject,
                    text: text
                };

                console.log('Sending a NewPublicAppsFound message: ', msgObj);

                act('role:mailer, cmd:send', msgObj).then(() => {
                    console.log('message has been sent');
                    respond();
                });
            }

            respond();
        });

        respond();
    });

    seneca.add('role:alertSystem, alert:newIntelFound', (msg, respond) => {
        if (msg && msg.data && msg.data.intelType) {
            const requestedIntel = msg.data.intelType;

            // Query with subdomains.
            const firstQuery = helpers.getIntelUpdatesQuery(requestedIntel, true);
            // Query without subdomains.
            const secondQuery = helpers.getIntelUpdatesQuery(requestedIntel, false);

            if (!firstQuery || !secondQuery) {
                const result = {
                    ok: false,
                    err: 'No matching intel for ' + requestedIntel + ' in alertSystem-newIntelFound.'
                };
                respond(result);
            } else {
                const cmd = firstQuery +
                    ' UNION ' +
                    secondQuery;

                getIntel(cmd)
                    .then((res) => {
                        if (res && res.length > 0) {
                            const result = {ok: true, data: res};
                            respond(result);
                        } else {
                            const result = {ok: false, err: 'No new ' + requestedIntel + ' intel found yet.'};
                            respond(result);
                        }
                    })
                    .catch((e) => {
                        console.log('Error in alertSystem-newIntelFound', e);
                        const result = {ok: false, err: 'Error in alertSystem-newIntelFound: ' + e};
                        respond(result);
                    });
            }
        } else {
            const result = {ok: false, err: 'Error in alertSystem-newIntelFound: no valid params.'};
            respond(result);
        }
    });

    const getIntel = (cmd) => {
        return new Promise((resolve, reject) => {
            const session = neo4j.session();
            console.log('QUERY: ', cmd);
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(cmd);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = this.convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getIntel: ', error);
                    return reject();
                });
        });
    };

    const getHTMLStyle = () => {
        return '<style>' +
            'div {' +
            '    direction: ltr;' +
            '}' +
            'table {' +
            '    margin: 5px;' +
            '    width: 95%;' +
            '}' +
            'th, td {' +
            '    border-collapse: collapse;' +
            '    text-align: left;' +
            '    padding: 8px;' +
            '}' +
            'tr:nth-child(even){background-color: #f2f2f2}' +
            '</style>';
    };

    this.convertToObject = (result) => {
        return result.records.map((record) => {
            const dataObj = {};

            for (let i = 0; i < record._fields.length; i++) {
                dataObj[record.keys[i]] = record._fields[i];
            }

            return dataObj;
        });
    };

    const getMessageIntroduction = (firstName, vulnCount, intelType, domainsIntelFindings) => {
        let intro = '<p>Dear ' + firstName + ',</p>';

        if (vulnCount > 1) {
            const intelInfo = (intelType === 'bucket') ? 'Amazon buckets discovered ' : (intelType + ' Risks found ');
            intro += '<p>' +
                'There were ' + vulnCount + ' new ' + intelInfo +
                'for <b>';
        } else {
            const intelInfo = (intelType === 'bucket') ? 'Amazon bucket discovered ' : (intelType + ' vulnerability has been found ');
            intro += '<p>A new ' + intelInfo +
                'for <b>';
        }
        if (domainsIntelFindings.length > 1) {
            intro += domainsIntelFindings.length + ' domains</b>:' +
                '</p>';
        } else {
            intro += domainsIntelFindings[0].domain + '</b>:' +
                '</p>';
        }

        return intro;
    };

    const getTableHeadersByIntelType = (intelType) => {
        let headers = '<tr>';
        if (intelType === 'cve') {
            headers += '<th>Hostname</th>';
            headers += '<th>Subdomain</th>';
            headers += '<th>IP</th>';
            headers += '<th>Port</th>';
            headers += '<th>Product</th>';
            headers += '<th>CVE</th>';
            headers += '<th>CVE Score</th>';
        } else if (intelType === 'bucket') {
            headers += '<th>Hostname</th>';
            headers += '<th>Subdomain</th>';
            headers += '<th>Bucket</th>';
            headers += '<th>Bucket Status</th>';
        }
        headers += '</tr>';
        return headers;
    };

    const getTableDataRowByIntelType = (intelData) => {
        let dataRow = '<tr>';
        if (intelData.intelType === 'cve') {
            dataRow += '<td>' + intelData.hostname + '</td>';
            dataRow += '<td>' + intelData.subdomain + '</td>';
            dataRow += '<td>' + intelData.ip + '</td>';
            dataRow += '<td>' + intelData.ports + '</td>';
            const cpeArr = intelData.cpe.split(':');
            const cpeStr = cpeArr[cpeArr.length - 3] + ' ' + cpeArr[cpeArr.length - 2] + ' ' + cpeArr[cpeArr.length - 1] || '';
            dataRow += '<td>' + cpeStr + '</td>';
            dataRow += '<td><a href="https://nvd.nist.gov/vuln/detail/' + intelData.cve + '" target="_blank">' + intelData.cve + '</a></td>';
            dataRow += '<td>' + intelData.cveScore + '</td>';
        } else if (intelData.intelType === 'bucket') {
            dataRow += '<td>' + intelData.hostname + '</td>';
            dataRow += '<td>' + intelData.subdomain + '</td>';
            dataRow += '<td>' + intelData.bucket + '</td>';
            dataRow += '<td>' + intelData.bucketStatus + '</td>';
        }
        dataRow += '</tr>';
        return dataRow;
    };

    const messageToUser = (userEmail, userName, domainsIntelFindings, vulnCount, intelType, respond) => {
        if (userEmail && domainsIntelFindings && domainsIntelFindings.length > 0 && vulnCount && intelType) {
            const firstName = (userName && userName.firstName) ? userName.firstName : 'Customer';

            let text = '<!DOCTYPE html><html><head>';

            text += getHTMLStyle();

            text += '</head><body><div>';

            text += getMessageIntroduction(firstName, vulnCount, intelType, domainsIntelFindings);

            text += '</div>';

            domainsIntelFindings.map((currFindings) => {
                text += '<p>Findings on <b>' + currFindings.domain + '</b> related to ';
                for (let i = 0; i < currFindings.relatedCompanies.length; i++) {
                    text += '<b>' + currFindings.relatedCompanies[i] + '</b>';
                    if (i < currFindings.relatedCompanies.length - 1) {
                        text += ', ';
                        if ((i + 1) % 5 === 0) {
                            text += '<br/>';
                        }
                    }
                }
                text += '</p>';

                text += '<div style=\"overflow-x:auto;overflow-y:auto;max-height:350px\">' +
                    '<table>';
                // Headers.
                text += getTableHeadersByIntelType(intelType);
                // Data.
                if (intelType === 'cve') {
                    currFindings.intelFindings.sort((a, b) => {
                        return b.cveScore - a.cveScore;
                    });
                }
                currFindings.intelFindings.map((currIntel) => {
                    text += getTableDataRowByIntelType(currIntel);
                });

                text += '</table>' +
                    '</div>' +
                    '<br/>';
            });

            const addressPath = config.addresses.website;

            const intelTypeParam = (intelType === 'bucket') ? 'buckets' : intelType;

            text += '<div><br/>You can find more details <a href="' + addressPath + '/intel/' + intelTypeParam + '" target="_blank">here</a>';
            text += '<br/><b>Rescana</b>';
            text += '</div></body></html>';

            const subject = 'New Risks detected in your organization';

            const msgObj = {
                to: userEmail,
                subject: subject,
                text: text
            };

            console.log('sending message to ' + userEmail);

            act('role:mailer, cmd:send', msgObj).then(() => {
                console.log('message has been sent');
                respond({ok: true});
            }).catch((e) => {
                respond({ok: false, err: 'Error in mailer: ' + e});
            });
        } else {
            respond({ok: false, err: 'Error in messageToUser: no valid params'});
        }
    };

    const messageScanIsDoneToUser = (userEmail, userName, companyNames, scanDate, respond, isFailed = false, err) => {
        if (userEmail && userName && companyNames && Array.isArray(companyNames) && scanDate) {
            let scanStatus, companyStatusSingle, companiesStatusPlural, subjectStatus;
            if (isFailed) {
                scanStatus = 'failed';
                companyStatusSingle = 'has failed to be scanned';
                companiesStatusPlural = 'have failed to be scanned';
                subjectStatus = 'failed';
            } else {
                scanStatus = 'completed';
                companyStatusSingle = 'has been scanned';
                companiesStatusPlural = 'have been scanned';
                subjectStatus = 'is done';
            }

            let text = '<!DOCTYPE html><html><head>';

            text += getHTMLStyle();

            text += '</head><body><div>';

            text += '<p>Dear ' + userName + ',</p>';

            text += '<p>Your manual scan from ' + scanDate + ' has ' + scanStatus + '.</p>';

            if (companyNames.length > 0) {
                text += '<p>';

                if (companyNames.length === 1) {
                    text += companyNames[0] + ' ' + companyStatusSingle + '.';
                } else {
                    text += 'The following companies ' + companiesStatusPlural + ': ';

                    // companiesCounter count the none-empty company names.
                    let companiesCounter = 0;

                    companyNames.map((currComp, i) => {
                        if (currComp) {
                            text += currComp;
                            companiesCounter++;

                            if (i < companyNames.length - 1) {
                                text += ', ';

                                if (companiesCounter === 5) {
                                    text += '<br/>';
                                    companiesCounter = 0;
                                }
                            }
                        }
                    });
                }

                text += '</p>';
            }
            text += '<br/>';
            text += '<p>';
            text +=  err;
            text += '</p>';

            text += '</div>';

            const addressPath = config.addresses.website;

            text += '<div><br/>You can find more details <a href="' + addressPath + '/" target="_blank">here</a>';
            text += '<br/><b>Rescana</b>';
            text += '</div></body></html>';

            const subject = 'Your scan ' + subjectStatus;

            const msgObj = {
                to: userEmail,
                subject: subject,
                text: text
            };

            console.log('sending message to ' + userEmail);

            act('role:mailer, cmd:send', msgObj).then(() => {
                console.log('message has been sent');
                respond({ok: true});
            }).catch((e) => {
                respond({ok: false, err: 'Error in mailer: ' + e});
            });
        } else {
            respond({ok: false, err: 'Error in messageToUser: no valid params'});
        }
    };

    seneca.add('role:alertSystem, alert:sendToUsersIntelFound', (msg, respond) => {
        if (msg && msg.data && msg.data.intelType && msg.data.intelByDomains && msg.data.usersObj) {
            // Generate the data for the mail.
            msg.data.usersObj.map((currentUser) => {
                const domainsIntelFindings = [];
                let vulnCount = 0;
                currentUser.domains.map((currentDomainObj) => {
                    // Look for current domain's intel findings.
                    for (let i = 0; i < msg.data.intelByDomains.length; i++) {
                        if (msg.data.intelByDomains[i].domain === currentDomainObj.domain) {
                            vulnCount += msg.data.intelByDomains[i].intelFindings.length;
                            domainsIntelFindings.push({
                                domain: currentDomainObj.domain,
                                intelFindings: msg.data.intelByDomains[i].intelFindings,
                                relatedCompanies: currentDomainObj.relatedCompanies
                            });
                            break;
                        }
                    }
                });
                if (domainsIntelFindings.length > 0) {
                    // send email for each user.
                    messageToUser(currentUser.email, currentUser.name, domainsIntelFindings, vulnCount, msg.data.intelType, respond);
                } else {
                    console.log('message has been excluded from user: ' + currentUser.email + ' due to his alertSystemConfig.');
                    respond({ok: true});
                }
            });
        } else {
            const result = {ok: false, err: 'Error in alertSystem-sendToUsersIntelFound: no valid params.'};
            respond(result);
        }
    });

    const messageMaliciousFilesToAdmin = (files, respond) => {
        if (files && Array.isArray(files) && files.length > 0) {
            let text = '<!DOCTYPE html><html><head>';

            text += getHTMLStyle();

            text += '</head><body><div>';

            text += '<p>Dear Admin,</p>';

            text += '<p>In the past month, the ' + files.length + ' following files were uploaded to surveys as evidence and were scanned as malicious:</p>';

            text += '<hr/>';

            files.map((currFile) => {
                if (currFile) {
                    if (currFile.filename) {
                        text += '<b>filename should guide you through surveyAnswer to find related company and related evidence file.</b>';
                    }
                    text += '<p>';
                    Object.keys(currFile).map((currKey) => {
                        if (currKey && currFile[currKey]) {
                            text += '<b>' + currKey + '</b>: <span>' + currFile[currKey] + '</span><br/>';
                        }
                    });

                    text += '</p><hr/>';
                }
            });

            text += '</div>';

            const addressPath = config.addresses.website;

            text += '<div><br/>You can find more details <a href="' + addressPath + '/" target="_blank">here</a>';
            text += '<br/><b>Rescana</b>';
            text += '</div></body></html>';

            const subject = 'Malicious files were found';

            const adminEmail = 'yuval@rescana.com';

            const msgObj = {
                to: adminEmail,
                subject: subject,
                text: text
            };

            console.log('sending message to ' + adminEmail);

            act('role:mailer, cmd:send', msgObj).then(() => {
                console.log('message has been sent');
                respond({ok: true});
            }).catch((e) => {
                respond({ok: false, err: 'Error in mailer: ' + e});
            });
        } else {
            respond({ok: false, err: 'Error in messageMaliciousFilesToAdmin: no data to send'});
        }
    };

    seneca.add('role:alertSystem, alert:sendToAdminMaliciousUploadedFiles', (msg, respond) => {
        if (msg && msg.files) {
            // send email to Admin.
            messageMaliciousFilesToAdmin(msg.files, respond);
        } else {
            const result = {ok: false, err: 'Error in alertSystem-sendToAdminMaliciousUploadedFiles: no valid params.'};
            respond(result);
        }
    });

    seneca.add('role:alertSystem, alert:sendToUserScanIsDone', (msg, respond) => {
        if (msg && msg.userEmail && msg.userName && msg.companyNames && msg.scanDate) {
            // send email to user.
            messageScanIsDoneToUser(msg.userEmail, msg.userName, msg.companyNames, msg.scanDate, respond);
        } else {
            const result = {ok: false, err: 'Error in alertSystem-sendToUserScanIsDone: no valid params.'};
            respond(result);
        }
    });

    seneca.add('role:alertSystem, alert:sendToUserScanHasFailed', (msg, respond) => {
        if (msg && msg.userEmail && msg.userName && msg.companyNames && msg.scanDate) {
            // send email to user.
            messageScanIsDoneToUser(msg.userEmail, msg.userName, msg.companyNames, msg.scanDate, respond, true, msg.error);
        } else {
            const result = {ok: false, err: 'Error in alertSystem-sendToUserScanHasFailed: no valid params.'};
            respond(result);
        }
    });
};
