#!/usr/bin/env node
'use strict';
const moment = require('moment');
const requestP = require('request-promise');
const wayback = require('wayback-machine');
const htmlToText = require('html-to-text');
const DaysToInvalidWebSite = 7;
let closest_url;
let weekAgo = moment.utc().subtract(1, 'week');
const parseDomain = require('parse-domain');

// get the closest page from wayback machine
const getClosests = function (url) {
    return new Promise(function (resolve, reject) {
        wayback.getClosest(url, function (err, closest) {
            if (err) {
                console.error(err);
                reject(err);
            }
            let parsedDate = moment(moment(closest.timestamp, 'YYYYMMDDhhmmss'));
            let oldness = moment().diff(parsedDate, 'days');
            console.log('Snapshot is ' + oldness + ' days old');
            console.log('Last wayback update is ' + parsedDate.toString());
            console.log('last week is ' + weekAgo.toString());
            if (parseInt(oldness) < DaysToInvalidWebSite) {
                closest_url = closest;
            }
            resolve(closest);
        });
    });
};

const redirectUrlToDomain = function (redirectUrl) {
    let newStringObJ = {url: '', isWithHttps: false};
    let tld = "";
    if (redirectUrl && typeof redirectUrl === 'string') {
        if (redirectUrl.startsWith("https://")) {
            newStringObJ.url = redirectUrl.slice(8, redirectUrl.length);
        } else if (redirectUrl.startsWith("http://")) {
            newStringObJ.url = redirectUrl.slice(7, redirectUrl.length);
        }
        if (newStringObJ.url.startsWith("www.")) {
            newStringObJ.url = newStringObJ.url.slice(4, newStringObJ.url.length);
        }
        if (newStringObJ.url.indexOf("/") !== -1) {
            newStringObJ.url = newStringObJ.url.slice(0, newStringObJ.url.indexOf("/"));
        }
        if (newStringObJ.url.indexOf("#") !== -1) {
            newStringObJ.url = newStringObJ.url.slice(0, newStringObJ.url.indexOf("#"))
        }
        let parsedDomain = parseDomain(newStringObJ.url);
        if (parsedDomain) {
            tld = parsedDomain.tld;
        }else{
            console.log("Could not parse ltd for domain : ", newStringObJ.url);
        }
        let hasPath = newStringObJ.url.indexOf(tld) + tld.length < newStringObJ.url.length;
        if (tld && hasPath) {
            newStringObJ.url = newStringObJ.url.slice(newStringObJ.url.indexOf(tld) + tld.length, newStringObJ.url.length)
        }
    }
    return newStringObJ;
};

const isParking = function (str) {
    let strArr = ['parking', 'iis windows server', '123 reg', 'under construction'];
    if(str){
        str = str.toLowerCase();
        for (let i = 0; i < strArr.length; i++) {
            if (str.indexOf(strArr[i]) !== -1){
                return true;
            }
        }
    }
    return false;
};

const fixDomainToURL = (lowerDomain, withSecure = true) => {
    let preURL = '';
    let sub = '';
    if (!lowerDomain.startsWith('http')) {
        preURL = 'http';
        if (withSecure) {
            preURL += 's';
        }
        preURL += '://';
        let parsedDomain = parseDomain(lowerDomain);
        if (parsedDomain) {
            sub = parsedDomain.subdomain;
        }else{
            console.log("Could not parse domain: ", lowerDomain);
        }

        if (!lowerDomain.startsWith('www.') && sub === "" && !sub) {
            preURL += 'www.';
        }
    }
    return preURL + lowerDomain;
};

const isRedirect = async (domain) => {
    return new Promise(async (resolve) => {
        let isAnswered = false;
        let isError = false;
        if (domain) {
            const lowerDomain = domain.toLowerCase();

            // First try URL with https.
            let domainToRequest = fixDomainToURL(lowerDomain, true);
            let sub = '';

            let url = domain;
            let parsedDomain = parseDomain(lowerDomain);
            if (parsedDomain) {
                sub = parsedDomain.subdomain;
            }

            if (domain.indexOf("www.") === -1 && sub === "" && !sub) {
                url = 'www.' + domain;
            }

            try {
                await requestP({url: domainToRequest, followRedirect: false, timeout: 1000});
                isAnswered = true;
                resolve({isError: isError, isAnswered: isAnswered, url: url});
            } catch (err) {

                // If first try failed - try just with http.
                domainToRequest = fixDomainToURL(lowerDomain,false);
                try {
                    let res = await requestP({url: domainToRequest, followRedirect: false, timeout: 1000});
                    if (res && res.headers && res.headers.location) {
                        let res = redirectUrlToDomain(res.headers.location);
                        let domainCheck = domain;
                        if (domainCheck.startsWith('www.')) {
                            domainCheck = domainCheck.slice(4, domainCheck.length);
                        }
                        //if redirectUrl redirect to the same url with "https://" then we return
                        // the redirectUrl to check if its valid with https request in getSslStatus() func.
                        if (res && res.url && res === domainCheck) {
                            isAnswered = true;
                            resolve({isError: isError, isAnswered: isAnswered, err: err, url: url});
                        } else {
                            //if http request is redirect
                            isAnswered = true;
                            resolve({
                                isError: isError,
                                isAnswered: isAnswered,
                                err: err,
                                url: url,
                                isHttpRedirectUrl: true
                            });
                        }
                    } else if (res && !res.headers) {
                        res = res.toLowerCase();
                        //in case we get redirect http with out redirect location we check for parking site
                        if (typeof res === 'string' && isParking(res)) {
                            isAnswered = true;
                            resolve({isError: isError, isAnswered: isAnswered, err: err, url: url, isParking: true});
                        } else {
                            //if http request is redirect
                            isAnswered = true;
                            resolve({
                                isError: isError,
                                isAnswered: isAnswered,
                                err: err,
                                url: url
                            });
                        }
                    } else {
                        //if http request is redirect
                        isAnswered = true;
                        resolve({
                            isError: isError,
                            isAnswered: isAnswered,
                            err: err,
                            url: url,
                            isHttpRedirectUrl: true
                        });
                    }
                } catch (err) {
                    //This means a redirect with code 301
                    if (err.statusCode === 301 || err.statusCode === 302) {
                        if (err && err.response && err.response.headers && err.response.headers.location) {
                            let res = redirectUrlToDomain(err.response.headers.location);
                            let domainCheck = domain;
                            if (domainCheck.startsWith('www.')) {
                                domainCheck = domainCheck.slice(4, domainCheck.length);
                            }
                            //if redirectUrl redirect to the same url with "https://" then we return
                            // the redirectUrl to check if its valid with https request in getSslStatus() func.
                            if (res && res.url && res.url === domainCheck) {
                                isAnswered = true;
                                resolve({isError: isError, isAnswered: isAnswered, err: err, url: url});
                            } else {
                                //if http request is redirect
                                isAnswered = true;
                                resolve({
                                    isError: isError,
                                    isAnswered: isAnswered,
                                    err: err,
                                    url: url,
                                    isHttpRedirectUrl: true
                                });
                            }
                        } else {
                            //if http request is redirect
                            isAnswered = false;
                            resolve({
                                isError: isError,
                                isAnswered: isAnswered,
                                err: err,
                                url: url,
                            });
                        }
                    } else {
                        //in case https and http requests were failed we return and check for the status error in getSslStatus()
                        isError = true;
                        resolve({isError: isError, isAnswered: isAnswered, err: err});
                    }
                }
            }
        } else {
            resolve({isError: isError, isAnswered: isAnswered});
        }
    });
};
module.exports.isRedirect = isRedirect;

module.exports.getResultForGDPR = (url) => {
    return new Promise(async (resolve) => {
        try {
            let closest;
            let response;
            let res = await this.isRedirect(url);
            const isRedirect = (res && res.isAnswered);
            const isError = (res && res.isError);
            if (isError) {
                console.log('Domain ', url, ' failed to check if contains a redirect action: ', res.err);
            } else {
                console.log('Domain ', url, ' found with' + ((isRedirect) ? '' : 'out'), ' a redirect action');
                if (!isRedirect) {
                    closest = await getClosests(url);
                }
            }
            if (closest) {
                response = await requestP(closest.url);
            }

            if (response) {
                const urlText = htmlToText.fromString(response, {
                    wordwrap: 'null',
                    noLinkBrackets: true,
                    ignoreHref: true,
                    ignoreImage: true
                });
                if (urlText) {
                    let isFound = false;
                    let keywords = ['uses cookies', 'privacy', 'use cookies', '301', 'third party cookies', 'מדיניות פרטיות', 'עוגיות', 'שימוש בעוגיות', 'תנאי שימוש'];
                    for (let i = 0; i < keywords.length; i++) {
                        if (urlText.toLowerCase().indexOf(keywords[i]) > -1) {
                            isFound = true;
                            break;
                        }
                    }
                    isFound ? console.log('privacy word exists') : console.log('privacy word missing');

                    resolve({resultGDPR: isFound});
                } else {
                    const errMsg = 'Error in getResultForGDPR-htmlToText-fromString: No text returned.';
                    console.error(errMsg);
                    resolve();
                }
            } else {
                resolve();
            }

        } catch (e) {
            console.error('Error in getResultForGDPR: ', e);
            resolve();
        }
    });
};










