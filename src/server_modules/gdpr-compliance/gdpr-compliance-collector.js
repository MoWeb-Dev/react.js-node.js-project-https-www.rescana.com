'use strict';
const waybackScraper = require('./wayback-scraper.js');
const PromiseB = require('bluebird');
const {getDistinctDomainsByCompanies} = require('../reports/common.js');
const {isNotEmptyArray} = require('../ssl-certificates/ssl-certificates-common.js');
const PQueue = require('promise-queue');

const maxConcurrent = 1;
const maxQueue = Infinity;

let queuesSinceServiceStarted = 0;


module.exports = function gdprComplianceCollector() {
    const act = PromiseB.promisify(this.act, {context: this});

    const collectGDPRCompliance = async (msg, respond) => {
        let gdprData;
        let allSystemDomains;

        console.log('In collectGDPRCompliance - starting to collect data');
        // Respond to scheduler that the collecting has started.
        respond({ok: true});
        // Increment Queues counter.
        const currentQueueID = ++queuesSinceServiceStarted;
        try {
            allSystemDomains = await getAllDomainsInSystem(msg.companyIDs);
        } catch (e) {
            console.error('In collectGDPRCompliance - Failed to get all domains');
        }

        try {
            gdprData = await getAllGDPRResults(allSystemDomains, currentQueueID);

            let gdprDataCount = 0;
            if (isNotEmptyArray(gdprData)) {
                gdprDataCount = gdprData.length;
            }
            console.log('Finished collecting ', gdprDataCount, ' GDPR Compliance data - from finished Queue [', currentQueueID, ']');
        } catch (e) {
            console.log('Error in collectGDPRCompliance-getAllDomainsInSystem: ', e);
            respond({ok: false});
            // Respond to scheduler that the collecting has failed to start.
        }
    };

    this.add('role:gdprComplianceCollect, cmd:collectGDPR', collectGDPRCompliance);

    // This function load all existing companies -> domains and returns all distinct domains.
    const getAllDomainsInSystem = async (inputCompanyIDs) => {
        console.log('In getAllDomainsInSystem - starting to collect domains');
        let allCompanies;
        let allDomains;

        // Get all companies.
        const params = {query: {}};
        // Check if admin requested a specific companies - otherwise all companies are returned.
        if (isNotEmptyArray(inputCompanyIDs)) {
            console.log('Requested companyIDs from Manual APIs: ', inputCompanyIDs);
            params.query = inputCompanyIDs;
        }
        try {
            allCompanies = await act('role:companies, listByObjectID:company', params);
            // Get all domains.
            allDomains = await getDistinctDomainsByCompanies(allCompanies);

            if (isNotEmptyArray(allDomains)) {
                console.log('Fetched a total of ', allDomains.length, ' domains');
            } else {
                console.log('No domains were found on the system');
            }

        }catch(e){
            console.log('Error getting all companies in getAllDomainsInSystem: ', e);
            return [];
        }
        try {
            return await act('role:intelQuery,cmd:getAllSubdomainsByDomains', {domains: allDomains});
        }catch(e){
            console.log('Error getting all subdomains in getAllSubdomainsByDomains: ', e);
            return [];
        }
    };

    const printGDPRQueueStatus = (gdprCollectQueue, currentQueueID) => {
        if (gdprCollectQueue) {
            console.log('Collect Web Privacy Policy Queue [', currentQueueID, '] status: Running - ', gdprCollectQueue.getPendingLength(), ' ; In Queue - ', gdprCollectQueue.getQueueLength());
        }
    };

    const saveFinishedDomainToDB = async (matchingDomain, resultData, currentQueueID, resolve) => {
        let savedData;
        // Save current domain's data to DB.
        if (matchingDomain && matchingDomain.domain) {
            try {
                savedData = await saveRelevantGDPRData([matchingDomain], currentQueueID);
                if (savedData && savedData.ok) {
                    console.log('Finished saving GDPR compliance data on domain ', matchingDomain.domain, ' - from finished Queue [', currentQueueID, ']');
                }
                resolve(resultData);
            }catch(e){
                console.log('Error in collectGDPRCompliance-saveRelevantGDPRData: ', e);
                resolve(resultData);
            }
        } else {
            resolve(resultData);
        }
    };

    // This function iterates on all domains and searches the relevant Web Privacy Policy results.
    const getAllGDPRResults = (domains, currentQueueID) => {
        return new Promise((resolve) => {
            console.log('In getAllGDPRResults - starting to collect all Web Privacy Policy results - for Queue [', currentQueueID, ']');

            if (isNotEmptyArray(domains)) {
                const resultData = [];

                const gdprCollectQueue = new PQueue(maxConcurrent, maxQueue);

                // Foreach domain - fetch its GDPR result.
                domains.map((currDomain) => {
                    if (currDomain && currDomain.domain) {
                        const domain = currDomain.domain;
                        console.log('Starting to fetch GDPR result for domain: ', domain);

                        gdprCollectQueue.add(function() {
                            return new Promise((resolveInQueue) => {
                                waybackScraper.getResultForGDPR(domain)
                                    .then((res) => {
                                        if (res && res.hasOwnProperty('resultGDPR')) {
                                            res.domain = domain;
                                            resultData.push(res);

                                            let logMsg = 'Domain: ' + domain + ' discovered with';
                                            if (!res.resultGDPR) {
                                                logMsg += 'out';
                                            }
                                            logMsg += ' a cookie banner';
                                            console.log(logMsg);

                                            // Save current domain's data without all subdomains to DB.
                                            saveFinishedDomainToDB(res, resultData, currentQueueID, resolveInQueue);
                                        } else {
                                            resolveInQueue();
                                        }
                                    }).catch((e) => {
                                    console.log('Error in getResultForGDPR of \'', domain, '\': ', e);
                                    resolveInQueue();
                                });
                            });
                        }).then(() => {
                            return checkIfGDPRCollectHasEnded(resultData.length, true, gdprCollectQueue, currentQueueID);
                        }).catch((e) => {
                            console.error('Error in getAllGDPRResults of \'', domain, '\': ', e);

                            return checkIfGDPRCollectHasEnded(resultData.length, true, gdprCollectQueue, currentQueueID);
                        }).then((startSubdomainsCollect) => {
                            if (startSubdomainsCollect && startSubdomainsCollect.ok) {
                                console.log('Starting to fetch all GDPR result records for all subdomains - for Queue [', currentQueueID, ']');
                                return getGDPRResultsForSubdomains(domains, resultData, gdprCollectQueue, currentQueueID)
                                    .then((resultWithSubdomainsData) => {
                                        if (isNotEmptyArray(resultWithSubdomainsData)) {
                                            resolve(resultWithSubdomainsData);
                                        } else {
                                            resolve(resultData);
                                        }
                                    }).catch((e) => {
                                        console.error('Error in getGDPRResultsForSubdomains: ', e);
                                        resolve(resultData);
                                    });
                            }
                        }).catch((e) => {
                            console.error('Error in checkIfGDPRCollectHasEnded at domain \'', domain, '\': ', e);
                            resolve(resultData);
                        });
                    }
                });

                // This is a fix for a case which no valid domains were found.
                if (checkIfGDPRQueueIsDone(gdprCollectQueue)) {
                    console.log('No domains were found while running Queue [', currentQueueID, '].');
                    resolve(resultData);
                }
            } else {
                console.log('No domains were found while running Queue [', currentQueueID, '].');
                resolve([]);
            }
        });
    };

    const checkIfGDPRQueueIsDone = (gdprCollectQueue) => {
        return !!(gdprCollectQueue && gdprCollectQueue.getPendingLength() === 0 && gdprCollectQueue.getQueueLength() === 0);
    };

    const checkIfGDPRCollectHasEnded = (resultDataCount, isDomainsNotSubdomainsCollect, gdprCollectQueue, currentQueueID) => {
        return new Promise((resolve) => {
            let ok;
            if (checkIfGDPRQueueIsDone(gdprCollectQueue)) {
                const dataCollected = (isDomainsNotSubdomainsCollect) ? 'domains' : 'subdomains';

                console.log('Finished with success to collect all ', dataCollected, ' GDPR results - from Queue [', currentQueueID, ']. Total records: ', resultDataCount);

                ok = true;
            } else {
                printGDPRQueueStatus(gdprCollectQueue, currentQueueID);

                ok = false;
            }
            resolve({ok: ok});
        });
    };

    const getGDPRResultsForSubdomains = (domains, resultDomainsData, gdprCollectQueue, currentQueueID) => {
        return new Promise((resolve) => {
            console.log('In getGDPRResultsForSubdomains - starting to collect all subdomains GDPR results');

            if (isNotEmptyArray(domains)) {
                const resultData = resultDomainsData || [];
                let subdomainResultsCount = 0;
                domains.map((currDomain) => {
                    if (currDomain && currDomain.domain && isNotEmptyArray(currDomain.subdomains)) {
                        // Find the matching domain to add each subdomain result to.
                        const matchingDomain = resultData.find((currEntity) => {
                            return currEntity && currEntity.domain && currEntity.domain === currDomain.domain;
                        });

                        // Foreach subdomain - fetch its GDPR result.
                        currDomain.subdomains.map((currSubdomain) => {
                            if (currSubdomain) {
                                console.log('Starting to fetch GDPR results for subdomain: ', currSubdomain);

                                gdprCollectQueue.add(function() {
                                    return new Promise((resolveInQueue) => {
                                        waybackScraper.getResultForGDPR(currSubdomain)
                                            .then((res) => {
                                                if (res && res.hasOwnProperty('resultGDPR') && res.resultGDPR) {
                                                    if (matchingDomain) {
                                                        subdomainResultsCount++;
                                                        res.subdomain = currSubdomain;
                                                        if (matchingDomain.subdomains) {
                                                            matchingDomain.subdomains.push(res);
                                                        } else {
                                                            matchingDomain.subdomains = [res];
                                                        }
                                                    }

                                                    let logMsg = 'Subdomain: ' + currSubdomain + ' discovered with';
                                                    if (!res.resultGDPR) {
                                                        logMsg += 'out';
                                                    }
                                                    logMsg += ' a GDPR';
                                                    console.log(logMsg);
                                                }
                                                resolveInQueue();
                                            }).catch((e) => {
                                            console.log('Error in getResultForGDPR of subdomain \'', domain, '\': ', e);
                                            resolveInQueue();
                                        });
                                    });
                                }).then(() => {
                                    return checkIfGDPRCollectHasEnded(subdomainResultsCount, false, gdprCollectQueue, currentQueueID);
                                }).catch((e) => {
                                    console.log('Error in getAllGDPRResults of subdomain \'', currSubdomain, '\': ', e);

                                    return checkIfGDPRCollectHasEnded(subdomainResultsCount, false, gdprCollectQueue, currentQueueID);
                                }).then((finishedSubdomainsCollect) => {
                                    if (finishedSubdomainsCollect && finishedSubdomainsCollect.ok) {
                                        // Get current domain's data with all subdomains and save it to DB.
                                        saveFinishedDomainToDB(matchingDomain, resultData, currentQueueID, resolve);
                                    }
                                }).catch((e) => {
                                    console.log('Error in checkIfGDPRCollectHasEnded at subdomain \'', currSubdomain, '\': ', e);
                                    resolve(resultData);
                                });
                            }
                        });
                    }
                });

                // This fix is for domains without any registered subdomains.
                if (checkIfGDPRQueueIsDone(gdprCollectQueue) && subdomainResultsCount === 0) {
                    console.log('No subdomains were found while running Queue [', currentQueueID, '].');
                    resolve(resultData);
                }
            } else {
                resolve(resultDomainsData);
            }
        });
    };

    // This function saves the results - gdprData to DB.
    const saveRelevantGDPRData = (gdprData, currentQueueID) => {
        return new Promise((resolve) => {
            if (isNotEmptyArray(gdprData)) {
                console.log('Starting to save ', gdprData.length, ' GDPR results on DB - from finished Queue [', currentQueueID, '].');
                // Save gdprData to DB.
                return act('role:gdprs, insertGDPRFindings:gdpr', {data: gdprData})
                    .then((result) => {
                        if (result && result.ok) {
                            console.log('Successfully saved ', gdprData.length, ' GDPR results on DB - from finished Queue [', currentQueueID, '].');
                            resolve({ok: true});
                        } else {
                            console.log('Failed saving GDPR results on DB - from finished Queue [', currentQueueID, '].');
                            resolve({ok: false});
                        }
                    }).catch((e) => {
                        console.log('Error in saveRelevantGDPRData - from finished Queue [', currentQueueID, ']: ', e);
                        resolve({ok: false});
                    });
            } else {
                resolve({ok: false});
            }
        });
    };
};
