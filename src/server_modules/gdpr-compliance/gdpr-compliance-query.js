'use strict';
const PromiseB = require('bluebird');
const {getDistinctDomainsByCompanies} = require('../reports/common.js');
const {whoIsRelatedByDomain} = require('../../server/api/api-helpers.js');
const {isNotEmptyArray} = require('../ssl-certificates/ssl-certificates-common.js');


module.exports = function gdprComplianceQuery() {
    const act = PromiseB.promisify(this.act, {context: this});

    // This function gets Web Privacy Policy results from mongo and adds whoIsRelatedByDomain for connecting domain -> relatedCompanies.
    const getGDPRComplianceData = (msg, respond) => {
        console.log('In getGDPRComplianceData');

        const params = {};
        // If companyIDs were given - only results related to these companies are returned. Otherwise, returned data related to all companies in the system.
        if (msg && isNotEmptyArray(msg.companyIDs)) {
            params.query = msg.companyIDs;
        }
        // Get all companies from DB.
        return act('role:companies, listByObjectID:company', params)
            .then((allCompanies) => {
                const query = {query: {}};

                // Get all companies domains.
                const allDomains = getDistinctDomainsByCompanies(allCompanies);

                // get Web Privacy Policy results only for relevant domains.
                if (isNotEmptyArray(allDomains)) {
                    query.query.domain = {$in: allDomains};
                }

                // Get GDPR results from DB.
                return [act('role:gdprs, list:gdpr', query), allCompanies];
            }).catch((e) => {
                console.log('Error in getGDPRComplianceData-list:gdpr: ', e);

                respond({ok: false});
            }).spread((gdprData, allCompanies) => {
                if (isNotEmptyArray(gdprData)) {
                    if (isNotEmptyArray(allCompanies)) {
                        const dataToReturn = [];

                        gdprData.map((currEntity) => {
                            // Make sure this feed result is relevant.
                            if (currEntity && currEntity.domain) {
                                // Add related companies to the object.
                                currEntity.relatedCompanies = whoIsRelatedByDomain(allCompanies, currEntity.domain);

                                // Insert the entity to the returned array only if related to any given company.
                                if (isNotEmptyArray(currEntity.relatedCompanies)) {
                                    dataToReturn.push(currEntity);
                                }
                            }
                        });

                        if (isNotEmptyArray(dataToReturn)) {
                            console.log('Found relevant results for given companies');
                            respond({ok: true, data: dataToReturn});
                        } else {
                            console.log('No relevant results were found for given companies');
                            respond({ok: false});
                        }
                    } else {
                        console.log('getGDPRComplianceData didn\'t return any companies data');
                        respond({ok: false});
                    }
                } else {
                    console.log('getGDPRComplianceData didn\'t return any Web Privacy Policy records');
                    respond({ok: false});
                }
            }).catch((e) => {
                console.error('Error in getGDPRComplianceData-getAllCompanies: ', e);

                respond({ok: false});
            });
    };

    this.add('role:gdprComplianceQuery, cmd:getGDPRCompliance', getGDPRComplianceData);
};
