'use strict';
const PromiseB = require('bluebird');
const unirest = require('unirest');
const config = require('app-config');
const PromiseThrottle = require('promise-throttle');

module.exports = function reportCreator() {
    const act = PromiseB.promisify(this.act, {context: this});

    const promiseBLThrottle = new PromiseThrottle({
        requestsPerSecond: 2, // Two requests per 1 second
        promiseImplementation: Promise
    });

    // This function search blacklists for inserted domains and returns the result (Without saving to DB).
    const searchByDomains = (msg, respond) => {
        console.log('In searchByDomains');

        if (!msg || !msg.domains || !Array.isArray(msg.domains) || msg.domains.length === 0) {
            console.log('No domains were inserted as parameters to searchByDomains.');

            respond({ok: false});
        } else {
            const allDomains = msg.domains;

            console.log('Searching blacklists for: ', allDomains.join(', '));

            const blacklists = [];

            return PromiseB.each(allDomains, (currDomain, idx) => {
                console.log('Completion Status: ', Math.floor(idx / allDomains.length * 100).toString(), '%');
                // Run each request at the rate defined above.
                return promiseBLThrottle.add(callAPI.bind(this, currDomain))
                    .then((findings) => {
                        if (findings) {
                            blacklists.push({domain: currDomain, data: findings});
                        }
                    })
                    .catch((e) => {
                        console.log('Error has occurred for ', currDomain, ' while searching blacklists: ', e);
                    });
            })
                .then(() => {
                    if (blacklists.length) {
                        console.log('Finished; Found ', blacklists.length, ' domains in blacklists.');
                    } else {
                        console.log('Finished; The given domains were not spotted on any blacklist.');
                    }

                    respond({ok: true, findings: blacklists});
                })
                .catch((e) => {
                    console.log('Error in searchByDomains: ', e);

                    respond({ok: false, err: e});
                });
        }
    };

    // This function looks up for blacklists on a specific domain or IP.
    const callAPI = (domainOrIP) => {
        return new Promise((resolve) => {
            let findings;

            console.log('Starting blacklists search for ', domainOrIP);

            const req = unirest('GET', 'https://api.apility.net/baddomain/' + domainOrIP);

            req.headers({
                'x-auth-token': config.consts.apilityAPI,
                'accept': 'application/json'
            });

            req.end(function(res) {
                if (res) {
                    if (res.error) {
                        console.log('Error using Apility API with domain: ' + domainOrIP + ': ', res.error);
                    } else if (res.body && res.body.response &&
                            (res.body.response.domain && (res.body.response.domain.blacklist.length ||
                                res.body.response.domain['blacklist_mx'].length ||
                                res.body.response.domain['blacklist_ns'].length)) ||
                            (res.body.response.ip && res.body.response.ip.blacklist.length)) {
                        console.log('Finding for ', domainOrIP, ':');

                        findings = {
                            'blacklist_domain': res.body.response.domain.blacklist,
                            'blacklist_mx': res.body.response.domain['blacklist_mx'],
                            'blacklist_ns': res.body.response.domain['blacklist_ns'],
                            'blacklist_ip': {
                                address: res.body.response.ip.address || '',
                                blacklist: res.body.response.ip.blacklist,
                                isQuarantined: res.body.response.ip['is_quarantined'] || false
                            },
                            'mx': res.body.response.domain.mx || [],
                            'ns': res.body.response.domain.ns || []
                        };

                        console.log(findings);
                    } else {
                        console.log('No blacklists were found for domain: ', domainOrIP);
                    }
                }

                resolve(findings);
            });
        });
    };

    // This function search blacklists for all inserted domains and then saves the result to DB.
    const searchAllDomains = (msg, respond) => {
        console.log('In searchAllDomains');

        if (!msg || !msg.domains || !Array.isArray(msg.domains) || msg.domains.length === 0) {
            console.log('No domains were inserted as parameters to searchAllDomains.');

            respond({ok: false});
        } else {
            const allDomains = msg.domains;

            respond({ok: true});

            console.log('Starting to collect all domains\' blacklists');

            searchByDomains({domains: allDomains}, (res) => {
                if (res.ok && res.findings) {
                    saveFindings(res.findings);
                }
            });
        }
    };

    // Save findings in mongoDB.
    const saveFindings = (findings) => {
        if (findings && Array.isArray(findings) && findings.length) {
            console.log('Saving blacklists\' findings in DB');

            return act('role:blacklists, cmd:updateDomainsFindings', {findings: findings})
                .then((res) => {
                    if (res && res.ok) {
                        console.log('All blacklists\' findings were successfully saved in DB');
                    } else {
                        console.log('Error has occurred while saving blacklists\' findings');
                    }
                })
                .catch((err) => {
                    console.log('Error in blacklists\' saveFindings: ', err);
                });
        } else {
            console.log('No findings were found');
        }
    };


    this.add('role:blacklistsSearcher, cmd:findBlacklists', searchByDomains);
    this.add('role:blacklistsSearcher, cmd:collectAllBlacklists', searchAllDomains);
};
