'use strict';
const PromiseB = require('bluebird');
const moment = require('moment');
const http = require('http');
const {RSS_FEEDS_INFO, TXT_FEEDS_INFO} = require('../../../config/server-consts.js');
const {getDistinctDomainsByCompanies} = require('../reports/common.js');
const {whoIsRelatedByDomain} = require('../../server/api/api-helpers.js');
const Parser = require('rss-parser');
const parser = new Parser();

const BOTNETS_DB_DATE_FORMAT = 'YYYY-MM-DD HH:mm';


module.exports = function rssFeedsCollector() {
    const act = PromiseB.promisify(this.act, {context: this});

    const collectRssFeeds = (msg, respond) => {
        console.log('In collectRssFeeds - starting to collect data');

        return getAllIPsInSystem(msg.companyIDs)
            .then((allSystemIPs) => {
                // Respond to scheduler that the collecting has started.
                respond({ok: true});

                // Search for relevant IPs findings in Botnets feeds.
                return getAllBotnetRssFeeds(allSystemIPs);
            })
            .catch((e) => {
                console.log('Error in collectRssFeeds-getAllIPsInSystem: ', e);
                // Respond to scheduler that the collecting has failed to start.
                respond({ok: false});
            }).then((feedsData) => {
                if (isNotEmptyArray(feedsData)) {
                    // Save feedsData to mongo.
                    return saveRelevantFeedData(feedsData);
                }
            }).catch((e) => {
                console.log('Error in collectRssFeeds-getAllBotnetRssFeeds: ', e);
            }).then((savedData) => {
                if (savedData && savedData.ok) {
                    console.log('Finished collecting RSS Feeds');
                }
            }).catch((e) => {
                console.log('Error in collectRssFeeds-saveRelevantFeedData: ', e);
            });
    };

    // This function gets RSS feeds results from mongo and adds whoIsRelatedByDomain for connecting ip -> domain -> relatedCompanies.
    const getRssFeedsData = (msg, respond) => {
        console.log('In getRssFeedsData');

        const feedDataParams = {};

        if (msg && msg.startDate && msg.endDate) {
            feedDataParams.startDate = moment(msg.startDate).utc().format(BOTNETS_DB_DATE_FORMAT);
            feedDataParams.endDate = moment(msg.endDate).utc().format(BOTNETS_DB_DATE_FORMAT);
        } else {
            // Default dates for botnets is decided to be last 24 hours.
            const endTime = moment();
            const startTime = moment().subtract(1, 'days');
            feedDataParams.startDate = startTime.format();
            feedDataParams.endDate = endTime.format();
        }
        console.log('Getting Botnet Feeds findings between ', feedDataParams.startDate, ' to ', feedDataParams.endDate);

        // Get feedsData from DB.
        return act('role:botnetFeeds, getBotnetFeedsFindings:botnetFeed', feedDataParams)
            .then((feedsData) => {
                const params = {};
                // If companyIDs were given - only results related to these companies are returned. Otherwise, returned data related to all companies in the system.
                if (msg && msg.companyIDs && isNotEmptyArray(msg.companyIDs)) {
                    params.query = msg.companyIDs;
                }
                // Get all companies from DB.
                return [act('role:companies, listByObjectID:company', params), feedsData];
            }).catch((e) => {
                console.log('Error in getRssFeedsData-getBotnetFeedsFindings: ', e);

                respond({ok: false});
            }).spread((allCompanies, feedsData) => {
                if (isNotEmptyArray(feedsData)) {
                    if (isNotEmptyArray(allCompanies)) {
                        console.log('Fetched ', feedsData.length, ' RSS Feeds records from DB');

                        const dataToReturn = [];

                        feedsData.map((currEntity) => {
                            // Make sure this feed result is relevant.
                            if (currEntity && currEntity.address && currEntity.domain && currEntity.feedName &&
                                (RSS_FEEDS_INFO.find((feed) => {
                                    return feed.feedName === currEntity.feedName;
                                }) ||
                                TXT_FEEDS_INFO.find((feed) => {
                                    return feed.feedName === currEntity.feedName;
                                }))) {
                                // Add related companies to the object.
                                currEntity.relatedCompanies = whoIsRelatedByDomain(allCompanies, currEntity.domain);

                                // Insert the entity to the returned array only if related to any given company.
                                if (isNotEmptyArray(currEntity.relatedCompanies)) {
                                    const currFeedName = currEntity.feedName;
                                    currEntity.id =  currEntity._id;

                                    // Remove redundant data from object.
                                    delete currEntity._id;
                                    delete currEntity.feedName;
                                    delete currEntity.contentIdentifier;

                                    // Find if feedName already exists in dataToReturn.
                                    const dataByFeedName = dataToReturn.find((d) => {
                                        return d.feedName === currFeedName;
                                    });
                                    // Separate by feedName and feedData.
                                    if (dataByFeedName) {
                                        // Meaning that this feedName already exists in dataToReturn, so add the new entity to the existing feed.
                                        dataByFeedName.feedData.push(currEntity);
                                    } else {
                                        // Meaning that this feedName is not inserted yet, so add the new feed with the new entity.
                                        dataToReturn.push({
                                            feedName: currFeedName,
                                            feedData: [currEntity]
                                        });
                                    }
                                }
                            }
                        });

                        if (isNotEmptyArray(dataToReturn)) {
                            console.log('Found relevant results for given companies');
                            respond({ok: true, data: dataToReturn});
                        } else {
                            console.log('No relevant results were found for given companies');
                            respond({ok: false});
                        }
                    } else {
                        console.log('getBotnetFeedsFindings didn\'t return any companies data');
                        respond({ok: false});
                    }
                } else {
                    console.log('getBotnetFeedsFindings didn\'t return any feeds records');
                    respond({ok: false});
                }
            }).catch((e) => {
                console.log('Error in getRssFeedsData-getAllCompanies: ', e);

                respond({ok: false});
            });
    };

    this.add('role:rssFeeds, cmd:collectFeeds', collectRssFeeds);
    this.add('role:rssFeeds, cmd:getFeedsData', getRssFeedsData);

    // This function load all existing companies -> domains -> IPs and returns all distinct IPs.
    const getAllIPsInSystem = (inputCompanyIDs) => {
        console.log('In getAllIPsInSystem - starting to collect IPs');

        // Get all companies.
        const params = {query: {}};
        // Check if admin requested a specific companies - otherwise all companies are returned.
        if (inputCompanyIDs && Array.isArray(inputCompanyIDs) && inputCompanyIDs.length > 0) {
            console.log('Requested companyIDs from Manual APIs: ', inputCompanyIDs);
            params.query = inputCompanyIDs;
        }
        return act('role:companies, listByObjectID:company', params)
            .then((allCompanies) => {
                // Get all domains.
                const allDomains = getDistinctDomainsByCompanies(allCompanies);
                allDomains.push('no_domain');
                if (isExistAndArray(allCompanies)) {
                    console.log('Fetched a total of ', allCompanies.length, ' companies with ', allDomains.length, ' domains');
                } else {
                    console.log('No companies were found on the system');
                }
                // Get all IPs.
                return act('role:intelQuery,cmd:getAllIPsByDomains', {domains: allDomains});
            }).catch((e) => {
                console.log('Error getting all companies in getAllIPsInSystem: ', e);
                return [];
            }).then((allIPs) => {
                if (isExistAndArray(allIPs)) {
                    console.log('Fetched a total of ', allIPs.length, ' IPs');
                } else {
                    console.log('No IPs were found on the system');
                    allIPs = [];
                }
                return allIPs;
            }).catch((e) => {
                console.log('Error getting all IPs by domains in getAllIPsInSystem: ', e);
                return [];
            });
    };

    const getGenericFeedPromise = (feedType, feedConfig, ips) => {
        return new Promise((resolve) => {
            let resultData = [];
            if (feedType && feedConfig) {
                let getSingleFeed, feedIPIdentifierProp;
                if (feedType === 'RSS') {
                    getSingleFeed = getSingleRssFeed(feedConfig.feedName, feedConfig.feedURL, feedConfig.feedProps);
                    feedIPIdentifierProp = 'feedIPIdentifierProp';
                } else {
                    // This type is "TXT".
                    getSingleFeed = getSingleTXTFeed(feedConfig);
                    feedIPIdentifierProp = 'feedIPPropName';
                }
                getSingleFeed
                    .then((data) => {
                        if (isNotEmptyArray(data)) {
                            console.log('Feed ', feedConfig.feedName, ' successfully fetched - got ', data.length, ' records');
                        }
                        console.log('Staring search for relevant feed records from: ', feedConfig.feedName);
                        return searchIPsInFeedData(ips, data, feedConfig[feedIPIdentifierProp], feedConfig.feedName);
                    }).catch((e) => {
                        console.log('Error getting RSS feed named \'', feedConfig.feedName, '\': ', e);
                        resolve(resultData);
                    }).then((res) => {
                        if (isNotEmptyArray(res)) {
                            resultData = res;

                            console.log('Feed search for relevant records from: ', feedConfig.feedName, ' resulted in ', res.length, ' matched records');
                        } else {
                            console.log('Feed search for relevant records from: ', feedConfig.feedName, ' resulted without any matched records');
                        }
                        resolve(resultData);
                    }).catch((e) => {
                        console.log('Error searching related IPs to RSS feed named \'', feedConfig.feedName, '\': ', e);
                        resolve(resultData);
                    });
            } else {
                resolve(resultData);
            }
        });
    };

    // This function iterates on all configured RSS Feeds and searches the relevant results for our system's IPs.
    const getAllBotnetRssFeeds = (ips) => {
        console.log('In getAllBotnetRssFeeds - starting to collect all RSS & TXT feeds data');

        if (isNotEmptyArray(ips) && isNotEmptyArray(RSS_FEEDS_INFO)) {
            const allPromises = [];
            let resultData = [];
            // Foreach RSS feed - fetch its data and try to match to input ips.
            RSS_FEEDS_INFO.map((currFeed) => {
                if (validateRSSConfig(currFeed)) {
                    console.log('Starting to fetch data from RSS feed: ', currFeed.feedName);
                    const currentPromise = getGenericFeedPromise('RSS', currFeed, ips)
                        .then((res) => {
                            if (isNotEmptyArray(res)) {
                                resultData = resultData.concat(res);
                            }
                        }).catch((e) => {
                            console.log('Error in getGenericFeedPromise by RSS feed named \'', currFeed.feedName, '\': ', e);
                        });

                    allPromises.push(currentPromise);
                }
            });

            // Foreach TXT feed - fetch its data and try to match to input ips.
            TXT_FEEDS_INFO.map((currFeed) => {
                if (validateTXTConfig(currFeed)) {
                    console.log('Starting to fetch data from TXT feed: ', currFeed.feedName);
                    const currentPromise = getGenericFeedPromise('TXT', currFeed, ips)
                        .then((res) => {
                            if (isNotEmptyArray(res)) {
                                resultData = resultData.concat(res);
                            }
                        }).catch((e) => {
                            console.log('Error in getGenericFeedPromise by TXT feed named \'', currFeed.feedName, '\': ', e);
                        });

                    allPromises.push(currentPromise);
                }
            });

            return PromiseB.all(allPromises)
                .then(() => {
                    console.log('Finished to fetch all records from all RSS feeds. Relevant feeds records: ', resultData.length);
                    return resultData;
                }).catch((e) => {
                    console.log('Error in fetching all records from RSS feeds: ', e);
                    return resultData;
                });
        }
    };

    // This function reads a single RSS feed and parses the content property by given feedProps.
    const getSingleRssFeed = (feedName, feedURL, feedProps) => {
        const data = [];
        const splitter = (feedName && feedName === 'Malwarebytes hpHosts') ? '<br>' : ', ';

        return parser.parseURL(feedURL)
            .then((feed) => {
                if (feed && isExistAndArray(feed.items)) {
                    feed.items.map((item) => {
                        if (item && item.content) {
                            const contentParser = item.content.split(splitter);
                            const parsedObject = {};
                            let isParsedObjectEmpty = true;
                            contentParser.map((currProp) => {
                                if (currProp) {
                                    const propParser = currProp.split(': ');
                                    if (propParser && propParser.length === 2) {
                                        const prop = propParser[0];
                                        const propData = propParser[1];

                                        const matchingKey = feedProps.find((p) => {
                                            return p.toLowerCase() === prop.toLowerCase();
                                        });
                                        if (matchingKey) {
                                            parsedObject[matchingKey] = propData;
                                            isParsedObjectEmpty = false;
                                        }
                                    }
                                }
                            });
                            if (!isParsedObjectEmpty) {
                                parsedObject.contentIdentifier = item.content;
                                data.push(parsedObject);
                            }
                        }
                    });
                }
                return data;
            }).catch((e) => {
                console.log('Error in getSingleRssFeed - ', feedName, ': ', e);
                return data;
            });
    };

    // This function reads a single TXT feed and parses the content by given URL.
    const getSingleTXTFeed = (config) => {
        return new Promise((resolve) => {
            const allTxtData = [];
            const options = {
                host: config.feedURLHost,
                path: config.feedURLPath
            };
            const request = http.request(options, function(res) {
                let data = '';
                res.on('data', (chunk) => {
                    data += chunk;
                });
                res.on('end', () => {
                    const str = data.toString();
                    const allFindings = str.split(config.feedSeparators.row);
                    allFindings.map((currFinding) => {
                        // Start parsing only if currFinding doesn't start with any of given strings-to-ignore from config.
                        if (currFinding && !config.feedIgnoreRowsStartsWith.find((strInitToIgnore) => {
                            return strInitToIgnore && currFinding.startsWith(strInitToIgnore);
                        })) {
                            const findingObj = {};
                            const allFindingData = currFinding.split(config.feedSeparators.column);
                            allFindingData.map((currFindingData, i) => {
                                if (currFindingData) {
                                    if (validateIPaddress(currFindingData)) {
                                        findingObj[config.feedIPPropName] = currFindingData;
                                    } else {
                                        // If this column is selected to be displayed by config - add its data.
                                        const matchingConfig = config.feedProps.find((prop) => {
                                            return prop && prop.column && prop.column === i+1;
                                        });
                                        if (matchingConfig && matchingConfig.label) {
                                            findingObj[matchingConfig.label] = currFindingData;
                                        }
                                    }
                                }
                            });
                            if (findingObj[config.feedIPPropName]) {
                                findingObj.feedName = config.feedName;
                                findingObj.contentIdentifier = currFinding;
                                // Fix for preventing duplicated results (of different createTime).
                                if (findingObj['createDate'] && findingObj.contentIdentifier.includes(findingObj['createDate'])) {
                                    findingObj.contentIdentifier = findingObj.contentIdentifier.replace(findingObj['createDate'], '');
                                }
                                allTxtData.push(findingObj);
                            }
                        }
                    });
                    resolve(allTxtData);
                });
            });
            request.on('error', (e) => {
                console.log('Error in getSingleTXTFeed - ', config.feedName, ': ', e);
                resolve(allTxtData);
            });
            request.end();
        });
    };

    // This function searches for a match between feedData and given IPs based on feedIPIdentifierProp.
    const searchIPsInFeedData = (ips, feedData, feedIPIdentifierProp, feedName, feedShouldInsertManualCreateDate = true) => {
        const foundIPs = [];
        if (isNotEmptyArray(ips) && isNotEmptyArray(feedData) && feedIPIdentifierProp && feedName) {
            feedData.map((currFeedData) => {
                if (currFeedData && currFeedData[feedIPIdentifierProp]) {
                    const currFeedIP = currFeedData[feedIPIdentifierProp];
                    // Check if each IPs record contains address and domain and equals to currFeedIP.
                    const matchedIP = ips.find((ip) => {
                        return ip && ip.address && ip.domain && ip.address === currFeedIP;
                    });
                    if (matchedIP) {
                        // Duplicate currFeedData JSON.
                        const dataToReturn = JSON.parse(JSON.stringify(currFeedData));

                        // Add matchedIP data.
                        dataToReturn.address = matchedIP.address;
                        dataToReturn.domain = matchedIP.domain;

                        // Add properties to recognize this finding.
                        dataToReturn.feedName = feedName;
                        if (feedShouldInsertManualCreateDate) {
                            dataToReturn.createDate = moment().format(BOTNETS_DB_DATE_FORMAT);
                        }

                        // Remove currFeedData' IP record since address will hold it for all types of feeds.
                        delete dataToReturn[feedIPIdentifierProp];
                        console.log('Matched feed record : ', dataToReturn);
                        foundIPs.push(dataToReturn);
                    }
                }
            });
        }
        return foundIPs;
    };

    // This function saves the matched feedsData to DB.
    const saveRelevantFeedData = (feedsData) => {
        if (isNotEmptyArray(feedsData)) {
            console.log('Starting to save ', feedsData.length, ' botnet RSS feeds findings on DB.');
            // Save feedsData to DB.
            return act('role:botnetFeeds, insertBotnetFeedsFindings:botnetFeed', {data: feedsData})
                .then((result) => {
                    if (result && result.ok) {
                        console.log('Successfully saved ', feedsData.length, ' botnet RSS feeds findings on DB.');
                        return {ok: true};
                    } else {
                        console.log('Failed saving botnet RSS feeds findings on DB.');
                        return {ok: false};
                    }
                }).catch((e) => {
                    console.log('Error in saveRelevantFeedData: ', e);
                    return {ok: false};
                });
        }
    };

    // This function checks whether the input is an array or not.
    const isExistAndArray = (data) => {
        return !!(data && Array.isArray(data));
    };

    // This function checks whether the input is an array with at least 1 item or not.
    const isNotEmptyArray = (data) => {
        return isExistAndArray(data) && data.length > 0;
    };
};

// This function is using RegEx to test if input is a valid IP Address.
// Conditions to pass: Must have 4 octets between [0-255]. Only digits are allowed.
const validateIPaddress = (ipaddress) => {
    return (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress));
};

module.exports.validateIPaddress = validateIPaddress;

const validateRSSConfig = (config) => {
    return !!(config && config.feedName && config.feedURL && config.feedProps && Array.isArray(config.feedProps) && config.feedIPIdentifierProp);
};

module.exports.validateRSSConfig = validateRSSConfig;

const validateTXTConfig = (config) => {
    return !!(config && config.feedName && config.feedURLHost && config.hasOwnProperty('feedURLPath') && config.feedIPPropName &&
        config.feedProps && Array.isArray(config.feedProps) && config.feedIgnoreRowsStartsWith && Array.isArray(config.feedIgnoreRowsStartsWith) &&
        config.feedSeparators && config.feedSeparators.row && config.feedSeparators.column);
};

module.exports.validateTXTConfig = validateTXTConfig;
