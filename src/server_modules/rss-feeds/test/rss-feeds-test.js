const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const rssFeedCollector = require('../rss-feeds-collector.js');

describe('RSS Feeds APIs', function() {
    describe('Identifying IP Address', function() {
        it('Should identify correct IP Addresses', function() {
            const validIPs = ['115.42.150.37', '192.168.0.1', '110.234.52.124', '10.0.0.138'];
            let areIPsValid = true;
            validIPs.map((currIP) => {
                areIPsValid = areIPsValid && rssFeedCollector.validateIPaddress(currIP);
            });
            return expect(areIPsValid).to.equals(true);
        });

        describe('Should identify wrong IP Addresses', function() {
            it('Must have 4 octets', function() {
                const invalidIPs = ['210.110', '255'];
                let areIPsValid = false;
                invalidIPs.map((currIP) => {
                    areIPsValid = areIPsValid || rssFeedCollector.validateIPaddress(currIP);
                });
                return expect(areIPsValid).to.equals(false);
            });
            it('Octet number must be between [0-255]', function() {
                const invalidIPs = ['666.10.10.20', '4444.11.11.11', '33.3333.33.3'];
                let areIPsValid = false;
                invalidIPs.map((currIP) => {
                    areIPsValid = areIPsValid || rssFeedCollector.validateIPaddress(currIP);
                });
                return expect(areIPsValid).to.equals(false);
            });
            it('Only digits are allowed', function() {
                const invalidIPs = ['y.y.y.y', '255.0.0.y'];
                let areIPsValid = false;
                invalidIPs.map((currIP) => {
                    areIPsValid = areIPsValid || rssFeedCollector.validateIPaddress(currIP);
                });
                return expect(areIPsValid).to.equals(false);
            });
        });
    });

    describe('Validating Feeds Configurations', function() {
        it('Should validate RSS feed configuration', function() {
            const config = {
                feedName: 'Feodo',
                feedURL: 'https://feodotracker.abuse.ch/feodotracker.rss',
                feedProps: ['Host', 'Version', 'Status', 'FirstSeen', 'LastSeen'],
                feedIPIdentifierProp: 'Host'
            };

            return expect(rssFeedCollector.validateRSSConfig(config)).to.equals(true);
        });

        it('Should validate TXT feed configuration', function() {
            const config = {
                feedName: 'c2-IP-MasterList',
                feedURLHost: 'osint.bambenekconsulting.com',
                feedURLPath: '/feeds/c2-ipmasterlist.txt',
                feedIPPropName: 'IP Address',
                feedProps: [{
                    column: 2,
                    label: 'Command & Control'
                }, {
                    column: 3,
                    label: 'createDate'
                }],
                feedSeparators: {
                    row: '\n',
                    column: ','
                },
                feedIgnoreRowsStartsWith: ['#'],
                feedShouldInsertManualCreateDate: false
            };

            return expect(rssFeedCollector.validateTXTConfig(config)).to.equals(true);
        });

        it('Should validate minimal TXT feed configuration', function() {
            const minimalTXTConfig = {
                feedName: 'c2-IP-MasterList',
                feedURLHost: 'osint.bambenekconsulting.com',
                feedURLPath: '/feeds/c2-ipmasterlist.txt',
                feedIPPropName: 'IP Address',
                feedProps: [],
                feedSeparators: {
                    row: '\n',
                    column: ','
                },
                feedIgnoreRowsStartsWith: []
            };

            return expect(rssFeedCollector.validateTXTConfig(minimalTXTConfig)).to.equals(true);
        });
    });

    // uncomment when testing new TXT / RSS feed list.
    /* describe('Testing some rss feeds APIs', function () {

        function getTXTFeed(config) {
            if (rssFeedCollector.validateTXTConfig(config)) {
                const http = require('http');
                const moment = require('moment');

                let options = {
                    host: 'osint.bambenekconsulting.com',
                    path: '/feeds/c2-ipmasterlist.txt'
                };
                let request = http.request(options, function (res) {
                    let allTxtData = [];
                    let data = '';
                    res.on('data', function (chunk) {
                        data += chunk;
                    });
                    res.on('end', function () {
                        let str = data.toString();
                        let allFindings = str.split(config.feedSeparators.row);
                        allFindings.map((currFinding) => {
                            // Start parsing only if currFinding doesn't start with any of given strings-to-ignore from config.
                            if (currFinding && !config.feedIgnoreRowsStartsWith.find((strInitToIgnore) => {
                                return strInitToIgnore && currFinding.startsWith(strInitToIgnore)
                            })) {
                                let findingObj = {};
                                let allFindingData = currFinding.split(config.feedSeparators.column);
                                allFindingData.map((currFindingData, i) => {
                                    if (currFindingData) {
                                        if (rssFeedCollector.validateIPaddress(currFindingData)) {
                                            findingObj[config.feedIPPropName] = currFindingData;
                                        }
                                        else {
                                            // If this column is selected to be displayed by config - add its data.
                                            let matchingConfig = config.feedProps.find((prop) => {
                                                return prop && prop.column && prop.column === i + 1
                                            });
                                            if (matchingConfig && matchingConfig.label) {
                                                findingObj[matchingConfig.label] = currFindingData;
                                            }
                                        }
                                    }
                                });
                                if (findingObj[config.feedIPPropName]) {
                                    if (config.feedShouldInsertManualCreateDate) {
                                        findingObj["createDate"] = moment().format("YYYY-MM-DD HH:mm");
                                    }
                                    findingObj.feedName = config.feedName;
                                    allTxtData.push(findingObj);
                                }
                            }
                        });
                        console.log("Feed resulted with ", allTxtData.length, " records.");
                    });
                });
                request.on('error', function (e) {
                    console.log(e.message);
                });
                request.end();
            }
        }

        it(' use this to test new TXT parser content props', function () {

            let TXT_FEEDS_INFO = {
                feedName: "c2-IP-MasterList",
                feedURLHost: "osint.bambenekconsulting.com",
                feedURLPath: "/feeds/c2-ipmasterlist.txt",
                feedIPPropName: "IP Address",
                feedProps: [{
                    column: 3,
                    label: "createDate"
                }],
                feedSeparators: {
                    row: "\n",
                    column: ","
                },
                feedIgnoreRowsStartsWith: ["#"],
                feedShouldInsertManualCreateDate: false
            };

            getTXTFeed(TXT_FEEDS_INFO);


        });
        it(' use this to test new rss parser content props', function () {
            const Parser = require('rss-parser');
            let parser = new Parser();

// To test a new feed - add its URL here and its content Props and set both to active.
            const feodoTrackerFeedURL = "https://feodotracker.abuse.ch/feodotracker.rss";
            const zeusTrackerFeedURL = "https://zeustracker.abuse.ch/rss.php";
            const malcodeFeedURL = "http://malc0de.com/rss/";
            const malwarebytesHpHostsURL = "https://hosts-file.net/rss.asp";
            const malwareDomainListFeedURL = "http://www.malwaredomainlist.com/hostslist/mdl.xml";
            const feodoTrackerProps = ["Host", "Version", "Status", "FirstSeen", "LastSeen"];
            const zeusTrackerProps = ["Host", "IP Address", "Status", "Level", "Malware", "AS", "Country", "SBL"];
            const malcodeProps = ["IP Address", "URL", "Country", "ASN", "MD5"];
            const malwarebytesHpHostsProps = ["Website", "IP", "Classification", "Added", "Added By"];
            const malwareDomainListProps = ["Host", "IP Address", "ASN", "Country", "Description"];

// Notice: when zeus finding is offline - the ip is empty.
            let activeURL = malwarebytesHpHostsURL;
            let activeProps = malwarebytesHpHostsProps;
            return parser.parseURL(activeURL)
                .then((feed) => {
                    if (feed && feed.items && Array.isArray(feed.items)) {
                        let data = [];
                        feed.items.map((item) => {
                            if (item && item.content) {
                                let contentParser = item.content.split("<br>");
                                let parsedObject = {};
                                contentParser.map((currProp) => {
                                    if (currProp) {
                                        let propParser = currProp.split(": ");
                                        if (propParser && propParser.length === 2) {
                                            let prop = propParser[0];
                                            let propData = propParser[1];

                                            let matchingKey = activeProps.find((p) => {
                                                return p.toLowerCase() === prop.toLowerCase()
                                            });
                                            if (matchingKey) {
                                                parsedObject[matchingKey] = propData;
                                            }
                                        }
                                    }
                                });
                                let parsedObjectString = "";
                                let keys = Object.keys(parsedObject);
                                keys.map((key, index) => {
                                    parsedObjectString += key + ": " + parsedObject[key];
                                    if (index < keys.length - 1) {
                                        parsedObjectString += " ; ";
                                    }
                                });
                                console.log("Parsed Object: ", parsedObjectString);
                                data.push(parsedObject);
                            }
                            //console.log(item);
                        });
                    }
                })
        });
    });*/
});
