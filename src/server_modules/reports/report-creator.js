'use strict';
const Promise = require('bluebird');
const excelReport = require('./excel-reports/excel-report.js');
const pdfReport = require('./pdf-reports/pdf-report.js');
const surveyPdfReport = require('./pdf-reports/survey-pdf-report.js');
const PdfHelpers = require('./pdf-reports/pdf-common.js');
const WordHelpers = require('./word-reports/word-report.js');
const helpers = require('./common.js');
const extraHelpers = require('../../server/api/helpers.js');
const intelHelpers = require('../utils/intel-helpers');
const {DATALEAKS_TYPE, BLACKLISTS_TYPE, BOTNETS_TYPE, SSL_CERTS_TYPE, GDPR_NOT_COMPLY_TYPE, EMAIL_BREACHES_TYPE} = require('../../../config/consts.js');
const config = require('app-config');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const mongoUrl = config.addresses.mongoConnectionString;
const PromiseB = require('bluebird');
const moment = require('moment');
const fs = require('fs');

module.exports = function reportCreator() {
    const seneca = this;
    const act = Promise.promisify(this.act, {context: this});

    const createReport = async (msg, respond) => {
        console.log('In createReport');

        if (!msg.data || !msg.data.uid || !msg.data.type) {
            respond({ok: false});
        }

        const displayScoreBackwards = msg.data.displayScoreBackwards || false;

        const data = {};
        data.isInDemoMode = msg.data.isInDemoMode;
        data.user = msg.data.uid;
        data.orgId = msg.data.orgId;
        data.type = msg.data.type;
        data.IDs = [];
        data.names = [];
        data.manualData = msg.data.manualData;
        data.manualDataID = msg.data.manualDataID;
        data.reportKind = msg.data.reportKind;
        data.userEmail = msg.data.userEmail;
        data.windowLocation = msg.data.windowLocation;
        if(msg.data.logo){
            data.logo = msg.data.logo;
        }
        if(msg.data && msg.data.companies && msg.data.companies[0] && msg.data.companies[0].name){
            data.companyName = msg.data.companies[0].name;
        }

        const projects = msg.data.projects;

        let companies = [];

        if (projects) {
            data.exportType = 'prj';

            data.IDs = projects.map((currProj) => {
                if (currProj.selectedCompanies && currProj.selectedCompanies.length > 0) {
                    companies = companies.concat(currProj.selectedCompanies);
                }

                if (currProj.projectName) {
                    data.names.push(currProj.projectName);
                }

                return currProj.id;
            });
        } else {
            companies = msg.data.companies;
            if (companies) {
                data.exportType = 'cmp';

                data.IDs = companies.map((currComp) => {
                    if (currComp.name) {
                        data.names.push(currComp.name);
                    }

                    return currComp.id;
                });
            } else {
                respond({ok: false});
            }
        }
        let res;
        let result;

        try {
            // Firstly get all companies data, then proceed to other stuff.
            res = await getCompaniesFullDataForReport(companies, data.orgId, act);
        }catch(e){
            console.error('Error in getCompaniesFullDataForReport: ' + e);
        }

        if (res && Array.isArray(res)) {
            // distinct companies result(res) by IDs.
            const uniqueRes = [];
            const keywordsForDataleaks = [];
            for (let i = 0; i < res.length; i++) {
                if (res[i] && res[i].id) {
                    let isUniqueID = true;
                    for (let j = 0; j < uniqueRes.length; j++) {
                        if (uniqueRes[j].id == res[i].id) {
                            isUniqueID = false;
                            break;
                        }
                    }
                    if (isUniqueID) {
                        uniqueRes.push(res[i]);

                        const keywords = res[i].keywords || [];
                        keywords.map((keyword) => {
                            if (keyword && !keywordsForDataleaks.includes(keyword)) {
                                keywordsForDataleaks.push(keyword);
                            }
                        });
                    }
                }
            }
            if (uniqueRes.length > 0) {
                companies = uniqueRes;
            }

            // Get all companies' CVE scores.
            try {
                result = await getAllDataForReport(data, companies, keywordsForDataleaks, act);
                console.log("Done getting all data for report");
            }catch(e){
                console.error("Error in getAllDataForReport: Error getting CVE Scores");
            }
        }else{
            console.log("No company Data found");
        }

        if (result && result.ok) {
            result = result.data;
            let companiesWithScores = [],
                resultCounts = {},
                allCVEVulns = [],
                vulnsByIP = [],
                allVulnsByIP = [],
                allPorts = [],
                allIPs = [],
                allASNs = [],
                allISPs = [],
                allProducts = [],
                allBuckets = [],
                allSPFs = [],
                allDMARCs = [],
                allDataleaks = [],
                allBlacklists = [],
                allBotnets = [],
                allSSLCerts = [],
                allEmailBreaches = [],
                allSubDomains = [],
                allGeoLocations = [],
                allAmData = {};

            if (result.cvss && Array.isArray(result.cvss)) {
                companiesWithScores = result.cvss;
            }
            if (result.cveVulns && Array.isArray(result.cveVulns)) {
                allCVEVulns = result.cveVulns;

                // This is for the Excel (no counters are necessary).
                allVulnsByIP = helpers.sortAllVulnsByIP(allCVEVulns);
                helpers.orderByCompanies(allVulnsByIP, companies);

                // This is for the PDF.
                vulnsByIP = helpers.sortVulnsByIP(allCVEVulns, resultCounts);
                helpers.countAllVulnByIP(allCVEVulns, resultCounts);
                helpers.orderByCompanies(vulnsByIP, companies);
            } else {
                resultCounts.totalCVEs = 0;
                resultCounts.criticalCVEs = 0;
            }

            if (result.ports && Array.isArray(result.ports)) {
                allPorts = result.ports;
                resultCounts.portsByIPCount = helpers.countPortsByIP(allPorts);
            } else {
                resultCounts.portsByIPCount = [];
            }

            if (result.ips && Array.isArray(result.ips)) {
                allIPs = result.ips;
                resultCounts.allIPsCount = helpers.countIPsWithAsnIps(allIPs);
                allIPs = helpers.groupIPsWithAsnIps(allIPs);
            } else {
                resultCounts.allIPsCount = 0;
            }

            if (result.asns && Array.isArray(result.asns)) {
                const countASNsObject = helpers.countASNs(result.asns);
                allASNs = countASNsObject.uniqueASNs;
                resultCounts.allASNsCount = countASNsObject.countASNs;
            } else {
                resultCounts.allASNsCount = 0;
            }

            if (result.isps && Array.isArray(result.isps)) {
                allISPs = result.isps;
            }

            if (result.products && Array.isArray(result.products)) {
                allProducts = result.products;
            }

            if (result.buckets && Array.isArray(result.buckets)) {
                allBuckets = result.buckets;
                resultCounts.allBucketsCount = helpers.countBuckets(allBuckets, resultCounts);
            } else {
                resultCounts.allBucketsCount = 0;
            }

            if (result.spfs && Array.isArray(result.spfs)) {
                allSPFs = result.spfs;
                resultCounts.allSPFsCount = helpers.countSPFs(allSPFs, resultCounts);
            } else {
                resultCounts.allSPFsCount = 0;
            }

            if (result.dmarcs && Array.isArray(result.dmarcs)) {
                allDMARCs = result.dmarcs;
                resultCounts.allDMARCsCount = helpers.countDMARCs(allDMARCs, resultCounts);
            } else {
                resultCounts.allDMARCsCount = 0;
            }

            if (result.dataleaks) {
                allDataleaks = result.dataleaks;
                resultCounts.allDataleaksCount = allDataleaks.length;
            } else {
                resultCounts.allDataleaksCount = 0;
            }

            if (result.emailBreaches) {
                allEmailBreaches = helpers.addRelatedCompaniesByDomains(companies, result.emailBreaches, resultCounts);
            }
            if (result.subdomains) {
                allSubDomains = result.subdomains;
                resultCounts.allSubDomainsCount = result.subdomains.length;
            }

            if (result.geoLocations) {
                allGeoLocations = result.geoLocations;
                resultCounts.allGeoLocationsCount = result.geoLocations.length;
            }

            if (result.blacklists) {
                allBlacklists = helpers.addRelatedCompaniesForBlacklists(companies, result.blacklists);
                resultCounts.allBlacklistsCount = allBlacklists.length;
            }

            if (result.botnets && Array.isArray(result.botnets)) {
                allBotnets = result.botnets;
                resultCounts.allBotnetsCount = result.botnets.length;
            }

            if (result.sslCerts && Array.isArray(result.sslCerts)) {
                allSSLCerts = result.sslCerts;
                resultCounts.allSslCertsCount = result.sslCerts.length;
            }

            if (result.allAmData && typeof result.allAmData === "object" && Object.getOwnPropertyNames(result.allAmData).length > 0) {
                allAmData = result.allAmData;
            }

            let locationRes;
            let lanLat;
            try {
                console.log("Starting to get location for report");
                locationRes = await helpers.getLocationsForReport(data, act);
            }catch(e){
                console.error("Error in getLocationsForReport: ", e);
            }
            if (locationRes.ok) {
                lanLat = locationRes.data;
            }else{
                console.error("Did not get results for location");
            }

            const findingsByTypeObj = (data.type === 'pdf' || data.type === 'word') ? {
                /* This is the findings for the PDF & Word report */
                imgFilename: null,
                companiesWithScores: companiesWithScores,
                vulnsByIP: vulnsByIP,
                companies: companies,
                allASNs: allASNs,
                allBuckets: allBuckets,
                allSPFs: allSPFs,
                allDMARCs: allDMARCs,
                allDataleaks: allDataleaks,
                allEmailBreaches: allEmailBreaches,
                allSubDomains: allSubDomains,
                allGeoLocations: allGeoLocations,
                allBlacklists: allBlacklists,
                allBotnets: allBotnets,
                allSSLCerts: allSSLCerts,
                allAmData: allAmData,
                resultCounts: resultCounts
            } : {
                /* This is the findings for the Excel report */
                imgFilename: null,
                companiesWithScores: companiesWithScores,
                allVulnsByIP: allVulnsByIP,
                allIPs: allIPs,
                companies: companies,
                allASNs: allASNs,
                allISPs: allISPs,
                allProducts: allProducts,
                allBuckets: allBuckets,
                allSPFs: allSPFs,
                allDMARCs: allDMARCs,
                allDataleaks: allDataleaks,
                allEmailBreaches: allEmailBreaches,
                allSubDomains: allSubDomains,
                allGeoLocations: allGeoLocations,
                allBlacklists: allBlacklists,
                allBotnets: allBotnets,
                allSSLCerts: allSSLCerts,
                allAmData: allAmData,
                allPorts: allPorts
            };

            const isSizeShouldBeReduced = (data.type === 'word');
            let imgFilename;
            try{
                imgFilename = await helpers.createMap(lanLat, isSizeShouldBeReduced);
                console.log('Saved map image');

                // Update each data.type's imgFilename finding.
                findingsByTypeObj.imgFilename = imgFilename;
            }catch(e){
                console.error('Error creating map in report:', e);
                res = await createReportByKind(data, displayScoreBackwards, findingsByTypeObj, seneca)
            }

            try {
                res = await createReportByKind(data, displayScoreBackwards, findingsByTypeObj, seneca);
            }catch(e){
                console.error("Error in createReportByKind: ", e);
            }

            try {
                if (res && res.ok && res.data && res.reportKind) {
                    let fileName = await generateReportFromStream(res.data, res.reportKind, seneca);
                    if (fileName) {
                        if(data.windowLocation && config.consts.enableReportSend){
                            let linkPath = data.windowLocation + fileName;
                            await sendExportedReportToUser(linkPath, data.userEmail || null, data.companyName, act);
                        }
                        console.log('Report exported successfully.');
                        respond({ok: true, data: fileName});
                    } else {
                        console.error('Error in generateReportFromStream() - failed to generate report');
                        respond({ok: false});
                    }
                } else {
                    console.error('Error in createReport() - was not able to create report');
                    respond({ok: false});
                }
            } catch (e) {
                console.error('Error in createSurveyReport() Error: '+ e);
                respond({ok: false});
            }
        } else {
            respond({ok: false});
        }
    };


    const sendExportedReportToUser = (linkPath, userEmail, name, promiseAct) => {
        return new Promise(async (resolve, reject) => {
            if (userEmail && typeof userEmail === 'string' && userEmail !== '' && userEmail !== ' ' &&
                linkPath && typeof linkPath === 'string' && linkPath !== '' && linkPath !== ' ' && promiseAct) {
                let nameText = '';
                if (name){
                    nameText = ' for ' + name;
                }
                if (typeof userEmail === 'object') {
                    // This is a fix to display API Key instead of user if needed.
                    userEmail = JSON.stringify(userEmail);
                }
                if (userEmail === "admin@admin.com") {
                    userEmail = ["tzach@rescana.com", "yuval@rescana.com"];
                }

                if (!Array.isArray(userEmail)) {
                    userEmail = [userEmail];
                }


                //we use fontColor for each one of the rows because the html text that sends with the
                //nodemailer npm sometimes change the color of the font on its own
                const fontColor = '#7e7e7e';

                let text = '<!DOCTYPE html><html><head></head><body style="color: '+ fontColor+' ">' +
                    '<div style="color: '+ fontColor +'; border-radius: 6px; border-bottom: none; background-color: #f7f7f7;">' +
                    '<div style="color: '+ fontColor +'; margin-left: 20px; ">';
                text += '<br/><p style="color: '+ fontColor +'">Hi, </p>';
                text += '<p style="color: '+ fontColor +'">The report you requested'+ nameText +' is ready and can be downloaded ' +
                    '<a style="color: #42afff" href="' + linkPath + '" target="_blank"> here</a>.</p>';
                text += '<p style="color: '+ fontColor +'">To download the file you must be logged in.</p>';

                text += '<br/><p style="color: '+ fontColor +'">Thank You,</p>';
                text += '<p style="color: '+ fontColor +'">The Rescana Team</p>';

                text += '<br/><br/></div><div style="border-radius: 0 0 6px 6px; background-color: #424242">' +
                    '<img style="padding: 3px 0 3px 0; margin-left: 88%;width: 110px; " alt="" ' +
                    'src="https://static.wixstatic.com/media/d64002_6495ce3b5ba747029c99b94aa741195e~mv2.png/v1/fill/w_412,h_236,al_c,lg_1/logo.png"/></div>';
                text += '</div></body></html>';


                if (userEmail && Array.isArray(userEmail) && userEmail.length > 0) {
                    try {
                        userEmail.map(async (emailToSendTo) => {
                            const msgObj = {
                                to: emailToSendTo,
                                subject: 'Rescana: Report Export',
                                text: text
                            };
                            await promiseAct('role:mailer, cmd:send', msgObj);
                            console.log('In sendExportedReportToUser() - Report link to: ' + emailToSendTo + ' has beenSent!');
                        });
                        resolve();
                    } catch (e) {
                        console.error('In sendExportedReportToUser() - Error:' + e);
                        reject();
                    }
                } else {
                    console.log('In sendExportedReportToUser() - Report was not send to any user - no mail to send to was found');
                    reject();
                }
            } else {
                console.error('In sendExportedReportToUser() - missing props to send the exported report.');
                reject();
            }
        });
    };


    const createSurveyReport = (msg, respond) => {
        console.log('In createSurveyReport');

        if (!msg.data || !msg.data.uid || !msg.data.type) {
            respond({ok: false});
        }

        const displayScoreBackwards = msg.data.displayScoreBackwards || false;

        const data = {};
        data.isInDemoMode = msg.data.isInDemoMode;
        data.user = msg.data.uid;
        data.type = msg.data.type;
        data.orgId = msg.data.orgId;
        data.IDs = [];
        data.names = [];
        data.manualData = msg.data.manualData;
        data.manualDataID = msg.data.manualDataID;
        data.reportKind = msg.data.reportKind;
        data.userEmail = msg.data.userEmail;
        data.windowLocation = msg.data.windowLocation;
        if(msg.data.logo){
            data.logo = msg.data.logo;
        }
        if(msg.data && msg.data.projects && msg.data.projects[0] && msg.data.projects[0].projectName){
            data.projectName = msg.data.projects[0].projectName;
        }
        const projects = msg.data.projects;

        let companies = [];

        if (projects) {
            data.exportType = 'prj';

            data.IDs = projects.map((currProj) => {
                if (currProj.selectedCompanies && currProj.selectedCompanies.length > 0) {
                    companies = companies.concat(currProj.selectedCompanies);
                }

                if (currProj.projectName) {
                    data.names.push(currProj.projectName);
                }

                return currProj.id;
            });
        } else {
            companies = msg.data.companies;
            if (companies) {
                data.exportType = 'cmp';

                data.IDs = companies.map((currComp) => {
                    if (currComp.name) {
                        data.names.push(currComp.name);
                    }

                    return currComp.id;
                });
            } else {
                respond({ok: false});
            }
        }
        // Firstly get all companies data, then proceed to other stuff.
        return getCompaniesFullDataForReport(companies, data.orgId, act)
            .then((res) => {
                if (res && Array.isArray(res)) {
                    // distinct companies result(res) by IDs.
                    const uniqueRes = [];
                    for (let i = 0; i < res.length; i++) {
                        if (res[i] && res[i].id) {
                            let isUniqueID = true;
                            for (let j = 0; j < uniqueRes.length; j++) {
                                if (uniqueRes[j].id == res[i].id) {
                                    isUniqueID = false;
                                    break;
                                }
                            }
                            if (isUniqueID) {
                                uniqueRes.push(res[i]);
                            }
                        }
                    }
                    if (uniqueRes.length > 0) {
                        companies = uniqueRes;
                    }
                    const findingsByTypeObj = {companies: companies};

                    return createReportByKind(data, displayScoreBackwards, findingsByTypeObj, seneca)
                }
            }).then(async (res) => {
                console.log('Done with creating survey report: ', res);
                try {
                    if (res && res.ok && res.data && res.reportKind) {
                        let fileName = await generateReportFromStream(res.data, res.reportKind, seneca);
                        if (fileName) {
                            if(data.windowLocation && config.consts.enableReportSend){
                                let linkPath = data.windowLocation + fileName;
                                await sendExportedReportToUser(linkPath, data.userEmail || null, data.projectName, act);
                            }
                            console.log('Report exported successfully.');
                            respond({ok: true, data: fileName});
                        } else {
                            console.error('Error in generateReportFromStream() - failed to generate report');
                            respond({ok: false});
                        }
                    } else {
                        console.error('Error in createSurveyReport() - was not able to create report');
                        respond({ok: false});
                    }
                } catch (e) {
                    console.error('Error in createSurveyReport() Error: '+ e);
                    respond({ok: false});
                }

            }).catch((e) => {
                console.log('Error creating report:', e);
                respond({ok: false});
            });
    };

    const createSingleSurveyAnswerReport = (msg, respond) => {
        console.log('In createSingleSurveyAnswerReport');

        if (!msg.data || !msg.data.uid || !msg.data.survey || !msg.data.survey.selectedCompanyId || !msg.data.survey.pages) {
            respond({ok: false});
        } else {
            return surveyPdfReport.getSurveyCompanyNameByID(msg.data.survey.selectedCompanyId, act)
                .then((res) => {
                    if (res.ok && res.companyName) {
                        let orgId = msg && msg.data && msg.data.orgId ? msg.data.orgId : '';

                        const htmlObj = surveyPdfReport.createHTMLForSingleSurvey(orgId, msg.data.survey, res.companyName);

                        PdfHelpers.createPDF(msg.data.uid, htmlObj, this)
                            .then(async (res) => {
                                if (res) {
                                    try{
                                        let fileName = await generateReportFromStream(res.data, 'surveyReport', seneca);
                                        respond({ok: true, data: fileName});
                                    } catch (e) {
                                        console.error('in createSingleSurveyAnswerReport: error: ' + e);
                                        respond({ok: false});
                                    }
                                } else {
                                    console.error('in createSingleSurveyAnswerReport: failed to create pdf');
                                    respond({ok: false});
                                }
                            }).catch((e) => {
                            if (e.error) {
                                console.log('Error in Creating PDF: ', e.error);
                            }
                            respond({ok: false});
                        });
                    } else {
                        if (res.error) {
                            console.log('Error in getSurveyAnswerNameByID: ', res.error);
                        }
                        respond({ok: false});
                    }
                })
                .catch((e) => {
                    if (e.error) {
                        console.log('Error in getSurveyAnswerNameByID: ', e.error);
                    }
                    respond({ok: false});
                });
        }
    };

    this.add('role:reportCreator, cmd:getReport', createReport);
    this.add('role:reportCreator, cmd:getSurveyReport', createSurveyReport);
    this.add('role:reportCreator, cmd:getSingleSurveyReport', createSingleSurveyAnswerReport);

    const generateReportFromStream = (rid, typeConfig) => {
        return new Promise((resolve, reject) => {
            console.log('Retrieving report-stream with rid: ', rid);

            MongoClient.connect(mongoUrl, async (error, client) => {
                const db = client.db('darkvision');
                let files = db.collection('fs.files');
                const find = PromiseB.promisify(files.findOne, {context: files});
                const result = await find({'filename': {$eq: rid}});

                if (result && result.metadata.stream_type && result.metadata.create_date) {
                    const bucket = new mongo.GridFSBucket(db, {
                        chunkSizeBytes: 1024
                    });

                    const date = stringifyDate(result.metadata.create_date);
                    const fileName = 'report--' + date + '.' + result.metadata.stream_type;
                    const configPath = (typeConfig === 'intelReport') ? config.paths.intelligenceReports :
                        (typeConfig === 'surveyReport') ? config.paths.surveyReports : config.paths.cveReports;

                    const filePath = configPath + '/' + fileName;

                    console.log('Starting to create file from buffer to ', filePath);

                    bucket.openDownloadStreamByName(rid)
                        .pipe(fs.createWriteStream(filePath))
                        .on('error', (error) => {
                            console.error('Error in bucket.openDownloadStreamByName: ', error);
                            reject(error);
                        })
                        .on('finish', () => {
                            console.log('{ Done saving report: ' + fileName + ' }');
                            resolve(fileName);
                        });
                } else {
                    console.log('No matching report-buffer found for rid: ', rid);
                    reject();
                }

            });
        });
    };


    module.exports.generateReportFromStream = generateReportFromStream;


    // This function add the unique create_time from DB in a correct format, if no such registry exists - generates a new unique ID.
    function stringifyDate(date) {
        let result = '';
        if (date) {
            const checkDate = moment(date);
            if (checkDate && checkDate.isValid()) {
                result = checkDate.format('YY-MM-DD--HH-mm-ss');
            }
        }
        if (!result) {
            result = shortid.generate();
        }
        return result;
    }


    const getCompaniesFullDataForReport = (companies,orgId, act) => {
        if (companies && companies.length > 0) {
            return Promise.map(companies, (company) => {
                return act('role:companies, get:company', {id: company.id});
            }).then((companiesFullData) => {
                if (companiesFullData) {
                    const requiredCompanyData = companiesFullData.map((currCompany) => {
                        if (currCompany && currCompany.id && currCompany.companyName) {
                            //check if company is unsafe
                            let isUnsafeCompany = false;
                            if(orgId && orgId !== 'No Id' && currCompany.dataByOrg && typeof currCompany.dataByOrg === 'object'){
                                Object.keys(currCompany.dataByOrg).map((key)=>{
                                    if(key === orgId && currCompany.dataByOrg[key].hasOwnProperty('isUnsafeCompany')){
                                        isUnsafeCompany = currCompany.dataByOrg[key].isUnsafeCompany;
                                    }
                                })
                            }
                            return {
                                id: currCompany.id,
                                companyName: currCompany.companyName,
                                selectedDomains: currCompany.selectedDomains || [],
                                keywords: currCompany.keywords || [],
                                ratios: currCompany.ratios,
                                surveyScore: (currCompany.surveyScore != null) ? currCompany.surveyScore : -1,
                                surveyLastUpdate: currCompany.scoresData && currCompany.scoresData.last_updated? currCompany.scoresData.last_updated : 'No Data',
                                surveyAnswered: currCompany.scoresData && currCompany.scoresData.isNA ? 'No' : 'Yes',
                                sensitivity: currCompany.sensitivity,
                                isUnsafeCompany: isUnsafeCompany
                            };
                        }
                    });
                    return (requiredCompanyData);
                }
            }).then((requiredCompanyData) => {
                //create companies id arr
                const companyIdArr = [];
                for (let i = 0; i < companies.length; i++) {
                    companyIdArr.push(companies[i].id);
                }
                //get survey info
                return [act('role:surveystore, listAnswersByCompanyIdArr:surveyAnswers', {
                    query: {companyIdArr: companyIdArr}
                }), requiredCompanyData];

            }).spread((companiesSurveyInfoArr, requiredCompanyData) => {

                //fill required info to the company data
                if (companiesSurveyInfoArr) {
                    companiesSurveyInfoArr.map((SurveyInfo) => {
                        for (let i = 0; i < requiredCompanyData.length; i++) {
                            if (requiredCompanyData) {
                                if (SurveyInfo.selectedCompanyId === requiredCompanyData[i].id) {
                                    requiredCompanyData[i].surveyName = SurveyInfo.name;
                                    requiredCompanyData[i].surveyCompletion = SurveyInfo.progress;
                                    requiredCompanyData[i].noGoAnsweredWrongArr = SurveyInfo.noGoAnsweredWrongArr || null;
                                }
                            }
                        }
                    });
                    return (requiredCompanyData);
                }
            }).then((requiredCompanyData) => {

                return intelHelpers.getCompanyExtraDataFromNeo(companies)
                    .then((extraData) => {
                        if (extraData && Array.isArray(extraData) && extraData.length > 0) {

                            extraData.map((extraDataForCompany) => {
                                for (let i = 0; i < requiredCompanyData.length; i++) {
                                    if (extraDataForCompany.extraData && requiredCompanyData) {
                                        if (extraDataForCompany.extraData.contacts && extraDataForCompany.id === requiredCompanyData[i].id) {
                                            requiredCompanyData[i].contact = extraDataForCompany.extraData.contacts[0].email;
                                        } else {
                                            requiredCompanyData[i].contact = '';
                                        }

                                    }
                                }
                            });
                            return (requiredCompanyData);
                        }
                    }).catch(() => {
                        return requiredCompanyData
                    });
            });
        }
    };


    const createReportByKind = async (data, displayScoreBackwards, findingsByTypeObj, seneca) => {
        if (data.type === 'pdf') {
            const htmlObj = pdfReport.createHTML(data.isInDemoMode, data.user, data.names, displayScoreBackwards, findingsByTypeObj, data.manualData, data.reportKind, data.logo);
            return await PdfHelpers.createPDF(data.user, htmlObj, seneca, data.reportKind);
        } else if (data.type === 'word') {
            let isWord = true;
            const htmlObj = WordHelpers.createHTML(data.isInDemoMode, findingsByTypeObj, data.names, displayScoreBackwards, data.manualData, data.reportKind, data.logo, isWord);
            return await WordHelpers.createWORD(data.user, htmlObj, seneca, data.reportKind);
        } else {
            return await excelReport.createExcel(data.isInDemoMode, data.user, data.names, displayScoreBackwards, findingsByTypeObj, seneca, data.reportKind);
        }
    };

    const getAllDataForReport = (data, companies, keywordsForDataleaks, act) => {
        console.log('In getAllDataForReport');

        let resultCVSSArr = [];
        let resultCVEVulnsArr = [];
        let resultPortsArr = [];
        let resultIPsArr = [];
        let resultASNsArr = [];
        let resultISPsArr = [];
        let resultProductsArr = [];
        let resultBucketsArr = [];
        let resultSPFsArr = [];
        let resultDMARCsArr = [];
        let resultDataleaksArr = [];
        let resultBlacklistsArr = [];
        let resultBotnetsArr = [];
        let resultSSLCertsArr = [];
        let resultEmailBreachesArr = [];
        let resultSubDomainsArr = [];
        let resultGeoLocationsArr = [];
        let resultAllAmData = {};

        const promiseArr = [];

        console.log('Getting data for: ', data.exportType);
        console.log(data.IDs);
        console.log(data.names);
        console.log(data.user);
        console.log('Starting getAllDataForReport');

        const promIntel = act('role:intelQuery,cmd:getAllDataForReport', {
            uid: data.user,
            exportType: data.exportType,
            IDs: data.IDs,
            names: data.names,
            orgId: data.orgId
        }).then((res) => {
            console.log('Got query results for getAllDataForReport');
            if (res) {
                const cveVulns = res.cveVulns;
                const ports = res.ports;
                const ips = res.ips;
                const asns = res.asns;
                const isps = res.isps;
                const products = res.products;
                const buckets = res.buckets;
                const spfs = res.spfs;
                const dmarcs = res.dmarcs;
                const emailBreaches = res.emailBreaches;
                const subdomains = res.subdomains;
                const geoLocations = res.geoLocations;
                const allAmData = res.allAmData;

                if (cveVulns && cveVulns.length > 0) {
                    resultCVEVulnsArr = cveVulns;
                }

                if (ports && ports.length > 0) {
                    resultPortsArr = ports;
                }

                if (ips && ips.length > 0) {
                    resultIPsArr = ips;
                }

                if (asns && asns.length > 0) {
                    resultASNsArr = asns;
                }

                if (isps && isps.length > 0) {
                    resultISPsArr = isps;
                }

                if (products && products.length > 0) {
                    resultProductsArr = products;
                }

                if (buckets && buckets.length > 0) {
                    resultBucketsArr = buckets;
                }

                if (spfs && spfs.length > 0) {
                    resultSPFsArr = spfs;
                }

                if (dmarcs && dmarcs.length > 0) {
                    resultDMARCsArr = dmarcs;
                }

                if (emailBreaches && emailBreaches.length > 0) {
                    resultEmailBreachesArr = emailBreaches;
                }
                if (subdomains && subdomains.length > 0) {
                    resultSubDomainsArr = subdomains;
                }
                if (geoLocations && geoLocations.length > 0) {
                    resultGeoLocationsArr = geoLocations;
                }
                if (allAmData && Object.getOwnPropertyNames(allAmData).length > 0) {
                    resultAllAmData = allAmData;
                }
            } else {
                console.error('Failed to load data for report');
                return Promise.resolve({ok: false});
            }
        }).catch((e) => {
            console.error('Error: error loading data for report: ' + e);
            return Promise.resolve({ok: false});
        });

        promiseArr.push(promIntel);

        const companyIDs = companies.map((currComp) => {
            return currComp.id;
        });

        const promIntelScores = act('role:intelQuery,cmd:getMaxCVSScore', {
            uid: data.user,
            orgId: data.orgId,
            companyIDs: companyIDs
        }).then((res) => {
            console.log('Got query results for getMaxCVSScore');
            if (res && Array.isArray(res) && res.length ) {
                if(res.length === 0){
                    console.error("getMaxCVSScore is empty.");
                }
                resultCVSSArr = res;
                return Promise.resolve(res);
            } else if (res){
                resultCVSSArr = res;
                return Promise.resolve(res);
            } else {
                console.error('Failed to load intel scores data for report');
                return Promise.resolve({ok: false});
            }
        }).catch((e) => {
            console.error('Error: error loading intel scores data for report: ' + e);
            return Promise.resolve({ok: false});
        });

        promiseArr.push(promIntelScores);

        const promDataleaks = act('role:pastes,getDataleaks:paste', {
            query: {
                uid: data.user,
                companiesKeywords: keywordsForDataleaks,
                companies: companies,
                lastMonth: true
            }
        }).then(async (res) => {
            console.log('Getting results for dataleaks');
            if (res && Array.isArray(res) && res.length > 0) {
                let amRes = [];
                if(data.orgId && data.orgId !== 'No Id') {
                    try {
                        amRes = await intelHelpers.updateResWithAssetUserInputAndMitigatedData(res, DATALEAKS_TYPE, ['_id'], data.orgId, true);
                    } catch (e) {
                        console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                    }
                    resultDataleaksArr = amRes.resultWithoutFpAndMitigated;
                    let dataleaksAm = [];
                    if (amRes.assetUserInputAndMitigatedResOnly) {
                        dataleaksAm = extraHelpers.removeUnnecessaryProps(amRes.assetUserInputAndMitigatedResOnly, DATALEAKS_TYPE);
                    }
                    return Promise.resolve({dataleaksAm: dataleaksAm});
                } else {
                    console.log("No Organization ID in promDataleaks");
                    resultDataleaksArr = res;
                    return Promise.resolve(res);
                }
            } else {
                console.log('No dataleaks found.');
                return Promise.resolve();
            }
        }).catch((e) => {
            console.log('Error getting dataleaks: ', e);
        });

        promiseArr.push(promDataleaks);

        const allDomains = helpers.getDistinctDomainsByCompanies(companies);

        const promBlacklists = act('role:blacklists, cmd:getDomainsFindings', {
            domains: allDomains
        }).then(async (res) => {
            console.log('Getting results for blacklists');
            if (res && res.ok && res.findings && Array.isArray(res.findings) && res.findings.length > 0) {
                let amRes = [];
                if(data.orgId && data.orgId !== 'No Id') {
                    try {
                        amRes = await intelHelpers.updateResWithAssetUserInputAndMitigatedData(res.findings, BLACKLISTS_TYPE, ['_id'], data.orgId, true);
                    } catch (e) {
                        console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                    }
                    resultBlacklistsArr = amRes.resultWithoutFpAndMitigated;
                    let blacklistsAm = [];
                    if (amRes.assetUserInputAndMitigatedResOnly) {
                        blacklistsAm = extraHelpers.removeUnnecessaryProps(amRes.assetUserInputAndMitigatedResOnly, BLACKLISTS_TYPE);
                    }
                    return Promise.resolve({blacklistsAm: blacklistsAm});
                } else {
                    console.log("No Organization ID in promBlacklists");
                    resultBlacklistsArr = res.findings;
                    return Promise.resolve();
                }
            } else {
                console.log('No blacklists were found.');
                return Promise.resolve();
            }
        }).catch((e) => {
            console.log('Error getting blacklists: ', e);
        });

        promiseArr.push(promBlacklists);

        const promBotnets = act('role:rssFeeds, cmd:getFeedsData', {
            companyIDs: companyIDs
        }).then(async (res) => {
            console.log('Getting results for Botnets');
            if (res && res.ok && res.data && Array.isArray(res.data) && res.data.length > 0) {
                let amRes = [];
                let feedArr =[];
                let botnetsAm =[];
                if(data.orgId && data.orgId !== 'No Id') {
                    try {
                        await extraHelpers.asyncForEach(res.data, async (curFeed) => {
                            amRes = await intelHelpers.updateResWithAssetUserInputAndMitigatedData(curFeed.feedData, BOTNETS_TYPE, ['id'], data.orgId, true);
                            if(amRes.resultWithoutFpAndMitigated && amRes.resultWithoutFpAndMitigated.length > 0){
                                feedArr.push({feedName: curFeed.feedName, feedData: amRes.resultWithoutFpAndMitigated});
                            }
                            if (amRes.assetUserInputAndMitigatedResOnly) {
                                botnetsAm = botnetsAm.concat(extraHelpers.removeUnnecessaryProps(amRes.assetUserInputAndMitigatedResOnly, BOTNETS_TYPE));
                            } else {
                                botnetsAm = botnetsAm.concat([]);
                            }
                        });
                    } catch (e) {
                        console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                    }
                    resultBotnetsArr = feedArr;
                    return Promise.resolve({botnetsAm: botnetsAm});
                } else {
                    console.log("No Organization ID in promBotnets");
                    resultBotnetsArr = res.data;
                    return Promise.resolve();
                }
            } else {
                console.log('No Botnets were found.');
                return Promise.resolve();
            }
        }).catch((e) => {
            console.log('Error getting Botnets: ', e);
        });

        promiseArr.push(promBotnets);

        const promSSLCerts = act('role:sslCertificatesQuery, cmd:getSSLCerts', {
            companyIDs: companyIDs
        }).then(async (res) => {
            console.log('Getting results for SSLCerts');
            if (res && res.ok && res.data && Array.isArray(res.data) && res.data.length > 0) {
                let amRes = [];
                if(data.orgId && data.orgId !== 'No Id') {
                    try {
                        amRes = await intelHelpers.updateResWithAssetUserInputAndMitigatedData(res.data, SSL_CERTS_TYPE, ['id'], data.orgId, true);
                        console.log("Got results for updateResWithAssetUserInputAndMitigatedData");
                    } catch (e) {
                        console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                    }
                    resultSSLCertsArr = amRes.resultWithoutFpAndMitigated;
                    let sslAm = [];
                    if (amRes.assetUserInputAndMitigatedResOnly) {
                        sslAm = extraHelpers.removeUnnecessaryProps(amRes.assetUserInputAndMitigatedResOnly, SSL_CERTS_TYPE);
                    }
                    return Promise.resolve({sslAm: sslAm});
                } else {
                    console.log("No Organization ID in promSSLCerts");
                    resultSSLCertsArr = res.data;
                    return Promise.resolve();
                }
            } else {
                console.log('No SSLCerts were found.');
                return Promise.resolve();
            }
        }).catch((e) => {
            console.log('Error getting SSLCerts: ', e);
        });

        promiseArr.push(promSSLCerts);

        return Promise.all(promiseArr)
            .then((amRes) => {
                console.log('Done with all the promises');
                if (resultCVSSArr.length > 0 || resultCVEVulnsArr.length > 0 || resultPortsArr.length > 0 || resultIPsArr.length > 0 || resultASNsArr.length > 0 || resultBucketsArr.length > 0 || resultSPFsArr.length > 0) {
                    if(amRes && Array.isArray(amRes) && amRes.length > 0){
                        amRes.map((curData)=>{
                            if(curData){
                                if(curData.dataleaksAm){
                                    resultAllAmData.dataleaksAm = curData.dataleaksAm;
                                } else if(curData.blacklistsAm){
                                    resultAllAmData.blacklistsAm = curData.blacklistsAm;
                                } else if(curData.botnetsAm){
                                    resultAllAmData.botnetsAm = curData.botnetsAm;
                                } else if(curData.sslAm){

                                    //create separated array for nossl data
                                    if (Array.isArray(curData.sslAm) && curData.sslAm.length > 0) {
                                        curData.noSslAm = [];
                                        for (let i = 0; i < curData.sslAm.length; i++) {
                                            if (curData.sslAm[i] && curData.sslAm[i].statusSSL && curData.sslAm[i].statusSSL !== "https") {
                                                curData.noSslAm.push(curData.sslAm[i]);
                                                curData.sslAm.splice(i, 1);
                                            }
                                        }
                                    }

                                    resultAllAmData.sslAm = curData.sslAm;
                                    resultAllAmData.noSslAm = curData.noSslAm;
                                }
                            }
                        });
                    }
                    return ({
                        ok: true,
                        data: {
                            cvss: resultCVSSArr,
                            cveVulns: resultCVEVulnsArr,
                            ports: resultPortsArr,
                            ips: resultIPsArr,
                            asns: resultASNsArr,
                            isps: resultISPsArr,
                            products: resultProductsArr,
                            buckets: resultBucketsArr,
                            spfs: resultSPFsArr,
                            dmarcs: resultDMARCsArr,
                            dataleaks: resultDataleaksArr,
                            emailBreaches: resultEmailBreachesArr,
                            subdomains: resultSubDomainsArr,
                            geoLocations: resultGeoLocationsArr,
                            blacklists: resultBlacklistsArr,
                            botnets: resultBotnetsArr,
                            sslCerts: resultSSLCertsArr,
                            allAmData: resultAllAmData
                        }
                    });
                } else {
                    console.error('No results returned at load max CVSScore, cve Risks, IPs, ASNs and ports for report');
                    return ({ok: false});
                }
            })
            .catch((e) => {
                console.log(e);
                return ({ok: false});
            });
    };
};
