const path = require('path');
const shortid = require('shortid');
const moment = require('moment');
const PromiseB = require('bluebird');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const config = require('app-config');
const mongoUrl = config.addresses.mongoConnectionString;

const EXCEL_TYPE = 'xlsx';
const PDF_TYPE = 'pdf';
const WORD_TYPE = 'docx';
const BUFFER_TYPES = [EXCEL_TYPE, PDF_TYPE, WORD_TYPE];

module.exports.turnIdsToCompanies = (ids, companies) => {
    const idsArr = ids.split(',');
    const companyArr = [];

    // Iterate over IDs and find company name.
    for (let i = 0; i < idsArr.length; i++) {
        for (let j = 0; j < companies.length; j++) {
            if (companies[j].id === idsArr[i]) {
                companyArr.push(companies[j].companyName);
            }
        }
    }

    return companyArr.join(', ');
};

module.exports.getFilePathInitial = () => {
    // Getting absolute path in current syntax.
    const dir = __dirname.replace(new RegExp(/\\/g), '/');
    // Moving to main folder (darkVision).
    return 'file:///' + dir;
};

module.exports.getFilePathInitialForExcel = () => {
    // Getting absolute path in current syntax.
    return __dirname.replace(new RegExp(/\\/g), '/');
};

const toNumber = (score) => {
    return Number(score) || 0;
};

// This function looks for critical CVEs at first. If none found - returns top 30 highest score CVEs.
module.exports.sortVulnsByIP = (ipVulns, resultCounts) => {
    // This array collect the regular CVEs (before any critical ones were found).
    const vulnsByIP = [];
    // This array collect the critical CVEs.
    const criticalVulns = [];
    // This is the array that returned for the report.
    let arrayToIterate = [];
    // Some const settings.
    const criticalThreshold = 7.5;
    const maxNotCriticalCVEs = 30;

    if (ipVulns && Array.isArray(ipVulns)) {
        for (let i = 0; i < ipVulns.length; i++) {
            const current = ipVulns[i];
            // Check if required properties exist.
            if (current && current.hostname && current.ip && current.port && current.cpe && current.cve) {
                const isCritical = (current.cveScore != null) && (toNumber(current.cveScore) >= criticalThreshold);

                // If this CVE is critical - check if exist already on criticalArray.
                if (isCritical) {
                    arrayToIterate = criticalVulns;
                }
                // If this CVE is not critical but some critical CVEs have already been found - ignore this CVE.
                else if (criticalVulns.length > 0) {
                    continue;
                }
                // Otherwise - collect any CVE.
                else {
                    arrayToIterate = vulnsByIP;
                }

                let isIPExist = false;
                let j;
                for (j = 0; j < arrayToIterate.length; j++) {
                    if (current.ip === arrayToIterate[j].ip &&
                        current.cve === arrayToIterate[j].cve &&
                        current.cpe === arrayToIterate[j].cpe &&
                        current.port === arrayToIterate[j].port) {
                        isIPExist = true;
                        break;
                    }
                }
                // If subdomain lead to this CVE, add it, otherwise add the domain(hostname).
                let currDomain;
                if (current.subdomain && current.subdomain !== 'none') {
                    currDomain = current.subdomain;
                } else {
                    currDomain = current.hostname;
                }

                // Group all domains by IP.
                if (isIPExist) {
                    if (!arrayToIterate[j].domains.includes(currDomain)) {
                        arrayToIterate[j].domains.push(currDomain);
                    }
                } else {
                    arrayToIterate.push({
                        domains: [currDomain],
                        ip: current.ip,
                        port: current.port,
                        cpe: current.cpe,
                        cve: current.cve,
                        cveScore: toNumber(current.cveScore),
                        cveSummary: current.summary || ''
                    });
                }
            }
        }
    }

    // Sort the CVEs from highest score to lowest (DESC).
    arrayToIterate = arrayToIterate.sort((vulnA, vulnB) => {
        return toNumber(vulnB.cveScore) - toNumber(vulnA.cveScore);
    });

    // Check if no critical CVEs were found.
    if (criticalVulns.length === 0) {
        // Select only 30 highest scores.
        arrayToIterate = arrayToIterate.slice(0, maxNotCriticalCVEs);
    }

    // Save the counts for summary display.
    if (resultCounts) {
        resultCounts.criticalCVEs = criticalVulns.length;
    } else {
        resultCounts = {totalCVEs: 0, criticalCVEs: 0};
    }

    return arrayToIterate;
};

// This function sort all CVEs by IPs and order by highest score CVEs.
module.exports.sortAllVulnsByIP = (ipVulns) => {
    // This is the array that returned for the Excel.
    let arrayToIterate = [];

    if (ipVulns && Array.isArray(ipVulns)) {
        for (let i = 0; i < ipVulns.length; i++) {
            const current = ipVulns[i];
            // Check if required properties exist.
            if (current && current.hostname && current.ip && current.port && current.cpe && current.cve) {
                let isIPExist = false;
                let j;
                for (j = 0; j < arrayToIterate.length; j++) {
                    if (current.ip === arrayToIterate[j].ip &&
                        current.cve === arrayToIterate[j].cve &&
                        current.cpe === arrayToIterate[j].cpe &&
                        current.port === arrayToIterate[j].port) {
                        isIPExist = true;
                        break;
                    }
                }
                // If subdomain lead to this CVE, add it, otherwise add the domain(hostname).
                let currDomain;
                if (current.subdomain && current.subdomain !== 'none') {
                    currDomain = current.subdomain;
                } else {
                    currDomain = current.hostname;
                }

                // Group all domains by IP.
                if (isIPExist) {
                    if (!arrayToIterate[j].domains.includes(currDomain)) {
                        arrayToIterate[j].domains.push(currDomain);
                    }
                } else {
                    arrayToIterate.push({
                        domains: [currDomain],
                        ip: current.ip,
                        port: current.port,
                        cpe: current.cpe,
                        cve: current.cve,
                        cveScore: toNumber(current.cveScore),
                        cveSummary: current.summary || ''
                    });
                }
            }
        }
    }

    // Sort the CVEs from highest score to lowest (DESC).
    arrayToIterate = arrayToIterate.sort((vulnA, vulnB) => {
        return toNumber(vulnB.cveScore) - toNumber(vulnA.cveScore);
    });

    return arrayToIterate;
};

module.exports.countAllVulnByIP = (ipVulns, resultCounts) => {
    const uniqueVulnsByIP = [];

    if (ipVulns && Array.isArray(ipVulns)) {
        for (let i = 0; i < ipVulns.length; i++) {
            const current = ipVulns[i];

            // Check if required properties exist.
            if (current && current.ip && current.port && current.cpe && current.cve) {
                let isIPExist = false;
                for (let j = 0; j < uniqueVulnsByIP.length; j++) {
                    if (current.ip === uniqueVulnsByIP[j].ip &&
                        current.cve === uniqueVulnsByIP[j].cve &&
                        current.cpe === uniqueVulnsByIP[j].cpe &&
                        current.port === uniqueVulnsByIP[j].port) {
                        isIPExist = true;
                        break;
                    }
                }
                if (!isIPExist) {
                    uniqueVulnsByIP.push(current);
                }
            }
        }
    }

    resultCounts.totalCVEs = uniqueVulnsByIP.length;
};

module.exports.countPortsByIP = (ports) => {
    let countIPs = 0;
    let countPorts = 0;
    const ipOfPorts = [];
    if (ports && ports.length > 0) {
        countPorts = ports.length;
        ports.map((currPort) => {
            if (currPort && currPort.port && currPort.ip && !ipOfPorts.includes(currPort.ip)) {
                ipOfPorts.push(currPort.ip);
            }
        });
        countIPs = ipOfPorts.length;
    }
    return [countPorts, countIPs];
};

module.exports.countASNs = (allASNs) => {
    const uniqueASNs = [];
    if (allASNs && allASNs.length > 0) {
        allASNs.map((currEntity) => {
            if (currEntity && currEntity.asn) {
                let currASN = currEntity.asn;
                if (!currASN.startsWith('AS') && Number.isInteger(Number(currASN))) {
                    currASN = 'AS' + currASN;
                }
                let isFound = false;
                // Check if current ASN not exists already.
                for (let i = 0; i < uniqueASNs.length; i++) {
                    if (uniqueASNs[i].asn === currASN) {
                        // Check if current information is not already saved in uniqueASNs[i] object.
                        if (currEntity.name && currEntity.finding && !uniqueASNs[i].hasOwnProperty(currEntity.name)) {
                            uniqueASNs[i][currEntity.name] = currEntity.finding;
                        }
                        // Check if current domain is not already saved in uniqueASNs[i] object.
                        if (currEntity.hostname) {
                            // If there are no domains connected yet - add this one as the first.
                            if (!uniqueASNs[i].hasOwnProperty('domains')) {
                                uniqueASNs[i].domains = [currEntity.hostname];
                            }
                            // Otherwise, if this domain is not assigned already, add it.
                            else if (!uniqueASNs[i].domains.includes(currEntity.hostname)) {
                                uniqueASNs[i].domains.push(currEntity.hostname);
                            }
                        }
                        isFound = true;
                        break;
                    }
                }
                // Add currEntity as a new ASN.
                if (!isFound) {
                    const asnObject = {asn: currASN};
                    if (currEntity.name && currEntity.finding) {
                        asnObject[currEntity.name] = currEntity.finding;
                    }
                    if (currEntity.hostname) {
                        asnObject.domains = [currEntity.hostname];
                    }
                    uniqueASNs.push(asnObject);
                }
            }
        });
    }
    return ({uniqueASNs: uniqueASNs, countASNs: uniqueASNs.length});
};

module.exports.addRelatedCompaniesByDomains = (companies, allEmailBreaches, resultCounts) => {
    const resultEmailBreaches = [];

    const uniqueEmails = [];

    const uniqueBreaches = [];

    if (companies && allEmailBreaches) {
        allEmailBreaches.map((currBreach) => {
            if (currBreach && currBreach.domain && currBreach.email && currBreach.breach) {
                currBreach.relatedCompanies = whoIsRelatedByDomain(companies, currBreach.domain);
                resultEmailBreaches.push(currBreach);

                if (uniqueEmails.indexOf(currBreach.email) === -1) {
                    uniqueEmails.push(currBreach.email);
                }
                if (uniqueBreaches.indexOf(currBreach.breach) === -1) {
                    uniqueBreaches.push(currBreach.breach);
                }
            }
        });
    }

    resultCounts.countEmailsBreached = uniqueEmails.length;

    resultCounts.countEmailBreaches = uniqueBreaches.length;

    return (resultEmailBreaches);
};

const whoIsRelatedByDomain = (companies, domain) => {
    const relatedCompanies = [];
    if (companies && domain) {
        companies.map((company) => {
            if (company.selectedDomains && company.selectedDomains.indexOf(domain) !== -1) {
                relatedCompanies.push(company.companyName);
            }
        });
    }
    return (relatedCompanies);
};

module.exports.addRelatedCompaniesForBlacklists = (companies, allBlacklists) => {
    const resultBlacklists = [];

    if (companies && allBlacklists) {
        allBlacklists.map((currFinding) => {
            if (currFinding && currFinding.domain) {
                currFinding.relatedCompanies = whoIsRelatedByDomain(companies, currFinding.domain);
                resultBlacklists.push(currFinding);
            }
        });
    }

    return resultBlacklists;
};

module.exports.groupIPsWithAsnIps = (allIPs) => {
    const distinctAllIPs = [];
    if (allIPs && allIPs.length > 0) {
        // This iteration is for adding all discovered IPs.
        allIPs.map((currEntity) => {
            if (currEntity && currEntity.ip && currEntity.hostname && currEntity.subdomain && !isIPExists(currEntity, distinctAllIPs)) {
                distinctAllIPs.push({
                    hostname: currEntity.hostname,
                    subdomain: currEntity.subdomain,
                    ip: currEntity.ip,
                    asn: currEntity.asn || ''
                });
            }
        });

        // This iteration is for adding all undiscovered ASN_IPs.
        allIPs.map((currEntity) => {
            if (currEntity && currEntity.asn && currEntity.asn_ip && !isASNIPExists(currEntity, distinctAllIPs)) {
                distinctAllIPs.push({
                    hostname: '',
                    subdomain: '',
                    ip: currEntity.asn_ip,
                    asn: currEntity.asn
                });
            }
        });
    }
    return (distinctAllIPs);
};

// Use only for groupIPsWithAsnIps.
const isIPExists = (ipEntity, array) => {
    let isExist = false;
    if (array.length > 0) {
        for (let i = 0; i < array.length; i++) {
            const currExistingEntity = array[i];
            if (currExistingEntity.ip === ipEntity.ip && currExistingEntity.hostname === ipEntity.hostname && currExistingEntity.subdomain === ipEntity.subdomain) {
                isExist = true;
                break;
            }
        }
    }
    return isExist;
};
// Use only for groupIPsWithAsnIps.
const isASNIPExists = (ipEntity, array) => {
    let isExist = false;
    if (array.length > 0) {
        for (let i = 0; i < array.length; i++) {
            const currExistingEntity = array[i];
            if (currExistingEntity.ip === ipEntity.asn_ip) {
                isExist = true;
                break;
            }
        }
    }
    return isExist;
};

module.exports.getDistinctDomainsByCompanies = (companies) => {
    const uniqueDomains = [];

    if (companies && Array.isArray(companies)) {
        companies.map((currComp) => {
            if (currComp && currComp.selectedDomains && Array.isArray(currComp.selectedDomains)) {
                currComp.selectedDomains.map((currDomain) => {
                    if (currDomain && !uniqueDomains.includes(currDomain)) {
                        uniqueDomains.push(currDomain);
                    }
                });
            }
        });
    }

    return uniqueDomains;
};

module.exports.countIPsWithAsnIps = (allIPs) => {
    const uniqueIPs = [];
    if (allIPs && allIPs.length > 0) {
        allIPs.map((currEntity) => {
            if (currEntity && currEntity.ip) {
                if (!uniqueIPs.includes(currEntity.ip)) {
                    uniqueIPs.push(currEntity.ip);
                }
                if (currEntity.asn_ip && !uniqueIPs.includes(currEntity.asn_ip)) {
                    uniqueIPs.push(currEntity.asn_ip);
                }
            }
        });
    }
    return (uniqueIPs.length);
};

module.exports.countBuckets = (allBuckets, resultCounts) => {
    const uniqueBuckets = [];
    resultCounts.notConfiguredBuckets = 0;
    if (allBuckets && allBuckets.length > 0) {
        allBuckets.map((currEntity) => {
            if (currEntity && currEntity.bucket) {
                if (!uniqueBuckets.includes(currEntity.bucket)) {
                    uniqueBuckets.push(currEntity.bucket);
                }
                if (!currEntity.bucket_status || currEntity.bucket_status === 'Public') {
                    resultCounts.notConfiguredBuckets++;
                }
            }
        });
    }
    return (uniqueBuckets.length);
};

module.exports.countDMARCs = (allDMARCs, resultCounts) => {
    let configuredDmarcs = 0;
    resultCounts.notConfiguredDMARCs = 0;
    if (allDMARCs && allDMARCs.length > 0) {
        allDMARCs.map((currEntity) => {
            if (currEntity && currEntity.dmarc) {
                if (currEntity.dmarc === 'No Records Found') {
                    resultCounts.notConfiguredDMARCs++;
                } else {
                    configuredDmarcs++;
                }
            }
        });
    }
    return (configuredDmarcs);
};

module.exports.countSPFs = (allSPFs, resultCounts) => {
    let configuredSPFs = 0;
    resultCounts.notConfiguredSPFs = 0;
    if (allSPFs && allSPFs.length > 0) {
        allSPFs.map((currEntity) => {
            if (currEntity && currEntity.spf) {
                if (currEntity.spf === 'No Records Found') {
                    resultCounts.notConfiguredSPFs++;
                } else {
                    configuredSPFs++;
                }
            }
        });
    }
    return (configuredSPFs);
};

module.exports.getLocationsForReport = (data, act) => {
    console.log('In getLocationsForReport');

    return act('role:intelQuery,cmd:getLocationsForReport', {
        uid: data.user,
        exportType: data.exportType,
        names: data.names,
        IDs: data.IDs
    })
        .then((locations) => {
            console.log("Got locations from query.");
            const latLan = this.groupLatLan(locations);
            return ({ok: true, data: latLan});
        }).catch((e) => {
            console.error('Failed to load locations for report: ' + e);
            return ({ok: false});
        });
};

module.exports.groupLatLan = (locations) => {
    const locationObj = {};
    for (let i = 0; i < locations.length; i++) {
        if (!locationObj[locations[i].ip]) {
            locationObj[locations[i].ip] = {};
        }
        if (locations[i].dis === 'latitude') {
            locationObj[locations[i].ip].latitude = locations[i].value;
        } else if (locations[i].dis === 'longitude') {
            locationObj[locations[i].ip].longitude = locations[i].value;
        }

        if (locations[i].subdomain !== 'none') {
            locationObj[locations[i].ip].domain = locations[i].subdomain;
        } else {
            locationObj[locations[i].ip].domain = locations[i].hostname;
        }
    }
    return locationObj;
};

module.exports.createMap = (lanLat, isSizeShouldBeReduced) => {
    return new Promise((resolve) => {
        const L = require('leaflet-headless');
        const document = global.document;
        const element = document.createElement('div');
        element.id = 'map-leaflet-image';
        document.body.appendChild(element);
        delete L.Icon.Default.prototype._getIconUrl;
        const markerIconPath = path.join(__dirname, '/resources/marker-icon.png');
        L.Icon.Default.mergeOptions({
            iconUrl: markerIconPath
        });

        const filename = path.join(__dirname, '/resources/leaflet-image.png');

        const map = L.map(element.id, {renderer: L.canvas()}).setView([0, 0], 1);

        const markers = [];
        const uniqueArr = [];


        Object.keys(lanLat).map((key) => {
            const lat = parseFloat(lanLat[key].latitude);
            const lon = parseFloat(lanLat[key].longitude);

            let duplicate = false;

            for (let i = 0; i < uniqueArr.length; i++) {
                if (uniqueArr[i].lat === lat && uniqueArr[i].lon === uniqueArr[i].lon) {
                    duplicate = true;
                }
            }

            // Validate lat and long.
            if ((lat >= -90 && lat <= 90 && lon >= -180 && lon <= 180) && !duplicate) {
                let doAdd = true;
                for (let i = 0; i < uniqueArr.length; i++) {
                    if (this.distance(uniqueArr[i].lat, uniqueArr[i].lon, lat, lon, 'M') > 8000) {
                        console.log('Marker at: ', uniqueArr[i].lat, ',', uniqueArr[i].lon, 'and marker at : ', lat, ',', lon, 'are more then ', 8100, 'units apart.');
                        doAdd = false;
                    }
                }

                if (doAdd) {
                    console.log('Adding marker in Lat : ', lat, 'and lan : ', lon);
                    uniqueArr.push({lat: lat, lon: lon});
                    L.marker([lat, lon]).addTo(map);
                    markers.push(L.marker([lat, lon]));
                }
            }
        });

        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        const group = L.featureGroup(markers).addTo(map);
        if (isSizeShouldBeReduced) {
            // This is for Word reports to display at proper size.
            map.setSize(760, 650);
        }
        map.fitBounds(group.getBounds());

        console.log("Saving Map Image");
        map.saveImage(filename, () => {
            console.log("Removing old map if exists");
            map.remove();
            resolve(this.getFilePathInitial() + '/resources/leaflet-image.png');
        });
    });
};

module.exports.distance = (lat1, lon1, lat2, lon2, unit) => {
    const radlat1 = Math.PI * lat1 / 180;
    const radlat2 = Math.PI * lat2 / 180;
    const theta = lon1 - lon2;
    const radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit === 'K') {
        dist = dist * 1.609344;
    }
    if (unit === 'N') {
        dist = dist * 0.8684;
    }
    return dist;
};

module.exports.orderByCompanies = (ipVulns, companies) => {
    if (ipVulns && ipVulns.length > 0 && companies && companies.length > 0) {
        companies.map((currComp) => {
            let hasCVEVuln = false;
            if (currComp && currComp.id && currComp.selectedDomains && currComp.selectedDomains.length > 0) {
                currComp.selectedDomains.map((currDomain) => {
                    for (let i = 0; i < ipVulns.length; i++) {
                        if (ipVulns[i] && ipVulns[i].domains && ipVulns[i].domains.length > 0 && ipVulns[i].domains.find((d) => d.includes(currDomain))) {
                            hasCVEVuln = true;
                            if (ipVulns[i].relatedCompanies && !ipVulns[i].relatedCompanies.includes(currComp.id)) {
                                ipVulns[i].relatedCompanies.push(currComp.id);
                            } else if (!ipVulns[i].relatedCompanies) {
                                ipVulns[i].relatedCompanies = [currComp.id];
                            }
                        }
                    }
                });

                currComp.hasCVEVulns = hasCVEVuln;
            }
        });
    }
};

const calculateTotalScore = (maxIntelScore, surveyScore, ratios, isSurveyNA) => {
    maxIntelScore = Math.floor(maxIntelScore);
    surveyScore = Math.floor(surveyScore);
    let shouldCalcSurvey = true;
    if (isNaN(surveyScore) || isSurveyNA) {
        shouldCalcSurvey = false;
        surveyScore = 0;
    }
    if (!surveyScore) {
        surveyScore = 0;
    }
    if (!maxIntelScore || isNaN(maxIntelScore)) {
        maxIntelScore = 0;
    }


    if (ratios && (ratios.surveyWeight || ratios.surveyWeight === 0) && (ratios.intelWeight || ratios.intelWeight === 0)) {
        maxIntelScore = maxIntelScore * (ratios.intelWeight / 100);
        surveyScore = surveyScore * (ratios.surveyWeight / 100);
    } else if (shouldCalcSurvey) {
        maxIntelScore = maxIntelScore * 0.5;
        surveyScore = surveyScore * 0.5;
    }

    return Math.floor(maxIntelScore + surveyScore);
};

module.exports.calculateTotalScore = calculateTotalScore;

module.exports.getSurveyImportantQuestions = (survey) => {
    const importantQuests = [];

    if (survey && survey.pages && Array.isArray(survey.pages)) {
        survey.pages.map((currPage) => {
            if (currPage && currPage.elements && Array.isArray(currPage.elements)) {
                currPage.elements.map((currCategory) => {
                    if (currCategory && currCategory.elements && Array.isArray(currCategory.elements)) {
                        currCategory.elements.map((currQuest) => {
                            if (currQuest && currQuest.markedAsImportant) {
                                importantQuests.push(currQuest);
                            }
                        });
                    }
                });
            }
        });
    }

    return (importantQuests);
};

const switchScoreBackwards = (score) => {
    return 100 - score;
};

const identifyRiskLevel = (totalScore) => {
    let risk = {level: 'Low', color: '#0091ea'};
    if (totalScore !== null) {
        if (totalScore < 33) {
            risk = {level: 'Low', color: '#0091ea'};
        } else if (totalScore >= 33 && totalScore < 66) {
            risk = {level: 'Medium', color: '#FFA000'};
        } else if (totalScore >= 66 && totalScore <= 100) {
            risk = {level: 'High', color: '#D92E2E'};
        }
    }
    return risk;
};

module.exports.switchScoreBackwards = switchScoreBackwards;
module.exports.identifyRiskLevel = identifyRiskLevel;

module.exports.PDF_TYPE = PDF_TYPE;

module.exports.WORD_TYPE = WORD_TYPE;

module.exports.EXCEL_TYPE = EXCEL_TYPE;

module.exports.saveBufferToDB = (buffer, buffer_type, creatorID, context, reportKind) => {
    return new Promise((resolve) => {
        if (buffer && Buffer.isBuffer(buffer) && buffer_type && BUFFER_TYPES.includes(buffer_type) && creatorID) {
            const reportType = (buffer_type === PDF_TYPE) ? 'PDF' : (buffer_type === WORD_TYPE) ? 'Word' : 'Excel';
            console.log('Starting to save ', reportType, ' report Buffer to DB');
            const app_entity = context.make('reports', 'report', 'map');

            app_entity.data$({
                rid: shortid.generate(),
                creator_uid: creatorID,
                create_date: moment().format(),
                report_type: reportKind,
                buffer_type: buffer_type,
                buffer: buffer
            });

            const save$ = PromiseB.promisify(app_entity.save$, {context: app_entity});
            return save$()
                .then((app) => {
                    if (app && app.rid) {
                        console.log('New ', reportType, ' report buffer was successfully created with id: ' + app.rid);
                        resolve({ok: true, data: app.rid, reportKind: reportKind});
                    } else {
                        console.log('Error in saveBufferToDB - buffer returned: ', app);
                        resolve({ok: false});
                    }
                })
                .catch((err) => {
                    console.log('Error in saveBufferToDB: ', err);
                    resolve({ok: false});
                });
        } else {
            console.log('Wrong parameters inserted to saveBufferToDB');
            resolve({ok: false});
        }
    });
};

module.exports.saveStreamToDB = (stream, stream_type, creatorID, context, reportKind) => {
    return new Promise((resolve) => {
        if (stream && stream_type && creatorID) {
            const reportType = (stream_type === PDF_TYPE) ? 'PDF' : (stream_type === WORD_TYPE) ? 'Word' : 'Excel';
            console.log('Starting to save ', reportType, ' report Buffer to DB');
            MongoClient.connect(mongoUrl, async (err, client) => {
                let dataObj = {
                    rid: shortid.generate(),
                    creator_uid: creatorID,
                    create_date: moment().format(),
                    report_type: reportKind,
                    stream_type: stream_type
                };

                const db = client.db('darkvision');
                const bucket = new mongo.GridFSBucket(db);
                stream.pipe(bucket.openUploadStream(dataObj.rid, {metadata: dataObj}))
                    .on('error', (error) => {
                        console.error('Error: ', error);
                        resolve({ok: false});
                        client.close();
                    })
                    .on('finish', () => {
                        console.log('done saving stream to fb');
                        console.log('New ', reportType, ' report stream was successfully created with id: ' + dataObj.rid);
                        resolve({ok: true, data: dataObj.rid, reportKind: reportKind});
                        client.close();
                    });

            });
        } else {
            console.log('Wrong parameters inserted to saveBufferToDB');
            resolve({ok: false});
        }
    });
};
module.exports.KeyFindingsCreation = (scores, displayScoreBackwards, isWord, portsByIPCount, notConfiguredSPFs, notConfiguredDMARCs, sslCertsCount, allCVEsCount, blacklistsCount, botnetsCount, notConfiguredBuckets) => {
    let risksSummaryHeader = '<h3 style="margin-top: 10px;">Key Findings</h3>';
    let risksSummaryText = '';

    if (portsByIPCount && portsByIPCount[0] > 0) {
        risksSummaryText += '<li>Nonstandard open ports - exposed databases, remote access and other services.\n</li>';
    }
    if ((notConfiguredSPFs && notConfiguredSPFs > 0) || (notConfiguredDMARCs && notConfiguredDMARCs > 0)) {
        risksSummaryText += '<li>Misconfigured DNS records - misconfiguration that may leave the company susceptible to impersonation attacks.\n</li>';
    }
    if (allCVEsCount && allCVEsCount > 0) {
        risksSummaryText += '<li>Systems running old versions - Systems that haven\'t been maintained, may expose the company to hacking and exploitation.\n</li>';
    }
    if (sslCertsCount && sslCertsCount > 0) {
        risksSummaryText += '<li>Websites without SSL certificates (or with weak ones) - indicate communication is not encrypted and may lead to eavesdropping.\n</li>';
    }
    if (blacklistsCount && blacklistsCount > 0) {
        risksSummaryText += '<li>Blacklisted IP addresses - IP addresses that are black listed indicate they have been used to distribute spam and other unwanted communications.\n</li>';
    }
    if (botnetsCount && botnetsCount > 0) {
        risksSummaryText += '<li>Malware and Botnets - IP addresses that are hosting malware or botnets (compromised).\n</li>';
    }
    if (notConfiguredBuckets && notConfiguredBuckets > 0) {
        risksSummaryText += '<li>Misconfigured Cloud services (S3) - Publicly accessible information stores on the cloud.\n</li>';
    }

    if (risksSummaryText === '') {
        return '';
    }
    return (
        risksSummaryHeader +
        '<p2>List of risks discovered:\n </p2>' +
        '<ul style="margin-left: 40px; font-size: 13px"> ' +
        risksSummaryText +
        ' </ul>'
    );
};

module.exports.createRisksByCategory = (scores, resultCounts, displayScoreBackwards, isWord) => {
    const allScores = scores[0].scoresData;

    let text = '<p2><myCenter>';
    let count = 0;
    let scoreArr = [];

    let cveInfo1 = 'Critical CVE\'s: ' + allScores.countCVEs.scoreData.criticalCVSSCount || 0;
    let cveInfo2 = 'Medium risk CVE\'s: ' + allScores.countCVEs.scoreData.mediumCVSSCount || 0;
    this.creatScoresArr('CVE', allScores.countCVEs.score || 0, cveInfo1, cveInfo2, displayScoreBackwards, scoreArr);

    let dnsInfo1 = 'SPF\'s not configured: ' + allScores.countDNS.scoreData.countNotConfiguredSPFs || 0;
    let dnsInfo2 = 'DMARC\'s not configured: ' + allScores.countDNS.scoreData.countNotConfiguredDMARCs || 0;
    this.creatScoresArr('DNS', allScores.countDNS.score || 0, dnsInfo1, dnsInfo2, displayScoreBackwards, scoreArr);

    let bucketsInfo1 = 'Public Buckets: ' + allScores.countBuckets.scoreData.publicBucketsCount || 0;
    let bucketsInfo2 = 'Total Buckets: ' + allScores.countBuckets.scoreData.totalBucketsCount || 0;
    this.creatScoresArr('S3 Buckets', allScores.countBuckets.score || 0, bucketsInfo1, bucketsInfo2, displayScoreBackwards, scoreArr);

    let breachesInfo1 = 'Emails in breaches: ' + allScores.countEmailBreaches.scoreData.breachedEmailsCount || 0;
    let breachesInfo2 = 'Total Emails: ' + allScores.countEmailBreaches.scoreData.totalEmailsCount || 0;
    this.creatScoresArr('Emails', allScores.countEmailBreaches.score || 0, breachesInfo1, breachesInfo2, displayScoreBackwards, scoreArr);

    let dataleaksInfo1 = 'Critical Dataleaks: ' + allScores.countDataleaks.scoreData.criticalDataleaks || 0;
    let dataleaksInfo2 = 'Medium risk Dataleaks: ' + allScores.countDataleaks.scoreData.mediumDataleaks || 0;
    this.creatScoresArr('Dataleaks', allScores.countDataleaks.score || 0, dataleaksInfo1, dataleaksInfo2, displayScoreBackwards, scoreArr);

    let blacklistInfo1 = 'IPs Blacklisted: ' + allScores.countBlacklists.scoreData.ip || 0;
    let blacklistInfo2 = 'MX Records Blacklisted: ' + allScores.countBlacklists.scoreData.mx || 0;
    this.creatScoresArr('Blacklists', allScores.countBlacklists.score || 0, blacklistInfo1, blacklistInfo2, displayScoreBackwards, scoreArr);

    let botnetsInfo1 = 'Infected Domains: ' + allScores.countBotnets.scoreData.infectedDomainsCount || 0;
    let botnetsInfo2 = 'Total Domains: ' + allScores.countBotnets.scoreData.totalDomainsCount || 0;
    this.creatScoresArr('Botnets', allScores.countBotnets.score || 0, botnetsInfo1, botnetsInfo2, displayScoreBackwards, scoreArr);

    let httpsInfo1 = 'Domains without valid SSL: ' + allScores.countSSLCerts.scoreData.notValidHttpsDomainsCount || 0;
    let httpsInfo2 = 'Domains with valid SSL: ' + allScores.countSSLCerts.scoreData.validHttpsDomainsCount || 0;
    this.creatScoresArr('https', allScores.countSSLCerts.score || 0, httpsInfo1, httpsInfo2, displayScoreBackwards, scoreArr);

    const pageBreak = '<p/><br style="page-break-before: always; clear: both" />';

    if(isWord){
        text = '<table style="text-align: left; background-color: white">';
        text += '<tr>';
        scoreArr.map((item) => {
            text += '<td style="width: 25%; border: 5px solid white; padding: 10px; background-color: lightgray">';
            text += '&nbsp; <span style="font-size: 16px; color:' + item.risk.color + '; font-weight: bold;">&nbsp;'+ item.score + '</span>' +
                '<span style="text-align: center;">&nbsp;-&nbsp;'+ item.name +' </span><br/><br/>';
            text += '<span style="font-size: 11px; color: #323232;">&nbsp; &nbsp;'+ item.info +' </span><br/>';
            text += '<span style="font-size: 11px; color: #323232;">&nbsp; &nbsp;'+ item.totalInfo +' </span><br/>';
            text += '</td>';
            count++;
            if(count === 4){text += '</tr><tr>';}
        });
        text += '</tr>';
        text += '</table>';
    } else {
        text += '<br/>';
        scoreArr.map((item) => {
            text += '<span style="text-align: left; display: inline-block; height: 70px; width: 185px; padding: 16px; border-radius: 5px; background-color: #f8f8f8; margin-right: 10px">' +
                '<span style="display: inline-block; padding: 6px 4px 3px 5px; border-radius: 100px; color: white; ' +
                'background-color: ' + item.risk.color + '; font-size: 14px;' +
                'height: 25px; width: 25px; margin: auto">';
            text += item.score.toString().length === 1 ? '<span style="position: relative; right: 2px; color:' + item.risk.color + ';">||||<span style="color: white;">' + item.score + ' </span></span> ' : '';
            text += item.score.toString().length === 2 ? '<span style="position: relative; right: -2px; color:' + item.risk.color + ';">|<span style="color: white;">' + item.score + ' </span></span> ' : '';
            text += item.score.toString().length === 3 ? item.score : '';
            text += ' </span><span style="margin-left: 8px">' + item.name + ' </span>' +
                '<span style="display: block; font-size: 11px; margin-top: 8px; margin-left: 8px">' + item.info + ' </span>' +
                '<span style="display: block; font-size: 11px; margin-left: 8px" >' + item.totalInfo + ' </span>' +
                '</span>';

            count++;
            if (count === 4) {
                text += '<br/><br/>';
            }
        });
    }

    text += '</myCenter></p2>';
    text += '<br/><br/><p2>The following score represents the Risk on a 0 - 100 scale,';
    if (displayScoreBackwards) {
        text += ' lower score means more risk, higher score means less risk.</p2><br/>';
    } else {
        text += ' lower score means less risk, higher score means more risk.</p2><br/>';
    }
    if(isWord){
        text += pageBreak;
    }
    return text;
};

module.exports.creatScoresArr = (name, score, info, totalInfo, displayScoreBackwards, scoreArr) => {
    let risk = this.identifyRiskLevel(score);
    if (displayScoreBackwards) {
        score = this.switchScoreBackwards(score);
    }
    scoreArr.push({name: name, score: score, risk: risk, info: info, totalInfo: totalInfo});
};
