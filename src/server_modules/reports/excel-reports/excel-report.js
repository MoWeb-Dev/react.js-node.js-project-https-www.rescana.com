const moment = require('moment');

const xl = require('excel4node');
const reportHelpers = require('../common.js');
const path = require('path');
const helpers = require('../common.js');
const pdfReport = require('../pdf-reports/pdf-report.js');
const { Readable } = require('stream');

module.exports.createExcel = (isInDemoMode, user, names, displayScoreBackwards, findingsObj, context, reportKind) => {
    return new Promise((resolve) => {

        if(isInDemoMode){
            findingsObj = pdfReport.updateAllReportDataToDemoModeData(findingsObj);
            names = 'Company Name';
        }

        let companies = findingsObj.companies;

        let wb = new xl.Workbook();
        const style = {
            categoryStyle: wb.createStyle({
                font: {
                    bold: true
                },
                alignment: {
                    wrapText: true,
                    horizontal: 'center'
                }
            }),
            generalStyle: wb.createStyle({
                alignment: {
                    wrapText: true,
                    horizontal: 'center',
                    shrinkToFit: true,
                    size: 80
                }
            }),
            headerStyle: wb.createStyle({
                alignment: {
                    wrapText: true,
                    horizontal: 'center',
                    shrinkToFit: true
                },
                font: {
                    color: 'white',
                    bold: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#1E88E5'
                }
            }),insidePageHeaderStyle: wb.createStyle({
                alignment: {
                    wrapText: true,
                    horizontal: 'center',
                    shrinkToFit: true
                },
                font: {
                    color: 'black',
                    bold: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#d0c9c7'
                }
            })
        };

        if (reportKind === 'intelReport') {

            let imgFilename = findingsObj.imgFilename,
                companiesWithScores = findingsObj.companiesWithScores,
                vulnsByIP = findingsObj.allVulnsByIP,
                allIPs = findingsObj.allIPs,
                allASNs = findingsObj.allASNs,
                allISPs = findingsObj.allISPs,
                allProducts = findingsObj.allProducts,
                allBuckets = findingsObj.allBuckets,
                allSPFs = findingsObj.allSPFs,
                allDMARCs = findingsObj.allDMARCs,
                allDataleaks = findingsObj.allDataleaks,
                allEmailBreaches = findingsObj.allEmailBreaches,
                allBlacklists = findingsObj.allBlacklists,
                allBotnets = findingsObj.allBotnets,
                allSSLCerts = findingsObj.allSSLCerts,
                allPorts = findingsObj.allPorts,
                allAmData = findingsObj.allAmData;

            let amAssets = pdfReport.createSeparatedAssetObjectsFromAmData(allAmData);
            let criticalImportanceAssets = amAssets.criticalImportanceAssets;
            let highImportanceAssets = amAssets.highImportanceAssets;
            let mitigatedAssets = amAssets.mitigatedAssets;
            let fpAssets = amAssets.fpAssets;


            wb = this.addScoresToExcel(companiesWithScores, displayScoreBackwards, wb, style);
            wb = this.addVulnsToExcel(vulnsByIP, companies, wb, style);
            wb = this.addIpsToExcel(allIPs, wb, style);
            wb = this.addPortsToExcel(allPorts, wb, style);
            wb = this.addProductsToExcel(allProducts, wb, style);
            wb = this.addTxtToExcel(allSPFs, 'spf', wb, style);
            wb = this.addTxtToExcel(allDMARCs, 'dmarc', wb, style);
            wb = this.addASNsToExcel(allASNs, wb, style);
            wb = this.addISPsToExcel(allISPs, wb, style);
            wb = this.addBucketsToExcel(allBuckets, wb, style);
            wb = this.addDataLeaksToExcel(allDataleaks, wb, style);
            wb = this.addEmailBreachesToExcel(allEmailBreaches, wb, style);
            wb = this.addBlacklistsToExcel(allBlacklists, wb, style);
            wb = this.addBotnetsToExcel(allBotnets, wb, style);
            wb = this.addSSLCertsToExcel(allSSLCerts, wb, style);
            wb = this.addMapToExcel(imgFilename, wb);

            if(criticalImportanceAssets && Object.getOwnPropertyNames(criticalImportanceAssets).length > 0){
                wb = this.addTableToExcel(criticalImportanceAssets, wb, style, 'Critical Importance Findings');
            }
            if(highImportanceAssets && Object.getOwnPropertyNames(highImportanceAssets).length > 0){
                wb = this.addTableToExcel(highImportanceAssets, wb, style, 'High Importance  Findings');
            }
            if(mitigatedAssets && Object.getOwnPropertyNames(mitigatedAssets).length > 0){
                wb = this.addTableToExcel(mitigatedAssets, wb, style, 'Mitigated Findings');
            }
            if(fpAssets && Object.getOwnPropertyNames(fpAssets).length > 0){
                wb = this.addTableToExcel(fpAssets, wb, style, 'False Positives');
            }

        } else if (reportKind === 'surveyReport') {
            wb = this.addFrontPage(companies, names, wb, style);
            wb = this.addSurveysInfoToExcel(companies, wb, style);
            wb = this.addNoGoQuestionsToExcel(companies, wb, style);
        }

       /* const pdf_create = pdf.create(htmlObj, options);
        const toStream = PromiseB.promisify(pdf_create.toStream, {context: pdf_create});

        try {
            const stream = await toStream();
            const res = await helpers.saveStreamToDB(stream, helpers.PDF_TYPE, user, context, reportKind);
            resolve(res);
        } catch(e){
            console.error("Error in createPDF: ", e);
            resolve({ok: false});
        }*/
        return wb.writeToBuffer()
            .then((buffer) => {
                let stream = bufferToStream(buffer);
                return reportHelpers.saveStreamToDB(stream, reportHelpers.EXCEL_TYPE, user, context, reportKind);
            })
            .catch((e) => {
                console.error('Error in createExcel-saveStreamToDB: ', e);
                return resolve({ok: false});
            })
            .then((res) => {
                return resolve(res);
            })
            .catch((e) => {
                console.error('Error in createExcel-saveStreamToDB: ', e);
                return resolve({ok: false});
            });
    });

};
module.exports.addFrontPage = (companies, names, wb, style) => {
    if (companies[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Survey Reports');
        ws.column(1).setWidth(100);

        const today = moment().format('LL');
        const logoSrc = helpers.getFilePathInitialForExcel() + '/resources/rescana_logo.png';

        ws.addImage({
            path: logoSrc,
            type: 'picture',
            position: {
                type: 'oneCellAnchor',
                from: {
                    col: 1,
                    colOff: '1.4in',
                    row: 1,
                    rowOff: 0,
                },
            },
        });

        ws.cell(15, 1).string('Surveys Status Reports: ' + names[0]).style(style.generalStyle);
        ws.cell(16, 1).string('Report Date: ' + today).style(style.generalStyle);
        ws.cell(19, 1).string('This report provides analysis, status, and visualization of the company vendors.' +
            ' It can be used to keep track and manage the surveys in accordance with the company\'s security needs.').style(style.generalStyle);

    }
    return wb;
};

//print in a single page with multiple tables(one after the other), each company has her own table with her no go questions.
module.exports.addNoGoQuestionsToExcel = (companies, wb, style, isMultCompaniesReport = false, isRtl = false, opts) => {
    if(companies && Array.isArray(companies) && companies.length > 0 && style){
        const ws = wb.addWorksheet(isRtl? 'שאלות קריטיות': 'CRITICAL QUESTIONS',opts);
        let row = 2;
        companies.map((curComp)=>{
           if(curComp && curComp.isUnsafeCompany && curComp.noGoAnsweredWrongArr &&
               Array.isArray(curComp.noGoAnsweredWrongArr) && curComp.noGoAnsweredWrongArr.length > 0){

               let columnHeaders = [];
               if (isRtl) {
                   if (isMultCompaniesReport) {
                       columnHeaders = ['שם הספק', 'שם הסקר'];
                   }
                   columnHeaders = columnHeaders.concat(['שאלות קריטיות', 'התשובה נכונה', 'תשובת הספק', 'תגובת הספק']);
               } else {
                   if (isMultCompaniesReport) {
                       columnHeaders = ['Company Name', 'Survey Name'];
                   }
                   columnHeaders = columnHeaders.concat(['Critical Questions', 'Correct Answer', 'Vendor Answer', 'Vendor Comment']);
               }


               for (let i = 0; i < columnHeaders.length; i++) {
                   ws.cell(row , 1 + i).string(columnHeaders[i]).style(style.headerStyle);
               }
               row++;
               curComp.noGoAnsweredWrongArr.map((curAnswer)=>{
                   let column = 1;
                   if(curAnswer.questionText && curAnswer.correctAnswer && curAnswer.vendorAnswer){

                       if(isMultCompaniesReport && curComp.companyName && curComp.surveyName ){
                           ws.cell(row, column++).string(curComp.companyName).style(style.generalStyle);
                           ws.cell(row, column++).string(curComp.surveyName).style(style.generalStyle);
                       }
                       ws.cell(row, column++).string(curAnswer.questionText).style(style.generalStyle);
                       ws.cell(row, column++).string(curAnswer.correctAnswer).style(style.generalStyle);
                       ws.cell(row, column++).string(curAnswer.vendorAnswer).style(style.generalStyle);
                       ws.cell(row, column).string(curAnswer.vendorComment || "-").style(style.generalStyle);
                       row++;
                   }
               });
               row++;
               row++;
           }
        });
    }
    return wb;
};

module.exports.addSurveysInfoToExcel = (companies, wb, style) => {
    if (companies[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Vendors');
        ws.column(1).setWidth(30);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(30);
        ws.column(4).setWidth(25);
        ws.column(5).setWidth(25);
        ws.column(6).setWidth(25);
        ws.column(7).setWidth(25);
        ws.column(8).setWidth(25);

        const columnHeaders = ['Company', 'Survey Name', 'Contact', 'Sensitivity', 'Last Modified', 'Completed', 'Survey Score'];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i]).style(style.headerStyle);
        }
        const scoreObjArr = [];
        for (let i = 0; i < companies.length; i++) {
            const scoreObj = {};
            for (const item in companies[i]) {
                if (companies[i].hasOwnProperty(item)) {

                    if (item === 'companyName' || item === 'contact' || item === 'surveyName') {
                        scoreObj[item] = companies[i][item] !== '' ? companies[i][item] : 'N/A';
                    } else if (item === 'surveyCompletion') {
                        scoreObj[item] = companies[i][item] !== 0 ? Math.floor(companies[i][item]) + '%' : '0%';
                    } else if (item === 'sensitivity') {

                        if (companies[i][item] === 1) {
                            scoreObj[item] = 'Low Sensitivity';
                        } else if (companies[i][item] === 2) {
                            scoreObj[item] = 'Moderate Sensitivity';
                        } else if (companies[i][item] === 3) {
                            scoreObj[item] = 'High Sensitivity';
                        } else if (companies[i][item] === 3) {
                            scoreObj[item] = 'Very High Sensitivity';
                        } else {
                            scoreObj[item] = 'N/A';
                        }

                    } else if (item === 'surveyScore') {
                        scoreObj[item] = companies[i][item] === -1 ? 'N/A' : Math.floor(companies[i][item] * 100).toString();
                    } else if (item === 'surveyLastUpdate') {
                        if (companies[i][item] !== '') {
                            scoreObj[item] = moment(companies[i][item]).calendar(null, {
                                sameDay: 'YYYY/MM/DD, HH:mm',
                                nextDay: 'YYYY/MM/DD, HH:mm',
                                nextWeek: 'YYYY/MM/DD, HH:mm',
                                lastDay: 'YYYY/MM/DD, HH:mm',
                                lastWeek: 'YYYY/MM/DD, HH:mm',
                                sameElse: 'YYYY/MM/DD, HH:mm'
                            });
                        } else {
                            scoreObj[item] = 'N/A';
                        }
                    }
                }
            }
            scoreObjArr.push(scoreObj);
        }
        let scoreObjArrSorted = scoreObjArr
            .sort((a, b) => {
                let textA = a.companyName;
                let textB = b.companyName;
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });

        for (let i = 0, column = 1; i < scoreObjArrSorted.length, column < scoreObjArrSorted.length + 1; i++, column++) {
            let vals = [];
            Object.keys(scoreObjArrSorted[i]).map((key) => {
                if (key === 'companyName') {
                    vals[0] = scoreObjArrSorted[i][key]
                } else if (key === 'surveyName') {
                    vals[1] = scoreObjArrSorted[i][key]
                } else if (key === 'contact') {
                    vals[2] = scoreObjArrSorted[i][key]
                } else if (key === 'sensitivity') {
                    vals[3] = scoreObjArrSorted[i][key]
                } else if (key === 'surveyLastUpdate') {
                    vals[4] = scoreObjArrSorted[i][key]
                } else if (key === 'surveyCompletion') {
                    vals[5] = scoreObjArrSorted[i][key]
                } else if (key === 'surveyScore') {
                    vals[6] = scoreObjArrSorted[i][key]
                }
            });

            for (let k = 0; k < vals.length; k++) {
                if (typeof vals[k] === 'string') {
                    ws.cell(column + 1, k + 1).string(vals[k]).style(style.generalStyle);
                }
            }
        }
        console.log('Done building Surveys Info spreadsheet');
    }
    return wb;
};

module.exports.addVulnsToExcel = (cveVulns, companies, wb, style) => {
    if (cveVulns[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Vulnerabilities');
        ws.column(1).setWidth(20);
        ws.column(2).setWidth(15);
        ws.column(3).setWidth(8);
        ws.column(4).setWidth(20);
        ws.column(5).setWidth(15);
        ws.column(6).setWidth(10);
        ws.column(7).setWidth(70);
        ws.column(8).setWidth(50);

        const columnHeaders = Object.keys(cveVulns[0]);
        for (let i = 0; i < columnHeaders.length; i++) {
            if (columnHeaders[i] === 'cveScore') {
                columnHeaders[i] = 'CVE SCORE';
            }

            if (columnHeaders[i] === 'cveSummary') {
                columnHeaders[i] = 'CVE SUMMARY';
            }

            if (columnHeaders[i] === 'relatedCompanies') {
                columnHeaders[i] = 'RELATED COMPANIES';
            }

            ws.cell(1, i + 1).string(columnHeaders[i].toUpperCase()).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < cveVulns.length, column < cveVulns.length + 1; i++, column++) {
            if (cveVulns[i]) {
                const vals = Object.keys(cveVulns[i]).map((key) => {
                    return cveVulns[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (Array.isArray(vals[j])) {
                        vals[j] = vals[j].join();
                    }

                    if (typeof vals[j] === 'string') {
                        if (j === 3) {
                            const cpeArr = vals[j].split(':');
                            const product = cpeArr[cpeArr.length - 3] + ' ' + cpeArr[cpeArr.length - 2] + ' ' + cpeArr[cpeArr.length - 1];
                            ws.cell(column + 1, j + 1).string(product).style(style.generalStyle);
                        } else if (j === 7) {
                            const companiesStr = reportHelpers.turnIdsToCompanies(vals[j], companies);
                            ws.cell(column + 1, j + 1).string(companiesStr).style(style.generalStyle);
                        } else if (j === 4) { // If it's the CVE column
                            ws.cell(column + 1, j + 1).style(style.generalStyle).link('https://nvd.nist.gov/vuln/detail/' + vals[j], vals[j]);
                        } else {
                            ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                        }
                    } else if (typeof vals[j] === 'number') {
                        ws.cell(column + 1, j + 1).number(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }

        console.log('Done building Risks spreadsheet');
    }
    return wb;
};

module.exports.addIpsToExcel = (AllIps, wb, style) => {
    if (AllIps[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('IP Mapping');
        ws.column(1).setWidth(20);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(20);
        ws.column(4).setWidth(20);
        ws.column(5).setWidth(20);

        const columnHeaders = ['Domain', 'Subdomain', 'IP', 'ASN'];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i]).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < AllIps.length, column < AllIps.length + 1; i++, column++) {
            if (AllIps[i]) {
                const vals = Object.keys(AllIps[i]).map((key) => {
                    return AllIps[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (Array.isArray(vals[j])) {
                        vals[j] = vals[j].join();
                    }

                    if (typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }

        console.log('Done building IP Mapping spreadsheet');
    }
    return wb;
};

module.exports.addTableToExcel = (allAmData, wb, style, title) => {
    const ws = wb.addWorksheet(title);
    let row = 3;
    let addNum = 1;

    for (let i = 0; i < 10; i++) {
        ws.column(i + 1).setWidth(25);
    }
    Object.keys(allAmData).map((currKey) => {
        if(allAmData[currKey] && Array.isArray(allAmData[currKey]) && allAmData[currKey].length > 0){
            let curFMData = allAmData[currKey];
            let title = pdfReport.getTitleForGenericTables(currKey);
            let headers = pdfReport.getHeadersForGenericTables(curFMData);

            ws.cell(row, 1).string(title).style(style.insidePageHeaderStyle);
            row++;
            for(let i = 0; i < headers.length ; i++){
                ws.cell(row, i + 1).string(headers[i]).style(style.headerStyle);
            }
            row++;

            for (let k=0; k < curFMData.length; k++) {
                if (curFMData[k]) {
                    const vals = Object.keys(curFMData[k]).map((key) => {
                        if(key !== 'asn' && key !== 'asn_ip'){
                            return curFMData[k][key];
                        }
                    });
                    let flag =false;
                    for (let j = 0; j < vals.length; j++) {
                        if (Array.isArray(vals[j])) {
                            vals[j] = vals[j].join();
                        }

                        if (typeof vals[j] === 'string' && vals[j] !== undefined) {
                            ws.cell(row , j + addNum).string(vals[j]).style(style.generalStyle);
                        } else if(vals[j] === null && headers.includes('Summary')){
                            ws.cell(row , j + addNum).string('-').style(style.generalStyle);
                        }else{
                            flag =true;
                            addNum--;
                        }
                    }
                    if(flag){
                        addNum = 1;
                    }
                    row++;
                }
            }
            row += 3;
        }
    });

    return wb;

};

module.exports.addPortsToExcel = (allPorts, wb, style) => {
    if (allPorts[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Ports');
        ws.column(1).setWidth(20);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(20);
        ws.column(4).setWidth(20);

        const columnHeaders = ['Domain', 'Subdomain', 'IP', 'Port'];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i]).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < allPorts.length, column < allPorts.length + 1; i++, column++) {
            if (allPorts[i]) {
                const vals = Object.keys(allPorts[i]).map((key) => {
                    return allPorts[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (Array.isArray(vals[j])) {
                        vals[j] = vals[j].join();
                    }

                    if (typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }
        console.log('Done building Ports spreadsheet');
    }
    return wb;
};

module.exports.addScoresToExcel = (allScores, displayScoreBackwards, wb, style) => {
    if (allScores[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Risk Scores');
        ws.column(1).setWidth(20);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(20);
        ws.column(4).setWidth(20);

        const columnHeaders = ['Company Name', 'Intel Risk Score', 'Survey Risk Score', 'Total Company Score'];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i]).style(style.headerStyle);
        }

        const scoreObjArr = [];
        for (let i = 0; i < allScores.length; i++) {
            const scoreObj = {};
            for (const item in allScores[i]) {
                if (allScores[i].hasOwnProperty(item)) {
                    if (item === 'companyName') {
                        scoreObj[item] = allScores[i][item];
                    }

                    if (item === 'maxIntelScore') {
                        scoreObj[item] = allScores[i][item];

                        if (displayScoreBackwards) {
                            scoreObj[item] = reportHelpers.switchScoreBackwards(scoreObj[item]);
                        }
                    }

                    if (item === 'surveyScore') {
                        if (allScores[i].isSurveyNA || !allScores[i][item]) {
                            scoreObj[item] = 'N/A';

                            allScores[i][item] = 0;
                        } else {
                            scoreObj[item] = allScores[i][item];

                            if (displayScoreBackwards) {
                                scoreObj[item] = reportHelpers.switchScoreBackwards(scoreObj[item]);
                            }
                        }
                    }
                }
            }

            if (!scoreObj.hasOwnProperty('maxIntelScore')) {
                scoreObj['maxIntelScore'] = 0;
                if (displayScoreBackwards) {
                    scoreObj['maxIntelScore'] = reportHelpers.switchScoreBackwards(scoreObj['maxIntelScore']);
                }
                allScores[i]['maxIntelScore'] = 0;
            }
            if (!scoreObj.hasOwnProperty('surveyScore')) {
                scoreObj['surveyScore'] = 'N/A';
                allScores[i]['surveyScore'] = 0;
            }

            scoreObj.totalScore = reportHelpers.calculateTotalScore(allScores[i]['maxIntelScore'], allScores[i]['surveyScore'], allScores[i]['ratios'], allScores[i].isSurveyNA);

            if (displayScoreBackwards) {
                scoreObj.totalScore = reportHelpers.switchScoreBackwards(scoreObj.totalScore);
            }

            scoreObjArr.push(scoreObj);
        }

        for (let i = 0, column = 1; i < scoreObjArr.length, column < scoreObjArr.length + 1; i++, column++) {
            const vals = Object.keys(scoreObjArr[i]).map((key) => {
                return scoreObjArr[i][key];
            });

            for (let j = 0; j < vals.length; j++) {
                if (typeof vals[j] === 'string') {
                    ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                }

                if (typeof vals[j] === 'number') {
                    ws.cell(column + 1, j + 1).number(vals[j]).style(style.generalStyle);
                }
            }
        }

        console.log('Done building Risk Score spreadsheet');
    }
    return wb;
};

module.exports.addTxtToExcel = (data, WsName, wb, style) => {
    if (data[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet(WsName);
        ws.column(1).setWidth(20);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(30);

        const columnHeaders = ['Domain', 'Subdomain', WsName];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i].toUpperCase()).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < data.length, column < data.length + 1; i++, column++) {
            if (data[i]) {
                const vals = Object.keys(data[i]).map((key) => {
                    return data[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (Array.isArray(vals[j])) {
                        vals[j] = vals[j].join();
                    }

                    if (typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }

        console.log('Done building ' + WsName + ' spreadsheet');
    }
    return wb;
};

module.exports.addASNsToExcel = (allASNs, wb, style) => {
    if (allASNs[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('ASNs');
        ws.column(1).setWidth(30);
        ws.column(2).setWidth(35);
        ws.column(3).setWidth(15);
        ws.column(4).setWidth(20);
        ws.column(5).setWidth(15);
        ws.column(6).setWidth(20);

        const columnHeaders = ['ASN used by the organization', 'Description', 'Registrar', 'Range', 'Country Code', 'Related Domains'];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i]).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < allASNs.length, column < allASNs.length + 1; i++, column++) {
            if (allASNs[i] && allASNs[i].asn) {
                const currRow = column + 1;
                let currCell = 1;

                ws.cell(currRow, currCell++).string(allASNs[i].asn).style(style.generalStyle);

                const asnDescription = (allASNs[i].description) ? allASNs[i].description : '';

                ws.cell(currRow, currCell++).string(asnDescription).style(style.generalStyle);

                const asnRegistrar = (allASNs[i].registrar) ? allASNs[i].registrar : '';

                ws.cell(currRow, currCell++).string(asnRegistrar).style(style.generalStyle);

                const asnRange = (allASNs[i].range) ? allASNs[i].range : '';

                ws.cell(currRow, currCell++).string(asnRange).style(style.generalStyle);

                const asnCountryCode = (allASNs[i].countryCode) ? allASNs[i].countryCode : '';

                ws.cell(currRow, currCell++).string(asnCountryCode).style(style.generalStyle);

                const asnRelatedDomains = (allASNs[i].domains) ? allASNs[i].domains.join(', \n') : '';

                ws.cell(currRow, currCell++).string(asnRelatedDomains).style(style.generalStyle);
            }
        }
        console.log('Done building ASNs spreadsheet');
    }
    return wb;
};

module.exports.addProductsToExcel = (allProducts, wb, style) => {
    if (allProducts[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Discovered Systems');
        ws.column(1).setWidth(20);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(20);
        ws.column(4).setWidth(20);
        ws.column(5).setWidth(30);

        const columnHeaders = ['Domain', 'Subdomain', 'IP Address', 'Port', 'Product'];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i]).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < allProducts.length, column < allProducts.length + 1; i++, column++) {
            if (allProducts[i]) {
                const vals = Object.keys(allProducts[i]).map((key) => {
                    return allProducts[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }
        console.log('Done building products spreadsheet');
    }
    return wb;
};

module.exports.addISPsToExcel = (allISPs, wb, style) => {
    if (allISPs[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('ISPs');
        ws.column(1).setWidth(25);
        ws.column(2).setWidth(35);
        ws.column(3).setWidth(25);
        ws.column(4).setWidth(35);

        const columnHeaders = ['Domain', 'Subdomain', 'IP Address', 'ISP'];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i]).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < allISPs.length, column < allISPs.length + 1; i++, column++) {
            if (allISPs[i]) {
                const vals = Object.keys(allISPs[i]).map((key) => {
                    return allISPs[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }
        console.log('Done building isps spreadsheet');
    }
    return wb;
};

module.exports.addBucketsToExcel = (allBuckets, wb, style) => {
    if (allBuckets[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('S3 Buckets');
        ws.column(1).setWidth(20);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(30);
        ws.column(4).setWidth(30);

        const columnHeaders = ['Domain', 'Subdomain', 'Bucket', 'Status'];

        for (let i = 0; i < columnHeaders.length; i++) {
            ws.cell(1, i + 1).string(columnHeaders[i]).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < allBuckets.length, column < allBuckets.length + 1; i++, column++) {
            if (allBuckets[i]) {
                const vals = Object.keys(allBuckets[i]).map((key) => {
                    return allBuckets[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (Array.isArray(vals[j])) {
                        vals[j] = vals[j].join();
                    }

                    if (typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }
        console.log('Done building buckets spreadsheet');
    }
    return wb;
};

module.exports.addDataLeaksToExcel = (allDataleaks, wb, style) => {
    if (allDataleaks[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Data Leaks');
        ws.column(1).setWidth(25);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(30);
        ws.column(4).setWidth(30);
        ws.column(5).setWidth(30);
        ws.column(6).setWidth(30);

        const dataLeaks = allDataleaks;
        for (let i = 0; i < allDataleaks.length; i++) {
            delete dataLeaks[i]._uid;
            delete dataLeaks[i].lid;
            delete dataLeaks[i].hideFrom;
            delete dataLeaks[i]._id;
        }

        const columnHeaders = ['URL', 'Title', 'keyword', 'Date', 'Source', 'Related Companies'];

        for (let i = 0; i < columnHeaders.length; i++) {
            const header = columnHeaders[i];
            ws.cell(1, i + 1).string(header).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < dataLeaks.length, column < dataLeaks.length + 1; i++, column++) {
            if (dataLeaks[i]) {
                const vals = Object.keys(dataLeaks[i]).map((key) => {
                    return dataLeaks[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (Array.isArray(vals[j])) {
                        vals[j] = vals[j].join();
                    }
                    if (j === 0 && typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).style(style.generalStyle).link('http://' + vals[j], vals[j]);
                    } else if (typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }

        console.log('Done building dataleaks spreadsheet');
    }
    return wb;
};

module.exports.addEmailBreachesToExcel = (allEmailBreaches, wb, style) => {
    if (allEmailBreaches[0]) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Leaked Emails');
        ws.column(1).setWidth(25);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(20);
        ws.column(4).setWidth(30);
        ws.column(5).setWidth(30);

        const emailBreaches = allEmailBreaches;
        for (let i = 0; i < emailBreaches.length; i++) {
            delete emailBreaches[i].domain;
            delete emailBreaches[i].breach_nodeID;
        }

        const columnHeaders = ['Email', 'Appeared in Breach', 'Breach Date', 'Compromised Data', 'Related Companies'];

        for (let i = 0; i < columnHeaders.length; i++) {
            const header = columnHeaders[i];
            ws.cell(1, i + 1).string(header).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < emailBreaches.length, column < emailBreaches.length + 1; i++, column++) {
            if (emailBreaches[i]) {
                if(emailBreaches[i].id){
                    delete emailBreaches[i].id;
                }
                const vals = Object.keys(emailBreaches[i]).map((key) => {
                        return emailBreaches[i][key];
                });

                for (let j = 0; j < vals.length; j++) {
                    if (Array.isArray(vals[j])) {
                        vals[j] = vals[j].join();
                    }
                    if (typeof vals[j] === 'string') {
                        ws.cell(column + 1, j + 1).string(vals[j]).style(style.generalStyle);
                    }
                }
            }
        }

        console.log('Done building email breaches spreadsheet');
    }
    return wb;
};

module.exports.addBlacklistsToExcel = (allBlacklists, wb, style) => {
    if (allBlacklists && allBlacklists.length) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Blacklists');
        ws.column(1).setWidth(25);
        ws.column(2).setWidth(20);
        ws.column(3).setWidth(30);
        ws.column(4).setWidth(30);
        ws.column(5).setWidth(30);
        ws.column(6).setWidth(30);
        ws.column(7).setWidth(30);
        ws.column(8).setWidth(30);

        const columnHeaders = ['Domain', 'IP Address', 'Appeared in Domain Blacklists', 'Appeared in IP Blacklists', 'MX Records', 'Appeared in MX Blacklists', 'NS Records', 'Appeared in NS Blacklists'];

        for (let i = 0; i < columnHeaders.length; i++) {
            const header = columnHeaders[i];
            ws.cell(1, i + 1).string(header).style(style.headerStyle);
        }

        for (let i = 0, column = 1; i < allBlacklists.length, column < allBlacklists.length + 1; i++, column++) {
            if (allBlacklists[i] && allBlacklists[i].domain && allBlacklists[i].data && allBlacklists[i].data.blacklist_ip && allBlacklists[i].data.blacklist_ip.address) {
                const currRow = column + 1;
                let currCell = 1;

                ws.cell(currRow, currCell++).string(allBlacklists[i].domain).style(style.generalStyle);

                ws.cell(currRow, currCell++).string(allBlacklists[i].data.blacklist_ip.address).style(style.generalStyle);

                ws.cell(currRow, currCell++).string(stringifyFindingArray(allBlacklists[i].data, 'blacklist_domain')).style(style.generalStyle);

                ws.cell(currRow, currCell++).string(stringifyFindingArray(allBlacklists[i].data.blacklist_ip, 'blacklist')).style(style.generalStyle);

                ws.cell(currRow, currCell++).string(stringifyFindingArray(allBlacklists[i].data, 'mx')).style(style.generalStyle);

                ws.cell(currRow, currCell++).string(stringifyFindingArray(allBlacklists[i].data, 'blacklist_mx')).style(style.generalStyle);

                ws.cell(currRow, currCell++).string(stringifyFindingArray(allBlacklists[i].data, 'ns')).style(style.generalStyle);

                ws.cell(currRow, currCell++).string(stringifyFindingArray(allBlacklists[i].data, 'blacklist_ns')).style(style.generalStyle);
            }
        }

        console.log('Done building blacklists spreadsheet');
    }
    return wb;
};

module.exports.addBotnetsToExcel = (allBotnets, wb, style) => {
    if (allBotnets && allBotnets.length) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Botnets');
        const feedPropsToDisplayAtStart = ['domain', 'address'];
        const feedHeadersToDisplayAtStart = ['Domain', 'IP Address'];
        const feedPropsToDisplayAtEnd = ['relatedCompanies'];
        const feedHeadersToDisplayAtEnd = ['Related Companies'];
        const defaultColumnWidth = 25;
        let configuredColumns = feedHeadersToDisplayAtStart.length + feedHeadersToDisplayAtEnd.length;
        let currentRow = 1;
        for (let i = 1; i <= configuredColumns; i++) {
            // Assume there will be at least 3 feeds properties. Will be expanded during creation of tables.
            ws.column(i).setWidth(defaultColumnWidth);
        }

        allBotnets.map((currFinding) => {
            if (currFinding && currFinding.feedName && currFinding.feedData && Array.isArray(currFinding.feedData)) {
                let currentFeedFindingsKeys = [];
                let currentFeedHeaders = [];
                currFinding.feedData.map((currFeedData) => {
                    if (currFeedData && currFeedData.domain && currFeedData.address && currFeedData.relatedCompanies && Array.isArray(currFeedData.relatedCompanies)) {
                        if (currentFeedHeaders.length === 0) {
                            currentFeedFindingsKeys = currentFeedFindingsKeys.concat(feedPropsToDisplayAtStart);
                            currentFeedHeaders = currentFeedHeaders.concat(feedHeadersToDisplayAtStart);
                            Object.keys(currFeedData).map((currKey) => {
                                if (currKey && !feedPropsToDisplayAtStart.includes(currKey) && !feedPropsToDisplayAtEnd.includes(currKey)) {
                                    currentFeedFindingsKeys.push(currKey);
                                    currentFeedHeaders.push(currKey);
                                }
                            });
                            currentFeedFindingsKeys = currentFeedFindingsKeys.concat(feedPropsToDisplayAtEnd);
                            currentFeedHeaders = currentFeedHeaders.concat(feedHeadersToDisplayAtEnd);

                            // Start displaying the headers.
                            ws.cell(currentRow++, 1).string(currFinding.feedName).style(style.headerStyle);
                            currentFeedHeaders.map((currHeader, index) => {
                                if (index + 1 > configuredColumns) {
                                    // Expand configured columns width as long as necessary.
                                    ws.column(index + 1).setWidth(defaultColumnWidth);
                                    configuredColumns = index + 1;
                                }
                                ws.cell(currentRow, index + 1).string(currHeader).style(style.headerStyle);
                            });
                            currentRow++;
                        }

                        // Start displaying the data.
                        currentFeedFindingsKeys.map((currKey, index) => {
                            // Set the correct format of data.
                            const data = (currKey && currFeedData[currKey]) ? (Array.isArray(currFeedData[currKey])) ? currFeedData[currKey].join(', ') : currFeedData[currKey].toString() : '';
                            ws.cell(currentRow, index + 1).string(data).style(style.generalStyle);
                        });
                        currentRow++;
                    }
                });
                // Add spacing of one row between feeds.
                currentRow++;
            }
        });

        console.log('Done building botnets spreadsheet');
    }
    return wb;
};

module.exports.addSSLCertsToExcel = (allSSLCerts, wb, style) => {
    if (allSSLCerts && allSSLCerts.length) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('SSL Certificates');
        const sslCertsProps = [
            {label: 'Domain', key: 'domain'},
            {label: 'Subdomain', key: 'subdomain'},
            {label: 'Status', key: 'statusSSL'},
            {label: 'Is Valid', key: 'valid'},
            {label: 'Last Updated', key: 'lastUpdated'},
            {label: 'Valid From', key: 'valid_from'},
            {label: 'Valid To', key: 'valid_to'},
            {label: 'Days Remaining', key: 'days_remaining'},
            {label: 'Days Expired', key: 'days_expired'},
            {label: 'Subject', key: 'subject'},
            {label: 'Issuer', key: 'issuer'},
            {label: 'subjectAltName', key: 'subjectaltname'},
            {label: 'Info Access', key: 'infoAccess'},
            {label: 'Exponent', key: 'exponent'},
            {label: 'Fingerprint', key: 'fingerprint'},
            {label: 'Ext Key Usage', key: 'ext_key_usage'},
            {label: 'Serial Number', key: 'serialNumber'},
            {label: 'Related Companies', key: 'relatedCompanies'}
        ];
        const defaultColumnWidth = 25;
        let currentRow = 1;
        for (let i = 1; i <= sslCertsProps.length; i++) {
            ws.column(i).setWidth(defaultColumnWidth);
            // Start displaying the headers.
            ws.cell(currentRow, i).string(sslCertsProps[i - 1].label).style(style.headerStyle);
        }
        currentRow++;

        allSSLCerts.map((currFinding) => {
            if (currFinding) {
                // Fix main domain' subdomain property to None.
                currFinding.subdomain = 'None';

                // Start displaying the data.
                sslCertsProps.map((currProp, index) => {
                    const currPropData = currFinding[currProp.key];
                    // Set the correct format of data.
                    const data = (currPropData != null) ?
                        (Array.isArray(currPropData)) ? currPropData.join(', ') :
                            (typeof currPropData === 'object') ? JSON.stringify(currPropData) :
                                currPropData.toString() : '';
                    ws.cell(currentRow, index + 1).string(data).style(style.generalStyle);
                });
                currentRow++;

                // Do the same for the subdomains (if any are exist).
                if (currFinding.subdomains && Array.isArray(currFinding.subdomains)) {
                    currFinding.subdomains.map((currSubdomain) => {
                        if (currSubdomain) {
                            // Fix subdomain properties.
                            currSubdomain.subdomain = currSubdomain.domain;
                            currSubdomain.domain = currFinding.domain;
                            currSubdomain.relatedCompanies = currFinding.relatedCompanies;

                            // Start displaying the subdomain data.
                            sslCertsProps.map((currProp, index) => {
                                const currPropData = currSubdomain[currProp.key];
                                // Set the correct format of data.
                                const data = (currPropData != null) ?
                                    (Array.isArray(currPropData)) ? currPropData.join(', ') :
                                        (typeof currPropData === 'object') ? JSON.stringify(currPropData) :
                                            currPropData.toString() : '';
                                ws.cell(currentRow, index + 1).string(data).style(style.generalStyle);
                            });
                            currentRow++;
                        }
                    });
                }
            }
        });

        console.log('Done building SSL Certificates spreadsheet');
    }
    return wb;
};

const stringifyFindingArray = (data, findingProp) => {
    let result;
    const finding = data[findingProp];
    if (finding && Array.isArray(finding) && finding.length) {
        result = finding.join(', ');
    } else {
        result = 'None';
    }
    return result;
};

module.exports.addMapToExcel = (imgFilename, wb) => {
    if (imgFilename) {
        // Add Worksheets to the workbook
        const ws = wb.addWorksheet('Asset Map');

        ws.addImage({
            path: path.join(__dirname, '../resources/leaflet-image.png'),
            type: 'picture',
            position: {
                type: 'absoluteAnchor',
                x: '1in',
                y: '1in'
            }
        });

        console.log('Done building map spreadsheet');
    }
    return wb;
};

function bufferToStream(binary) {
    const readableInstanceStream = new Readable({
        read() {
            this.push(binary);
            this.push(null);
        }
    });

    return readableInstanceStream;

}
