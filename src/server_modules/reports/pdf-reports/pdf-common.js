const pdf = require('html-pdf');
const moment = require('moment');
const helpers = require('../common.js');
const config = require('app-config');
const PromiseB = require('bluebird');
const {getKeyNestedChildsIfExists} = require('../../../modules/common/CommonHelpers.js');

const TYPE_SURVEY_ANSWER_QUESTION = 'SA_Question';
const TYPE_SURVEY_ANSWER_NO_GO_QUESTION = 'NoGo_Question';
const TYPE_DATALEAKS = 'Dataleaks';
const TYPE_EMAIL_BREACHES = 'Email_Breaches';
const TYPE_BLACKLISTS = 'Blacklists';
const TYPE_BOTNETS = 'Botnets';
const TYPE_SSL_CERTS = 'sslCerts';
const TYPE_ASNS = 'ASNs';
const TYPE_SURVEY = 'Surveys';
const ASSETS_DISCOVERD = 'AssetsDiscovered';
const TYPE_GEO_LOCATIONS = 'GeoLocations';
const FP_MITIGATED_TYPE = 'fpAndMitigated';


module.exports.addFrontPage = (header, names, logo, isRTL = false) => {
    let text = '<div id="frontPage"';
    // In survey reports, the direction can be RTL if the survey language is hebrew.
    if (isRTL) {
        text += ' dir="rtl"';
    }
    text += '>';

    if (header && names) {
        let namesArray;
        if (!Array.isArray(names)) {
            namesArray = [names];
        } else {
            namesArray = names;
        }

        const today = moment().format('LL');

        if(logoObjValidation(logo)) {
            const orgLogo = helpers.getFilePathInitial() + '/../../../orgLogo' + logo.path;
            text += '<myCenter><logo><img style=" width: initial; height: 100px" src="' + orgLogo + '"></logo>';
        }
        text += '<myCenter><h1>' + header + '</h1>';
        text += '<h3>';
        for (let i = 0; i < namesArray.length; i++) {
            text += namesArray[i];
            if (i < namesArray.length - 1) {
                text += ', ';
                if ((i + 1) % 5 === 0) {
                    text += '<br/>';
                }
            }
        }
        text += '</h3>';
        const logoSrc = helpers.getFilePathInitial() + '/resources/rescana_logo_2.png';
        text += '<br/><img src="' + logoSrc + '">';
        text += '<br/><b>' + today + '</b>';
        text += '<br/><span style="position: relative; top: 450px">Company confidential </span></myCenter>';
    }
    text += '</div>';

    return text;
};

const logoObjValidation = (logo) =>{
    return !!(logo && typeof logo === 'object' && logo.hasOwnProperty('path') &&
        logo.path && logo.path !== "" && typeof logo.path === 'string')
};

module.exports.logoObjValidation = logoObjValidation;

module.exports.addRecordsForGenericTable = (allRecords, headers, type) => {
    let result = '';
    if (allRecords && allRecords.length > 0 && headers && headers.length > 1 && type) {
        result = '<table>';
        result += '<tr>';
        headers.map((currHeader) => {
            result += '<th style="background-color: lightgray">' + currHeader + '</th>';
        });
        result += '</tr>';
        allRecords.map((currRecord) => {
            result += '<tr>';
            Object.keys(currRecord).map((key) => {
                if(key && key === 'summary' && currRecord && currRecord[key] === null &&  type === 'cveAm'){
                    result += '<td> - </td>';
                } if(key && key === 'summary' && currRecord && currRecord[key] !== null &&  type === 'cveAm'){
                    result += '<td style="font-size: 8px">' + currRecord[key] + '</td>';
                } else if (currRecord && currRecord[key]) {
                    result += '<td>' + currRecord[key] + '</td>';
                }
            });
            result += '</tr>';
        });

        result += '</table>';
    }
    return result;
};
// type 'Other' is for SPF,DMARC and Buckets.
// categoryIndex has values only when type is TYPE_SURVEY_ANSWER_QUESTION
module.exports.addRecordsTable = (allRecords, headers, type = 'Other', categoryIndex = 0) => {
    let result = '';
    if (allRecords && allRecords.length > 0 && headers && headers.length > 1 && type) {
        result = '<table>';

        let countToLimit = 0;
        const RESULTS_LIMIT = 50;
        //Survey Report
        if (type === 'Surveys') {
            result += '<tr>';
            headers.map((currHeader) => {
                result += '<th style="background-color: lightgray">' + currHeader + '</th>';
            });
            result += '</tr>';

            allRecords.map((currRecord) => {
                result += '<tr>';
                if (type === TYPE_SURVEY && currRecord.companyName) {
                    result += currRecord.companyName ? '<td>' + currRecord.companyName + '</td>' : '<td>' + '-' + '</td>';
                    result += currRecord.surveyName && currRecord.surveyName !== '' ? '<td>' + currRecord.surveyName + '</td>' : '<td>' + '-' + '</td>';
                    result += currRecord.contact && currRecord.contact !== '' ? '<td>' + currRecord.contact + '</td>' : '<td>' + '-' + '</td>';

                    if (currRecord.sensitivity === 1) {
                        result += '<td>' + 'Low Sensitivity' + '</td>'
                    } else if (currRecord.sensitivity === 2) {
                        result += '<td>' + 'Moderate Sensitivity' + '</td>'
                    } else if (currRecord.sensitivity === 3) {
                        result += '<td>' + 'High Sensitivity' + '</td>'
                    } else if (currRecord.sensitivity === 4) {
                        result += '<td>' + 'Very High Sensitivity' + '</td>'
                    } else {
                        result += '<td>' + '-' + '</td>';
                    }

                    result += currRecord.lastUpdated ? '<td>' +
                        moment(currRecord.lastUpdated).calendar(null, {
                            sameDay: 'YYYY/MM/DD, HH:mm',
                            nextDay: 'YYYY/MM/DD, HH:mm',
                            nextWeek: 'YYYY/MM/DD, HH:mm',
                            lastDay: 'YYYY/MM/DD, HH:mm',
                            lastWeek: 'YYYY/MM/DD, HH:mm',
                            sameElse: 'YYYY/MM/DD, HH:mm'
                        })
                        + '</td>' : '<td>' + '-' + '</td>';

                    result += currRecord.surveyCompletion && currRecord.surveyCompletion !== 0 ? '<td>' + Math.floor(currRecord.surveyCompletion) + '%' + '</td>' : '<td>' + '0%' + '</td>';
                    result += currRecord.surveyScore && currRecord.surveyScore === -1 ? '<td>' + '-' + '</td>' : '<td>' + Math.floor(currRecord.surveyScore * 100) + '</td>';
                }
            });
        }
        //intel Report
        else {
            result += '<tr>';
            headers.map((currHeader) => {
                result += '<th style="background-color: lightgray">' + currHeader + '</th>';
            });
            result += '</tr>';

            allRecords.map((currRecord, currIndex) => {
                result += '<tr>';
                if (type === FP_MITIGATED_TYPE) {
                    Object.keys(currRecord).map((currKey) => {
                        if (currKey && currRecord[currKey]) {
                            result += '<td>' + currRecord[currKey] + '</td>';
                        }
                    });
                // This section is for Dataleaks table.
                }else if (type === TYPE_DATALEAKS && currRecord.source && currRecord.title && currRecord.url) {
                    result += '<td>' + currRecord.source + '</td>';
                    result += '<td>' + this.add3Dots(currRecord.title, 25) + '</td>';
                    result += '<td><a style="color: dodgerblue;" href="http://' + currRecord.url + '" target="_blank">' + this.add3Dots(currRecord.url, 25) + '</a></td>';
                }
                // This section is for EmailBreaches table.
                else if (type === TYPE_ASNS && currRecord.asn && currRecord.relatedDomains) {
                    result += '<td>' + currRecord.asn + '</td>';
                    result += '<td>';
                    if (currRecord.description) {
                        result += currRecord.description;
                    }
                    result += '</td>';
                    result += '<td>';
                    if (currRecord.registrar) {
                        result += currRecord.registrar;
                    }
                    result += '</td>';
                    result += '<td>';
                    if (currRecord.range) {
                        result += currRecord.range;
                    }
                    result += '</td>';
                    result += '<td>';
                    if (currRecord.countryCode) {
                        result += currRecord.countryCode;
                    }
                    result += '</td>';
                    result += '<td>';
                    if (currRecord.relatedDomains) {
                        result += currRecord.relatedDomains.join(', \n');
                    }
                    result += '</td>';
                }
                // This section is for EmailBreaches table.
                else if (type === TYPE_EMAIL_BREACHES && currRecord.email && currRecord.breach && currRecord.breach_date) {
                    // Limit EmailBreaches to show top 50 results.
                    if (countToLimit < RESULTS_LIMIT) {
                        countToLimit++;
                        result += '<td>' + this.add3Dots(currRecord.email, 35) + '</td>';
                        result += '<td>' + currRecord.breach + '</td>';
                        result += '<td>' + moment(currRecord.breach_date).format('YYYY/MM/DD') + '</td>';
                        let compromisedData;
                        if (currRecord.breach_classes) {
                            compromisedData = currRecord.breach_classes;
                        } else {
                            compromisedData = '';
                        }
                        result += '<td>' + compromisedData + '</td>';
                    }
                }
                // This section is for Blacklists table.
                else if (type === TYPE_BLACKLISTS && currRecord.domain && currRecord.data && currRecord.data.blacklist_ip && currRecord.data.blacklist_ip.address) {
                    // Limit Blacklists to show top 50 results.
                    if (countToLimit < RESULTS_LIMIT) {
                        countToLimit++;
                        result += '<td>' + this.add3Dots(currRecord.domain, 35) + '</td>';
                        result += '<td>' + currRecord.data.blacklist_ip.address + '</td>';
                        result += addBlacklistsCountAsColumn(currRecord.data, 'blacklist_domain');
                        result += addBlacklistsCountAsColumn(currRecord.data.blacklist_ip, 'blacklist');
                        result += addBlacklistsCountAsColumn(currRecord.data, 'blacklist_mx');
                        result += addBlacklistsCountAsColumn(currRecord.data, 'blacklist_ns');
                    }
                }
                // This section is for Botnets table.
                else if (type === TYPE_BOTNETS) {
                    // Firstly, display these props.
                    const firstPropsToDisplay = ['domain', 'address'];
                    firstPropsToDisplay.map((currProp) => {
                        result += addBotnetsPropertyAsColumn(currRecord, currProp);
                    });
                    // Then, display other properties on the current record.
                    Object.keys(currRecord).map((currKey) => {
                        if (currKey && !firstPropsToDisplay.includes(currKey)) {
                            result += addBotnetsPropertyAsColumn(currRecord, currKey);
                        }
                    });
                }
                // This section is for SurveyAnswer-Questions table.
                else if (type === TYPE_SURVEY_ANSWER_QUESTION && currRecord.name) {
                    if (categoryIndex) {
                        result += '<td>' + categoryIndex + '.' + (currIndex + 1) + '</td>';
                    }
                    result += '<td>' + currRecord.name + '</td>';
                    result += '<td>';
                    if (currRecord.answer) {
                        result += currRecord.answer;
                    }
                    result += '</td>';
                    result += '<td>';
                    if (currRecord.comment) {
                        result += currRecord.comment;
                    }
                    result += '</td>';
                    for (let i = 0; i < 3; i++) {
                        result += '<td>';
                        if (currRecord.files && currRecord.files[i] && currRecord.files[i].path) {
                            const path = config.addresses.website + '/evidence' + currRecord.files[i].path;
                            result += '<a style="color: dodgerblue;" href="' + path + '" target="_blank">Evidence</a>';
                        }
                        result += '</td>';
                    }
                } else if (type === TYPE_SURVEY_ANSWER_NO_GO_QUESTION) {
                    if (currRecord.questionText) {
                        result += '<td>' + currRecord.questionText + '</td>';
                    }
                    if (currRecord.correctAnswer) {
                        result += '<td>' + currRecord.correctAnswer + '</td>';
                    }
                    if (currRecord.vendorAnswer) {
                        result += '<td>' + currRecord.vendorAnswer + '</td>';
                    }
                    let comment = currRecord.vendorComment? currRecord.vendorComment : ' ';
                    result += '<td>' + comment + '</td>';
                } else if (type === ASSETS_DISCOVERD) {
                    let noResult = '<td>0</td>';

                    if(currRecord.ips && currRecord.ips > 0){
                        result += '<td>' + currRecord.ips.toString() + '</td>';
                    } else {result += noResult;}

                    if(currRecord.domainsAndSub && currRecord.domainsAndSub > 0){
                        result += '<td>' + currRecord.domainsAndSub.toString() + '</td>';
                    } else {result += noResult;}

                    if(currRecord.emails && currRecord.emails > 0){
                        result += '<td>' + currRecord.emails.toString() + '</td>';
                    } else {result += noResult;}

                    if(currRecord.asns && currRecord.asns > 0){
                        result += '<td>' + currRecord.asns.toString() + '</td>';
                    } else {result += noResult;}

                    if(currRecord.ssl && currRecord.ssl > 0){
                        result += '<td>' + currRecord.ssl.toString() + '</td>';
                    } else {result += noResult;}
                } else if (type === TYPE_GEO_LOCATIONS) {
                        if(currRecord.Assets && currRecord.Assets.low && currRecord.Country){
                            result += '<td>' + currRecord.Country.toString() + '</td>';
                            result += '<td>' + currRecord.Assets.low.toString() + '</td>';
                        }
                }
                    // This section is for SPF,DMARC and Buckets table.
                else {
                    if (currRecord.subdomain && currRecord.subdomain !== 'none') {
                        result += '<td>' + currRecord.subdomain + '</td>';
                    } else if (currRecord.hostname) {
                        result += '<td>' + currRecord.hostname + '</td>';
                    }

                    if (currRecord.bucket && currRecord.bucket_status) {
                        result += '<td>' + currRecord.bucket + '</td>';
                        result += '<td>' + currRecord.bucket_status + '</td>';
                    } else if (currRecord.dmarc) {
                        result += '<td';
                        if (currRecord.dmarc === 'No Records Found') {
                            result += ' class="warning"';
                        }
                        result += '>' + currRecord.dmarc + '</td>';
                    } else if (currRecord.spf) {
                        result += '<td';
                        if (currRecord.spf === 'No Records Found') {
                            result += ' class="warning"';
                        }
                        result += '>' + currRecord.spf + '</td>';
                    }
                }
                result += '</tr>';
            });
        }
        result += '</table>';

        /* Check if the none-empty results limit has reached and the results' length is bigger then the limit. */
        if (countToLimit === RESULTS_LIMIT && allRecords.length > RESULTS_LIMIT) {
            result += '<br/><p2><b>* Further results can be found in the Excel report.</b></p2>';
        }
    }
    return result;
};

module.exports.addRecordsTableByKeys = (allRecords, headers, dataKeys, type) => {
    let result = '';
    if (allRecords && allRecords.length > 0 && headers && headers.length > 1 && dataKeys && dataKeys.length > 1 && type) {
        result = '<table>';
        result += '<tr>';
        headers.map((currHeader) => {
            result += '<th style="background-color: #e0e0e0">' + currHeader + '</th>';
        });
        result += '</tr>';
        // Using countToLimit is optional.
        let countToLimit = 0;
        const RESULTS_LIMIT = 50;
        let totalRecords = allRecords.length;
        allRecords.map((currRecord) => {
            if (currRecord) {
                result += '<tr>';
                if (type === TYPE_SSL_CERTS) {
                    // Limit sslCerts to show top 50 results.
                    if (countToLimit < RESULTS_LIMIT) {
                        countToLimit++;
                        dataKeys.map((currKey) => {
                            result += '<td style="font-size: 11px;">';

                            result += getKeyNestedChildsIfExists(currRecord, currKey);

                            result += '</td>';
                        });

                        // Do the same for subdomains data if exists.
                        if (currRecord && currRecord.subdomains && Array.isArray(currRecord.subdomains)) {
                            currRecord.subdomains.map((currSubdomain) => {
                                if (currSubdomain) {
                                    // Limit sslCerts also in subdomains to show top 50 results.
                                    if (countToLimit < RESULTS_LIMIT) {
                                        countToLimit++;

                                        // Open new table-row for current subdomain.
                                        result += '</tr><tr>';

                                        dataKeys.map((currKey) => {
                                            result += '<td style="font-size: 11px;">';

                                            result += getKeyNestedChildsIfExists(currSubdomain, currKey);

                                            result += '</td>';
                                        });
                                    }
                                }
                            });

                            // Increment totalRecords also for subdomains.
                            totalRecords += currRecord.subdomains.length;
                        }
                    }
                }
                result += '</tr>';
            }
        });
        result += '</table>';

        /* Check if the none-empty results limit has reached and the results' length is bigger then the limit. */
        if (countToLimit === RESULTS_LIMIT && totalRecords > RESULTS_LIMIT) {
            result += '<br/><p2><b>* Further results can be found in the Excel report.</b></p2>';
        }
    }
    return result;
};

const addBlacklistsCountAsColumn = (data, blacklistProp) => {
    let result;
    if (data && blacklistProp && data[blacklistProp] && Array.isArray(data[blacklistProp]) && data[blacklistProp].length > 0) {
        result = '<td>' + data[blacklistProp].length + ' appearances</td>';
    } else {
        result = '<td>None</td>';
    }
    return result;
};

const addBotnetsPropertyAsColumn = (record, property) => {
    let result = '<td>';
    if (property && record && record[property]) {
        result += record[property];
    }
    result += '</td>';
    return result;
};

module.exports.createPDF = (user, htmlObj, context, reportKind) => {
    return new Promise(async (resolve) => {
        const today = moment().format('LL');

        // Default A4 is: 595 x 842 (but the display is bigger than desired)
        const options = {
            height: '1132px',
            width: '800px',
            //margin: '2cm',
            zoomFactor: '1.25',
            timeout: 100000,
            "header": {
                "height": "25mm",
            },
            "footer": {
                "height": "25mm",
                "contents": {
                    default: '<div style="font-family: Barlow, sans-serif; ' +
                        'font-size: 14px; text-align: start; margin: 30px 0 0 53px;">' + today + '</div>'
                }
            }
        };

        const pdf_create = pdf.create(htmlObj, options);
        const toStream = PromiseB.promisify(pdf_create.toStream, {context: pdf_create});

        try {
            const stream = await toStream();
            const res = await helpers.saveStreamToDB(stream, helpers.PDF_TYPE, user, context, reportKind);
            resolve(res);
        } catch(e){
            console.error("Error in createPDF: ", e);
            resolve({ok: false});
        }
    });
};

module.exports.add3Dots = (str, limit) => {
    const dots = '...';
    if (str.length > limit) {
        str = str.substring(0, limit) + dots;
    }

    return str;
};

module.exports.TYPE_SURVEY_ANSWER_QUESTION = TYPE_SURVEY_ANSWER_QUESTION;
module.exports.TYPE_SURVEY_ANSWER_NO_GO_QUESTION = TYPE_SURVEY_ANSWER_NO_GO_QUESTION;
