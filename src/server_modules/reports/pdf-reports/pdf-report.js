const pdfHelpers = require('./pdf-common.js');
const pdfStyle = require('./pdf-style.js');
const helpers = require('../common');
const wordReport = require('../word-reports/word-report.js');
const parseDomain = require('parse-domain');

const TYPE_BUCKET = 'S3 Bucket';
const TYPE_DMARC = 'DMARC';
const TYPE_SPF = 'SPF';
const TYPE_ASNS = 'ASNs';
const TYPE_DATALEAKS = 'Dataleaks';
const TYPE_EMAIL_BREACHES = 'Email_Breaches';
const TYPE_BLACKLISTS = 'Blacklists';
const TYPE_BOTNETS = 'Botnets';
const TYPE_SSL_CERTS = 'sslCerts';
const TYPE_SURVEY = 'Surveys';
const ASSETS_DISCOVERD = 'AssetsDiscovered';
const TYPE_GEO_LOCATIONS = 'GeoLocations';
const FP_MITIGATED_TYPE = 'fpAndMitigated';

module.exports.TYPE_BUCKET = TYPE_BUCKET;
module.exports.TYPE_DMARC = TYPE_DMARC;
module.exports.TYPE_SPF = TYPE_SPF;
module.exports.TYPE_ASNS = TYPE_ASNS;
module.exports.TYPE_DATALEAKS = TYPE_DATALEAKS;
module.exports.TYPE_EMAIL_BREACHES = TYPE_EMAIL_BREACHES;
module.exports.TYPE_BLACKLISTS = TYPE_BLACKLISTS;
module.exports.TYPE_BOTNETS = TYPE_BOTNETS;
module.exports.TYPE_SSL_CERTS = TYPE_SSL_CERTS;
module.exports.TYPE_SURVEY = TYPE_SURVEY;
module.exports.ASSETS_DISCOVERD = ASSETS_DISCOVERD;
module.exports.TYPE_GEO_LOCATIONS = TYPE_GEO_LOCATIONS;
module.exports.FP_MITIGATED_TYPE = FP_MITIGATED_TYPE;

module.exports.createScoreImg = (score, displayScoreBackwards, isWord) => {
    let scoreImg = '';
    let risk = helpers.identifyRiskLevel(score);
    if (displayScoreBackwards) {
        score = helpers.switchScoreBackwards(score);
    }
    if(isWord){
        scoreImg = '&nbsp; - &nbsp;&nbsp;<span style="color:' + risk.color + ';">' + score +' </span>';
    } else {
        scoreImg = '<span style="position:relative; bottom: 3px; margin-left: 10px; line-height:27px;' +
            ' display: inline-block; padding: 3px 3px 5px 4px; border-radius: 100px; color: white; ' +
            'background-color:' + risk.color + '; font-size: 14px; height: 25px; width: 25px;">'+ score +' </span>';
    }

    return scoreImg;
};

module.exports.createHTML = (isInDemoMode, user, names, displayScoreBackwards, findingsObj, manualData, reportKind, logo, isWord) => {
    let scoreImg = '';

    if(isInDemoMode){
        findingsObj = this.updateAllReportDataToDemoModeData(findingsObj);
        names = 'Company Name';
    }

    let companies = findingsObj.companies;
    companies.map((curComp) => {
        if(curComp.selectedDomains && Array.isArray(curComp.selectedDomains)){
            curComp.selectedDomains.push("no_domain");
        } else {
            curComp.selectedDomains = [];
            curComp.selectedDomains.push("no_domain");
        }
    });
    let companyName = isInDemoMode?  ' "Company Name" ' : companies[0].companyName;

    let text = ' ';
    if (reportKind === 'intelReport') {
        let locationsImg = findingsObj.imgFilename,
            scores = findingsObj.companiesWithScores,
            cveVulns = findingsObj.vulnsByIP,
            allASNs = findingsObj.allASNs,
            allBuckets = findingsObj.allBuckets,
            allSPFs = findingsObj.allSPFs,
            allDMARCs = findingsObj.allDMARCs,
            allDataleaks = findingsObj.allDataleaks,
            allEmailBreaches = findingsObj.allEmailBreaches,
            allGeoLocations = findingsObj.allGeoLocations,
            allBlacklists = findingsObj.allBlacklists,
            allBotnets = findingsObj.allBotnets,
            allSSLCerts = findingsObj.allSSLCerts,
            resultCounts = findingsObj.resultCounts,
            allAmData = findingsObj.allAmData;

        let amAssets = this.createSeparatedAssetObjectsFromAmData(allAmData || {});
        let criticalImportanceAssets = amAssets.criticalImportanceAssets;
        let highImportanceAssets = amAssets.highImportanceAssets;
        let mitigatedAssets = amAssets.mitigatedAssets;
        let fpAssets = amAssets.fpAssets;


        let allScoresData = scores[0].scoresData;



        text += '<!DOCTYPE html><html><head>';
        text += pdfStyle.getHtmlStyle() + pdfStyle.getHtmlCSS();
        text += '</head><body>';

        text += pdfHelpers.addFrontPage('Risk Intelligence Report Of', names, logo);

        text += '<div id="page" style="page-break-after: always;">';
        text += '    <myCenter>';
        text += '      <h2>EXECUTIVE SUMMARY</h2>';
        text += '    </myCenter>';
        text += this.addExecutiveSummary(resultCounts, scores, displayScoreBackwards, false, isInDemoMode);
        text += '    <br/>';
        text += '</div>';

        text += '<div id="page" style="page-break-after: always;">';
        text += '    <myCenter>';
        text += '      <h2>REPORT DETAILS</h2>';
        text += '    </myCenter>';
        text += this.addReportDetails(resultCounts, scores, locationsImg, allGeoLocations, isInDemoMode);
        text += '    <br/>';
        text += '</div>';

        if (cveVulns && cveVulns.length > 0 && companies && companies.length > 0) {
            if(cveVulns.length > 5){
                cveVulns = cveVulns.splice(0 ,5);
            }
            text += '<div id="page" style="page-break-after: always;">';
            scoreImg = this.createScoreImg(allScoresData.countCVEs.score, displayScoreBackwards);
            text += ' </span>';
            text += '<myCenter><h2>UNPATCHED SYSTEMS (CVE) ' + scoreImg + '</h2></myCenter>';
            text += '<p2>This analysis module is looking for systems that are running vulnerable software.\n' +
                ' Hackers look for vulnerable systems open from the Internet in their attempts to compromise \n' +
                'the systems. The systems maybe vulnerable for 2 primary reasons a) released patched were \n' +
                'not implemented or b) the systems are end of life versions and were not replaced by supported versions.' + '<br/>' +
                'CVE (<a style="color: dodgerblue;" href="https://cve.mitre.org/"> https://cve.mitre.org/</a>) ' +
                'is well known list of publicly knowns cybersecurity vulnerabilities.\n' +
                ' Once we identify a company asset we search the CVE list to identify if the software running \n' +
                'contains any known vulnerabilities. When found we present the vulnerability details. \n</p2>';

            // shouldStartNewPage will be true from second company with CVEs and on.
            let shouldStartNewPage = false;
            text += '<h3>CVE\'s Highlights</h3>';
            companies.map((currComp) => {
                if (currComp && currComp.hasCVEVulns && currComp.id && currComp.companyName) {
                    if (shouldStartNewPage) {
                        text += '</div><div id="page" style="page-break-after: always;">';
                    }
                    text += '<p2>Discovered on ' + currComp.companyName + '</p2><br/>';
                    cveVulns.map((currVuln) => {
                        if (currVuln && currVuln.relatedCompanies && currVuln.relatedCompanies.includes(currComp.id)) {
                            text += this.addCVECard(currVuln);
                        }
                    });
                    shouldStartNewPage = true;
                }
            });
            text += '<h3>Recommendations</h3>' +
                '<p2>Unpatched systems can be source of critical risk and should be considered seriously.\n </p2>';
            text += '</div>';
        }


        // Create tables for Buckets, SPFs and DMARCs.
        if (allBuckets && allBuckets.length > 0) {
            let publicBuckets =[];
            allBuckets.map((curBucket) => {
                if(curBucket && curBucket.bucket_status && curBucket.bucket_status.toLowerCase() === "public"){
                    publicBuckets.push(curBucket);
                }
            });
            if(publicBuckets.length > 0) {
                scoreImg = this.createScoreImg(allScoresData.countBuckets.score, displayScoreBackwards);
                text += '<div id="page" style="page-break-after: always;">';
                text += '<myCenter><h2>S3 BUCKETS ' + scoreImg + '</h2></myCenter>';
                text += '    <p2>Amazon Simple Storage Service (S3 buckets)' +
                    ' (<a style="color: dodgerblue;" href="https://docs.aws.amazon.com/AmazonS3/latest/dev/Welcome.html"> https://docs.aws.amazon.com/AmazonS3/latest/dev/Welcome.html</a>)' +
                    '  is storage service offered by Amazon on the cloud. This storage service became very popular storage' +
                    ' alternative. The risk involved with S3 buckets is mainly due to the case in which the storage is configured' +
                    ' to be open for public access leading in many cases to leakage of company confidential data. Our automatic scan' +
                    ' technology is continuously looking for publicly open S3 buckets with the goal to identify and alert our ' +
                    'customers as soon as possible. </p2>';
                text += this.addRecordsByCompany(publicBuckets, TYPE_BUCKET, companies);
                text += '<h3>Recommendations</h3>' +
                    '<p2>Any open S3 bucket needs to quickly be checked and most likely the access need to be properly configured.\n</p2>';
                text += '</div>';
            }
        }

        if ((allSPFs && allSPFs.length > 0) || (allDMARCs && allDMARCs.length > 0)) {
            text += '<div id="page" style="page-break-after: always;">';
            scoreImg = this.createScoreImg(allScoresData.countDNS.score, displayScoreBackwards);
            text += '<myCenter><h2>EMAIL SECURITY ' + scoreImg + '</h2></myCenter>';
            text += '<p2>Email authentication mechanisms address the challenge of unauthorized use of company email servers for email ' +
                'spoofing often used for phishing attacks. Rescana assess the usage of the two major authentication ' +
                'schemes SPF and DMARC used for email authentication. Domains without authentication records may allow attackers' +
                ' to abuse the domain names and use them for phishing or spamming purposes. </p2><br/>';
            let noRecordsFoundDMARC = [];
            let noRecordsFoundSPF = [];
            allDMARCs.map((item) => {
                if (item.dmarc && item.dmarc.toLowerCase() === "no records found") {
                    noRecordsFoundDMARC.push(item);
                }
            });
            allSPFs.map((item) => {
                if (item.spf && item.spf.toLowerCase() === "no records found") {
                    noRecordsFoundSPF.push(item);
                }
            });

            text += '<p2>';
            text += 'We have identified ' + noRecordsFoundDMARC.length + ' of total ' + scores[0].selectedDomains.length + ' domains with no DMARC authentication records';
            text += ' and ' + noRecordsFoundSPF.length + ' of ' + scores[0].selectedDomains.length + ' domains with no SPF authentication records.';
            text += '</p2><br/>';
            text += '<p2>';
            text += 'The following are the lists of domains with no appropriate DMARC and SPF records.\n';
            text += '</p2>';
            text += this.addRecordsByCompany(noRecordsFoundDMARC, TYPE_DMARC, companies);
            text += this.addRecordsByCompany(noRecordsFoundSPF, TYPE_SPF, companies);
            text += '<br/><h3>Recommendations</h3>' +
                '<p2>SPF and DMARC records must be setup for all your domains, (even those which are not actively being used).' +
                ' Please see the following guide -' +
                ' <a style="color: dodgerblue;" href=" https://www.esecurityplanet.com/applications/how-to-set-up-implement-dmarc-email-security.html">' +
                'https://www.esecurityplanet.com/applications/how-to-set-up-implement-dmarc-email-security.html</a></p2>';
            text += '</div>';
        }

        if (allEmailBreaches && allEmailBreaches.length > 0) {
            text += '<div id="page" style="page-break-after: always;">';
            scoreImg = this.createScoreImg(allScoresData.countEmailBreaches.score, displayScoreBackwards);
            text += '<myCenter><h2>EMAILS FOUND IN BREACHES ' + scoreImg + '</h2></myCenter>';
            text += '    <p2>The following is a list of emails that were found in various breaches. ' +
                'This could also give an indication about the security awareness status in the company.</p2>';
            text += this.addEmailBreachesByCompany(allEmailBreaches, companies);
            text += '<h3>Recommendations</h3>' +
                '<p2>It is recommended to alert the users and require them to change passwords on all company systems.</p2>';
            text += '</div>';
        }

        if (allDataleaks && allDataleaks.length > 0) {
            text += '<div id="page" style="page-break-after: always;">';
            scoreImg = this.createScoreImg(allScoresData.countDataleaks.score, displayScoreBackwards);
            text += '<myCenter><h2>DATALEAKS ' + scoreImg + '</h2></myCenter>';
            text += '<p2> This section searches for keywords which are related to the company that appear in paste ' +
                'sites such as <a style="color: dodgerblue;" href="https://en.wikipedia.org/wiki/Pastebin">Pastebin</a>. ' +
                'data found could include: code snippets, database dumps, ' +
                'email corresponding, hacking target lists and more. When a page with such data is found it is advised ' +
                'to start a takedown process.</p2>';
            text += this.addDataleaksByCompany(allDataleaks, companies);
            text += '</div>';
        }

        if (allBlacklists && allBlacklists.length > 0) {
            text += '<div id="page" style="page-break-after: always;">';
            scoreImg = this.createScoreImg(allScoresData.countBlacklists.score, displayScoreBackwards);
            text += '<myCenter><h2>BLACKLISTS ' + scoreImg + '</h2></myCenter>';
            text += '<p2>Multiple companies and agencies maintain publicly available black lists. There are 4 types of lists: domain,' +
                ' IP, MX and NS blacklists. These lists hold suspicious domains, IPs and email addresses that were found to' +
                ' be involved in illegitimate activities or hold inappropriate content. \n</p2><br/><br/>';
            text += '    <p2>The following is a list of ' + companyName + '\'s ip addresses which were found as being' +
                ' used for spam or distribution of other unwanted content.\n</p2>';
            text += this.addBlacklistsByCompany(allBlacklists, companies);
            text += '<h3>Recommendations</h3>' +
                '<p2>Eradicate the systems of potential botnets or malware that may be the cause of spamming and' +
                ' make sure they are adequately protected by anti-malware technologies. If these ip addresses belong' +
                ' to a shared hosting provider, change the server/provider.</p2>';
            text += '</div>';
        }

        if (allBotnets && allBotnets.length > 0) {
            text += '<div id="page" style="page-break-after: always;">';
            scoreImg = this.createScoreImg(allScoresData.countBotnets.score, displayScoreBackwards);
            text += '<myCenter><h2>BOTNETS AND MALWARE ' + scoreImg + '</h2></myCenter>';
            text += '    <p2>Botnets are network of connected devices (many time compromised) which are jointly executing' +
                ' malicious payloads. Botnets often used for: distributed denial of service, steal data, send spam and other' +
                ' malicious activities. We are scanning the internet in an attempt to identify company\'s compromised devices' +
                ' that run under such botnets.\n</p2><br/>';

            text += '<p2>The following is a list of IP addresses that are hosting malicious code and are most likely compromised. </p2>';
            text += this.addBotnetsByCompany(allBotnets, companies);
            text += '<h3>Recommendations</h3>' +
                '<p2>Eradicate the systems of potential botnets or malware and make sure they are adequately protected by anti-malware technologies. ' +
                'If these ip addresses belong to a shared hosting provider, change the server/provider.\n</p2>';
            text += '</div>';
        }

        let notValidHttpsSSL = [];
        allSSLCerts.map((item) => {
            if (item.statusSSL && (item.statusSSL.toLowerCase() === "no https" || !item.valid && item.statusSSL.toLowerCase() === "https")) {
                notValidHttpsSSL.push(item);
            }
            if (item.subdomains) {
                item.subdomains.map((item) => {
                    if (item.statusSSL && (item.statusSSL.toLowerCase() === "no https" || !item.valid && item.statusSSL.toLowerCase() === "https")) {
                        notValidHttpsSSL.push(item);
                    }
                });
            }
        });

        if (notValidHttpsSSL.length > 0) {
            scoreImg = this.createScoreImg(allScoresData.countSSLCerts.score, displayScoreBackwards);
            text += '<div id="page" style="page-break-after: always;">';
            text += '<myCenter><h2>HTTPS – SSL CERTIFICATES ' + scoreImg + '</h2></myCenter>';
            text += '    <p2>SSL certificates are used to prove ownership of a public key. The certificate holds several ' +
                'fields about the owner, its expiry date and its issuer. The certificate is a key component in' +
                ' establishing secure communication.  </p2><br/>';
            text += '    <p2>The following is a list of ' + companyName + '\'s expired ssl certificates. Misconfigured certificates here may ' +
                'indicate sensitive data could be compromised by eavesdropping.\n</p2>';
            text += this.addSSLCertsByCompany(notValidHttpsSSL, companies);
            text += '<h3>Recommendations</h3>' +
                '<p2>Make sure the SSL certificates are properly configured and maintained. ' +
                'Also, make sure to use adequate encryption and key exchange levels. ' +
                'Please see the following guide for more information:' +
                '<a style="color: dodgerblue;" href="https://www.acunetix.com/blog/articles/tls-ssl-cipher-hardening/"> ' +
                'https://www.acunetix.com/blog/articles/tls-ssl-cipher-hardening/</a></p2>';
            text += '</div>';
        }

        if (criticalImportanceAssets && Object.getOwnPropertyNames(criticalImportanceAssets).length > 0) {
            text += '<div id="page" style="page-break-after: always;">';
            text += '<myCenter><h2>CRITICAL FINDINGS</h2></myCenter>';
            text += '    <p2>The following were marked manually as critical assets and the company\'s risk score was set accordingly. ' +
                'When finding found on a critical asset the category risk score is set to the highest possible score.</p2>';
            Object.keys(criticalImportanceAssets).sort().map((currKey) => {
                if(criticalImportanceAssets[currKey] && Array.isArray(criticalImportanceAssets[currKey]) && criticalImportanceAssets[currKey].length > 0){
                    text += this.addGenericTablesByArrOfObj(criticalImportanceAssets[currKey], currKey, companies);
                }
            });
            text += '</div>';
        }

        if (highImportanceAssets && Object.getOwnPropertyNames(highImportanceAssets).length > 0) {
            text += '<div id="page" style="page-break-after: always;">';
            text += '<myCenter><h2>HIGH IMPORTANCE FINDINGS</h2></myCenter>';
            text += '    <p2>The following were marked manually as high importance assets and have a high impact on ' +
                'the risk assessment calculation.</p2>';
            Object.keys(highImportanceAssets).sort().map((currKey) => {
                if(highImportanceAssets[currKey] && Array.isArray(highImportanceAssets[currKey]) && highImportanceAssets[currKey].length > 0){
                    text += this.addGenericTablesByArrOfObj(highImportanceAssets[currKey], currKey, companies);
                }
            });
            text += '</div>';
        }

        if (mitigatedAssets && Object.getOwnPropertyNames(mitigatedAssets).length > 0) {
            text += '<div id="page" style="page-break-after: always;">';
            text += '<myCenter><h2>FINDINGS MARKED AS MITIGATED</h2></myCenter>';
            text += '    <p2>The following were marked manually as mitigated and  ' +
                'were not included in the risk assessment calculation.</p2>';
            Object.keys(mitigatedAssets).sort().map((currKey) => {
                if(mitigatedAssets[currKey] && Array.isArray(mitigatedAssets[currKey]) && mitigatedAssets[currKey].length > 0){
                    text += this.addGenericTablesByArrOfObj(mitigatedAssets[currKey], currKey, companies);
                }
            });
            text += '</div>';
        }

        if(allBuckets.length > 0 || allSPFs.length > 0 || allDMARCs.length > 0 || allSSLCerts.length > 0 || allASNs.length > 0){
            text += '<div id="page" style="page-break-after: always;">';
            text += '<myCenter><h2 style="padding-top: 600px; font-size: 35px">APPENDIX</h2></myCenter>';
            text += '</div>';

            text += this.addAppendix(allBuckets, allSPFs, allDMARCs, allSSLCerts, allASNs, fpAssets, companies, false);
        }

    } else if (reportKind === 'surveyReport') {

        text += '<!DOCTYPE html><html><head>';
        text += pdfStyle.getHtmlStyle() + pdfStyle.getHtmlCSS();
        text += '</head><body>';

        text += pdfHelpers.addFrontPage('Surveys Status Reports', names, logo);

        text += '<div id="page" style="page-break-after: always;">';
        text += '    <myCenter>';
        text += '      <h2>Executive Summary</h2>';
        text += '    </myCenter>';
        text += '<div style=" margin: 50px;">' + 'This report provides analysis, status, and visualization of the company vendors.' +
            ' It can be used to keep track and manage the surveys in accordance with the company\'s security needs.' + '</div>';
        text += '</div>';

        // Create table for Surveys.
        if (companies && companies.length > 0) {
            text += '<div id="page" style="page-break-after: always;">';
            text += '<myCenter><h2>Vendors</h2></myCenter>';
            text += this.addSurveyInfoByProject(companies);
            text += '</div>';
        }
    }

    if (manualData && Array.isArray(manualData) && manualData.length > 0) {
        manualData.map((currManualData) => {
            if (currManualData && (currManualData.title || currManualData.text || currManualData.files)) {
                text += '<div id="page" style="page-break-after: always;">';
                text += '<myCenter><h2>';
                text += (currManualData.title) ? currManualData.title : 'FINDING REVIEW';
                text += '</h2></myCenter>';
                text += '<p2>';
                text += (currManualData.text) ? currManualData.text : '';
                text += '</p2>';
                if (currManualData.files && Array.isArray(currManualData.files) && currManualData.files.length > 0) {
                    text += '<div><myCenter>';
                    currManualData.files.map((currFile, i) => {
                        if (currFile && currFile.path) {
                            const currPath = helpers.getFilePathInitial() + '/../../../intelManuals' + currFile.path;
                            text += '<img src="' + currPath + '" style="margin: 15px 0px; height: auto; width: auto; max-height: 768px; max-width: 1024px;">';
                            if (i < currManualData.files.length - 1) {
                                text += '<br/>';
                            }
                        }
                    });
                    text += '</div></myCenter>';
                }
                text += '</div>';
            }
        });
    }
    text += '</body></html>';
    return text;
};


module.exports.createSeparatedAssetObjectsFromAmData = (allAmData) => {
    let criticalImportanceAssets = {};
    let highImportanceAssets = {};
    let mitigatedAssets = {};
    let fpAssets = {};

    if (allAmData && typeof allAmData === 'object') {
        Object.keys(allAmData).map((currKey) => {
            if (allAmData[currKey] && Array.isArray(allAmData[currKey]) && allAmData[currKey].length > 0) {
                allAmData[currKey].map((curData) => {
                    if (curData && curData.type) {
                        if (curData.type === 'mitigated') {
                            if (mitigatedAssets[currKey] === undefined) {
                                mitigatedAssets[currKey] = [];
                            }
                            curData.status = 'Mitigated';
                            delete curData.importance;
                            delete curData.type;
                            mitigatedAssets[currKey].push(curData);
                        } else if (curData.type === 'assetUserInput' && curData.importance) {
                            if (Number(curData.importance) === 1) {
                                if (fpAssets[currKey] === undefined) {
                                    fpAssets[currKey] = [];
                                }
                                curData.status = 'False Positive';
                                delete curData.importance;
                                delete curData.type;
                                fpAssets[currKey].push(curData);
                            } else if (Number(curData.importance) === 4) {
                                if (highImportanceAssets[currKey] === undefined) {
                                    highImportanceAssets[currKey] = [];
                                }
                                curData.importance = 'High';
                                delete curData.type;
                                highImportanceAssets[currKey].push(curData);
                            } else if (Number(curData.importance) === 5) {
                                if (criticalImportanceAssets[currKey] === undefined) {
                                    criticalImportanceAssets[currKey] = [];
                                }
                                curData.importance = 'Critical';
                                delete curData.type;
                                criticalImportanceAssets[currKey].push(curData);
                            }
                        }
                    }
                });
            }
        });
    }

    return {criticalImportanceAssets: criticalImportanceAssets, highImportanceAssets: highImportanceAssets, mitigatedAssets: mitigatedAssets, fpAssets: fpAssets};
};

module.exports.updateAllReportDataToDemoModeData = (findingsObj) => {
    if (findingsObj && typeof findingsObj === 'object') {
        Object.keys(findingsObj).map((currKey) => {
            if (findingsObj[currKey] && Array.isArray(findingsObj[currKey]) && findingsObj[currKey].length > 0) {
                let idx = 1;
                findingsObj[currKey].map((curData) => {
                    idx = this.changePropsToDemoMode(curData, idx);
                });
            } else if(findingsObj[currKey] && typeof findingsObj[currKey] === 'object'){
                let curObject = findingsObj[currKey];
                Object.keys(curObject).map((key) => {
                    if (curObject[key] && Array.isArray(curObject[key]) && curObject[key].length > 0) {
                        let idx = 1;
                        curObject[key].map((curObjData) => {
                            idx = this.changePropsToDemoMode(curObjData, idx);
                        });
                    }
                });
            }
        });
    }

    return findingsObj;
};


module.exports.changePropsToDemoMode = (curData, idx = 0) =>{
    if(curData){
        let wasUpdate = false;
        let demoCompanyName = 'Company Name ' + idx;
        let demoDomain = 'domain-example-' + idx + '.com';
        let demoSubdomain = 'subdomain-example-' + idx + '.com';
        let demoEmail = 'emailExample' + idx + 'mail.com';
        if (curData.hostname) {
            curData.hostname = demoDomain;
            wasUpdate = true;
        }
        if (curData.Hostname) {
            curData.Hostname = demoDomain;
            wasUpdate = true;
        }
        if (curData.domain) {
            curData.domain = demoDomain;
            wasUpdate = true;
        }
        if (curData.subjectaltname) {
            curData.subjectaltname = 'DNS:' + demoDomain;
            wasUpdate = true;
        }
        if (curData.subdomain) {
            curData.subdomain = demoSubdomain;
            wasUpdate = true;
        }
        if (curData.email) {
            curData.email = demoEmail;
            wasUpdate = true;
        }
        if (curData.userEmail) {
            curData.userEmail = demoEmail;
            wasUpdate = true;
        }
        if (curData.companyName) {
            curData.companyName = demoCompanyName;
            wasUpdate = true;
        }
        if (curData.ip) {
            curData.ip = this.createIpStrForDemoMode(curData.ip, idx);
            wasUpdate = true;
        }
        if (curData.data && curData.data.blacklist_ip && curData.data.blacklist_ip.address) {
            curData.data.blacklist_ip.address = this.createIpStrForDemoMode(curData.data.blacklist_ip.address, idx);
            wasUpdate = true;
        }
        if (curData.subject && curData.subject.CN) {
            curData.subject.CN = demoDomain;
            wasUpdate = true;
        }
        if(curData.domains && Array.isArray(curData.domains) && curData.domains.length > 0){
            for(let i=0;i<curData.domains.length;i++){
                curData.domains[i] = 'domain-example-' + i + '.com';
            }
        }
        if(curData.selectedDomains && Array.isArray(curData.selectedDomains) && curData.selectedDomains.length > 0){
            for(let j=0;j<curData.selectedDomains.length;j++){
                curData.selectedDomains[j] = 'domain-example-' + j + '.com';
            }
        }
        if(curData.relatedCompanies && Array.isArray(curData.relatedCompanies) && curData.relatedCompanies.length > 0 && (curData.cve === undefined && curData.cpe === undefined)){
            for(let j=0;j<curData.relatedCompanies.length;j++){
                curData.relatedCompanies[j] = 'Company Name ' + j;
            }
        }
        if(wasUpdate){
            idx++;
        }
    }
    return idx;
};

module.exports.createIpStrForDemoMode = (ip, idx = 1) => {
    let returnedStr = 'xx.xx.' + idx + '.';
    if(ip && ip.length > 0){
        returnedStr += ip.split('.')[3];
    } else {
        returnedStr += 'xx';
    }
    return returnedStr;
};

module.exports.addAppendix = (allBuckets, allSPFs, allDMARCs, allSSLCerts, allASNs, fpAssets, companies, isWord, wordSpacing) =>{
    const wordTitleStyle = 'style="margin-left: auto; margin-right: auto; display: inherit; text-align: center;"';
    let companyName = companies && companies[0] && companies[0].companyName? companies[0].companyName : '';
    let text = '';

    //check who is the last page so we dont have one page extra
    let bucketsLast = false, sPFsLast = false, dMARCsLast = false, sSLCertsLast = false, aSNsLast = false, fmLast = false;
    let pageId= 'id="page" style="page-break-after: always;"';
    let lastPageId = ' id="page"';
    if (Object.getOwnPropertyNames(fpAssets).length > 0) {
        fmLast = true;
    } else if (allASNs.length > 0) {
        aSNsLast = true;
    } else if (allSSLCerts.length > 0) {
        sSLCertsLast = true;
    } else if (allDMARCs.length > 0) {
        dMARCsLast = true;
    } else if(allSPFs.length > 0) {
        sPFsLast = true;
    } else {
        bucketsLast = true;
    }

    if (allBuckets && allBuckets.length > 0) {
        text += isWord && wordSpacing? wordSpacing.pageBreak : '';
        text += bucketsLast ? '<div '+ lastPageId +' >':'<div '+ pageId +' >';
        text += isWord? '<myCenter><h2 ' + wordTitleStyle + '>S3 BUCKETS</h2></myCenter>' : '<myCenter><h2>S3 BUCKETS</h2></myCenter>';
        text += this.addRecordsByCompany(allBuckets, TYPE_BUCKET, companies);
        text += '</div>';
    }

    if (allSPFs && allSPFs.length > 0) {
        text += isWord && wordSpacing? wordSpacing.pageBreak : '';
        text += sPFsLast ? '<div '+ lastPageId +' >':'<div '+ pageId +' >';
        text += isWord? '<myCenter><h2 ' + wordTitleStyle + '>SPF</h2></myCenter>' : '<myCenter><h2>SPF</h2></myCenter>';
        text += this.addRecordsByCompany(allSPFs, TYPE_SPF, companies);
        text += this.addSPFExtraInfo(isWord && wordSpacing? wordSpacing.rowBreak : '');
        text += '</div>';
    }

    if (allDMARCs && allDMARCs.length > 0) {
        text += isWord && wordSpacing? wordSpacing.pageBreak : '';
        text += dMARCsLast ? '<div '+ lastPageId +' >':'<div '+ pageId +' >';
        text += isWord? '<myCenter><h2 ' + wordTitleStyle + '>DMARC</h2></myCenter>' : '<myCenter><h2>DMARC</h2></myCenter>';
        text += this.addRecordsByCompany(allDMARCs, TYPE_DMARC, companies);
        text += '</div>';
    }

    if (allSSLCerts && Array.isArray(allSSLCerts) && allSSLCerts.length > 0) {
        text += isWord && wordSpacing? wordSpacing.pageBreak : '';
        text += sSLCertsLast ? '<div '+ lastPageId +' >':'<div '+ pageId +' >';
        text += isWord? '<myCenter><h2 ' + wordTitleStyle + '>SSL CERTIFICATE</h2></myCenter>' : '<myCenter><h2>SSL CERTIFICATE</h2></myCenter>';
        text += '<p2>The following is an analysis of ' + companyName + '\'s ssl certificates. Misconfigured certificates here may' +
            ' indicate sensitive data could be compromised by eavesdropping.</p2>';
        text += this.addSSLCertsByCompany(allSSLCerts, companies);
        text += '</div>';
    }

    if (allASNs && allASNs.length > 0) {
        text += isWord && wordSpacing? wordSpacing.pageBreak : '';
        text += aSNsLast ? '<div '+ lastPageId +' >':'<div '+ pageId +' >';
        text += isWord? '<myCenter><h2 ' + wordTitleStyle + '>ASN REVIEW</h2></myCenter>' : '<myCenter><h2>ASN REVIEW</h2></myCenter>';
        text += '    <p2>Please find the following list of ASNs by company ' +
            '(an ASN stands for "Autonomous system number" which is in a fact a large block of assigned ip addresses).\n</p2>';
        text += this.addASNsByCompany(allASNs, companies);
        text += '</div>';
    }

    if (fpAssets && Object.getOwnPropertyNames(fpAssets).length > 0) {
        text += isWord && wordSpacing? wordSpacing.pageBreak : '';
        text += fmLast ? '<div '+ lastPageId +' >':'<div '+ pageId +' >';
        text += isWord? '<myCenter><h2 ' + wordTitleStyle + '>FINDINGS MARKED AS FALSE POSITIVE</h2></myCenter>' : '<myCenter><h2>FINDINGS MARKED AS FALSE POSITIVE</h2></myCenter>';
        text += '    <p2>The following were marked manually as false positive and were ' +
            'not included in the risk assessment calculation.</p2>';
        Object.keys(fpAssets).map((currKey) => {
            if(fpAssets[currKey] && Array.isArray(fpAssets[currKey]) && fpAssets[currKey].length > 0){
                text += this.addGenericTablesByArrOfObj(fpAssets[currKey], currKey, companies);
            }
        });
        text += '</div>';
    }

    return text;
};

module.exports.addSurveyInfoByProject = (companies) => {
    let text = '';
    text += '<br/>';
    if (companies && companies.length > 0) {
        const recordsHeaders = ['Company', 'Survey Name', 'Contact', 'Sensitivity', 'Last Modified', 'Completed', 'Survey Score'];
        const currentCompSurveyInfo = [];

        companies.map((currCompany) => {
            if (currCompany && currCompany.companyName) {
                const currObj = {
                    companyName: currCompany.companyName,
                    surveyName: currCompany.surveyName,
                    contact: currCompany.contact,
                    sensitivity: currCompany.sensitivity,
                    lastUpdated: currCompany.surveyLastUpdate,
                    surveyCompletion: currCompany.surveyCompletion,
                    surveyScore: currCompany.surveyScore
                };
                currentCompSurveyInfo.push(currObj);
            }
        });
        let currentCompSurveyInfoSorted = currentCompSurveyInfo
            .sort((a, b) => {
                let textA = a.companyName;
                let textB = b.companyName;
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });

        if (currentCompSurveyInfoSorted.length > 0) {
            text += pdfHelpers.addRecordsTable(currentCompSurveyInfoSorted, recordsHeaders, TYPE_SURVEY);
        }
    }
    return text;
};


module.exports.getHeadersForGenericTables = (data) => {
    let headers = [];
    if(data && data.length > 0 && data[0]){
        let curData = data[0];
        Object.keys(curData).map((key) => {
            if (curData && curData[key] && key) {
                if (key === 'userEmail') {
                    headers.push('Marked By');
                } else if (key === 'dmarc') {
                    headers.push('DMARC');
                } else if (key === 'spf') {
                    headers.push('SPF');
                } else if (key === 'isp') {
                    headers.push('ISP');
                } else if (key === 'bucket_status') {
                    headers.push('Bucket Status');
                } else if (key === 'cveScore') {
                    headers.push('Cve Score');
                    if(curData['summary'] === null){
                        headers.push('Summary');
                    }
                } else {
                    headers.push(key.charAt(0).toUpperCase() + key.slice(1));
                }
            }
        });
    }
    return headers;
};
module.exports.getTitleForGenericTables = (currKey) => {
    let title = '';
    if(currKey === "botnetsAm"){
        title = 'Botnets';
    } else if(currKey === "bucketsAm"){
        title = 'Buckets';
    } else if(currKey === "cveAm"){
        title = 'Cves';
    } else if(currKey === "dataleaksAm"){
        title = 'Dataleaks';
    } else if(currKey === "dmarcAm"){
        title = 'DMARCs';
    } else if(currKey === "spfAm"){
        title = 'SPFs';
    } else if(currKey === "emailBreachesAm"){
        title = 'Email Breaches';
    } else if(currKey === "ipsAm"){
        title = 'IPs';
    } else if(currKey === "ispFm"){
        title = 'ISPs';
    } else if(currKey === "portsAm"){
        title = 'Ports';
    } else if(currKey === "productsAm"){
        title = 'Discovered Systems';
    } else if(currKey === "sslAm"){
        title = 'SSLs';
    } else if(currKey === "noSslAm"){
        title = 'No SSLs';
    } else if(currKey === "blacklistsAm"){
        title = 'BlackLists';
    }
    return title;
};


module.exports.addGenericTablesByArrOfObj = (curData, type, companies) => {
    let text = '';
    if (curData && Array.isArray(curData) && curData.length > 0) {

        let title = this.getTitleForGenericTables(type);
        let headers = this.getHeadersForGenericTables(curData);
        if (companies && companies.length > 0) {
            text += '<h3>'+ title +'</h3>';
            text += pdfHelpers.addRecordsForGenericTable(curData, headers, type);
        }
    }
    return text;

};

module.exports.addASNsByCompany = (allASNs, companies) => {
    let text = '';
    if (allASNs && allASNs.length > 0) {
        text += '<h3>ASN Findings</h3>';
        if (companies && companies.length > 0) {
            const recordsHeaders = ['ASN', 'Description', 'Registrar', 'Range', 'Country Code', 'Related Domains'];

            companies.map((currCompany) => {
                const currentCompASNs = [];
                if (currCompany && currCompany.companyName && currCompany.selectedDomains && currCompany.selectedDomains.length > 0) {
                    allASNs.map((currASN) => {
                        if (currASN && currASN.domains) {
                            const matchingDomains = [];
                            currASN.domains.map((currASNDomain) => {
                                if (currASNDomain) {
                                    currCompany.selectedDomains.map((currCompDomain) => {
                                        if (currCompDomain && currASNDomain === currCompDomain) {
                                            matchingDomains.push(currCompDomain);
                                        }
                                    });
                                }
                            });
                            if (matchingDomains.length > 0) {
                                const currObj = {
                                    asn: currASN.asn,
                                    description: currASN.description,
                                    registrar: currASN.registrar,
                                    range: currASN.range,
                                    countryCode: currASN.countryCode,
                                    relatedDomains: matchingDomains
                                };
                                currentCompASNs.push(currObj);
                            }
                        }
                    });
                }
                if (currentCompASNs.length > 0) {
                    text += '<h4>' + currCompany.companyName + '\'s ASNs</h4>';
                    text += pdfHelpers.addRecordsTable(currentCompASNs, recordsHeaders, TYPE_ASNS);
                }
            });
        }
    }
    return text;
};

module.exports.addDataleaksByCompany = (allDataleaks, companies) => {
    let text = '';
    if (allDataleaks && allDataleaks.length > 0) {
        text += '<h3>Last Month Findings</h3>';
        if (companies && companies.length > 0) {
            const recordsHeaders = ['Source', 'Title', 'URL'];

            companies.map((currCompany) => {
                const currentCompDataleaks = [];
                if (currCompany && currCompany.companyName && currCompany.keywords && currCompany.keywords.length > 0) {
                    allDataleaks.map((currDataleak) => {
                        if (currDataleak && currDataleak.keyword && currCompany.keywords.includes(currDataleak.keyword)) {
                            currentCompDataleaks.push((currDataleak));
                        }
                    });
                }
                if (currentCompDataleaks.length > 0) {
                    text += '<h4>Discovered on ' + currCompany.companyName + '</h4>';
                    text += pdfHelpers.addRecordsTable(currentCompDataleaks, recordsHeaders, TYPE_DATALEAKS);
                }
            });
        }
    }
    return text;
};

module.exports.addEmailBreachesByCompany = (allEmailBreaches, companies) => {
    const recordsHeaders = ['Email', 'Appeared in Breach', 'Breach Date', 'Compromised Data'];

    return addFindingsByCompany(allEmailBreaches, companies, TYPE_EMAIL_BREACHES, recordsHeaders);
};

module.exports.addBlacklistsByCompany = (allBlacklists, companies) => {
    const recordsHeaders = ['Domain', 'IP Address', 'Appeared in Domain Blacklists', 'Appeared in IP Blacklists', 'Appeared in MX Blacklists', 'Appeared in NS Blacklists'];

    return addFindingsByCompany(allBlacklists, companies, TYPE_BLACKLISTS, recordsHeaders);
};

module.exports.addBotnetsByCompany = (allBotnets, companies) => {
    // recordsHeaders is empty since every feed will define its own headers.
    const recordsHeaders = [];

    return addFindingsByCompany(allBotnets, companies, TYPE_BOTNETS, recordsHeaders);
};

module.exports.addSSLCertsByCompany = (allSSLCerts, companies) => {
    const recordsHeaders = [
        {label: 'Hostname', key: 'domain'},
        {label: 'Status', key: 'statusSSL'},
        {label: 'Is Valid', key: 'valid'},
        {label: 'Days Remaining', key: 'days_remaining'},
        {label: 'Days Expired', key: 'days_expired'},
        {label: 'Issued To', key: 'subject.CN'},
        {label: 'Issued By', key: 'issuer.CN'}
    ];

    return addFindingsByCompany(allSSLCerts, companies, TYPE_SSL_CERTS, recordsHeaders);
};

// This function used for Leaked Emails and Blacklists.
const addFindingsByCompany = (allRecords, companies, recordsType, recordsHeaders) => {
    let text = '';
    if (allRecords && allRecords.length > 0) {
        let dataLabel;
        if (recordsType === TYPE_EMAIL_BREACHES) {
            dataLabel = 'Leaked Emails';
        } else if (recordsType === TYPE_BOTNETS) {
            dataLabel = 'Botnets And Malware Findings';
        } else if (recordsType === TYPE_SSL_CERTS) {
            dataLabel = 'SSL Certificate Findings';
        } else {
            // This means type is Blacklists.
            dataLabel = '' + companies[0].companyName + '\'s blacklisted ip addresses';
        }
        text += '<h3>' + dataLabel + ' </h3>';
        if (companies && companies.length > 0) {
            companies.map((currCompany) => {
                if (recordsType === TYPE_BOTNETS) {
                    if (currCompany && currCompany.companyName && currCompany.selectedDomains && currCompany.selectedDomains.length > 0) {
                        let isFirstFinding = true;
                        allRecords.map((currFinding) => {
                            if (currFinding && currFinding.feedName && currFinding.feedData && Array.isArray(currFinding.feedData)) {
                                const currentCompFeedFindings = [];
                                const currentCompFeedHeaders = [];
                                currFinding.feedData.map((currFeedData) => {
                                    if (currFeedData && currFeedData.domain && currCompany.selectedDomains.includes(currFeedData.domain)) {
                                        if (isFirstFinding) {
                                            text += '<h4>' + currCompany.companyName + '\'s ip addresses with botnets or malware:</h4>';
                                            isFirstFinding = false;
                                        }
                                        // Duplicate currFeedData.
                                        const objectToInsert = JSON.parse(JSON.stringify(currFeedData));
                                        if (objectToInsert.hasOwnProperty('relatedCompanies')) {
                                            delete objectToInsert.relatedCompanies;
                                        }
                                        currentCompFeedFindings.push(objectToInsert);
                                        // If the headers weren't initialize yet - do it.
                                        if (currentCompFeedHeaders.length === 0) {
                                            // Set this props as first ones to be displayed.
                                            currentCompFeedHeaders.push('Domain', 'IP Address');
                                            Object.keys(objectToInsert).map((currKey) => {
                                                if (currKey && !['domain', 'address'].includes(currKey)) {
                                                    currentCompFeedHeaders.push(currKey);
                                                }
                                            });
                                        }
                                    }
                                });
                                if (currentCompFeedFindings.length > 0) {
                                    text += '<h4 style="color: rgb(31, 77, 120)">' + currFinding.feedName + '</h4>';
                                    text += pdfHelpers.addRecordsTable(currentCompFeedFindings, currentCompFeedHeaders, recordsType);
                                }
                            }
                        });
                    }
                } else {
                    const currentCompFindings = [];
                    if (currCompany && currCompany.companyName && currCompany.selectedDomains && currCompany.selectedDomains.length > 0) {
                        allRecords.map((currFinding) => {
                            if (currFinding && currFinding.domain && (currCompany.selectedDomains.includes(currFinding.domain)
                                || currCompany.selectedDomains.includes(parseDomain(currFinding.domain).domain + '.' + parseDomain(currFinding.domain).tld))) {
                                currentCompFindings.push((currFinding));
                            }
                        });
                    }
                    if (currentCompFindings.length > 0) {
                        text += '<h4>Discovered on ' + currCompany.companyName + '</h4>';
                        if (recordsType === TYPE_SSL_CERTS) {
                            let dataHeaders = [], dataKeys = [];
                            recordsHeaders.map((currHeader) => {
                                if (currHeader && currHeader.label && currHeader.key) {
                                    dataHeaders.push(currHeader.label);
                                    dataKeys.push(currHeader.key);
                                }
                            });
                            text += pdfHelpers.addRecordsTableByKeys(currentCompFindings, dataHeaders, dataKeys, recordsType);
                        } else {
                            text += pdfHelpers.addRecordsTable(currentCompFindings, recordsHeaders, recordsType);
                        }
                    }
                }
            });
        }
    }
    return text;
};

// This function used for Buckets, DMARCs and SPFs.
module.exports.addRecordsByCompany = (allRecords, recordType, companies, rowBreak = '') => {
    let text = '';
    if (allRecords && allRecords.length > 0 && recordType) {
        text += '<h3>Domains with ' + recordType.toUpperCase() + ' records</h3>';
        if (companies && companies.length > 0) {
            companies.map((currCompany) => {
                const currentCompRecords = [];
                if (currCompany && currCompany.companyName && currCompany.selectedDomains && currCompany.selectedDomains.length > 0) {
                    currCompany.selectedDomains.map((currDomain) => {
                        allRecords.map((currRecord) => {
                            if (recordType === TYPE_BUCKET) {
                                if (currRecord && ((currRecord.subdomain && currRecord.subdomain !== 'none' && currRecord.subdomain.includes(currDomain)) ||
                                    (currRecord.hostname && currRecord.hostname === currDomain))) {
                                    currentCompRecords.push((currRecord));
                                }
                            } else if (recordType === TYPE_DMARC || recordType === TYPE_SPF) {
                                if (currRecord && currRecord.hostname && currRecord.hostname === currDomain) {
                                    currentCompRecords.push((currRecord));
                                }
                            }
                        });
                    });
                }
                if (currentCompRecords.length > 0) {
                    text += '<h4>' + currCompany.companyName + '\'s ' + recordType + ' report\n</h4>';
                    const recordsHeaders = ['Domain', recordType + ' Record'];
                    if (recordType === TYPE_BUCKET) {
                        recordsHeaders.push(recordType + ' Status');
                    }
                    text += pdfHelpers.addRecordsTable(currentCompRecords, recordsHeaders);
                }
            });
        }
    }
    return text;
};

// rowBreak is used for Word report.
module.exports.addSPFExtraInfo = (rowBreak = '') => {
    return '<h3>SPF tags index</h3>' +
        '<p2><strong>ALL</strong> - Matches always; used for a default result like -all for all IPs not matched by prior mechanisms.</p2>' + rowBreak +
        '<p2><strong>A</strong> - If the domain name has an address record (A or AAAA) that can be resolved to the sender\'s address, it will match.</p2>' + rowBreak +
        '<p2><strong>IP4</strong> - If the sender is in a given IPv4 address range, match.</p2>' + rowBreak +
        '<p2><strong>IP6</strong> - If the sender is in a given IPv6 address range, match.</p2>' + rowBreak +
        '<p2><strong>MX</strong> - If the domain name has an MX record resolving to the sender\'s address, it will match (i.e. the mail comes from one of the domain\'s incoming mail servers).</p2>' + rowBreak +
        '<p2><strong>PTR</strong> - If the domain name (PTR record) for the client\'s address is in the given domain and that domain name resolves to the client\'s address (forward-confirmed reverse DNS), match. This mechanism is deprecated and should no longer be used.</p2>' + rowBreak +
        '<p2><strong>EXISTS</strong> - If the given domain name resolves to any address, match (no matter the address it resolves to). This is rarely used. Along with the SPF macro language it offers more complex matches like.</p2>' + rowBreak +
        '<p2><strong>INCLUDE</strong> - References the policy of another domain. If that domain\'s policy passes, this mechanism passes. However, if the included policy fails, processing continues. To fully delegate to another domain\'s policy, the redirect extension must be used.</p2>' + rowBreak +
        '<h3>Recommendations</h3>' + rowBreak +
        '<p2>SPF and DMARC records must be setup for all your domains, ' +
        '(even those which are not actively being used).' +
        ' Please see the following guide - ' +
        '<a style="color: dodgerblue;" href="https://www.esecurityplanet.com/applications/how-to-set-up-implement-dmarc-email-security.html"> https://www.esecurityplanet.com/applications/how-to-set-up-implement-dmarc-email-security.html</a></p2>';
};

module.exports.addCVECard = (cveVuln) => {
    if (!cveVuln || !cveVuln.domains || cveVuln.domains.length < 1 || !cveVuln.ip || !cveVuln.port || !cveVuln.cpe || !cveVuln.cve) {
        return '';
    }
    let cvePreHeader, cveColor, cveScoreColor;
    if (cveVuln.cveScore) {
        cveScoreColor = pdfStyle.getScoreColor(cveVuln.cveScore);
        if (cveVuln.cveScore >= 7.5 && cveVuln.cveScore < 9.0) {
            cvePreHeader = 'High | ';
            cveColor = '#F4511E';
        } else if (cveVuln.cveScore >= 9.0) {
            cvePreHeader = 'Critical | ';
            cveColor = '#e22f2f';
        } else {
            cvePreHeader = '';
            cveColor = 'grey';
        }
    } else {
        cvePreHeader = '';
        cveColor = 'grey';
        cveScoreColor = 'black';
    }
    let relatedDomains = (cveVuln.domains.length === 1) ? '<b>Domain</b>: ' : '<b>Domains</b>: ';
    for (let i = 0; i < cveVuln.domains.length; i++) {
        relatedDomains += cveVuln.domains[i];
        if (i < cveVuln.domains.length - 1) {
            relatedDomains += ', ';
        }
    }
    const cpeArr = cveVuln.cpe.split(':');
    let vulnCpe;
    if (cpeArr.length >= 3) {
        vulnCpe = cpeArr[cpeArr.length - 3] + ' ' + cpeArr[cpeArr.length - 2] + ' ' + cpeArr[cpeArr.length - 1];
    } else {
        vulnCpe = 'None';
    }
    let scoreAndSummary = '';
    if (cveVuln.cveScore != null || cveVuln.cveSummary) {
        scoreAndSummary = '<div style=" background-color: rgb(245,245,245); padding: 0px 20px; margin-bottom: 2px;">' +
            '<div style="box-sizing: border-box; content: \' \'; display: table;"></div>' +
            '<div>';

        if (cveVuln.cveScore != null) {
            scoreAndSummary += '<div style="padding-bottom: 0px; margin: 10px 0;"><div style="font-weight: 500; box-sizing: border-box; position: relative; white-space: nowrap; cursor: pointer;">' +
                '<div style="display: inline-flex; vertical-align: left; white-space: normal; margin-bottom: 5px; padding-right: 90px;">' +
                '<span style="display: inline; font-size: 13px;">CVSS: </span>' +
                '<span style="color: ' + cveScoreColor + '; display: inline; font-size: 13px;">' +
                cveVuln.cveScore +
                '</span></div>' +
                '</div>';
        }
        if (cveVuln.cveSummary) {
            scoreAndSummary += '<div style="position: relative;">' +
                '<span style=" color: rgb(75,75,75); display: block; font-size: 11px; margin-top: 17px;">' +
                cveVuln.cveSummary +
                '</span></div>';
        }
        scoreAndSummary += '</div></div><div style="box-sizing: border-box; content: \' \'; clear: both; display: table;"></div></div>';
    }
    return '<div style=" color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms 0ms; box-sizing: border-box; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 0px 52px 25px;">\n' +
        '<div style="box-sizing: border-box; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); background-color: rgb(232, 232, 232);  padding: 0px 24px; display: flex; justify-content: space-between;">\n' +
        '<div style="position: relative; display: flex; justify-content: space-between; align-items: left;">\n' +
        '<span style="padding-right: 16px; line-height: 38px; font-size: 14px; position: relative; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; color: ' + cveColor + ';">' +
        ' ' + cvePreHeader + ' ' +
        '<a href="https://nvd.nist.gov/vuln/detail/' + cveVuln.cve + '" target="_blank" style=" text-decoration: none !important; color: ' + cveColor + ';">' + cveVuln.cve + '</a>' +
        '</span></div></div>' +
        '<div class="markdown-body" style="margin-top: 0px; margin-bottom: 0px; padding: 10px 20px 0px; background: rgb(246,246,246); overflow: auto; align-items: left;">\n' +
        '<p style="line-height: 10px;"><b>Product</b>: ' + vulnCpe + '</p>\n' +
        '<p style="line-height: 140%;">' + relatedDomains + '</p>\n' +
        '<p style="line-height: 10px;"><b>IP</b>: ' + cveVuln.ip + '</p>\n' +
        '<p style="line-height: 10px;"><b>Port</b>: ' + cveVuln.port + '</p>\n' +
        '</div>\n' +
        scoreAndSummary +
        '</div>';
};

module.exports.addScoreBox = (name, maxIntelScore, surveyScore, ratios, displayScoreBackwards, isSurveyNA) => {
    // Check if maxIntelScore is a number. (maxIntelScore == null means null or undefined)
    if (maxIntelScore == null || isNaN(parseFloat(maxIntelScore)) || !isFinite(maxIntelScore) || !name || !name.trim()) {
        return '';
    } else {
        let maxIntelScoreSpan = '', surveyScoreSpan = '';

        const greySpanStyle = ' style="color: rgb(111, 111, 111); margin-left: 10px" ';

        if (surveyScore != null && !isNaN(surveyScore) && isFinite(surveyScore)) {
            // isSurveyNA is just to identify if the survey exists or not, fixed afterwards to 0 for further calculations.
            if (isSurveyNA) {
                surveyScore = 0;
                surveyScoreSpan = '';
            } else {
                let surveyRiskScore = surveyScore;

                if (surveyRiskScore > 0) {
                    if (displayScoreBackwards) {
                        surveyRiskScore = helpers.switchScoreBackwards(surveyRiskScore);
                    }
                    surveyScoreSpan = '<span' + greySpanStyle + '> Survey Score: ' + surveyRiskScore + '</span>';
                }
            }
        } else {
            surveyScore = 0;
        }

        let totalScore = helpers.calculateTotalScore(maxIntelScore, surveyScore, ratios, isSurveyNA);

        if (totalScore === 0) {
            maxIntelScoreSpan = '<span' + greySpanStyle + '>Low Risk</span>';

            // Reset surveyScoreSpan so only 'Low Risk' message is displayed in case of totalScore being 0.
            surveyScoreSpan = '';
        } else {
            let maxIntelRiskScore = maxIntelScore;

            if (displayScoreBackwards) {
                maxIntelRiskScore = helpers.switchScoreBackwards(maxIntelRiskScore);
            }

            maxIntelScoreSpan = '<span' + greySpanStyle + '>Intel Score: ' + maxIntelRiskScore + '</span>';
        }

        const colorClass = pdfStyle.getScoreColorClass(totalScore);

        const scoreClass = this.getScoreClass(totalScore);

        if (displayScoreBackwards) {
            totalScore = helpers.switchScoreBackwards(totalScore);
        }

        // Should look like: <div class="c100 p25 low"> or <div class="c100 p99 high">
        return '<div style="display: inline-block; margin: 15px">\n' +
            '      <span><b>' + name + '</b></span>\n' +
            '      <div class="c100 p' + scoreClass + ' ' + colorClass + ' big center" style="margin: 10px;">\n' +
            '         <span>' + totalScore + '</span>\n' +
            '         <div class="slice">\n' +
            '            <div class="bar"></div>\n' +
            '            <div class="fill"></div>\n' +
            '         </div>\n' +
            '      </div>\n' +
            maxIntelScoreSpan +
            surveyScoreSpan +
            '  </div>';
    }
};

module.exports.getScoreClass = (score) => {
    if (score < 0) {
        return 0;
    }
    if (score > 100 || score === 0) {
        return 100;
    }
    // check if a natural number. (0,1,2...)
    if (!Number.isInteger(score)) {
        return parseInt(score, 10);
    } else {
        return score;
    }
};

module.exports.addReportDetails = (resultCounts, companiesWithScores, locationsImg, allGeoLocations, isInDemoMode) => {
    let text = '';

    let companyName = 'the chosen companies';
    let companyString = 'those companies';

    //in case we get multiple companies to the report, we dont want to specific one on the summary.
    if (companiesWithScores && Array.isArray(companiesWithScores) && companiesWithScores.length > 0) {
        companyName = '';
        companyString = '';
        if(isInDemoMode){
            companyName = ' "Company Name" ';
        } else if (companiesWithScores.length > 1) {
            for (let i = 0; i < companiesWithScores.length - 1; i++) {
                if (i !== 0) {
                    companyName += ', ';
                }
                companyName += companiesWithScores.companyName;
            }
            companyName += 'and ' + companiesWithScores[companiesWithScores.length - 1].companyName;
            companyString = 'those companies';
            //in case we get single company, we specific one company on the summary.
        } else if (companiesWithScores.length === 1) {
            companyName = companiesWithScores[0].companyName;
            companyString = companyName + '\'s';
        }
    }
    text += locationsImg? '<div id="page" style="page-break-after: always;">': '';
    text += '<div>' +
        '<p2>The following report provides an in-depth OSINT based risk analysis of ' + companyName + ', as well as a\n' +
        'summary of scores from any surveys completed by ' + companyName + '. OSINT stands for open source\n' +
        'intelligence, which is the process of collecting large quantities of free-flowing information,\n' +
        'analyzing and correlating this data in order to reach certain assumptions.\n' +
        'The purpose of our analysis is to provide information about ' + companyString + ' Internet facing assets and the\n' +
        'risks they might currently sustain. This is done in three phases, the "Discovery" phase, the\n' +
        '"Enrichment" phase and the scoring phase. During the discovery phase, our system uses our\n' +
        '"smart traversal" algorithm, to find the connections from each asset to another, ultimately\n' +
        'discovering the entire network of assets. In the enrichment phase, we take all the information\n' +
        'from the discovery phase, and cross reference it with security data feeds, and our own best\n' +
        'practice rules. Finally, the last step takes all the gathered information from the first two phases' +
        ' and the surveys then calculates a risk score according to our methodology and configured weights. </p2>\n' +
        '</div>';


    if (locationsImg) {
        text += '    <h3>Assets Location</h3>';
        text += '    <p2>The following is a geographical display of ' + companyName + '\'s assets. Our experience indicates that\n' +
            ' in many cases even for very well organized companies it is possible to identify unknown resources in unexpected\n' +
            'geographies.  This is true mainly for larger multinational companies where local offices take initiatives\n' +
            ' that are not always known to the security/compliance teams. \n</p2>';

        text += '<h3>Recommendations</h3>' +
            '<p2>We find it important to scan the map and identify surprising asset locations. We also find this map ' +
            'useful for assets geographical distribution when considering GDPR compliance.\n</p2>';
        text += '</div>';


        text += '    <h3>Map of assets</h3>';
        text += '    <myCenter>';
        text += '    <img alt="" src="' + locationsImg + '" style="width: 850px; height: 850px">';
        text += '    </myCenter>';

        if (allGeoLocations && allGeoLocations.length > 0) {
            text += '    <h3>IP/Geolocation Table</h3>';
            text += pdfHelpers.addRecordsTable(allGeoLocations,
                ['Country', 'Assets'],
                TYPE_GEO_LOCATIONS);
        }
        text += '    <br/>';
    }

    return text;

};

module.exports.addExecutiveSummary = (resultCounts, companiesWithScores, displayScoreBackwards, isWord, isInDemoMode) => {
    let text = '';

    const portsByIPCount = resultCounts.portsByIPCount || [];
    const notConfiguredBuckets = resultCounts.notConfiguredBuckets || 0;
    const notConfiguredSPFs = resultCounts.notConfiguredSPFs || 0;
    const notConfiguredDMARCs = resultCounts.notConfiguredDMARCs || 0;
    const allCVEsCount = resultCounts.totalCVEs || 0;
    const blacklistsCount = resultCounts.allBlacklistsCount || 0;
    const botnetsCount = resultCounts.allBotnetsCount || 0;
    const sslCertsCount = resultCounts.allSslCertsCount || 0;

    let companyName = 'the chosen companies';

    //in case we get multiple companies to the report, we dont want to specific one on the summary.
    if (companiesWithScores && Array.isArray(companiesWithScores) && companiesWithScores.length > 0) {
        companyName = '';
        if(isInDemoMode){
            companyName = ' "Company Name" ';
        } else if (companiesWithScores.length > 1) {
            for (let i = 0; i < companiesWithScores.length - 1; i++) {
                if (i !== 0) {
                    companyName += ', ';
                }
                companyName += companiesWithScores.companyName;
            }
            companyName += 'and ' + companiesWithScores[companiesWithScores.length - 1].companyName;
            //in case we get single company, we specific one company on the summary.
        } else if (companiesWithScores.length === 1) {
            companyName = companiesWithScores[0].companyName;
        }
    }

    text += '<div>' +
        '<p2>This report presents our risk analysis for ' + companyName + '. The risk assessment covers all\n' +
        companyName + '\'s Internet facing assets as well as any systems and information that could be identified on the' +
        ' Internet/Cloud ("External assets"). The risk score does not cover systems within the company perimeter.\n ' +
        '</p2>' +
        '</div>';
    text += '<br/><br/><br/>';

    if (companiesWithScores && companiesWithScores.length > 0) {
        text += '<div id="page" style="page-break-after: always;">';
        text += '    <h3>Risk Score</h3>';
        let companiesRisksText = '';
        let totalScore = 0;
        if (companiesWithScores && companiesWithScores.length > 0) {
            companiesWithScores.map((score) => {
                totalScore = helpers.calculateTotalScore(score.maxIntelScore, score.surveyScore || 0, score.ratios, score.isSurveyNA);
                let risk = helpers.identifyRiskLevel(totalScore);
                if (displayScoreBackwards) {
                    totalScore = helpers.switchScoreBackwards(totalScore);
                }
                let companyName = isInDemoMode? ' "Company Name" ' : score.companyName;
                if(isWord){
                    companiesRisksText += '<p2>Our analysis indicates that ' + companyName + ' has a: ' +
                        '<span style="color: ' +risk.color + '">' + risk.level + ' </span> Risk Level.</p2>';
                } else {
                    let dotLevel = '<span style="color: ' + risk.color + '; font-size: 30px; position: relative; top:4px; margin-left: 4px"> • </span>';
                    companiesRisksText += '<p2>Our analysis indicates that ' + companyName + ' has a: ' +
                        dotLevel + risk.level + ' Risk Level.</p2>';
                }
            });
        }
        text += companiesRisksText;

        if(isWord){
            text += wordReport.addScoreBoxes(companiesWithScores, displayScoreBackwards, isInDemoMode);
        } else {
            text += '    <br/>';
            text += '    <myCenter>';
            companiesWithScores.map((score) => {
                if (score && score.maxIntelScore != null && score.companyName) {
                    let companyName = isInDemoMode? 'Company Name' : score.companyName;
                    text += this.addScoreBox(companyName, score.maxIntelScore, score.surveyScore || 0, score.ratios, displayScoreBackwards, score.isSurveyNA);
                }
            });
            text += '    </myCenter>';
        }

        text += '    <h3>Risks By Category</h3><br/>';
        text += helpers.createRisksByCategory(companiesWithScores, resultCounts, displayScoreBackwards, isWord);
        text+= '</div><br/>';


        text += helpers.KeyFindingsCreation(companiesWithScores, displayScoreBackwards, isWord, portsByIPCount,
            notConfiguredSPFs, notConfiguredDMARCs, sslCertsCount, allCVEsCount, blacklistsCount, botnetsCount, notConfiguredBuckets);

        let currentCompFindings = {};
        currentCompFindings.ips = resultCounts.allIPsCount;
        currentCompFindings.domainsAndSub = companiesWithScores[0].selectedDomains.length + resultCounts.allSubDomainsCount;
        currentCompFindings.emails = resultCounts.countEmailsBreached;
        currentCompFindings.asns = resultCounts.allASNsCount;
        currentCompFindings.ssl = resultCounts.allSslCertsCount;

        text += '<br/><h3>Summary Of External Assets Discovered</h3>';
        text += pdfHelpers.addRecordsTable([currentCompFindings],
            ['Public IPs', 'Registered domains and subdomains', 'Publicly listed email addresses', 'ASNs', 'SSL Certificates'],
            ASSETS_DISCOVERD);
    }

    return text;
};
