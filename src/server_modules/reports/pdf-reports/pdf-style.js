const helpers = require('../common.js');
const config = require('app-config');

module.exports.getHtmlStyle = () => {
    return '<style>' +
        config.consts.html +
        '#page {' +
        '  color: #000000;' +
        '  font-family: Barlow, sans-serif !important;' +
        '}' +
        '#frontPage {' +
        '  page-break-after: always;' +
        '  background-color: white /*#303030*/;' +
        '  color: #2a2a2a;' + /* '  color: #5B9BD5;' +*/
        '  font-family: Barlow, sans-serif !important;' +
        '}' +
        '#frontPage h1{' +
        '  padding-top: 45%;' +
        '}' +
        '#frontPage logo{' +
        '  top: 400px;' +
        '  position: relative;' +
        '}' +
        '#page h2{' +
        '  width: 100%;' +
        '  font-size: 22px;' +
        '  margin-bottom: 20px;' +
        '  color: #26528d;' +
        '}' +
        '#page h3{' +
        '  font-size: 16px;' +
        '  width: auto;' +
        '  margin-left: 50px;' +
        '  margin-right: 50px;' +
        '  margin-top: 40px;' +
        '  border: 0;' +
        '  padding: 5px 5px 7px 10px;' +
        '  border-radius: 2px;' +
        '  background-color: #efefef;' +
        '  color: rgb(31, 77, 120);' +
        '}' +
        '#page h4{' +
        '  font-size: 14px;' +
        '  padding: 0 0 0 10px;' +
        '  margin-left: 52px;' +
        '  margin-right: 52px;' +
        '}' +
        '#page p2{' +
        '  font-size: 14px;' +
        '  display: block;' +
        '  line-height: 140%;' +
        '  margin: 0 52px;' +
        '  padding: 0 0 0 5px;' +
        '}' +
        '#page strong{' +
        '  font-size: 13px;' +
        '  background-color: #efefef;' +
        '  color: rgb(31, 77, 120);' +
        '  border-radius: 1px;' +
        '  padding: 1px 3px 1px 3px;' +
        '}' +
        '#page table{' +
        '  width: 90.7%;' +
        '  font-size: 11px;' +
        '  text-align: center;' +
        '  margin: auto;' +
        '  border-collapse: collapse;' +
        '  border-radius: 2px;' +
        '}' +
        '#page table td,th{' +
        '  border: 1px solid #ffffff;' +
        '  padding: 5px 8px 5px 12px;' +
        '  background-color: #f7f7f7;' +
        '  font-family: Barlow, sans-serif;' +
        '}' +
        '#page table th{' +
        '  background-color: #e0e0e0;' +
        '}' +
        '#page table td.warning{' +
        '  color: #e22f2f;' +
        '}' +
        'myCenter {' +
        '  margin-bottom: 40px;' +
        '  margin-left: auto;' +
        '  margin-right: auto;' +
        '  display: inherit;' +
        '  text-align: center;' +
        '}' +
        '#frontPage img {' +
        '  height: 100px;' +
        '  width: 210px;' +
        '}' +
        '#lastPage {' +
        'overflow-wrap: normal !important;' +
        'white-space: normal !important;' +
        'margin-bottom: 0;' +
        'padding-bottom: 0;' +
        'visibility: hidden;' +
        '}' +
        '</style>';
};

module.exports.getHtmlCSS = () => {
    return '<link rel="stylesheet" type="text/css" href="' + helpers.getFilePathInitial() + '/resources/report.css">' +
        '<link href="https://fonts.googleapis.com/css?family=Barlow:300" rel="stylesheet">';
};

module.exports.getScoreColor = (score) => {
    if (score == null) {
        return 'black';
    }
    if (score < 3) {
        return '#0091ea';
    }
    if (score >= 3 && score < 7.5) {
        return '#F57C00';
    }
    if (score >= 7.5 && score < 9) {
        return '#F4511E';
    } else {
        return '#e22f2f';
    }
};

module.exports.getScoreColorClass = (score) => {
    if (score == null) {
        return 'dark';
    }
    if (score < 30) {
        return 'low';
    }
    if (score >= 30 && score < 75) {
        return 'medium';
    }
    if (score >= 75 && score < 90) {
        return 'high';
    } else {
        return 'dark';
    }
};

