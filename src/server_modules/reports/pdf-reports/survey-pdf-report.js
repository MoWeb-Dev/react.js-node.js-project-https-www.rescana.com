const pdfStyle = require('./pdf-style');
const pdfHelpers = require('./pdf-common');
const reportHelpers = require('../common');
const Consts = require('../../../../config/consts');
const moment = require('moment');
const TYPE_SURVEY_ANSWER_QUESTION = pdfHelpers.TYPE_SURVEY_ANSWER_QUESTION;
const TYPE_SURVEY_ANSWER_NO_GO_QUESTION = pdfHelpers.TYPE_SURVEY_ANSWER_NO_GO_QUESTION;

// When adding several surveys to the big PDF report, shouldAddFirstPage need to be false (because the big report already has first page).
module.exports.createHTMLForSingleSurvey = (orgId, survey, surveyCompanyName, shouldAddFirstPage = true )=> {
    let text = '';
    if (survey && survey.name && survey.pages && Array.isArray(survey.pages) && survey.pages.length > 0) {
        const isRTL = (survey.lang && survey.lang === Consts.LANG_HE) || false;

        let questionHeaders;
        if (isRTL) {
            questionHeaders = ['סעיף', 'שאלה', 'סטטוס', 'הערה', 'הוכחה 1', 'הוכחה 2', 'הוכחה 3'];
        } else {
            questionHeaders = ['Section', 'Question', 'Status', 'Comment', 'Evidence A', 'Evidence B', 'Evidence C'];
        }

        if (shouldAddFirstPage) {
            text = '<!DOCTYPE html><html><head>';
            text += pdfStyle.getHtmlStyle() + pdfStyle.getHtmlCSS();
            text += '</head><body>';

            text += pdfHelpers.addFrontPage('Risk Survey Report', survey.name, isRTL);
        }
        survey.pages.map((currPage, pageIndex) => {
            if (currPage && currPage.name && currPage.elements && Array.isArray(currPage.elements) && currPage.elements.length > 0) {
                let currDivNewPage = '<div id="page"';
                if (isRTL) {
                    currDivNewPage += ' dir="rtl"';
                }
                currDivNewPage += '>';

                text += currDivNewPage;

                if (pageIndex === 0) {
                    text += this.addSurveyIntroBox(survey, surveyCompanyName);

                    const importantQuestions = reportHelpers.getSurveyImportantQuestions(survey);
                    if (importantQuestions && importantQuestions.length > 0) {
                        text += this.addSurveyExecutiveSummary(importantQuestions, isRTL);
                        text += '</div>';
                        text += currDivNewPage;
                    }

                    //Create No Go questions tab only if company related to organization and is unsafe
                    if (orgId && orgId !== 'No Id' && survey.orgId && orgId === survey.orgId && survey.isUnsafeCompany && survey.noGoAnsweredWrongArr &&
                        Array.isArray(survey.noGoAnsweredWrongArr) && survey.noGoAnsweredWrongArr.length > 0) {

                        let columnHeaders = [];
                        if (isRTL) {
                            text += '<myCenter><h2>שאלות קריטיות</h2></myCenter>';
                            columnHeaders = columnHeaders.concat(['שאלות קריטיות', 'התשובה נכונה', 'תשובת הספק', 'תגובת הספק']);
                        } else {
                            text += '<myCenter><h2>Critical Questions</h2></myCenter>';
                            columnHeaders = columnHeaders.concat(['Critical Questions', 'Correct Answer', 'Vendor Answer', 'Vendor Comment']);
                        }

                        text += '<div id="page"';
                        if (isRTL) {
                            text += ' dir="rtl"';
                        }
                        text += '>';

                        // Using addRecordsTable for questions (currCategory.elements).
                        text += pdfHelpers.addRecordsTable(survey.noGoAnsweredWrongArr, columnHeaders, TYPE_SURVEY_ANSWER_NO_GO_QUESTION);
                        text += '</div>';
                    }
                }

                text += '<myCenter><h2>' + currPage.name.toUpperCase() + '</h2></myCenter>';

                currPage.elements.map((currCategory, catIndex) => {
                    // check if props exists and if the category is a follow up/nested type and if its shown(activated on the survey)
                    if (currCategory && currCategory.name && currCategory.elements && Array.isArray(currCategory.elements) && currCategory.elements.length > 0 &&
                        (!currCategory.followUpCategoryData || (currCategory.followUpCategoryData && currCategory.followUpCategoryData.isShown))) {
                        text += '<h3>' + (catIndex + 1) + ' - ' + currCategory.name.toUpperCase() + '</h3>';

                        text += '<div';
                        if (isRTL) {
                            text += ' dir="rtl"';
                        }
                        text += '>';

                        // Using addRecordsTable for questions (currCategory.elements).
                        text += pdfHelpers.addRecordsTable(currCategory.elements, questionHeaders, TYPE_SURVEY_ANSWER_QUESTION, catIndex + 1);

                        text += '</div>';
                    }
                });

                text += '</div>';
            }
        });

        if (shouldAddFirstPage) {
            text += '</body></html>';
        }
    }

    return text;
};

module.exports.addSurveyIntroBox = (survey, surveyCompanyName) => {
    if (!survey || !survey.name || !survey.respondent || !survey.lastChangedDate || !surveyCompanyName) {
        return '';
    } else {
        let surveyHeader, sName, sCompName, sRespondent, sLastChanged, sUnsafeCompany;
        let unsafeText = '';

        if (survey.lang && survey.lang === Consts.LANG_HE) {
            surveyHeader = 'פרטי הסקר';
            sName = 'שם';
            sCompName = 'החברה הנסקרת';
            sRespondent = 'ממלא השאלון';
            sLastChanged = 'תאריך עדכון אחרון';
            sUnsafeCompany = 'סטטוס הספק';

            if(survey.isUnsafeCompany){
                let explanation = 'הספק לא ענה על שאלות החובה על פי דרישות החברה.';
                unsafeText = '<p style="line-height: 10px;"><b>' + sUnsafeCompany + '</b> .הספק לא עמד בדרישות האבטחה של החברה :</p>\n' +
                    '<p style="line-height: 10px; font-size: 12px; color: #424242">' + explanation  + '</p>\n';
            }
        } else {
            surveyHeader = 'Survey Details';
            sName = 'Name';
            sCompName = 'Company Being Surveyed';
            sRespondent = 'Respondent';
            sLastChanged = 'Last Changed Date';
            sUnsafeCompany = 'Vendor Status';

            if(survey.isUnsafeCompany){
                let explanation = 'Company didn\'t answer obligatory questions according to ' + surveyCompanyName + '\'s requirements.';
                unsafeText = '<p style="line-height: 10px;"><b>' + sUnsafeCompany + '</b>: Company didn\'t meet  ' + surveyCompanyName + '\'s security requirements. </p>\n' +
                    '<p style="line-height: 10px; font-size: 12px; color: #424242">' + explanation  + '</p>\n';
            }
        }


        const dateInFormat = moment(survey.lastChangedDate).format('DD/MM/YYYY, HH:mm:ss');

        return '<div style="box-shadow: 0px 0px 5px 1px #888888; color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 52px;">\n' +
            '<div style="box-sizing: border-box; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); background-color: rgb(232, 232, 232); height: 56px; padding: 0px 24px; display: flex; justify-content: space-between;">\n' +
            '<div style="position: relative; display: flex; justify-content: space-between; align-items: left;">\n' +
            '<h2 style="padding-right: 16px; line-height: 56px; font-size: 17px; font-family: Roboto, sans-serif; position: relative; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">' +
            surveyHeader +
            '</h2></div></div>' +
            '<div class="markdown-body" style="margin-top: 0px; font-size: 13px; margin-bottom: 0px; padding: 10px 20px 0px; background: rgb(255, 255, 255); overflow: auto; align-items: left;">\n' +
            '<p style="line-height: 10px;">' + sName + ': ' + survey.name + '</p>\n' +
            '<p style="line-height: 10px;">' + sCompName + ': ' + surveyCompanyName + '</p>\n' +
            '<p style="line-height: 10px;">' + sRespondent + ': ' + survey.respondent + '</p>\n' +
            '<p style="line-height: 10px;">' + sLastChanged + ': ' + dateInFormat + '</p>\n' +
            unsafeText +
            '</div>\n' +
            '</div>';
    }
};

module.exports.addSurveyExecutiveSummary = (surveyImportants, isRTL) => {
    if (!surveyImportants || !Array.isArray(surveyImportants) || surveyImportants.length < 1) {
        return '';
    } else {
        let executiveSummaryTitle, questionHeaders;
        if (isRTL) {
            executiveSummaryTitle = 'שאלות שסומנו כחשובות';
            questionHeaders = ['שאלה', 'סטטוס', 'הערה', 'הוכחה 1', 'הוכחה 2', 'הוכחה 3'];
        } else {
            executiveSummaryTitle = 'Important Questions';
            questionHeaders = ['Question', 'Status', 'Comment', 'Evidence A', 'Evidence B', 'Evidence C'];
        }

        let text = '<myCenter><h2>' + executiveSummaryTitle + '</h2></myCenter>';

        text += pdfHelpers.addRecordsTable(surveyImportants, questionHeaders, pdfHelpers.TYPE_SURVEY_ANSWER_QUESTION);

        return text;
    }
};

module.exports.getSurveyCompanyNameByID = (companyID, act) => {
    return act('role:companies, get:company', {id: companyID})
        .then((res) => {
            if (res && res.companyName) {
                return ({ok: true, companyName: res.companyName});
            } else {
                return ({ok: false, error: 'No surveyWithAnswers found.'});
            }
        })
        .catch((e) => {
            return ({ok: false, error: e});
        });
};
