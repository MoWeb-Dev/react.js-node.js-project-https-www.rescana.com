const fs = require('fs');


// function to encode file data to base64 encoded string
module.exports.to_base64_encode = (file) => {
    // read binary data
    const bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
};

module.exports.getFilePathInitial = (shouldBeFile = false) => {
    // Getting absolute path in current syntax.
    const dir = __dirname.replace(new RegExp(/\\/g), '/');
    // Check whether the initial file should be added.
    const prePath = (shouldBeFile) ? 'file:///' : '';
    // Moving to main folder.
    return prePath + dir;
};

// This function completes all left space of frontPage in rowBreaks.
module.exports.completeBlankEndOfPage = (countNamesRowsAdded, rowBreak) => {
    let result = '';

    const numOfRowToAdd = 21 - countNamesRowsAdded;

    for (let i = 0; i < numOfRowToAdd; i++) {
        result += rowBreak;
    }

    return result;
};
