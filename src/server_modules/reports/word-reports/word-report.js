const htmlDocx = require('html-docx-js');
const helpers = require('../common.js');
const moment = require('moment');
const config = require('app-config');
const wordHelpers = require('./word-helpers.js');
const pdfReport = require('../pdf-reports/pdf-report.js');
const pdfHelpers = require('../pdf-reports/pdf-common.js');
const {getScoreColor} = require('../pdf-reports/pdf-style.js');
const { Readable } = require('stream');

const pageMargin = 'margin: 60px 0px 0px 0px;';
const pageMargin2 = 'margin: 60px 0px 0px 0px;';
const pageBreak = '<p/><br style="page-break-before: always; clear: both" />';
const rowBreak = '<br style="page-break-before: avoid; page-break-after: avoid;"/>';
const font = 'Calibri Light';
const divPageStyle = 'style="' +
    pageMargin +
    'font-family: ' + font + ';' +
    '"';

const divPageStyle2 = 'style="' +
    pageMargin2 +
    'font-family: ' + font + ';' +
    '"';

module.exports.createWORD = (user, htmlObj, context, reportKind) => {
    return new Promise((resolve) => {
        const options = {
            orientation: 'portrait',
            margins: {top: 1000, right: 800, bottom: 1440, left: 800},
        };

        try {
            const convertedBuffer = htmlDocx.asBlob(htmlObj, options);
            const stream = bufferToStream(convertedBuffer);

            return helpers.saveStreamToDB(stream, helpers.WORD_TYPE, user, context, reportKind)
                .then((res) => {
                    resolve(res);
                })
                .catch((e) => {
                    console.log('Error in createWORD-saveStreamToDB: ', e);
                    resolve({ok: false, err: e});
                });
        } catch (e) {
            console.log('Error in createWORD-htmlDocx.asBlob: ', e);
            resolve({ok: false, err: e});
        }
    });
};

module.exports.createHTML = (isInDemoMode = false, data , companyTitle, displayScoreBackwards, manualData, reportKind, logo, isWord) => {
    let htmlObj = '<!DOCTYPE html><html><head>' +
        getHtmlStyle() +
        getHtmlCSS() +
        '</head><body>';
    companyTitle = isInDemoMode? 'Company Name' : companyTitle;
    if (reportKind === 'intelReport') {

        htmlObj += addFrontPage('Risk Intelligence Report Of', companyTitle, logo) +
            pageBreak +
            addDataPages(data, displayScoreBackwards, reportKind, isInDemoMode) +
            addManualDataPages(manualData);

    } else if (reportKind === 'surveyReport') {
        htmlObj += addFrontPage('Surveys Status Reports', companyTitle) +
            addDataPages(data, displayScoreBackwards, reportKind, isInDemoMode) +
            addManualDataPages(manualData);
    }

    htmlObj += '</body></html>';
    return (htmlObj);
};

const getHtmlStyle = () => {
    return '<style>' +
        config.consts.html +
        '#page {' +
        '  page-break-after: always;' +
        '  font-family: ' + font + ' !important;' +
        '  font-size: 14px;' +
        '  font-weight: normal;' +
        '}' +
        '#frontPage {' +
        '  page-break-after: always;' +
        '  font-weight: normal;' +
        '  background-color: #303030;' +
        '  color: white;' + /* '  color: #5B9BD5;' +*/
        '  text-shadow: 0 0 5px #000000;' +
        '  font-family: ' + font + ' !important;' +
        '}' +
        '#frontPage h1{' +
        '  font-weight: normal;' +
        '}' +
        '#frontPage logo{' +
        '  top: 400px;' +
        '  position: relative;' +
        '}' +
        '#page h2{' +
        '  width: 100%;' +
        '  font-size: 18px;' +
        '  color: #26528d;' +
        '  margin-bottom: 20px;' +
        '  font-weight: normal;' +
        '}' +
        '#page h3{' +
        '  font-size: 14px;' +
        '  width: auto;' +
        '  margin-top: 40px;' +
        '  border-left: 5px solid #efefef;' +
        '  border-bottom: 2px solid #efefef;' +
        '  border-top: 2px solid #efefef;' +
        '  border-radius: 2px;' +
        '  background-color: #efefef;' +
        '  color: rgb(31, 77, 120);' +
        '  font-weight: normal;' +
        '}' +
        '#page h4{' +
        '  font-size: 14px;' +
        '  padding: 0 0 0 10px;' +
        '  font-weight: normal;' +
        '  font-weight: normal;' +
        '}' +
        '#page strong{' +
        '  background-color: #efefef;' +
        '  color: rgb(31, 77, 120);' +
        '  font-weight: normal;' +
        '}' +
        '#page p2{' +
        '  font-size: 12px;' +
        '  display: block;' +
        '  line-height: 140%;' +
        '  margin: 0 52px;' +
        '  padding: 0 0 0 5px;' +
        '  font-weight: normal;' +
        '}' +
        '#page b{' +
        '  font-weight: normal;' +
        '}' +
        '#page table{' +
        '  width: 98%;' +
        '  color: #1f1f1f;' +
        '  font-size: 12px;' +
        '  text-align: center;' +
        '  margin-left: 7px;' +
        '  margin-right: 7px;' +
        '  border-collapse: collapse;' +
        '  font-family: ' + font + ' !important;' +
        '}' +
        '#page table td,th{' +
        '  border: 1px solid #ffffff;' +
        '  font-family: ' + font + ' !important;' +
        '  page-break-inside: avoid;' +
        '  page-break-before: avoid;' +
        '  background-color: #f7f7f7;' +
        '  padding: 3px 8px 3px 12px;' +
        '}'
        +'#page table th{' +
        '  background-color: #ececec;' +
        '  font-weight: normal;' +
        '}' +
        '#page2 table{' +
        '  width: 22%;' +
        '  margin-left: 50px;' +
        '  margin-right: 50px;' +
        '  font-family: ' + font + ' !important;' +
        '}' +
        '#page2 table td,th{' +
        '  border: 1px solid #ffffff;' +
        '  padding: 5px 8px 5px 12px;' +
        '  font-family: ' + font + ' ' +
        '  page-break-inside: avoid;' +
        '  page-break-before: avoid;' +
        '  background-color: #f7f7f7;' +
        '}' +
        'myCenter {' +
        '  margin-bottom: 40px;' +
        '  margin-left: auto;' +
        '  margin-right: auto;' +
        '  display: inherit;' +
        '  text-align: center;' +
        '  font-weight: normal;' +
        '}' +
        '#frontPage img {' +
        '  height: 100px;' +
        '  width: 210px;' +
        '}' +
        '</style>';
};

const getHtmlCSS = () => {
    return '<link rel="stylesheet" type="text/css" href="' + wordHelpers.getFilePathInitial() + '/resources/report.css">'+
        '<link href="https://fonts.googleapis.com/css?family=Barlow:200" rel="stylesheet">';
};

const addFrontPage = (header, names, logo) => {
    const frontPageDivStyle = 'style="margin-left: auto; margin-right: auto; display: inherit; text-align: center;' +
        'background-color: white; color: #2a2a2a; font-family: ' + font + ';' +
        '"';
    let text = '<div id="frontPage" ' + frontPageDivStyle + '>';

    if (header) {
        // This variable counts how many rows are added for all names- to balance empty rows afterwards.
        let countNamesRowsAdded;

        let namesArray;
        if (names) {
            if (!Array.isArray(names)) {
                namesArray = [names];
            } else {
                namesArray = names;
            }
            countNamesRowsAdded = 1;
        } else {
            namesArray = [];

            countNamesRowsAdded = 0;
        }

        // if(logo.path && logo) {
        //     let orgLogo = wordHelpers.getFilePathInitial() + '/orgLogo' + logo.path;
        //     orgLogo = 'data:image/png;base64,' + wordHelpers.to_base64_encode(orgLogo);
        //     text += '<myCenter><logo><img style=" top: 400px; position: relative; width: initial; height: 100px" src="' + orgLogo + '"></logo>';
        // }
        const today = moment().format('LL');
        text += '<myCenter>';
        text += rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak;
        text += '<h1 style="font-family: ' + font + ' !important;">' + header + '</h1>';
        text += '<h3>';
        for (let i = 0; i < namesArray.length; i++) {
            text += namesArray[i];
            if (i < namesArray.length - 1) {
                text += ', ';
                if ((i + 1) % 5 === 0) {
                    text += '<br/>';
                    countNamesRowsAdded++;
                }
            }
        }
        text += '</h3>';
        let logoSrc = wordHelpers.getFilePathInitial() + '/resources/rescana_logo_2.png';
        logoSrc = 'data:image/png;base64,' + wordHelpers.to_base64_encode(logoSrc);
        text += '<br/><img width="15%" height="10%" src="' + logoSrc + '">';
        text += '<br/><b>' + today + '</b>';
        //text += wordHelpers.completeBlankEndOfPage(countNamesRowsAdded, rowBreak);
        text += '</myCenter>';
    }
    text += '</div>';

    return text;
};

const addExecutiveSummary = (data, displayScoreBackwards, isWord, isInDemoMode) => {
    let text = '<div id="page" ' + divPageStyle + '>' +
        '<div style="display: inherit; text-align: center">' +
        rowBreak +
        '<h2 style="font-family: ' + font + ' !important;">EXECUTIVE SUMMARY</h2>' +
        '</div>';
    text += pdfReport.addExecutiveSummary(data.resultCounts, data.companiesWithScores, displayScoreBackwards, isWord, isInDemoMode);
    text += '</div>';
    return text;
};

const addDataPages = (data, displayScoreBackwards, reportKind, isInDemoMode) => {

    if(isInDemoMode){
        data = pdfReport.updateAllReportDataToDemoModeData(data);
    }

    let companies = data.companies;
    companies.map((curComp) => {
        if(curComp.selectedDomains && Array.isArray(curComp.selectedDomains)){
            curComp.selectedDomains.push("no_domain");
        } else {
            curComp.selectedDomains = [];
            curComp.selectedDomains.push("no_domain");
        }
    });

    if (reportKind === 'intelReport') {


        let locationsImg = data.imgFilename,
            scores = data.companiesWithScores,
            cveVulns = data.vulnsByIP,
            allASNs = data.allASNs,
            allBuckets = data.allBuckets,
            allSPFs = data.allSPFs,
            allDMARCs = data.allDMARCs,
            allDataleaks = data.allDataleaks,
            allEmailBreaches = data.allEmailBreaches,
            allBlacklists = data.allBlacklists,
            allBotnets = data.allBotnets,
            allSSLCerts = data.allSSLCerts,
            allGeoLocations = data.allGeoLocations,
            resultCounts = data.resultCounts,
            allAmData = data.allAmData;

        let amAssets = pdfReport.createSeparatedAssetObjectsFromAmData(allAmData);
        let criticalImportanceAssets = amAssets.criticalImportanceAssets;
        let highImportanceAssets = amAssets.highImportanceAssets;
        let mitigatedAssets = amAssets.mitigatedAssets;
        let fpAssets = amAssets.fpAssets;

            resultCounts.allSubDomainsCount = data.allSubDomains.length;
        let allScoresData = scores[0].scoresData;

        return addExecutiveSummary(data, displayScoreBackwards, true, isInDemoMode) +
            addReportDetails(resultCounts, scores, locationsImg, allGeoLocations, isInDemoMode) +
            addCVEVulns(cveVulns, companies, displayScoreBackwards, allScoresData) +
            addBuckets(allBuckets, companies, displayScoreBackwards, allScoresData) +
            addDMARCandSPFs(allDMARCs, allSPFs, companies, resultCounts, displayScoreBackwards, allScoresData) +
            addEmailBreaches(allEmailBreaches, companies, displayScoreBackwards, allScoresData) +
            addDataleaks(allDataleaks, companies, displayScoreBackwards, allScoresData) +
            addBlacklists(allBlacklists, companies, displayScoreBackwards, allScoresData) +
            addBotnets(allBotnets, companies, displayScoreBackwards, allScoresData) +
            addSSLCerts(allSSLCerts, companies, displayScoreBackwards, allScoresData) +
            addCriticalImportanceAssets(criticalImportanceAssets, companies) +
            addHighImportanceAssets(highImportanceAssets, companies) +
            addMitigatedAssets(mitigatedAssets, companies) +
            addAppendix(allBuckets, allSPFs, allDMARCs, allSSLCerts, allASNs, fpAssets, companies);

    } else if (reportKind === 'surveyReport') {
        return addSurveysInfo(companies);
    }
};

const addCriticalImportanceAssets = (criticalImportanceAssets, companies) => {
    let assetTitle = 'CRITICAL FINDINGS';
    let assetExplanation = 'The following were marked manually as critical assets and the company\'s risk score was set accordingly. ' +
        'When finding found on a critical asset the category risk score is set to the highest possible score.';


    return addAsset(assetTitle, assetExplanation, criticalImportanceAssets, companies);
};

const addHighImportanceAssets = (highImportanceAssets, companies) => {
    let assetTitle = 'HIGH IMPORTANCE FINDINGS';
    let assetExplanation = 'The following were marked manually as high importance assets and have a high impact on ' +
        'the risk assessment calculation.';

    return addAsset(assetTitle, assetExplanation, highImportanceAssets, companies);
};

const addMitigatedAssets = (mitigatedAssets, companies) => {
    let assetTitle = 'FINDINGS MARKED AS MITIGATED';
    let assetExplanation = 'The following were marked manually as mitigated and ' +
        'were not included in the risk assessment calculation.';

    return addAsset(assetTitle, assetExplanation, mitigatedAssets, companies);
};

const addAsset = (assetTitle, assetExplanation, assetData, companies) => {
    let text = '';
    let style = 'style="margin-left: auto; margin-right: auto; display: inherit; text-align: center;"';
    if (assetTitle && assetExplanation && assetData && Object.getOwnPropertyNames(assetData).length > 0 && companies) {
        text += pageBreak;
        text += '<div id="page" style="page-break-after: always;">';
        text += '<myCenter><h2 ' + style +'>'+ assetTitle +'</h2></myCenter>';
        text += '<p2>' + assetExplanation + '</p2>';
        Object.keys(assetData).sort().map((currKey) => {
            if(assetData[currKey] && Array.isArray(assetData[currKey]) && assetData[currKey].length > 0){
                text += pdfReport.addGenericTablesByArrOfObj(assetData[currKey], currKey, companies);
            }
        });
        text += '</div>';
    }
    return text;
};

const addAppendix = (allBuckets, allSPFs, allDMARCs, allSSLCerts, allASNs, fpAssets, companies) => {
    let text = '';
    if(allBuckets.length > 0 || allSPFs.length > 0 || allDMARCs.length > 0 || allSSLCerts.length > 0 || allASNs.length > 0 || Object.getOwnPropertyNames(fpAssets).length > 0){
        const frontPageDivStyle = 'style="margin-left: auto; margin-right: auto; display: inherit; text-align: center;' +
            'background-color: white; color: #2a2a2a;  font-family: ' + font + ';' +
            '"';
        text += pageBreak;
        text += '<div id="page" '+ frontPageDivStyle +'">';
        text += rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak;
        text += rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak;
        text += rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak;
        text += '<myCenter><h2 style="font-size: 30px;">APPENDIX</h2></myCenter>';
        text += '</div>';
        text += pdfReport.addAppendix(allBuckets, allSPFs, allDMARCs, allSSLCerts, allASNs, fpAssets, companies, true, {pageBreak: pageBreak, rowBreak: rowBreak});
    }

    return text;
};

const addReportDetails = (resultCounts, companiesWithScores, locationsImg, allGeoLocations, isInDemoMode) => {

    let text = '';

    let companyName = 'the chosen companies';
    let companyString = 'those companies';

    //in case we get multiple companies to the report, we dont want to specific one on the summary.
    if (companiesWithScores && Array.isArray(companiesWithScores) && companiesWithScores.length > 0) {
        companyName = '';
        companyString = '';
        if(isInDemoMode){
            companyName = ' "Company Name" ';
        } else if (companiesWithScores.length > 1) {
            for (let i = 0; i < companiesWithScores.length - 1; i++) {
                if (i !== 0) {
                    companyName += ', ';
                }
                companyName += companiesWithScores.companyName;
            }
            companyName += 'and ' + companiesWithScores[companiesWithScores.length - 1].companyName;
            companyString = 'those companies';
            //in case we get single company, we specific one company on the summary.
        } else if (companiesWithScores.length === 1) {
            companyName = companiesWithScores[0].companyName;
            companyString = companyName + '\'s';
        }
    }

    text += pageBreak;

    text += '<div id="page" style="page-break-after: always;' + divPageStyle2 + '">';
    text += '    <div style="text-align: center;">';
    text += '      <h2>REPORT DETAILS</h2>';
    text += '    </div>';

    text += '<div>' +
        '<p2>The following report provides an in-depth OSINT based risk analysis of ' + companyName + ', as well as a\n' +
        'summary of scores from any surveys completed by ' + companyName + '. OSINT stands for open source\n' +
        'intelligence, which is the process of collecting large quantities of free-flowing information,\n' +
        'analyzing and correlating this data in order to reach certain assumptions.\n' +
        'The purpose of our analysis is to provide information about ' + companyString + ' Internet facing assets and the\n' +
        'risks they might currently sustain. This is done in three phases, the "Discovery" phase, the\n' +
        '"Enrichment" phase and the scoring phase. During the discovery phase, our system uses our\n' +
        '"smart traversal" algorithm, to find the connections from each asset to another, ultimately\n' +
        'discovering the entire network of assets. In the enrichment phase, we take all the information\n' +
        'from the discovery phase, and cross reference it with security data feeds, and our own best\n' +
        'practice rules. Finally, the last step takes all the gathered information from the first two phases' +
        ' and the surveys then calculates a risk score according to our methodology and configured weights. </p2>\n' +
        '</div>';


    // text += (locationsImg) ? pageBreak +
    //     '<div id="page" ' + divPageStyle + '>' +
    //     '<div style="display: inherit; text-align: center">' +
    //     rowBreak +
    //     '<h2 style="font-family: ' + font + ' !important;">ASSETS LOCATION</h2>' +
    //     '<p2>The following is a geographical analysis of all assets.' +
    //     ' This may be used for GDPR compliance purposes or general reference.\n</p2>' +
    //     '<h3>Map of assets</h3>' +
    //     '</div>' +
    //     '<div>' +
    //     '<img src="' + locationsImg + '" style="margin: 15px 0px;">' +
    //     '</div>' +
    //     '</div>' : '';

    text += '</div>';

    if (locationsImg) {
        text += '<div id="page" style="page-break-after: always;' + divPageStyle2 + '">';
        text += '    <h3>Assets Location</h3>';
        text += '    <p2>The following is a geographical display of ' + companyName + '\'s assets. Our experience indicates that\n' +
            ' in many cases even for very well organized companies it is possible to identify unknown resources in unexpected\n' +
            'geographies.  This is true mainly for larger multinational companies where local offices take initiatives\n' +
            ' that are not always known to the security/compliance teams. \n</p2>';

        text += '<h3>Recommendations</h3>' +
            '<p2>We find it important to scan the map and identify surprising asset locations. We also find this map ' +
            'useful for assets geographical distribution when considering GDPR compliance.\n</p2>';
        text += pageBreak;

        text += '    <h3>Map of assets</h3>';
        text += '    <myCenter>';
        text += '    <img alt="" src="' + locationsImg + '" style="width: 650px; height: 650px">';
        text += '    </myCenter>';

        if (allGeoLocations && allGeoLocations.length > 0) {
            text += '    <h3>IP/Geolocation Table</h3>';
            text += pdfHelpers.addRecordsTable(allGeoLocations,
                ['Country', 'Assets'],
                pdfReport.TYPE_GEO_LOCATIONS);
        }
        text += '    <br/>';
        text += '</div>';
    }

    return text;
};

const addSurveysInfo = (companies) => {
    let text = '<div id="page" ' + divPageStyle2 + '>' +
        '<div style="display: inherit; text-align: center">' +
        rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak + rowBreak +
        '<h2 style="font-family: ' + font + ' !important;">EXECUTIVE SUMMARY</h2>' +
        'This report provides analysis, status, and visualization of the company vendors. It can be used' +
        ' to keep track and manage the surveys in accordance with the company\'s security needs.' +
        '</div>';

    // Create table for Surveys Info.
    text += addGenericPage(companies, companies, 'VENDORS', pdfReport.TYPE_SURVEY, '');
    return text;
};

const addScoreBoxes = (scores, displayScoreBackwards, isInDemoMode) => {
    let text = '';
    if (scores && scores.length > 0) {
        scores.map((score) => {
            if (score && score.maxIntelScore != null && score.companyName) {
                let companyName = isInDemoMode? 'Company Name' : score.companyName;
                text += addScoreBox(companyName, score.maxIntelScore, score.surveyScore || 0, score.ratios, displayScoreBackwards, score.isSurveyNA);
            }
        });
    } else {
        text = '';
    }
    return text;
};

module.exports.addScoreBoxes = addScoreBoxes;

const addScoreBox = (name, maxIntelScore, surveyScore, ratios, displayScoreBackwards, isSurveyNA) => {
    // Check if maxIntelScore is a number. (maxIntelScore == null means null or undefined)
    if (maxIntelScore == null || isNaN(parseFloat(maxIntelScore)) || !isFinite(maxIntelScore) || !name || !name.trim()) {
        return '';
    } else {
        let maxIntelScoreSpan = '', surveyScoreSpan = '';

        const greySpanStyle = ' style="color: rgb(111, 111, 111); font-size: 12px;" ';

        if (surveyScore != null && !isNaN(surveyScore) && isFinite(surveyScore)) {
            // isSurveyNA is just to identify if the survey exists or not, fixed afterwards to 0 for further calculations.
            if (isSurveyNA) {
                surveyScore = 0;
            } else {
                let surveyRiskScore = surveyScore;
                if (surveyRiskScore > 0) {
                    if (displayScoreBackwards) {
                        surveyRiskScore = helpers.switchScoreBackwards(surveyRiskScore);
                    }
                    surveyScoreSpan = '<span' + greySpanStyle + '> Survey Score: ' + surveyRiskScore + '</span>';
                }
            }
        } else {
            surveyScore = 0;
        }

        let totalScore = helpers.calculateTotalScore(maxIntelScore, surveyScore, ratios, isSurveyNA);

        if (totalScore === 0) {
            maxIntelScoreSpan = '<span' + greySpanStyle + '>Low Risk</span>';

            // Reset surveyScoreSpan so only 'Low Risk' message is displayed in case of totalScore being 0.
            surveyScoreSpan = '';
        } else {
            let maxIntelRiskScore = maxIntelScore;

            if (displayScoreBackwards) {
                maxIntelRiskScore = helpers.switchScoreBackwards(maxIntelRiskScore);
            }

            maxIntelScoreSpan = '<span' + greySpanStyle + '>Intel Score: ' + maxIntelRiskScore + '</span>';
        }

        const scoreColor = getScoreColor(totalScore / 10);

        if (displayScoreBackwards) {
            totalScore = helpers.switchScoreBackwards(totalScore);
        }

        const myCenter = 'style="margin-left: auto; margin-right: auto; display: inherit; text-align: center; font-size: 15px;"';

        // Should look like: <div class="c100 p25 low"> or <div class="c100 p99 high">
        let text = '<br/><br/><div id="page2" ' + myCenter + '>' +
            '<table><tr><td ' + myCenter + '>' +
            '      <span ' + myCenter + '><span style="color: rgb(55,55,55)">' + name + '</span></span>\n' +
            '</td></tr><tr><td' + myCenter + '>' +
            '         <span style="margin: 10px; font-size: 60px; color: ' + scoreColor + ';">' + totalScore + '</span>\n' +
            '</td></tr><tr><td' + myCenter + '><!--<div>-->' +
            maxIntelScoreSpan +
            '<br>' +
            surveyScoreSpan +
            '</td></tr></table></div>';

        return text;
    }
};

const addCVEVulns = (cveVulns, companies, displayScoreBackwards, allScoresData) => {
    let text;
    if (cveVulns && cveVulns.length > 0 && companies && companies.length > 0) {
        if(cveVulns.length > 5){
            cveVulns = cveVulns.splice(0 ,5);
        }
        let scoreImg = pdfReport.createScoreImg(allScoresData.countCVEs.score, displayScoreBackwards, true);
        text = pageBreak +
            '<div id="page" ' + divPageStyle + '>' +
            '<div style="display: inherit; text-align: center">' +
            rowBreak +
            '<myCenter><h2>UNPATCHED SYSTEMS (CVE) ' + scoreImg + '</h2></myCenter>' +
        '</div>';
        text += '<p2>This analysis module is looking for systems that are running vulnerable software.\n' +
            ' Hackers look for vulnerable systems open from the Internet in their attempts to compromise \n' +
            'the systems. The systems maybe vulnerable for 2 primary reasons a) released patched were \n' +
            'not implemented or b) the systems are end of life versions and were not replaced by supported versions.' + '<br/>' +
            'CVE (<a style="color: dodgerblue;" href="https://cve.mitre.org/"> https://cve.mitre.org/</a>) ' +
            'is well known list of publicly knowns cybersecurity vulnerabilities.\n' +
            ' Once we identify a company asset we search the CVE list to identify if the software running \n' +
            'contains any known vulnerabilities. When found we present the vulnerability details. \n</p2>';

           text += '<h3>CVE\'s Highlights</h3>';
        // shouldStartNewPage will be true from second company with CVEs and on.
        let shouldStartNewPage = false;
        companies.map((currComp) => {
            if (currComp && currComp.hasCVEVulns && currComp.id && currComp.companyName) {
                if (shouldStartNewPage) {
                    text += pageBreak + '</div><div id="page" ' + divPageStyle + '>';
                }
                text += '<p2>Discovered on ' + currComp.companyName + '</p2><br/>';
                cveVulns.map((currVuln) => {
                    if (currVuln && currVuln.relatedCompanies && currVuln.relatedCompanies.includes(currComp.id)) {
                        text += addCVECard(currVuln);
                    }
                });
                shouldStartNewPage = true;
            }
        });
        text += '<h3>Recommendations</h3>' +
            '<p2>Unpatched systems can be source of critical risk and should be considered seriously.\n </p2>';
        text += '</div>';
    } else {
        text = '';
    }
    return text;
};

const addCVECard = (cveVuln) => {
    if (!cveVuln || !cveVuln.domains || cveVuln.domains.length < 1 || !cveVuln.ip || !cveVuln.port || !cveVuln.cpe || !cveVuln.cve) {
        return '';
    }
    let cvePreHeader, cveColor, cveScoreColor;
    if (cveVuln.cveScore) {
        cveScoreColor = getScoreColor(cveVuln.cveScore);
        if (cveVuln.cveScore >= 7.5 && cveVuln.cveScore < 9.0) {
            cvePreHeader = 'High | ';
            cveColor = '#F4511E';
        } else if(cveVuln.cveScore >= 9.0) {
            cvePreHeader = 'Critical | ';
            cveColor = '#e22f2f';
        } else {
            cvePreHeader = '';
            cveColor = 'grey';
        }
    } else {
        cvePreHeader = '';
        cveColor = 'grey';
        cveScoreColor = 'black';
    }
    let relatedDomains = (cveVuln.domains.length === 1) ? '<b>Domain</b>: ' : '<b>Domains</b>: ';
    for (let i = 0; i < cveVuln.domains.length; i++) {
        relatedDomains += cveVuln.domains[i];
        if (i < cveVuln.domains.length - 1) {
            relatedDomains += ', ';
        }
    }
    const cpeArr = cveVuln.cpe.split(':');
    let vulnCpe;
    if (cpeArr.length >= 3) {
        vulnCpe = cpeArr[cpeArr.length - 3] + ' ' + cpeArr[cpeArr.length - 2] + ' ' + cpeArr[cpeArr.length - 1];
    } else {
        vulnCpe = 'None';
    }
    let scoreAndSummary = '';
    if (cveVuln.cveScore != null || cveVuln.cveSummary) {
        scoreAndSummary = '<div style=" background-color: rgb(247,247,247); padding: 0px 20px;">' +
            '<div style="box-sizing: border-box; content: \' \'; display: table;"></div>' +
            '<div>';

        if (cveVuln.cveScore != null) {
            scoreAndSummary += '<div><div style="box-sizing: border-box; position: relative; white-space: nowrap; cursor: pointer;">' +
                '<div style="display: inline-flex; vertical-align: left; white-space: normal; margin-bottom: 0px; padding-right: 90px;">' +
                '<span style="display: inline; font-size: 11px;">CVSS: </span>' +
                '<span style="color: ' + cveScoreColor + '; display: inline; font-size: 11px;">' +
                cveVuln.cveScore +
                '</span></div>' +
                '</div>';
        }
        if (cveVuln.cveSummary) {
            scoreAndSummary += '<div style="position: relative; border-top: 5px solid rgb(247,247,247);">' +
                '<span style="color: rgb(93,93,93); display: block; font-size: 9px; margin-top: 0px;">' +
                cveVuln.cveSummary +
                '</span></div>';
        }
        scoreAndSummary += '</div></div><div style="box-sizing: border-box; content: \' \'; clear: both; display: table;"></div></div>';
    }
    return '<div style="background-color: rgb(247,247,247); color: rgba(0, 0, 0, 0.87); transition: all 450ms 0ms; margin-top: 20px;">' +
        '<div style="border: none; font-size: 11px">' +
        '<div style="border: 5px solid rgb(232, 232, 232); position: relative; display: flex; justify-content: space-between; align-items: left; background-color: rgb(232, 232, 232);">' +
        '<span style="padding-right: 16px; line-height: 38px; font-size: 13px; position: relative; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; color: ' + cveColor + ';">' +
        cvePreHeader +
        '<a href="https://nvd.nist.gov/vuln/detail/' + cveVuln.cve + '" target="_blank" style=" color: ' + cveColor + ';">' + cveVuln.cve + '</a>' +
        '</span></div>' +
        '<div class="markdown-body" style="border: 5px solid rgb(247,247,247); font-size: 11px  overflow: auto; align-items: left;">' +
        '<div style="line-height: 140%;"><b>Product</b>: ' + vulnCpe + '</div>' +
        '<div style="line-height: 140%;">' + relatedDomains + '</div>' +
        '<div style="line-height: 140%;"><b>IP</b>: ' + cveVuln.ip + '</div>' +
        '<div style="line-height: 140%;"><b>Port</b>: ' + cveVuln.port + '</div>' +
        '</div>' +
        '</div><div style="border: 5px solid rgb(247,247,247);">' +
        scoreAndSummary +
        '</div>' +
        '</div>';
};

// This is a fix for empty rows in table (so they won't be displayed in Word).
const fixEmptyRowsInTables = (text) => {
    if (text) {
        // This will remove all empty rows.
        text = text.replace(/<tr><\/tr>/g, '');
    } else {
        text = '';
    }
    return text;
};

const addGenericData = (data, companies, dataType) => {
    let text;
    if (dataType === pdfReport.TYPE_ASNS) {
        text = pdfReport.addASNsByCompany(data, companies);
    } else if (dataType === pdfReport.TYPE_BUCKET) {
        text = pdfReport.addRecordsByCompany(data, dataType, companies);
    } else if (dataType === pdfReport.TYPE_DATALEAKS) {
        text = pdfReport.addDataleaksByCompany(data, companies);
    } else if (dataType === pdfReport.TYPE_EMAIL_BREACHES) {
        text = fixEmptyRowsInTables(pdfReport.addEmailBreachesByCompany(data, companies));
    } else if (dataType === pdfReport.TYPE_BLACKLISTS) {
        text = fixEmptyRowsInTables(pdfReport.addBlacklistsByCompany(data, companies));
    } else if (dataType === pdfReport.TYPE_BOTNETS) {
        text = pdfReport.addBotnetsByCompany(data, companies);
    } else if (dataType === pdfReport.TYPE_SSL_CERTS) {
        text = pdfReport.addSSLCertsByCompany(data, companies);
    } else if (dataType === pdfReport.TYPE_SURVEY) {
        text = pdfReport.addSurveyInfoByProject(companies);
    } else {
        text = '';
    }
    return text;
};

const addGenericPage = (data, companies, title, dataType, explanation, recommendations) => {
    let text;
    // Create table for ASNs.
    if (data && data.length > 0 && title && dataType) {
        text = pageBreak +
            '<div id="page" ' + divPageStyle2 + '>' +
            '<div style="display: inherit; text-align: center">' +
            rowBreak +
            '<myCenter><h2>' + title + '</h2></myCenter>' +
            '</div>';
        text += '<p2>'+ explanation +'</p2>';
        text += addGenericData(data, companies, dataType);
        text += recommendations? recommendations : '';
        text += '</div>';
    } else {
        text = '';
    }
    return text;
};

const addBuckets = (allBuckets, companies, displayScoreBackwards, allScoresData) => {
    let text = '';
    if (allBuckets && allBuckets.length > 0) {
        let publicBuckets =[];
        allBuckets.map((curBucket) => {
            if(curBucket && curBucket.bucket_status && curBucket.bucket_status.toLowerCase() === "public"){
                publicBuckets.push(curBucket);
            }
        });
        if(publicBuckets.length > 0){
        let explanationText = 'Amazon Simple Storage Service (S3 buckets)' +
            ' (<a style="color: dodgerblue;" href="https://docs.aws.amazon.com/AmazonS3/latest/dev/Welcome.html"> https://docs.aws.amazon.com/AmazonS3/latest/dev/Welcome.html</a>)' +
            '  is storage service offered by Amazon on the cloud. This storage service became very popular storage' +
            ' alternative. The risk involved with S3 buckets is mainly due to the case in which the storage is configured' +
            ' to be open for public access leading in many cases to leakage of company confidential data. Our automatic scan' +
            ' technology is continuously looking for publicly open S3 buckets with the goal to identify and alert our ' +
            'customers as soon as possible. ';

        let recommendations = '<h3>Recommendations</h3>' +
            '<p2>Any open S3 bucket needs to quickly be checked and most likely the access need to be properly configured.\n</p2>';
        let scoreImg = pdfReport.createScoreImg(allScoresData.countBuckets.score, displayScoreBackwards, true);

        // Create tables for Buckets.
        text += addGenericPage(publicBuckets, companies, 'S3 BUCKETS ' + scoreImg, pdfReport.TYPE_BUCKET, explanationText, recommendations);
        }
    }
    return text;
};

const addDMARCandSPFs = (allDMARCs, allSPFs, companies, resultCounts, displayScoreBackwards, allScoresData) => {
    let text;
    // Create tables for DMARCs and SPFs.
    if ((allSPFs && allSPFs.length > 0) || (allDMARCs && allDMARCs.length > 0)) {
        let scoreImg = pdfReport.createScoreImg(allScoresData.countDNS.score, displayScoreBackwards, true);

        let noRecordsFoundDMARC = [];
        let noRecordsFoundSPF = [];
        allDMARCs.map((item) => {
            if (item.dmarc && item.dmarc.toLowerCase() === "no records found") {
                noRecordsFoundDMARC.push(item);
            }
        });
        allSPFs.map((item) => {
            if (item.spf && item.spf.toLowerCase() === "no records found") {
                noRecordsFoundSPF.push(item);
            }
        });

        text = pageBreak +
            '<div id="page" ' + divPageStyle + '>' +
            '<div style="display: inherit; text-align: center">' +
            rowBreak +
            '<myCenter><h2>EMAIL SECURITY ' + scoreImg + '</h2></myCenter>' +
            '</div>';
        text += '<p2>Email authentication mechanisms address the challenge of unauthorized use of company email servers for rmsil ' +
            'spoofing often used for phishing attacks. Rescana assess the usage of the two major authentication ' +
            'schemes SPF and DMARC used for email authentication. Domains without authentication records may allow attackers' +
            ' to abuse the domain names and use them for phishing or spamming purposes. </p2><br/><br/>';

        text += '<p2>';
        text += 'We have identified ' + noRecordsFoundDMARC.length + ' of total ' + companies[0].selectedDomains.length + ' domains with no DMARC authentication records';
        text += ' and ' + noRecordsFoundSPF.length + ' of ' + companies[0].selectedDomains.length + ' domains with no SPF authentication records.';
        text += '</p2><br/><br/>';

        text += '<p2>';
        text += 'The following are the lists of domains with no appropriate DMARC and SPF records.\n';
        text += '</p2>';

        text += pdfReport.addRecordsByCompany(noRecordsFoundDMARC, pdfReport.TYPE_DMARC, companies);
        text += rowBreak;
        text += pdfReport.addRecordsByCompany(noRecordsFoundSPF, pdfReport.TYPE_SPF, companies, rowBreak);

        text += '<br/><h3>Recommendations</h3>' +
            '<p2>SPF and DMARC records must be setup for all your domains, (even those which are not actively being used).' +
            ' Please see the following guide -' +
            ' <a style="color: dodgerblue;" href=" https://www.esecurityplanet.com/applications/how-to-set-up-implement-dmarc-email-security.html">' +
            'https://www.esecurityplanet.com/applications/how-to-set-up-implement-dmarc-email-security.html</a></p2>';
        text += '</div>';
    } else {
        text = '';
    }
    return text;
};

const addDataleaks = (allDataleaks, companies, displayScoreBackwards, allScoresData) => {
    let scoreImg = pdfReport.createScoreImg(allScoresData.countDataleaks.score, displayScoreBackwards, true);
    let explanationText = 'This section searches for keywords which are related to the company that appear in paste ' +
        'sites such as <a style="color: dodgerblue;" href="https://en.wikipedia.org/wiki/Pastebin">Pastebin</a>.' +
        ' data found could include: code snippets, database dumps, ' +
        'email corresponding, hacking target lists and more. When a page with such data is found it is advised ' +
        'to start a takedown process.';
    // Create tables for Dataleaks.
    return addGenericPage(allDataleaks, companies, 'DATALEAKS ' + scoreImg, pdfReport.TYPE_DATALEAKS, explanationText);
};

const addEmailBreaches = (allEmailBreaches, companies, displayScoreBackwards, allScoresData) => {
    let scoreImg = pdfReport.createScoreImg(allScoresData.countEmailBreaches.score, displayScoreBackwards, true);
    let explanationText = 'The following is a list of emails that were found in various breaches.' +
        ' This could also give an indication about the security awareness status in the company.';
    let recommendations = '<h3>Recommendations</h3>' +
        '<p2>It is recommended to alert the users and require them to change passwords on all company systems.</p2>';

    // Create tables for Email Breaches.
    return addGenericPage(allEmailBreaches, companies, 'EMAILS FOUND IN BREACHES ' + scoreImg, pdfReport.TYPE_EMAIL_BREACHES, explanationText, recommendations);
};

const addBlacklists = (allBlacklists, companies, displayScoreBackwards, allScoresData) => {
    let explanationText = 'Multiple companies and agencies maintain publicly available black lists. There are 4 types of lists: domain,' +
        ' IP, MX and NS blacklists. These lists hold suspicious domains, IPs and email addresses that were found to' +
        ' be involved in illegitimate activities or hold inappropriate content. \\n</p2><br/><br/>\';\n' +
        '    <p2>The following is a list of ' + companies[0].companyName + '\'s ip addresses which were found as being' +
        ' used for spam or distribution of other unwanted content.';
    let recommendations = '<h3>Recommendations</h3>' +
        '<p2>Eradicate the systems of potential botnets or malware that may be the cause of spamming and' +
        ' make sure they are adequately protected by anti-malware technologies. If these ip addresses belong' +
        ' to a shared hosting provider, change the server/provider.</p2>';
    let scoreImg = pdfReport.createScoreImg(allScoresData.countBlacklists.score, displayScoreBackwards, true);
    // Create tables for Blacklists.
    return addGenericPage(allBlacklists, companies, 'BLACKLISTS ' + scoreImg , pdfReport.TYPE_BLACKLISTS, explanationText, recommendations);
};

const addBotnets = (allBotnets, companies, displayScoreBackwards, allScoresData) => {
    let explanationText = 'Botnets are network of connected devices (many time compromised) which are jointly executing' +
        ' malicious payloads. Botnets often used for: distributed denial of service, steal data, send spam and other' +
        ' malicious activities. We are scanning the internet in an attempt to identify company\'s compromised devices'+
        ' that run under such botnets.</p2><br/>' +
        '<p2>The following is a list of IP addresses that are hosting malicious code and are most likely compromised. ';
    let recommendations = '<h3>Recommendations</h3>' +
        '<p2>Eradicate the systems of potential botnets or malware and make sure they are adequately protected by anti-malware technologies. ' +
        'If these ip addresses belong to a shared hosting provider, change the server/provider.\n</p2>';
    let scoreImg = pdfReport.createScoreImg(allScoresData.countBotnets.score, displayScoreBackwards, true);
    // Create tables for Botnets.
    return addGenericPage(allBotnets, companies, 'BOTNETS AND MALWARE ' + scoreImg , pdfReport.TYPE_BOTNETS, explanationText, recommendations);
};

const addSSLCerts = (allSSLCerts, companies, displayScoreBackwards, allScoresData) => {
    let sslCertsText = '';
    if (allSSLCerts && allSSLCerts.length > 0) {
        let notValidHttpsSSL = [];
        allSSLCerts.map((item) => {
            if (item.statusSSL && (item.statusSSL.toLowerCase() === "no https" || !item.valid && item.statusSSL.toLowerCase() === "https")) {
                notValidHttpsSSL.push(item);
            }
            if (item.subdomains) {
                item.subdomains.map((item) => {
                    if (item.statusSSL && (item.statusSSL.toLowerCase() === "no https" || !item.valid && item.statusSSL.toLowerCase() === "https")) {
                        notValidHttpsSSL.push(item);
                    }
                });
            }
        });

        sslCertsText += '<div id="page">';
        let explanationText = 'SSL certificates are used to prove ownership of a public key. The certificate holds several ' +
            'fields about the owner, its expiry date and its issuer. The certificate is a key component in' +
            'establishing secure communication.  </p2><br/><br/>' +
            '<p2>The following is a list of ' + companies[0].companyName + '\'s expired ssl certificates. Misconfigured certificates here may ' +
            'indicate sensitive data could be compromised by eavesdropping.';
        let recommendations = '<h3>Recommendations</h3>' +
            '<p2>Make sure the SSL certificates are properly configured and maintained. ' +
            'Also, make sure to use adequate encryption and key exchange levels. ' +
            'Please see the following guide for more information:' +
            '<a style="color: dodgerblue;" href="https://www.acunetix.com/blog/articles/tls-ssl-cipher-hardening/"> ' +
            'https://www.acunetix.com/blog/articles/tls-ssl-cipher-hardening/</a></p2>';
        let scoreImg = pdfReport.createScoreImg(allScoresData.countSSLCerts.score, displayScoreBackwards, true);
        // Create tables for SSL Certificates.
        sslCertsText += addGenericPage(notValidHttpsSSL, companies, 'HTTPS - SSL CERTIFICATES ' + scoreImg, pdfReport.TYPE_SSL_CERTS, explanationText, recommendations);
        sslCertsText += '</div>';
    }
    return sslCertsText;
};

const addManualDataPages = (manualData) => {
    let text = '';
    if (manualData && Array.isArray(manualData) && manualData.length > 0) {
        manualData.map((currManualData) => {
            if (currManualData && (currManualData.title || currManualData.text || currManualData.files)) {
                text += pageBreak +
                    '<div id="page" ' + divPageStyle + '>' +
                    '<div style="display: inherit; text-align: center">' +
                    rowBreak +
                    '<myCenter><h2>';
                text += (currManualData.title) ? currManualData.title : 'FINDING REVIEW';
                text += '</h2></myCenter>' +
                    '</div>' +
                    '<p2>';
                text += (currManualData.text) ? currManualData.text : '';
                text += '</p2>';
                if (currManualData.files && Array.isArray(currManualData.files) && currManualData.files.length > 0) {
                    text += '<div style="display: inherit; text-align: center"><myCenter>';
                    currManualData.files.map((currFile, i) => {
                        if (currFile && currFile.path) {
                            const currPath = helpers.getFilePathInitial() + '/../../../intelManuals' + currFile.path;
                            text += rowBreak +
                                '<img src="' + currPath + '" style="margin: 15px 0px; height: auto; width: auto; max-height: 768px; max-width: 1024px;">';
                            if (i < currManualData.files.length - 1) {
                                text += '<br/>';
                            }
                        }
                    });
                    text += '</div></myCenter>';
                }
                text += '</div>';
            }
        });
    }
    return text;
};

function bufferToStream(binary) {
    const readableInstanceStream = new Readable({
        read() {
            this.push(binary);
            this.push(null);
        }
    });

    return readableInstanceStream;

}
