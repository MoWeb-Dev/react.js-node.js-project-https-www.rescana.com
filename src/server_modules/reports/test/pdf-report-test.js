const chai = require('chai');
const expect = chai.expect;
const pdfCommon = require('../pdf-reports/pdf-common.js');

describe('Pdf Reports', function() {
    describe('logoObjValidation() - validation for valid logo object', function() {

        const logo ={
            "path" : "/512-1562230966844.png",
            "scanStatus" : "scanning"
        };
        const logoWithNoProperties ={};

        const logoWithNoPathProperty ={
            "scanStatus" : "scanning"
        };

        it('Should return true', function() {
            let result = pdfCommon.logoObjValidation(logo);
            return expect(result).to.be.true;
        });

        it('Should return false', function() {
            let result = pdfCommon.logoObjValidation(logoWithNoProperties);
            return expect(result).to.be.false;
        });

        it('Should return false', function() {
            let result = pdfCommon.logoObjValidation(logoWithNoPathProperty);
            return expect(result).to.be.false;
        });

        it('Should return false', function() {
            let result = pdfCommon.logoObjValidation();
            return expect(result).to.be.false;
        });

        it('Should return false', function() {
            let result = pdfCommon.logoObjValidation(0);
            return expect(result).to.be.false;
        });
    });
});
