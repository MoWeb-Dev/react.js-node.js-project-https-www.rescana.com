const Seneca = require('seneca');
const chai = require('chai');
const expect = chai.expect;
const excelReport = require('../excel-reports/excel-report.js');
const xl = require('excel4node');
const mock = require('./mocks');

describe('Excel Reports', function() {
    describe('addTxtToExcel()', function(fin) {
        const wb = new xl.Workbook();
        let style;
        before(function() {
            style = {
                categoryStyle: wb.createStyle({
                    font: {
                        bold: true
                    },
                    alignment: {
                        wrapText: true,
                        horizontal: 'center'
                    }
                }),
                generalStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true,
                        size: 80
                    }
                }),
                headerStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true
                    },
                    font: {
                        color: 'white',
                        bold: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#1E88E5'
                    }
                })
            };
        });

        it('Should return worksheet object', function(fin) {
            expect(excelReport.addTxtToExcel(mock.spfMock, 'spf', wb, style)).to.be.an('object');
            fin();
        });

        it('Worksheet should have name', function(fin) {
            const excelObj = excelReport.addTxtToExcel(mock.spfMock, 'spf', wb, style);
            expect(excelObj.sheets[0].name).to.equal('spf');
            fin();
        });

        it('Worksheet should have all headers', function(fin) {
            const excelObj = excelReport.addTxtToExcel(mock.spfMock, 'spf', wb, style);
            const spfKeysArr = Object.keys(mock.spfMock[0]);
            const cols = Object.keys(excelObj.sheets[0].cols);

            expect(cols.length).to.equal(spfKeysArr.length);
            fin();
        });
    });

    describe('addBucketsToExcel()', function(fin) {
        const wb = new xl.Workbook();
        let style;
        before(function() {
            style = {
                categoryStyle: wb.createStyle({
                    font: {
                        bold: true
                    },
                    alignment: {
                        wrapText: true,
                        horizontal: 'center'
                    }
                }),
                generalStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true,
                        size: 80
                    }
                }),
                headerStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true
                    },
                    font: {
                        color: 'white',
                        bold: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#1E88E5'
                    }
                })
            };
        });

        it('Should return worksheet object', function(fin) {
            expect(excelReport.addBucketsToExcel(mock.bucketMock, wb, style)).to.be.an('object');
            fin();
        });

        it('Worksheet should have name', function(fin) {
            const excelObj = excelReport.addBucketsToExcel(mock.bucketMock, wb, style);
            expect(excelObj.sheets[0].name).to.equal('S3 Buckets');
            fin();
        });

        it('Worksheet should have all headers', function(fin) {
            const excelObj = excelReport.addBucketsToExcel(mock.bucketMock, wb, style);
            const spfKeysArr = Object.keys(mock.bucketMock[0]);
            const cols = Object.keys(excelObj.sheets[0].cols);

            expect(cols.length).to.equal(spfKeysArr.length);
            fin();
        });
    });

    describe('addPortsToExcel()', function(fin) {
        const wb = new xl.Workbook();
        let style;
        before(function() {
            style = {
                categoryStyle: wb.createStyle({
                    font: {
                        bold: true
                    },
                    alignment: {
                        wrapText: true,
                        horizontal: 'center'
                    }
                }),
                generalStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true,
                        size: 80
                    }
                }),
                headerStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true
                    },
                    font: {
                        color: 'white',
                        bold: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#1E88E5'
                    }
                })
            };
        });

        it('Should return worksheet object', function(fin) {
            expect(excelReport.addPortsToExcel(mock.ports, wb, style)).to.be.an('object');
            fin();
        });

        it('Worksheet should have name', function(fin) {
            const excelObj = excelReport.addPortsToExcel(mock.ports, wb, style);
            expect(excelObj.sheets[0].name).to.equal('Ports');
            fin();
        });

        it('Worksheet should have all headers', function(fin) {
            const excelObj = excelReport.addPortsToExcel(mock.ports, wb, style);
            const spfKeysArr = Object.keys(mock.ports[0]);
            const cols = Object.keys(excelObj.sheets[0].cols);

            expect(cols.length).to.equal(spfKeysArr.length);
            fin();
        });
    });

    describe('addDataLeaksToExcel()', function(fin) {
        const wb = new xl.Workbook();
        let style;
        before(function() {
            style = {
                categoryStyle: wb.createStyle({
                    font: {
                        bold: true
                    },
                    alignment: {
                        wrapText: true,
                        horizontal: 'center'
                    }
                }),
                generalStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true,
                        size: 80
                    }
                }),
                headerStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true
                    },
                    font: {
                        color: 'white',
                        bold: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#1E88E5'
                    }
                })
            };
        });

        it('Should return worksheet object', function(fin) {
            expect(excelReport.addDataLeaksToExcel(mock.dataleaksMock, wb, style)).to.be.an('object');
            fin();
        });

        it('Worksheet should have name', function(fin) {
            const excelObj = excelReport.addDataLeaksToExcel(mock.dataleaksMock, wb, style);
            expect(excelObj.sheets[0].name).to.equal('Data Leaks');
            fin();
        });

        it('Worksheet should have all headers', function(fin) {
            const excelObj = excelReport.addDataLeaksToExcel(mock.dataleaksMock, wb, style);
            const spfKeysArr = Object.keys(mock.dataleaksMock[0]);
            const cols = Object.keys(excelObj.sheets[0].cols);

            expect(cols.length).to.equal(spfKeysArr.length);
            fin();
        });
    });

    describe('addScoresToExcel()', function(fin) {
        const wb = new xl.Workbook();
        let style;
        before(function() {
            style = {
                categoryStyle: wb.createStyle({
                    font: {
                        bold: true
                    },
                    alignment: {
                        wrapText: true,
                        horizontal: 'center'
                    }
                }),
                generalStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true,
                        size: 80
                    }
                }),
                headerStyle: wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true
                    },
                    font: {
                        color: 'white',
                        bold: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#1E88E5'
                    }
                })
            };
        });

        it('Should return worksheet object', function(fin) {
            expect(excelReport.addScoresToExcel(mock.companiesWithScores, false, wb, style)).to.be.an('object');
            fin();
        });

        it('Worksheet should have name', function(fin) {
            const excelObj = excelReport.addScoresToExcel(mock.companiesWithScores, false, wb, style);
            expect(excelObj.sheets[0].name).to.equal('Risk Scores');
            fin();
        });
    });
});


// Construct a Seneca instance suitable for unit testing
function test_seneca(fin) {
    return Seneca({log: 'test'})

    // activate unit test mode. Errors provide additional stack tracing context.
    // The fin callback is called when an error occurs anywhere.
        .test(fin)

        .use('basic')

        .use('entity')

        // Load the microservice business logic
        .use(require('../report-creator.js'))

        // Define mock messages that the business logic needs
        .add('role:surveystore, list:surveyAnswers', function(msg, reply) {
            reply(null, surveyObj.surveyObj2);
        })

        .add('role:surveystore, listById:survey', function(msg, reply) {
            reply(null, surveyObj.surveyObj2);
        })

        .add('role:companies, get:company', function(msg, reply) {
            reply(null, surveyObj.elCamino);
        });
}

