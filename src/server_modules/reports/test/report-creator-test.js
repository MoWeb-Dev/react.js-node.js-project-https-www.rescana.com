const chai = require('chai');
const expect = chai.expect;
const reportHelpers = require('../common');
/* const Seneca = require('seneca');
const surveyObj = require('../report-creator');*/

// /*describe('get data for reports', function () {
//     describe('role:reportCreator,cmd:getPreDataForReport', function (fin) {
//         let seneca = test_seneca(fin);
//         it('should collect all data for reports', function (fin) {
//             setTimeout(() => {
//                 // Create a Seneca instance for testing.
//                 seneca
//                     .gate()
//                     // Send an action, and validate the response.
//                     .act({
//                         role: 'reportCreator',
//                         cmd: 'getPreDataForReport',
//                         data: {
//                             uid: 1,
//                             projects: [{id: 12, projectName: "poalim"}]
//                         }
//                     }, function (ignore, result) {
//                         return expect(result).to.be.an('object');
//                     })
//
//                     // Under gating, `ready` will wait until all actions have completed.
//                     .ready(fin)
//             }, 300);
//         });
//     });
// });*/


/* // Construct a Seneca instance suitable for unit testing
function test_seneca(fin) {
    return Seneca({log: 'test'})

    // activate unit test mode. Errors provide additional stack tracing context.
    // The fin callback is called when an error occurs anywhere.
        .test(fin)

        .use('basic')

        .use('entity')

        // Load the microservice business logic
        .use(require('../report-creator.js'))

        // Define mock messages that the business logic needs
        .add('role:surveystore, list:surveyAnswers', function (msg, reply) {
            reply(null, surveyObj.surveyObj2);
        })

        .add('role:surveystore, listById:survey', function (msg, reply) {
            reply(null, surveyObj.surveyObj2);
        })

        .add('role:companies, get:company', function (msg, reply) {
            reply(null, surveyObj.elCamino);
        })

}*/

describe('Excel Reports', function() {
    // test to sortAllVulnsByIP in reports/common.js
    describe('sortAllVulnsByIP()', function() {
        let ipVulns, asns;

        before(function() {
            ipVulns = [
                {hostname: 'aaa.com', ip: '1.1.1.1', port: '80', cpe: 'cpe:/a:apache:http_server:2.4.18', cve: 'CVE-2016-5000'},
                {hostname: 'aaa.com', ip: '1.1.1.1', port: '80', cpe: 'cpe:/a:apache:http_server:2.4.18', cve: 'CVE-2016-5001', cveScore: 5, summary: 'cveSummary'},
                {hostname: 'bbb.com', ip: '1.1.1.1', port: '80', cpe: 'cpe:/a:apache:http_server:2.4.18', cve: 'CVE-2016-5001', cveScore: 5, summary: 'cveSummary'},
                {hostname: 'aaa.com', ip: '1.1.1.1', port: '80', cpe: 'cpe:/a:apache:http_server:2.4.18', cve: 'CVE-2016-4979', cveScore: 10, summary: 'cveSummary'}
            ];

            asns = [
                {
                    'hostname': 'tnuva.co.il',
                    'subdomain': 'minion.tnuva.co.il',
                    'ip': '82.166.160.146',
                    'asn': '1680'
                },
                {
                    'hostname': 'tnuva.co.il',
                    'subdomain': 'minion.tnuva.co.il',
                    'ip': '82.166.160.146',
                    'asn': 'AS1680'
                },
                {
                    'hostname': 'tnuva.co.il',
                    'subdomain': 'minion.tnuva.co.il',
                    'ip': '82.166.160.146',
                    'asn': 'as1680'
                },
                {
                    'hostname': 'tnuva.co.il',
                    'subdomain': 'minion.tnuva.co.il',
                    'ip': '82.166.160.146',
                    'asn': '1670'
                },
                {
                    'hostname': 'tnuva.co.il',
                    'subdomain': 'minion.tnuva.co.il',
                    'ip': '82.166.160.146',
                    'asn': 'AS1670'
                },
                {
                    'hostname': 'tnuva.co.il',
                    'subdomain': 'minion.tnuva.co.il',
                    'ip': '82.166.160.146',
                    'asn': ''
                },
                {}];
        });

        it('Should always return Risks array', function(fin) {
            expect(reportHelpers.sortAllVulnsByIP()).to.be.an('array');
            expect(reportHelpers.sortAllVulnsByIP([])).to.be.an('array');
            expect(reportHelpers.sortAllVulnsByIP({})).to.be.an('array');
            expect(reportHelpers.sortAllVulnsByIP(null)).to.be.an('array');
            expect(reportHelpers.sortAllVulnsByIP(ipVulns)).to.be.an('array');
            fin();
        });

        it('Should sort CVEs by score', function(fin) {
            const result = reportHelpers.sortAllVulnsByIP(ipVulns);
            expect(result[0].cveScore).to.equal(10);
            fin();
        });

        it('Should merge domains of same CVEs', function(fin) {
            const result = reportHelpers.sortAllVulnsByIP(ipVulns);
            expect(result[1].domains.length).to.equal(2);
            fin();
        });

        it('Should assign cveScore to 0 if not defined', function(fin) {
            const result = reportHelpers.sortAllVulnsByIP(ipVulns);
            expect(result[result.length - 1].cveScore).to.equal(0);
            fin();
        });

        it('Should assign cveSummary to empty string if not defined', function(fin) {
            const result = reportHelpers.sortAllVulnsByIP(ipVulns);
            expect(result[result.length - 1].cveSummary).to.equal('');
            fin();
        });

        it('Should count unique ASNs', function(fin) {
            expect(reportHelpers.countASNs(asns).countASNs).to.equal(3);
            fin();
        });
    });
});


