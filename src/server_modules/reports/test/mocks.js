const moment = require('moment');

module.exports.spfMock = [{
    'hostname': 'companyA.com',
    'subdomain': 'fun.companyA.com',
    'spf': 'No Records Found'
}, {
    'hostname': 'companyA.co.il',
    'subdomain': 'dev.companyA.co.il',
    'spf': 'v=spf1 include:_spf.google.com ~all'
}, {
    'hostname': 'xn--4dbdjb0drfyx.com',
    'subdomain': 'none',
    'spf': 'No Records Found'
}, {
    'hostname': 'xn--4dbdjb0drfyx.com',
    'subdomain': 'none',
    'spf': 'v=spf1 include:_spf.google.com ~all'
}
];

module.exports.dataleaksMock = [{
    source: 'Pastebin',
    url: 'pastebin.com/dfasjvia',
    title: 'A title',
    keyword: 'bla',
    createDate: moment().format('DD/MM/YYYY, h:mm:ss a'),
    relatedCompanies: 'Moshe Inc.'
}
];

module.exports.bucketMock = [{
    'hostname': 'moshe.co.il',
    'subdomain': 'none',
    'bucket': 'moshe.s3.amazonaws.com',
    'bucket_status': 'AllAccessDisabled'
}
];

module.exports.companiesWithScores = [
    {
        'id': '5a327a24203ca32354f33216',
        'companyName': 'Bank 1',
        'selectedDomains': [
            'example1.co.il',
            'example2.com',
            'example3.co.il'
        ],
        'keywords': [],
        'surveyScore': 35,
        'maxIntelScore': 75
    },
    {
        'id': '599eb9b770cbd63ff42c8078',
        'companyName': 'Bank 2',
        'selectedDomains': [
            'example4.co.il',
            'example76.com',
            'example2342.net'
        ],
        'keywords': [],
        'surveyScore': 0,
        'maxIntelScore': 68
    }];

module.exports.ports =
    [
        {
            'hostname': 'exampleA.com',
            'subdomain': 'mail1.exampleA.ch',
            'ip': '194.124.243.144',
            'port': '25'
        },
        {
            'hostname': 'exampleA.com',
            'subdomain': 'mail2.exampleA.ch',
            'ip': '194.124.243.144',
            'port': '53'
        },
        {
            'hostname': 'exampleB.com',
            'subdomain': 'mail2.exampleB.ch',
            'ip': '194.124.243.144',
            'port': '443'
        },
        {
            'hostname': 'exampleC.com',
            'subdomain': 'mail2.exampleC.ch',
            'ip': '194.124.243.144',
            'port': '80'
        }
    ];


