const request = require('request-promise');
const config = require('app-config');
let circllu = 'http://35.227.19.113:5000/api/';

if (config && config.addresses) {
    circllu = config.addresses.circllu;
}
const options = {
    method: 'GET',
    uri: circllu,
    json: true
};

// To get a JSON with all the vendors:
module.exports.getAllVendors = () => {
    const opts = {};
    Object.assign(opts, options);
    opts.uri = options.uri + 'browse';
    return request(opts)
        .then((response) => {
            return response;
        })
        .catch((err) => {
            throw err;
        });
};

// To get a JSON with all the products associated to a vendor:
module.exports.getProdsAssociatedWithVendors = (vendor) => {
    const opts = {};
    Object.assign(opts, options);
    opts.uri = options.uri + 'browse/' + vendor;
    return request(opts)
        .then((response) => {
            return response;
        })
        .catch((err) => {
            throw err;
        });
};

// To get a JSON with all the Risks per vendor and a specific product:
module.exports.getCveByProdsAndVendor = (vendor, product) => {
    const opts = {};
    Object.assign(opts, options);
    opts.uri = options.uri + 'search/' + vendor + '/' + product;
    return request(opts)
        .then((response) => {
            return response;
        })
        .catch((err) => {
            throw err;
        });
};

// To get a JSON of a specific CVE ID:
module.exports.getCVEInfo = (cveID) => {
    const opts = {};
    Object.assign(opts, options);
    opts.uri = options.uri + 'cve/' + cveID;
    return request(opts)
        .then((response) => {
            return response;
        })
        .catch((err) => {
            throw err;
        });
};

// To get a JSON of the last 30 CVEs including CAPEC, CWE and CPE expansions:
module.exports.getlast30CVE = () => {
    const opts = {};
    Object.assign(opts, options);
    opts.uri = options.uri + 'last';
    return request(opts)
        .then((response) => {
            return response;
        })
        .catch((err) => {
            throw err;
        });
};

// To get a JSON CVE for CPE
module.exports.getCVEForCPE = (cpe) => {
    const opts = {};
    Object.assign(opts, options);
    opts.uri = options.uri + 'cvefor/' + cpe;
    console.log('Getting cve from uri:', opts.uri);
    return request(opts)
        .then((response) => {
            console.log('Got response for CVE');
            return response;
        })
        .catch((err) => {
            throw err;
        });
};

// this.getCVEForCPE("cpe:/a:apache:http_server:2.2.3");
// this.getlast30CVE();
