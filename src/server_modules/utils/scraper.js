'use strict';
const read = require('node-read');
const hObj = require('htm-to-json');
const Promise = require('bluebird');

// curl -d '{"role":"scraper","scrape":"page","url":"http://www.bekaloot.co.il"}' http://localhost:10500/act
module.exports = function scraper() {
    const seneca = this;
    seneca.add('role:scraper, scrape:page', scrape);
    const act = Promise.promisify(seneca.act, {context: seneca});

    async function scrape(msg, respond) {
        console.log('in scraper, scraping', msg.app.url);
        let url = msg.app.url,
            store = 'publicstore';
        if (url) {
            console.log("url is :",url);
            if (url.indexOf('http://') === -1) {
                url = 'http://' + url;
            }

            let article;

            try {
                article = await readArticle(url);
            } catch (e) {
                console.error('Error in readArticle: ', e);
                respond();
            }

            if (article) {
                let data;
                try {
                    data = await convertHtmlToJson(article);
                } catch (err) {
                    console.log('Error:', err);
                    respond();
                }

                if (msg.customer) {
                    store = 'customerstore';
                }

                // The store breaks when it gets a dotted key attribute, so we'll encode it.
                data = traverse(data);
                console.log('updating : ', msg.app.id);
                try {
                    await act('role:' + store + ', update:app', {data: {id: msg.app.id, scrapedData: data}});
                } catch (e) {
                    console.error('Error updating app: ', e);
                }
                respond();
            }
            respond();
        }else{
            console.log("Did not receive URL");
            respond();
        }
    }
};

async function readArticle(url) {
    return new Promise((resolve, reject) => {
        read(url, (err, article) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                resolve(article);
            }
        });
    });
}

async function convertHtmlToJson(article) {
    return new Promise((resolve, reject) => {
        hObj.convert_html_to_json(article.html, (err, data) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

function traverse(data) {
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            if (key && (key.indexOf('.') !== -1 || key.indexOf('$') !== -1)) {
                console.log('deleting : ', key);
                delete data[key];
            }

            if (Array.isArray(data[key])) {
                const arr = data[key];
                for (let i = 0; i < arr.length; i++) {
                    for (const k in arr[i]) {
                        if (arr[i].hasOwnProperty(k)) {
                            if (k && (k.indexOf('.') !== -1 || k.indexOf('$') !== -1)) {
                                console.log('deleting : ', k);
                                delete arr[i][k];
                            }
                        }
                    }
                }
            } else {
                console.log('key is: ', key);
                if (key && (key.indexOf('.') !== -1 || key.indexOf('$') !== -1)) {
                    console.log('deleting : ', key);
                    delete data[key];
                }
            }
        }
    }
    return data;
}
