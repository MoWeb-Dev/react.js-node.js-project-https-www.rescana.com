'use strict';
const parseDomain = require('parse-domain');
const manualKeywords = require('../../../config/keywords');
const providers = require('../../../config/providers');
const Promise = require('bluebird');
const RegexParser = require('regex-parser');
const moment = require('moment');
const helpers = require('../../server/api/api-helpers.js');

// This module returns the domain name from URLs
module.exports.getDomainFromURL = (url) => {
    if (url) {
        return parseDomain(url).domain + '.' + parseDomain(url).tld;
    }
};

module.exports.getDomainFromURLWithoutTld = (url) => {
    if (url) {
        return parseDomain(url).domain;
    }
};

// This module returns the domain name from URLs
module.exports.getDomainAndSubFromURL = (url) => {
    if (url && parseDomain(url).subdomain) {
        return parseDomain(url).subdomain + '.' + parseDomain(url).domain + '.' + parseDomain(url).tld;
    }
};

// Deletes all empty attributes from object.
module.exports.cleanObject = (obj) => {
    for (const i in obj) {
        if (obj.hasOwnProperty(i)) {
            if (obj[i] === null || obj[i] === undefined) {
                delete obj[i];
            }
        }
    }
    return obj;
};

module.exports.extractHostname = (url) => {
    let hostname;
    // find & remove protocol (http, ftp, etc.) and get the hostname
    if (url.indexOf('://') > -1) {
        hostname = url.split('/')[2];
    } else {
        hostname = url.split('/')[0];
    }

    // find & remove port number
    hostname = hostname.split(':')[0];

    return hostname;
};

module.exports.getBrandName = (url) => {
    // to address those who want the "root domain"
    let domain = this.extractHostname(url),
        splitArr = domain.split('.');
    // extracting the root domain here
    return splitArr[0];
};

module.exports.fetchKeywords = () => {
    return manualKeywords.expressions;
};
module.exports.loadKeywords = (context) => {
    const promiseAct = Promise.promisify(context.act, {context: context});

    context.client({
        host: 'localhost',
        port: 10904
    });

    return promiseAct('role:companies, list:company', {
        query: {}
    }).then((results) => {
        const keywords = [];
        if (results) {
            results.map((company) => {
                if (company.keywords) {
                    company.keywords.map((keyword) => {
                        const regexKeyword = RegexParser('/' + keyword.replace('.', '\\.') + '/i');
                        let isExist = false;
                        for (let i = 0; i < keywords.length; i++) {
                            if (String(keywords[i].keyword) === String(regexKeyword)) {
                                isExist = true;
                            }
                        }
                        if (!isExist) {
                            keywords.push({keyword:  regexKeyword, company : company.companyName});
                        }
                    });
                }
            });
        }
        return keywords;
    }).catch((e) => {
        console.log('loadKeywords: ', e);
        return [];
    });
};

module.exports.cleanSurveyObject = async (arr, userId, seneca) => {
    let isAdmin = false;
    try {
        isAdmin = await helpers.isAdminUserByID(userId, seneca);
    } catch (e) {
        console.error('in cleanSurveyObject() - error' + e);
    }

    for (let i = 0; i < arr.length; i++) {
        if (!isAdmin) {
            delete arr[i].allowedOrganizations;
            delete arr[i].uid;
        }
        delete arr[i]._id;
    }

    return arr;
};

module.exports.getIntelUpdatesQuery = (requestedIntel, withSubdomain) => {
    const isIP = requestedIntel === 'ip';
    const isASN = requestedIntel === 'asn';
    const isISP = requestedIntel === 'isp';
    const isVPN = requestedIntel === 'vpn';
    const label = (isIP) ? 'IP' : (isASN) ? 'ASN' : requestedIntel;
    let neoAtt = (isIP) ? 'address' : (isISP || isASN) ? 'finding' : (isVPN) ? 'name' : requestedIntel;
    let addedProperties = '';

    let cmd = 'MATCH (n:' + label + ')<-[k:' + label + ']-(t';
    // This logic rely on a specific hierarchy so do not change the cases' order.
    switch (requestedIntel) {
    case ('cve'): {
        addedProperties = ', n.score as cveScore' +
                ', ' + cmd[cmd.length - 1] + '.cpe as cpe';
        cmd += ':cpe)<-[a3:cpe]-(a';
    }
    case ('vpn'):
    case ('cpe'): {
        addedProperties += ', ' + cmd[cmd.length - 1] + '.ports as ports';
        cmd += ':ports)<-[a5:ports]-(b';
    }
    case ('isp'):
    case ('asn'):
    case ('ports'): {
        addedProperties += ', ' + cmd[cmd.length - 1] + '.address as ip';
        cmd += ':IP)<-[a7:IP]-(';
        if (withSubdomain){
            // Must be one letter like 't' is one letter (also in the above node names).
            cmd += 'c';
        } else {
            cmd += 'd:domain)';
        }
    }
    case ('ip'): {
        // Used for a union between a subdomain case and directly domain.
        if (withSubdomain){
            addedProperties += ', ' + cmd[cmd.length - 1] + '.hostname as subdomain';
            cmd += ':subdomain)<-[a9:subdomain]-(d:domain)';
        } else {
            addedProperties += ', \'None\' as subdomain';
            // If requestedIntel is 'ip' and no subdomains case - t is the domain.
            if (cmd[cmd.length - 1] === 't') {
                // Remove the 't' and label it as domain.
                cmd = cmd.substring(0, cmd.length - 1) + 'd:domain)';
            }
        }
        break;
    }
    case ('dmarc'):
    case ('spf'): {
        // If requestedIntel is the above two - t is the domain.
        if (cmd[cmd.length - 1] === 't'){
            // Remove the 't' and label it as domain.
            cmd = cmd.substring(0, cmd.length - 1) + 'd:domain)';
        }
        break;
    }
    case ('bucket'): {
        neoAtt = 's3';
        cmd = 'MATCH (n:bucket_status)<-[k:bucket_status]-(b:bucket)<-[a8:bucket]-(';
        addedProperties += ', n.bucket_status as bucketStatus';
        if (withSubdomain){
            cmd += 's:subdomain)<-[a9:subdomain]-(d:domain)';
            addedProperties += ', s.hostname as subdomain';
        } else {
            cmd += 'd:domain)';
            addedProperties += ', \'None\' as subdomain';
        }
        break;
    }
    default: {
        return null;
    }
    }
    addedProperties += ', \'' + requestedIntel + '\' as intelType';

    const today = moment().format('YYYY-MM-DD');

    cmd += ' WHERE k.resolve_time >= \'' + today + '\' RETURN n.' + neoAtt + ' as ' + requestedIntel + addedProperties +
        ', d.hostname as hostname, k.resolve_time as resolve_time';

    return cmd;
};

module.exports.getTitleForCVE = (item) => {
    if (item['metasploit'] && item['metasploit'][0] && item['metasploit'][0].title) {
        return item['metasploit'][0].title;
    }
    if (item['exploit-db'] && item['exploit-db'][0] && item['exploit-db'][0].title) {
        return item['exploit-db'][0].title;
    }
    if (item['oval'] && item['oval'][0] && item['oval'][0].title) {
        return item['oval'][0].title;
    }
    return '';
};

