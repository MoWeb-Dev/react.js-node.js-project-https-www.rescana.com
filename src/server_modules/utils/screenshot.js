'use strict';
const webshot = require('webshot');
const Promise = require('bluebird');
const shortid = require('shortid');
const path = require('path');
const fs = require('fs');

module.exports = function TakeScreenShot() {
    const act = Promise.promisify(this.act, {context: this});
    const options = {
        phantomConfig: {
            'ssl-protocol': 'any'
        }
    };

    this.add('role:screenshot, capture:app', async (msg, respond) => {
        const imgName = shortid.generate();
        const imgPath = path.join(__dirname, '..', '..', '..', 'scr', 'img' + imgName + '.png');
        let role = 'publicstore';
        console.log('In screenshot module');
        if (msg.customer) {
            role = 'customerstore';
        }
        console.log('Capturing : ', msg.app.url);
        console.log('saving in path : ', imgPath);

        if (!msg.app.screenshotFullPath) {
            await takeScreenshot(msg, imgPath, imgName, role, respond);
        } else {
            // Check if file exists before writing again.
            fs.open(msg.app.screenshotFullPath, 'wx',  async (err) => {
                if (err) {
                    if (err.code === 'EEXIST') {
                        console.error('Image already exists, writing over');
                        fs.unlink(msg.app.screenshotFullPath, async (err) => {
                            if (err) {
                                console.log('Error : can\'t delete file');
                            }
                            await takeScreenshot(msg, imgPath, imgName, role, respond);
                        });
                    } else {
                        console.log('ERROR: ', err.code);
                        respond();
                    }
                } else {
                    console.log('Taking screenshot');
                    await takeScreenshot(msg, imgPath, imgName, role, respond);
                }
            });
        }
    });

    const takeScreenshot = async (msg, imgPath, imgName, role, respond) => {
        console.log(imgPath);
        console.log(imgName);
        if(!msg.app.url){
            console.log("No URL received");
            await act('role:' + role + ', update:app', {
                data: {
                    id: msg.app.id,
                    screenshotFullPath: '',
                    screenshot: ''
                }
            });
            respond();
        }
        {
            try {
                await startWebShot(msg.app.url, imgPath, options);
            } catch (e) {
                if (e) {
                    console.log('Error: ', e);
                    await act('role:' + role + ', update:app', {
                        data: {
                            id: msg.app.id,
                            screenshotFullPath: '',
                            screenshot: ''
                        }
                    });

                    respond();
                }
            }

            try {
                await act('role:' + role + ', update:app', {
                    data: {
                        id: msg.app.id,
                        screenshotFullPath: imgPath,
                        screenshot: '/img' + imgName + '.png'
                    }
                });
            } catch (e) {
                console.error('Error: ', e);
            }

            console.log('Done updating screenshot locations.');
            respond();
        }
    };

    const startWebShot = (msg, imgPath, options) => {
        return new Promise((resolve, reject) => {
            webshot(msg.app.url, imgPath, options, (err) => {
                if (err) {
                    console.error(err);
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    };
};
