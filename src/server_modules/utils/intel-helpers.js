const dns = require('dns');
const PromiseB = require('bluebird');
const neo4jDriver = require('neo4j-driver').v1;
const config = require('app-config');
const neoHelpers = require('helpers');
const extraHelpers = require('../../server/api/helpers.js');
const domainFinderHelpers = require('../intel/domain-finder/helpers.js');
const neo4jRetried = require('@ambassify/neo4j-retried');
const queries = require("./cypher-queries.js");
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass),
    {
        maxConnectionLifetime: 20 * 60 * 1000,
        connectionTimeout: 1000 * 45,
        connectionAcquisitionTimeout: 600000,
        maxTransactionRetryTime: 10000,
        connectionPoolSize: 1000
    }), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});
const moment = require('moment');
const IPToASN = require('ip-to-asn');
module.exports.shodanKey = 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA';
const providers = require('../../../config/providers');
const whiteList = require('../../../config/whiteListProviderKeywords');
const helpers = require('./utils-helpers');
const ip2AsnClient = new IPToASN();
const neoAndMongoHelpers = require('helpers');
const request = require('request-promise');
const consts = require('../../../config/consts');
const {DEFAULT_INTEL_SCORE_RATIOS, STATUS_SSL, DISCOVERED_DOMAINS_SOURCES} = require('../../../config/server-consts.js');
const {BLACKLISTS_TYPE, SSL_CERTS_TYPE, BOTNETS_TYPE, EMAIL_BREACHES_TYPE, DATALEAKS_TYPE, GDPR_NOT_COMPLY_TYPE} = require('../../../config/consts');

const mongo = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
const mongoUrl = config.addresses.mongoConnectionString;
const collectionName = 'scan_map';
const ObjectId = mongo.ObjectId;

const calcImpactNum = 3;

module.exports.DEFAULT_INTEL_SCORE_RATIOS = DEFAULT_INTEL_SCORE_RATIOS;

module.exports.createNode = (label, type, props) => {
    return new PromiseB((resolve, reject) => {
        console.log('Creating first domain node');
        const session = neo4j.session();
        const writeTxResultPromise = session.writeTransaction((transaction) => {
            return transaction.run(queries.createNodeCommand(label, type, props));
        });

        return writeTxResultPromise
            .then((result) => {
                console.log('Go result');
                session.close();
                return resolve(result.records[0].keys[0].n);
            })
            .catch((error) => {
                console.error('createNode: ', error);
                return reject();
            });
    });
};

module.exports.detachNodes = (data) => {
    return new PromiseB(async (resolve, reject) => {
        console.log('On detachNodes');
        const session = neo4j.session();
        try {
            await session.writeTransaction((transaction) => {
                return transaction.run(queries.detachNodeCommand(data));
            });
            console.log('Detach Nodes Success');
            session.close();
            return resolve();
        } catch (error) {
            console.error('detachNode: ', error);
            session.close();
            return reject();
        }
    });
};

module.exports.objectToString = (props) => {
    if (typeof props !== 'object') {
        return '';
    }
    let propsStr = '{';
    let firstItem = true;
    for (let key in props) {
        if (props.hasOwnProperty(key)) {
            let value = props[key];
            if (key && value) {
                value = escapeHtml(value.toString());
                key = key.split(' ').join('_');
                if (firstItem) {
                    firstItem = false;
                    propsStr += key + ':"' + value + '"';
                } else {
                    propsStr += ' , ' + key + ' : "' + value + '"';
                }
            }
        }
    }
    propsStr += '}';
    return propsStr;
};

module.exports.lookUp = (domain, scanId, date, isSubdomain, companyId, companyName) => {
    return new Promise(async (resolve) => {
        if (domain) {
            let fromNodeLabelName = 'domain';
            domain = helpers.extractHostname(domain);

            if (domain === 'no_domain') {
                let noDomainIpsArr = [];
                try {
                    noDomainIpsArr = await getNoDomainIps(companyId);
                    if (noDomainIpsArr && noDomainIpsArr.length > 0) {
                        await updateNoDomainAndIpNode(noDomainIpsArr, scanId, date, companyName);
                        let returnArr = [];
                        if (noDomainIpsArr && noDomainIpsArr.length > 0) {
                            noDomainIpsArr.map((curIp) => {
                                if (curIp.address) {
                                    returnArr.push(curIp.address);
                                }
                            });
                        }
                        resolve(returnArr);
                    } else {
                        console.log('No IP\'s found for no_domain');
                        resolve([]);
                    }
                } catch (e) {
                    console.error('noDomain Error: ', e);
                    resolve(noDomainIpsArr);
                }
            } else {
                dns.resolve4(domain, async (err, resultArr) => {
                    console.log('Connecting IPs to domain names.');
                    if (err) {
                        console.error('Error in dns resolve: ', err);
                        resolve([]);
                    }

                    if (resultArr && resultArr.length > 0) {
                        let relArr = [];
                        for (let i = 0; i < resultArr.length; i++) {
                            let data = {
                                resolve_time: date, //Set once and can't change in query
                                scanTime: date, //Set every scan.
                                scanId: scanId
                            };

                            if (isSubdomain) {
                                fromNodeLabelName = 'subdomain';
                            }

                            let relData = {
                                fromNodeLabelName: fromNodeLabelName,
                                fromNodeProps: {hostname: domain},
                                newNodeLabelName: 'IP',
                                relationshipLabel: 'IP',
                                data: data,
                                toNodeLabel: resultArr[i],
                                toNodeType: 'IP',
                                toNodeProps: {address: resultArr[i]}
                            };

                            relArr.push(relData);
                        }
                        await neoAndMongoHelpers.createNodeAndRelationship(relArr);
                        resolve(resultArr);
                    } else {
                        resolve(resultArr);
                    }
                });
            }
        } else {
            console.log('No domain received in lookUp');
            resolve([]);
        }

    });
};

module.exports.resolveDmarc = (dmarcStr, domain, scanId, date) => {
    return new PromiseB(resolve => {
        if (domain && dmarcStr) {
            domain = helpers.extractHostname(domain);
            console.log('extracted hostname for DMARC: ', domain);
            dns.resolveTxt(dmarcStr, (err, resultArr) => {
                if (err) {
                    console.log(err);
                    let resultArr = ['Error resolving DMARC record'];
                    this.saveDMARC(resultArr, domain, scanId, date);
                } else {
                    if (resultArr) {
                        console.log('Found DMARC: ', resultArr);
                        this.saveDMARC(resultArr, domain, scanId, date);
                        resolve([]);
                    } else {
                        console.log('Didn\'t find DMARC: ', resultArr);
                        let resultArr = ['no record'];
                        this.saveDMARC(resultArr, domain, scanId, date);
                        resolve([]);
                    }
                }
            });
        } else {
            resolve([]);
        }
    });
};

module.exports.resolveSpf = (domain, scanId, date) => {
    return new PromiseB(resolve => {
        if (domain) {
            domain = helpers.extractHostname(domain);
            console.log('extracted hostname for SPF: ', domain);
            dns.resolveTxt(domain, (err, resultArr) => {
                if (err) {
                    console.log(err);
                    let resultArr = ['Error resolving spf record'];
                    this.saveSpf(resultArr, domain, scanId, date);
                } else {
                    if (resultArr) {
                        console.log('Found SPF: ', resultArr);
                        this.saveSpf(resultArr, domain, scanId, date);
                    } else {
                        console.log('Didn\'t find SPF: ', resultArr);
                        let resultArr = ['no record'];
                        this.saveSpf(resultArr, domain, scanId, date);
                    }
                }
            });
        } else {
            resolve([]);
        }
    });
};

module.exports.saveSpf = async (spfArr, brandDomain, scanId, date) => {
    const relArr = [];
    for (let i = 0; i < spfArr.length; i++) {
        for (let j = 0; j < spfArr[i].length; j++) {
            if (spfArr[i][j].indexOf('spf') !== -1) {
                const data = {
                    resolve_time: date, // Set once and can't change in query
                    scanTime: date, // Set every scan.
                    scanId: scanId
                };
                const relData = {
                    fromNodeLabelName: 'domain',
                    fromNodeProps: {hostname: brandDomain},
                    newNodeLabelName: 'spf',
                    relationshipLabel: 'spf',
                    data: data,
                    toNodeLabel: 'spf',
                    toNodeType: 'spf',
                    toNodeProps: {spf: spfArr[i][j], related: brandDomain}
                };
                relArr.push(relData);
            }
        }
    }

    if (relArr.length === 0) {
        const data = {
            resolve_time: date, // Set once and can't change in query
            scanTime: date, // Set every scan.
            scanId: scanId
        };
        const relData = {
            fromNodeLabelName: 'domain',
            fromNodeProps: {hostname: brandDomain},
            newNodeLabelName: 'spf',
            relationshipLabel: 'spf',
            data: data,
            toNodeLabel: 'spf',
            toNodeType: 'spf',
            toNodeProps: {spf: 'No Records Found', related: brandDomain}
        };
        relArr.push(relData);
    }

    return await neoAndMongoHelpers.createNodeAndRelationship(relArr);
};

module.exports.saveDMARC = async (dmarcArr, brandDomain, scanId, date) => {
    const relArr = [];
    for (let i = 0; i < dmarcArr.length; i++) {
        for (let j = 0; j < dmarcArr[i].length; j++) {
            if (dmarcArr[i][j] && dmarcArr[i][j].toLowerCase().indexOf('dmarc') !== -1) {
                const data = {
                    resolve_time: date, // Set once and can't change in query
                    scanTime: date, // Set every scan.
                    scanId: scanId
                };
                const relData = {
                    fromNodeLabelName: 'domain',
                    fromNodeProps: {hostname: brandDomain},
                    newNodeLabelName: 'dmarc',
                    relationshipLabel: 'dmarc',
                    data: data,
                    toNodeLabel: 'dmarc',
                    toNodeType: 'dmarc',
                    toNodeProps: {dmarc: dmarcArr[i][j], related: brandDomain}
                };
                relArr.push(relData);
            }
        }
    }

    if (relArr.length === 0) {
        const data = {
            resolve_time: date, // Set once and can't change in query
            scanTime: date, // Set every scan.
            scanId: scanId
        };
        const relData = {
            fromNodeLabelName: 'domain',
            fromNodeProps: {hostname: brandDomain},
            newNodeLabelName: 'dmarc',
            relationshipLabel: 'dmarc',
            data: data,
            toNodeLabel: 'dmarc',
            toNodeType: 'dmarc',
            toNodeProps: {dmarc: 'No Records Found', related: brandDomain}
        };
        relArr.push(relData);
    }

    return await neoAndMongoHelpers.createNodeAndRelationship(relArr);
};

const isNSResultAlreadyScanned = (nsNewResult, scanId) => {
    return new Promise((resolve) => {
        try {
            if (nsNewResult && scanId) {
                MongoClient.connect(mongoUrl, async (err, client) => {
                    if (client) {
                        const db = client.db('darkvision');
                        const scansCollection = db.collection('scan_map');
                        const find = PromiseB.promisify(scansCollection.findOne, {context: scansCollection});
                        try {
                            const findScanQuery = {scanId: scanId};
                            let scanResult = await find(findScanQuery);
                            if (scanResult) {
                                if (scanResult.hasOwnProperty('scannedNSList') && Array.isArray(scanResult.scannedNSList)
                                    && scanResult.scannedNSList.includes(nsNewResult)) {
                                    client.close();
                                    resolve(true);
                                } else {
                                    // Add newNSResult to scanned array.
                                    const update = PromiseB.promisify(scansCollection.update, {context: scansCollection});
                                    await update(findScanQuery, {$addToSet: {scannedNSList: nsNewResult}});
                                    client.close();
                                    resolve(false);
                                }
                            } else {
                                console.error('Error in isNSResultAlreadyScanned: Could not find scanId: ', scanId);
                                client.close();
                                resolve(false);
                            }
                        } catch (e) {
                            client.close();
                            console.error('Error in isNSResultAlreadyScanned-findOne: ' + e);
                            resolve(false);
                        }
                    } else {
                        if (err) {
                            console.error('Error in isNSResultAlreadyScanned-MongoClient.connect: ' + err);
                        }
                        resolve(false);
                    }
                });
            } else {
                resolve(false);
            }
        } catch (e) {
            console.error('Error in isNSResultAlreadyScanned-MongoClient.connect: ', e);
            resolve(false);
        }
    });
};

module.exports.isNSResultAlreadyScanned = isNSResultAlreadyScanned;

const isNSResultRelatedToCompany = (nsNewResult, existingNSResults, rootDomain, scanId, allCompanyDomains) => {
    return new Promise(async (resolve) => {
        if (nsNewResult &&
            existingNSResults &&
            !existingNSResults.includes(nsNewResult) &&
            (nsNewResult.includes(rootDomain) ||
                (allCompanyDomains && allCompanyDomains.find((d) => {
                    return nsNewResult.includes(helpers.getBrandName(d));
                }))
            )
        ) {
            // check if scanId contains nsNewResult.
            if (!await isNSResultAlreadyScanned(nsNewResult, scanId)) {
                resolve(true);
            } else {
                resolve(false);
            }
        } else {
            resolve(false);
        }
    });
};

module.exports.isNSResultRelatedToCompany = isNSResultRelatedToCompany;

const promisifyResolveNS = (domain, isCalledFromTest) => {
    return new Promise((resolve, reject) => {
        try {
            if (!isCalledFromTest) {
                // The original dns.resolveNs works this way.
                dns.resolveNs(domain, (err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
            } else {
                dns.resolveNs(domain)
                    .then((res) => {
                        resolve(res);
                    })
                    .catch((e) => {
                        reject(e);
                    });
            }
        } catch (e) {
            reject(e);
        }
    });
};

module.exports.promisifyResolveNS = promisifyResolveNS;

const removeDuplicatesAndMainDomain = (mainDomain, discoveredDomains) => {
    let uniqueResults = [];
    if (mainDomain && discoveredDomains && Array.isArray(discoveredDomains)) {
        discoveredDomains.map((currDomain) => {
            // If current finding is not the main domain and not exists already - add it to result.
            if (currDomain && currDomain !== mainDomain && !uniqueResults.includes(currDomain)) {
                uniqueResults.push(currDomain);
            }
        });
    }
    return uniqueResults;
};

module.exports.resolveNS = async (domain, allDomains, scanId, isCalledFromTest = false) => {
    return new Promise(async (resolve) => {
        if (domain) {
            console.log('In resolveNS for domain ', domain);
            try {
                domain = helpers.extractHostname(domain);
                const resultArr = await promisifyResolveNS(domain, isCalledFromTest);

                let relatedNS = [];
                if (resultArr && Array.isArray(resultArr)) {
                    const rootDomain = helpers.getBrandName(domain);
                    // Check if result array related to the company.
                    const getRelatedNS = async () => {
                        return await Promise.all(resultArr.map(async (currResult) => {
                            if (await isNSResultRelatedToCompany(currResult, relatedNS, rootDomain, scanId, allDomains)) {
                                relatedNS.push(currResult);
                            }
                        }));
                    };
                    await getRelatedNS();
                }
                if (relatedNS.length > 0) {
                    console.log('resolveNS for domain ', domain, ' resulted with ', relatedNS.length, ' related NS.');

                    const promiseArr = [];
                    relatedNS.map((currNS) => {
                        // Uses viewDNS API
                        promiseArr.push(getReverseNS(currNS));
                    });

                    return PromiseB.all(promiseArr)
                        .then((res) => {
                            let newDomains = [];
                            if (res && Array.isArray(res)) {
                                res.map((currRes) => {
                                    if (currRes && Array.isArray(currRes) && currRes.length > 0) {
                                        newDomains = newDomains.concat(currRes);
                                    }
                                });
                            }
                            // Avoid duplicates in discoveredDomains.
                            newDomains = removeDuplicatesAndMainDomain(domain, newDomains);

                            // Save to mongoDB with origin: "reverseNS" and display in Admin page as an editable modal.
                            if (newDomains.length > 0) {
                                const dataToSave = {
                                    domain: domain,
                                    source: DISCOVERED_DOMAINS_SOURCES.REVERSE_NS,
                                    discoveredDomains: newDomains
                                };
                                return neoAndMongoHelpers.insertDiscoveredDomainsFindings([dataToSave])
                                    .then((result) => {
                                        if (result && result.ok) {
                                            console.log('Successfully saved new discovered domains to DB.');
                                            resolve(newDomains);
                                        } else {
                                            console.error('Failed saving new discovered domains to DB: ', (result && result.err) ? result.err : 'couldn\'t identify the source of the error');
                                            resolve([]);
                                        }
                                    })
                                    .catch((err) => {
                                        console.error('Error in resolveNS-insertDiscoveredDomainsFindings: ', err);
                                        resolve([]);
                                    });
                            } else {
                                console.log('resolveNS for domain ', domain, ' resulted without any new discovered domains.');
                                resolve([]);
                            }
                        })
                        .catch((err) => {
                            console.error('Error in resolveNS-PromiseB.all on domain ', domain, ' : ', err);
                            resolve([]);
                        });
                } else {
                    console.log('resolveNS for domain ', domain, ' resulted without any related NS.');
                    resolve([]);
                }
            } catch (err) {
                console.error('Error in resolveNS for ', domain, ' : ', err);
                resolve([]);
            }
        } else {
            resolve([]);
        }
    });
};

function getReverseNS(domain) {
    return new Promise((resolve) => {
        const options = {
            uri: 'https://api.viewdns.info/reversens/',
            qs: {
                ns: domain,
                apikey: consts.VIEWDNS_KEY,
                output: 'json'
            },
            json: true // Automatically parses the JSON string in the response
        };
        try {
            return request.get(options)
                .then((result) => {
                    if (result && result.response) {
                        let allDomains = [];
                        if (result.response.domains && Array.isArray(result.response.domains)) {
                            allDomains = result.response.domains.map((d) => {
                                return (d && d.domain) ? d.domain : '';
                            });
                        }

                        if (result.response['total_pages']
                            && parseInt(result.response['total_pages']) > 1) {
                            const promiseArr = [];

                            for (let i = 2; i <= result.response["total_pages"]; i++) {
                                const secondOptions = {
                                    uri: options.uri,
                                    qs: {
                                        ns: options.qs.ns,
                                        apikey: options.qs.apikey,
                                        output: options.qs.output,
                                        page: i
                                    },
                                    json: options.json // Automatically parses the JSON string in the response
                                };

                                promiseArr.push(request(secondOptions));
                            }
                            return PromiseB.all(promiseArr)
                                .then((results) => {
                                    if (results && Array.isArray(results) && results.length > 0) {
                                        results.map((result) => {
                                            if (result && result.response && result.response.domains && Array.isArray(result.response.domains)) {
                                                allDomains = allDomains.concat(result.response.domains.map((d) => {
                                                    return (d && d.domain) ? d.domain : '';
                                                }));
                                            }
                                        });

                                        resolve(allDomains);
                                    } else {
                                        resolve(allDomains);
                                    }
                                })
                                .catch((err) => {
                                    console.log('Error in Reverse NS for domain ', domain, ' : ', err);
                                    resolve(allDomains);
                                });
                        } else {
                            resolve(allDomains);
                        }
                    } else {
                        resolve();
                    }
                })
                .catch((err) => {
                    console.error('Error in getReverseNS for ', domain, ' : ', err);
                    resolve();
                });
        } catch (e) {
            console.error('Error in request reverseNS for ', domain, ' : ', e);
            resolve();
        }
    });
}

function escapeHtml(text) {
    let specials = ['"', '\\'], regex = RegExp('[' + specials.join('\\') + ']', 'g');
    return text.replace(regex, '\\$&');
}

// Return what domains this user is allowed to see by company.
module.exports.getAllowedDomainsByCompany = (usedId, context) => {
    const act = PromiseB.promisify(context.act, {context: context});
    return act('role:companies, getAllowedDomains:company', {data: {id: usedId}})
        .then((res) => {
            return res;
        })
        .catch((e) => {
            console.log('Error in getAllowedDomainsByCompany: ', e);
        });
};

const setDates = (startDate, endDate, scanArr) => {
    let sDate, eDate;
    const scans = [];
    scanArr.map((scan) => {
        if (scan.length === 1) {
            scans.push(scan[0]);
        } else if (scan.length === 2) {
            // Get the scan that is one before the current - this allows to cover over delta while new scan is running.
            scans.push(scan[1]);
        }
    });

    // Fixing bug where sDate and eDate were the same and caused empty results.
    if (scans.length === 0 && scanArr.length > 0) {
        if (scanArr.length === 1) {
            scans.push(scanArr[0]);
        } else {
            scans.push(scanArr[1]);
        }
    }

    if (startDate && endDate) {
        sDate = startDate;
        eDate = endDate;
    }
    //if no dates are received, return undefined
    return {startDate: sDate, endDate: eDate, scans: scans};
};

module.exports.setDates = setDates;

// Return what companies this user is allowed to see by company.
module.exports.getAllowedCompaniesByProject = (usedId, projectName, context) => {
    const act = PromiseB.promisify(context.act, {context: context});
    return act('role:projects, getAllowedCompaniesByProject:project', {data: {id: usedId, projectName: projectName}})
        .then((res) => {
            return res;
        })
        .catch((e) => {
            console.log('Error in getAllowedCompaniesByProject: ', e);
        });
};

// Return what domains this companies' IDs have.
module.exports.getDomainsByCompanies = (usedId, companyIDs, obj) => {
    const act = PromiseB.promisify(obj.context.act, {context: obj.context});
    return PromiseB.map(companyIDs, (companyID) => {
        return act('role:companies, get:company', {id: companyID});
    })
        .then((companies) => {
            if (companies && companies.length !== 0) {
                //const domains = ['no_domain'];
                const domains = [];
                for (let i = 0; i < companies.length; i++) {
                    if (companies[i]) {
                        for (let j = 0; j < companies[i].selectedDomains.length; j++) {
                            domains.push({domain: companies[i].selectedDomains[j], latestScanDate: companies[i].currentScanDate});
                        }
                    }
                }
                return domains;
            } else {
                return [];
            }
        });
};

module.exports.getCompaniesSortFunc = (selectedDisplaySort, orgId) => {
    let sortFunc;
    const replaceCapitalAlpha = /[^A-Z]/g;
    const replaceNum = /[^0-9]/g;
    switch (selectedDisplaySort) {
        // Highest Risk First.
        case (consts.DASHBOARD_COMPANIES_SORT_TYPES[1]): {
            sortFunc = (compA, compB) => {
                let scoreA;
                let scoreB;

                scoreA = (compA && compA.scoresData && compA.scoresData.totalScore) || 0;
                scoreB = (compB && compB.scoresData && compB.scoresData.totalScore) || 0;
                if(isNumeric(scoreA) && isNumeric(scoreB)) {
                    return(Number(scoreB) - Number(scoreA));
                } else {
                    return (scoreB - scoreA);
                }
            };
            break;
        }
        // Lowest Risk First.
        case (consts.DASHBOARD_COMPANIES_SORT_TYPES[2]): {
            sortFunc = (compA, compB) => {
                let scoreA;
                let scoreB;

                scoreA = (compA && compA.scoresData && compA.scoresData.totalScore) || 0;
                scoreB = (compB && compB.scoresData && compB.scoresData.totalScore) || 0;
                if(isNumeric(scoreA) && isNumeric(scoreB)){
                    return(Number(scoreA) - Number(scoreB));
                } else {
                    return (scoreA - scoreB);
                }
            };
            break;
        } // flagged First.
        case (consts.DASHBOARD_COMPANIES_SORT_TYPES[3]): {
                sortFunc = (compA, compB) => {
                    let scoreA;
                    let scoreB;
                    if(orgId && orgId !== 'No Id' && typeof orgId === 'string' && orgId !== '' && orgId !== ' '){
                        scoreA = (compA && compA.dataByOrg && compA.dataByOrg[orgId] && compA.dataByOrg[orgId].isUnsafeCompany) ? 0 : 1;
                        scoreB = (compB && compB.dataByOrg && compB.dataByOrg[orgId] && compB.dataByOrg[orgId].isUnsafeCompany) ? 0 : 1;
                        if (isNumeric(scoreA) && isNumeric(scoreB)) {
                            return (Number(scoreA) - Number(scoreB));
                        } else {
                            return (scoreA - scoreB);
                        }
                    }
                };
            break;
        } // flagged Last.
        case (consts.DASHBOARD_COMPANIES_SORT_TYPES[4]): {
                sortFunc = (compA, compB) => {
                    let scoreA;
                    let scoreB;
                    if(orgId && orgId !== 'No Id' && typeof orgId === 'string' && orgId !== '' && orgId !== ' '){
                        scoreA = (compA && compA.dataByOrg && compA.dataByOrg[orgId] && compA.dataByOrg[orgId].isUnsafeCompany) ? 1 : 0;
                        scoreB = (compB && compB.dataByOrg && compB.dataByOrg[orgId] && compB.dataByOrg[orgId].isUnsafeCompany) ? 1 : 0;
                        if (isNumeric(scoreA) && isNumeric(scoreB)) {
                            return (Number(scoreA) - Number(scoreB));
                        } else {
                            return (scoreA - scoreB);
                        }
                    }
                };
            break;
        }
        // Alphabetic Ascending - should sort also alphabetic and numeric.
        case (consts.DASHBOARD_COMPANIES_SORT_TYPES[5]): {
            sortFunc = (compA, compB) => {
                let nameA;
                let nameB;
                let aAlpha;
                let bAlpha;
                let aNum;
                let bNum;

                nameA = (compA && compA.companyName && compA.companyName.toUpperCase()) || '';
                nameB = (compB && compB.companyName && compB.companyName.toUpperCase()) || '';

                // This sort will work also with string that contains numbers at the end. (example: [A1, A4, A10, B1])
                aAlpha = nameA.replace(replaceCapitalAlpha, '');
                bAlpha = nameB.replace(replaceCapitalAlpha, '');

                if(aAlpha === bAlpha) {
                    aNum = parseInt(nameA.replace(replaceNum, ''), 10);
                    bNum = parseInt(nameB.replace(replaceNum, ''), 10);
                    return aNum === bNum ? 0 : aNum > bNum ? 1 : -1;
                } else {
                    return aAlpha > bAlpha ? 1 : -1;
                }
            };
            break;
        }
        // Alphabetic Descending.
        case (consts.DASHBOARD_COMPANIES_SORT_TYPES[6]): {
            sortFunc = (compA, compB) => {
                let nameA;
                let nameB;
                let aAlpha;
                let bAlpha;
                let aNum;
                let bNum;

                nameA = (compA && compA.companyName && compA.companyName.toUpperCase()) || '';
                nameB = (compB && compB.companyName && compB.companyName.toUpperCase()) || '';

                // This sort will work also with string that contains numbers at the end. (example: [B1, A10, A4, A1])
                aAlpha = nameA.replace(replaceCapitalAlpha, '');
                bAlpha = nameB.replace(replaceCapitalAlpha, '');
                if(aAlpha === bAlpha){
                    aNum = parseInt(nameA.replace(replaceNum, ''), 10);
                    bNum = parseInt(nameB.replace(replaceNum, ''), 10);
                    return aNum === bNum ? 0 : bNum > aNum ? 1 : -1;
                } else {
                    return bAlpha > aAlpha ? 1 : -1;
                }
            };
            break;
        }
        default: {
            break;
        }
    }
    return sortFunc;
};

// Return what domains this user is allowed to see by company.
module.exports.getAllowedDomainsByProject = (usedId, projectName, obj) => {
    const act = PromiseB.promisify(obj.context.act, {context: obj.context});
    return act('role:projects, list:project', {query: {projectName: projectName}})
        .then((projects) => {
            const project = projects[0];
            const companies = project.selectedCompanies;
            // Get all domains of all companies in project.
            return PromiseB.map(companies, (company) => {
                return act('role:companies, get:company', {id: company.id});
            });
        })
        .catch((e) => {
            console.log('Error in role:projects, list:project: ', e);
        })
        .then((companies) => {
            const domains = [];
            if (companies && companies.length !== 0) {
                // Prevent too much data on too many companies in Dashboard.
                if (obj.sliceStartIndex != null && obj.sliceMaxLength) {
                    companies = companies.slice(obj.sliceStartIndex, obj.sliceStartIndex + obj.sliceMaxLength);
                }
                for (let i = 0; i < companies.length; i++) {
                    if (companies[i]) {
                        for (let j = 0; j < companies[i].selectedDomains.length; j++) {
                            domains.push(companies[i].selectedDomains[j]);
                        }
                    }
                }
                return domains;
            } else {
                return [];
            }
        });
};

// Return what domains this user is allowed to see by companyID.
module.exports.getAllowedDomainsByCompanyID = (companyID, obj) => {
    const act = PromiseB.promisify(obj.context.act, {context: obj.context});
    return act('role:companies, get:company', {id: companyID})
        .then((company) => {
            const domains = [];
            if (company) {
                for (let j = 0; j < company.selectedDomains.length; j++) {
                    domains.push(company.selectedDomains[j]);
                }
            }
            return domains;
        });
};

// Return what domains this user is allowed to see by companyID.
module.exports.getAllowedDomainsByCompanyIDs = (companyIDs, obj) => {
    const act = PromiseB.promisify(obj.context.act, {context: obj.context});
    return act('role:companies, listByObjectID:company', {query: companyIDs})
        .then((companies) => {
            let domains = [];
            for (let i = 0; i < companies.length; i++) {
                if (companies[i]) {
                    for (let j = 0; j < companies[i].selectedDomains.length; j++) {
                        if (!domains.includes(companies[i].selectedDomains[j])) {
                            domains.push(companies[i].selectedDomains[j]);
                        }
                    }
                }
            }
            return domains;
        });
};

module.exports.getEmailBreachesByDomains = (domains) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(queries.queryEmailBreachesByDomains(domains));
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('getEmailBreachesByDomains: ', error);
                return reject();
            });
    });
};

module.exports.getEmailBreachByNodeID = (nodeID) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(queries.queryEmailBreachByNodeID(nodeID));
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('getEmailBreachByNodeID[' + nodeID + ']: ', error);
                return reject();
            });
    });
};

module.exports.getSurroundingNodesOfNodeID = (nodeID, nodeType, startDate, endDate, scans) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        let query = queries.queryExtraNodesString(nodeID, nodeType, startDate, endDate, scans);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getSurroundingNodesOfNodeID[' + nodeID + ']: ', error);
                    return reject();
                });
        }else{
            console.error("getSurroundingNodesOfNodeID Error for ", nodeID, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getAllIPs = (domains, startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }

        let query = queries.queryIPsString(domains, startDate, endDate, scans, isForReport, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getAllIPs: ', error);
                    return reject();
                });
        }else{
            console.error("getAllIPs Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getAllIPsByDomains = (domains) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }

        let query = queries.queryIPsByDomainsString(domains);
        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(query);
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('getAllIPsByDomains: ', error);
                return reject();
            });
    });
};

module.exports.getAllSubdomainsByDomains = (domains) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }

        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(queries.querySubdomainsByDomainsString(domains));
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('getAllSubdomainsByDomains: ', error);
                return reject();
            });
    });
};

module.exports.getAllGeoLocationsByCompanyName = (companyName, scans) => {
    return new PromiseB((resolve, reject) => {
        if (companyName && typeof companyName === 'string') {
            const session = neo4j.session();
            let query = queries.queryGeoLocationsByCompanyNameString(companyName, scans);
            if(query !== '') {
                const readTxResultPromise = session.readTransaction((transaction) => {
                    return transaction.run(query);
                });

                return readTxResultPromise
                    .then((result) => {
                        session.close();
                        const data = convertToObject(result);
                        return resolve(data);
                    })
                    .catch((error) => {
                        console.error('getAllGeoLocationsByDomains: ', error);
                        return reject();
                    });
            }else{
                console.error("getAllGeoLocationsByCompanyName Error for ", companyName, " query returns empty.");
                return resolve({});
            }
        } else {
            console.error('getAllGeoLocationsByDomains - Error no company name was found for the search.');
            return reject();
        }
    });
};

module.exports.groupSubdomainsByDomains = (domains, subdomains) => {
    const domainsWithSubdomains = [];

    if (domains && Array.isArray(domains)) {
        domains.map((currDomain) => {
            if (currDomain) {
                domainsWithSubdomains.push({domain: currDomain});
            }
        });
    }
    if (subdomains && Array.isArray(subdomains)) {
        subdomains.map((currSubdomain) => {
            if (currSubdomain && currSubdomain.domain && currSubdomain.subdomain) {
                let matchingDomain = domainsWithSubdomains.find((currDomain) => {
                    return currDomain && currDomain.domain === currSubdomain.domain;
                });
                if (matchingDomain) {
                    // Add distinct subdomains.
                    if (matchingDomain.subdomains && !matchingDomain.subdomains.includes(currSubdomain.subdomain)) {
                        matchingDomain.subdomains.push(currSubdomain.subdomain);
                    } else {
                        matchingDomain.subdomains = [currSubdomain.subdomain];
                    }
                }
            }
        });
    }

    return domainsWithSubdomains;
};

module.exports.getPortsByIP = (domains, startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.getPortsQueryString(domains, startDate, endDate, scans, isForReport, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getPortsByIP: ', error);
                    return reject();
                });
        }else{
            console.error("getPortsByIP Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getProductsByIP = (domains, startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryProductQueryString(domains, startDate, endDate, scans, isForReport, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getProductsByIP: ', error);
                    return reject();
                });
        }else{
            console.error("getProductsByIP Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getVPNByIP = (domains, startDate, endDate, scans, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryVPNQueryString(domains, startDate, endDate, scans, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('queryVPNQueryString: ', error);
                    return reject();
                });
        }else{
            console.error("getVPNByIP Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getASNByIP = (domains, startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryASNString(domains, startDate, endDate, scans, isForReport, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getASNByIP: ', error);
                    return reject();
                });
        }else{
            console.error("getASNByIP Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

const getVulnsByIP = (startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        let query = queries.queryCVEString(startDate, endDate, scans, isForReport, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getVulnsByIP: ', error);
                    return reject();
                });
        }else{
            console.error("getVulnsByIP Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getVulnsByIP = getVulnsByIP;

module.exports.getMaxCVSScore = (domains, startDate, endDate, scans) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetMaxCVSScore(domains, startDate, endDate, scans)
        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(query);
        });

        if(query !== '') {
            return readTxResultPromise
                .then((result) => {
                    console.log('In readTxResultPromise');
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getMaxCVSScore: ', error);
                    return reject();
                });
        }else{
            console.error("getMaxCVSScore Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getAndNormalizeCompaniesData = (companiesIDs, uid, act) => {
    // Get all companiesIDs' full data.
    return PromiseB.map(companiesIDs, (companyID) => {
        return act('role:companies, get:company', {id: companyID});
    }).then((companies) => {
            // Keep only essential companies data.
            companies = normalizeData(companies, true, uid);
            return companies;
        })
        .catch((e) => {
            console.log('Error in getAndNormalizeCompaniesData while fetching companies data : ', e);
        });
};

function normalizeData(data, partial, id) {
    let newDataArr = data.map((item) => {
        let newData = {};
        if (item.projectName) {
            newData.projectName = item.projectName;
        }

        if (item.companyName) {
            newData.companyName = item.companyName;
        }

        if (!partial) {
            newData.allowedUsers = item.allowedUsers;
            newData.allowedEditUsers = item.allowedEditUsers;

            if (item.keywords) {
                newData.keywords = item.keywords;
            }
        }

        // Check if user is allowed to edit company
        if (item.allowedEditUsers && item.allowedEditUsers.length > 0 && id) {
            for (let i = 0; i < item.allowedEditUsers.length; i++) {
                if (item.allowedEditUsers[i].id === id) {
                    newData.edit = true;
                }
            }
        }

        if (item.assessmentFields) {
            newData.assessmentFields = item.assessmentFields;
        }

        if (item.dataByOrg && typeof item.dataByOrg === 'object') {
            newData.dataByOrg = item.dataByOrg;
        }

        if (item.assessmentScore) {
            newData.assessmentScore = item.assessmentScore;
        }

        if (item.notificationAreaComments) {
            newData.notificationAreaComments = item.notificationAreaComments;
        }

        newData.intelScoreRatios = item.intelScoreRatios;
        if (newData.intelScoreRatios) {
            // Iterate on all supposed-to-exist intelScoreRatios to verify there is a value for each one.
            Object.keys(DEFAULT_INTEL_SCORE_RATIOS).map((currKey) => {
                if (currKey && newData.intelScoreRatios[currKey] == null) {
                    newData.intelScoreRatios[currKey] = 0;
                }
            });
        }
        if (item.IPList) {
            newData.IPList = item.IPList;
        }

        newData.selectedDomains = item.selectedDomains;
        newData.sensitivity = item.sensitivity;
        newData.ratios = item.ratios;
        newData.createDate = item.createDate;
        newData.id = item.id || item._id;
        return newData;
    });
    return (newDataArr);
}

module.exports.normalizeData = normalizeData;

function getHighestCVSSAndUpdateCvssByUserConfig(cveData, AllowedDomainsCollection) {
    let curMaxIntelScore = 0;
    let hasCriticalAsset = false;
    for (let i = 0; i < cveData.length; i++) {
        if(cveData[i] && cveData[i].cveScore){
            if((cveData[i].hostname && AllowedDomainsCollection.includes(cveData[i].hostname)) ||
                cveData[i].subdomain && AllowedDomainsCollection.includes(cveData[i].subdomain)) {
                let curCveScore = Number(cveData[i].cveScore);

                if(cveData[i] && cveData[i].importance){
                    if(cveData[i].importance === 2 || cveData[i].importance === 4){
                        curCveScore = updateScoreUsingImportanceImpact(curCveScore, cveData[i].importance);
                    } else if(cveData[i].importance === 5){
                        hasCriticalAsset = true;
                        curMaxIntelScore = 10;
                    }
                }
                if(curCveScore > curMaxIntelScore){
                    curMaxIntelScore = curCveScore;
                }
            }
        }
    }
    return {curMaxIntelScore: curMaxIntelScore, hasCriticalAsset: hasCriticalAsset};
}

module.exports.getIntelAndSurveyScoresByCVSScore = (companiesWithCVSS, uid, orgId, act) => {
    const promiseArr = [];
    companiesWithCVSS.map((company) => {
        let getIntelPromise = getIntelAndSurveyFindingsCounts(act, {
            companyID: company.id,
            uid: uid,
            orgId: orgId
        }).then((scores) => {
            return getIntelScoreParts({data: scores});
        }).catch((e) => {
            console.log('Error in getIntelAndSurveyScoresByCVSScore-getIntelAndSurveyFindingsCounts: ', e);
        }).then((scoresData) => {
            if (scoresData) {
                company.scoresData = updateCompanyDataByScoreDataField(scoresData, company);
            } else {
                console.log('Error in companyID : ', company.id, ' - intelScoreParts fetched empty results.');
            }
            return PromiseB.resolve(company);
        }).catch((e) => {
            console.log('Error in getIntelAndSurveyScoresByCVSScore-getIntelScoreParts: ', e);
        });
        promiseArr.push(getIntelPromise);
    });
    return PromiseB.all(promiseArr)
        .then((res) => {
            return res;
        })
        .catch((e) => {
            console.log('Error in getIntelAndSurveyScoresByCVSScore: ', e);
        });
};

const updateCompanyDataByScoreDataField = (scoresData, company) => {
    if (scoresData && typeof scoresData === 'object') {
        if (company && !company.intelScoreRatios && scoresData.intelScoreRatios) {
            // If current company data has no configuration of intelScoreRatios - set a default one.
            company.intelScoreRatios = scoresData.intelScoreRatios;
            delete scoresData.intelScoreRatios;
        }
        if (scoresData.intelScore != null) {
            // If intel score is calculated correctly - set it as company's intelScore.
            company.maxIntelScore = scoresData.intelScore;
            delete scoresData.intelScore;
        }
        if (scoresData.surveyData && scoresData.surveyData.surveyScore != null) {
            // If survey score is calculated correctly - set it as company's surveyScore.
            company.surveyScore = scoresData.surveyData.surveyScore;
            if (scoresData.surveyData.isNA) {
                company.isSurveyNA = true;
            }
        } else if (company && company.surveyScore != null) {
            // Otherwise, normalize the existing surveyScore(the last one saved on company).
            company.surveyScore = Math.round(company.surveyScore * 100);
        }
        if (scoresData && scoresData.countEmailBreaches && scoresData.countEmailBreaches.dataCounts) {
            // Remove EmailBreaches' dataCounts because it is not used from now on.
            delete scoresData.countEmailBreaches.dataCounts;
        }
    }

    return scoresData;
};

module.exports.updateCompanyDataByScoreDataField = updateCompanyDataByScoreDataField;

const updateResWithAssetUserInputAndMitigatedData = async (res, type, propsArr, orgId, isFromMongo = false) => {
    return new Promise(async (resolve, reject) => {
        try {
            //AMData = assetUserInputAndMitigatedData
            let AMData = [];
            if (res && Array.isArray(res) && res.length > 0 &&
                type && typeof type === 'string' && type !== '' && type !== ' ' &&
                propsArr && Array.isArray(propsArr) && propsArr.length > 0 &&
                orgId && typeof orgId === 'string' && orgId !== '' && orgId !== ' ') {

                let resultWithoutFpAndMitigated = [];
                let assetUserInputAndMitigatedResOnly = [];
                AMData = await getAssetUserInputAndMitigatedNodesByType(type, orgId, isFromMongo);
                if (AMData && AMData.length > 0) {
                    for (let j = 0; j < res.length; j++) {
                        let returnedObject = checkIfNeedToBeAddedToFinalArrByProps(res[j], AMData, propsArr, isFromMongo);

                        //add cur data to the result array if not found to be fp or mitigated
                        if (returnedObject && returnedObject.isNeedToBeAdded && !resultWithoutFpAndMitigated.includes(res[j])) {
                            if (returnedObject.importanceVal && returnedObject.importanceVal > 1) {
                                res[j].importance = returnedObject.importanceVal;
                            }
                            resultWithoutFpAndMitigated.push(res[j]);
                        }

                        //add cur data to the assetUserInput and mitigated array if found to be one of them
                        if (returnedObject && !assetUserInputAndMitigatedResOnly.includes(res[j]) && (!returnedObject.isNeedToBeAdded || returnedObject.isObjContainsImportanceVal)) {
                            returnedObject.foundIdxArr.map((curIdx) => {
                                let newObj = Object.assign({}, res[j]);
                                newObj.userEmail = AMData[curIdx].userEmail ? AMData[curIdx].userEmail : '';
                                newObj.type = AMData[curIdx].type ? AMData[curIdx].type : '';
                                newObj.importance = AMData[curIdx].type && AMData[curIdx].type === 'assetUserInput' && AMData[curIdx].importance ? AMData[curIdx].importance : null;
                                if (!assetUserInputAndMitigatedResOnly.includes(newObj)) {
                                    assetUserInputAndMitigatedResOnly.push(newObj);
                                }
                            });
                        }
                    }
                    console.log("updateResWithAssetUserInputAndMitigatedData - returning results");
                    resolve({
                        resultWithoutFpAndMitigated: resultWithoutFpAndMitigated,
                        assetUserInputAndMitigatedResOnly: assetUserInputAndMitigatedResOnly
                    });
                } else {
                    console.log("updateResWithAssetUserInputAndMitigatedData - No AMData found");
                    resolve({resultWithoutFpAndMitigated: res});
                }
            } else {
                console.log("updateResWithAssetUserInputAndMitigatedData - Missing properties, returning result");
                resolve({resultWithoutFpAndMitigated: res});
            }
        } catch (e) {
            console.error('In getIntelAndSurveyFindingsCounts - getAssetUserInputAndMitigatedNodesByType Error: ' + e);
            reject({resultWithoutFpAndMitigated: res});
        }
    });
};

module.exports.updateResWithAssetUserInputAndMitigatedData = updateResWithAssetUserInputAndMitigatedData;

const checkIfNeedToBeAddedToFinalArrByProps = (curRes, AMData, propsArr, isFromMongo) => {
    let isNeedToBeAdded = true;
    let isObjContainsImportanceVal = false;
    let foundIdxArr = [];
    let importanceVal = 0;
    if (curRes && typeof curRes === 'object' && AMData && Array.isArray(AMData) && propsArr && Array.isArray(propsArr) && propsArr.length > 0) {
        for (let i = 0; i < AMData.length; i++) {
            // am is short for assetUserInput and mitigated
            let amObj = AMData[i];
            let resObj = curRes;

            //check if the obj is not false positive or mitigate if not its been added to the final result array
            if (amObj && resObj) {
                let conditionRes = false;

                let amFirstProp = propsArr[0];
                let objFirstProp = propsArr[0];

                let amSecondProp = '';
                let objSecondProp = '';

                let amThirdProp = '';
                let objThirdProp = '';

                if (propsArr[1]) {
                    amSecondProp = propsArr[1];
                    objSecondProp = propsArr[1];
                }
                if (propsArr[2]) {
                    amThirdProp = propsArr[2];
                    objThirdProp = propsArr[2];
                }

                //change prop name in case its different from database variables name

                if (isFromMongo) {
                    amFirstProp = 'identifier';
                }
                if (objFirstProp === 'id') {
                    resObj['id'] = resObj['id'] && resObj['id'].low ? resObj['id'].low.toString() : resObj['id'] ? resObj['id'].toString() : '';
                }

                //in case entity needs to be checked by one prop
                if (propsArr.length === 1) {
                    conditionRes = amObj[amFirstProp] && resObj[objFirstProp] &&
                        amObj[amFirstProp] === resObj[objFirstProp];
                    //in case entity needs to be checked by two props
                } else if (propsArr.length === 2) {
                    conditionRes = amObj[amFirstProp] && resObj[objFirstProp] && amObj[amSecondProp] && resObj[objSecondProp] &&
                        (amObj[amFirstProp] === resObj[objFirstProp] &&
                            amObj[amSecondProp] === resObj[objSecondProp]);
                    //in case entity needs to be checked by three props
                } else if (propsArr.length === 3) {
                    conditionRes = amObj[amFirstProp] && resObj[objFirstProp] && amObj[amSecondProp] && resObj[objSecondProp] && amObj[amThirdProp] && resObj[objThirdProp] &&
                        amObj[amFirstProp] === resObj[objFirstProp] &&
                        amObj[amSecondProp] === resObj[objSecondProp] &&
                        amObj[amThirdProp] === resObj[objThirdProp];
                }

                if (conditionRes) {
                    if ((amObj.type === 'mitigated' || amObj.type === 'assetUserInput' && amObj.importance !== null && amObj.importance === 1)) {
                        isNeedToBeAdded = false;
                        foundIdxArr.push(i);
                    } else if (amObj.type === 'assetUserInput' && amObj.importance !== null && amObj.importance > 1) {
                        foundIdxArr.push(i);
                        isObjContainsImportanceVal = true;
                        importanceVal = amObj.importance;
                    }
                }
            }
        }
    } else {
        console.error('In checkIfNeedToBeAddedToFinalArrByProps() - Error: some properties are missing.');
    }

    return {
        isNeedToBeAdded: isNeedToBeAdded,
        isObjContainsImportanceVal: isObjContainsImportanceVal,
        importanceVal: importanceVal,
        foundIdxArr: foundIdxArr
    };
};

const getIntelAndSurveyFindingsCounts = (act, msg) => {
    return new Promise(async (respond) => {
        if (msg && msg.companyID) {
            let companyData;
            let scanArr = [];

            try {
                companyData = await act('role:companies, get:company', {id: msg.companyID});
            } catch (e) {
                console.error('Failed to fetch company data of id: ', msg.companyID, ' : ', e);
                respond();
            }

            try {
                scanArr = await act('role:scans, getLatestScans:scan', {company: msg.companyID});
            } catch (e) {
                console.error('Failed to fetch scan data of companyID: ', msg.companyID, ' : ', e);
                respond();
            }

            // Start and End date will be undefined here.
            const data = setDates(msg.startDate, msg.endDate, scanArr);
            // Create distinct domains array from companyData' selectedDomains.
            const AllowedDomainsCollection = [];
            if (companyData && companyData.selectedDomains) {
                companyData.selectedDomains.map((currDomain) => {
                    if (currDomain && !AllowedDomainsCollection.includes(currDomain)) {
                        AllowedDomainsCollection.push(currDomain);
                    }
                });
            }

            if (AllowedDomainsCollection.length > 0) {
                let intelScoreRatios;

                if (companyData.hasOwnProperty('intelScoreRatios')) {
                    intelScoreRatios = companyData.intelScoreRatios;

                    // Iterate on all supposed-to-exist intelScoreRatios to verify there is a value for each one.
                    Object.keys(DEFAULT_INTEL_SCORE_RATIOS).map((currKey) => {
                        if (currKey && intelScoreRatios[currKey] == null) {
                            intelScoreRatios[currKey] = 0;
                        }
                    });
                } else {
                    intelScoreRatios = DEFAULT_INTEL_SCORE_RATIOS;
                }
                const resultDNSCount = {
                    countNotConfiguredSPFs: 0,
                    countNotConfiguredDMARCs: 0,
                    totalDomains: AllowedDomainsCollection.length
                };
                let resultCVEsCount = {
                    isEmpty: true
                };
                let resultBucketsCount = {
                    isEmpty: true
                };
                let resultEmailBreachesCountByYears = {
                    isEmpty: true
                };
                let resultDataleaksCount = {
                    isEmpty: true
                };
                let resultBlacklistsCount = {
                    isEmpty: true
                };
                let resultBotnetsCount = {
                    isEmpty: true
                };
                let resultSSLCertsCount = {
                    isEmpty: true
                };
                let resultSurveyAnswerObj = {
                    isEmpty: true
                };
                const promiseArr = [];
                let companyNameInArr = companyData && companyData.companyName ? [companyData.companyName] : [];

                // getVulnsByIP' isForReport parameter is true for proper results.
                const promCVEs = new Promise(async (resolve) => {
                    let res;
                    try {
                        res = await getVulnsByIP(AllowedDomainsCollection, data.startDate, data.endDate, data.scans, true, companyNameInArr)
                    }catch(e){
                        console.error("Error in getVulnsByIP: ", e);
                    }

                    let companyMaxIntelScore = 0;
                    if (res && Array.isArray(res)) {
                        try {
                            res = await updateResWithAssetUserInputAndMitigatedData(res, 'cve', ['cve', 'cpe', 'ip'], msg.orgId);
                        } catch (e) {
                            console.error('Error in updateResWithAssetUserInputAndMitigatedData - getVulnsByIP: ', e);
                        }
                        let updatedRes = getHighestCVSSAndUpdateCvssByUserConfig(res.resultWithoutFpAndMitigated, AllowedDomainsCollection);

                        companyMaxIntelScore = updatedRes.curMaxIntelScore;
                        resultCVEsCount.hasCriticalAsset = updatedRes.hasCriticalAsset;

                        resultCVEsCount = countCVEs(res.resultWithoutFpAndMitigated, companyMaxIntelScore);
                        console.log('getVulnsByIP result array length:', res.length);

                    }
                    resolve({resultCVEsCount: resultCVEsCount, maxIntelScore: companyMaxIntelScore});
                });

                promiseArr.push(promCVEs);

                //const promSPFs = getNotConfiguredSPFsCount(AllowedDomainsCollection, data.startDate, data.endDate, data.scans)
                const promSPFs = getSpf(AllowedDomainsCollection, data.startDate, data.endDate, data.scans, true, companyNameInArr)
                    .then(async (res) => {
                        console.log(res);
                        let resultNotConfiguredSPFsCount = 0;
                        if (res && res.length > 0) {
                            try {
                                res = await updateResWithAssetUserInputAndMitigatedData(res, 'spf', ['spf', 'hostname'], msg.orgId);
                            } catch (e) {
                                console.error('Error in updateResWithAssetUserInputAndMitigatedData - getSpf: ', e);
                            }

                            resultNotConfiguredSPFsCount = countSpfOrDmarc(res.resultWithoutFpAndMitigated, 'spf');
                        }
                        resultDNSCount.countNotConfiguredSPFs = resultNotConfiguredSPFsCount.notConfiguredCount;
                        resultDNSCount.notConfiguredSPFCalcPoints = resultNotConfiguredSPFsCount.notConfiguredSum;
                        resultDNSCount.hasCriticalSPFAsset = resultNotConfiguredSPFsCount.hasCriticalAsset;
                        console.log('Not configured SPF\'s found : ', resultNotConfiguredSPFsCount);
                        return resultNotConfiguredSPFsCount;
                    }).catch((e) => {
                        console.log('Error in getIntelAndSurveyFindingsCounts - getNotConfiguredSPFsCount: ', e);
                    });

                promiseArr.push(promSPFs);
                const promDMARCs = getDmarc(AllowedDomainsCollection, data.startDate, data.endDate, data.scans, true, companyNameInArr)
                    .then(async (res) => {
                        console.log(res);
                        let resultNotConfiguredDMARCsCount = 0;
                        if (res && res.length > 0) {
                            try {
                                res = await updateResWithAssetUserInputAndMitigatedData(res, 'dmarc', ['dmarc', 'hostname'], msg.orgId);
                            } catch (e) {
                                console.error('Error in updateResWithAssetUserInputAndMitigatedData - getDmarc: ', e);
                            }

                            resultNotConfiguredDMARCsCount = countSpfOrDmarc(res.resultWithoutFpAndMitigated, 'dmarc');
                        }
                        resultDNSCount.countNotConfiguredDMARCs = resultNotConfiguredDMARCsCount.notConfiguredCount;
                        resultDNSCount.notConfiguredDMARCCalcPoints = resultNotConfiguredDMARCsCount.notConfiguredSum;
                        resultDNSCount.hasCriticalDMARCAsset = resultNotConfiguredDMARCsCount.hasCriticalAsset;
                        console.log('Not configured DMARC\'s found : ', resultNotConfiguredDMARCsCount);
                        return resultNotConfiguredDMARCsCount;
                    }).catch((e) => {
                        console.log('Error in getIntelAndSurveyFindingsCounts - getNotConfiguredSPFsCount: ', e);
                    });

                promiseArr.push(promDMARCs);

                // IsForReport in getBuckets must be true for proper results.
                const promBuckets = getBuckets(AllowedDomainsCollection, data.startDate, data.endDate, data.scans, true, companyNameInArr)
                    .then(async (res) => {
                        let bucketCounts = {};
                        if (res && res.length) {
                            let amRes;
                            if (msg.orgId && msg.orgId !== 'No Id') {
                                try {
                                    amRes = await updateResWithAssetUserInputAndMitigatedData(res, 'bucket', ['bucket'], msg.orgId);
                                } catch (e) {
                                    console.error('Error in updateResWithAssetUserInputAndMitigatedData - getBuckets: ', e);
                                }
                                if (amRes && amRes.resultWithoutFpAndMitigated) {
                                    res = amRes.resultWithoutFpAndMitigated;
                                }
                            }

                            // Count distinct Buckets.
                            bucketCounts = countBuckets(res);
                            if (bucketCounts && bucketCounts.hasOwnProperty('publicBucketsCount') && bucketCounts.hasOwnProperty('totalBucketsCount')) {
                                resultBucketsCount = {
                                    publicBucketsCount: bucketCounts.publicBucketsCount,
                                    totalBucketsCount: bucketCounts.totalBucketsCount,
                                    publicBucketsCalcPoints: bucketCounts.publicBucketsCalcPoints,
                                    hasCriticalAsset: bucketCounts.hasCriticalAsset
                                };
                            }
                            console.log('Public Buckets found: ', bucketCounts.publicBucketsCount,
                                ' out of total Buckets found : ', bucketCounts.totalBucketsCount);
                        } else {
                            console.log('No Buckets were found');
                        }
                        return bucketCounts;
                    }).catch((e) => {
                        console.log('Error in getIntelAndSurveyFindingsCounts - getBuckets: ', e);
                    });

                promiseArr.push(promBuckets);

                const promEmails = getEmailBreachesCount(AllowedDomainsCollection)
                    .then(async (res) => {
                        let amRes;
                        if (msg.orgId && msg.orgId !== 'No Id') {
                            try {
                                amRes = await updateResWithAssetUserInputAndMitigatedData(res, EMAIL_BREACHES_TYPE, ['id'], msg.orgId, true);
                            } catch (e) {
                                console.error('Error in updateResWithAssetUserInputAndMitigatedData - getEmailBreachesCount: ', e);
                            }
                            if (amRes && amRes.resultWithoutFpAndMitigated) {
                                res = amRes.resultWithoutFpAndMitigated;
                            }
                        }

                        // Count emails by years.
                        resultEmailBreachesCountByYears = countEmailBreaches(res);
                        // Print the result.
                        console.log('', resultEmailBreachesCountByYears.breachedEmailsCount,
                            ' Emails were breached through ',
                            resultEmailBreachesCountByYears.breachesYearsCount,
                            ' years.');
                        // Get total Emails count (regardless if breached or not).
                        return getTotalEmailsByDomainsCount(AllowedDomainsCollection);
                    }).catch((e) => {
                        console.log('Error in getIntelAndSurveyFindingsCounts - getEmailBreachesCount: ', e);
                    }).then((res) => {
                        if (res && res.length) {
                            resultEmailBreachesCountByYears.totalEmailsCount = res.length;
                            // Print the result.
                            console.log('Total Emails found : ', resultEmailBreachesCountByYears.totalEmailsCount);
                        } else {
                            resultEmailBreachesCountByYears.totalEmailsCount = 0;

                            console.log('No Emails were found for these domains');
                        }

                        return resultEmailBreachesCountByYears;
                    }).catch((e) => {
                        console.log('Error in getIntelAndSurveyFindingsCounts - getTotalEmailsByDomainsCount: ', e);
                    });

                promiseArr.push(promEmails);

                const promDataleaks = act('role:pastes,getDataleaksCountByCompanyID:paste', {
                    uid: msg.uid,
                    companyID: msg.companyID,
                    lastMonth: true
                }).then(async (res) => {
                    console.log('Getting results for dataleaks');
                    if (res && res.dataleaksData && res.countDataleaks) {
                        let amRes;
                        if (msg.orgId && msg.orgId !== 'No Id') {
                            try {
                                amRes = await updateResWithAssetUserInputAndMitigatedData(res.dataleaksData, DATALEAKS_TYPE, ['_id'], msg.orgId, true);
                            } catch (e) {
                                console.error('Error in updateResWithAssetUserInputAndMitigatedData - role:pastes,getDataleaksCountByCompanyID:paste: ', e);
                            }
                            if (amRes && amRes.resultWithoutFpAndMitigated) {
                                res.dataleaksData = amRes.resultWithoutFpAndMitigated;
                            }
                        }
                        let countDataleaks = countDataleaksData(res.dataleaksData);

                        resultDataleaksCount = {
                            criticalDataleaks: 0,
                            mediumDataleaks: countDataleaks.totalDataleaksCount,
                            dataleaksCalcPoints: countDataleaks.dataleaksCalcPoints,
                            hasCriticalAsset: countDataleaks.hasCriticalAsset
                        };
                        console.log('', countDataleaks.totalDataleaksCount, ' dataleaks were found.');
                    } else {
                        console.log('No dataleaks were found.');
                    }
                    return resultDataleaksCount;
                }).catch((e) => {
                    console.log('Error in updateResWithAssetUserInputAndMitigatedData - getDataleaksCountByCompanyID: ', e);
                });

                promiseArr.push(promDataleaks);

                const promBlacklists = act('role:blacklists, cmd:getDomainsFindings', {
                    uid: msg.uid,
                    domains: AllowedDomainsCollection
                }).then(async (res) => {
                    console.log('Getting results for Blacklists');
                    if (res && res.ok && Array.isArray(res.findings) && res.findings.length > 0) {
                        let fmRes;
                        if (msg.orgId && msg.orgId !== 'No Id') {
                            try {
                                fmRes = await updateResWithAssetUserInputAndMitigatedData(res.findings, BLACKLISTS_TYPE, ['_id'], msg.orgId, true);
                            } catch (e) {
                                console.error('Error in updateResWithAssetUserInputAndMitigatedData - role:blacklists, cmd:getDomainsFindings: ', e);
                            }
                            if (fmRes && fmRes.resultWithoutFpAndMitigated) {
                                res.findings = fmRes.resultWithoutFpAndMitigated;
                            }
                        }

                        console.log('', res.findings.length, ' Blacklists were found.');
                        resultBlacklistsCount = countBlacklists(res.findings);
                    } else {
                        console.log('No Blacklists were found.');
                    }
                    return resultBlacklistsCount;
                }).catch((e) => {
                    console.log('Error in updateResWithAssetUserInputAndMitigatedData - role:blacklists, cmd:getDomainsFindings: ', e);
                });

                promiseArr.push(promBlacklists);

                const promBotnets = act('role:rssFeeds, cmd:getFeedsData', {
                    companyIDs: [msg.companyID]
                }).then(async (res) => {
                    console.log('Getting results for Botnets');
                    if (res && res.ok && res.data && Array.isArray(res.data) && res.data.length > 0) {
                        let fmRes = [];
                        let feedArr = [];
                        if (msg.orgId && msg.orgId !== 'No Id') {
                            try {
                                await extraHelpers.asyncForEach(res.data, async (curFeed) => {
                                    fmRes = await updateResWithAssetUserInputAndMitigatedData(curFeed.feedData, BOTNETS_TYPE, ['id'], msg.orgId, true);
                                    if (fmRes.resultWithoutFpAndMitigated && fmRes.resultWithoutFpAndMitigated.length > 0) {
                                        feedArr.push({feedName: curFeed.feedName, feedData: fmRes.resultWithoutFpAndMitigated});
                                    }
                                });
                            } catch (e) {
                                console.log('Error in updateResWithAssetUserInputAndMitigatedData - role:rssFeeds, cmd:getFeedsData: ', e);
                            }
                            res.data = feedArr;
                        }

                        resultBotnetsCount = countBotnets(res.data, AllowedDomainsCollection);
                        console.log('', resultBotnetsCount.infectedDomainsCount, ' domains were found as Botnets infected out of a total of', resultBotnetsCount.totalDomainsCount, ' domains.');
                    } else {
                        console.log('No Botnets were found.');
                    }
                    return resultBotnetsCount;
                }).catch((e) => {
                    console.log('Error in getIntelAndSurveyFindingsCounts - Botnets getFeedsData: ', e);
                });

                promiseArr.push(promBotnets);

                const promSSLCerts = act('role:sslCertificatesQuery, cmd:getSSLCerts', {
                    companyIDs: [msg.companyID]
                }).then(async (res) => {
                    console.log('Getting results for SSL Certificates');
                    if (res && res.ok && res.data && Array.isArray(res.data) && res.data.length > 0) {
                        let fmRes = [];
                        if (msg.orgId && msg.orgId !== 'No Id') {
                            try {
                                fmRes = await updateResWithAssetUserInputAndMitigatedData(res.data, SSL_CERTS_TYPE, ['id'], msg.orgId, true);
                            } catch (e) {
                                console.log('Error in updateResWithAssetUserInputAndMitigatedData - role:sslCertificatesQuery, cmd:getSSLCerts: ', e);
                            }
                            if (fmRes && fmRes.resultWithoutFpAndMitigated) {
                                res.data = fmRes.resultWithoutFpAndMitigated;
                            }
                        }

                        resultSSLCertsCount = countSSLCerts(res.data);
                        console.log('', resultSSLCertsCount.validHttpsDomainsCount, ' domains were found with valid SSL Certificates and ', resultSSLCertsCount.notValidHttpsDomainsCount, ' domains were found without valid SSL Certificates.');
                    } else {
                        console.log('No SSL Certificates were found.');
                    }
                    return resultSSLCertsCount;
                }).catch((e) => {
                    console.log('Error in getIntelAndSurveyFindingsCounts - SSL Certificates getSSLCerts: ', e);
                });

                promiseArr.push(promSSLCerts);

                const promSurvey = act('role:surveystore, getLatestSurveyByCompanyID:surveyAnswers', {
                    companyID: msg.companyID
                })
                    .then((res) => {
                        if (res && res.data) {
                            resultSurveyAnswerObj = res.data;
                        }
                        console.log(resultSurveyAnswerObj);
                        return resultSurveyAnswerObj;
                    }).catch((e) => {
                        console.log('Error in getIntelAndSurveyFindingsCounts - getLatestSurveyByCompanyID: ', e);
                    });

                promiseArr.push(promSurvey);


                return PromiseB.all(promiseArr)
                    .then((res) => {
                        respond({
                            companyID: msg.companyID,
                            intelScoreRatios: intelScoreRatios,
                            maxIntelScore: res && res[0] && res[0].maxIntelScore ? res[0].maxIntelScore : 0,
                            countCVEs: res && res[0] && res[0].resultCVEsCount ? res[0].resultCVEsCount : resultCVEsCount,
                            countDNS: resultDNSCount,
                            countBuckets: resultBucketsCount,
                            countEmailBreaches: resultEmailBreachesCountByYears,
                            countDataleaks: resultDataleaksCount,
                            countBlacklists: resultBlacklistsCount,
                            countBotnets: resultBotnetsCount,
                            countSSLCerts: resultSSLCertsCount,
                            surveyData: resultSurveyAnswerObj
                        });
                    })
                    .catch((e) => {
                        console.log(e);
                        respond();
                    });
            } else {
                console.log('Some required input parameters are missing in getIntelAndSurveyFindingsCounts');
                respond();
            }
        }
    });
};

const getIntelScoreParts = (msg) => {
    return new Promise((respond) => {
        if (msg && msg.data) {
            const data = msg.data;
            const intelScoreRatios = data.intelScoreRatios;

            data.countCVEs = calcIntelScoreCVE(data.countCVEs);

            data.countDNS = calcIntelScoreDNS(data.countDNS);

            data.countBuckets = calcIntelScoreBuckets(data.countBuckets);

            data.countEmailBreaches = calcIntelScoreEmailBreaches(data.countEmailBreaches);

            data.countDataleaks = calcIntelScoreDataleaks(data.countDataleaks);

            data.countBlacklists = calcIntelScoreBlacklists(data.countBlacklists);

            data.countBotnets = calcIntelScoreBotnets(data.countBotnets);

            data.countSSLCerts = calcIntelScoreSSLCerts(data.countSSLCerts);

            // Normalize surveyScore.
            data.surveyData = calcSurveyScore(data.surveyData);

            // Get all scores weights.
            let cveWeight = intelScoreRatios.cveWeight || 0,
                dnsWeight = intelScoreRatios.dnsWeight || 0,
                bucketsWeight = intelScoreRatios.bucketsWeight || 0,
                breachesWeight = intelScoreRatios.breachesWeight || 0,
                dataleaksWeight = intelScoreRatios.dataleaksWeight || 0,
                blacklistsWeight = intelScoreRatios.blacklistsWeight || 0,
                botnetsWeight = intelScoreRatios.botnetsWeight || 0,
                sslCertsWeight = intelScoreRatios.sslCertsWeight || 0;

            // Calculate total intelScore.
            data.intelScore = Math.round(
                data.countCVEs.score * (cveWeight / 100)
                + data.countDNS.score * (dnsWeight / 100)
                + data.countBuckets.score * (bucketsWeight / 100)
                + data.countEmailBreaches.score * (breachesWeight / 100)
                + data.countDataleaks.score * (dataleaksWeight / 100)
                + data.countBlacklists.score * (blacklistsWeight / 100)
                + data.countBotnets.score * (botnetsWeight / 100)
                + data.countSSLCerts.score * (sslCertsWeight / 100)
            );

            console.log('Total intelScore : ', data.intelScore);

            respond(data);
        } else {
            console.log('Some required input parameters are missing in getIntelScoreParts');
            respond();
        }
    });
};

const getNotConfiguredSPFsCount = (domains, startDate, endDate, scans) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetNotConfiguredSPFsCount(domains, startDate, endDate, scans);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getNotConfiguredSPFsCount: ', error);
                    return reject();
                });
        }else{
            console.error("getNotConfiguredSPFsCount Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getNotConfiguredSPFsCount = getNotConfiguredSPFsCount;

const getNotConfiguredDMARCsCount = (domains, startDate, endDate, scans) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetNotConfiguredDMARCsCount(domains, startDate, endDate, scans);

        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getNotConfiguredDMARCsCount: ', error);
                    return reject();
                });
        }else{
            console.error("getAllIPs Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getNotConfiguredDMARCsCount = getNotConfiguredDMARCsCount;

const getEmailBreachesCount = (domains) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetEmailBreachesCount(domains);
        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(query);
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('getNotConfiguredDMARCsCount: ', error);
                return reject();
            });
    });
};

module.exports.getEmailBreachesCount = getEmailBreachesCount;

const countEmailBreaches = (emailBreaches) => {
    // resultCounts holds how many years encoutered breaches, how many Emails were breached and an array called dataCounts.
    // dataCounts holds for each year - how many Emails were breached in it.
    const resultCounts = {
        breachesYearsCount: 0,
        breachedEmailsCount: 0,
        dataCounts: [],
        breachedEmailsCalcPoints: 0,
        hasCriticalAsset: false
    };
    const uniqueYears = [];
    const uniqueEmails = [];
    const breachYear = 'breach_year', email = 'email';

    if (emailBreaches && emailBreaches.length > 0) {
        emailBreaches.map((currEntity) => {
            if (currEntity && currEntity[breachYear] && currEntity[email]) {

                if (currEntity.importance && currEntity.importance === 5) {
                    resultCounts.hasCriticalAsset = true;
                }
                // Check if current email already exists or not.
                if (!uniqueEmails.includes(currEntity[email])) {
                    uniqueEmails.push(currEntity[email]);
                }

                // Check if current year already exists or not.
                if (!uniqueYears.includes(currEntity[breachYear])) {
                    uniqueYears.push(currEntity[breachYear]);
                    // Save the new year with 1 email finding;
                    resultCounts.dataCounts.push({breachYear: currEntity[breachYear], breachedEmails: 1, breachedEmailsObjectsArr: [currEntity]});
                } else {
                    // input(emailBreaches) is ordered by years, so if the year already exists - increment it.
                    resultCounts.dataCounts[resultCounts.dataCounts.length - 1].breachedEmails++;
                    resultCounts.dataCounts[resultCounts.dataCounts.length - 1].breachedEmailsObjectsArr.push(currEntity);
                }
            }
        });

        // Save total counts.
        resultCounts.breachesYearsCount = uniqueYears.length;
        resultCounts.breachedEmailsCount = uniqueEmails.length;
    }

    return resultCounts;
};

module.exports.countEmailBreaches = countEmailBreaches;

const getTotalEmailsByDomainsCount = (domains) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetTotalEmailsByDomainsCount(domains);
        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(query);
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('getTotalEmailsByDomainsCount: ', error);
                return reject();
            });
    });
};

module.exports.getTotalEmailsByDomainsCount = getTotalEmailsByDomainsCount;

const GetCompanyExtraDataByID = (companyID) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();
        console.log("In GetCompanyExtraDataByID");
        let query = queries.queryCypherGetCompanyExtraDataByID(companyID);
        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(query);
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('Error in GetCompanyExtraDataByID: ', error);
                return reject();
            });
    });
};

module.exports.GetCompanyExtraDataByID = GetCompanyExtraDataByID;

module.exports.getDiscoveredDomains = async (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const session = neo4j.session();
            const cmd = 'MATCH (company: COMPANY)-[*0..4]->(l)-[connection]->(domain:domain) ' +
                'WHERE company.cid="' + id + '" and (domain.connectionStrength IS NOT NULL OR domain.connectionLevel IS NOT NULL) ' +
                'OPTIONAL MATCH (domain:domain)-->(whois:whois)' +
                ' return DISTINCT(domain.hostname) as domain, TYPE(connection) as connection, domain.connectionLevel as Connection_Level,' +
                ' domain.connectionStrength as Connection_Strength, whois as whois';
            console.log(cmd);
            if (cmd) {
                let readTxResultPromise = session.readTransaction((transaction) => {
                    return transaction.run(cmd);
                });

                try {
                    let results = await readTxResultPromise;

                    console.log('Done getting domains from DB');
                    session.close();

                    return resolve(results.records.map((record) => {
                        let connectionStrength = record.get('Connection_Strength');
                        let validated = false;
                        if (connectionStrength > config.consts.domainValidationScoreLimit) {
                            validated = true;
                        }

                        return {
                            domain: record.get('domain'),
                            source: record.get('connection'),
                            Connection_Level: record.get('Connection_Level'),
                            connectionStrength: record.get('Connection_Strength'),
                            whois: record.get('whois'),
                            validated: validated
                        };
                    }));
                } catch (e) {
                    console.error('Error in getDiscoveredDomains', e);
                    return reject();
                }
            }
        } catch (e) {
            console.error(e);
        }
    });
};

module.exports.updateDomainConnectionStrengthAndValidationDate = (domain, connectionStrength) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();
        let date = moment().format();
        if (domain && (connectionStrength || connectionStrength === 0)) {
            let cmd = 'MATCH (n:domain) where n.hostname = "' + domain + '"' +
                ' SET n +={connectionStrength :' + connectionStrength + ', validationDate : "' + date + '", manualValidation : true}' +
                ' RETURN n';

            console.log(cmd);

            if (cmd) {
                let writeTxResultPromise = session.writeTransaction((transaction) => {
                    return transaction.run(cmd);
                });

                return writeTxResultPromise
                    .then(() => {
                        console.log('Done updating connectionStrength');
                        session.close();
                        return resolve();
                    })
                    .catch((error) => {
                        console.error('Error in updateConnectionStrength', error);
                        return reject();
                    });
            } else {
                console.error('ERROR: No command was entered');
                return resolve();
            }
        } else {
            console.error('ERROR updateDomainConnectionStrength: No domain hostname or connectionStrength was found');
            return resolve();
        }
    });
};

const UpdateCompanyInfoStatusByID = (companyID, nodeID, checked) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        let query = queries.queryCypherUpdateCompanyInfoStatusByID(companyID, nodeID, checked);
        const readTxResultPromise = session.readTransaction((transaction) => {
            return transaction.run(query);
        });

        return readTxResultPromise
            .then((result) => {
                session.close();
                const data = convertToObject(result);
                return resolve(data);
            })
            .catch((error) => {
                console.error('Error in GetCompanyExtraDataByID: ', error);
                return reject();
            });
    });
};

module.exports.UpdateCompanyInfoStatusByID = UpdateCompanyInfoStatusByID;

const countBlacklists = (blacklists) => {
    // resultCounts holds the highest number of Blacklists found on any IP, and if there are few IPs with this number - count them too.
    // dataCounts is a key-value object where the keys are the number of blacklists for each entity and the values are counts of how many IPs with each key's Blacklists.
    const resultCounts = {
        highestTotalBlacklistsCount: 0,
        recordsWithHighestTotalBlacklistsCount: 0,
        dataCounts: {},
        hasCriticalAsset: false,
        highestBlacklistsByTypeCount: {
            domain: 0,
            ip: 0,
            mx: 0,
            ns: 0
        }
    };
    let uniqueBlacklistsForEntity;
    const domainBL = 'blacklist_domain', mxBL = 'blacklist_mx', nsBL = 'blacklist_ns', ipBL = 'blacklist_ip',
        ipBL_DD = 'blacklist';

    if (blacklists && blacklists.length > 0) {
        blacklists.map((currEntity) => {
            if (currEntity && currEntity.data && currEntity.data[domainBL] && currEntity.data[mxBL] && currEntity.data[nsBL] && currEntity.data[ipBL]) {
                uniqueBlacklistsForEntity = [];
                // Add each distinct Blacklist to total.
                uniqueBlacklistsForEntity = combineArraysWithoutDuplicates(uniqueBlacklistsForEntity, currEntity.data[domainBL]);
                uniqueBlacklistsForEntity = combineArraysWithoutDuplicates(uniqueBlacklistsForEntity, currEntity.data[mxBL]);
                uniqueBlacklistsForEntity = combineArraysWithoutDuplicates(uniqueBlacklistsForEntity, currEntity.data[nsBL]);
                // Find highest count by type.
                resultCounts.highestBlacklistsByTypeCount.domain = getMaxValue(resultCounts.highestBlacklistsByTypeCount.domain, currEntity.data[domainBL].length);
                resultCounts.highestBlacklistsByTypeCount.mx = getMaxValue(resultCounts.highestBlacklistsByTypeCount.mx, currEntity.data[mxBL].length);
                resultCounts.highestBlacklistsByTypeCount.ns = getMaxValue(resultCounts.highestBlacklistsByTypeCount.ns, currEntity.data[nsBL].length);
                if (currEntity.data[ipBL][ipBL_DD]) {
                    // Add each distinct Blacklist to total.
                    uniqueBlacklistsForEntity = combineArraysWithoutDuplicates(uniqueBlacklistsForEntity, currEntity.data[ipBL][ipBL_DD]);
                    // Find highest count by type.
                    resultCounts.highestBlacklistsByTypeCount.ip = getMaxValue(resultCounts.highestBlacklistsByTypeCount.ip, currEntity.data[ipBL][ipBL_DD].length);
                }
                // Update resultCounts according to matching situation.
                if (uniqueBlacklistsForEntity.length > 0) {
                    if (uniqueBlacklistsForEntity.length > resultCounts.highestTotalBlacklistsCount) {
                        resultCounts.highestTotalBlacklistsCount = uniqueBlacklistsForEntity.length;
                        resultCounts.recordsWithHighestTotalBlacklistsCount = 1;
                        resultCounts.dataCounts[uniqueBlacklistsForEntity.length] = {count: 1, entityData: currEntity};
                    } else if (uniqueBlacklistsForEntity.length === resultCounts.highestTotalBlacklistsCount) {
                        resultCounts.recordsWithHighestTotalBlacklistsCount++;
                        resultCounts.dataCounts[uniqueBlacklistsForEntity.length].count++;
                        resultCounts.dataCounts[uniqueBlacklistsForEntity.length].entityData = currEntity;
                    } else {
                        if (resultCounts.dataCounts.hasOwnProperty(uniqueBlacklistsForEntity.length)) {
                            resultCounts.dataCounts[uniqueBlacklistsForEntity.length].count++;
                            resultCounts.dataCounts[uniqueBlacklistsForEntity.length].entityData = currEntity;
                        } else {
                            resultCounts.dataCounts[uniqueBlacklistsForEntity.length] = {count: 1, entityData: currEntity};
                        }
                    }
                }
            }
        });
    }

    return resultCounts;
};

module.exports.countBlacklists = countBlacklists;

const countBotnets = (botnetsFindings, allDomains) => {
    // resultCounts holds the number of Botnets infected domains and the number of all existing domains.
    const resultCounts = {
        infectedDomainsCount: 0,
        totalDomainsCount: 0,
        infectedDomainsCalcPoints: 0,
        hasCriticalAsset: false
    };

    if (allDomains && Array.isArray(allDomains)) {
        resultCounts.totalDomainsCount = allDomains.length;
    }

    if (botnetsFindings && Array.isArray(botnetsFindings)) {
        let uniqueInfectedDomains = [];

        botnetsFindings.map((currFinding) => {
            if (currFinding && currFinding.feedData && Array.isArray(currFinding.feedData)) {
                currFinding.feedData.map((currFeedData) => {
                    if (currFeedData && currFeedData.domain && !uniqueInfectedDomains.includes(currFeedData.domain)) {
                        uniqueInfectedDomains.push(currFeedData.domain);

                        if (currFeedData.importance) {
                            if (currFeedData.importance === 5) { //critical risk asset
                                resultCounts.hasCriticalAsset = true;
                            } else if (currFeedData.importance === 4) { //high risk asset
                                resultCounts.infectedDomainsCalcPoints += (10 * calcImpactNum);
                            } else if (currFeedData.importance === 2) { //low risk asset
                                resultCounts.infectedDomainsCalcPoints += (10 / calcImpactNum);
                            } else { // medium risk asset
                                resultCounts.infectedDomainsCalcPoints += 10;
                            }
                        } else { // medium risk asset
                            resultCounts.infectedDomainsCalcPoints += 10;
                        }
                    }
                });
            }
        });

        resultCounts.infectedDomainsCount = uniqueInfectedDomains.length;
    }

    return resultCounts;
};

module.exports.countBotnets = countBotnets;

const countSSLCerts = (sslCertsFindings) => {
    // resultCounts holds the number of SSL Certificates configured and not configured domains.
    const resultCounts = {
        validHttpsDomainsCount: 0,
        notValidHttpsDomainsCount: 0,
        sslCalcPoints: 0,
        hasCriticalAsset: false
    };

    if (sslCertsFindings && Array.isArray(sslCertsFindings)) {
        let uniqueHttpsDomains = [];
        let uniqueNotHttpsDomains = [];

        sslCertsFindings.map((currFinding) => {
            if (currFinding && currFinding.domain && currFinding.statusSSL) {
                // This array will point to matching result array - whether this finding is valid or not.
                let arrayToPushCurrFinding;
                let notHttpsDomainsFlag = false;
                if (currFinding.statusSSL !== 'offline' && currFinding.statusSSL !== 'timeout' && currFinding.statusSSL !== 'redirect') {
                    if (currFinding.statusSSL === STATUS_SSL.HTTPS && currFinding.valid) {
                        arrayToPushCurrFinding = uniqueHttpsDomains;
                    } else {
                        arrayToPushCurrFinding = uniqueNotHttpsDomains;
                        notHttpsDomainsFlag = true;
                    }

                    if (!arrayToPushCurrFinding.includes(currFinding.domain)) {
                        arrayToPushCurrFinding.push(currFinding.domain);
                        if (notHttpsDomainsFlag) {
                            if (currFinding.importance) {
                                if (currFinding.importance === 5) { //critical risk asset
                                    resultCounts.hasCriticalAsset = true;
                                } else if (currFinding.importance === 4) { //high risk asset
                                    resultCounts.sslCalcPoints += (10 * calcImpactNum);
                                } else if (currFinding.importance === 2) { //low risk asset
                                    resultCounts.sslCalcPoints += (10 / calcImpactNum);
                                } else { // medium risk asset
                                    resultCounts.sslCalcPoints += 10;
                                }
                            } else { // medium risk asset
                                resultCounts.sslCalcPoints += 10;
                            }
                        }
                    }
                }

                // Do the same for the subdomains (if any are exist).
                if (currFinding.subdomains && Array.isArray(currFinding.subdomains)) {
                    currFinding.subdomains.map((currSubdomain) => {
                        let notHttpsSubDomainsFlag = false;
                        if (currSubdomain && currSubdomain.statusSSL &&
                            currSubdomain.statusSSL !== 'offline' && currSubdomain.statusSSL !== 'timeout' && currSubdomain.statusSSL !== 'redirect') {
                            if (currSubdomain && currSubdomain.statusSSL === STATUS_SSL.HTTPS && currSubdomain.valid) {
                                arrayToPushCurrFinding = uniqueHttpsDomains;
                            } else {
                                arrayToPushCurrFinding = uniqueNotHttpsDomains;
                                notHttpsSubDomainsFlag = true;
                            }

                            if (!arrayToPushCurrFinding.includes(currSubdomain.domain)) {
                                arrayToPushCurrFinding.push(currSubdomain.domain);
                                if (notHttpsSubDomainsFlag) {
                                    if (currSubdomain.importance) {
                                        if (currSubdomain.importance === 5) { //critical risk asset
                                            resultCounts.hasCriticalAsset = true;
                                        } else if (currSubdomain.importance === 4) { //high risk asset
                                            resultCounts.sslCalcPoints += (10 * calcImpactNum);
                                        } else if (currSubdomain.importance === 2) { //low risk asset
                                            resultCounts.sslCalcPoints += (10 / calcImpactNum);
                                        } else { // medium risk asset
                                            resultCounts.sslCalcPoints += 10;
                                        }
                                    } else { // medium risk asset
                                        resultCounts.sslCalcPoints += 10;
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });

        resultCounts.validHttpsDomainsCount = uniqueHttpsDomains.length;
        resultCounts.notValidHttpsDomainsCount = uniqueNotHttpsDomains.length;
    }

    return resultCounts;
};

module.exports.countSSLCerts = countSSLCerts;

function groupScoreDataObject(object, keysArrayToGroup) {
    // This function adds to object the given keys as a single object called [scoreDataKey].
    if (object && keysArrayToGroup && Array.isArray(keysArrayToGroup) && keysArrayToGroup.length > 0) {
        const scoreDataKey = 'scoreData';
        object[scoreDataKey] = {};
        keysArrayToGroup.map((currKey) => {
            if (currKey && object.hasOwnProperty(currKey)) {
                object[scoreDataKey][currKey] = object[currKey];
                delete object[currKey];
            }
        });
    }
    return object;
}

const calcIntelScoreCVE = (countCVEs) => {
    if (countCVEs && !countCVEs.isEmpty) {
        if (countCVEs.hasCriticalAsset) {
            countCVEs.score = 100;
        } else {
            countCVEs.score = Math.round(countCVEs.maxIntelScore * 10);
            if (countCVEs.score && countCVEs.score > 100) {
                countCVEs.score = 100;
            }
        }
        countCVEs = groupScoreDataObject(countCVEs, ['criticalCVSSCount', 'mediumCVSSCount', 'hasCriticalAsset']);
    } else {
        countCVEs = {
            score: 0,
            scoreData: {
                criticalCVSSCount: 0,
                mediumCVSSCount: 0,
                hasCriticalAsset: false
            }
        };
    }
    return countCVEs;
};

module.exports.calcIntelScoreCVE = calcIntelScoreCVE;

const calcIntelScoreDNS = (countDNS) => {
    if (countDNS) {
        if (countDNS.hasCriticalDMARCAsset || countDNS.hasCriticalSPFAsset) {
            countDNS.score = 100;
        } else {
            const spfScore = countDNS.notConfiguredSPFCalcPoints / countDNS.totalDomains * 10;
            const dmarcScore = countDNS.notConfiguredDMARCCalcPoints / countDNS.totalDomains * 10;
            countDNS.score = Math.round(((spfScore + dmarcScore) / 2) * 100);
            if (countDNS.score > 100) {
                countDNS.score = 100;
            }
        }
        countDNS = groupScoreDataObject(countDNS, ['countNotConfiguredSPFs', 'countNotConfiguredDMARCs', 'hasCriticalAsset']);
    } else {
        countDNS = {
            score: 0,
            scoreData: {
                countNotConfiguredSPFs: 0,
                countNotConfiguredDMARCs: 0,
                hasCriticalAsset: false
            }
        };
    }
    return countDNS;
};

module.exports.calcIntelScoreDNS = calcIntelScoreDNS;

const calcIntelScoreBuckets = (countBuckets) => {
    if (countBuckets && !countBuckets.isEmpty) {
        if (countBuckets.totalBucketsCount !== 0) {
            if (countBuckets.hasCriticalAsset) {
                countBuckets.score = 100;
            } else {
                countBuckets.score = Math.round((countBuckets.publicBucketsCalcPoints / (countBuckets.totalBucketsCount * 10)) * 100);
            }
        } else {
            countBuckets.score = 0;
        }
        countBuckets = groupScoreDataObject(countBuckets, ['publicBucketsCount', 'totalBucketsCount', 'hasCriticalAsset']);
    } else {
        countBuckets = {
            score: 0,
            scoreData: {
                publicBucketsCount: 0,
                totalBucketsCount: 0,
                hasCriticalAsset: false
            }
        };
    }
    return countBuckets;
};

module.exports.calcIntelScoreBuckets = calcIntelScoreBuckets;

const calcIntelScoreEmailBreaches = (countEmailBreaches) => {
    if (countEmailBreaches && !countEmailBreaches.isEmpty) {
        // Calc by hierarchic status. differVal is the difference between current year and breach year. points are how much it worth.
        const hierarchicPoints = [
            {differVal: 0, points: 10},
            {differVal: 1, points: 8},
            {differVal: 2, points: 6},
            {differVal: 3, points: 4},
            {differVal: 4, points: 2}
        ];
        let scoreSum = 0;
        if (countEmailBreaches.hasCriticalAsset) {
            countEmailBreaches.score = 100;
        } else {
            let hasOldBreaches = false;
            const todayYear = moment().year();
            for (let i = 0; i < countEmailBreaches.dataCounts.length; i++) {
                const currEntity = countEmailBreaches.dataCounts[i];
                const currEntityYear = Number(currEntity.breachYear);
                const differVal = todayYear - currEntityYear;
                // Get the last year plus one. That means that this finding is marked as old.
                if (differVal >= hierarchicPoints[hierarchicPoints.length - 1].differVal + 1) {
                    hasOldBreaches = true;
                } else {
                    const matchingObj = hierarchicPoints.find((h) => {
                        return h.differVal === differVal;
                    });
                    let breachedEmailsObjectsArr = countEmailBreaches.dataCounts[i].breachedEmailsObjectsArr;

                    if (matchingObj && matchingObj.points != null && breachedEmailsObjectsArr &&
                        Array.isArray(breachedEmailsObjectsArr) && breachedEmailsObjectsArr.length > 0) {
                        breachedEmailsObjectsArr.map((curEmail) => {
                            if (curEmail.importance) {
                                if (curEmail.importance === 4) { //high risk asset
                                    scoreSum += (matchingObj.points * calcImpactNum);
                                } else if (curEmail.importance === 2) { //low risk asset
                                    scoreSum += (matchingObj.points / calcImpactNum);
                                } else { // medium risk asset
                                    scoreSum += matchingObj.points;
                                }
                            } else { // medium risk asset
                                scoreSum += matchingObj.points;
                            }
                        });
                    }
                    if (scoreSum > 100) {
                        scoreSum = 100;
                        break;
                    }
                }
            }
            if (scoreSum === 0 && hasOldBreaches) {
                scoreSum = 10;
            }

            countEmailBreaches.score = Math.floor(scoreSum);
        }
        countEmailBreaches = groupScoreDataObject(countEmailBreaches, ['breachedEmailsCount', 'totalEmailsCount', 'hasCriticalAsset']);
    } else {
        countEmailBreaches = {
            score: 0,
            scoreData: {
                breachedEmailsCount: 0,
                totalEmailsCount: 0,
                hasCriticalAsset: false
            }
        };
    }
    return countEmailBreaches;
};

module.exports.calcIntelScoreEmailBreaches = calcIntelScoreEmailBreaches;

const calcIntelScoreDataleaks = (countDataleaks) => {
    if (countDataleaks && !countDataleaks.isEmpty) {
        if (countDataleaks.hasCriticalAsset) {
            countDataleaks.score = 100;
        } else {
            countDataleaks.score = countDataleaks.dataleaksCalcPoints;
            if (countDataleaks.score > 100) {
                countDataleaks.score = 100;
            }
        }
        countDataleaks = groupScoreDataObject(countDataleaks, ['criticalDataleaks', 'mediumDataleaks', 'hasCriticalAsset']);
    } else {
        countDataleaks = {
            score: 0,
            scoreData: {
                criticalDataleaks: 0,
                mediumDataleaks: 0,
                hasCriticalAsset: false
            }
        };
    }
    return countDataleaks;
};

module.exports.calcIntelScoreDataleaks = calcIntelScoreDataleaks;

const calcIntelScoreBlacklists = (countBlacklists) => {
    if (countBlacklists && !countBlacklists.isEmpty) {
        // Calc by hierarchic status. differVal is the total number of blacklists found for each entity. points are how much it worth.
        const hierarchicPoints = [
            {differVal: 5, points: 50},
            {differVal: 4, points: 8},
            {differVal: 3, points: 6},
            {differVal: 2, points: 4},
            {differVal: 1, points: 2}
        ];

        let scoreSum = 0;

        let hasCriticalAsset = false;
        let dataCountsData = [];
        Object.keys(countBlacklists.dataCounts).map((curkey) => {
            countBlacklists.dataCounts[curkey].blacklistsAmount = Number(curkey);
            dataCountsData.push(countBlacklists.dataCounts[curkey]);
        });
        if (dataCountsData && Array.isArray(dataCountsData)) {
            for (let i = 0; i < dataCountsData.length; i++) {
                const matchingObj = hierarchicPoints.find((h) => {
                    let res = false;
                    if (dataCountsData[i] && dataCountsData[i].blacklistsAmount) {
                        res = dataCountsData[i].blacklistsAmount >= h.differVal;
                    } else {
                        console.log('In calcIntelScoreBlacklists() - count prop dont exists.');
                    }
                    return res;
                });

                if (matchingObj && matchingObj.points != null && dataCountsData[i] && dataCountsData[i].count) {
                    if (dataCountsData[i].entityData && dataCountsData[i].entityData.importance) {
                        if (dataCountsData[i].entityData.importance === 5) { //high risk asset
                            scoreSum = 100;
                            hasCriticalAsset = true;
                        } else if (dataCountsData[i].entityData.importance === 4) { //high risk asset
                            scoreSum += (dataCountsData[i].count * matchingObj.points * calcImpactNum);
                        } else if (dataCountsData[i].entityData.importance === 2) { //low risk asset
                            scoreSum += ((dataCountsData[i].count * matchingObj.points) / calcImpactNum);
                        } else { // medium risk asset
                            scoreSum += dataCountsData[i].count * matchingObj.points;
                        }
                    } else { // medium risk asset
                        scoreSum += dataCountsData[i].count * matchingObj.points;
                    }
                }

                if (scoreSum > 100) {
                    scoreSum = 100;
                    break;
                }
            }
        }

        countBlacklists.score = Math.floor(scoreSum);

        countBlacklists.scoreData = getTopTwoHighestBlacklistsByType(countBlacklists.highestBlacklistsByTypeCount);
        countBlacklists.scoreData.hasCriticalAsset = hasCriticalAsset;
        delete countBlacklists.highestBlacklistsByTypeCount;
    } else {
        countBlacklists = {
            score: 0,
            hasCriticalAsset: false,
            scoreData: getTopTwoHighestBlacklistsByType(null)
        };
    }
    return countBlacklists;
};

module.exports.calcIntelScoreBlacklists = calcIntelScoreBlacklists;

const calcIntelScoreBotnets = (countBotnets) => {
    if (countBotnets && !countBotnets.isEmpty && countBotnets.totalDomainsCount) {
        if (countBotnets.hasCriticalAsset) {
            countBotnets.score = 100;
        } else {
            //if we have one or more infected domains the score will be 100 otherwise 0
            countBotnets.score = countBotnets.infectedDomainsCalcPoints && countBotnets.infectedDomainsCalcPoints > 0 ? 100 : 0;
        }

        countBotnets = groupScoreDataObject(countBotnets, ['infectedDomainsCount', 'totalDomainsCount', 'hasCriticalAsset']);
    } else {
        countBotnets = {
            score: 0,
            scoreData: {
                infectedDomainsCount: 0,
                totalDomainsCount: 0,
                hasCriticalAsset: false
            }
        };
    }
    return countBotnets;
};

module.exports.calcIntelScoreBotnets = calcIntelScoreBotnets;

const calcIntelScoreSSLCerts = (countSSLCerts) => {
    if (countSSLCerts && !countSSLCerts.isEmpty) {
        if (countSSLCerts.hasCriticalAsset) {
            countSSLCerts.score = 100;
        } else {
            const totalDomainsCount = (countSSLCerts.validHttpsDomainsCount + countSSLCerts.notValidHttpsDomainsCount) * 10;
            countSSLCerts.score = (totalDomainsCount) ? Math.round((countSSLCerts.sslCalcPoints / totalDomainsCount) * 100) : 0;
        }

        countSSLCerts = groupScoreDataObject(countSSLCerts, ['validHttpsDomainsCount', 'notValidHttpsDomainsCount', 'hasCriticalAsset']);
    } else {
        countSSLCerts = {
            score: 0,
            scoreData: {
                validHttpsDomainsCount: 0,
                notValidHttpsDomainsCount: 0,
                hasCriticalAsset: false
            }
        };
    }
    return countSSLCerts;
};

module.exports.calcIntelScoreSSLCerts = calcIntelScoreSSLCerts;

const calcSurveyScore = (surveyData) => {
    if (surveyData) {
        if (!surveyData.isEmpty && surveyData.surveyScore != null) {
            surveyData.surveyScore = Math.round(surveyData.surveyScore * 100);
        } else {
            surveyData.surveyScore = 0;
            surveyData.isNA = true;
        }
    } else {
        surveyData = {
            surveyScore: 0,
            isNA: true
        };
    }
    return surveyData;
};

function getTopTwoHighestBlacklistsByType(blacklistsByTypes) {
    if (blacklistsByTypes &&
        (blacklistsByTypes.ip !== 0 || blacklistsByTypes.domain !== 0 || blacklistsByTypes.mx !== 0 || blacklistsByTypes.ns !== 0)) {
        // Types are : ip, domain, mx and ns.
        const arrayToSort = [
            {name: 'ip', finding: blacklistsByTypes.ip},
            {name: 'domain', finding: blacklistsByTypes.domain},
            {name: 'mx', finding: blacklistsByTypes.mx},
            {name: 'ns', finding: blacklistsByTypes.ns}
        ];
        // Sort from higher to lower.
        arrayToSort.sort((a, b) => {
            return b.finding - a.finding;
        });

        // Save only top 2 results.
        blacklistsByTypes = {};
        for (let i = 0; i < 2; i++) {
            blacklistsByTypes[arrayToSort[i].name] = arrayToSort[i].finding;
        }
    } else {
        blacklistsByTypes = {
            ip: 0,
            mx: 0
        };
    }
    return blacklistsByTypes;
}

function combineArraysWithoutDuplicates(mainArray, arrayToInsert) {
    if (arrayToInsert && Array.isArray(arrayToInsert) && arrayToInsert.length) {
        arrayToInsert.map((currItem) => {
            if (currItem && !mainArray.includes(currItem)) {
                mainArray.push(currItem);
            }
        });
    }
    return mainArray;
}

function getMaxValue(mainMax, testNum) {
    if (mainMax != null && testNum != null && testNum > mainMax) {
        mainMax = testNum;
    }
    return mainMax;
}

module.exports.getIsp = (domains, startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetIspData(domains, startDate, endDate, scans, isForReport, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getIsp: ', error);
                    return reject();
                });
        }else{
            console.error("getIsp Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

const getBuckets = (domains, startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetBucketsData(domains, startDate, endDate, scans, isForReport, filteredCompaniesNames);

        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();

                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getBuckets: ', error);
                    return reject();
                });
        }else{
            console.error("getBuckets Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getBuckets = getBuckets;

const countBuckets = (allBuckets) => {
    const uniqueBuckets = [];
    const resultCounts = {
        publicBucketsCount: 0,
        totalBucketsCount: 0,
        publicBucketsCalcPoints: 0,
        hasCriticalAsset: false
    };
    if (allBuckets && allBuckets.length > 0) {
        allBuckets.map((currEntity) => {
            if (currEntity && currEntity.bucket) {
                if (!uniqueBuckets.includes(currEntity.bucket)) {
                    uniqueBuckets.push(currEntity.bucket);
                    resultCounts.totalBucketsCount++;
                    if (!currEntity.bucket_status || currEntity.bucket_status === 'Public') {
                        resultCounts.publicBucketsCount++;

                        if (currEntity.importance) {
                            if (currEntity.importance === 5) { //critical risk asset
                                resultCounts.hasCriticalAsset = true;
                            } else if (currEntity.importance === 4) { //high risk asset
                                resultCounts.publicBucketsCalcPoints += (10 * calcImpactNum);
                            } else if (currEntity.importance === 2) { //low risk asset
                                resultCounts.publicBucketsCalcPoints += (10 / calcImpactNum);
                            } else { // medium risk asset
                                resultCounts.publicBucketsCalcPoints += 10;
                            }
                        } else { // medium risk asset
                            resultCounts.publicBucketsCalcPoints += 10;
                        }
                    }
                }
            }
        });
    }
    return (resultCounts);
};

const countDataleaksData = (allDataleaks) => {
    const uniqueDataleaks = [];
    const resultCounts = {
        totalDataleaksCount: allDataleaks.length,
        dataleaksCalcPoints: 0,
        hasCriticalAsset: false
    };
    if (allDataleaks && allDataleaks.length > 0) {
        allDataleaks.map((currEntity) => {
            if (currEntity) {
                if (!uniqueDataleaks.includes(currEntity)) {
                    uniqueDataleaks.push(currEntity);
                    if (currEntity.importance) {
                        if (currEntity.importance === 5) { //critical risk asset
                            resultCounts.hasCriticalAsset = true;
                        } else if (currEntity.importance === 4) { //high risk asset
                            resultCounts.dataleaksCalcPoints += (10 * calcImpactNum);
                        } else if (currEntity.importance === 2) { //low risk asset
                            resultCounts.dataleaksCalcPoints += (10 / calcImpactNum);
                        } else { // medium risk asset
                            resultCounts.dataleaksCalcPoints += 10;
                        }
                    } else { // medium risk asset
                        resultCounts.dataleaksCalcPoints += 10;
                    }
                }
            }
        });
    }
    return (resultCounts);
};

module.exports.countBuckets = countBuckets;

const countSpfOrDmarc = (allSpfOrDmarc, type) => {
    let notConfiguredSum = 0;
    let notConfiguredCount = 0;
    let hasCriticalAsset = false;
    if (allSpfOrDmarc && allSpfOrDmarc.length > 0 &&
        type && typeof type === 'string' && (type === 'spf' || type === 'dmarc')) {
        allSpfOrDmarc.map((currEntity) => {
            if (currEntity && currEntity[type]) {
                if (currEntity.importance && currEntity.importance === 5) { //critical risk asset
                    hasCriticalAsset = true;
                }
                if (currEntity[type] === 'No Records Found') {
                    if (currEntity.importance) {
                        if (currEntity.importance === 5) { //critical risk asset
                            hasCriticalAsset = true;
                        } else if (currEntity.importance === 4) { //high risk asset
                            notConfiguredCount++;
                            notConfiguredSum += (10 * calcImpactNum);
                        } else if (currEntity.importance === 2) { //low risk asset
                            notConfiguredCount++;
                            notConfiguredSum += (10 / calcImpactNum);
                        } else { // medium risk asset
                            notConfiguredCount++;
                            notConfiguredSum += 10;
                        }
                    } else { // medium risk asset
                        notConfiguredCount++;
                        notConfiguredSum += 10;
                    }
                }
            }
        });
    }
    return ({notConfiguredCount: notConfiguredCount, notConfiguredSum: notConfiguredSum, hasCriticalAsset: hasCriticalAsset});
};

module.exports.countSpfOrDmarc = countSpfOrDmarc;

const countCVEs = (allVulns, maxIntelScore = 0) => {
    const criticalTreshold = 7.5;
    const mediumThreshold = 4;
    const uniqueCriticalCVSS = [];
    const uniqueMediumCVSS = [];
    const uniqueOtherCVSS = [];
    const resultCounts = {
        maxIntelScore: maxIntelScore,
        highestCVSSCount: 0,
        criticalCVSSCount: 0,
        mediumCVSSCount: 0
    };
    if (allVulns && allVulns.length > 0 && maxIntelScore != null) {
        allVulns.map((currEntity) => {
            if (currEntity && currEntity.cve && currEntity.cveScore) {
                let currCveScore = Number(currEntity.cveScore);
                if (currCveScore >= criticalTreshold) {
                    if (!uniqueCriticalCVSS.includes(currEntity.cve)) {
                        uniqueCriticalCVSS.push(currEntity.cve);
                        // This check will happen only at insertion so will be counted as distinct.
                        if (currCveScore === maxIntelScore) {
                            resultCounts.highestCVSSCount++;
                        }
                    }
                } else if (currCveScore >= mediumThreshold) {
                    if (!uniqueMediumCVSS.includes(currEntity.cve)) {
                        uniqueMediumCVSS.push(currEntity.cve);
                        // This check will happen only at insertion so will be counted as distinct.
                        if (currCveScore === maxIntelScore) {
                            resultCounts.highestCVSSCount++;
                        }
                    }
                } else if (!uniqueOtherCVSS.includes(currEntity.cve)) {
                    uniqueOtherCVSS.push(currEntity.cve);
                    // This check will happen only at insertion so will be counted as distinct.
                    if (currCveScore === maxIntelScore) {
                        resultCounts.highestCVSSCount++;
                    }
                }
            }
        });

        //this is for showing in the company dashboard num of cvss count
        resultCounts.criticalCVSSCount = uniqueCriticalCVSS.length;
        resultCounts.mediumCVSSCount = uniqueMediumCVSS.length;
    }
    return (resultCounts);
};

module.exports.countCVEs = countCVEs;

const updateScoreUsingImportanceImpact = (score, importance) => {
    let updatedScore = 0;

    if (score && importance) {

        importance = Number(importance);
        updatedScore = Number(score);

        if (importance === 2) {
            updatedScore *= 0.5;

        } else if (importance === 4) {
            updatedScore *= 1.5;
        }
        updatedScore = updatedScore > 10 || updatedScore > 10.0 ? 10 : updatedScore;

    }
    return updatedScore;
};

const getSpf = (domains, startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetSpf(domains, startDate, endDate, scans, isForReport, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getSpf query: ', error);
                    return reject();
                });
        }else{
            console.error("getSpf Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getSpf = getSpf;

const getDmarc = (domains, startDate, endDate, scans, isForReport, filteredCompaniesNames) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetDmarc(domains, startDate, endDate, scans, isForReport, filteredCompaniesNames);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getDmarc: ', error);
                    return reject();
                });
        }else{
            console.error("getDmarc Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getDmarc = getDmarc;

module.exports.getCVESeverity = (domains, startDate, endDate, scans) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetCVESeverity(domains, startDate, endDate, scans);

        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const CVESeverityData = result.records.map((record) => {
                        return record._fields[1];
                    });

                    return resolve(CVESeverityData);
                })
                .catch((error) => {
                    console.error('getCVESeverity: ', error);
                    return reject();
                });
        }else{
            console.error("getCVESeverity Error for ", domains, " query returns empty.");
            return resolve([]);
        }
    });
};

module.exports.getLocations = (startDate, endDate, scans, companyIDs) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        let query = queries.queryCypherGetLocations(startDate, endDate, scans);
        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getLocations: ', error);
                    return reject();
                });
        }else{
            console.error("getAllIPs Error for ", companyIDs, " query returns empty.");
            return resolve({});
        }
    });
};

module.exports.getCompanyExtraDataFromNeo = (companies) => {
    return new Promise((resolve) => {
        if (companies) {
            let dataToIterate;
            if (!Array.isArray(companies)) {
                dataToIterate = [companies];
            } else {
                dataToIterate = [].concat(companies);
            }
            console.log("In getCompanyExtraDataFromNeo");
            return PromiseB.map(dataToIterate, (company) => {
                if (company && company.id) {
                    return this.GetCompanyExtraDataByID(company.id);
                }
            })
                .then((allCompaniesExtraData) => {
                    if (allCompaniesExtraData && Array.isArray(allCompaniesExtraData)) {
                        allCompaniesExtraData.map((currResult) => {
                            if (currResult && Array.isArray(currResult)) {
                                currResult.map((currEntity) => {
                                    if (currEntity && currEntity.companyID && currEntity.type && currEntity.data && currEntity.nid && currEntity.nid.hasOwnProperty('low')) {
                                        let matchingCompany = companies.find((c) => {
                                            if (c && c.id && typeof c.id === 'object') {
                                                c.id = c.id.valueOf().toString();
                                            }
                                            return (c && c.id === currEntity.companyID);
                                        });
                                        // For type responsible -> responsibles and for type contact ->  contacts
                                        let typeArrayName = currEntity.type + 's';
                                        if (matchingCompany) {
                                            // This row adds a unique id to each entity (which is nodeID).
                                            currEntity.data.nid = currEntity.nid.low;
                                            if (!matchingCompany.hasOwnProperty('extraData')) {
                                                matchingCompany.extraData = {};
                                            }
                                            if (!matchingCompany.extraData.hasOwnProperty(typeArrayName)) {
                                                matchingCompany.extraData[typeArrayName] = [];
                                            }
                                            matchingCompany.extraData[typeArrayName].push(currEntity.data);
                                        }
                                    }
                                });
                            }
                        });
                    }
                    resolve(companies);
                })
                .catch((e) => {
                    console.error('Error in getCompanyExtraDataFromNeo: ', e);
                    resolve(companies);
                });
        } else {
            resolve([]);
        }
    });
};

const convertToObject = (result) => {
    return result.records.map((record) => {
        const dataObj = {};

        for (let i = 0; i < record._fields.length; i++) {
            dataObj[record.keys[i]] = record._fields[i];
        }

        return dataObj;
    });
};

module.exports.convertToObject = convertToObject;

module.exports.getCVECountPerHostname = (domains, startDate, endDate, scans) => {
    return new PromiseB((resolve, reject) => {
        const session = neo4j.session();

        if (!Array.isArray(domains)) {
            domains = [domains];
        }
        let query = queries.queryCypherGetCVECountPerHostname(domains, startDate, endDate, scans);

        if(query !== '') {
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const locationsData = result.records.map((record) => {
                        const locationObj = {};

                        for (let i = 0; i < record._fields.length; i++) {
                            locationObj[record.keys[i]] = record._fields[i];
                        }

                        return locationObj;
                    });

                    return resolve(locationsData);
                })
                .catch((error) => {
                    console.error('getCVECountPerHostname: ', error);
                    return reject();
                });
        }else{
            console.error("getCVECountPerHostname Error for ", domains, " query returns empty.");
            return resolve({});
        }
    });
};

function createStringArr(arr) {
    let stringArr = '';
    if (arr && Array.isArray(arr) && arr.length > 0) {
        for (let i = 0; i < arr.length; i++) {
            if (i === arr.length - 1) {
                stringArr += '"' + arr[i] + '"';
            } else if (i === 0 && arr.length > 1) {
                stringArr += '"' + arr[i] + '",';
            } else {
                stringArr += '"' + arr[i] + '",';
            }
        }
    }

    return stringArr;
}

module.exports.createStringArr = createStringArr;

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

module.exports.saveIPReports = (ipAddressArr, scanId, date, seneca) => {
    seneca.client({
        type: 'gcloud',
        topicPrefix: 'shodan',
        projectId: 'rescana-qa',
        keyFilename: config.paths.gcloudKeyFile,
        timeout: 999999999
    });
    const pubsubAct = PromiseB.promisify(seneca.act, {context: seneca});
    let runIPAddressArr = this.reduceIpArr(ipAddressArr);

    return pubsubAct('role:shodan, cmd:reverseDNS', {ipAddressArr: runIPAddressArr})
        .then((data) => {
            //Save hostnames from shodan to graph.
            for (let key in data) {
                if (data.hasOwnProperty(key)) {
                    let value = data[key];
                    if (value) {
                        PromiseB.each(value, (hostname) => {
                            let data = {
                                resolve_time: date, //Set once and can't change in query
                                scanTime: date, //Set every scan.
                                scanId: scanId
                            };

                            let relData = {
                                fromNodeLabelName: 'IP',
                                relationshipLabel: 'hostname',
                                data: data,
                                toNodeLabel: hostname,
                                toNodeType: 'hostname',
                                fromNodeProps: {address: key}
                            };

                            return neoAndMongoHelpers.saveItemToGraph('hostname', hostname, relData);
                        });
                    } else {
                        return [PromiseB.resolve(), ipAddressArr];
                    }
                } else {
                    return [PromiseB.resolve(), ipAddressArr];
                }
            }
        })
        .catch((e) => {
            console.error('reverseDNS Error: ', e);
        })
        .then(() => {
            console.log('Turning IP to ASN');
            return this.ip2Asn(runIPAddressArr);
        })
        .catch((e) => {
            console.log(e);
        })
        .then((asnObjects) => {
            console.log('Received ASN');
            const pairs = this.convertASNObj(asnObjects);
            return PromiseB.map(pairs, (item) => {
                return neoAndMongoHelpers.setPropsAsNodes(item.asn, item.ip, 'IP', {address: item.ip}, null, scanId, date);
            }).then(() => {
                console.log('Done');
                return PromiseB.resolve();
            });
        });
};

module.exports.reduceIpArr = (ipAddressArr) => {
    // This is a fix to work well with array of arrays.
    let runIPAddressArr = [];
    if (ipAddressArr[0] && Array.isArray(ipAddressArr[0])) {
        ipAddressArr.map((currEntity) => {
            if (currEntity) {
                // Add currEntity without duplicates.
                if (Array.isArray(currEntity)) {
                    runIPAddressArr = [...runIPAddressArr, ...currEntity];
                } else if (!runIPAddressArr.includes(currEntity)) {
                    runIPAddressArr.push(currEntity);
                }
            }
        });
    } else {
        runIPAddressArr = ipAddressArr;
    }

    return runIPAddressArr;
};

module.exports.convertASNObj = (asnObjects) => {
    const pairs = [];
    for (const ip in asnObjects) {
        if (asnObjects.hasOwnProperty(ip)) {
            const asn = asnObjects[ip];
            pairs.push({asn: asn, ip: ip});
        }
    }

    return pairs;
};

let singleReverseDNS = (ip) => {
    return new Promise((resolve, reject) => {
        dns.reverse(ip, (err, hostnames) => {
            if (err) {
                if (err.code === 'ENOTFOUND') {
                    console.log(err);
                    resolve([]);
                } else {
                    reject(err);
                }
            } else {
                resolve(hostnames);
            }
        });
    });
};

module.exports.reverseDNS = (ipArr, scanId, date) => {
    let promArr = [];
    if (!Array.isArray(ipArr)) {
        ipArr = [ipArr];
    }

    for (let i = 0; i < ipArr.length; i++) {
        promArr.push(singleReverseDNS(ipArr[i]));
    }

    Promise.all(promArr).then((data) => {
        //Save hostnames to graph.
        for (let key in data) {
            if (data.hasOwnProperty(key)) {
                let value = data[key];
                if (value) {
                    PromiseB.each(value, (hostname) => {
                        let data = {
                            resolve_time: date, //Set once and can't change in query
                            scanTime: date, //Set every scan.
                            scanId: scanId
                        };

                        let relData = {
                            fromNodeLabelName: 'IP',
                            relationshipLabel: 'hostname',
                            data: data,
                            toNodeLabel: hostname,
                            toNodeType: 'hostname',
                            fromNodeProps: {address: key}
                        };

                        return neoAndMongoHelpers.saveItemToGraph('hostname', hostname, relData);
                    });
                } else {
                    return PromiseB.resolve();
                }
            } else {
                return PromiseB.resolve();
            }
        }
    }).catch((e) => {
        console.error('Error in reverseDNS: ', e);
        return PromiseB.reject();
    });
};

module.exports.ip2Asn = async (ipArr, allDomains, scanId, date) => {
    let asns = await ip2AsnClientP(ipArr);

    let pairs = this.convertASNObj(asns);
    console.log('Saving ASN');
    pairs.forEach(async (item) => {
        try {
            if (item.asn && item.asn.ASN && !item.asn.ASN.startsWith('AS')) {
                item.asn.ASN = 'AS' + item.asn.ASN;
            }
            await neoHelpers.setPropsAsNodes(item.asn, item.ip, 'IP', {address: item.ip}, null, scanId, date);
            await neoHelpers.setPropsAsNodes(item.asn, item.asn.ASN, 'ASN', {
                name: 'ASN',
                finding: item.asn.ASN
            }, ['ASN'], scanId, date);
        } catch (e) {
            console.error('Error with saving ASN: ', e);
        }

        //TODO: Need to save IP array as bulk and not one by one - it causes all kinds of nasty stuff.
        /* try {
             if (item && item.asn && item.asn.range) {
                 let ipArr = cidrRange(item.asn.range);
                 if (this.appearsInDomainName(item.asn.description, allDomains) && !this.appearsInProviderList(item.asn.description) && ipArr.length <= 512) {
                     console.log("Connecting new IPs to ASN");
                     this.connectIPArrToNode(ipArr, "ASN", {
                         finding: item.asn.ASN,
                         name: "ASN"
                     }, data);
                 }
             } else {
                 console.log("Can't save, item is: ", item.asn.range);
             }
         } catch(e){
             console.log("Error with IP from ASN: ", item.asn.range, " Error is: ", e);
         }*/
    });
};

const ip2AsnClientP = (ipArr) => {
    return new PromiseB((resolve) => {
        if (Array.isArray(ipArr)) {
            ip2AsnClient.query(ipArr, (err, results) => {
                if (err) {
                    console.error('ASN ERROR:', err);
                    return resolve([]);
                } else {
                    if (results) {
                        for (const item in results) {
                            if (results.hasOwnProperty(item)) {
                                if (results[item].ASN && !results[item].ASN.startsWith('AS')) {
                                    results[item].ASN = 'AS' + results[item].ASN;
                                }
                            }
                        }
                    }
                    return resolve(results);
                }
            });
        } else {
            return resolve([]);
        }
    });
};

module.exports.splitAsnName = (asnName) => {
    let splitASName = [];
    if (asnName) {
        asnName = asnName.toLowerCase();
        splitASName = asnName.split(' ');
        let splitASNNameByDot = [];
        let combinedSplitASName = [];

        for (let i = 0; i < splitASName.length; i++) {
            const doubleAsnSplitBydot = splitASName[i].replace(',', '').split('.');
            const doubleAsnSplitByDash = splitASName[i].replace(',', '').split('-');
            splitASNNameByDot = doubleAsnSplitBydot.concat(doubleAsnSplitByDash);
            splitASNNameByDot = splitASNNameByDot.concat(doubleAsnSplitBydot);
            combinedSplitASName = combinedSplitASName.concat(splitASNNameByDot);
        }

        // Remove duplicates.
        splitASName = [...new Set(combinedSplitASName)];
    }
    return splitASName;
};

module.exports.appearsInDomainName = (asnName, domains, checkBlacklists) => {
    let appearsInBlackList = false;
    let appearsInDomainName = false;
    if (!Array.isArray(domains)) {
        domains = [domains];
    }

    if (asnName && Array.isArray(domains)) {
        const unFilteredSplitAS = this.splitAsnName(asnName);
        const splitASName = [];
        //TODO: add permutation check

        for (let j = 0; j < whiteList.length; j++) {
            if (asnName.toLowerCase().indexOf(whiteList[j].toLowerCase()) !== -1) {
                console.log(asnName, ' is a whitelisted keyword: ', whiteList[j]);
                return true;
            }
        }

        for (let k = 0; k < unFilteredSplitAS.length; k++) {
            if (unFilteredSplitAS[k] !== '' && unFilteredSplitAS[k].length > 2 && unFilteredSplitAS[k] !== 'com' && unFilteredSplitAS[k] !== 'llc' && unFilteredSplitAS[k] !== 'ltd.' && unFilteredSplitAS[k] !== 'LTD.' && unFilteredSplitAS[k] !== 'ltd' && unFilteredSplitAS[k] !== 'LTD' && unFilteredSplitAS[k] !== 'INC.' && unFilteredSplitAS[k] !== 'inc.' && unFilteredSplitAS[k] !== 'INC' && unFilteredSplitAS[k] !== 'inc') {
                splitASName.push(unFilteredSplitAS[k]);
            }
        }

        if (Array.isArray(domains)) {
            for (let i = 0; i < domains.length; i++) {
                for (let j = 0; j < splitASName.length; j++) {
                    if (!checkBlacklists || (checkBlacklists && !(domainFinderHelpers.inBlackListedKeywords(splitASName[j]) || domainFinderHelpers.inBlackListedKeywords(domains[i])))) {
                        if (domains[i] && domains[i].toLowerCase().includes(splitASName[j])) {
                            appearsInDomainName = true;
                        }
                    } else {
                        console.log('Found ', asnName, ' in keyword blacklist (', splitASName[j], ')');
                        appearsInBlackList = true;
                        appearsInDomainName = false;
                    }
                }
            }
        }
        return appearsInDomainName;
    } else {
        return appearsInDomainName;
    }
};

module.exports.appearsInProviderList = (asnName) => {
    for (let j = 0; j < whiteList.length; j++) {
        if (asnName.indexOf(whiteList[j].toLowerCase()) !== -1) {
            console.log(asnName, ' is a whitelisted keyword: ', whiteList[j]);
            return false;
        }
    }

    for (let i = 0; i < providers.length; i++) {
        providers[i] = providers[i].toLowerCase();

        const splitProvName = providers[i].split(' ');
        const splitASName = this.splitAsnName(asnName);

        if (providers[i].includes(asnName)) {
            console.log(asnName, ' was found in provider list');
            return true;
        }

        for (let k = 0; k < splitProvName.length; k++) {
            if (splitProvName[k].includes(asnName)) {
                console.log(asnName, ' was found in provider list');
                return true;
            }
        }

        for (let k = 0; k < splitASName.length; k++) {
            if (splitASName[k].length > 2 && (splitASName[k] !== 'llc' || splitASName[k] !== 'ltd.' || splitASName[k] !== 'LTD.' || splitASName[k] !== 'ltd' || splitASName[k] !== 'LTD' || splitASName[k] !== 'INC.' || splitASName[k] !== 'inc.' || plitASName[k] !== 'INC' || splitASName[k] !== 'inc')) {
                if (providers[i].includes(splitASName[k])) {
                    console.log(splitASName[k], ' was found in provider list');
                    return true;
                }
            }
        }

        if (splitASName[0] > 2 && providers[i].includes(splitASName[0])) {
            console.log(splitASName[0], ' was found in provider list');
            return true;
        }

        if (splitASName[0] > 2 && providers[i].includes(splitASNNameByDot[0])) {
            console.log(splitASName[0], ' was found in provider list');
            return true;
        }
    }

    console.log(asnName, 'ASN was not found in list');
    return false;
};

module.exports.connectIPArrToNode = (ipAddressArr, fromNodeLabelName, fromNodeProps, data) => {
    const relArr = [];

    if (ipAddressArr && ipAddressArr.length !== 0) {
        console.log('Go over all IPs');
        return PromiseB.map(ipAddressArr, (ipAddress) => {
            const relData = {
                fromNodeLabelName: fromNodeLabelName,
                fromNodeProps: fromNodeProps,
                newNodeLabelName: 'IP',
                relationshipLabel: 'IP',
                data: data,
                toNodeLabel: ipAddress,
                toNodeType: 'IP',
                toNodeProps: {address: ipAddress}
            };
            relArr.push(relData);
            return ipAddress;
        }).then((ipAddressArr) => {
            console.log('Saving relArr');
            return [neoAndMongoHelpers.createNodeAndRelationship(relArr), ipAddressArr];
        }).spread((ignore, ipAddressArr) => {
            return PromiseB.resolve(ipAddressArr);
        });
    } else {
        console.log('No IPs found.');
        return PromiseB.resolve([]);
    }
};

module.exports.getHostData = (ipAddressArr, seneca) => {
    seneca.client({
        type: 'gcloud',
        topicPrefix: 'shodan',
        projectId: 'rescana-qa',
        keyFilename: config.paths.gcloudKeyFile,
        timeout: 999999999
    });
    const pubsubAct = PromiseB.promisify(seneca.act, {context: seneca});
    let promArr = [];

    if (ipAddressArr && ipAddressArr[0] && ipAddressArr[0].length !== 0) {
        // This is a fix to work well with array of arrays.
        let runIPAddressArr = this.reduceIpArr(ipAddressArr);
        for (let i = 0; i < runIPAddressArr.length; i++) {
            promArr.push(pubsubAct('role:shodan, cmd:getHost', {ip: runIPAddressArr[i]}));
        }
    } else {
        return PromiseB.resolve();
    }
    return PromiseB.all(promArr);
};

module.exports.filterCompanies = (filters, companies) => {
    let filteredCompanies = [];
    // If a filter exists, use it
    if (filters && filters.length > 0) {
        for (let i = 0; i < filters.length; i++) {
            filteredCompanies.push(filters[i].id);
        }
    } else {
        filteredCompanies = companies;
    }
    return filteredCompanies;
};

module.exports.filterCompaniesNames = (filters, companies) => {
    let filteredCompanies = [];
    // If a filter exists, use it
    if (filters && filters.length > 0) {
        for (let i = 0; i < filters.length; i++) {
            filteredCompanies.push(filters[i].companyName);
        }
    } else {
        filteredCompanies = companies;
    }
    return filteredCompanies;
};

module.exports.saveSubdomain = (brandDomain, scanId, date, mainDomain) => {
    console.log('Working on ' + brandDomain);
    let data = {
        resolve_time: date, //Set once and can't change in query
        scanTime: date, //Set every scan.
        scanId: scanId
    };
    let relData = {
        fromNodeLabelName: 'domain',
        fromNodeProps: {hostname: mainDomain},
        relationshipLabel: 'subdomain',
        data: data,
        toNodeLabel: brandDomain,
        toNodeType: 'subdomain',
        toNodeProps: {hostname: brandDomain}
    };
    return neoAndMongoHelpers.createNodeAndRelationship(relData);
};

const getNoDomainIps = (companyId) => {
    console.log(' In getNoDomainIps() - start getting ips for no_domain');

    return new PromiseB((resolve, reject) => {
        if (companyId) {
            const session = neo4j.session();
            const query = 'MATCH (c:COMPANY)-[R:owner]->(d:domain)-[i:IP]->(IP:IP) WHERE c.cid="' + companyId.toString() + '" AND d.hostname="no_domain" RETURN IP.address as address';
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();
                    const data = convertToObject(result);
                    return resolve(data);
                })
                .catch((error) => {
                    console.error('getNoDomainIps: ', error);
                    return reject();
                });
        } else {
            console.error('getNoDomainIps Error - companyId is missing');
            return reject();
        }
    });

};

const updateNoDomainAndIpNode = (noDomainIpsArr, scanId, date, companyName) => {
    console.log(' In updateNoDomainAndIpNode() - starting');
    return new PromiseB((resolve, reject) => {
        let relArr = [];
        if (noDomainIpsArr && noDomainIpsArr.length > 0) {
            let fromNodeLabelName = 'domain';
            for (let i = 0; i < noDomainIpsArr.length; i++) {
                let data = {
                    resolve_time: date, //Set once and can't change in query
                    scanTime: date, //Set every scan.
                    scanId: scanId
                };

                let relData = {
                    fromNodeLabelName: fromNodeLabelName,
                    fromNodeProps: {hostname: 'no_domain', companyName: companyName},
                    newNodeLabelName: 'IP',
                    relationshipLabel: 'IP',
                    data: data,
                    toNodeLabel: 'IP',
                    toNodeType: 'IP',
                    toNodeProps: {address: noDomainIpsArr[i].address}
                };

                relArr.push(relData);
            }
            PromiseB.resolve(neoAndMongoHelpers.createNodeAndRelationship(relArr))
                .then(() => {
                    resolve(relArr);
                });

        } else {
            reject(relArr);
        }
    });
};

module.exports.incItemStatus = (data) => {
    let {scanId, scanDate, topic, increment, count, companyName, companyId} = data;
    return new Promise((resolve) => {
        MongoClient.connect(mongoUrl, (err, client) => {
            if (client) {
                const db = client.db('darkvision');
                let Qcollection = db.collection(collectionName);
                let incItem = {};
                let metaData = {date: scanDate, percentageDone: 0};

                if (companyName && companyId) {
                    metaData = {date: scanDate, companyName: companyName, percentageDone: 0, companyId: companyId};
                }

                if (increment) {
                    incItem[topic + 'StartCounter'] = count;
                    Qcollection.updateOne({scanId: scanId}, {$inc: incItem, $set: metaData}, {upsert: true}, () => {
                        client.close();
                        resolve();
                    });
                } else {
                    incItem[topic + 'EndCounter'] = count;
                    Qcollection.updateOne({scanId: scanId}, {$inc: incItem, $set: metaData}, {upsert: true}, () => {
                        client.close();
                        resolve();
                    });
                }
            } else {
                if (err) {
                    console.error('Error in incItemStatus: ', err);
                }
                resolve();
            }
        });
    });
};

const listToArray = (list) => {
    return new Promise((resolve) => {
        list.toArray((err, docs) => {
            resolve(docs);
        });
    });
};

module.exports.getAllTokens = () => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(mongoUrl, (err, client) => {
            const db = client.db('darkvision');
            const collection = db.collection('tokens_map');
            const find = PromiseB.promisify(collection.find, {context: collection});

            find({})
                .then((list) => {
                    return listToArray(list);
                })
                .then((results) => {
                    client.close();
                    resolve(results);
                }).catch((e) => {
                client.close();
                reject(e);
            });
        });
    });
};

module.exports.getAllRevokedTokens = () => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(mongoUrl, async (err, client) => {
            const db = client.db('darkvision');
            const collection = db.collection('revoked_tokens_map');
            const find = PromiseB.promisify(collection.find, {context: collection});
            try {
                let list = await find({});
                if (list) {
                    let results = await listToArray(list);
                    if (results) {
                        client.close();
                        resolve(results);
                    }
                }
            } catch (e) {
                client.close();
                console.error('Error in getAllRevokedTokens: ' + e);
                reject(e);
            }
        });
    });
};

module.exports.checkFindingStatusObj = (findingStatusData) => {
    return !!(findingStatusData &&
        findingStatusData.uid && typeof findingStatusData.uid === 'string' &&
        findingStatusData.userEmail && typeof findingStatusData.userEmail === 'string' &&
        findingStatusData.curOrgObj && typeof findingStatusData.curOrgObj === 'object' &&
        findingStatusData.curOrgObj.orgId && typeof findingStatusData.curOrgObj.orgId === 'string' && findingStatusData.curOrgObj.orgId !== 'No Id' &&
        findingStatusData.curOrgObj.orgId !== '' && findingStatusData.curOrgObj.orgId !== ' ' &&
        findingStatusData.curOrgObj.orgName && typeof findingStatusData.curOrgObj.orgName === 'string' &&
        findingStatusData.statusType && typeof findingStatusData.statusType === 'string' &&
        (findingStatusData.statusType === 'assetUserInput' || findingStatusData.statusType === 'mitigated') &&
        findingStatusData.objToUpdate && typeof findingStatusData.objToUpdate === 'object' &&
        findingStatusData.objToUpdate.type && typeof findingStatusData.objToUpdate.type === 'string' &&
        findingStatusData.objToUpdate.val && typeof findingStatusData.objToUpdate.val === 'string' &&
        findingStatusData.hasOwnProperty('shouldDetachMitigatedConnection') && typeof findingStatusData.shouldDetachMitigatedConnection === 'boolean' &&
        findingStatusData.hasOwnProperty('isAssetUserInputUpdate') && typeof findingStatusData.isAssetUserInputUpdate === 'boolean');
};

module.exports.createAssetUserInputAndMitigatedDefaultNode = async (orgId, orgName) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (orgId && typeof orgId === 'string' && orgId !== 'No Id' && orgId !== '' && orgId !== ' ' &&
                orgName && typeof orgName === 'string' && orgName !== '' && orgName !== ' ') {

                const relArr = [];
                relArr.push({
                    fromNodeLabelName: 'organization',
                    fromNodeProps: {oid: orgId},
                    newNodeLabelName: 'assetUserInput',
                    relationshipLabel: 'assetUserInput',
                    toNodeLabel: 'assetUserInput',
                    toNodeType: 'assetUserInput',
                    toNodeProps: {type: 'assetUserInput', oid: orgId, oname: orgName}
                });

                relArr.push({
                    fromNodeLabelName: 'organization',
                    fromNodeProps: {oid: orgId},
                    newNodeLabelName: 'mitigated',
                    relationshipLabel: 'mitigated',
                    toNodeLabel: 'mitigated',
                    toNodeType: 'mitigated',
                    toNodeProps: {type: 'mitigated', oid: orgId, oname: orgName}
                });


                await neoAndMongoHelpers.createNodeAndRelationship(relArr);
                console.log('In createAssetUserInputAndMitigatedDefaultNode() - created assetUserInput and mitigate Nodes successfully!');
                resolve({ok: true});
            } else {
                console.error('In createAssetUserInputAndMitigatedDefaultNode() - failed to create assetUserInput and mitigate Nodes - orgId or orgName is missing.');
                reject({ok: false});
            }
        } catch (e) {
            console.error('In createAssetUserInputAndMitigatedDefaultNode() - failed to create assetUserInput and mitigate Nodes: ' + e);
            reject({ok: false});
        }
    });
};

const updateObjArrOnNeo = async (shouldDetach, arrOfObjToUpdate, statusType) => {
    return new PromiseB(async (resolve, reject) => {
        const session = neo4j.session();
        try {
            await extraHelpers.asyncForEach(arrOfObjToUpdate, async (curObjData) => {
                await session.writeTransaction((transaction) => {
                    return transaction.run(shouldDetach ? queries.detachFpAndMitigatedNodesCommand(curObjData) : queries.addAMAndMitigatedNodesCommand(curObjData, statusType));
                });
            });
            let text = shouldDetach ? 'Detach' : 'Update';
            console.log('updateObjArrOnNeo: ' + text + ' Nodes Success');
            session.close();
            resolve();
        } catch (error) {
            console.error('updateObjArrOnNeo: ', error);
            session.close();
            reject();
        }
    });
};

//statusType is 'fp' or 'mitigate'
//type is 'IP', 'cve', buckets' ...
module.exports.updateAssetUserInputAndMitigatedNodesOnNeo = async (shouldDetachMitigated, uid, userEmail, statusType, orgId, objToUpdate, isAssetUserInputUpdate) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (uid && typeof uid === 'string' &&
                userEmail && typeof userEmail === 'string' &&
                statusType && typeof statusType === 'string' &&
                orgId &&  typeof orgId === 'string' &&
                objToUpdate && typeof objToUpdate === 'object' &&
                objToUpdate.type && typeof objToUpdate.type === 'string' &&
                objToUpdate.val &&
                objToUpdate.AMNodePropsObject && typeof objToUpdate.AMNodePropsObject === 'object' ) {
                if(objToUpdate.type === 'IP'){
                    objToUpdate.type = 'ip';
                } else {
                    if (objToUpdate.type === 'ports') {
                        objToUpdate.type = 'port';
                    }
                }
                let relatedData = [];
                let AMProps = objToUpdate.AMNodePropsObject;
                //only in cases of IP/ports/cpe we use the query to get all the entities below them to be included in the process
                //else we use only the object we got from the ui
                if (objToUpdate.type === 'ip' || objToUpdate.type === 'port' || objToUpdate.type === 'cpe') {
                    let query = queries.getQuery(objToUpdate);

                    let returnedData = await extraHelpers.getFromNeoByQuery(query);
                    if (objToUpdate.type === 'ip') {
                        if (AMProps.ip) {
                            relatedData.push({ip: AMProps.ip, type: objToUpdate.type});
                        }
                        relatedData = relatedData.concat(buildObjArrFromSpecificProps(['ip', 'isp'], returnedData, 'isp'));
                        relatedData = relatedData.concat(buildObjArrFromSpecificProps(['ip', 'port'], returnedData, 'port'));
                        relatedData = relatedData.concat(buildObjArrFromSpecificProps(['ip', 'cpe'], returnedData, 'cpe'));
                        relatedData = relatedData.concat(buildObjArrFromSpecificProps(['ip', 'cpe', 'cve'], returnedData, 'cve'));
                    } else if (objToUpdate.type === 'port') {
                        if (AMProps.ip && AMProps.port) {
                            relatedData.push({ip: AMProps.ip, port: AMProps.port, type: objToUpdate.type});
                        }
                        relatedData = relatedData.concat(buildObjArrFromSpecificProps(['ip', 'port', 'cpe'], returnedData, 'cpe'));
                        relatedData = relatedData.concat(buildObjArrFromSpecificProps(['ip', 'port', 'cpe', 'cve'], returnedData, 'cve'));
                    } else if (objToUpdate.type === 'cpe') {
                        if (AMProps.ip && AMProps.cpe) {
                            relatedData.push({ip: AMProps.ip, cpe: AMProps.cpe, type: 'cpe'});
                        }
                        relatedData = relatedData.concat(buildObjArrFromSpecificProps(['ip', 'cpe', 'cve'], returnedData, 'cve'));
                        relatedData.map((curobj) => {
                            curobj.ip = AMProps.ip;
                        });
                    }
                }

                //In case relateData is empty or didnt get any results from query we use only the object we got from the ui.
                if (relatedData && relatedData.length === 0) {
                    if (objToUpdate.type === 'cve') {
                        relatedData.push({ip: AMProps.ip, cve: AMProps.cve, cpe: AMProps.cpe, type: objToUpdate.type});
                    } else if (objToUpdate.type === 'isp') {
                        relatedData.push({isp: AMProps.isp, ip: AMProps.ip, type: objToUpdate.type});
                    } else if (objToUpdate.type === 'bucket') {
                        relatedData.push({bucket: AMProps.bucket, type: objToUpdate.type});
                    } else if (objToUpdate.type === 'product') {
                        relatedData.push({product: AMProps.product, ip: AMProps.ip, type: objToUpdate.type});
                    } else if (objToUpdate.type === 'spf') {
                        relatedData.push({spf: AMProps.spf, hostname: AMProps.hostname, type: objToUpdate.type});
                    } else if (objToUpdate.type === 'dmarc') {
                        relatedData.push({dmarc: AMProps.dmarc, hostname: AMProps.hostname, type: objToUpdate.type});
                    }
                }

                //creating all the objects - AM = assetUserInput and Mitigated
                let arrOfObjToUpdate = await getArrOfObjToUpdateAMNodes(statusType, orgId, objToUpdate, relatedData, shouldDetachMitigated, uid, userEmail, isAssetUserInputUpdate, objToUpdate.type);

                //creating and updating the objects on neo4j database
                if (arrOfObjToUpdate && arrOfObjToUpdate.length > 0) {
                    await updateObjArrOnNeo(shouldDetachMitigated, arrOfObjToUpdate, statusType);
                    console.log('In updateAssetUserInputAndMitigatedNodesOnNeo() - updated ' + statusType + ' Nodes successfully!');
                    resolve({ok: true});
                } else {
                    console.error('In updateAssetUserInputAndMitigatedNodesOnNeo() - failed to update ' + statusType + ' Nodes.');
                    reject({ok: false});
                }
            } else {
                console.error('In updateAssetUserInputAndMitigatedNodesOnNeo() - failed to update ' + statusType + ' Nodes.');
                reject({ok: false});
            }
        } catch (e) {
            console.error('In updateAssetUserInputAndMitigatedNodesOnNeo() - failed to update ' + statusType + ' Nodes: ' + e);
            reject({ok: false});
        }
    });
};

const buildObjArrFromSpecificProps = (propsArr, data, type) => {
    let newArr = [];
    if (propsArr && Array.isArray(propsArr) && propsArr.length > 0 &&
        data && Array.isArray(data) && data.length > 0 &&
        type && typeof type === 'string') {
        (data).map((curData)=>{
            let newObj = {};
            Object.keys(curData).map((curKey) => {
                if (propsArr.includes(curKey)) {
                    newObj[curKey] = curData[curKey];
                }
            });
            if (Object.getOwnPropertyNames(newObj).length > 0 && !newArr.includes(newObj)) {
                newObj.type = type;
                newArr.push(newObj);
            }
        });
    }
    if (newArr.length > 0) {
        newArr = extraHelpers.removeDuplicates(newArr, type);
    }

    return newArr;
};

module.exports.buildObjArrFromSpecificProps = buildObjArrFromSpecificProps;

const createRelDataObj = (allDataForObj, markedAssetType) => {
    let relData = {};
    if (allDataForObj && typeof allDataForObj === 'object' && markedAssetType && typeof markedAssetType === 'string') {
        let shouldDetachMitigated = allDataForObj.shouldDetachMitigated;
        let curData = allDataForObj.curData;
        let nodeType = allDataForObj.nodeType;
        let statusType = allDataForObj.statusType;
        let objToUpdate = allDataForObj.objToUpdate;
        let orgId = allDataForObj.orgId;
        let updateTime = allDataForObj.updateTime;
        let uid = allDataForObj.uid;
        let userEmail = allDataForObj.userEmail;
        let isAssetUserInputUpdate = allDataForObj.isAssetUserInputUpdate;

        if (curData && typeof curData === 'object' &&
            nodeType && typeof nodeType === 'string' &&
            statusType && typeof statusType === 'string' &&
            orgId && typeof orgId === 'string' &&
            updateTime && typeof updateTime === 'string' &&
            uid && typeof uid === 'string' &&
            userEmail && typeof userEmail === 'string') {
            //On detach mitigated
            if (shouldDetachMitigated && statusType === 'mitigated') {
                relData = {
                    fromNodeLabelName: statusType,
                    relationshipLabel: statusType,
                    fromProp: 'oid',
                    fromPropVal: orgId,
                    targetNode: 'AM' + nodeType,
                    whereStatement: ''
                };

                let strForNodeProps = '';
                Object.keys(curData).map((curPropKey) => {
                    if (curPropKey !== 'type') {
                        strForNodeProps += 'AND t.' + curPropKey + '="' + curData[curPropKey] + '" '
                    }
                });
                strForNodeProps += 'AND t.nodeType="' + relData.targetNode + '" ';
                relData.whereStatement = strForNodeProps;


                if (relData.whereStatement.length === 0) {
                    relData = '';
                }
                //On add relationship
            } else {
                relData = {
                    fromNodeLabelName: statusType,
                    fromProp: 'oid',
                    fromPropVal: orgId,
                    targetNode: 'AM' + nodeType,
                    relationshipLabel: statusType,
                    relationshipData: 'updateTime:"' + updateTime + '", uid:"' + uid + '", userEmail:"' + userEmail + '" '
                };

                let strForNodeProps = '';
                Object.keys(curData).map((curPropKey) => {
                    if (curPropKey !== 'type') {
                        strForNodeProps += curPropKey + ': "' + curData[curPropKey] + '", '
                    }
                });
                strForNodeProps += 'nodeType: "' + relData.targetNode + '" ';
                relData.targetMatchProps = strForNodeProps;


                if (isAssetUserInputUpdate && objToUpdate && objToUpdate.importance && typeof objToUpdate.importance === 'number') {
                    relData.relationshipData += ', importance:' + objToUpdate.importance + ' ';
                    if (markedAssetType === nodeType && objToUpdate.comments && Array.isArray(objToUpdate.comments)) {
                        let commentsArr = createStringArr(objToUpdate.comments);
                        relData.relationshipData += ', comments: [' + commentsArr + '] ';
                    }
                }

                if (Object.getOwnPropertyNames(relData.targetMatchProps).length === 0) {
                    relData = '';
                }
            }
        }
    }
    return relData;
};

module.exports.createRelDataObj = createRelDataObj;

/**
 * This function inserts relationship objects (all the data relevant) for each asset.
 *
 * @param statusType
 * @param orgId
 * @param objToUpdate
 * @param relatedData
 * @param shouldDetachMitigated
 * @param uid
 * @param userEmail
 * @param isAssetUserInputUpdate
 * @param markedAssetType
 */
const getArrOfObjToUpdateAMNodes = (statusType, orgId, objToUpdate, relatedData, shouldDetachMitigated,
                                    uid, userEmail, isAssetUserInputUpdate, markedAssetType) => {
    return new PromiseB(async (resolve, reject) => {
        const updateTime = moment().format();
        try {
            let finalObjDataArr = [];
            if (relatedData && Array.isArray(relatedData) && relatedData.length > 0) {
                relatedData.map((curData) => {
                    let allDataForObj = {
                        shouldDetachMitigated: !!shouldDetachMitigated,
                        curData: curData,
                        nodeType: curData.type,
                        statusType: statusType,
                        orgId: orgId,
                        objToUpdate: objToUpdate,
                        updateTime: updateTime,
                        uid: uid,
                        userEmail: userEmail,
                        isAssetUserInputUpdate: !!isAssetUserInputUpdate
                    };

                    let relDataObj = createRelDataObj(allDataForObj, markedAssetType);
                    //check if relDataObj is not empty obj
                    if (Object.getOwnPropertyNames(relDataObj).length > 0) {
                        finalObjDataArr.push(relDataObj);
                    }
                });

                console.log('In getArrOfObjToUpdateAMNodes() - ' + statusType + ' array of objects successfully returned!');
                resolve(finalObjDataArr);
            } else {
                console.error('In getArrOfObjToUpdateAMNodes() - failed to get ' + statusType + ' array of objects.');
                reject();
            }
        } catch (e) {
            console.error('In getArrOfObjToUpdateAMNodes() - failed to get ' + statusType + ' array of objects: ' + e);
            reject();
        }
    });
};

/**
 *  Pulls data from NEO4J or Mongo by the current table type.
 * @param type
 * @param orgId
 * @param isFromMongo
 * @returns {Promise}
 */
const getAssetUserInputAndMitigatedNodesByType = async (type, orgId, isFromMongo = false) => {
    return new Promise(async (resolve) => {
        try {
            console.log("In getAssetUserInputAndMitigatedNodesByType looking for : ", type);
            //This is to formalize the name for bucket/buckets - it's different from the UI and in the DB.
            let datatype = type && type === 'buckets' ? 'bucket' : type;

            if (orgId && orgId !== 'No Id' && type && typeof type === 'string') {
                if (isFromMongo) {
                    console.log("Getting from  Mongo");
                    if (datatype === SSL_CERTS_TYPE || datatype === BOTNETS_TYPE || datatype === BLACKLISTS_TYPE ||
                        datatype === DATALEAKS_TYPE || datatype === GDPR_NOT_COMPLY_TYPE || datatype === EMAIL_BREACHES_TYPE) {
                        let returnedData = await getAssetUserInputAndMitigatedByDataTypeFromMongo(datatype, orgId);
                        if (returnedData && returnedData.length > 0) {
                            console.log('In getAssetUserInputAndMitigatedNodesByType() - data successfully returned!');
                            resolve(returnedData);
                        } else {
                            console.log('In getAssetUserInputAndMitigatedNodesByType() - no data was returned');
                            resolve([]);
                        }
                    } else {
                        console.error('In getAssetUserInputAndMitigatedNodesByType() - Error: datatype do not exist or is undefined');
                        resolve([]);
                    }
                } else if (datatype === 'IP' || datatype === 'ports'
                    || datatype === 'cpe' || datatype === 'cve'
                    || datatype === 'bucket' || datatype === 'product'
                    || datatype === 'dmarc' || datatype === 'spf' || datatype === 'isp') {

                    console.log("Getting from Neo");

                    if (datatype === 'IP') {
                        datatype = 'ip';
                    } else if (datatype === 'ports') {
                        datatype = 'port';
                    }

                    let query = getUserInputAndMitigatedQueryByType(datatype, orgId);
                    if (query.length > 0) {
                        let assetUserInputAndMitigatedNodesData = await extraHelpers.getFromNeoByQuery(query);
                        //If it's a cpe, we need to get the CVE's as well.
                        if (datatype === 'cpe') {
                            query = getUserInputAndMitigatedQueryByType('cve', orgId);
                            let cveAssetUserInputAndMitigatedNodesData = await extraHelpers.getFromNeoByQuery(query);
                            if (cveAssetUserInputAndMitigatedNodesData && cveAssetUserInputAndMitigatedNodesData.length) {
                                //concat cpe and CVE data.
                                assetUserInputAndMitigatedNodesData = assetUserInputAndMitigatedNodesData.concat(cveAssetUserInputAndMitigatedNodesData);
                            }
                        }
                        //DB sometimes returns low and high properties with the data - this is to handle such cases.
                        if (assetUserInputAndMitigatedNodesData && assetUserInputAndMitigatedNodesData.length > 0) {
                            assetUserInputAndMitigatedNodesData.map((curData) => {
                                if (curData && curData.importance && typeof curData.importance === 'object' && curData.importance.low) {
                                    curData.importance = Number(curData.importance.low);
                                }
                            });
                            console.log('In getAssetUserInputAndMitigatedNodesByType() - data successfully returned!');
                            resolve(assetUserInputAndMitigatedNodesData);
                        } else {
                            console.log('In getAssetUserInputAndMitigatedNodesByType() - no data was returned');
                            resolve([]);
                        }
                    } else {
                        console.error('In getAssetUserInputAndMitigatedNodesByType() - Error: no query returned');
                        resolve([]);
                    }
                } else {
                    console.error('In getAssetUserInputAndMitigatedNodesByType() - Error: datatype do not exist or is undefined');
                    resolve([]);
                }
            } else {
                console.error('In getAssetUserInputAndMitigatedNodesByType() - Error: datatype or orgId do not exist or is undefined');
                resolve([]);
            }
        } catch (e) {
            console.error('In getAssetUserInputAndMitigatedNodesByType() - Error: ' + e);
            resolve([]);
        }
    });
};

module.exports.getAssetUserInputAndMitigatedNodesByType = getAssetUserInputAndMitigatedNodesByType;

const getUserInputAndMitigatedQueryByType = (type, orgId) => {
    let returnStatement = '';
    let query = '';
    if (type && orgId && type !== '' && orgId !== '' && typeof orgId === 'string' && typeof type === 'string') {
        if (type === 'ip') {
            returnStatement = 't.ip as ip';
        } else if (type === 'port') {
            returnStatement = 't.port as port, t.ip as ip';
        } else if (type === 'cpe') {
            returnStatement = 't.cpe as cpe, t.ip as ip';
        } else if (type === 'cve') {
            returnStatement = 't.cve as cve, t.cpe as cpe, t.ip as ip';
        } else if (type === 'bucket') {
            returnStatement = 't.bucket as bucket';
        } else if (type === 'isp') {
            returnStatement = 't.isp as isp, t.ip as ip';
        } else if (type === 'product') {
            returnStatement = 't.product as product, t.ip as ip';
        } else if (type === 'spf') {
            returnStatement = 't.' + type + ' as spf, t.hostname as hostname';
        } else if (type === 'dmarc') {
            returnStatement = 't.' + type + ' as dmarc, t.hostname as hostname';
        }
        if (returnStatement.length > 0) {
            query = 'MATCH (u:assetUserInput)-[r:assetUserInput]->(t:AM' + type + ') ' +
                'WHERE u.oid="' + orgId + '" AND t.nodeType="AM' + type + '" ' +
                'return ' + returnStatement + ', u.type as type, r.userEmail as userEmail, r.importance as importance, r.comments as comments ' +
                'UNION ' +
                'MATCH (m:mitigated)-[r:mitigated]->(t:AM' + type + ') ' +
                'WHERE m.oid="' + orgId + '" AND t.nodeType="AM' + type + '" ' +
                'return ' + returnStatement + ', m.type as type, r.userEmail as userEmail, null as importance, null as comments ';
        }
    }
    console.log(query);

    return query;
};

module.exports.getUserInputAndMitigatedQueryByType = getUserInputAndMitigatedQueryByType;

module.exports.getOrg = async (orgId) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                if (orgId && orgId !== '' && orgId !== 'No Id') {
                    const db = client.db('darkvision');
                    let collection = db.collection('organizations_map');
                    let orgValidObjId = ObjectId(orgId.toString());

                    const find = PromiseB.promisify(collection.find, {context: collection});
                    const list = await find({_id: orgValidObjId});

                    if (list) {
                        const result = await listToArray(list);
                        if (result) {
                            client.close();
                            resolve(result);
                        }
                    }
                }else{
                    reject("getShowScanDateBool: No orgId");
                }
            } catch (e) {
                client.close();
                reject("Error in getShowScanDateBool: ", e);
            }
        });
    });
};

module.exports.updateAssetUserInputAndMitigatedNodesOnMongo = async (shouldDeleteMitigated, uid, userEmail, statusType, orgId, objToUpdate, isAssetUserInputUpdate) => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(mongoUrl, (err, client) => {

            if (uid && userEmail && statusType && orgId && orgId !== 'No Id' &&
                objToUpdate && objToUpdate.val) {

                const db = client.db('darkvision');
                let collection = db.collection('organizations_map');

                let orgValidObjId = '';

                if (orgId && orgId !== '') {
                    orgValidObjId = ObjectId(orgId.toString());
                }

                let destinationArray = 'assetConfigurationByUser.' + objToUpdate.type + '.' + statusType;

                let dataToUpdate = {
                    identifier: objToUpdate.val,
                    uid: uid,
                    userEmail: userEmail,
                    type: statusType
                };

                let find = {_id: orgValidObjId};
                let action = '';

                if (isAssetUserInputUpdate && objToUpdate.importance && objToUpdate.comments) {
                    dataToUpdate.importance = objToUpdate.importance;
                    dataToUpdate.comments = objToUpdate.comments;
                    destinationArray = destinationArray + '.' + objToUpdate.val.toString();
                    action = '$set';

                } else {
                    if (shouldDeleteMitigated) {
                        action = '$pull';
                    } else {
                        action = '$addToSet';
                    }
                }

                collection.update(find, {[action]: {[destinationArray]: dataToUpdate}}, (err) => {
                    client.close();
                    if (err) {
                        console.error('In updateAssetUserInputAndMitigatedNodesOnMongo() - Failed To update Finding Status Data To DB : ' + err);
                        reject({ok: false});
                    } else {
                        console.log('In updateAssetUserInputAndMitigatedNodesOnMongo() - Finding Status Data updated!');
                        resolve({ok: true});
                    }
                });
            } else {
                console.error('In updateAssetUserInputAndMitigatedNodesOnMongo() - Failed To update Finding Status Data To mongoDB - one or more props are missing');
                client.close();
                reject({ok: false});
            }
        });
    });
};

const getAssetUserInputAndMitigatedByDataTypeFromMongo = (type, orgId) => {
    return new Promise((resolve) => {
        return MongoClient.connect(mongoUrl, async (err, client) => {
            try {
                if (type && orgId) {
                    const db = client.db('darkvision');
                    const collection = db.collection('organizations_map');
                    const find = PromiseB.promisify(collection.find, {context: collection});

                    let orgValidObjId = '';

                    if (orgId && orgId !== '') {
                        orgValidObjId = ObjectId(orgId.toString());
                    }
                    let destination = 'assetConfigurationByUser.' + type;

                    const list = await find({_id: orgValidObjId}, {[destination]: 'assetUserInput', [destination]: 'mitigated'});
                    if (list) {
                        const result = await listToArray(list);
                        if (result && result[0]) {
                            let res = result[0];
                            if (res.assetConfigurationByUser && res.assetConfigurationByUser[type]) {
                                let mergedRes = [];
                                if (res.assetConfigurationByUser[type].assetUserInput && typeof res.assetConfigurationByUser[type].assetUserInput === 'object') {
                                    Object.keys(res.assetConfigurationByUser[type].assetUserInput).map((currData) => {
                                        mergedRes.push(res.assetConfigurationByUser[type].assetUserInput[currData]);
                                    });
                                }
                                if (res.assetConfigurationByUser[type].mitigated && Array.isArray(res.assetConfigurationByUser[type].mitigated)) {
                                    mergedRes = mergedRes.concat(res.assetConfigurationByUser[type].mitigated);
                                }
                                client.close();
                                console.log('In getAssetUserInputAndMitigatedByDataTypeFromMongo - Returned Successfully');
                                resolve(mergedRes);
                            } else {
                                client.close();
                                console.log('In getAssetUserInputAndMitigatedByDataTypeFromMongo - no data returned');
                                resolve([]);
                            }
                        } else {
                            client.close();
                            console.log('In getAssetUserInputAndMitigatedByDataTypeFromMongo - no data returned');
                            resolve([]);
                        }
                    } else {
                        client.close();
                        console.log('In getAssetUserInputAndMitigatedByDataTypeFromMongo - no data returned');
                        resolve([]);
                    }
                } else {
                    console.error('In getAssetUserInputAndMitigatedByDataTypeFromMongo - properties are missing.');
                    resolve([]);
                }
            } catch (e) {
                console.error('In getAssetUserInputAndMitigatedByDataTypeFromMongo - Error: ' + e);
                client.close();
                resolve([]);
            }
        });
    });
};
