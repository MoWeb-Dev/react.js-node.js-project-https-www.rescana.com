const chai = require('chai');
const mocha = require('mocha');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const helpers = require('../utils-helpers');

describe('Helpers', function() {
    describe('cleanSurveyObject() - Deletes properties that should not be shown', function() {
        it('Should remove props when input is array',async function() {
            const surveyArr = [{
                '_id': '5962354642b97e3620ba0ba8',
                'name': 'Survey with users',
                'sid': 'S14fk2JBZ',
                'pages': [{
                    'name': 'Page 1',
                    'elements': [{
                        'type': 'panel',
                        'name': 'Category 1',
                        'elements': [{
                            'type': 'radiogroup',
                            'name': 'Enter question here',
                            'choices': ['Non Comply', 'Comply', 'Other'],
                            'withText': {'name': 'Comments'}
                        }]
                    }]
                }],
                'createDate': '2017-07-09T16:26:35+03:00',
                'uid': [{'id': '58f8c421f7f5de03d412d039', 'email': 'example@gmail.com', 'name': ''}]
            }];
            const result = await helpers.cleanSurveyObject(surveyArr);
            expect(result).to.be.an('array');
            return expect(result).to.not.contain.any.keys('_id');
        });
    });
});
