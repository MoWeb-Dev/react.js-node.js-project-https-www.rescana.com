const chai = require('chai');
const mocha = require('mocha');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const helpers = require('../intel-helpers.js');
const neoHelpers = require('helpers');
const moment = require('moment');
const sinon = require('sinon');
const dns = require('dns');
const request = require('request-promise');
const {DASHBOARD_COMPANIES_SORT_TYPES} = require('../../../../config/consts.js');

describe('intel Helpers', function() {
    before(function() {
        sinon.stub(neoHelpers, 'createNodeAndRelationship').resolves({});
    });

    describe('lookUp() - Returns a promise with array of IPs', function() {

        it('Should handle domains with protocol', function() {
            const domainWithProtocol = 'http://kiwinet.co.il';
            return expect(helpers.lookUp(domainWithProtocol)).to.eventually.be.an('array');
        });

        it('Should handle domains without protocol', function() {
            const domainWithOutProtocol = 'kiwinet.co.il';
            return expect(helpers.lookUp(domainWithOutProtocol)).to.eventually.be.an('array');
        });

        it('Should return empty array when input is bad', function() {
            const domainWithOutProtocol = 'kiwinet';
            return expect(helpers.lookUp(domainWithOutProtocol)).to.eventually.be.an('array');
        });

        it('Should return empty array when input is empty', function() {
            return expect(helpers.lookUp()).to.eventually.be.an('array');
        });
    });

    describe('setspecialCase() - returns data object', function() {
        it('Should handle object and item', function() {
            const item = 'vpn';
            const relData = {};
            relData.relationshipLabel = 'tags';
            relData.toNodeLabl = 'tags';
            relData.toNodeType = 'tags';
            return expect(neoHelpers.setspecialCase(relData, item)).to.be.an('object');
        });

        it('Should handle no input', function() {
            return expect(neoHelpers.setspecialCase()).to.deep.equal({relData: {}, prop: ''});
        });
    });

    describe('ip2Asn() - Returns a promise with object of ASN data', function() {
        it('Should receive ip array', function() {
            this.timeout(30000);
            const ipArray = ['127.0.0.1'];
            return expect(helpers.ip2Asn(ipArray)).to.be.fulfilled;
        });
    });


    describe('convertASNObj() - Converts ASN objects to Array of objects with ASN data and IPs', function() {
        it('Should receive ASNObj', function() {
            const ASNObj = {
                '185.70.251.118':
                    {
                        range: '185.70.248.0/22',
                        countryCode: 'IL',
                        ASN: '201415',
                        registrar: 'ripencc',
                        dateString: '2014-09-23',
                        description: 'XPM, IL'
                    }
            };
            expect(helpers.convertASNObj(ASNObj)).to.be.an('array');
            expect(helpers.convertASNObj(ASNObj)).to.deep.include.members([{
                asn: {
                    range: '185.70.248.0/22',
                    countryCode: 'IL',
                    ASN: '201415',
                    registrar: 'ripencc',
                    dateString: '2014-09-23',
                    description: 'XPM, IL'
                },
                ip: '185.70.251.118'
            }]);
        });
    });

    describe('appearsInProviderList() - Checks if string appears in list and returns bool', function() {
        it('Should receive string and return true if in list', function() {
            expect(helpers.appearsInProviderList('google')).to.be.true;
        });

        it('Should receive string and return false if not in list', function() {
            expect(helpers.appearsInProviderList('something')).to.be.false;
        });

        it('Should receive string and return true if one word of asn name is in list', function() {
            expect(helpers.appearsInProviderList('google LTD')).to.be.true;
        });

        it('Should receive string and return true if part of word of asn name is in list', function() {
            expect(helpers.appearsInProviderList('goog')).to.be.true;
        });

        it('Should receive string and return true case insensitive if part of asn name is in list', function() {
            expect(helpers.appearsInProviderList('AmazOn')).to.be.true;
        });

        it('Should receive a domain and return true if the first hostname is in list', function() {
            expect(helpers.appearsInProviderList('amazon.com')).to.be.true;
        });

        it('Should receive a string and return true if the asn is in list', function() {
            expect(helpers.appearsInProviderList('nv-asn 013 netvision ltd.')).to.be.true;
        });

        it('Should receive a string and should not return true if part of the ASN name is shorter then 2 letters', function() {
            expect(helpers.appearsInProviderList('moshe IL')).to.be.false;
        });

        it('Should receive a domain and should return true if the ASN name is included in the hostname', function() {
            expect(helpers.appearsInProviderList('as-26496-go-daddy-com-llc - godaddy.com, llc, us')).to.be.true;
        });

        it('Should receive a domain and should return true if the ASN name with dashes is included in the hostname', function() {
            expect(helpers.appearsInProviderList('as-26496-go-daddy-com-llc')).to.be.true;
        });

        it('Should receive a domain and should return false if it has coontains a word from the white list', function() {
            expect(helpers.appearsInProviderList('senior ltd')).to.be.false;
        });

        it('Should receive a domain and should return false if it has coontains a word from the white list with a capital letter', function() {
            expect(helpers.appearsInProviderList('Senior')).to.be.false;
        });
    });

    describe('validateUnionQuery() - Take off redundent UNION from query', function() {
        it('Should return string', function() {
            const query = 'MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`asn`  {name:"asn" , finding : "15169"}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`asn`]->(b) MERGE (a)-[k:`asn` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`asn`]->(b:`asn`  {name:"asn" , finding : "15169"}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"  UNION MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`country`  {name:"country" , finding : "US"}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`country`]->(b) MERGE (a)-[k:`country` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`country`]->(b:`country`  {name:"country" , finding : "US"}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"  UNION MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`as_owner`  {name:"as_owner" , finding : "Google Inc."}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`as_owner`]->(b) MERGE (a)-[k:`as_owner` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`as_owner`]->(b:`as_owner`  {name:"as_owner" , finding : "Google Inc."}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"  UNION ';
            const result = neoHelpers.validateUnionQuery(query);
            expect(result).to.be.a('string');
        });

        it('Should return a string without union at the end', function() {
            const query = 'MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`asn`  {name:"asn" , finding : "15169"}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`asn`]->(b) MERGE (a)-[k:`asn` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`asn`]->(b:`asn`  {name:"asn" , finding : "15169"}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"  UNION MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`country`  {name:"country" , finding : "US"}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`country`]->(b) MERGE (a)-[k:`country` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`country`]->(b:`country`  {name:"country" , finding : "US"}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"  UNION MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`as_owner`  {name:"as_owner" , finding : "Google Inc."}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`as_owner`]->(b) MERGE (a)-[k:`as_owner` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`as_owner`]->(b:`as_owner`  {name:"as_owner" , finding : "Google Inc."}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"  UNION ';
            const result = neoHelpers.validateUnionQuery(query);
            const resultArr = result.split(' UNION ');
            expect(resultArr[resultArr.length - 1]).to.not.equal('');
        });

        it('Should handle correct query', function() {
            const query = 'MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`asn`  {name:"asn" , finding : "15169"}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`asn`]->(b) MERGE (a)-[k:`asn` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`asn`]->(b:`asn`  {name:"asn" , finding : "15169"}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"  UNION MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`country`  {name:"country" , finding : "US"}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`country`]->(b) MERGE (a)-[k:`country` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`country`]->(b:`country`  {name:"country" , finding : "US"}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"  UNION MERGE (a:IP{address:"130.211.46.223"}) MERGE (b:`as_owner`  {name:"as_owner" , finding : "Google Inc."}) WITH a,b MATCH (a), (b) WHERE NOT (a)-[:`as_owner`]->(b) MERGE (a)-[k:`as_owner` {resolve_time:"2017-12-21T11:42:30+02:00" , scanTime : "2017-12-21T11:42:30+02:00" , scanId : "S1JfGWKGf"}]->(b) WITH count(*) as dummy MATCH (a:IP{address:"130.211.46.223"})-[k:`as_owner`]->(b:`as_owner`  {name:"as_owner" , finding : "Google Inc."}) SET k.scanId="S1JfGWKGf", k.scanTime="2017-12-21T11:42:30+02:00"';
            const result = neoHelpers.validateUnionQuery(query);
            const resultArr = result.split(' UNION ');
            expect(resultArr[resultArr.length - 1]).to.not.equal('');
        });
    });

    describe('filterCompanies() - Should return companies by filter', function() {
        it('Should return string', function() {
            const filters = ['23133'];
            const companies = ['23133', '11111111'];
            const result = helpers.filterCompanies(filters, companies);
            expect(result).to.be.an('array');
        });
    });

    describe('countEmailBreaches() - Should return Email breaches count by years', function() {
        it('Should always return an object', function() {
            const input = undefined;
            const result = helpers.countEmailBreaches(input);
            const expectedResult = !!(result && typeof (result) === 'object'
                && result.hasOwnProperty('breachesYearsCount')
                && result.hasOwnProperty('breachedEmailsCount')
                && result.hasOwnProperty('dataCounts') && Array.isArray(result['dataCounts']));
            expect(expectedResult).to.equal(true);
        });

        it('Should return a distinct count by years', function() {
            // input will be ordered from highest year to lowest one and there will be no duplicated emails at each year - Due to helpers.getEmailBreachesCount().
            const input = [
                {'breach_year': '2018', 'email': 'a@a.com'},
                {'breach_year': '2018', 'email': 'b@b.com'},
                {'breach_year': '2018', 'email': 'c@c.com'},
                {'breach_year': '2005', 'email': 'a@a.com'},
                {'breach_year': '2005', 'email': 'c@c.com'}
            ];
            const result = helpers.countEmailBreaches(input);
            const expectedResult = !!(result && typeof (result) === 'object'
                && result['breachesYearsCount'] === 2
                && result['breachedEmailsCount'] === 3
                && result['dataCounts'].length === 2
                && result['dataCounts'][0]['breachYear'] === '2018' && result['dataCounts'][0]['breachedEmails'] === 3
                && result['dataCounts'][1]['breachYear'] === '2005' && result['dataCounts'][1]['breachedEmails'] === 2);
            expect(expectedResult).to.equal(true);
        });
    });

    describe('countBlacklists() - Should return Blacklists counts and matching IPs count', function() {
        it('Should always return an object', function() {
            const input = undefined;
            const result = helpers.countBlacklists(input);
            const expectedResult = !!(result && typeof (result) === 'object'
                && result.hasOwnProperty('highestTotalBlacklistsCount')
                && result.hasOwnProperty('recordsWithHighestTotalBlacklistsCount')
                && result.hasOwnProperty('dataCounts') && typeof (result['dataCounts']) === 'object'
                && result.hasOwnProperty('highestBlacklistsByTypeCount') && typeof (result['highestBlacklistsByTypeCount']) === 'object');
            expect(expectedResult).to.equal(true);
        });

        it('Should return a distinct counts by IPs', function() {
            const input = [
                {
                    '_id': '5aeed1c79e01528bd2a40569',
                    'domain': 'cola.com',
                    'data': {
                        'blacklist_domain': [],
                        'blacklist_mx': [
                            'LISINGE-DED',
                            'IVOLO-DED',
                            'MARTENSON-DED',
                            'DEA'
                        ],
                        'blacklist_ns': [],
                        'blacklist_ip': {
                            'address': '185.53.179.8',
                            'blacklist': [
                                'BBCAN177-MS1'
                            ],
                            'isQuarantined': false
                        },
                        'mx': [
                            'mail.h-email.net'
                        ],
                        'ns': [
                            'ns2.parkingcrew.net',
                            'ns1.parkingcrew.net'
                        ]
                    },
                    'resolve_time': '2018-05-28T13:37:29+03:00'
                },
                {
                    '_id': '5af9a64b52590c08931d7e53',
                    'domain': 'appboy.info',
                    'data': {
                        'blacklist_domain': [
                            'MARTENSON-DED'
                        ],
                        'blacklist_mx': [
                            'IVOLO-DED',
                            'LISINGE-DED',
                            'MARTENSON-DED'
                        ],
                        'blacklist_ns': [],
                        'blacklist_ip': {
                            'address': '192.64.119.162',
                            'blacklist': [],
                            'isQuarantined': false
                        },
                        'mx': [
                            'eforward4.registrar-servers.com',
                            'eforward2.registrar-servers.com',
                            'eforward5.registrar-servers.com',
                            'eforward3.registrar-servers.com',
                            'eforward1.registrar-servers.com'
                        ],
                        'ns': [
                            'dns2.registrar-servers.com',
                            'dns1.registrar-servers.com'
                        ]
                    },
                    'resolve_time': '2018-05-15T17:05:33+03:00'
                },
                {
                    '_id': '5af9a64b52590c08931d7e55',
                    'domain': 'appboy.org',
                    'data': {
                        'blacklist_domain': [],
                        'blacklist_mx': [
                            'LISINGE-DED',
                            'MARTENSON-DED'
                        ],
                        'blacklist_ns': [],
                        'blacklist_ip': {
                            'address': '162.255.119.107',
                            'blacklist': [
                                'MARTENSON-DED-IP',
                                'IVOLO-DED-IP',
                                'LISINGE-DED-IP'
                            ],
                            'isQuarantined': false
                        },
                        'mx': [
                            'eforward3.registrar-servers.com',
                            'eforward4.registrar-servers.com',
                            'eforward2.registrar-servers.com',
                            'eforward1.registrar-servers.com',
                            'eforward5.registrar-servers.com'
                        ],
                        'ns': [
                            'dns4.registrar-servers.com',
                            'dns5.registrar-servers.com',
                            'dns3.registrar-servers.com',
                            'dns1.registrar-servers.com',
                            'dns2.registrar-servers.com'
                        ]
                    },
                    'resolve_time': '2018-05-15T17:05:33+03:00'
                }
            ];
            const result = helpers.countBlacklists(input);
            // dataCounts' keys are integers and Object.keys turns them into strings, so need to parse it back to int.
            let dataCountsData = [] ;

            Object.keys(result.dataCounts).map((curkey) => {
                result.dataCounts[curkey].blacklistsAmount = Number(curkey);
                dataCountsData.push(result.dataCounts[curkey]);
            });
            const expectedResult = !!(result && typeof (result) === 'object'
                && result['highestTotalBlacklistsCount'] === 5
                && result['recordsWithHighestTotalBlacklistsCount'] === 2
                && dataCountsData[0].blacklistsAmount === 3 && result['dataCounts'][dataCountsData[0].blacklistsAmount].count === 1
                && dataCountsData[1].blacklistsAmount === 5 && result['dataCounts'][dataCountsData[1].blacklistsAmount].count === 2
                && result['highestBlacklistsByTypeCount']['domain'] === 1
                && result['highestBlacklistsByTypeCount']['ip'] === 3
                && result['highestBlacklistsByTypeCount']['mx'] === 4
                && result['highestBlacklistsByTypeCount']['ns'] === 0
            );
            expect(expectedResult).to.equal(true);
        });
    });

    describe('countCVEs() - Should return CVEs counts sorted by critical and medium and also maxCVSSCounts', function() {
        it('Should always return an object', function() {
            const input = undefined;
            const result = helpers.countCVEs(input, null);
            const expectedResult = !!(result && typeof (result) === 'object'
                && result.hasOwnProperty('maxIntelScore')
                && result.hasOwnProperty('highestCVSSCount')
                && result.hasOwnProperty('criticalCVSSCount')
                && result.hasOwnProperty('mediumCVSSCount'));
            expect(expectedResult).to.equal(true);
        });

        it('Should return a distinct counts by CVSS', function() {
            const input = [
                {
                    'cve': 'CVE-2014-0098',
                    'cveScore': 5.0
                },
                {
                    'cve': 'CVE-2014-0098',
                    'cveScore': 5.0
                },
                {
                    'cve': 'CVE-2017-3169',
                    'cveScore': 8
                },
                {
                    'cve': 'CVE-2016-8743',
                    'cveScore': 5
                },
                {
                    'cve': 'CVE-2013-2249',
                    'cveScore': 7.5
                },
                {
                    'cve': 'CVE-2013-1896',
                    'cveScore': 3.3
                }
            ];
            const result = helpers.countCVEs(input, 8.0);
            const expectedResult = !!(result && typeof (result) === 'object'
                && result['maxIntelScore'] === 8
                && result['highestCVSSCount'] === 1
                && result['criticalCVSSCount'] === 2
                && result['mediumCVSSCount'] === 2
            );
            expect(expectedResult).to.equal(true);
        });

        it('Should return a distinct count of highest CVSS even if there are no critical or medium', function() {
            const input = [
                {
                    'cve': 'CVE-2014-0098',
                    'cveScore': 3.0
                },
                {
                    'cve': 'CVE-2014-0098',
                    'cveScore': 3
                },
                {
                    'cve': 'CVE-2017-3169',
                    'cveScore': 3.5
                },
                {
                    'cve': 'CVE-2016-8743',
                    'cveScore': 3.8
                },
                {
                    'cve': 'CVE-2016-8743',
                    'cveScore': 3.8
                }
            ];
            const result = helpers.countCVEs(input, 3.8);

            const expectedResult = !!(result && typeof (result) === 'object'
                && result['maxIntelScore'] === 3.8
                && result['highestCVSSCount'] === 1
                && result['criticalCVSSCount'] === 0
                && result['mediumCVSSCount'] === 0
            );
            expect(expectedResult).to.equal(true);
        });
    });

    describe('getIntelScoreParts - Should calculate all intel score parts', function() {
        describe('calcIntelScoreEmailBreaches() - Should calc score for Email Breaches', function() {
            let currentYear;

            before(function() {
                currentYear = moment().year();
            });

            it('calcIntelScoreEmailBreaches() Should calc score for an empty input', function() {
                const input = {dataCounts: []};
                const result = helpers.calcIntelScoreEmailBreaches(input);
                const expectedResult = !!(result && typeof (result) === 'object'
                    && result['score'] === 0
                );
                expect(expectedResult).to.equal(true);
            });

            it('calcIntelScoreEmailBreaches() Should calc score for normal input', function() {
                const input = {dataCounts: [
                    {breachYear: '' + currentYear, breachedEmails: 3, breachedEmailsObjectsArr: [{},{},{}]},
                    {breachYear: '' + (currentYear - 1), breachedEmails: 2, breachedEmailsObjectsArr: [{},{}]},
                    {breachYear: '' + (currentYear - 14), breachedEmails: 5, breachedEmailsObjectsArr: [{},{},{},{},{}]}
                ]};
                const result = helpers.calcIntelScoreEmailBreaches(input);
                const expectedResult = !!(result && typeof (result) === 'object'
                    && result['score'] === 46
                );
                expect(expectedResult).to.equal(true);
            });

            it('calcIntelScoreEmailBreaches() Should calc score for input with total score above than 100', function() {
                const input = {dataCounts: [
                    {breachYear: '' + currentYear, breachedEmails: 3, breachedEmailsObjectsArr: [{},{},{}]},
                    {breachYear: '' + (currentYear - 1), breachedEmails: 2, breachedEmailsObjectsArr: [{},{}]},
                    {breachYear: '' + (currentYear - 3), breachedEmails: 8, breachedEmailsObjectsArr: [{importance: 4},{importance: 4},{importance: 4},{importance: 4},{},{},{},{}]},
                    {breachYear: '2004', breachedEmails: 5, breachedEmailsObjectsArr: [{},{},{},{},{}]}
                ]};
                const result = helpers.calcIntelScoreEmailBreaches(input);
                const expectedResult = !!(result && typeof (result) === 'object'
                    && result['score'] === 100
                );
                expect(expectedResult).to.equal(true);
            });

            it('calcIntelScoreEmailBreaches() Should calc score for input with old findings', function() {
                const inputWithOldFindings = {dataCounts: [
                    {breachYear: '2004', breachedEmails: 5},
                    {breachYear: '2002', breachedEmails: 1}
                ]};
                const result = helpers.calcIntelScoreEmailBreaches(inputWithOldFindings);
                const expectedResult = !!(result && typeof (result) === 'object'
                    && result['score'] === 10
                );
                expect(expectedResult).to.equal(true);
            });
        });

        describe('calcIntelScoreBlacklists() - Should calc score for Blacklists', function() {
            it('calcIntelScoreBlacklists() Should calc score for an empty input', function() {
                const input = {dataCounts: {}};
                const result = helpers.calcIntelScoreBlacklists(input);
                const expectedResult = !!(result && typeof (result) === 'object'
                    && result['score'] === 0
                );
                expect(expectedResult).to.equal(true);
            });

            it('calcIntelScoreBlacklists() Should calc score for normal input', function() {
                const input = {
                    dataCounts: {
                        5: {count: 1, entityData: {}},
                        4: {count: 2, entityData: {}},
                    }
                };
                const result = helpers.calcIntelScoreBlacklists(input);
                const expectedResult = !!(result && typeof (result) === 'object'
                    && result['score'] === 66
                );
                expect(expectedResult).to.equal(true);
            });

            it('calcIntelScoreBlacklists() Should calc score for input with total score above than 100', function() {
                const input = {
                    dataCounts: {
                        5: {count: 3, entityData: {}},
                        4: {count: 2, entityData: {}},
                    }
                };
                const result = helpers.calcIntelScoreBlacklists(input);
                const expectedResult = !!(result && typeof (result) === 'object'
                    && result['score'] === 100
                );
                expect(expectedResult).to.equal(true);
            });

            it('calcIntelScoreBlacklists() Should calc score for input with zero findings', function() {
                const input = {
                    dataCounts: {
                        0: 0
                    }
                };
                const result = helpers.calcIntelScoreBlacklists(input);
                const expectedResult = !!(result && typeof (result) === 'object'
                    && result['score'] === 0
                );
                expect(expectedResult).to.equal(true);
            });
        });
    });

    describe('isNSResultRelatedToCompany() - Indicates if the result is new and related to the company', function() {
        const rootDomain = 'dnsimple';
        const newReverseNS = 'ns1.' + rootDomain + '.com';

        it('Should check if new reverseNS result related to root domain', async function() {
            const result = await helpers.isNSResultRelatedToCompany(newReverseNS, [], rootDomain, '', [
                'tnuva.co.il',
                'openrest.org'
            ]);
            return expect(result).to.equal(true);
        });

        it('Should check if new reverseNS result related to company domains', async function() {
            const result = await helpers.isNSResultRelatedToCompany(newReverseNS, [], 'aaa', '', [
                'tnuva.co.il',
                rootDomain + '.org'
            ]);
            return expect(result).to.equal(true);
        });

        it('Should check if new reverseNS result is actually new', async function() {
            const isValidResult = await helpers.isNSResultRelatedToCompany(newReverseNS, [], rootDomain, '', []);
            const isValid = expect(isValidResult).to.equal(true);
            const isExistResult = await helpers.isNSResultRelatedToCompany(newReverseNS, [newReverseNS], rootDomain, '', []);
            const isExist = expect(isExistResult).to.equal(false);
            return isValid && isExist;
        });

        it('Should handle NULLs', async function() {
            const result = await helpers.isNSResultRelatedToCompany();
            return expect(result).to.equal(false);
        });
    });

    describe('resolveNs() - Returns a promise with array of new domains discovered by reverseNS', function() {
        const domainWithoutProtocol = 'aaa.com';

        beforeEach(() => {
            // Mock dns.resolveNs to resolve fake data without calling real dns API.
            sinon.stub(dns, 'resolveNs').resolves(['ns1.' + domainWithoutProtocol]);
            sinon.stub(helpers, 'promisifyResolveNS').resolves(['ns1.' + domainWithoutProtocol]);

            // Mock request.get to resolve fake data without calling real viewDNS API.
            sinon.stub(request, 'get').resolves({
                response: {
                    total_pages: "1",
                    domains: [
                        {domain: 'aaa.com'},
                        {domain: 'bbb.com'},
                        {domain: 'bbb.com'},
                        {domain: 'ccc.com'},
                        {domain: 'ddd.com'}
                    ]
                }
            });

            // Mock neoAndMongoHelpers.insertDiscoveredDomainsFindings to resolve fake data to avoid unnecessary writes to DB.
            sinon.stub(neoHelpers, 'insertDiscoveredDomainsFindings').resolves({ok: true});
        });

        afterEach(() => {
            dns.resolveNs.restore();
            helpers.promisifyResolveNS.restore();
            neoHelpers.insertDiscoveredDomainsFindings.restore();
        });

        it('Should resolve related domains with identical NS records', async function() {
            this.timeout(30000);
            const result = await helpers.resolveNS(domainWithoutProtocol, [], '', true);
            expect(result).to.be.an('array');
            expect(result.length).to.equal(3);
        });
    });

    describe('getCompaniesSortFunc() - Returns matching sort function by input', function() {
        let inputArray = [];

        // Before each sort - initialize the input array.
        beforeEach(() => {
            inputArray = [{
                companyName: 'company 4',
                scoresData: {
                    totalScore: 75
                }
            }, {
                companyName: 'company 10',
                scoresData: {
                    totalScore: '22'
                }
            }, {
                companyName: 'company 1',
                scoresData: {
                    totalScore: 46
                }
            }, {
                companyName: 'Vendor',
                scoresData: undefined
            }];
        });

        it('Should handle null input', function() {
            const sortFunc = helpers.getCompaniesSortFunc();
            return expect(sortFunc).to.equal(undefined);
        });

        it('Should return nothing when default sort method is inserted', function() {
            const sortFunc = helpers.getCompaniesSortFunc(DASHBOARD_COMPANIES_SORT_TYPES[0]);
            return expect(sortFunc).to.equal(undefined);
        });

        it('Should return a function for all sort method types other than default', function() {
            // Starting from index 1 to skip default.
            for (let i = 1; i < DASHBOARD_COMPANIES_SORT_TYPES.length; i++) {
                const sortFunc = helpers.getCompaniesSortFunc(DASHBOARD_COMPANIES_SORT_TYPES[i]);
                expect(sortFunc).to.be.a('function');
            }
        });

        it('Should sort companies array by Highest Risk First', function() {
            const sortFunc = helpers.getCompaniesSortFunc(DASHBOARD_COMPANIES_SORT_TYPES[1]);
            const expectedResult = [inputArray[0], inputArray[2], inputArray[1], inputArray[3]];
            const expectedResultStr = JSON.stringify(expectedResult);
            const sortedArray = inputArray.sort(sortFunc);
            const sortedArrayStr = JSON.stringify(sortedArray);
            return expect(sortedArrayStr).to.equal(expectedResultStr);
        });

        it('Should sort companies array by Lowest Risk First', function() {
            const sortFunc = helpers.getCompaniesSortFunc(DASHBOARD_COMPANIES_SORT_TYPES[2]);
            const expectedResult = [inputArray[3], inputArray[1], inputArray[2], inputArray[0]];
            const expectedResultStr = JSON.stringify(expectedResult);
            const sortedArray = inputArray.sort(sortFunc);
            const sortedArrayStr = JSON.stringify(sortedArray);
            return expect(sortedArrayStr).to.equal(expectedResultStr);
        });

        it('Should sort companies array by Alphabetic Ascending', function() {
            const sortFunc = helpers.getCompaniesSortFunc(DASHBOARD_COMPANIES_SORT_TYPES[5]);
            const expectedResult = [inputArray[2], inputArray[0], inputArray[1], inputArray[3]];
            const expectedResultStr = JSON.stringify(expectedResult);
            const sortedArray = inputArray.sort(sortFunc);
            const sortedArrayStr = JSON.stringify(sortedArray);
            return expect(sortedArrayStr).to.equal(expectedResultStr);
        });

        it('Should sort companies array by Alphabetic Descending', function() {
            const sortFunc = helpers.getCompaniesSortFunc(DASHBOARD_COMPANIES_SORT_TYPES[6]);
            const expectedResult = [inputArray[3], inputArray[1], inputArray[0], inputArray[2]];
            const expectedResultStr = JSON.stringify(expectedResult);
            const sortedArray = inputArray.sort(sortFunc);
            const sortedArrayStr = JSON.stringify(sortedArray);
            return expect(sortedArrayStr).to.equal(expectedResultStr);
        });
    });


    describe('updateCompanyDataByScoreDataField() - update Company Data By ScoreData Field', function() {
        let company = {
            "companyName": "Bonshine",
            "intelScoreRatios": {
                "cveWeight": 30,
                "dnsWeight": 10,
                "bucketsWeight": 10,
                "breachesWeight": 10,
                "dataleaksWeight": 10,
                "blacklistsWeight": 10,
                "botnetsWeight": 10,
                "sslCertsWeight": 10
            },
            "selectedDomains": [
                "bonshine.co.il",
                "bonshine.com"
            ],
            "sensitivity": 2,
            "ratios": {
                "intelWeight": 50,
                "surveyWeight": 50
            },
            "createDate": "2019-07-09T10:55:16+03:00",
            "id": "5ccb1165f371e460784e1bc0",
            "maxIntelScore": 0
        };

        let company2 = {
            "companyName": "Bonshine",
            "selectedDomains": [
                "bonshine.co.il",
                "bonshine.com"
            ],
            "sensitivity": 2,
            "ratios": {
                "intelWeight": 50,
                "surveyWeight": 50
            },
            "createDate": "2019-07-09T10:55:16+03:00",
            "id": "5ccb1165f371e460784e1bc0",
            "maxIntelScore": 0
        };

        let scoresData = {
            "companyID": "5ccb1165f371e460784e1bc0",
            "intelScoreRatios": {
                "cveWeight": 30,
                "dnsWeight": 10,
                "bucketsWeight": 10,
                "breachesWeight": 10,
                "dataleaksWeight": 10,
                "blacklistsWeight": 10,
                "botnetsWeight": 10,
                "sslCertsWeight": 10
            },
            "countCVEs": {
                "maxIntelScore": 0,
                "highestCVSSCount": 0,
                "score": 0,
                "scoreData": {
                    "criticalCVSSCount": 0,
                    "mediumCVSSCount": 0
                }
            },
            "countDNS": {
                "totalDomains": 2,
                "score": 0,
                "scoreData": {
                    "countNotConfiguredSPFs": 0,
                    "countNotConfiguredDMARCs": 0
                }
            },
            "countBuckets": {
                "score": 0,
                "scoreData": {
                    "publicBucketsCount": 0,
                    "totalBucketsCount": 0
                }
            },
            "countEmailBreaches": {
                "breachesYearsCount": 0,
                "dataCounts": [],
                "score": 0,
                "scoreData": {
                    "breachedEmailsCount": 0,
                    "totalEmailsCount": 0
                }
            },
            "countDataleaks": {
                "score": 0,
                "scoreData": {
                    "criticalDataleaks": 0,
                    "mediumDataleaks": 0
                }
            },
            "countBlacklists": {
                "score": 0,
                "scoreData": {
                    "ip": 0,
                    "mx": 0
                }
            },
            "countBotnets": {
                "score": 0,
                "scoreData": {
                    "infectedDomainsCount": 0,
                    "totalDomainsCount": 0
                }
            },
            "countSSLCerts": {
                "score": 0,
                "scoreData": {
                    "validHttpsDomainsCount": 0,
                    "notValidHttpsDomainsCount": 0
                }
            },
            "surveyData": {
                "said": "Vh-padSF4",
                "progress": 40,
                "surveyScore": 40,
                "counts": {
                    "complyCount": 0,
                    "nonComplyCount": 2,
                    "otherCount": 0,
                    "NACount": 3
                }
            },
            "intelScore": 0
        };


        it('Should return object with specific fields', function() {
            const result = helpers.updateCompanyDataByScoreDataField(scoresData, company);
            expect(result.intelScore).to.equal(undefined);
            expect(result.countEmailBreaches.dataCounts).to.equal(undefined);
            expect(company.surveyScore).to.equal(scoresData.surveyData.surveyScore);
            return expect(result).to.be.an('object');
        });

        it('Should return the same result that we send as scoreData (empty array)', function() {
            const result = helpers.updateCompanyDataByScoreDataField([], company);
            expect(result.length).to.equal(0);
            return expect(result).to.be.an('array');
        });

        it('Should return object with specific fields', function() {
            const result = helpers.updateCompanyDataByScoreDataField(scoresData, company2);
            expect(result.intelScoreRatios).to.equal(scoresData.intelScoreRatios);
            expect(result.countEmailBreaches.dataCounts).to.equal(undefined);
            return expect(result).to.be.an('object');
        });

        it('Should return 0', function() {
            const result = helpers.updateCompanyDataByScoreDataField(0, company2);
            expect(result).to.equal(0);
            return expect(result).to.be.a('number');
        });
    });
});

describe('appearsInDomainName() - Should check if string appears in domain', function() {

    it('Should return true for string and domain', function() {
        const result = helpers.appearsInDomainName("carbyne.com", ["carbyne domains"], false);
        return expect(result).to.equal(true);
    });

    it('Should return false if string is not in domain', function() {
        const result = helpers.appearsInDomainName("carbyne.com", ["banana"], false);
        return expect(result).to.equal(false);
    });

    it('Should return false if string is in blacklist', function() {
        const result = helpers.appearsInDomainName("carbyne.com", ["privacy"], true);
        return expect(result).to.equal(false);
    });

    it('Should return false if domain is in blacklist', function() {
        const result = helpers.appearsInDomainName("privacy.com", ["banana"], true);
        return expect(result).to.equal(false);
    });

    it('Should return true none of the two strings is in blacklist', function() {
        const result = helpers.appearsInDomainName("carbyne.com", ["carbyne", "carbyne2"], true);
        return expect(result).to.equal(true);
    });

    it('Should return true if one of the domain is in the string', function() {
        const result = helpers.appearsInDomainName("carbyne", ["carbyne.com"], true);
        return expect(result).to.equal(true);
    });
});

describe('createStringArr() - Should create from array a single string with the array variables.', function() {

    it('Should return string 1 - send array as it should', function() {
        const result = helpers.createStringArr(['hello','world']);
        return expect('"hello","world"').to.equal(result);
    });

    it('Should return string 2 - send num in the array', function() {
        const result = helpers.createStringArr([8,8]);
        return expect('"8","8"').to.equal(result);
    });

    it('Should return string 3 - send arr inside of array', function() {
        const result = helpers.createStringArr([8,"dsa",["asd"]]);
        return expect('"8","dsa","asd"').to.equal(result);
    });

    it('Should return string 4 - send arr inside of array with null', function() {
        const result = helpers.createStringArr([8,"dsa",[null]]);
        return expect('"8","dsa",""').to.equal(result);
    });

    it('Should return string 5 - send null', function() {
        const result = helpers.createStringArr(null);
        return expect('').to.equal(result);
    });

    it('Should return string 6 - send num', function() {
        const result = helpers.createStringArr(8);
        return expect('').to.equal(result);
    });
});


describe('checkFindingStatusObj() - return true or false if all needed properties are exists.', function () {

    let obj = {
        uid: 'efrdt14vs34csmxapw',
        userEmail: "tzach@gmail.com",
        curOrgObj: {
            orgId: '123d3214123da92b1e9suj',
            orgName: 'Some Organization',
        },
        statusType: 'assetUserInput',
        objToUpdate: {type: 'spf', val: 'some val'},
        shouldDetachMitigatedConnection: false,
        isAssetUserInputUpdate: true,

    };

    let objWithWrongStatusType = {
        uid: 'efrdt14vs34csmxapw',
        userEmail: "tzach@gmail.com",
        curOrgObj: {
            orgId: '123d3214123da92b1e9suj',
            orgName: 'Some Organization',
        },
        statusType: 'typeNotExists',
        objToUpdate: {type: 'spf', val: 'some val'},
        shouldDetachMitigatedConnection: false,

    };
    let objWithWrongObjToUpdate = {
        uid: 'efrdt14vs34csmxapw',
        userEmail: "tzach@gmail.com",
        curOrgObj: {
            orgId: '123d3214123da92b1e9suj',
            orgName: 'Some Organization',
        },
        statusType: 'typeNotExists',
        objToUpdate: {noType: '', val: 'some val'},
        shouldDetachMitigatedConnection: false,

    };

    it('Should return true - obj has all properties', function () {
        const result = helpers.checkFindingStatusObj(obj);
        return expect(result).to.equal(true);
    });

    it('Should return false - nothing sent', function () {
        const result = helpers.checkFindingStatusObj();
        return expect(result).to.equal(false);
    });

    it('Should return false - obj With Wrong StatusType sent', function () {
        const result = helpers.checkFindingStatusObj(objWithWrongStatusType);
        return expect(result).to.equal(false);
    });

    it('Should return false - obj With Wrong ObjToUpdate sent', function () {
        const result = helpers.checkFindingStatusObj(objWithWrongObjToUpdate);
        return expect(result).to.equal(false);
    });

});


describe('getUserInputAndMitigatedQueryByType() - return query string.', function () {

let finalQuery = 'MATCH (u:assetUserInput)-[r:assetUserInput]->(t:AMip) WHERE u.oid="1234"' +
    ' AND t.nodeType="AMip" return t.ip as ip, u.type as type, r.userEmail as userEmail, ' +
    'r.importance as importance, r.comments as comments UNION MATCH (m:mitigated)-[r:mitigated]->(t:AMip)' +
    ' WHERE m.oid="1234" AND t.nodeType="AMip" return t.ip as ip, m.type as type, r.userEmail as userEmail,' +
    ' null as importance, null as comments ';

    it('Should return  final query', function () {
        const result = helpers.getUserInputAndMitigatedQueryByType('ip', '1234');
        return expect(result).to.equal(finalQuery);
    });

    it('Should return empty str - nothing sent', function () {
        const result = helpers.getUserInputAndMitigatedQueryByType();
        return expect(result).to.equal('');
    });

    it('Should return empty str - Wrong type sent', function () {
        const result = helpers.getUserInputAndMitigatedQueryByType('noType', '1234');
        return expect(result).to.equal('');
    });
});


describe('buildObjArrFromSpecificProps() - return array of objects with only the props ' +
    'that we send to the func and add the type of the object as prop too.', function () {

let type = 'ip';
let propArr = ['ip', 'port'];
let data = [{ip:'12.12.12.12', port: 9090, hostname:'somename'},{ip:'12.13.13.12', port: 8080, hostname:'somename'}];

    it('Should return array - obj has propArr properties', function () {
        const result = helpers.buildObjArrFromSpecificProps(propArr, data, type);
        expect(result[0].hostname).to.equal(undefined);
        expect(result[1].hostname).to.equal(undefined);
        expect(result[1].type).to.equal('ip');
        return expect(result.length).to.equal(2);
    });

    it('Should return empty array - no type was sent', function () {
        const result = helpers.buildObjArrFromSpecificProps(propArr, data);
        return expect(result.length).to.equal(0);
    });

    it('Should return empty array - nothing was sent', function () {
        const result = helpers.buildObjArrFromSpecificProps();
        return expect(result.length).to.equal(0);
    });
});


describe('createRelDataObj() - gets props to create object for neo4j node and relationship ' +
    'that we send to the func and add the type of the object as prop too.', function () {


let data = {
    "shouldDetachMitigated": false,
    "curData": {
        "spf": "No Records Found",
        "hostname": "newsfast.co.uk",
        "type": "spf"
    },
    "nodeType": "spf",
    "statusType": "mitigated",
    "orgId": "5d35bf8644ae7049792555f2",
    "objToUpdate": {
        "type": "spf",
        "val": "No Records Found",
        "extraVal": "newsfast.co.uk",
        "AMNodePropsObject": {
            "spf": "No Records Found",
            "hostname": "newsfast.co.uk"
        },
        "importance": 3
    },
    "updateTime": "2019-10-22T18:17:07+03:00",
    "uid": "5c6935e09ee0db203a64db46",
    "userEmail": "admin@admin.com",
    "isAssetUserInputUpdate": true
};

let resData = {
    "fromNodeLabelName": "mitigated",
    "fromProp": "oid",
    "fromPropVal": "5d35bf8644ae7049792555f2",
    "targetNode": "AMspf",
    "relationshipLabel": "mitigated",
    "relationshipData": "updateTime:\"2019-10-22T18:19:17+03:00\", uid:\"5c6935e09ee0db203a64db46\", userEmail:\"admin@admin.com\" , importance:3 ",
    "targetMatchProps": "spf: \"No Records Found\", hostname: \"newsfast.co.uk\", nodeType: \"AMspf\" "
};

    it('Should return object with currect props and values', function () {
        const result = helpers.createRelDataObj(data, 'spf');
         expect(result.fromProp).to.equal("oid");
         expect(result.fromPropVal).to.equal("5d35bf8644ae7049792555f2");
         expect(result.targetNode).to.equal("AMspf");
         expect(result.relationshipLabel).to.equal("mitigated");
        return expect(result.targetMatchProps).to.equal("spf: \"No Records Found\", hostname: \"newsfast.co.uk\", nodeType: \"AMspf\" ");
    });

    it('Should return empty object - no data was sent', function () {
        const result = helpers.createRelDataObj();
        return expect(Object.getOwnPropertyNames(result).length).to.equal(0);
    });

    it('Should return empty object - no type was sent', function () {
        const result = helpers.createRelDataObj(data);
        return expect(Object.getOwnPropertyNames(result).length).to.equal(0);
    });
});

describe('countSpfOrDmarc() - return object with count(num) of not Configured spf or dmarc and if has a critical asset(bool).', function () {

    let obj = {
        uid: 'efrdt14vs34csmxapw',
        userEmail: "tzach@gmail.com",
        curOrgObj: {
            orgId: '123d3214123da92b1e9suj',
            orgName: 'Some Organization',
        },
        statusType: 'fp',
        objToUpdate: {type: 'spf', val: 'some val'},
        shouldDetachMitigatedConnection: false,

    };


    it('Should return 1 ', function () {
        const result = helpers.countSpfOrDmarc([{hostname: 'dsad.com' ,spf:'No Records Found'}], 'spf');
        expect(result.notConfiguredCount).to.be.a('number');
        return expect(result.notConfiguredCount).to.equal(1);
    });

    it('Should return 0 - sent type not matched to the property in the obj', function () {
        const result = helpers.countSpfOrDmarc([{hostname: 'dsad.com' , spf:'No Records Found'}], 'dmarc');
        expect(result.notConfiguredCount).to.be.a('number');
        return expect(result.notConfiguredCount).to.equal(0);
    });

    it('Should return 0 - sent not valid type', function () {
        const result = helpers.countSpfOrDmarc([{hostname: 'dsad.com' , spf:'No Records Found'}], 'noSuchType');
        expect(result.notConfiguredCount).to.be.a('number');
        return expect(result.notConfiguredCount).to.equal(0);
    });

    it('Should return 0 - sent object instead of array', function () {
        const result = helpers.countSpfOrDmarc({hostname: 'dsad.com' , spf:'No Records Found'}, 'spf');
        expect(result.notConfiguredCount).to.be.a('number');
        return expect(result.notConfiguredCount).to.equal(0);
    });

    it('Should return 0 - sent number instead of array', function () {
        const result = helpers.countSpfOrDmarc(6, 'spf');
        expect(result.notConfiguredCount).to.be.a('number');
        return expect(result.notConfiguredCount).to.equal(0);
    });

});

