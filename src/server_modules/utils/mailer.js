'use strict';
const sg = require('sendgrid')('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');
const helper = require('sendgrid').mail;

module.exports = function mailer() {
    const seneca = this;
    const fromAddress = 'no-reply@rescana.com';

    seneca.ready((error) => {
        if (error) {
            return console.log(error);
        }
    });

    seneca.add('role:mailer, cmd:send', (msg, respond) => {
        const from_email = new helper.Email(fromAddress);
        const to_email = new helper.Email(msg.to);
        const subject = msg.subject;
        const content = new helper.Content('text/html', msg.text);
        const mail = new helper.Mail(from_email, subject, to_email, content);

        const request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
        });

        sg.API(request, (error, response) => {
            if (error) {
                console.log('Mailer Error: ', error );
            }
            console.log(response.statusCode);
            console.log(response.body);
            console.log(response.headers);
        });

        respond();
    });
};
