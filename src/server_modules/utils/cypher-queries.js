'use strict';
const TYPE_CVE = 'cve';
const TYPE_PORTS = 'ports';
const TYPE_ASN = 'asn';
const TYPE_ISP = 'isp';
const TYPE_PRODUCT = 'product';
const TYPE_BUCKET = 'bucket';
const TYPE_SPF = 'spf';
const TYPE_DMARC = 'dmarc';

let createScanIdStr = (startDate, endDate, scanNode, scans) => {
    let scanString = '';
    if (!(startDate || endDate) && scans && scans.length > 0 && scanNode) {
        for (let j = 0; j < scans.length; j++) {
            scanString += ' (';

            scanString +=  scanNode + '.scanId="' + scans[j].scanId + '" ';

            if(j !== scans.length-1){
                scanString += ' ) OR ';
            }else if (j === scans.length-1){
                scanString += ' )';
            }
        }
        return scanString;
    }
};

function createStringArr(arr) {
    let stringArr = '';
    if (arr && Array.isArray(arr) && arr.length > 0) {
        for (let i = 0; i < arr.length; i++) {
            let val = arr[i];

            if(arr[i].domain || arr[i].hostname){
                val = arr[i].domain || arr[i].hostname;
            }

            if (i === arr.length - 1) {
                stringArr += '"' + val + '"';
            } else if (i === 0 && arr.length > 1) {
                stringArr += '"' + val + '",';
            } else {
                stringArr += '"' + val + '",';
            }
        }
    }

    return stringArr;
}

const escapeHtml = (text) => {
    if (text) {
        let specials = ['"', '\\'], regex = RegExp('[' + specials.join('\\') + ']', 'g');
        return text.replace(regex, '\\$&');
    } else {
        return text;
    }
};

function objectToString(props) {
    if (typeof props !== 'object') {
        return '';
    }
    let propsStr = '{';
    let firstItem = true;
    for (let key in props) {
        if (props.hasOwnProperty(key)) {
            let value = props[key];
            if (key && value) {
                value = escapeHtml(value.toString());
                key = key.split(' ').join('_');
                if (firstItem) {
                    firstItem = false;
                    propsStr += key + ':"' + value + '"';
                } else {
                    propsStr += ' , ' + key + ' : "' + value + '"';
                }
            }
        }
    }
    propsStr += '}';
    return propsStr;
}
/* This function handles the query making of report related queries */
function queryReturnStatementMakerForReport(query, type) {
    let ret = '';

    if (type && type === TYPE_CVE) {
        ret = ' RETURN n.hostname as hostname, g.hostname as subdomain, IP.address as ip, i.ports as port, f.cpe as cpe, o.cve as cve, o.score as cveScore, o.summary as summary LIMIT 4000 ';
    } else if (type && type === TYPE_PORTS) {
        ret = ' RETURN n.hostname as hostname, g.hostname as subdomain, IP.address as ip, i.ports as port LIMIT 4000 ';
    } else if (type && type === TYPE_ASN) {
        // For report - subdomain and IP nodes are not essentials and causes a lot of duplicates in ASN findings.
        ret = ' RETURN n.hostname as hostname, i.finding as asn, e.name as name, e.finding as finding LIMIT 4000 ';
    } else if (type && type === TYPE_BUCKET) {
        ret = ' RETURN n.hostname as hostname, g.hostname as subdomain, bucket.bucket as bucket, bucket_status.bucket_status as bucket_status LIMIT 4000 ';
    } else if (type && type === TYPE_SPF) {
        ret = ' RETURN n.hostname as hostname, j.spf as spf LIMIT 4000 ';
    } else if (type && type === TYPE_DMARC) {
        ret = ' RETURN n.hostname as hostname, j.dmarc as dmarc LIMIT 4000 ';
    } else if (type && type === TYPE_ISP) {
        ret = ' RETURN n.hostname as hostname, g.hostname as subdomain, IP.address as ip, isp.finding as isp LIMIT 4000 ';
    } else if (type && type === TYPE_PRODUCT) {
        ret = ' RETURN n.hostname as hostname, g.hostname as subdomain, IP.address as ip, i.ports as port, f.finding as product LIMIT 4000 ';
    } else {
        /* default case that should not be reached (used for regression) */
        ret = ' RETURN nodes(path), relationships(path) LIMIT 4000 ';
    }

    return ret;
}

module.exports.createNodeCommand = (label, type, props) => {
    if (props && typeof props === 'object' && props.length !== 0) {
        const propsStr = objectToString(props);
        return 'MERGE (n:`' + label + '`:' + type + ' ' + propsStr + ') ' + 'RETURN n';
    } else {
        return 'MERGE (n:`' + label + '`:' + type + ') ' + 'RETURN n';
    }
};

module.exports.detachNodeCommand = (data) => {

    //Detach single node from arr of nodes(no_domain case) (detach array of ips from no_domain node by specific company
    if (data.arrPropStr && Array.isArray(data.arrPropStr) && data.propStr && data.propStr === 'no_domain') {
        if (data.label && data.relationship && data.toLabel && data.prop && data.propStr && data.secondProp && data.thirdProp) {
            return 'MATCH (n:' + data.label + ')-[r:' + data.relationship + ']->(m:' + data.toLabel +
                ') WHERE n.' + data.prop + '=' + '"' + data.propStr + '"  AND n.companyName = "' + data.thirdProp + '"' + 'AND m.' + data.secondProp + ' IN ' + JSON.stringify(data.arrPropStr) + ' DELETE r';
        }
    }

    //Detach arr of nodes from arr of entities node
    if (data.arrPropStr && Array.isArray(data.arrPropStr) && data.arrSecondPropStr && Array.isArray(data.arrSecondPropStr)) {
        if (data.label && data.relationship && data.toLabel && data.prop && data.secondProp) {
            return 'MATCH (n:' + data.label + ')-[r:' + data.relationship + ']->(m:' + data.toLabel +
                ') WHERE n.' + data.prop + ' IN ' + JSON.stringify(data.arrPropStr) + ' AND m.' + data.secondProp + ' IN ' + JSON.stringify(data.arrSecondPropStr)
                + ' DELETE r';
        }
    }
    //Detach single node from arr of nodes
    if (data.arrPropStr && Array.isArray(data.arrPropStr)) {
        if (data.label && data.relationship && data.toLabel && data.prop && data.propStr && data.secondProp) {
            return 'MATCH (n:' + data.label + ')-[r:' + data.relationship + ']->(m:' + data.toLabel +
                ') WHERE n.' + data.prop + '=' + '"' + data.propStr + '"' + 'AND m.' + data.secondProp + ' IN ' + JSON.stringify(data.arrPropStr) + ' DELETE r';
        }
    }
    //Detach org from all related companies
    if (data.label && data.label === 'organization' && data.prop === 'oid') {
        if (data.relationship &&
            data.prop &&
            data.propStr &&
            typeof data.label === 'string' &&
            typeof data.relationship === 'string' &&
            typeof data.prop === 'string' &&
            typeof data.propStr === 'string') {
            return 'MATCH (n:' + data.label + ')-[r:' + data.relationship + ']->(m) WHERE n.' + data.prop + '=' + '"' + data.propStr + '"' + ' DELETE r';
        }
    }
    //Detach all organizations from specific company
    if (data.label && data.label === 'organization' && data.prop === 'cid') {
        if (data.relationship &&
            data.prop &&
            data.propStr &&
            typeof data.label === 'string' &&
            typeof data.relationship === 'string' &&
            typeof data.prop === 'string' &&
            typeof data.propStr === 'string') {
            return 'MATCH (n:' + data.label + ')-[r:' + data.relationship + ']->(m:COMPANY) WHERE m.' + data.prop + '=' + '"' + data.propStr + '"' + ' DELETE r';
        }
    }
};

module.exports.queryEmailBreachesByDomains = (domains) => {
    console.log('Starting Query');

    let query = 'MATCH (n:domain)-[:email]->(e:email)-[p:appeared_in]->(b:breach)-[:BreachDate]-(d:BreachDate) WHERE e.email IS NOT NULL AND b.title IS NOT NULL AND d.name=\'BreachDate\' AND d.finding IS NOT NULL ';

    query += ' AND n.hostname IN [';

    if (domains) {
        if (!Array.isArray(domains)) {
            domains = [domains];
        }
    } else {
        domains = [];
    }

    domains.map((domain, i) => {
        if (domain) {
            if(domain.domain || domain.hostname){
                domain = domain.domain || domain.hostname;
            }
            query += '\"' + domain + '\"';
            if (i < domains.length - 1) {
                query += ', ';
            }
        }
    });

    query += '] ';

    const optionalQuery = ' OPTIONAL MATCH (b:breach)-[:DataClasses]->(c:DataClasses) WHERE c.finding is not null ';

    const retQuery = ' RETURN DISTINCT n.hostname as domain, e.email as email, ID(e) as id, b.title as breach, ID(b) as breach_nodeID, d.finding as breach_date';

    const optionalRet = ', c.finding as breach_classes ';

    const sortRet = ' ORDER BY d.finding DESC ';

    const fullQuery = query + optionalQuery + retQuery + optionalRet + sortRet;
    console.log('Running query : ', fullQuery);

    return fullQuery;
};

module.exports.queryEmailBreachByNodeID = (nodeID) => {
    if (nodeID && isNumeric(nodeID)) {
        console.log('Starting Query');

        nodeID = Number.parseInt(nodeID);

        const query = 'MATCH (b:breach)-->(d) WHERE ID(b)=' + nodeID + ' AND d.name IN ["Description", "BreachDate", "DataClasses", "IsSpamList"] AND d.finding IS NOT NULL ';

        const retQuery = ' RETURN d.name as name, d.finding as finding ';

        const fullQuery = query + retQuery;
        console.log('Running query : ', fullQuery);
        return fullQuery;
    } else {
        return '';
    }
};

module.exports.queryExtraNodesString = (nodeID, nodeType, startDate, endDate, scans) => {
    console.log('Starting Query');
    const IP_Type = 'IP';
    const ASN_Type = 'ASN';
    const CVE_Type = 'CVE';
    const PORT_Type = 'port';
    const HOSTNAME_Type = 'hostname';
    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let type, secType;
    if (nodeType === IP_Type) {
        type = ':IP';
    } else if (nodeType === ASN_Type) {
        type = ':ASN';
    } else if (nodeType === CVE_Type) {
        type = ':cve';
    } else if (nodeType === PORT_Type) {
        type = ':ports';
    } else if (nodeType === HOSTNAME_Type) {
        type = ':domain';
        secType = ':subdomain';
    } else {
        type = secType = '';
    }

    let query = 'MATCH (scan: scan)-[found:found]->(e)<-[p]-(n' + type + ') WHERE ' + scanString + ' AND ID(n)=' + nodeID + ' AND e.name IS NOT NULL and e.finding IS NOT NULL ';
    const retQuery = ' RETURN DISTINCT e.name as name, e.finding as finding';
    let secQuery, secRet;
    if (nodeType === IP_Type) {
        secQuery = 'MATCH (scan: scan)-[found:found]->(e)<-[p]-(n' + type + ') WHERE ' + scanString + ' AND ID(n)=' + nodeID + ' AND e.name IS NOT NULL and e.tags IS NOT NULL ';
        secRet = ' RETURN DISTINCT e.name as name, e.tags as finding';
    } else if (nodeType === HOSTNAME_Type) {
        secQuery = 'MATCH (scan: scan)-[found:found]->(e)<-[p]-(n' + type + ') WHERE ' + scanString + ' AND ID(n)=' + nodeID + ' AND e.name IS NOT NULL and e.finding IS NOT NULL ';
        secRet = ' RETURN DISTINCT e.name as name, e.finding as finding';
    } else if (nodeType === CVE_Type) {
        secQuery = 'MATCH (scan: scan)-[found:found]->(e)<-[p:impact]-(n' + type + ') WHERE ' + scanString + ' AND ID(n)=' + nodeID + ' AND e.integrity IS NOT NULL AND e.confidentiality IS NOT NULL AND e.availability IS NOT NULL ';
        secRet = ' RETURN DISTINCT \'impact\' as name, \'integrity : \' + e.integrity + \'\\n\\tconfidentiality : \' + e.confidentiality + \'\\n\\tavailability : \' + e.availability as finding';
    } else {
        secQuery = null;
        secRet = null;
    }
    let fullQuery = query + retQuery;

    if(secQuery && secRet){
        fullQuery += ' UNION ' + secQuery + secRet;
    }

    // Add a third query for IP prop called hostnames.
    if (nodeType === IP_Type) {
        secQuery = ' UNION MATCH (scan: scan)-[found:found]->(e)<-[p]-(n' + type + ') WHERE ' + scanString + ' AND ID(n)=' + nodeID + ' AND e.name IS NOT NULL and e.hostnames IS NOT NULL ';
        secRet = ' RETURN DISTINCT e.name as name, e.hostnames as finding';
        fullQuery = fullQuery + secQuery + secRet;
    }
    console.log('Running query : ', fullQuery);
    return fullQuery;
};

module.exports.queryIPsString = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);
    let query = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with n,g,IP,scan ';

    const optionalQuery = 'OPTIONAL MATCH (IP)-[as:ASN]->(asn:ASN)<-[found:found]-(scan) ' +
        'with n,g,IP,asn,scan ' +
        'OPTIONAL MATCH (asn:ASN)-[p2:IP]->(IP2:IP)<-[found:found]-(scan) ' +
        'with n,g,IP,asn,IP2 ' +
        'OPTIONAL MATCH path2 = ((IP:IP)-[as:ASN]->(asn:ASN)-[p2:IP]->(IP2:IP))  ' +
        'with path2,n,g,asn,IP,IP2 ';

    let lastMatchAndReturn =
        optionalQuery +
        'MATCH path = (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP) ' +
        'with path,path2 ' +
        ' RETURN NODES(path)+COALESCE (NODES(path2),[]), RELATIONSHIPS(path)+COALESCE (RELATIONSHIPS(path2),[]) ';

    if (isForReport) {
        let retQuery = ' RETURN n.hostname as hostname, g.hostname as subdomain, IP.address as ip, asn.finding as asn,  IP2.address as asn_ip ';
        lastMatchAndReturn = optionalQuery + retQuery;
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryIPsByDomainsString = (domains) => {
    console.log('Starting Query');

    const stringArr = createStringArr(domains);

    const query = 'MATCH path = ((n:domain)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)) WHERE n.hostname IN [' + stringArr + '] ' ;
    const retQuery = ' RETURN g.hostname as domain, IP.address as address ';

    const fullQuery = query + retQuery;
    console.log('Running query : ', fullQuery);
    return fullQuery;
};

module.exports.querySubdomainsByDomainsString = (domains) => {
    console.log('Starting Query');

    const stringArr = createStringArr(domains);

    const query = 'MATCH (n:domain)-[subdomain:subdomain]->(s:subdomain) WHERE n.hostname IN [' + stringArr + '] ' + ' AND s.hostname <> "null" ';
    const retQuery = ' RETURN n.hostname as domain, s.hostname as subdomain ';

    const fullQuery = query + retQuery;
    console.log('Running query : ', fullQuery);
    return fullQuery;
};

module.exports.queryGeoLocationsByCompanyNameString = (companyName, scans) =>{
    console.log('Starting Query');

    let company = createStringArr([companyName]);
    let scanString = createScanIdStr(null, null, 'scan' , scans);

    let fullQuery = 'MATCH (scan:scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n:domain)<-[owner:owner]-(COMPANY:COMPANY) WHERE COMPANY.companyName= ' + company + ' '  +
        'with n,scan ' +
        'MATCH (scan)-[found:found]->(IP:IP)<-[p:IP]-(g)<-[:IP|subdomain*0..1]-(n)' +
        'with IP,scan ' +
        'MATCH (scan)-[found:found]->(country:country_name)<-[c:country_name]-(IP) ' +
        'with IP,country ' +
        'RETURN count(DISTINCT IP.address) as Assets, country.finding as Country ';

    console.log('Running query : ', fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.getPortsQueryString = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let lastMatchAndReturn = 'MATCH path = (n)-[:IP|:subdomain*0..1]->(g)-[p:IP]->(IP:IP)-[port:ports]->(i:ports) ' +
        'return nodes(path), relationships(path)';

    let query = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n:domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with n,g,IP,scan ' +
        'MATCH (IP)-[port:ports]->(i:ports)<-[found:found]-(scan) ' +
        'with n,g,IP,i,scan ';

    if (isForReport) {
        lastMatchAndReturn = queryReturnStatementMakerForReport(query, TYPE_PORTS);
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryProductQueryString = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let lastMatchAndReturn = 'MATCH path = (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)-[port:ports]->(i:ports)-[t:product]->(f:product)\n' +
        'return nodes(path), relationships(path)';

    let query = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with scan, n ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with n,g,IP,scan ' +
        'MATCH (IP)-[port:ports]->(i:ports)<-[found:found]-(scan) ' +
        'with n,g,IP,i,scan ' +
        'MATCH (i)-[t:product]->(f:product)<-[found:found]-(scan) ' +
        'with n,g,IP,i,f,scan ';

    if (isForReport) {
        lastMatchAndReturn = queryReturnStatementMakerForReport(query, TYPE_PRODUCT);
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryVPNQueryString = (domains, startDate, endDate, scans, filteredCompaniesNames) => {

    console.log('Starting Query');

    let edges = ['t', 'p', 'port'];
    let scanString = createScanIdStr(startDate, endDate, edges, scans);

    const stringArr = createStringArr(domains);
    const companiesNames = createStringArr(filteredCompaniesNames);

    const query = 'MATCH path = ((n:domain)-[*0..2]->(g)-[p:IP]->(IP:IP)-[port:ports]->(i:ports)-[t:vpn]->(f:vpn)) WHERE ' + scanString + ' (n.hostname="no_domain" AND n.companyName IN [' + companiesNames + ']) OR n.hostname IN [' + stringArr + '] AND (g:resolutions OR g:subdomain) AND g.hostname <> "null" ';
    const secondaryQuery = 'MATCH path = ((n:domain)-[p:IP]->(IP:IP)-[port:ports]->(i:ports)-[t:vpn]->(f:vpn)) WHERE ' + scanString + ' n.hostname IN [' + stringArr + '] ';
    let fullQuery = queryMaker(query, startDate, endDate, secondaryQuery, null, null, edges);

    console.log('Running query : ', fullQuery);
    return fullQuery;
};

module.exports.queryASNString = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let query = '';
    const ASN_EXTRA_INFO_NAMES = '"description", "registrar", "range", "countryCode"';

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let lastMatchAndReturn = 'MATCH path = (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)-[ASN:ASN]->(i:ASN)-[k]->(e) WHERE e.name IN [\'' + ASN_EXTRA_INFO_NAMES + '\'] ' +
        'return nodes(path), relationships(path)';

    query += 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with scan, n ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with n,g,IP,scan ' +
        'MATCH (IP)-[ASN:ASN]->(i:ASN)<-[found:found]-(scan) ' +
        'with n,g,IP,i,scan ';

    if (isForReport){
        query += 'MATCH (i)-[k]->(e)<-[found:found]-(scan) \n' +
            'with n,g,IP,i,e,scan \n';
    }

    if (isForReport) {
        lastMatchAndReturn = queryReturnStatementMakerForReport(query, TYPE_ASN);
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCVEString = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let lastMatchAndReturn = 'MATCH path = (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)-[port:ports]->(i:ports)-[t:cpe]->(f:cpe)-[k:cve]->(o:cve) ' +
        'return nodes(path), relationships(path)';

    let query = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with n,g,IP,scan ' +
        'MATCH (IP)-[port:ports]->(i:ports)<-[found:found]-(scan) ' +
        'with n,g,IP,i,scan ' +
        'MATCH (i)-[t:cpe]->(f:cpe)<-[found:found]-(scan) ' +
        'with n,g,IP,i,f,scan ' +
        'MATCH (f)-[k:cve]->(o:cve)<-[found:found]-(scan) ' +
        'with n,g,IP,i,f,o, scan ';

    if (isForReport) {
        lastMatchAndReturn = queryReturnStatementMakerForReport(query, TYPE_CVE);
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCypherGetMaxCVSScore = (domains, startDate, endDate, scans) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let fullQuery = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with g,IP,scan ' +
        'MATCH (IP)-[port:ports]->(i:ports)<-[found:found]-(scan) ' +
        'with g,IP,i,scan ' +
        'MATCH (i)-[t:cpe]->(f:cpe)<-[found:found]-(scan) ' +
        'with g,IP,i,f,scan ' +
        'MATCH (f)-[k:cve]->(o:cve)<-[found:found]-(scan) ' +
        'with g,IP,i,f,o, scan ' +
        'MATCH (g)-[p:IP]->(IP:IP)-[port:ports]->(i:ports)-[t:cpe]->(f:cpe)-[k:cve]->(o:cve) ' +
        'RETURN MAX(toFloat(o.score)) AS iScore, g.hostname AS hostname';

    console.log(fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCypherGetNotConfiguredSPFsCount = (domains, startDate, endDate, scans) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let fullQuery = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[s:spf]->(j:spf)<-[found:found]-(scan) WHERE j.spf=\'No Records Found\'' +
        'with n,j,scan ' +
        'MATCH path = (n)-[s:spf]->(j:spf) ' +
        'RETURN count(DISTINCT j) as notConfigSPFs ';

    console.log('Running query : ', fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCypherGetNotConfiguredDMARCsCount = (domains, startDate, endDate, scans) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let fullQuery = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[s:dmarc]->(j:dmarc)<-[found:found]-(scan) WHERE j.dmarc=\'No Records Found\'' +
        'with n,j,scan ' +
        'MATCH path = (n)-[s:dmarc]->(j:dmarc) ' +
        'RETURN count(DISTINCT j) as notConfigDMARCs ';

    console.log('Running query : ', fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCypherGetEmailBreachesCount = (domains) => {
    console.log('Starting Query');

    const stringArr = createStringArr(domains);

    const query = 'MATCH (n:domain)-[:email]->(e:email)-[p:appeared_in]->(b:breach)-[t:BreachDate]-(d:BreachDate) WHERE e.email IS NOT NULL AND b.title IS NOT NULL AND d.name=\'BreachDate\' AND d.finding IS NOT NULL  AND n.hostname IN [' + stringArr + '] ';

    const ret = ' RETURN LEFT(d.finding, 4) as breach_year,ID(e) as id, e.email as email ORDER BY LEFT(d.finding, 4) DESC ';

    // Using Union instead of DISTINCT (secondaryQuery and secRet are equals to first query and ret).
    // Not using queryMaker here since scanTime is irrelevant when it comes to breached emails.
    const fullQuery = query + ret + ' UNION ' + query + ret;

    console.log('Running query : ', fullQuery);
    return fullQuery;
};

module.exports.queryCypherGetTotalEmailsByDomainsCount = (domains) => {
    console.log('Starting Query');

    const stringArr = createStringArr(domains);

    const query = 'MATCH (n:domain)-[t:email]->(e:email) WHERE e.email IS NOT NULL AND n.hostname IN [' + stringArr + '] ';

    const ret = ' RETURN e.email as email ';

    // Using Union instead of DISTINCT (secondaryQuery and secRet are equals to first query and ret).
    // Not using queryMaker here since scanTime is irrelevant when it comes to breached emails.
    const fullQuery = query + ret + ' UNION ' + query + ret;

    console.log('Running query : ', fullQuery);
    return fullQuery;
};

module.exports.queryCypherGetCompanyExtraDataByID = (companyID) => {
    console.log('Starting Query');

    const query = 'MATCH (n:COMPANY)-[k:responsible|:contact|:information|:classification|:description|:sector|:date_of_contact|:date_of_survey]->(e) ';

    const filter = ' WHERE n.cid="' + companyID + '" ';

    const ret = ' RETURN PROPERTIES(e) as data, TYPE(k) as type, n.cid as companyID, ID(e) as nid ';

    // Not using queryMaker here since scanTime is irrelevant when it comes to company's extra data.
    const fullQuery = query + filter + ret;

    console.log('Running query GetCompanyExtraDataByID : ', fullQuery);
    return fullQuery;
};

module.exports.queryCypherUpdateCompanyInfoStatusByID = (companyID, nodeID, checked) => {
    console.log('Starting Query');

    const query = 'MATCH (n:COMPANY)-[k:information]->(e:INFORMATION) ';

    const filter = ' WHERE n.cid="' + companyID + '" AND ID(e)=' + nodeID + ' ';

    let update = ' SET e.checked=' + checked + ' ';

    const ret = ' RETURN COUNT(*) as updatedNodes ';

    // Not using queryMaker here since scanTime is irrelevant when it comes to company's extra data.
    const fullQuery = query + filter + update + ret;

    console.log('Running query UpdateCompanyInfoStatusByID : ', fullQuery);
    return fullQuery;
};

module.exports.queryCypherGetIspData = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let lastMatchAndReturn = 'MATCH path = (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)-[i:isp]->(isp:isp) ' +
        'return nodes(path), relationships(path)';

    let query = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH(scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with n,g,IP,scan ' +
        'MATCH (IP)-[i:isp]->(isp:isp)<-[found:found]-(scan) ' +
        'with n,g,IP,isp,scan ';

    if (isForReport) {
        lastMatchAndReturn = queryReturnStatementMakerForReport(query, TYPE_ISP);
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);

    if(!scanString){
        fullQuery = '';
    }

    return fullQuery;
};

module.exports.queryCypherGetBucketsData = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let lastMatchAndReturn = 'MATCH path = (n)-[:bucket|:subdomain*0..1]->(g)-[p:bucket]->(bucket:bucket)-[s:bucket_status]->(bucket_status:bucket_status) ' +
        'return nodes(path), relationships(path)';

    let query = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH(scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:bucket|:subdomain*0..1]->(g)<-[found:found]-(scan) ' +
        'with n,g,scan ' +
        'MATCH (g)-[p:bucket]->(bucket:bucket)-[s:bucket_status]->(bucket_status:bucket_status) ' +
        'with n,g,bucket,bucket_status,scan ';

    if (isForReport) {
        lastMatchAndReturn = queryReturnStatementMakerForReport(query, TYPE_BUCKET);
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;

};

module.exports.queryCypherGetSpf = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let lastMatchAndReturn = 'MATCH path = (n)-[s:spf]->(j:spf) ' +
        'return nodes(path), relationships(path)';

    let query = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[s:spf]->(j:spf)<-[found:found]-(scan) ' +
        'with n,j,scan ';

    if (isForReport) {
        lastMatchAndReturn = queryReturnStatementMakerForReport(query, TYPE_SPF);
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);

    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCypherGetDmarc = (domains, startDate, endDate, scans, isForReport) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let lastMatchAndReturn = 'MATCH path = (n)-[s:dmarc]->(j:dmarc) ' +
        'return nodes(path), relationships(path)';

    let query = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[s:dmarc]->(j:dmarc)<-[found:found]-(scan) ' +
        'with n,j,scan ';

    if (isForReport) {
        lastMatchAndReturn = queryReturnStatementMakerForReport(query, TYPE_DMARC);
    }

    let fullQuery = query + lastMatchAndReturn;

    const logString = (isForReport) ? 'Running query for report : ' : 'Running query : ';
    console.log(logString, fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCypherGetCVESeverity = (domains, startDate, endDate, scans) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let fullQuery = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with g,IP,scan ' +
        'MATCH (IP)-[port:ports]->(i:ports)<-[found:found]-(scan) ' +
        'with g,IP,i,scan ' +
        'MATCH (i)-[t:cpe]->(f:cpe)<-[found:found]-(scan) ' +
        'with g,IP,i,f,scan ' +
        'MATCH (f)-[k:cve]->(o:cve)<-[found:found]-(scan) ' +
        'with g,IP,i,f,o, scan ' +
        'MATCH (g)-[p:IP]->(IP:IP)-[port:ports]->(i:ports)-[t:cpe]->(f:cpe)-[k:cve]->(o:cve) ' +
        'RETURN o AS CVE, o.score AS score';

    console.log(fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCypherGetLocations = (startDate, endDate, scans) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let fullQuery = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'with scan ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan)  ' +
        'with n,g,IP,scan ' +
        'MATCH (IP)-[y:longitude|latitude]->(l)<-[found:found]-(scan) ' +
        'with n,g,IP,l, scan ' +
        'RETURN n.hostname AS hostname, g.hostname AS subdomain, IP.address AS ip,l.name AS dis ,l.finding AS value LIMIT 200';

    console.log('Running query : ', fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.queryCypherGetCVECountPerHostname = (domains, startDate, endDate, scans) => {
    console.log('Starting Query');

    let scanString = createScanIdStr(startDate, endDate, 'scan' , scans);

    let fullQuery = 'MATCH (scan: scan) WHERE ' + scanString + ' ' +
        'MATCH (scan)-[found:found]->(n: domain) ' +
        'with n, scan ' +
        'MATCH (n)-[:IP|subdomain*0..1]->(g)-[p:IP]->(IP:IP)<-[found:found]-(scan) ' +
        'with g,IP,scan ' +
        'MATCH (IP)-[port:ports]->(i:ports)<-[found:found]-(scan) ' +
        'with g,IP,i,scan ' +
        'MATCH (i)-[t:cpe]->(f:cpe)<-[found:found]-(scan) ' +
        'with g,IP,i,f,scan ' +
        'MATCH (f)-[k:cve]->(o:cve)<-[found:found]-(scan) ' +
        'with g,IP,i,f,o, scan ' +
        'MATCH (g)-[p:IP]->(IP:IP)-[port:ports]->(i:ports)-[t:cpe]->(f:cpe)-[k:cve]->(o:cve) ' +
        'RETURN g AS hostname, count(o) AS CVECount ';

    console.log('Running query : ', fullQuery);
    if(!scanString){
        fullQuery = '';
    }
    return fullQuery;
};

module.exports.getQuery = (objToUpdate) => {
    let query = '';
    if(objToUpdate && objToUpdate.type && typeof objToUpdate.type === 'string'){
        if(objToUpdate.type === 'ip'){
            query = 'MATCH (i:IP) WHERE i.address="' + objToUpdate.val + '" ' +
                'OPTIONAL MATCH (i:IP)-[r:ports]->(p:ports) WHERE i.address="' + objToUpdate.val + '" ' +
                'OPTIONAL MATCH (i:IP)-[r:ports]->(p:ports)-[e:cpe]->(c:cpe) WHERE i.address="' + objToUpdate.val + '" ' +
                'OPTIONAL MATCH (i:IP)-[r:ports]->(p:ports)-[e:cpe]->(c:cpe)-[d:cve]->(cve:cve) WHERE i.address="' + objToUpdate.val + '" ' +
                'OPTIONAL MATCH (i:IP)-[isp:isp]->(t:isp) WHERE i.address="' + objToUpdate.val + '" ' +
                'RETURN DISTINCT i.address as address, t.finding as isp, p.ports as port, p.IP as ip, c.cpe as cpe, cve.cve as cve ORDER BY cve.cve, c.cpe';

        } else if (objToUpdate.type === 'port') {
            query = 'MATCH (p:ports) WHERE p.ports="' + objToUpdate.val + '" AND p.IP="' + objToUpdate.extraVal + '" ' +
                'OPTIONAL MATCH (p:ports)-[e:cpe]->(c:cpe) WHERE p.ports="' + objToUpdate.val + '" AND p.IP="' + objToUpdate.extraVal + '" ' +
                'OPTIONAL MATCH (p:ports)-[e:cpe]->(c:cpe)-[d:cve]->(cve:cve) WHERE p.ports="' + objToUpdate.val + '" AND p.IP="' + objToUpdate.extraVal + '" ' +
                'RETURN DISTINCT p.ports as port, p.IP as ip, c.cpe as cpe, cve.cve as cve ORDER BY cve.cve, c.cpe';

        } else if (objToUpdate.type === 'cpe') {
            query = 'MATCH (c:cpe) WHERE c.cpe="' + objToUpdate.val + '" ' +
                'OPTIONAL MATCH (c:cpe)-[d:cve]->(cve:cve) WHERE c.cpe="' + objToUpdate.val + '" ' +
                'RETURN DISTINCT c.cpe as cpe, cve.cve as cve ORDER BY cve.cve, c.cpe';
        }
    }

    return query;
};

module.exports.detachFpAndMitigatedNodesCommand = (data) => {
    let query = '';

    //The counter is used so we know if the node is connected to more then 1 other node.
    //This is for preventing detachment of a node from multipule nodes.
    if (data.fromNodeLabelName && data.relationshipLabel && data.targetNode && data.fromProp && data.fromPropVal && data.whereStatement) {
        query = 'MATCH (f:' + data.fromNodeLabelName + ')-[r:' + data.relationshipLabel + ']->(t:' + data.targetNode + ') ' +
            'WHERE f.' + data.fromProp + '="' + data.fromPropVal + '" AND r.counter = 1 ' + data.whereStatement +
            ' DELETE r RETURN f ' +
            'UNION ' +
            'MATCH (f:' + data.fromNodeLabelName + ')-[r:' + data.relationshipLabel + ']->(t:' + data.targetNode + ') ' +
            'WHERE f.' + data.fromProp + '="' + data.fromPropVal + '" ' + data.whereStatement +
            ' SET (CASE WHEN r.counter > 1 THEN r END ).counter = r.counter - 1 RETURN f';
    }
    console.log('In addAMAndMitigatedNodesCommand - final query: ' + query);
    return query;
};

module.exports.addAMAndMitigatedNodesCommand = (data, statusType) => {
    let query = '';
    if (data.fromNodeLabelName && data.relationshipLabel && data.fromProp && data.fromPropVal && data.targetNode && data.targetMatchProps && data.relationshipData) {
        if (statusType && statusType === 'assetUserInput') {
            query = 'MATCH (from:' + data.fromNodeLabelName + ' {' + data.fromProp + ':"' + data.fromPropVal + '"})' +
                ' MERGE (target:' + data.targetNode + ' {' + data.targetMatchProps + '})' +
                ' MERGE (from)-[r:' + data.relationshipLabel + ']->(target)' +
                ' ON CREATE SET r += {' + data.relationshipData + '}' +
                ' ON MATCH SET r += {' + data.relationshipData + '}';
        } else {
            query = 'MATCH (from:' + data.fromNodeLabelName + ' {' + data.fromProp + ':"' + data.fromPropVal + '"})' +
                ' MERGE (target:' + data.targetNode + ' {' + data.targetMatchProps + '})' +
                ' MERGE (from)-[r:' + data.relationshipLabel + ']->(target)' +
                ' ON CREATE SET r += {' + data.relationshipData + ', counter:1}' +
                ' ON MATCH SET r += {' + data.relationshipData + ', counter: r.counter + 1}';
        }
    }
    console.log('In addAMAndMitigatedNodesCommand - getIntelAndSurveyScoresByCVSScore final query: ' + query);
    return query;
};
