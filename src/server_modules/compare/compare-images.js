'use strict';
const resemble = require('node-resemble-js');
const Promise = require('bluebird');
const chokidar = require('chokidar');
const fs = require('fs');
const path = require('path');

const spath = path.join(__dirname, '..', '..', '..', 'scr');

module.exports = function compareScreenShots() {
    const act = Promise.promisify(this.act, {context: this});

    this.add('role:compareimages, compare:images', (msg, respond) => {
        console.log('directory is: ', spath);

        // Check if file exists(if it doesn't you'll get an error).
        // If it exists, start comparing.
        try {
            if (!msg.publicApp.screenshot || !msg.customerApp.screenshot) {
                console.log('Public or customer app do not have screenshots');
                respond();
            } else {
                console.log('comparing ', msg.publicApp.screenshot, 'with ', msg.customerApp.screenshot);
                compareImages(msg.publicApp, spath + msg.publicApp.screenshot, spath + msg.customerApp.screenshot, respond);
            }
        } catch (e) {
            console.log(e);
            respond();
        }
    });

    function compareImages(publicApp, publicScreenShotPath, customerScreenShotPath, respond) {
        console.log('in compareImages, comparing :', publicApp.url);
        console.log('File path for customerscreenshot is: ', customerScreenShotPath);
        console.log('File path for publicScreenShotPath is: ', publicScreenShotPath);
        try {
            fs.stat(customerScreenShotPath, (err, data) => {
                if (err) {
                    console.log(customerScreenShotPath, ' does not exist');
                    respond();
                } else {
                    fs.stat(publicScreenShotPath, (err, data) => {
                        if (err) {
                            console.log(publicScreenShotPath, ' does not exist');
                            respond();
                        } else {
                            resemble(customerScreenShotPath).compareTo(publicScreenShotPath).ignoreColors().onComplete((data, err) => {
                                if (err) {
                                    console.log(err);
                                    respond(null);
                                }
                                console.log('in resemble');
                                if (data) {
                                    publicApp.screenShotCompareScore = 100 - data.misMatchPercentage;
                                    act('role:publicstore, update:app', {data: publicApp}).then(() => {
                                        console.log('updating app');
                                        respond(null, {screenShotCompareScore: 100 - data.misMatchPercentage});
                                    });
                                    if (global.gc) {
                                        global.gc();
                                    } else {
                                        console.log('Garbage collection unavailable.  Pass --expose-gc '
                                            + 'when launching node to enable forced garbage collection.');
                                    }
                                    respond(null);
                                    /*
                                     {
                                     misMatchPercentage : 100, // %
                                     isSameDimensions: true, // or false
                                     dimensionDifference: { width: 0, height: -1 }, // defined if dimensions are not the same
                                     getImageDataUrl: function(){}
                                     }
                                     */
                                } else {
                                    console.log('Coulden\'t find data? WTF?');
                                    respond(null);
                                }
                            });
                        }
                    });
                }
            });
        } catch (e) {
            console.log(e);
            respond(null);
        }
    }
};

