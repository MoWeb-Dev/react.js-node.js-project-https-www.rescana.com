'use strict';
const compareStrings = require('./compare-strings');
const Promise = require('bluebird');

module.exports = function CompareApps() {
    const seneca = this;
    const act = Promise.promisify(seneca.act, {context: seneca});

    // curl -d '{"role" : "compareApps", "compare" : "app", "customerAppId" : "xxxxx", "publicAppId" : "xxxxx"}' http://localhost:12222/act
    // curl -d '{"role" : "compareApps", "compare" : "app", "customerAppId" : "xxxxx", "publicAppURL" : "http://xxxxx.com"}' http://localhost:12222/act
    seneca.add('role:compareApps,compareNow:app', compareNow);


    async function compareNow(msg, respond) {
        console.log('in compareNow');
        await collectAppData(msg, respond);
    }

    async function collectAppData(msg, respond) {
        let promArr = [];
        // Compare can receive
        try {
            if (!msg.publicAppId && msg.publicAppURL) {
                console.log('Getting data from store using app URL');

                let appArr = await getAppDatabyURL(msg.customerAppId, msg.publicAppURL);
                await startCompareProcess(appArr, respond);
            } else {
                console.log('Getting data from store using app id');
                // Get from DB customerApp and public app by id.
                console.log('customerAppID is:', msg.customerAppId);
                console.log('publicAppId is:', msg.publicAppId);
                promArr = await getAppDatabyID(msg.customerAppId, msg.publicAppId);
                await startCompareProcess(promArr, respond);
            }
        } catch(e){
            console.error("Error in collectAppData: ", e);
        }
    }

    async function startCompareProcess(promArr, respond) {
        console.log('starting compare process');
        let customerApp;
        let publicApp;
        let results = await Promise.all(promArr);

        customerApp = results[0];
        publicApp = results[1];
        console.log('PublicApp:', publicApp);
        try {
            let publicAppDataArr = await addDataToPublicApp(publicApp);
            await Promise.all(publicAppDataArr);
        }catch(e) {
            console.error("Error in addDataToPublicApp: ", e);
            respond();
        }
        try {
            publicApp = await act('role:publicstore, get:app', {id: publicApp.id});
        } catch(e){
            console.error("Error in role:publicstore, get:app : ", e);
            respond();
        }
        // If same registerrer, do not compare.
        filterByWhois(publicApp, customerApp);

        // Compare domains.
        publicApp.domainCompareScore = compareStrings.compareStrings(customerApp.url, publicApp.url);

        // Compare content of specific html attributes and save score.
        publicApp.contentCompareScore = compareContent(publicApp, customerApp);

        try {
            // Compare screenshots of sites.
            await act('role:compareimages, compare:images, fatal$: false', {
                customerApp: customerApp,
                publicApp: publicApp
            });
        } catch(e){
            console.error("Error in role:compareimages, compare:images: ", e);
            respond();
        }
        // Update to DB
        console.log('updating public app data');
        publicApp.lastModified = new Date().getTime();
        customerApp.lastModified = new Date().getTime();

        try {
            await act('role:publicstore, update:app', {data: publicApp});
        } catch(e){
            console.error("Error in role:publicstore, update:app", e);
            respond();
        }
        respond();
    }

    function getMaxScore(scoreArr) {
        let max = 0;
        for (const score in scoreArr) {
            if (scoreArr.hasOwnProperty(score)) {
                if (scoreArr[score] > max) {
                    max = scoreArr[score];
                }
            }
        }
        return max;
    }

    function compareContent(publicApp, customerApp) {
        console.log('Comparing content');
        const elementsToScrape = ['h1', 'h2', 'h3', 'img', 'title', 'li'];
        const score = {};
        if (publicApp.scrapedData && customerApp.scrapedData) {
            for (let k = 0; k < elementsToScrape.length; k++) {
                const pubElToScrape = publicApp.scrapedData[elementsToScrape[k]];
                const cusElToScrape = customerApp.scrapedData[elementsToScrape[k]];

                if (pubElToScrape && cusElToScrape) {
                    for (let i = 0; i < pubElToScrape.length; i++) {
                        for (let j = 0; j < cusElToScrape.length; j++) {
                            let pubElement = pubElToScrape[i],
                                custElement = cusElToScrape[j];

                            if (pubElement && custElement) {
                                const elScore = compareStrings.compareStrings(pubElement.innerHTML, custElement.innerHTML);
                                if (elScore != -1) {
                                    score[elementsToScrape[k]] = elScore;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            console.log('publicApp or customerApp do not have scraping data');
        }
        // TO DO: Check href for images.
        return getMaxScore(score);
    }

    // Get app data by IDs
    function getAppDatabyID(customerAppId, publicAppId) {
        const customerAppP = act('role:customerstore, get:app', {id: customerAppId});
        const publicAppP = act('role:publicstore, get:app', {id: publicAppId});
        console.log('returning promises from getting data by ID');
        return [customerAppP, publicAppP];
    }

    // Get app data by URL
    async function getAppDatabyURL(customerAppId, publicAppURL) {
        try {
            const apps = [];
            let customerApp = await act('role:customerstore, get:app', {id: customerAppId});
            apps.push(customerApp);
            let res = await act('role:publicstore, list:app', {query: {url: publicAppURL}});
            let publicApp = await act('role:publicstore, update:app', {
                data: {
                    id: res[0].id,
                    url: publicAppURL,
                    aid: apps[0].aid
                }
            });
            apps.push(publicApp);
            return apps;
        }catch (e){
            console.error("Error in getAppDatabyURL: ", e);
        }
    }

    function addDataToPublicApp(publicApp) {
        console.log('Adding data to public app', publicApp);
        // TO DO: Need to check that result is always in the same order.
        const whoisPromise = act('role:whois, cmd:sendQuery', {app: publicApp});
        const scrapePromise = act('role:scraper, scrape:page', {app: publicApp});
        const screenShotPromise = act('role:screenshot, capture:app', {app: publicApp});

        return [whoisPromise, scrapePromise, screenShotPromise];
    }

    // check whois registrar, if public app and customer app have the same one, it's not phishing.
    function filterByWhois(publicApp, customerApp) {
        const publicWhois = publicApp.whois;
        if (publicWhois && customerApp.whois) {
            for (const key in publicWhois) {
                if (publicWhois.hasOwnProperty(key)) {
                    if (key == 'Registrar') {
                        if (publicWhois[key] == customerApp.whois['Registrar']) {
                            return {score: 'Not phishing'};
                        }
                    }
                    if (key == 'person') {
                        if (publicWhois[key] == customerApp.whois['person']) {
                            return {score: 'Not phishing'};
                        }
                    }
                }
            }
        } else {
            console.log('No whois data');
        }
    }
};
