'use strict';
require('string_score');
const url = require('url');

/** string_score tester */
module.exports.compareStrings = function(str1, str2) {
/*    console.log("string 1: "+ str1);
    console.log("string 2: "+ str2);*/
    let str1Arr = [];
    let str2Arr = [];
    if (!str1 || !str2) {
        return -1;
    }
    str1 = removeProtocol(str1);
    str2 = removeProtocol(str2);

    const re = /:\/\/(.*)/; // Remove the http://www. from the string
    let cleanAppURL;
    const newStr = re.exec(str2); // Clean the url from "repeated" string like"www // http " using regular expression

    if (newStr) {
        cleanAppURL = newStr[1];
    } else {
        cleanAppURL = str2;
    }
    if (cleanAppURL && cleanAppURL.trim()) {
        const arrayOfStrings2 = cleanAppURL.split('/');
        str2Arr = addSplitedByDotsStr(arrayOfStrings2);
        str2Arr = arrayOfStrings2.concat(str2Arr);

        const arrayOfStrings1 = str1.split('/');
        str1Arr = addSplitedByDotsStr(arrayOfStrings1);
        str1Arr = arrayOfStrings1.concat(str1Arr);

        let maxScore = 0;
        /*        console.log("str1Arr is: ", str1Arr);
        console.log("str2Arr is: ", str2Arr);*/
        for (let k = 0; k < str1Arr.length; k++) {
            for (let o = 0; o < str2Arr.length; o++) {
                const score = str2Arr[o].score(str1Arr[k], 0.9); // The test function and a fuzzy value (0.5 and can be  0.1-0.9)
                if (score > maxScore) {
                    maxScore = score;
                }
            }
        }
        return maxScore;
    } else {
        return -1;
    }
};

// Removes protocol from string.
function removeProtocol(str) {
    const protocol = url.parse(str).protocol;
    if (protocol) {
        console.log('found protocol ', protocol);
        const index = str.indexOf(protocol);
        str = str.substr(index + protocol.length + 2);
    }
    return str;
}

function addSplitedByDotsStr(arrayOfStrings) {
    const newArr = [];
    for (let j = 0; j < arrayOfStrings.length; j++) {
        const arrByDots = arrayOfStrings[j].split('.');
        for (let i = 0; i < arrByDots.length; i++) {
            // If the word is shorter then 3 chars, ignore it (it can be com,gov etc.)
            if (arrByDots[i].length > 3) {
                newArr.push(arrByDots[i]);
            }
        }
    }
    return newArr;
}
