/**
 * Mail helper
 *
 * @example
 * sender.sendMail(
 *     'user@example.com',
 *     'Hello',
 *     'Hello, {name}!',
 *     {name: 'John Doe'},
 *     function(err) {
 *         console.log('email err', err);
 *     }
 * );
 *
 */

'use strict';

const smtpTransport = require('nodemailer-smtp-transport'),

    /**
     * Mail configuration
     * @type {Object} config
     * @property {string} service - SMTP-service, example Google
     * @property {Object} auth - authentication config
     * @property {string} auth.user - user name
     * @property {string} auth.pass - user password
     * @property {string} sender - sender email
     */
    config = {
        service: 'Google',
        auth: {
            user: 'info@darkvision.com',
            pass: 'pswd'
        },
        sender: 'info@darkvision.com'
    },

    /**
     * SMTP transport module instance
     * @type {Object} mailer
     */
    mailer = require('nodemailer').createTransport(smtpTransport({
        service: config.service,
        auth: config.auth
    })),

    /**
     *
     * @param {?string} tpl - template, example "Hello, {name}!"
     * @param {Object} data - plain-object, example {name: "John Doe"}
     * @return {string}
     */
    templater = function(tpl, data) {
        tpl = tpl || '';

        for (const key in data) {
            tpl = tpl.replace( new RegExp( '{' + key + '}', 'g' ), data[key] );
        }

        return tpl;
    };


module.exports = {
    /**
     * @see :27
     */
    mailer: mailer,

    /**
     * send email to user
     * @param {string} email
     * @param {string} subject
     * @param {string} message - HTML-template
     * @param {Object} data - plain-object of data for template
     * @param {?function(err)} cb - callback
     */
    sendMail: function(email, subject, message, data, cb) {
        if (!(cb instanceof Function)) {
            cb = function() {};
        }

        mailer.sendMail({
            from: config.sender,
            to: email,
            subject: subject,
            html: templater(message, data)
        }, cb);
    }
};
