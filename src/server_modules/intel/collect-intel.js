'use strict';
const config = require('app-config');
//Google Pub Sub
const {PubSub} = require('@google-cloud/pubsub');
const pubsubClient = require('@google-cloud/pubsub');
const shortid = require('shortid');
const helpers = require('../utils/intel-helpers');
const neoAndMongoHelpers = require('helpers');
const moment = require('moment');
const PromiseB = require('bluebird');
process.env.GOOGLE_APPLICATION_CREDENTIALS = config.paths.googlePubSubCredentials;

// Creates a client
const pubsub = new PubSub();
const clienT = new pubsubClient.PubSub();

//Topics
const shodangetHostTopic = process.env['NODE_ENV'] + '-shodan-get-host-topic';
const domain2SubdomainTopic = process.env['NODE_ENV'] + '-domain-to-subdomain-topic';
const domainCollectTopic = process.env['NODE_ENV'] + '-domain-collect-topic';
const companyCollectTopic = process.env['NODE_ENV'] + '-company-collect-topic';

//Subscriptions
const companyCollectSubName = process.env['NODE_ENV'] + '-company-collect-sub';
const domainCollectSubName = process.env['NODE_ENV'] + '-domain-collect-subscription';
const domainCollectSub = pubsub.subscription(domainCollectSubName, {
    ackDeadlineSeconds: 120
});
const companyCollectSub = pubsub.subscription(companyCollectSubName, {
    ackDeadlineSeconds: 120
});

const SCAN_DATE_EMAIL_FORMAT = 'MMMM Do YYYY, [at] HH:mm';

module.exports = function collectIntel() {
    const seneca = this;

    seneca.ready(async () => {
        const promiseAct = PromiseB.promisify(seneca.act, {context: seneca});

        const errorHandler = (err) => {
            console.error("Error from pubsub: ", err);
        };

        const companyCollectHandler = async (message) => {
            console.log('Starting company data collect');

            let msg = JSON.parse(message.data);
            if (msg && msg.id && msg.uEmail) {
                console.info('In companyCollectHandler:');
                try {
                    await collectInQueueCompanyFindings({
                        uEmail: msg.uEmail,
                        uName: msg.uName,
                        id: msg.id,
                        scheduled: msg.scheduled
                    });
                } catch (e) {
                    console.error('Error in collect: ', e);
                }
            } else {
                let errMsg = 'Missing email or id parameters in collect:';
                console.log(errMsg);
            }

            // "Ack" (acknowledge receipt of) the message
            message.ack();
        };

        const collectInQueueCompanyFindings = (msg) => {
            let company;
            let res;

            return new Promise(async (resolve) => {
                console.log('In collectInQueueCompanyFindings - companyId is: ', msg.id);

                const companyNameToEmail = [];

                const scanDateToEmail = moment().format(SCAN_DATE_EMAIL_FORMAT);

                try {
                    company = await promiseAct('role:companies, get:company', {id: msg.id});
                } catch (e) {
                    console.error('Error in collectInQueueCompanyFindings: ', e);
                }

                //if in a scheduled run - 0 scan priority means the company should be excluded.
                if (company && (msg.scheduled && company.scanPriority !== 0 || !msg.scheduled)) {
                    console.log('Working on: ', company.companyName);
                    companyNameToEmail.push(company.companyName);

                    try {
                        await collect(company, msg.scheduled);
                    } catch (e) {
                        console.log('Error in collect:companyFindings', e);
                        resolve({err: e});

                        // Prepare the message object for sending the Email.
                        const failedMsgObj = getAlertMessageObj(msg.uEmail, msg.uName, companyNameToEmail, scanDateToEmail, e);

                        console.log('Starting to notify user: ', failedMsgObj.userEmail, ' that scan from ', failedMsgObj.scanDate, ' was failed.');

                        // Notify user that this company failed.
                        try {
                            res = await promiseAct('role:alertSystem, alert:sendToUserScanHasFailed', failedMsgObj);
                            if (res && res.ok) {
                                console.log('Successfully notified user that scan was failed.');
                            } else {
                                console.log('Error notifying user that scan was failed');
                            }
                        } catch (e) {
                            console.error('Error notifying user that scan was failed:', e);
                        }
                    }
                } else {
                    console.error('No company found with id: ', msg.id);
                    resolve();
                }

                try {
                    if (company && company.scanPriority !== 0) {
                        // Prepare the message object for sending the Email.
                        const alertMsgObj = getAlertMessageObj(msg.uEmail, msg.uName, companyNameToEmail, scanDateToEmail);

                        console.log('Starting to notify user: ', alertMsgObj.userEmail, ' that scan from ', alertMsgObj.scanDate, ' is done.');
                        // Notify user that this company is done.
                        res = await promiseAct('role:alertSystem, alert:sendToUserScanIsDone', alertMsgObj);
                        if (res && res.ok) {
                            console.log('Successfully notified user that scan is done.');

                            resolve({ok: true});
                        } else {
                            let errMsg = 'Error notifying user that scan is done';

                            if (res && res.err) {
                                errMsg += ': ' + res.err;
                            }
                            console.log(errMsg);

                            resolve({err: errMsg});
                        }
                    } else {
                        console.log(companyNameToEmail, 'Did not run due to priority being 0');
                    }
                } catch (e) {
                    console.error('Error in collect:companyFindings3', e);
                    resolve({err: e});
                }
            });
        };

        // This function prepares the message object for sending the email when scan is done.
        const getAlertMessageObj = (userEmail, userName, companyNames, scanDate, err) => {
            const alertMsgObj = {};

            alertMsgObj.userEmail = (userEmail === 'admin@admin.com' || !userEmail.toString().includes('@')) ? 'yuval@rescana.com' : userEmail;

            alertMsgObj.userName = (userName && userName.firstName) ? userName.firstName : 'user';

            if (!Array.isArray(companyNames)) {
                alertMsgObj.companyNames = [companyNames];
            } else {
                alertMsgObj.companyNames = companyNames;
            }

            alertMsgObj.scanDate = scanDate;

            alertMsgObj.error = err;

            return alertMsgObj;
        };

        this.add('role:intel, collectAll:projectFindings', async (msg, respond) => {

            const companies = msg.companies;

            let userData = {
                uEmail: 'yuval@rescana.com',
                uName: 'admin',
                scheduled: msg.scheduled
            };
            respond();

            console.log('Going over all companies for intel collection : ', companies);
            //Get all domains of all companies in project.
            await asyncForEach(companies, async (company) => {
                userData.id = company.id;
                let currentDate = moment().startOf('day').hour(12);
                let scans = await promiseAct('role:scans, getLatestScans:scan', {company: company.id});
                let happenedBeforeTheLastThreeDays;

                if(scans[0] && scans[0].date) {
                    happenedBeforeTheLastThreeDays = currentDate.diff(scans[0].date, 'days') > 3;
                }

                if(scans.length === 0 || scans[0] && (scans[0].percentageDone === 100 || scans[0].date || happenedBeforeTheLastThreeDays)) {
                    try {
                        const dataBuffer = Buffer.from(JSON.stringify(userData));
                        console.log('Adding ', company, ' to queue topic: ', companyCollectTopic);
                        pubsub.topic(companyCollectTopic, {
                            batching: {
                                maxMessages: 1,
                                maxMilliseconds: 1000,
                            }
                        }).publish(dataBuffer);
                    } catch (e) {
                        console.error('Error: Failed to add company', company, ' to topic queue: ', companyCollectTopic);
                    }
                }else{
                    console.log("Didn't start the run, previous scan for", company.companyName ,"did not finish.")
                }
            });
        });

        let connectScanToDomain = async (domain, scanId, date) => {
            let relArr = [];

            const data = {
                resolve_time: date, // Set once and can't change in query
                scanTime: date, // Set every scan.
                scanId: scanId
            };
            const relData = {
                data: data,
                newNodeLabelName: 'domain',
                toNodeLabel: 'domain',
                toNodeType: 'domain',
                toNodeProps: {'hostname': domain}
            };
            relArr.push(relData);
            await neoAndMongoHelpers.createNodeAndRelationship(relArr);
        };

        let collect = async (companies, scheduled) => {
            let date = moment().format();
            let currenScanId = '';

            if (!Array.isArray(companies)) {
                companies = [companies];
            }

            try {
                await asyncForEach(companies, async (company) => {
                    let scanId = shortid.generate();
                    currenScanId = scanId;

                    console.log('In collect - scanId: ', scanId, ' on date: ', date);
                    let scans = await promiseAct('role:scans, getLatestScans:scan', {company: company.id});
                    const prevScans = helpers.setDates(null, null, scans);
                    let prevDate = date;
                    let prevScanId = scanId;

                    if(prevScans.length > 0) {
                        console.log('In collect - previous scanId is : ', prevScans.scans[0].scanId, ' on date: ', prevScans.scans[0].date);

                        console.log('ScanId :', scanId, ': updating scanId and scan date to company');
                        if(prevScans.scans[0]){
                            prevDate = prevScans.scans[0].date;
                            prevScanId = prevScans.scans[0].scanId;
                        }

                        promiseAct('role:basic, update:props', {
                            companyID: company.id,
                            scanId: prevScanId,
                            date: prevDate
                        });
                    }else{
                        promiseAct('role:basic, update:props', {
                            companyID: company.id,
                            scanId: scanId,
                            date: date
                        });
                    }
                    console.log('In collect - Adding collectCompanyData promise to scan: ', (company && company.companyName) ? company.companyName : 'company');
                    await collectCompanyData(scanId, date, companies, company, scheduled);
                });

                if (!scheduled) {
                    console.log('Manual collectCompanyData is done');
                    return PromiseB.resolve();
                } else {
                    console.log('Scheduled collectCompanyData is done');
                    return await promiseAct('role:scheduler, collect:notifyDone', {
                        scanId: scanId,
                        date: date,
                        isOK: true
                    });
                }
            } catch (e) {
                if (!scheduled) {
                    console.log('ScanId :', currenScanId, ': Manual companyQueue.start() has failed: ', e);
                    return PromiseB.resolve();
                } else {
                    console.log('ScanId :', currenScanId, ': Scheduled companyQueue.start() has failed: ', e);
                    return await promiseAct('role:scheduler, collect:notifyDone', {
                        scanId: currenScanId,
                        date: date,
                        error: e
                    });
                }
            }
        };

        let createCompanyNameArray = (companies) => {
            if (companies && Array.isArray(companies) && companies.length > 1) {
                return ' from [' +
                    companies.map((c) => {
                        return (c && c.companyName) ? c.companyName : '';
                    }).join(', ') +
                    ']';
            }
        };

        let collectCompanyData = async (scanId, date, companies, company, scheduled) => {
            let companyName = (company && company.companyName) ? company.companyName : 'company';
            console.log('ScanId :', scanId, ': In collectCompanyData - with ', companyName);
            let happenedBeforeTheLastThreeDays;
            let currentDate = moment().startOf('day').hour(12);
            let scans = await promiseAct('role:scans, getLatestScans:scan', {company: company.id});

            if(scans[0]) {
                happenedBeforeTheLastThreeDays = currentDate.diff(scans[0].date, 'days') > 3;
            }

            if(scans.length === 0 || scans[0] && (scans[0].percentageDone === 100 || scans[0].date || happenedBeforeTheLastThreeDays)) {
                try {
                    await promiseAct('role:scans, set:scan', {data: {date: date, scanId: scanId, company: company}});
                } catch (e) {
                    console.error('ScanId :', scanId, ': Error in role:scans, set:scan on ', companyName, ':', e);
                    return PromiseB.resolve();
                }

                let allCompaniesName = createCompanyNameArray(companies);
                console.log('ScanId :', scanId, ' : Collecting Data on : ', companyName, ' domains', allCompaniesName);
                company.selectedDomains.push('no_domain');

                let scanPriority = 3;
                scanPriority = company.scanPriority || 3;

                if (!scheduled) {
                    scanPriority = 1;
                }

                await asyncForEach(company.selectedDomains, async (domain) => {
                    console.log('ScanId :', scanId, ': Adding', domain, 'To ', domainCollectTopic);
                    let data = {
                        brandDomain: domain,
                        scanId: scanId,
                        date: date,
                        scanPriority: scanPriority,
                        allDomains: company.selectedDomains,
                        companyName: companyName,
                        companyId: company.id
                    };
                    const dataBuffer = Buffer.from(JSON.stringify(data));
                    try {
                        let id = await pubsub.topic(domainCollectTopic, {
                            batching: {
                                maxMessages: 1,
                                maxMilliseconds: 1000,
                            }
                        }).publish(dataBuffer);
                        console.log('ScanId :', scanId, ': Done Publishing ', domain, ' to topic ', domainCollectTopic, 'with ID ', id);
                    } catch (e) {
                        console.error('ScanId :', scanId, ': Error while publishing domain ', domain, ' to ', domainCollectTopic, 'queue: ', e);
                    }
                });

                console.log('ScanId :', scanId, ': All ', companyName, ' domains were inserted to collect queue');
            }else{
                console.log("Didn't start the run, previous scan for", company.companyName ,"did not finish.")
            }
        };

        let collectIntel = async (msg) => {
            let data = JSON.parse(msg.data);
            console.log('ScanId :', data.scanId, ': Received message from queue for : ', data.brandDomain);
            let {brandDomain, date, scanId, allDomains, mainDomain, isSubDomain, companyName, companyId, scanPriority} = data;
            let ipAddressArr = [];

            if (brandDomain !== 'no_domain') {

                if (!isSubDomain) {

                    let incData = {
                        scanId: scanId,
                        scanDate: date,
                        topic: domainCollectTopic,
                        increment: true,
                        count: 1,
                        companyName: companyName,
                        companyId: companyId
                    };
                    //Update counter for progress monitoring
                    await helpers.incItemStatus(incData);

                    try {
                        await connectScanToDomain(brandDomain,scanId,date);
                    } catch (e) {
                        console.error('Failed to create domain node: ', e);
                    }

                    console.log('ScanId :', data.scanId, ': Getting Txt records for SPF for domain: ', brandDomain);
                    helpers.resolveSpf(brandDomain, scanId, date);

                    console.log('ScanId :', data.scanId, ': Getting Txt records for DMARCfor domain: ', brandDomain);
                    let dmarcStr = '_dmarc.' + brandDomain;
                    helpers.resolveDmarc(dmarcStr, brandDomain, scanId, date);
                } else {

                    let incData = {
                        scanId: scanId,
                        scanDate: date,
                        topic: domain2SubdomainTopic + scanPriority,
                        increment: false,
                        count: 1
                    };

                    //Update counter for progress monitoring
                    await helpers.incItemStatus(incData);

                    console.log('ScanId :', data.scanId, ': saving subdomain: ', brandDomain);
                    await helpers.saveSubdomain(brandDomain, scanId, date, mainDomain);
                }
            }


            console.log('ScanId :', data.scanId, ': Looking up domain: ', brandDomain , ' for IP');
            try {
                ipAddressArr = await helpers.lookUp(brandDomain, scanId, date, isSubDomain, companyId, companyName);
            } catch (e) {
                console.error('ScanId :', data.scanId, ': LookUp Error: ', e);
                ipAddressArr = [];
            }

            if (ipAddressArr && ipAddressArr.length !== 0) {
                console.log('ScanId :', data.scanId, ': Doing reverse DNS on IPs for domain: ', brandDomain);
                helpers.reverseDNS(ipAddressArr, scanId, date);
                helpers.ip2Asn(ipAddressArr, allDomains, scanId, date);
            }

            let incShodangetHostTopic;
            let incDomain2SubHostTopic;
            if (scanPriority) {
                incShodangetHostTopic = shodangetHostTopic + scanPriority;
                incDomain2SubHostTopic = domain2SubdomainTopic + scanPriority;
            } else {
                incShodangetHostTopic = shodangetHostTopic;
                incDomain2SubHostTopic = domain2SubdomainTopic;
            }

            try {
                console.log('ScanId :', data.scanId, ': Getting Shodan data for IPs for domain: ', brandDomain);
                let runIPAddressArr = helpers.reduceIpArr(ipAddressArr);

                //Update counter for progress monitoring
                let incData = {
                    scanId: scanId,
                    scanDate: date,
                    topic: incShodangetHostTopic,
                    increment: true,
                    count: runIPAddressArr.length
                };
                //Update counter for progress monitoring
                await helpers.incItemStatus(incData);

                for (let i = 0; i < runIPAddressArr.length; i++) {
                    let data = {
                        ip: runIPAddressArr[i],
                        scanId: scanId,
                        date: date,
                        domain: brandDomain
                    };
                    const dataBuffer = Buffer.from(JSON.stringify(data));
                    const messageId = pubsub.topic(shodangetHostTopic + scanPriority, {
                        batching: {
                            maxMessages: 1,
                            maxMilliseconds: 1000,
                        }
                    }).publish(dataBuffer);
                    console.log('ScanId :', scanId, ': Message published for IP: ', runIPAddressArr[i], ' with id ', messageId, ' for domain: ', brandDomain);
                }
            } catch (e) {
                console.error('ScanId :', scanId, ':domain:', brandDomain ,' getShodanHostData ERROR:', e);
            }

            //No need in checking subdomains of subdomains - it's given in the vt call.
            if (!isSubDomain && brandDomain !== 'no_domain') {
                try {
                    let incData = {
                        scanId: scanId,
                        scanDate: date,
                        topic: incDomain2SubHostTopic,
                        increment: true,
                        count: 1
                    };
                    //Update counter for progress monitoring
                    await helpers.incItemStatus(incData);
                    console.log('ScanId :', scanId, ': Getting Virus Total data for ', brandDomain, ' (subdomains).');
                    const dataBuffer = Buffer.from(JSON.stringify({
                        brandDomain: brandDomain,
                        scanId: scanId,
                        date: date,
                        allDomains: allDomains
                    }));
                    let id = await pubsub.topic(domain2SubdomainTopic + scanPriority, {
                        batching: {
                            maxMessages: 1,
                            maxMilliseconds: 1000,
                        }
                    }).publish(dataBuffer);
                    console.log('ScanId :', scanId, ': Message published to ', domain2SubdomainTopic + scanPriority, 'for domain ', brandDomain, ' with id ', id);
                } catch (e) {
                    console.error('ScanId : ', scanId, ': Virus total ERROR for domain ', brandDomain, '(get subdomains):', e);
                }
            }

            console.log('ScanId: ', scanId, ': collectIntel msg.ack is called on domain: ', brandDomain);

            try {
                msg.ack();
            }catch(e){
                console.error("Error in ack: ", e);
            }

            let incData = {
                scanId: scanId,
                scanDate: date,
                topic: domainCollectTopic,
                increment: false,
                count: 1
            };
            //Update counter for progress monitoring
            await helpers.incItemStatus(incData);
        };

        for (let i = 1; i <= 5; i++) {
            topicCreator(shodangetHostTopic + i);
            topicCreator(domain2SubdomainTopic + i);
        }

        await topicCreator(companyCollectTopic);
        await topicCreator(domainCollectTopic);
        await subscriberCreator(domainCollectTopic, domainCollectSubName);
        await subscriberCreator(companyCollectTopic, companyCollectSubName);

        domainCollectSub.on(`message`, collectIntel);
        companyCollectSub.on(`message`, companyCollectHandler);
        domainCollectSub.on(`error`, errorHandler);
        companyCollectSub.on(`error`, errorHandler);
    });
};

const topicCreator = async (topicName) => {
    try {
        console.log('Creating Topic: ', topicName);
        // Creates the new topic
        const [topic] = await clienT.createTopic(topicName);
        console.log(`Topic ${topic.name} created.`);
    } catch (err) {
        if (err.code === 409 || err.code === 6) {
            console.log('Topic "' + topicName + '" already exists.');
        }
    }
};

const subscriberCreator = async (topicName, subscriptionName) => {
    try {
        console.log('Creating subscription: ', subscriptionName);
        const [subscription] = await clienT.topic(topicName).createSubscription(subscriptionName, {ackDeadlineSeconds: 120});
        console.log(`Subscription ${subscription.name} created.`);
    } catch (err) {
        if (err.code === 409 || err.code === 6) {
            console.log('subscription "' + subscriptionName + '" already exists.');
        }
    }
};


const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};
