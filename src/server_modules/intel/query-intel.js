const helpers = require('../utils/intel-helpers');
const Promise = require('bluebird');
const {DEFAULT_INTEL_SCORE_RATIOS, EMAIL_BREACHES_TYPE} = require('../../../config/consts');
const apiHelpers = require('../../server/api/api-helpers.js');
const extraHelpers = require('../../server/api/helpers');
const moment = require('moment');


module.exports = function intelQueries() {


    this.add('role:intelQuery,cmd:getVulnsByIP', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'cpe');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getVulnsByIP: ", e);
        }
    });

    this.add('role:intelQuery,cmd:getPortsByIP', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'ports');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getPortsByIP: ", e);
        }
    });

    this.add('role:intelQuery,cmd:getIsp', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'isp');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getIsp: ", e);
        }
    });

    this.add('role:intelQuery,cmd:getByDomain', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'domain');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getByDomain: ", e);
        }
    });

    this.add('role:intelQuery,cmd:getBuckets', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'bucket');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getBuckets: ", e);
        }
    });

    this.add('role:intelQuery,cmd:getSpf', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'spf');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getSpf: ", e);
        }
    });

    this.add('role:intelQuery,cmd:getDmarc', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'dmarc');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getDmarc: ", e);
        }
    });

    this.add('role:intelQuery,cmd:getAllIPs', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'IP');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getDmarc: ", e);
        }
    });

    this.add('role:intelQuery,cmd:getProductsByIP', async (msg, respond) => {
        try {
            await getFindingsForIntelByType(msg, respond, 'product');
        } catch (e) {
            console.error("Error: error in role:intelQuery,cmd:getProductsByIP: ", e);
        }
    });

    const getFindingsForIntelByType = async (msg, respond, type) => {
        const act = Promise.promisify(this.act, {context: this});
        let filteredCompanies = [];
        let filteredCompaniesNames = [];

        try {
            let companies = await helpers.getAllowedCompaniesByProject(msg.uid, msg.project, this);

            filteredCompanies = helpers.filterCompanies(msg.companiesFilter, companies);
            filteredCompaniesNames = helpers.filterCompaniesNames(msg.companiesFilter, companies);

            let scanArr = await Promise.map(filteredCompanies, (company) => {
                // Returns an array of object [{companyID:companyID, scanId: scanId}]
                return act('role:scans, getLatestScans:scan', {company: company});
            });

            const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);

            let AllowedDomainsCollection = await helpers.getDomainsByCompanies(msg.uid, filteredCompanies, {context: this});
            let res = await getIntelDataByType(AllowedDomainsCollection, data.startDate, data.endDate, data.scans, false, filteredCompaniesNames, type);
            let assetUserInputAndMitigatedRes = {};
            let org = false;
            if (msg.orgId && msg.orgId !== 'No Id') {
                try {
                    assetUserInputAndMitigatedRes = await helpers.getAssetUserInputAndMitigatedNodesByType(type, msg.orgId);
                    org = await helpers.getOrg(msg.orgId);
                } catch (e) {
                    console.log('Error: In getAssetUserInputAndMitigatedNodesByType: ', e);
                }
            }

            let returnData = [res];
            if (assetUserInputAndMitigatedRes && assetUserInputAndMitigatedRes.length > 0) {
                returnData.push(assetUserInputAndMitigatedRes);
            }else{
                returnData.push(null);
            }

            //Show latest scan date only when displaying one company.
            if (org && Array.isArray(org) && org[0].showScanDate && data.scans.length === 1) {
                returnData.push({scanDateShow:org[0].showScanDate, latestScanDate: data.scans[0].date});
            }else{
                returnData.push(null);
            }


            respond(null, returnData);
        } catch (e) {
            console.error('In getFindingsForIntelByType - '+ type +' - Error: ' + e);
            respond(null, []);
        }
    };


    const getIntelDataByType = async (AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames, type) => {
        if(type) {
            if (type === 'cpe') {
                return await helpers.getVulnsByIP(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            } else if (type === 'ports') {
                return await helpers.getPortsByIP(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            } else if (type === 'isp') {
                return await helpers.getIsp(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            } else if (type === 'domain') {
                return await helpers.getVulnsByIP(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            } else if (type === 'bucket') {
                return await helpers.getBuckets(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            } else if (type === 'spf') {
                return await helpers.getSpf(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            } else if (type === 'dmarc') {
                return await helpers.getDmarc(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            } else if (type === 'IP') {
                return await helpers.getAllIPs(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            } else if (type === 'product') {
                return await helpers.getProductsByIP(AllowedDomainsCollection, startDate, endDate, scans, isForReport, filteredCompaniesNames);
            }
        } else {
            console.error("No type found.");
        }
    };

    this.add('role:intelQuery,cmd:getEmailBreachesData', async (msg, respond) => {
        if (msg.uid && msg.domains) {
            let res = await helpers.getEmailBreachesByDomains(msg.domains);
            if (res && Array.isArray(res)) {
                respond(null, res);
            } else {
                respond();
            }
        } else {
            respond();
        }
    });

    this.add('role:intelQuery,cmd:getEmailBreachInfo', async (msg, respond) => {
        if (msg.uid && msg.nodeID) {
            let res = await helpers.getEmailBreachByNodeID(msg.nodeID);
            if (res && Array.isArray(res)) {
                respond(null, res);
            } else {
                respond();
            }
        } else {
            respond();
        }
    });

    this.add('role:intelQuery,cmd:getNodeNearestInfo', (msg, respond) => {
        if (msg.uid && msg.project && msg.nodeID && msg.nodeType) {
            const act = Promise.promisify(this.act, {context: this});
            return helpers.getAllowedCompaniesByProject(msg.uid, msg.project, this)
                .then((companies) => {
                    return Promise.map(companies, (company) => {
                        // Returns an array of object [{companyID:companyID, scanId: scanId}]
                        return act('role:scans, getLatestScans:scan', {company: company});
                    });
                })
                .then((scanArr) => {
                    const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);
                    return helpers.getSurroundingNodesOfNodeID(msg.nodeID, msg.nodeType, data.startDate, data.endDate, data.scans);
                })
                .then((res) => {
                    if (res && Array.isArray(res)) {
                        respond(null, res);
                    } else {
                        respond();
                    }
                });
        } else {
            respond();
        }
    });

    // This is called from scheduler for the whole system's data and not for a specific user.
    this.add('role:intelQuery,cmd:getAllIPsByDomains', (msg, respond) => {
        console.log('In getAllIPsByDomains');
        return helpers.getAllIPsByDomains(msg.domains)
            .then((allIPs) => {
                if (allIPs && Array.isArray(allIPs) && allIPs.length > 0) {
                    console.log('getAllIPsByDomains returned a total of ', allIPs.length, ' IPs');
                } else {
                    console.log('getAllIPsByDomains didn\'t fetch any IPs');
                }
                respond(null, allIPs);
            })
            .catch((e) => {
                console.log('Error in getAllIPsByDomains: ', e);
                respond(null, []);
            });
    });

    this.add('role:intelQuery,cmd:getAllSubdomainsByDomains', (msg, respond) => {
        console.log('In getAllSubdomainsByDomains');
        if (msg && msg.domains && Array.isArray(msg.domains) && msg.domains.length > 0) {
            return helpers.getAllSubdomainsByDomains(msg.domains)
                .then((allSubdomains) => {
                    if (allSubdomains && Array.isArray(allSubdomains) && allSubdomains.length > 0) {
                        console.log('getAllSubdomainsByDomains returned a total of ', allSubdomains.length, ' subdomains');
                    } else {
                        console.log('getAllSubdomainsByDomains didn\'t fetch any subdomains');
                    }

                    const allDomainsWithSubdomains = helpers.groupSubdomainsByDomains(msg.domains, allSubdomains);

                    respond(null, allDomainsWithSubdomains);
                })
                .catch((e) => {
                    console.log('Error in getAllSubdomainsByDomains: ', e);
                    respond(null, msg.domains);
                });
        } else {
            console.log('No input was sent to getAllSubdomainsByDomains');
            respond(null, []);
        }
    });

    this.add('role:intelQuery,cmd:getASNByIP', (msg, respond) => {
        const act = Promise.promisify(this.act, {context: this});
        let filteredCompanies = [];
        let filteredCompaniesNames = [];
        return helpers.getAllowedCompaniesByProject(msg.uid, msg.project, this)
            .then((companies) => {
                filteredCompanies = helpers.filterCompanies(msg.companiesFilter, companies);
                filteredCompaniesNames = helpers.filterCompaniesNames(msg.companiesFilter, companies);

                return Promise.map(filteredCompanies, (company) => {
                    // Returns an array of object [{companyID:companyID, scanId: scanId}]
                    return act('role:scans, getLatestScans:scan', {company: company});
                });
            })
            .then((scanArr) => {
                const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);

                helpers.getDomainsByCompanies(msg.uid, filteredCompanies, {context: this})
                    .then((AllowedDomainsCollection) => {
                        return helpers.getASNByIP(AllowedDomainsCollection, data.startDate, data.endDate, data.scans, false, filteredCompaniesNames);
                    })
                    .then(async (res) => {
                        let amRes = {};
                        if (msg.orgId && msg.orgId !== 'No Id') {
                            try {
                                amRes = await helpers.getAssetUserInputAndMitigatedNodesByType('asn', msg.orgId);
                            } catch (e) {
                                console.log('In getAssetUserInputAndMitigatedNodesByType - no data returned');
                            }
                        }
                        return [amRes, res];
                    }).spread((assetUserInputAndMitigatedRes, res) => {
                    let returnData = [res];
                    if (assetUserInputAndMitigatedRes && assetUserInputAndMitigatedRes.length > 0) {
                        returnData.push(assetUserInputAndMitigatedRes);
                    }
                    respond(null, returnData);
                }).catch((e) => {
                    console.error('In role:intelQuery,cmd:getASNByIP - Error: ' + e);
                    respond(null, []);
                });
            });
    });

    this.add('role:intelQuery,cmd:getVPNByIP', (msg, respond) => {
        const act = Promise.promisify(this.act, {context: this});
        let filteredCompanies = [];
        let filteredCompaniesNames = [];
        return helpers.getAllowedCompaniesByProject(msg.uid, msg.project, this)
            .then((companies) => {
                filteredCompanies = helpers.filterCompanies(msg.companiesFilter, companies);
                filteredCompaniesNames = helpers.filterCompaniesNames(msg.companiesFilter, companies);

                return Promise.map(filteredCompanies, (company) => {
                    // Returns an array of object [{companyID:companyID, scanId: scanId}]
                    return act('role:scans, getLatestScans:scan', {company: company});
                });
            })
            .then((scanArr) => {
                const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);

                helpers.getDomainsByCompanies(msg.uid, filteredCompanies, {context: this})
                    .then((AllowedDomainsCollection) => {
                        return helpers.getVPNByIP(AllowedDomainsCollection, data.startDate, data.endDate, data.scans, filteredCompaniesNames);
                    })
                    .then(async (res) => {
                        let amRes = {};
                        if (msg.orgId && msg.orgId !== 'No Id') {
                            try {
                                amRes = await helpers.getAssetUserInputAndMitigatedNodesByType('vpn', msg.orgId);
                            } catch (e) {
                                console.log('In getAssetUserInputAndMitigatedNodesByType - no data returned');
                            }
                        }
                        return [amRes, res];
                    }).spread((assetUserInputAndMitigatedRes, res) => {
                    let returnData = [res];
                    if (assetUserInputAndMitigatedRes && assetUserInputAndMitigatedRes.length > 0) {
                        returnData.push(assetUserInputAndMitigatedRes);
                    }
                    respond(null, returnData);
                }).catch((e) => {
                    console.error('In role:intelQuery,cmd:getVPNByIP - Error: ' + e);
                    respond(null, []);
                });
            });
    });

    /* This function is called after all domains obtained from either projects or companies. */
    const privateGetAllDataForReport = (DomainsCollection, data, respond, companyName, orgId) => {
        console.log("In privateGetAllDataForReport");
        //AM short for assetUserInput and mitigated
        let allAmData = {};
        let resultCVEVulnsArr = [];
        let resultPortsArr = [];
        let resultIPsArr = [];
        let resultASNsArr = [];
        let resultISPsArr = [];
        let resultProductsArr = [];
        let resultBucketsArr = [];
        let resultSPFsArr = [];
        let resultDMARCsArr = [];
        let resultEmailBreachesArr = [];
        let resultSubDomainsArr = [];
        let resultGeoLocationsArr = [];
        const promiseArr = [];

        if (DomainsCollection && DomainsCollection.length > 0) {
            /* true parameter means that this query is for report */
            const promCVEs = helpers.getVulnsByIP(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                            amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, 'cve', ['cve'], orgId);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultCVEVulnsArr = amRes.resultWithoutFpAndMitigated || [];
                        let cveAm = amRes.assetUserInputAndMitigatedResOnly || [];
                        console.log("Finished getVulnsByIP");
                        return Promise.resolve({cveAm: cveAm});
                    } else {
                        console.log("No orgID found in promCVEs");
                        resultCVEVulnsArr = res;
                        console.log("Finished getVulnsByIP");
                        return Promise.resolve();
                    }
                }).catch((e) => {
                    console.error("Error in getVulnsByIP: ", e);
                });

            promiseArr.push(promCVEs);

            /* true parameter means that this query is for report */
            const promPorts = helpers.getPortsByIP(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                            amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, 'ports', ['port', 'ip'], orgId);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultPortsArr = amRes.resultWithoutFpAndMitigated || [];
                        let portsAm = amRes.assetUserInputAndMitigatedResOnly || [];
                        console.log("Finished getPortsByIP");
                        return Promise.resolve({portsAm: portsAm});
                    } else {
                        console.log("No orgID found in promPorts");
                        resultPortsArr = res;
                        console.log("Finished getPortsByIP");
                        return Promise.resolve();
                    }
                }).catch((e) => {
                    console.error("Error in getPortsByIP: ", e);
                });

            promiseArr.push(promPorts);

            /* true parameter means that this query is for report */
            const promIPs = helpers.getAllIPs(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                            amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, 'IP', ['ip'], orgId);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultIPsArr = amRes.resultWithoutFpAndMitigated || [];
                        let ipsAm = amRes.assetUserInputAndMitigatedResOnly || [];
                        console.log("Finished getAllIPs");
                        return Promise.resolve({ipsAm: ipsAm});

                    } else {
                        console.log("No orgID found in promIPs");
                        resultIPsArr = res;
                        console.log("Finished getAllIPs");
                        return Promise.resolve();
                    }
                }).catch((e) => {
                    console.error("Error in getAllIPs: ", e);
                });

            promiseArr.push(promIPs);

            /* true parameter means that this query is for report */
            const promASNs = helpers.getASNByIP(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then((res) => {
                    resultASNsArr = res;
                    console.log("Finished getASNByIP");
                    return Promise.resolve();
                }).catch((e) => {
                    console.error("Error in getASNByIP: ", e);
                });

            promiseArr.push(promASNs);

            /* true parameter means that this query is for report */
            const promISPs = helpers.getIsp(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                            amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, 'isp', ['isp'], orgId);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultISPsArr = amRes.resultWithoutFpAndMitigated || [];
                        let ispFm = amRes.assetUserInputAndMitigatedResOnly || [];
                        console.log("Finished getIsp");
                        return Promise.resolve({ispFm: ispFm});
                    } else {
                        console.log("No orgID found in promISPs");
                        resultISPsArr = res;
                        console.log("Finished getIsp");
                        return Promise.resolve();
                    }
                }).catch((e) => {
                    console.error("Error in getIsp: ", e);
                });

            promiseArr.push(promISPs);

            /* true parameter means that this query is for report */
            const promProducts = helpers.getProductsByIP(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                            amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, 'product', ['product'], orgId);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultProductsArr = amRes.resultWithoutFpAndMitigated || [];
                        let productsAm = amRes.assetUserInputAndMitigatedResOnly || [];
                        console.log("Finished getProductsByIP");
                        return Promise.resolve({productsAm: productsAm});
                    } else {
                        console.log("No orgID found in promProducts");
                        resultProductsArr = res;
                        console.log("Finished getProductsByIP");
                        return Promise.resolve();
                    }
                }).catch((e) => {
                    console.error("Error in getProductsByIP: ", e);
                });

            promiseArr.push(promProducts);

            /* true parameter means that this query is for report */
            const promBuckets = helpers.getBuckets(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                            amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, 'bucket', ['bucket'], orgId);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultBucketsArr = amRes.resultWithoutFpAndMitigated || [];
                        let bucketsAm = amRes.assetUserInputAndMitigatedResOnly || [];
                        console.log("Finished getBuckets");
                        return Promise.resolve({bucketsAm: bucketsAm});
                    } else {
                        console.log("No orgID found in promBuckets");
                        resultBucketsArr = res;
                        console.log("Finished getBuckets");
                        return Promise.resolve();
                    }
                }).catch((e) => {
                    console.error("Error in getBuckets: ", e);
                });

            promiseArr.push(promBuckets);

            /* true parameter means that this query is for report */
            const promSPFs = helpers.getSpf(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                           amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, 'spf', ['spf', 'hostname'], orgId);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultSPFsArr = amRes.resultWithoutFpAndMitigated || [];
                        let spfAm = amRes.assetUserInputAndMitigatedResOnly || [];
                        console.log("Finished getSpf");
                        return Promise.resolve({spfAm: spfAm});
                    } else {
                        console.log("No orgID found in promSPFs");
                        resultSPFsArr = res;
                        console.log("Finished getSpf");
                        return Promise.resolve();
                    }
                }).catch((e) => {
                    console.error("Error in getSpf: ", e);
                });

            promiseArr.push(promSPFs);

            /* true parameter means that this query is for report */
            const promDMARCs = helpers.getDmarc(DomainsCollection, data.startDate, data.endDate, data.scans, true, [companyName])
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                            amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, 'dmarc', ['dmarc', 'hostname'], orgId);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultDMARCsArr = amRes.resultWithoutFpAndMitigated || [];
                        let dmarcAm = amRes.assetUserInputAndMitigatedResOnly || [];
                        console.log("Finished getDmarc");
                        return Promise.resolve({dmarcAm: dmarcAm});
                    } else {
                        console.log("No orgID found in promDMARCs");
                        console.log("Finished getDmarc");
                        resultDMARCsArr = res;
                        return Promise.resolve();
                    }
                }).catch((e) => {
                    console.error("Error in getDmarc: ", e);
                });

            promiseArr.push(promDMARCs);

            /* Same query for report and breaches' component */
            const promEmailBreaches = helpers.getEmailBreachesByDomains(DomainsCollection)
                .then(async (res) => {
                    let amRes = {};
                    if (orgId && orgId !== 'No Id') {
                        try {
                            amRes = await helpers.updateResWithAssetUserInputAndMitigatedData(res, EMAIL_BREACHES_TYPE, ['id'], orgId, true);
                        } catch (e) {
                            console.log('In updateResWithAssetUserInputAndMitigatedData - no data returned');
                        }
                        resultEmailBreachesArr = amRes.resultWithoutFpAndMitigated || [];
                        let emailBreachesAm = [];
                        if (amRes.assetUserInputAndMitigatedResOnly) {
                            emailBreachesAm = extraHelpers.removeUnnecessaryProps(amRes.assetUserInputAndMitigatedResOnly, EMAIL_BREACHES_TYPE);
                        }
                        console.log("Finished getEmailBreachesByDomains");
                        return Promise.resolve({emailBreachesAm: emailBreachesAm});
                    } else {
                        console.log("No orgID found in promEmailBreaches");
                        console.log("Finished getEmailBreachesByDomains");
                        resultEmailBreachesArr = res;
                        return Promise.resolve();
                    }

                }).catch((e) => {
                    console.error("Error in getEmailBreachesByDomains: ", e);
                });

            promiseArr.push(promEmailBreaches);


            const promSubDomains = helpers.getAllSubdomainsByDomains(DomainsCollection)
                .then((res) => {
                    console.log("Finished getAllSubdomainsByDomains");
                    resultSubDomainsArr = res;
                    return Promise.resolve();
                }).catch((e) => {
                    console.error("Error in getAllSubdomainsByDomains: ", e);
                });

            promiseArr.push(promSubDomains);

            const promGeoLocations = helpers.getAllGeoLocationsByCompanyName(companyName, data.scans)
                .then((res) => {
                    console.log("Finished getAllGeoLocationsByCompanyName");
                    resultGeoLocationsArr = res;
                    return Promise.resolve();
                }).catch((e) => {
                    console.error("Error in getAllGeoLocationsByCompanyName: ", e);
                });

            promiseArr.push(promGeoLocations);

            return Promise.all(promiseArr)
                .then((amRes) => {
                    console.log("All promises in privateGetAllDataForReport are done");
                    if (amRes && Array.isArray(amRes) && amRes.length > 0) {
                        amRes.map((curData) => {
                            if (curData) {
                                if (curData.cveAm) {
                                    allAmData.cveAm = curData.cveAm;
                                } else if (curData.portsAm) {
                                    allAmData.portsAm = curData.portsAm;
                                } else if (curData.ipsAm) {
                                    allAmData.ipsAm = curData.ipsAm;
                                } else if (curData.ispFm) {
                                    allAmData.ispFm = curData.ispFm;
                                } else if (curData.productsAm) {
                                    allAmData.productsAm = curData.productsAm;
                                } else if (curData.bucketsAm) {
                                    allAmData.bucketsAm = curData.bucketsAm;
                                } else if (curData.spfAm) {
                                    allAmData.spfAm = curData.spfAm;
                                } else if (curData.dmarcAm) {
                                    allAmData.dmarcAm = curData.dmarcAm;
                                } else if (curData.emailBreachesAm) {
                                    allAmData.emailBreachesAm = curData.emailBreachesAm;
                                }
                            }
                        });
                    }
                    respond(null, {
                        cveVulns: resultCVEVulnsArr,
                        ports: resultPortsArr,
                        ips: resultIPsArr,
                        asns: resultASNsArr,
                        isps: resultISPsArr,
                        products: resultProductsArr,
                        buckets: resultBucketsArr,
                        spfs: resultSPFsArr,
                        dmarcs: resultDMARCsArr,
                        emailBreaches: resultEmailBreachesArr,
                        subdomains: resultSubDomainsArr,
                        geoLocations: resultGeoLocationsArr,
                        allAmData: allAmData
                    });
                })
                .catch((e) => {
                    console.log(e);
                    respond();
                });
        } else {
            console.log("No domainCollection returned");
            respond();
        }
    };

    this.add('role:intelQuery,cmd:getAllDataForReport', async(msg, respond) => {
        const act = Promise.promisify(this.act, {context: this});

        if (msg.exportType && msg.exportType === 'prj') {
            const companyIDs = [];
            let companies = [];
            try {
                await asyncForEach(msg.names, async (projectName) => {
                    let res = await helpers.getAllowedCompaniesByProject(msg.uid, projectName, this);
                    companies.push(res);
                });
            }catch(e){
                console.error("Error in getAllowedCompaniesByProject: ", e);
            }

            if (companies && companies.length > 0) {
                for (let i = 0; i < companies.length; i++) {
                    if (companies[i] && Array.isArray(companies[i]) && companies[i].length > 0) {
                        for (let j = 0; j < companies[i].length; j++) {
                            if (companies[i][j] && !companyIDs.includes(companies[i][j])) {
                                companyIDs.push(companies[i][j]);
                            }
                        }
                    }
                }
            }

            let scans = [];
            try {
                await asyncForEach(companyIDs, async (companyID) => {
                    // Returns an array of object [{companyID:companyID, scanId: scanId}]
                    let res = await act('role:scans, getLatestScans:scan', {company: companyID});
                    scans.push(res);
                });
            } catch(e){
                console.error("Error in getLatestScans: ", e);
            }

            let DomainsCollection = [];
            try {
                DomainsCollection = await helpers.getDomainsByCompanies(msg.uid, companyIDs, {context: this});
            }catch(e){
                console.error("Error in getDomainsByCompanies: ", e);
            }

            // Start and End date will be undefined here.
            const data = helpers.setDates(msg.startDate, msg.endDate, scans);
            let privateData;
            try {
                privateData = await privateGetAllDataForReport(DomainsCollection, data, respond, null, msg.orgId);
            }catch(e){
                console.error("Error in privateGetAllDataForReport: ", e);
            }
            respond(privateData);

        } else if (msg.exportType && msg.exportType === 'cmp') {
            let scans = [];
            await asyncForEach(msg.IDs, async (companyID) => {
                // Returns an array of object [{companyID:companyID, scanId: scanId}]
                let res = await act('role:scans, getLatestScans:scan', {company: companyID});
                scans.push(res);
            });

            let DomainsCollection = [];
            try {
                DomainsCollection = await helpers.getDomainsByCompanies(msg.uid, msg.IDs, {context: this});
            }catch(e){
                console.error("Error in getDomainsByCompanies: ", e);
            }

            // Start and End date will be undefined here.
            const companyName = msg && msg.names && msg.names[0] ? msg.names[0] : null;
            const data = helpers.setDates(msg.startDate, msg.endDate, scans);
            console.log("Got domains by companies");
            let privateData;
            try {
                privateData = await privateGetAllDataForReport(DomainsCollection, data, respond, companyName, msg.orgId);
            }catch(e){
                console.error("Error in privateGetAllDataForReport: ", e);
            }
            respond(privateData);

        } else {
            respond();
        }
    });

    this.add('role:intelQuery,cmd:getMaxCVSScore', async (msg, respond) => {
        const actContext = this;
        const act = Promise.promisify(this.act, {context: actContext});
        let companiesIDs;
        let companies;
        let getExtraInfo = true;

        if(msg.hasOwnProperty("getExtraInfo")){
            getExtraInfo = msg.getExtraInfo;
        }

        console.log('In getMaxCVSScore');
        try {
            if (msg.companyID) {
                // getCompanyDataByID.
                companies = await act('role:companies, get:company', {id: msg.companyID});
            } else if (msg.companyIDs) {
                companies = await new Promise((resolve) => resolve(msg.companyIDs));
            } else {
                companies = await helpers.getAllowedCompaniesByProject(msg.uid, msg.project, this);
            }
        } catch(e){
            console.error("Error in getMaxCVSScore : ", e);
        }

        console.log('In getMaxCVSScore - received companiesIDs');
        if (companies && !Array.isArray(companies)) {
            companies = [companies.id];
        }

                // Sort results if requested from user.
                if (msg.selectedDisplaySort) {
                    // This function will return the matching sort function if needed to be used.
                    const sortFunc = helpers.getCompaniesSortFunc(msg.selectedDisplaySort, msg.orgId);

            if (sortFunc && typeof sortFunc === 'function') {
                // Get all companies data by companyIDs.
                const fullDataCompanies = await act('role:companies, listByObjectID:company', {query: companies});

                // Sort and return only companyIDs.
                companies = fullDataCompanies.sort(sortFunc).map((comp) => {
                    return (comp && (comp._id || comp.id));
                });
            }
        }

        // Prevent too much data on too many companies in Dashboard.
        if (msg.sliceStartIndex != null && msg.sliceMaxLength) {
            companies = companies.slice(msg.sliceStartIndex, msg.sliceStartIndex + msg.sliceMaxLength);
        }
        companiesIDs = companies;
        let maxCvssResult = [];

        try {
            await asyncForEach(companies, async (company) => {
                //In case orgId was not provided from the ui or when we calculate more then one company (like scheduler or admin manual) -
                //we getting organization id for each company we calculating score
                if (msg.orgId === null || msg.orgId === undefined || msg.culcFromAdminMenual) {
                    try {
                        let query = 'MATCH (i:organization)-[r:owner]->(c:COMPANY) WHERE c.cid="' + company + '" RETURN i.oid as orgId LIMIT 1';
                        let res = await extraHelpers.getFromNeoByQuery(query);
                        if (res && res[0] && res[0].orgId) {
                            msg.orgId = res[0].orgId;
                        }
                        console.log('In getFromNeoByQuery - Company with id: ' + company + ' is related to organization with id: ' + msg.orgId + ' was returned');
                    } catch (e) {
                        console.log('In getFromNeoByQuery - Company with id: ' + company + ' Do not related to any organization');
                    }
                }
                // Run on each company separately so scans will relate to companies well.
                let res = await getMaxCVSScoreByCompanyID(company, msg, act, actContext);
                maxCvssResult.push(res);
            });
        } catch(e) {
            console.log('Error in getMaxCVSScore-getMaxCVSScoreByCompanyID: ', e);
            respond(null, []);
        }

        let companiesWithExtraData = [];
        try {
            if(getExtraInfo) {
                companiesWithExtraData = await helpers.getCompanyExtraDataFromNeo(maxCvssResult);
            }else{
                companiesWithExtraData = maxCvssResult;
            }
        } catch (e) {
            console.log('Error in getMaxCVSScore-getCompanyExtraDataFromNeo: ', e);
            respond(null, []);
        }

        console.log('Finished with success getMaxCVSScore on ', companiesIDs);
        if ((msg.companyID) && companiesWithExtraData && Array.isArray(companiesWithExtraData) && companiesWithExtraData.length > 0) {
            companiesWithExtraData = companiesWithExtraData[0];
        }
        respond(null, companiesWithExtraData);
    });

    const getMaxCVSScoreByCompanyID = (companyID, msg, act) => {
        return new Promise((resolve) => {
            if (companyID) {
                let isFirstScoresDataLoad = false;
                console.log("In getMaxCVSScoreByCompanyID - working on companyID: ", companyID);
                return act('role:scans, getLatestScans:scan', {company: companyID})
                    .then((scanArr) => {
                        // Start and End date will be undefined here.
                        const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);

                        //return [helpers.getAllowedDomainsByCompanyID(companyID, {context: actContext}), data];
                        return [act('role:companies, get:company', {id: companyID}), data];
                    }).catch((e) => {
                        console.error("Error in getMaxCVSScoreByCompanyID-getLatestScans: ", e);
                    }).spread((company) => {
                        console.log(company.companyName);
                        // If calculating scoreData again is not necessary - load it and return right away.
                        if (company && company.scoresData && !msg.saveScoresOnDB) {
                            // Load existing scoresData without recalculating (like cache).
                            console.log("In getMaxCVSScoreByCompanyID - loading existing scoresData");
                            let scoresData = company.scoresData;

                            // normalizeData works with Array and company is object. Therefor the conversion.
                            let normallizedCompany = helpers.normalizeData([company], true, msg.uid)[0];

                            if (normallizedCompany && !normallizedCompany.intelScoreRatios) {
                                if (scoresData.intelScoreRatios) {
                                    // If current company data has no configuration of intelScoreRatios - set a default one.
                                    normallizedCompany.intelScoreRatios = scoresData.intelScoreRatios;
                                    delete scoresData.intelScoreRatios;
                                } else {
                                    normallizedCompany.intelScoreRatios = DEFAULT_INTEL_SCORE_RATIOS;
                                }
                            }
                            if (scoresData.intelScore != null) {
                                // If intel score is calculated correctly - set it as company's intelScore.
                                normallizedCompany.maxIntelScore = scoresData.intelScore;
                            }
                            if (scoresData.surveyData && scoresData.surveyData.surveyScore != null) {
                                // If survey score is calculated correctly - set it as company's surveyScore.
                                normallizedCompany.surveyScore = scoresData.surveyData.surveyScore;
                                if (scoresData.surveyData.isNA) {
                                    normallizedCompany.isSurveyNA = true;
                                }
                            } else if (normallizedCompany && normallizedCompany.surveyScore != null) {
                                // Otherwise, normalize the existing surveyScore(the last one saved on company).
                                normallizedCompany.surveyScore = Math.round(normallizedCompany.surveyScore * 100);
                            }
                            if (scoresData && scoresData.countEmailBreaches && scoresData.countEmailBreaches.dataCounts) {
                                // Remove EmailBreaches' dataCounts because it is not used from now on.
                                delete scoresData.countEmailBreaches.dataCounts;
                            }

                            normallizedCompany.scoresData = scoresData;

                            return Promise.resolve(normallizedCompany);
                        } else {
                            // Fetch all scoresData and eventually save it for future use (like cache).
                            console.log("In getMaxCVSScoreByCompanyID - calculating scoresData");
                            let AllowedDomainsCollection = [];

                            if (company && company.selectedDomains) {
                                for (let j = 0; j < company.selectedDomains.length; j++) {
                                    if (!AllowedDomainsCollection.includes(company.selectedDomains[j])) {
                                        AllowedDomainsCollection.push(company.selectedDomains[j]);
                                    }
                                }
                            }

                            if (AllowedDomainsCollection.length > 0) {
                                    // Call getAndNormalizeCompaniesData with companiesIDs. then fetch companies data.
                                    return helpers.getAndNormalizeCompaniesData([companyID], msg.uid, act)
                                    .then((res) => {
                                        console.log("getMaxCVSScore - fetched data successfully from getAndNormalizeCompaniesData");

                                        // Save scoreData after first time calculated.
                                        isFirstScoresDataLoad = true;

                                        // Call getIntelAndSurveyScoresByCVSScore to fetch the scores data.
                                        return helpers.getIntelAndSurveyScoresByCVSScore(res, msg.uid, msg.orgId, act)
                                    }).catch((e) => {
                                        console.log("Error in getMaxCVSScore - getAndNormalizeCompaniesData: ", e);
                                        return Promise.resolve([]);
                                    })
                            } else {
                                console.log("In getMaxCVSScore - No domains were found");
                                // Getting default scores.
                                company.scoresData = {
                                    companyID: company.id,
                                    countCVEs: {
                                        score: 0,
                                        scoreData: {
                                            criticalCVSSCount: 0,
                                            mediumCVSSCount: 0
                                        }
                                    },
                                    countDNS: {
                                        score: 0,
                                        scoreData: {
                                            countNotConfiguredSPFs: 0,
                                            countNotConfiguredDMARCs: 0
                                        }
                                    },
                                    countBuckets: {
                                        score: 0,
                                        scoreData: {
                                            publicBucketsCount: 0,
                                            totalBucketsCount: 0
                                        }
                                    },
                                    countEmailBreaches: {
                                        score: 0,
                                        scoreData: {
                                            breachedEmailsCount: 0,
                                            totalEmailsCount: 0
                                        }
                                    },
                                    countDataleaks: {
                                        score: 0,
                                        scoreData: {
                                            criticalDataleaks: 0,
                                            mediumDataleaks: 0
                                        }
                                    },
                                    countBlacklists: {
                                        score: 0,
                                        scoreData: {
                                            ip: 0,
                                            mx: 0
                                        }
                                    },
                                    countBotnets: {
                                        score: 0,
                                        scoreData: {
                                            infectedDomainsCount: 0,
                                            totalDomainsCount: 0
                                        }
                                    },
                                    countSSLCerts: {
                                        score: 0,
                                        scoreData: {
                                            validHttpsDomainsCount: 0,
                                            notValidHttpsDomainsCount: 0
                                        }
                                    },
                                    surveyData: {
                                        surveyScore: 0,
                                        isNA: true
                                    },
                                    intelScore: 0,
                                    totalScore: 0,
                                    'last_updated': moment().format()
                                };
                                return Promise.resolve(company);
                            }
                        }
                    }).catch((e) => {
                        console.error("Error in getMaxCVSScoreByCompanyID-getAllowedDomainsByCompanyID: ", e);
                        return Promise.resolve([]);
                    }).then((res) => {
                        console.log("getMaxCVSScore Done - fetched data successfully");
                        if (res && Array.isArray(res) && res.length > 0) {
                            // Return only companyData and remove redundant data.
                            res = res[0];
                            if (res.hasOwnProperty('domainScores')) {
                                delete res.domainScores;
                            }
                            // Delete scoreData unless it is the first load of scores for this company (and then need to save it)
                            if (msg.project && res.hasOwnProperty("scoresData") && !isFirstScoresDataLoad) {
                                // When fetching companies for dashboard project - the scoresData are relevant only for drilldown.
                                delete res.scoresData;
                            }
                        }
                        resolve(res);

                        if (msg.saveScoresOnDB || isFirstScoresDataLoad) {
                            console.log("In getMaxCVSScoreByCompanyID - Saving scoresData of company ", companyID);
                            // If company intel collect is done - save new scores on DB.
                            act('role:companies, updateScoresDataByCompanyID:company', {companyData: res})
                        }
                    }).catch((e) => {
                        console.log('Error in getMaxCVSScore-getIntelAndSurveyScoresByCVSScore: ', e);
                        resolve([]);
                    });
            } else {
                console.log('Wrong parameters inserted to getMaxCVSScoreByCompanyID');
                resolve([]);
            }
        });
    };

    this.add('role:intelQuery,cmd:getCVECountPerHostname', async (msg, respond) => {
        const act = Promise.promisify(this.act, {context: this});
        let companies;
        let scanArr = [];

        try {
            if (msg.companyID) {
                // getCompanyDataByID.
                companies = await act('role:companies, get:company', {id: msg.companyID});
            } else {
                companies = await helpers.getAllowedCompaniesByProject(msg.uid, msg.project, this);
            }
        } catch (e) {
            console.error("role:intelQuery,cmd:getCVECountPerHostname - Error while getting companies: ", e);
        }

        if (companies && !Array.isArray(companies)) {
            companies = [companies.id];
        }

        try {
            await asyncForEach(companies, async (company) => {
                let res = await act('role:scans, getLatestScans:scan', {company: company});
                scanArr.push(res);
            });
        } catch (e) {
            console.error("role:intelQuery,cmd:getCVECountPerHostname - role:scans, getLatestScans:scan Error: ", e);
        }

        const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);

        let AllowedDomainsCollection;
        try {
            if (msg.companyID) {
                AllowedDomainsCollection = await helpers.getAllowedDomainsByCompanyID(msg.companyID, {context: this});
            } else {
                AllowedDomainsCollection = await helpers.getAllowedDomainsByProject(msg.uid, msg.project, {context: this});
            }
        } catch (e) {
            console.error("role:intelQuery,cmd:getCVECountPerHostname - get AllowedDomainsCollection Error: ", e);
        }

        let finalRes;
        try {
            finalRes = await helpers.getCVECountPerHostname(AllowedDomainsCollection, data.startDate, data.endDate, data.scans);
        } catch (e) {
            console.error("role:intelQuery,cmd:getCVECountPerHostname - getCVECountPerHostname() -  Error: ", e);
        }
        respond(null, finalRes);
    });


    this.add('role:intelQuery,cmd:getCVESeverity', (msg, respond) => {
        const act = Promise.promisify(this.act, {context: this});
        let results;
        if (msg.companyID) {
            // getCompanyDataByID.
            results = act('role:companies, get:company', {id: msg.companyID});
        } else {
            results = helpers.getAllowedCompaniesByProject(msg.uid, msg.project, this);
        }
        return results
            .then((companies) => {
                if (companies && !Array.isArray(companies)) {
                    companies = [companies.id];
                }
                return Promise.map(companies, (company) => {
                    // Returns an array of object [{companyID:companyID, scanId: scanId}]
                    return act('role:scans, getLatestScans:scan', {company: company});
                });
            })
            .then((scanArr) => {
                // Start and End date will be undefined here.
                let data = helpers.setDates(msg.startDate, msg.endDate, scanArr);

                let res;
                if (msg.companyID) {
                    res = helpers.getAllowedDomainsByCompanyID(msg.companyID, {context: this});
                } else {
                    res = helpers.getAllowedDomainsByProject(msg.uid, msg.project, {context: this});
                }

                return res
                    .then((AllowedDomainsCollection) => {
                        return helpers.getCVESeverity(AllowedDomainsCollection, data.startDate, data.endDate, data.scans);
                    });
            })
            .then((res) => {
                respond(null, res);
            })
    });

    this.add('role:intelQuery,cmd:getLocationsForReport', async (msg, respond) => {
        const act = Promise.promisify(this.act, {context: this});
        if (msg.exportType && msg.exportType === 'prj') {
            const companyIDs = [];
            let companies = [];

            try {
                await asyncForEach(msg.names, async (projectName) => {
                    let res = await helpers.getAllowedCompaniesByProject(msg.uid, projectName, this);
                    companies.push(res);
                });
            }catch(e){
                console.error("Error in getAllowedCompaniesByProject: ", e);
            }

            // companies is an array of arrays, so needs to be isolated only to distinct IDs.
            if (companies && companies.length > 0) {
                for (let i = 0; i < companies.length; i++) {
                    if (companies[i] && Array.isArray(companies[i]) && companies[i].length > 0) {
                        for (let j = 0; j < companies[i].length; j++) {
                            if (companies[i][j] && !companyIDs.includes(companies[i][j])) {
                                companyIDs.push(companies[i][j]);
                            }
                        }
                    }
                }
            } else{
                console.log("No companies found");
            }
            let scanArr = [];
            await asyncForEach(companyIDs, async (companyID) => {
                 let res = await act('role:scans, getLatestScans:scan', {company: companyID});
                scanArr.push(res);
            });

            // Start and End date will be undefined here.
            const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);
            let locations = [];
            try {
                locations = await helpers.getLocations(data.startDate, data.endDate, data.scans, companyIDs);
                respond(null, locations);
            } catch(e){
                console.error("Error in getLocations: ", e);
                respond();
            }

        } else if (msg.exportType && msg.exportType === 'cmp') {

            let scanArr = [];
            await asyncForEach(msg.IDs, async (companyID) => {
                // Returns an array of object [{companyID:companyID, scanId: scanId}]
                let res = await act('role:scans, getLatestScans:scan', {company: companyID});
                scanArr.push(res);
            });

            // Start and End date will be undefined here.
            const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);
            let locations = [];

            try {
                locations = await helpers.getLocations(data.startDate, data.endDate, data.scans, msg.IDs);
                respond(null, locations);
            }catch(e){
                console.error("Error in getLocations: ", e);
                respond();
            }
        } else {
            respond();
        }
    });

    this.add('role:intelQuery,cmd:getLocations', async (msg, respond) => {
        const act = Promise.promisify(this.act, {context: this});
        let companies;
        let scanArr = [];

        try {
            if (msg.companyID) {
                companies = await act('role:companies, get:company', {id: msg.companyID});
            } else {
                companies = await helpers.getAllowedCompaniesByProject(msg.uid, msg.project, this);
            }
        } catch(e){
            console.error("role:intelQuery,cmd:getLocations - Error while getting companies: ", e);
        }

        if (companies && !Array.isArray(companies)) {
            companies = [companies.id];
        }

        await asyncForEach(companies, async (company) => {
            let res = await act('role:scans, getLatestScans:scan', {company: company});
            scanArr.push(res);
        });

        // Start and End date will be undefined here.
        const data = helpers.setDates(msg.startDate, msg.endDate, scanArr);

        let res = await helpers.getLocations(data.startDate, data.endDate, data.scans, companies);
        respond(null, res);

    });

    this.add('role:intelQuery,add:company', async (msg, respond) => {
        const act = Promise.promisify(this.act, {context: this});
        let passedNeoCompanyName;
        let passedMongo;
        try {
            passedNeoCompanyName = await apiHelpers.updateCompanyNameOnNeo(msg.data.id, msg.data.companyName);
        } catch (err) {
            console.error('Error in addCompany - admin mode - updateCompanyNameOnNeo: ', e);
            respond(null, {
                ok: false,
                error: err || 'Error has occurred while updating companyName.'
            });
        }

        if (passedNeoCompanyName) {
            if (Array.isArray(passedNeoCompanyName) && passedNeoCompanyName[0] && passedNeoCompanyName[0].updatedNodes) {
                // This is just a notification that Neo query ran ok. (updatedNodes.low could be 0 if there is no such company yet)
                console.log('In addCompany - admin mode - updateCompanyNameOnNeo: companyName updated Successfully');
            } else if (passedNeoCompanyName.hasOwnProperty('error')) {
                console.error('In addCompany - admin mode - updateCompanyNameOnNeo: Failed to update companyName');
            }
        }

        let savedCompanyInfo;
        // Save extraData on Neo4j if exists.
        if (msg.data.companyInfo) {
            // Save companyInfo for later registering on neo4j.
            savedCompanyInfo = msg.data.companyInfo;
            delete msg.data.companyInfo;
            // This is a fix to remove redundant UI data from company.
            if (msg.data.hasOwnProperty('companyResponsibles')) {
                delete msg.data.companyResponsibles;
            }
            if (msg.data.hasOwnProperty('companyContacts')) {
                delete msg.data.companyContacts;
            }
            if (msg.data.hasOwnProperty('scoresData')) {
                delete msg.data.scoresData;
            }
            if (msg.data.hasOwnProperty('isSurveyNA')) {
                delete msg.data.isSurveyNA;
            }
            if (msg.data.hasOwnProperty('maxIntelScore')) {
                delete msg.data.maxIntelScore;
            }
            if (msg.data.hasOwnProperty('edit')) {
                delete msg.data.edit;
            }
        } else {
            savedCompanyInfo = null;
        }

        let savedCompanySelectedDomains;
        if (msg.data.selectedDomains) {
            savedCompanySelectedDomains = msg.data.selectedDomains;
        } else {
            savedCompanySelectedDomains = null;
        }

        try {
            //erase unnecessary properties before saving to database
            if (msg.data.chosenOrg) {
                msg.data.chosenOrg = undefined;
            }
            if (msg.data.chosenProj) {
                msg.data.chosenProj = undefined;
            }
            passedMongo = await act('role:companies, update:company', {
                id: msg.data.id, data: msg.data
            });
        } catch (e) {
            const resultCB = {ok: false};
            if (e) {
                resultCB.error = e;
            }
            respond(null, resultCB);
        }

        let passed;

        if (passedMongo) {
            console.log('In addCompany - admin mode - Successfully saved companyData on MongoDB. Starting to update companyName in projects');
            let res = passedMongo;
            // Update companyName also in all projects' instances.
            if (msg.data.hasOwnProperty('companyName')) {
                try {
                    passed = await act('role:companies, updateCompanyNameInProjects:company', {
                        id: msg.data.id,
                        companyName: msg.data.companyName
                    })
                } catch (err) {
                    console.error('In addCompany - admin mode - Failed to update companyName in projects');
                    respond(null, {
                        ok: false,
                        error: err || (passed && passed.err) || 'Error has occurred while updating companyName.'
                    });
                }

                let company;
                if (passed && passed.ok) {
                    console.log('In addCompany - admin mode - Successfully updated companyName in projects. Starting to retrieve company data');
                    // Need to return the added query so we can add it to the table.
                    try {
                        company = await act('role:companies, get:company', {id: res.id});
                    } catch (err) {
                        console.log('In addCompany - admin mode - Failed to retrieve company data');
                        respond(null, {ok: false, error: err});
                    }
                    console.log('In addCompany - admin mode - Successfully retrieved company data');
                    const newCompany = company.data$(false);
                    // This is a fix for returned data to be returned with extraData
                    if (savedCompanyInfo) {
                        newCompany.extraData = {};
                        if (savedCompanyInfo.companyResponsibles) {
                            newCompany.extraData.responsibles = savedCompanyInfo.companyResponsibles;
                        }
                        if (savedCompanyInfo.companyContacts) {
                            newCompany.extraData.contacts = savedCompanyInfo.companyContacts;
                        }
                        if (savedCompanyInfo.companyClassifications) {
                            newCompany.extraData.classifications = savedCompanyInfo.companyClassifications;
                        }
                        if (savedCompanyInfo.companySectors) {
                            newCompany.extraData.sectors = savedCompanyInfo.companySectors;
                        }
                        if (savedCompanyInfo.companyDateOfContact) {
                            newCompany.extraData['date_of_contacts'] = savedCompanyInfo.companyDateOfContact;
                        }
                        if (savedCompanyInfo.companyDateOfSurvey) {
                            newCompany.extraData['date_of_surveys'] = savedCompanyInfo.companyDateOfSurvey;
                        }
                        if (savedCompanyInfo.companyDescription) {
                            newCompany.extraData.descriptions = savedCompanyInfo.companyDescription;
                        }
                        if (savedCompanyInfo.companyInfo) {
                            newCompany.extraData.informations = savedCompanyInfo.companyInfo;
                        }
                    }

                    if (savedCompanySelectedDomains) {
                        newCompany.selectedDomains = savedCompanySelectedDomains;
                    }

                    let passedNeo;
                    // Save companyInfo on neo4j.
                    passedNeo = await apiHelpers.saveCompanyInfoOnNeo({
                        id: res.id,
                        companyName: newCompany.companyName,
                        companyInfo: savedCompanyInfo
                    }, this);

                    if (passedNeo) {
                        if (passedNeo.ok) {
                            console.log('In addCompany - admin mode - Successfully saved companyInfo on GraphDB');
                        }
                        // There is a third case of empty result where there is no companyInfo to save, so there is no need to write any additional log.
                        else if (passedNeo.hasOwnProperty('error')) {
                            console.log('In addCompany - admin mode - Failed to save companyInfo on GraphDB');
                        }
                    }

                    let passedNeoMerge;

                    try {
                        passedNeoMerge = await apiHelpers.mergeCompanyAndRelatedDomainsOnNeo({
                            id: res.id,
                            companyName: newCompany.companyName,
                            selectedDomains: savedCompanySelectedDomains
                        }, this);


                        if (passedNeoMerge) {
                            if (passedNeoMerge.ok) {
                                console.log('In addCompany - admin mode - Successfully merge company and related domains on GraphDB');
                            }
                            // There is a third case of empty result where there is no companyInfo to save, so there is no need to write any additional log.
                            else if (passedNeoMerge.hasOwnProperty('error')) {
                                console.log('In addCompany - admin mode - Failed to merge company and related domains on GraphDB');
                            }
                        }
                    } catch (e) {
                        console.error('Error in addCompany - admin mode - saveCompanyInfoOnNeo: ', e);
                        respond(null, {ok: true, newCompany: newCompany});
                    }
                    try {
                        //Detach company from domains when domains are removed from companies
                        if (msg.detachArrayToRemoveFromNeo && msg.detachArrayToRemoveFromNeo.length > 0) {
                            const data = {
                                label: 'COMPANY',
                                relationship: 'owner',
                                toLabel: 'domain',
                                prop: 'cid',
                                secondProp: 'hostname',
                                propStr: newCompany.id,
                                arrPropStr: msg.detachArrayToRemoveFromNeo
                            };
                            await helpers.detachNodes(data);
                        }
                    } catch (e) {
                        console.error('Error in addCompany - admin mode - Detach Nodes on Neo Graph: ', e);
                    }

                    if (msg.detachIPArrayToRemoveFromNeo || msg.detachIPArrayToAddFromNeo) {
                        try {
                            let ipResult = await apiHelpers.handleIpMergeAndDetach({
                                id: res.id,
                                companyName: newCompany.companyName,
                                detachArr: msg.detachIPArrayToRemoveFromNeo,
                                addArr: msg.detachIPArrayToAddFromNeo,
                            }, this);

                            if (ipResult && ipResult.ok) {
                                console.log('handleIpMergeAndDetach() - Success!');
                            } else {
                                console.error('Error in addCompany - handleIpMergeAndDetach(): ', e);
                            }
                        } catch (e) {
                            console.error('Error in addCompany -  handleIpMergeAndDetach(): ', e);
                        }
                    }

                    respond(null, {ok: true, newCompany: newCompany});
                }
            } else {
                console.error('In addCompany - admin mode - Failed to update companyName in projects');
                respond(null, {
                    ok: false,
                    error: 'You do not have Permissions to edit company.'
                });
            }
        } else {
            console.log('In addCompany - admin mode - Failed to save companyData on MongoDB');
            respond(null, {
                ok: false,
                error: 'You do not have Permissions to edit company.'
            });
        }

    });


    this.add('role:intelQuery,cmd:updateFindingStatus', async (msg, respond) => {

            let respondFailedObj = {
                ok: false,
                error: 'In role:intelQuery,cmd:updateFindingStatus - Failed to update finding status'
            };

            if (msg.findingStatusData && helpers.checkFindingStatusObj(msg.findingStatusData)) {
                let findingStatusData = msg.findingStatusData;
                let objToUpdate = findingStatusData.objToUpdate;
                let orgId = findingStatusData.curOrgObj.orgId;
                let orgName = findingStatusData.curOrgObj.orgName;
                let uid = findingStatusData.uid;
                let userEmail = findingStatusData.userEmail;
                let statusType = findingStatusData.statusType;
                let shouldDetachMitigatedConnection = findingStatusData.shouldDetachMitigatedConnection;
                let updateOnMongoDB = findingStatusData.updateOnMongoDB;
                let dataType = findingStatusData.dataType;
                let isAssetUserInputUpdate = findingStatusData.isAssetUserInputUpdate;

                if (updateOnMongoDB) {
                    try {
                        let updateRes = await helpers.updateAssetUserInputAndMitigatedNodesOnMongo(shouldDetachMitigatedConnection, uid, userEmail, statusType, orgId, objToUpdate, isAssetUserInputUpdate);
                        if (updateRes && updateRes.ok) {
                            let amRes = {};
                            try {
                                amRes = await helpers.getAssetUserInputAndMitigatedNodesByType(dataType, orgId, true);
                            } catch (e) {
                                console.log('In getAssetUserInputAndMitigatedNodesByType - ' + dataType + ' - no data returned');
                            }
                            respond(null, {ok: true, assetUserInputAndMitigatedRes: amRes});
                        } else {
                            console.error('In role:intelQuery,cmd:updateFindingStatus - Error: Failed to update data on mongoDB');
                            respond(null, respondFailedObj);
                        }
                    } catch (e) {
                        console.error('In role:intelQuery,cmd:updateFindingStatus - Error: ' + e);
                        respond(null, respondFailedObj);
                    }
                } else {
                    try {
                        let creationRes = await helpers.createAssetUserInputAndMitigatedDefaultNode(orgId, orgName);
                        if (creationRes && creationRes.ok) {
                            try {
                                let updateRes = await helpers.updateAssetUserInputAndMitigatedNodesOnNeo(shouldDetachMitigatedConnection, uid, userEmail, statusType, orgId, objToUpdate, isAssetUserInputUpdate);
                                if (updateRes && updateRes.ok) {
                                    let amRes = {};
                                    if (orgId && orgId !== 'No Id' && dataType) {
                                        try {
                                            amRes = await helpers.getAssetUserInputAndMitigatedNodesByType(dataType, orgId);
                                        } catch (e) {
                                            console.log('In role:intelQuery,cmd:updateFindingStatus - getAssetUserInputAndMitigatedNodesByType - no data returned');
                                        }
                                    }
                                    respond(null, {ok: true, assetUserInputAndMitigatedRes: amRes});
                                } else {
                                    console.error('In role:intelQuery,cmd:updateFindingStatus - Error: Failed to update nodes on neo');
                                    respond(null, respondFailedObj);
                                }
                            } catch (e) {
                                console.error('In role:intelQuery,cmd:updateFindingStatus - Error: ' + e);
                                respond(null, respondFailedObj);
                            }
                        } else {
                            console.error('In role:intelQuery,cmd:updateFindingStatus - Error: Failed to create fp/mitigated default node.');
                            respond(null, respondFailedObj);
                        }
                    } catch (e) {
                        console.error('In role:intelQuery,cmd:updateFindingStatus - Error: ' + e);
                        respond(null, respondFailedObj);
                    }
                }
            } else {
                console.error('In role:intelQuery,cmd:updateFindingStatus - Error: Some props are missing to finish the process');
                respond(null, respondFailedObj);
            }
        }
    );

    asyncForEach = async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    };
};
