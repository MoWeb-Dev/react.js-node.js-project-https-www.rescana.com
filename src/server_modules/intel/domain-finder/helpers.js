const request = require('request-promise');
const VIEWDNS_KEY = 'fdc418f171d09c7e92ad72bb84f0f9584e2ad817';
const dns = require('dns');
const config = require('app-config');
const neoHelpers = require('helpers');
const helpers = require('./helpers.js');
const moment = require('moment');
const parseDomain = require('parse-domain');
const neo4jRetried = require('@ambassify/neo4j-retried');
const neoAndMongoHelpers = require('helpers');
const neo4jDriver = require('neo4j-driver').v1;
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass),
    {
        maxConnectionLifetime: 20 * 60 * 1000,
        connectionTimeout: 1000 * 45,
        connectionAcquisitionTimeout: 600000,
        maxTransactionRetryTime: 10000,
        connectionPoolSize: 1000
    }), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});

module.exports.removeSubdomains = (domainList) => {
    let newDomainList = [];
    if (!Array.isArray(domainList)){
        domainList = [domainList];
    }

    for (let i = 0; i < domainList.length; i++) {
        let parsedDomain = parseDomain(domainList[i]).domain;
        let parsedDomainTld = parseDomain(domainList[i]).tld;
        if(parsedDomain && parsedDomainTld) {
            newDomainList[i] = parsedDomain + '.' + parsedDomainTld;
        } else {
            console.log("Could not parse domain: ",  newDomainList[i])
        }
    }
    if(newDomainList.length === 1) {
        return newDomainList[0];
    }else{
        return newDomainList;
    }
};


module.exports.inBlackListedKeywords = (phrase) => {
    let bl = ['dns', 'privacy', 'admin', 'google', 'azure', 'amazon', 'cloudflare', 'wix', 'redacted', 'apple', 'twitter','salesforce','appearsInDomainName', 'On behalf of',
        'abuse', 'ENOM', 'elron', 'netvision', 'registry', 'godaddy', 'AWS', 'domain', 'whois', 'sitesdepot', 'BARAK', 'NETVISION', 'AKAM', 'github', 'atlassian.com', 'inter',
    'gsites', 'maxbot', '012', 'NETSYSTEM'];
    let wordArr = [];
    if(phrase) {
        let splitBySpace = phrase.split(' ');
        let splitByDash = phrase.split('-');
        let splitByDot = phrase.split('.');
        wordArr = wordArr.concat(splitBySpace);
        wordArr = wordArr.concat(splitByDash);
        wordArr = wordArr.concat(splitByDot);
        wordArr = [...new Set(wordArr)];

        for (let i = 0; i < bl.length; i++) {
            for (let j = 0; j < wordArr.length; j++) {
                if (wordArr[j].toLowerCase().indexOf(bl[i].toLowerCase()) !== -1) {
                    console.log("Found ", phrase, " in blacklist");
                    return true;
                }
            }
        }
        return false;
    }
    return false;
};

module.exports.extendedWhois = async (domainNode, date) => {
    const options = {
        uri: 'https://api.viewdns.info/whois/',
        qs: {
            domain: domainNode.hostname,
            apikey: VIEWDNS_KEY,
            output: 'json'
        },
        json: true // Automatically parses the JSON string in the response
    };
    try {
        const whois = await request(options);
        if (whois) {
            const relData = {
                fromNodeLabelName: 'domain',
                fromMatchProp: 'hostname',
                toMatchProp: 'whois',
                fromMatchPropVal: domainNode.hostname,
                toMatchPropVal: whois.response.rawdata,
                relationshipLabel: 'whois',
                data: {},
                toNodeLabel: 'whois',
                toNodeType: 'whois',
                toNodeProps: {
                    whois: whois.response.rawdata,
                    ns: whois.response.name_servers,
                    registrant: whois.response.registrant,
                    scanDate: date,
                    expired: domainNode.expired
                }
            };

            await helpers.createNewNode(relData);
            return whois;
        }
    } catch (e) {
        console.error('Error in extendedWhois: ', e);
    }
};

module.exports.reverseWhois = async (whoisData, query, type, date, domainNode) => {
    if (query) {
        console.log("Got inpot for reverse whois: ", query);

        //Removing parentheses
        query = query.replace(/ *\([^)]*\) */g, "");

        console.log('in reverse whois, searching by : ', query);
        let data = {
            apiCall: 'reversewhois',
            query: query,
            queryParam: 'q',
            whois: whoisData,
            type: type,
            domainNode: domainNode,
            date: date,
            rel: {
                fromNodeLabelName: 'whois',
                fromNodeProps: {
                    whois: whoisData.response.rawdata,
                    ns: whoisData.response.name_servers,
                    registrant: whoisData.response.registrant
                }
            }
        };

        return await viewDNSrequestWithPaging(data);
    } else {
        return [];
    }
};

module.exports.reverseNS = async (NS, date, domainNode) => {
    let domainList = [];
    for (let i = 0; i < NS.length; i++) {

        let data = {
            apiCall: 'reversens',
            query: NS[i],
            queryParam: 'ns',
            domainNode: domainNode,
            type: 'reverseNS',
            date: date,
            rel: {
                fromNodeLabelName: 'domain',
                fromNodeProps: {
                    hostname: domainNode.hostname,
                    date: date
                }
            }
        };

        domainList = domainList.concat(await viewDNSrequestWithPaging(data));
    }
    return domainList;
};

const viewDNSrequestWithPaging = async (data) => {
    let {apiCall, query, queryParam, type, date, rel} = data;
    let domainList;
    let firstOptions;
    let results;
    let relData = [];
    let validDomains = [];
    let connectionLvl = parseInt(data.domainNode.connectionLevel) + 1;
    let resultCount = 2000;

    if(data.queryParam === "ns" || data.queryParam === "mx")  {
        resultCount = 200;
    }

    if (query && !this.inBlackListedKeywords(query)) {
        firstOptions = {
            uri: 'https://api.viewdns.info/' + apiCall + '/',
            qs: {
                apikey: VIEWDNS_KEY,
                output: 'json'
            },
            json: true // Automatically parses the JSON string in the response
        };
        firstOptions.qs[queryParam] = query;

        domainList = await request(firstOptions);

        let connectionStrength = 50;
        let fromMatchProp = 'whois';
        let fromMatchPropVal = rel.fromNodeProps.whois;

        if (type === 'reverseNS' || type === 'reverseMX') {
            connectionStrength = 75;
            fromMatchProp = 'hostname';
            fromMatchPropVal = data.domainNode.hostname;
        }

        if (domainList && parseInt(domainList.response.total_pages) > 0 && (parseInt(domainList.response.result_count) < resultCount || parseInt(domainList.response.domain_count) < resultCount)) {
            let promArr = [];
            if (parseInt(domainList.response.total_pages) > 1) {
                for (let i = 1; i <= domainList.response.total_pages; i++) {
                    const secondOptions = {
                        uri: 'https://api.viewdns.info/' + apiCall + '/',
                        qs: {
                            q: firstOptions.qs.q,
                            apikey: VIEWDNS_KEY,
                            output: 'json',
                            page: i
                        },
                        json: true // Automatically parses the JSON string in the response
                    };

                    promArr.push(request(secondOptions));
                }

                results = await Promise.all(promArr);

                for (let i = 0; i < results.length; i++) {
                    let domains = results[i].response.matches || results[i].response.domains || [];

                    if (domains) {
                        for (let i = 0; i < domains.length; i++) {
                            let hostname = domains[i].domain || domains[i];
                            if (hostname !== data.domainNode.hostname) {
                                relData ={
                                    fromNodeLabelName: rel.fromNodeLabelName,
                                    fromNodeProps: rel.fromNodeProps,
                                    newNodeLabelName: 'domain',
                                    relationshipLabel: 'reverse_by_' + type,
                                    fromMatchProp: fromMatchProp,
                                    toMatchProp: 'hostname',
                                    fromMatchPropVal: fromMatchPropVal,
                                    toMatchPropVal: hostname,
                                    data: {},
                                    toNodeLabel: 'domain',
                                    toNodeType: 'domain',
                                    toNodeProps: {
                                        hostname: hostname,
                                        connectionLevel: connectionLvl,
                                        scanDate: date,
                                        connectionStrength: connectionStrength,
                                        validationDate: null,
                                        expired: null
                                    }
                                };
                                validDomains.push(hostname);

                                if (relData && relData.length > 0) {
                                    await helpers.createNewNode(relData);
                                }
                            }
                        }
                    }
                }
            } else {

                let domains = domainList.response.matches || domainList.response.domains || [];
                if (domains) {
                    for (let i = 0; i < domains.length; i++) {
                        let hostname = domains[i].domain || domains[i];
                        if (hostname !== data.domainNode.hostname) {
                            relData = {
                                fromNodeProps: rel.fromNodeProps,
                                newNodeLabelName: 'domain',
                                relationshipLabel: 'reverse_by_' + type,
                                fromMatchProp: fromMatchProp,
                                toMatchProp: 'hostname',
                                fromMatchPropVal: fromMatchPropVal,
                                toMatchPropVal: hostname,
                                fromNodeLabelName: rel.fromNodeLabelName,
                                data: {},
                                toNodeLabel: 'domain',
                                toNodeType: 'domain',
                                toNodeProps: {
                                    hostname: hostname,
                                    connectionLevel: connectionLvl,
                                    scanDate: date,
                                    connectionStrength: connectionStrength,
                                    validationDate: null,
                                    expired: null
                                }
                            };
                            validDomains.push(hostname);
                            if (relData) {
                                await helpers.createNewNode(relData);
                            }
                        }
                    }
                }
            }

            const dataToSave = {
                domain: data.domainNode.hostname,
                source: type,
                discoveredDomains: validDomains
            };

            try {
                if(connectionStrength >= 75) {
                    let savResult = await neoAndMongoHelpers.insertDiscoveredDomainsFindings([dataToSave]);
                    if (savResult && savResult.ok) {
                        console.log('Successfully saved new discovered domains to DB.');
                    } else {
                        console.error('Failed saving new discovered domains to DB: ', (savResult && savResult.err) ? savResult.err : 'couldn\'t identify the source of the error');
                    }
                }
            } catch (e) {
                console.error(e);
            }

            return validDomains;
        }
    }
};

module.exports.saveToNeo = async (relData) => {
    if (relData && relData.length > 0) {
        await neoHelpers.createNodeAndRelationship(relData);
    }
};

module.exports.extractEmails = async (str, whois, domain, date) => {
    let regex = /\S+[a-z0-9]@[a-z0-9\.]+/img;
    let emails = str.match(regex);
    let relData = [];
    let validEmails = [];

    if (emails) {
        for (let i = 0; i < emails.length; i++) {
            if (!this.inBlackListedKeywords(emails[i])) {
                relData = {
                    fromNodeLabelName: 'whois',
                    fromNodeProps: {
                        whois: whois.response.rawdata,
                        related: domain,
                        date: date,
                        ns: whois.response.name_servers,
                        registrant: whois.response.registrant
                    },
                    newNodeLabelName: 'email',
                    relationshipLabel: 'extracted_email',
                    fromMatchProp: 'whois',
                    toMatchProp: 'email',
                    fromMatchPropVal: whois.response.rawdata,
                    toMatchPropVal: emails[i],
                    data: date,
                    toNodeLabel: 'email',
                    toNodeType: 'email',
                    toNodeProps: {email: emails[i], scanDate: date}
                };
                validEmails.push(emails[i]);
                if (relData.length > 0) {
                    await helpers.createNewNode(relData);
                }
            }
        }
    }
    return [...new Set(validEmails)];
};

module.exports.getMxRecords = (domain) => {
    return new Promise((resolve, reject) => {
        dns.resolveMx(domain, (err, results) => {
            if (err) {
                console.error(err);
                reject();
            }
            resolve(results);
        });
    });
};

module.exports.reverseMx = async (domain, date, domainNode) => {
    try {

        let mxAdresses = await this.getMxRecords(domain);
        let domainList = [];
        for (let i = 0; i < mxAdresses.length; i++) {
            let data = {
                apiCall: 'reversemx',
                query: mxAdresses[i].exchange,
                queryParam: 'mx',
                domainNode: domainNode,
                type: 'reverseMX',
                date: date,
                rel: {
                    fromNodeLabelName: 'domain',
                    fromNodeProps: {
                        hostname: domain,
                        date: date
                    }
                }
            };

            domainList.concat(await viewDNSrequestWithPaging(data));
        }
        return domainList;
    } catch (e) {
        console.error('reverseMx error : ', e);
        return [];
    }
};

module.exports.lookUp = (domain, scanId, date, isSubdomain) => {
    return new Promise(resolve => {
        if (domain) {
            let fromNodeLabelName = 'domain';
            domain = this.extractHostname(domain);
            dns.resolve4(domain, (err, resultArr) => {
                if (err) {
                    console.log(err);
                    resolve([]);
                } else {
                    console.log('Connecting IPs to domain names.');
                    if (resultArr && resultArr.length > 0) {
                        let relArr = [];
                        for (let i = 0; i < resultArr.length; i++) {
                            let data = {
                                resolve_time: date, //Set once and can't change in query
                                scanTime: date, //Set every scan.
                                scanId: scanId
                            };

                            if (isSubdomain) {
                                fromNodeLabelName = 'subdomain';
                            }

                            let relData = {
                                fromNodeLabelName: fromNodeLabelName,
                                fromNodeProps: {hostname: domain},
                                newNodeLabelName: 'IP',
                                relationshipLabel: 'IP',
                                data: data,
                                toNodeLabel: resultArr[i],
                                toNodeType: 'IP',
                                toNodeProps: {address: resultArr[i]}
                            };

                            relArr.push(relData);
                        }
                        Promise.resolve(neoHelpers.createNodeAndRelationship(relArr))
                            .then(() => {
                                resolve(resultArr);
                            });

                    } else {
                        resolve(resultArr);
                    }
                }
            });
        } else {
            resolve([]);
        }

    });
};

module.exports.getUnique = (arr, comp) => {

    //store the comparison  values in array
    const unique = arr.map(e => e[comp]).// store the keys of the unique objects
    map((e, i, final) => final.indexOf(e) === i && i)
    // eliminate the dead keys & return unique objects
        .filter((e) => arr[e]).map(e => arr[e]);

    return unique;

};

module.exports.createIndex = (label, property) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();
        console.log('QUERY: ' + 'CREATE INDEX ON :' + label + '(' + property + ')');
        const writeTxResultPromise = session.writeTransaction((transaction) => {
            return transaction.run('CREATE INDEX ON :' + label + '(' + property + ')');
        });

        return writeTxResultPromise
            .then(() => {
                session.close();
                return resolve();
            })
            .catch((error) => {
                console.error('createIndex: ', error);
                return reject();
            });
    });
};

module.exports.createNode = (label, type, props) => {
    return new Promise((resolve, reject) => {
        console.log('Creating first domain node');
        const session = neo4j.session();
        const writeTxResultPromise = session.writeTransaction((transaction) => {
            return transaction.run(createNodeCommand(label, type, props));
        });

        return writeTxResultPromise
            .then((result) => {
                console.log('Go result');
                session.close();
                return resolve(result.records[0].keys[0].n);
            })
            .catch((error) => {
                console.error('createNode: ', error);
                return reject();
            });
    });
};

const createNodeCommand = (label, type, props) => {
    if (props && typeof props === 'object' && props.length !== 0) {
        const propsStr = this.objectToString(props);
        return 'MERGE (n:`' + label + '`:' + type + ' ' + propsStr + ') ' + 'RETURN n';
    } else {
        return 'MERGE (n:`' + label + '`:' + type + ') ' + 'RETURN n';
    }
};

module.exports.objectToString = (props) => {
    if (typeof props !== 'object') {
        return '';
    }
    let propsStr = '{';
    let firstItem = true;
    for (let key in props) {
        if (props.hasOwnProperty(key)) {
            let value = props[key];
            if (key && value) {
                value = escapeHtml(value.toString());
                key = key.split(' ').join('_');
                if (firstItem) {
                    firstItem = false;
                    propsStr += key + ':"' + value + '"';
                } else {
                    propsStr += ' , ' + key + ' : "' + value + '"';
                }
            }
        }
    }
    propsStr += '}';
    return propsStr;
};

const escapeHtml = (text) => {
    if (text) {
        let specials = ['"', '\\'], regex = RegExp('[' + specials.join('\\') + ']', 'g');
        return text.replace(regex, '\\$&');
    } else {
        return text;
    }
};

module.exports.extractHostname = (url) => {
    let hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf('//') > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
};

module.exports.saveDomain = async (data) => {
    await this.createNode('domain', 'domain', {
        hostname: data.hostname,
        connectionLevel: data.connectionLevel,
        insertDate: data.insertDate,
        scanDate: data.scanDate,
        validationDate: data.validationDate,
        expired: data.expired,
        connectionStrength: data.connectionStrength
    });
};

module.exports.createNewNode = async (rel) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();
        let cmd = '';
        let date = moment().format();
        let toMatchPropValObj = {};
        toMatchPropValObj[rel.toMatchProp] = rel.toMatchPropVal;
        rel.fromMatchPropVal = escapeHtml(rel.fromMatchPropVal);
        let newPropsStr = rel.toNodeProps;
        newPropsStr.scanDate = date;
        newPropsStr = neoHelpers.objectToString(newPropsStr);
        toMatchPropValObj = neoHelpers.objectToString(toMatchPropValObj);

        cmd = 'MATCH (`' + rel.fromNodeLabelName + '` : `' + rel.fromNodeLabelName + '`) where ' + rel.fromNodeLabelName + '.' + rel.fromMatchProp + ' = "' + rel.fromMatchPropVal + '"' +
            ' MERGE (b:`' + rel.toNodeType + '`' + toMatchPropValObj + ')' +
            ' MERGE (`' + rel.fromNodeLabelName + '`)-[:`' + rel.relationshipLabel + '`]->(b)' +
            ' ON CREATE SET b = ' + newPropsStr;


        console.log(cmd);

        if (cmd) {
            let writeTxResultPromise = session.writeTransaction((transaction) => {
                return transaction.run(cmd);
            });

            return writeTxResultPromise
                .then(() => {
                    console.log('Done getting domains from DB');
                    session.close();
                    return resolve();
                })
                .catch((error) => {
                    console.error('Error in createNewNode', error);
                    return reject();
                });
        }
        else {
            console.error('ERROR: No command was entered');
            return resolve();
        }
    });
};

module.exports.asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};
