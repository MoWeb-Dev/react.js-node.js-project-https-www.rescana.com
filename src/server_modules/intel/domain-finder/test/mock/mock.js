module.exports.webSpyData = {
    "status": "found",
    "result": {
        "domain": {
            "fullmooncalendar.net": {
                "items": {
                    "adsense": {
                        // Format: "adsense_code": number_of_domains
                        "pub-5953444431482912": 10,
                        "pub-8423794689684356": 0
                    },
                    "analytics": {
                        // Format: "analytics_code": number_of_domains
                        "UA-15207196": 0,
                        "UA-34505845": 9
                    },
                    "dns_servers": {
                        // Format: "nameserver": "nameserver_ip"
                        "erdomain.earth.orderbox-dns.com": "67.15.253.220",
                        "erdomain.mars.orderbox-dns.com": "184.173.150.58",
                        "erdomain.mercury.orderbox-dns.com": "50.23.136.174",
                        "erdomain.venus.orderbox-dns.com": "50.23.75.44"
                    },
                    "ip": {
                        // Format: "ip_address": number_of_domains
                        "209.40.194.244": 9
                    }
                }
            }
        },
        "adsense": {
            "pub-5953444431482912": {
                "fetched": 10,
                "found": 10,
                "items": {
                    // Format: "domain_name": "updated_at"
                    "bobhairstyles.biz": "2013-07-27",
                    "bronchitis-symptoms.biz": "2014-03-18",
                    "fullmooncalendar.net": "2013-07-29",
                    "hernia-symptoms.com": "2014-03-20",
                    "hewlettpackardprinterdrivers.net": "2013-07-28",
                    "howtopatentanidea.net": "2014-03-21",
                    "liver-disease-symptoms.com": "2014-03-21",
                    "long-haircuts.com": "2013-07-30",
                    "mens-haircuts.net": "2014-03-24",
                    "short-haircuts.org": "2013-07-29"
                }
            },
            "pub-8423794689684356": {
                "fetched": 0,
                "found": 0,
                "items": {}
            }
        },
        "analytics": {
            "UA-15207196": {
                "fetched": 0,
                "found": 0,
                "items": {}
            },
            "UA-34505845": {
                "fetched": 9,
                "found": 9,
                "items": {
                    "bobhairstyles.biz": "2014-03-23",
                    "bronchitis-symptoms.biz": "2014-03-18",
                    "fullmooncalendar.net": "2014-03-22",
                    "hewlettpackardprinterdrivers.net": "2014-03-19",
                    "howtopatentanidea.net": "2014-03-21",
                    "liver-disease-symptoms.com": "2014-03-21",
                    "long-haircuts.com": "2014-03-19",
                    "mens-haircuts.net": "2014-03-24",
                    "short-haircuts.org": "2014-03-21"
                }
            }
        },
        "dns_domain": {
            "erdomain.earth.orderbox-dns.com": {
                "fetched": 0,
                "found": 470,
                "items": {}
            },
            "erdomain.mars.orderbox-dns.com": {
                "fetched": 0,
                "found": 470,
                "items": {}
            },
            "erdomain.mercury.orderbox-dns.com": {
                "fetched": 0,
                "found": 469,
                "items": {}
            },
            "erdomain.venus.orderbox-dns.com": {
                "fetched": 0,
                "found": 469,
                "items": {}
            }
        },
        "ip_dns": {
            "67.15.253.220": {
                "fetched": 0,
                "found": 100,
                "items": {}
            },
            "184.173.150.58": {
                "fetched": 0,
                "found": 100,
                "items": {}
            },
            "50.23.136.174": {
                "fetched": 0,
                "found": 100,
                "items": {}
            },
            "50.23.75.44": {
                "fetched": 0,
                "found": 18597,
                "items": {}
            }
        },
        "ip": {
            "209.40.194.244": {
                "fetched": 9,
                "found": 9,
                "items": {
                    "americanpitbull.biz": "2014-03-23",
                    "bobhairstyles.biz": "2014-03-23",
                    "bronchitis-symptoms.biz": "2014-03-18",
                    "childbirthvideo.biz": "2014-03-17",
                    "free-powerpoint-download.com": "2014-03-22",
                    "fullmooncalendar.net": "2014-03-22",
                    "heart-disease-symptoms.com": "2013-07-31",
                    "leukemia-symptoms.net": "2014-03-22",
                    "sinus-symptoms.net": "2014-03-18"
                }
            }
        }
    }
};
