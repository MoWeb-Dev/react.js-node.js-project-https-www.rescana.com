const request = require('request-promise');
const helpers = require('./helpers.js');

let findDomainsByEmails = async (domainNode, date) => {

    let options = {
        uri: 'https://api.hunter.io/v2/domain-search',
        qs: {
            domain: domainNode.hostname,
            api_key: 'fe28ec7730dabf18261e44a69ce5fff0decd566a',
            limit: 1000,
            offset: 0
        },
        json: true // Automatically parses the JSON string in the response
    };

    try {
        let emails = await request.get(options);
        let connectionLvl = parseInt(domainNode.connectionLevel) + 1;
        //get sources from emails object

        let domainArr = [];
        let emailAddresses = [];

        for (let i = 0; i < emails.data.emails.length; i++) {
            emailAddresses.push(emails.data.emails[i].value);
            for (let j = 0; j < emails.data.emails[i].sources.length; j++) {
                let hostname = helpers.removeSubdomains(emails.data.emails[i].sources[j].domain);
                domainArr.push(helpers.extractHostname(hostname));
            }
        }

        domainArr = [...new Set(domainArr)];

        let relData = [];

        for (let i = 0; i < domainArr.length; i++) {

            if (typeof domainArr[i] === 'string') {
                domainArr[i] = {
                    hostname: domainArr[i],
                    connectionLevel: connectionLvl,
                    connectionStrength: 20,
                    scanDate: date,
                    insertDate: date,
                    validationDate: null,
                    expired: null
                };
            }

            if (domainNode.hostname !== domainArr[i].domain) {
                relData.push({
                    fromNodeLabelName: 'domain',
                    fromNodeProps: {
                        hostname: domainNode.hostname
                    },
                    fromMatchProp: 'hostname',
                    toMatchProp: 'hostname',
                    fromMatchPropVal: domainNode.hostname,
                    toMatchPropVal: domainArr[i].hostname,
                    newNodeLabelName: 'domain',
                    relationshipLabel: 'hunter.io',
                    data: date,
                    toNodeLabel: 'domain',
                    toNodeType: 'domain',
                    toNodeProps: domainArr[i]
                });
            }
        }
        console.log('Extracting domains from hunter.io: ', domainNode.hostname);

        await helpers.asyncForEach(relData, async (rel) => {
            console.log("Saving: ", rel.fromNodeProps.hostname);
            await helpers.createNewNode(rel);
        });

        if(domainArr.length !== 0) {
            return {domains: domainArr, emails: emailAddresses, source: 'hunter.io'};
        } else{
            console.log("No results found from hunter.io");
        }

    } catch (e) {
        console.log(e);
    }
};

module.exports.findDomainsByEmails = findDomainsByEmails;
