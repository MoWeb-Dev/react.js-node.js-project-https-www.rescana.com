const helpers = require('./helpers.js');
const neo4jDriver = require('neo4j-driver').v1;
const moment = require('moment');
const config = require('app-config');
const neo4jRetried = require('@ambassify/neo4j-retried');
const neoAndMongoHelpers = require('helpers');
const intelHelpers = require('../../utils/intel-helpers.js');
const wayback = require('wayback-machine');
const waybackModule = require('./wayback.js');
const htmlToText = require('html-to-text');

const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass), {maxTransactionRetryTime: 30000}), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});

const getDomainsByConnectionStrengthAndValidationDate = (connectionStrength) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();
        connectionStrength = connectionStrength || 55;
        let cmd = '';
        cmd = 'MATCH (domain: domain) WHERE domain.hostname IS NOT NULL AND domain.validationDate IS NULL' +
            ' AND toInteger(domain.connectionStrength) < ' + connectionStrength +
            ' RETURN domain.hostname as hostname, domain.connectionLevel as connectionLevel, domain.connectionStrength as connectionStrength' +
            ', domain.scanDate as scanDate, domain.validationDate as validationDate, domain.expired as expired ' +
            ' ORDER BY domain.connectionStrength DESC LIMIT 20';

        console.log(cmd);

        if (cmd) {
            let readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(cmd);
            });

            return readTxResultPromise
                .then((results) => {
                    console.log('Done getting domains from DB');
                    session.close();
                    return resolve(results.records.map((record) => {
                        return {
                            hostname: record.get('hostname'),
                            connectionLevel: record.get('connectionLevel'),
                            connectionStrength: record.get('connectionStrength'),
                            scanDate: record.get('scanDate'),
                            validationDate: record.get('validationDate'),
                            expired: record.get('expired')
                        };
                    }));
                })
                .catch((error) => {
                    console.error('Error in getDomainsByConnectionLvlAndScanDate', error);
                    return reject();
                });
        }
        else {
            console.error('ERROR: No command was entered: ');
            return resolve();
        }
    });
};

const validateRedirectURL = async (domain, companyName, initialDomain) => {
    try {
        let page = await waybackModule.getLatestOnTimeline(domain);
        if(page) {
            const urlText = htmlToText.fromString(page, {
                wordwrap: 'null',
                noLinkBrackets: false,
                ignoreHref: false,
                ignoreImage: false
            });

            //Check if page is a wayback redirect page
            if (urlText.toLowerCase().indexOf("redirecting")) {
                //Extract URLS from redirect page to see where we are redirecting to
                let urlArr = await waybackModule.extractURL(urlText, domain);

                console.log(urlArr);
                //Check if urls on this page lead to a page we know.
                return intelHelpers.appearsInDomainName(companyName, urlArr, true) || intelHelpers.appearsInDomainName(initialDomain, urlArr, true);
            }
        }
    } catch(e){
        console.error("Error With validateRedirectURL: ", e);
    }

};

const updateDomainConnectionStrength = (domain, connectionStrength) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();

        if (domain && domain.hostname || connectionStrength) {
            let cmd = 'MATCH (n:domain) where n.hostname = "' + domain.hostname + '"' +
                ' SET n +={connectionStrength :' + connectionStrength + '}' +
                ' RETURN n';

            console.log(cmd);

            if (cmd) {
                let writeTxResultPromise = session.writeTransaction((transaction) => {
                    return transaction.run(cmd);
                });

                return writeTxResultPromise
                    .then(() => {
                        console.log('Done updating connectionStrength');
                        session.close();
                        return resolve();
                    })
                    .catch((error) => {
                        console.error('Error in updateConnectionStrength', error);
                        return reject();
                    });
            }
            else {
                console.error('ERROR: No command was entered');
                return resolve();
            }
        } else {
            console.error('ERROR updateDomainConnectionStrength: No domain hostname or connectionStrength was found');
            return resolve();
        }
    });
};

const updateValidationDate = (domain) => {
    return new Promise((resolve, reject) => {
        let date = moment().format();
        if (domain && domain.hostname) {
            const session = neo4j.session();
            let cmd = 'MATCH (n:domain) where n.hostname = "' + domain.hostname + '" ' +
                ' SET n +={validationDate : "' + date + '"}' +
                ' RETURN n';
            if (cmd) {
                let writeTxResultPromise = session.writeTransaction((transaction) => {
                    return transaction.run(cmd);
                });

                return writeTxResultPromise
                    .then(() => {
                        console.log('Done updating validation date for : ', domain.hostname);
                        session.close();
                        return resolve();
                    })
                    .catch((error) => {
                        console.error('Error in updateValidationDate', error);
                        return reject();
                    });
            }
            else {
                console.error('ERROR: No command was entered');
                return resolve();
            }
        } else {
            console.error('ERROR updateValidationDate: No domain hostname was found');
            return resolve();
        }
    });
};

const getMultiSources = (domain) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();
        let cmd = 'MATCH (g)-[j]->(n:domain) where n.hostname = "' + domain.hostname + '" ' +
            ' RETURN j';
        if (cmd) {
            let readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(cmd);
            });

            return readTxResultPromise
                .then((results) => {
                    console.log('Done getting sources');
                    session.close();
                    return resolve(results.records.map((record) => {
                        let source = record._fields[0].type;
                        if (source === 'reverse_by_reverse_org' || source === 'reverse_by_reverse_registrant'
                            || source === 'reverse_by_reverse_domain' || source === 'reverse_by_reverse_email'
                            || source === 'reverse_by_reverseNS' || source === 'reverse_by_reverseMX') {
                            return {
                                source: source
                            };
                        }
                    }));
                })
                .catch((error) => {
                    console.error('Error in getMultiSources', error);
                    return reject();
                });
        }
        else {
            console.error('ERROR: No command was entered: ');
            return resolve();
        }
    });
};

const getDomainsByOldValidationDate = (days, connectionStrength) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();


        let cmd = '';
        cmd = 'MATCH (domain: domain) WHERE domain.hostname IS NOT NULL AND \'2014-08-20 15:30:00\' < domain.validationDate < \'2014-08-24 15:30:00\' IS NULL' +
            ' AND toInt(domain.connectionStrength) < ' + connectionStrength +
            ' RETURN domain.hostname as hostname, domain.connectionLevel as connectionLevel, domain.connectionStrength as connectionStrength' +
            ', domain.scanDate as scanDate, domain.validationDate as validationDate, domain.expired as expired ' +
            ' ORDER BY domain.connectionStrength DESC LIMIT 5';

        if (cmd) {
            let readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(cmd);
            });

            return readTxResultPromise
                .then((results) => {
                    console.log('Done getting domains from DB');
                    session.close();
                    return resolve(results.records.map((record) => {
                        return {
                            hostname: record.get('hostname'),
                            connectionLevel: record.get('connectionLevel'),
                            connectionStrength: record.get('connectionStrength'),
                            scanDate: record.get('scanDate'),
                            validationDate: record.get('validationDate'),
                            expired: record.get('expired')
                        };
                    }));
                })
                .catch((error) => {
                    console.error('Error in getDomainsByConnectionLvlAndScanDate', error);
                    return reject();
                });
        }
        else {
            console.error('ERROR: No command was entered');
            return resolve();
        }
    });
};

const GetCompanyDomainsForValidation = (cid, minConnectionStrength) => {
    return new Promise(async (resolve, reject) => {
            const session = neo4j.session();
            let cmd = '';

            if (cid) {
                cmd = 'MATCH (company: COMPANY)-[*2..4]->(domain:domain) WHERE company.cid = \'' + cid + '\'' +
                    ' AND toInteger(domain.connectionStrength) < ' + minConnectionStrength +
                     ' RETURN domain.hostname as hostname, domain.connectionLevel as connectionLevel, domain.connectionStrength as connectionStrength' +
                    ', domain.scanDate as scanDate, domain.validationDate as validationDate LIMIT 100';

                console.log(cmd);

                if (cmd) {
                    let readTxResultPromise = session.readTransaction((transaction) => {
                        return transaction.run(cmd);
                    });
                    try {
                        let results = await readTxResultPromise;

                        console.log('Done getting domains from DB');
                        session.close();
                        return resolve(results.records.map((record) => {
                            return {
                                hostname: record.get('hostname'),
                                connectionLevel: record.get('connectionLevel'),
                                connectionStrength: record.get('connectionStrength'),
                                scanDate: record.get('scanDate'),
                                validationDate: record.get('validationDate')
                            };
                        }));
                    } catch (e) {
                        console.error('Error in GetCompanyDomains', e);
                        return reject();
                    }
                }
                else {
                    console.error('ERROR: No command was entered');
                    return resolve();
                }
            }
        }
    );
};

const cleanNulls = (arr) => {
    return arr.filter(function(el) {
        return el != null;
    });
};

const getInitialDomain = (cid) => {
    return new Promise(async (resolve, reject) => {
            const session = neo4j.session();
            let cmd = '';

            if (cid) {
                cmd = 'MATCH (company: COMPANY)-[:owner]->(domain:domain) WHERE domain.hostname <> "no_domain" AND company.cid = \'' + cid + '\'' +
                     ' RETURN domain.hostname as hostname, company.companyName as companyName LIMIT 1';

                console.log(cmd);
                if (cmd) {
                    let readTxResultPromise = session.readTransaction((transaction) => {
                        return transaction.run(cmd);
                    });
                    try {
                        let results = await readTxResultPromise;

                        console.log('Done getting domains from DB');
                        session.close();
                        return resolve(results.records.map((record) => {
                            return {
                                hostname: record.get('hostname'),
                                companyName: record.get('companyName'),
                            };
                        }));
                    } catch (e) {
                        console.error('Error in GetCompanyDomains', e);
                        return reject();
                    }
                }
                else {
                    console.error('ERROR: No command was entered');
                    return resolve();
                }
            }
        }
    );
};

module.exports = function validate() {
    const validateDomains = async (msg, respond) => {
        let domains = [];
        const validationLimit = config.consts.domainValidationScoreLimit;
        let initialData;

        try {
            //TODO: Need to support more then 1 inital domain
            //Get any of the initial domains as it is required for UI display and to relate to the company.
            initialData = await getInitialDomain(msg.id);
            domains = await GetCompanyDomainsForValidation(msg.id, 75);
            //domains = await getDomainsByConnectionStrengthAndValidationDate(80);
        } catch (e) {
            console.error(e);
            domains = await getDomainsByOldValidationDate(3, 70);
        }

        if (!domains || domains.length === 0) {
            console.log("No domains found");
        }

        let date = moment().format();
        for (let i = 0; i < domains.length; i++) {
            let isRedirect = false;
            let domain = domains[i];
            let whois;
            let score;
            let sources = [];

            if(domain && domain.hostname && initialData[0].hostname !== "no_domain" ) {
                try {
                    isRedirect = await validateRedirectURL(domain.hostname, initialData[0].companyName, initialData[0].hostname);
                } catch (e) {
                    console.log('Error validateRedirectURL: ', e);
                }
                if (isRedirect) {
                    console.log(domain.hostname, " Redirects to ", initialData[0].companyName);
                    await updateDomainConnectionStrength(domain, 95);
                    score = 95;
                } else {
                    try {
                        whois = await helpers.extendedWhois(domain, date);
                    } catch (e) {
                        console.log('Error in extended whois: ', e);
                    }

                    //Getting expiry date from whois.
                    let expiryDate = whois && whois.response && whois.response.registration.expires;
                    expiryDate = moment(expiryDate, 'YYYY-MM-DD[T]HH:mm[Z]');
                    let today = moment().format('YYYY-MM-DD[T]HH:mm[Z]');
                    let now = moment(today, 'YYYY-MM-DD[T]HH:mm[Z]');

                    //Checking if domain has expired
                    try {
                        if (isNaN(expiryDate) || expiryDate.isAfter(now)) {
                            console.log('Domain:' + domains[i].hostname + ' has not expired or is valid');
                        } else {
                            console.log('Domain:' + domains[i].hostname + ' has expired, lowering strength');
                            await updateDomainConnectionStrength(domain, 0);
                            score = 0;
                        }
                    } catch (e) {
                        console.log('Error: ', e);
                    }

                    sources = await getMultiSources(domain);
                    sources = cleanNulls(sources);
                    sources = [...new Set(sources.map(item => item.source))];
                    sources = sources.splice(sources.indexOf("reverse_by_reverse_domain"), 1);

                    if (sources.length === 0) {
                        sources.push({"source":"Validation"});
                    }

                    //If company name appears in domain it's a good validation
                    if (intelHelpers.appearsInDomainName(initialData[0].companyName, [domains[i].hostname], true)) {
                        await updateDomainConnectionStrength(domain, 70);
                        score = 70;
                    }
                    //If whois organization or registrant appear domain name
                    if (whois && whois.response && intelHelpers.appearsInDomainName(whois.response.registrant, [domains[i].hostname], true) || intelHelpers.appearsInDomainName(whois.response.organisation, [domains[i].hostname], true)) {
                        await updateDomainConnectionStrength(domain, 75);
                        score = 75;
                    }

                    //If company name appears in whois organization
                    if (whois && whois.response && intelHelpers.appearsInDomainName(initialData[0].companyName, [whois.response.registrant], true)) {
                        await updateDomainConnectionStrength(domain, 90);
                        score = 90;
                    }

                    //If company name appears in whois registrant
                    if (whois && whois.response && intelHelpers.appearsInDomainName(initialData[0].companyName, [whois.response.organisation], true)) {
                        await updateDomainConnectionStrength(domain, 90);
                        score = 90;
                    }

                    //If domain has more than 1 source it makes it's connectionStrength higher.
                    if (sources.length > 2) {
                        await updateDomainConnectionStrength(domain, 90);
                        score = 90;
                    } else if (sources.length === 2) {
                        await updateDomainConnectionStrength(domain, 80);
                        if (score < 80) {
                            score = 80;
                        }
                    }

                    await updateValidationDate(domain);

                }

                if (sources.length === 0) {
                    sources.push({"source" : "Validation"});
                }

                if (score >= validationLimit) {
                    const dataToSave = {
                        domain: initialData[0].hostname,
                        source: sources[0] && sources[0].source,
                        discoveredDomains: [domain.hostname]
                    };

                    try {
                        let result = await neoAndMongoHelpers.insertDiscoveredDomainsFindings([dataToSave]);
                        if (result && result.ok) {
                            console.log('Successfully saved new discovered domains to DB.');
                        } else {
                            console.error('Failed saving new discovered domains to DB: ', (result && result.err) ? result.err : 'couldn\'t identify the source of the error');
                        }
                    } catch (err) {
                        console.error('Error in resolveNS-insertDiscoveredDomainsFindings: ', err);
                    }
                }
            }
        }
        respond();

    };

    this.add('role:domain-validator,cmd:validate-domains', validateDomains);
};

