'use strict';
const helpers = require('./helpers.js');
const moment = require('moment');
const wayback = require('./wayback.js');
const hunter = require('./hunter.js');
const config = require('app-config');
const webspy = require('./webspy.js');
const neo4jDriver = require('neo4j-driver').v1;
const neo4jRetried = require('@ambassify/neo4j-retried');
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass), {maxTransactionRetryTime: 30000}), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});

module.exports = function discovery() {

    const findDomains = async (data, respond) => {

        let domains = await GetCompanyDomains(data.id, 1, 100);
        let companyName = await GetCompanyName(data.id);
        let date = moment().format();
        let emails;
        let initialDomains = [];

        //TODO: Should be a time limit until the next time the domain can be scanned.
        for (let i = 0; i < domains.length; i++) {
            if(!domains[i].connectionLevel && !domains[i].connectionStrength){
                console.log("Got domain :", domains[i].hostname , " without connection level and strength");
                initialDomains.push(domains[i].hostname);
            }else{
                console.log("Domain ", domains[i].hostname, "has been scanned for discovery already.");
            }
        }

        await updateDomainConnectionStrengthAndLvl(initialDomains, 100, 1);

        //let domains = await getDomainsByConnectionLvlAndScanDate(1, null);
        //let domains = await getDomainsByConnectionLvlAndScanDate(2, true, 49);
        domains = cleanNulls(domains);

        for (let i = 0; i < domains.length; i++) {
            if(domains[i].hostname !== "no_domain") {
                let connectionStrength = domains[i] && domains[i].connectionStrength;
                let connectionLevel = domains[i] && domains[i].connectionLevel;

                if (connectionStrength && connectionStrength.low) {
                    connectionStrength = connectionStrength.low
                }

                if (connectionLevel && connectionLevel.low) {
                    connectionLevel = connectionLevel.low
                }

                domains[i].connectionStrength = connectionStrength;
                domains[i].connectionLevel = connectionLevel;
                const domain = domains[i];

                try {
                    console.log("Getting URLs from page.");
                    await wayback.getUrlsFromPage(domain, date);
                } catch (e) {
                    console.log("Could not resolve urls from page: ", domain);
                }
                //await webspy.findAndSaveDomains(domain, date);
                const whois = await helpers.extendedWhois(domain, date);
                emails = await helpers.extractEmails(whois.response.rawdata, whois, domain, date);

                //Reverse whois.
                //TODO: If returned more then 200 domains, ask for permission and continue anyway.
                await helpers.reverseWhois(whois, whois.response.organisation, 'reverse_org', date, domain);
                await helpers.reverseWhois(whois, whois.response.registrant, 'reverse_registrant', date, domain);
                await helpers.reverseWhois(whois, domain.hostname, 'reverse_domain', date, domain);
                if (companyName.length > 4) {
                    await helpers.reverseWhois(whois, companyName, 'reverse_company_name', date, domain);
                }

                await helpers.asyncForEach(emails, async (email) => {
                    await helpers.reverseWhois(whois, email, 'reverse_email', date, domain);
                });

                //Reveres NS
                await helpers.reverseNS(whois.response.name_servers, date, domain);

                //Reverse MX
                await helpers.reverseMx(domain.hostname, date, domain);

                try {
                    await hunter.findDomainsByEmails(domain, date);
                }
                catch (e) {
                    console.error(e);
                }
            }
            respond();

            /*
                    //Extract Emails
                    emails = emails.concat(hunterObj.emails);

                    helpers.removeSubdomains(domainList);

                    //console.log(domainList);
                    //domainList = helpers.getUnique(domainList, "domain");

                    let ipList = [];
                    for (let i = 0; i < domainList.length; i++) {
                        ipList = ipList.concat(await helpers.lookUp(domainList[i]));
                    }

                    //Creates a unique list
                    ipList = [...new Set(ipList)];

                    domainList.concat(vt.getDomainList(ipList)); */
        }
    };

    this.add('role:domain-finder,cmd:find-domains', findDomains);
};


const getDomainsByConnectionLvlAndScanDate = (connectionLvl, scanDate, minConnectionStrength) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();
        let cmd = '';
        if (!minConnectionStrength) {
            minConnectionStrength = 49;
        }

        if (!scanDate) {
            cmd = 'MATCH (domain: domain) WHERE domain.hostname IS NOT NULL AND domain.connectionLevel = \'' + connectionLvl + '\' AND  toInteger(domain.connectionStrength) > ' + minConnectionStrength + ' AND domain.scanDate IS NULL ' +
                'RETURN domain.hostname as hostname, domain.connectionLevel as connectionLevel, domain.connectionStrength as connectionStrength' +
                ', domain.scanDate as scanDate, domain.validationDate as validationDate LIMIT 10';
        } else {
            cmd = 'MATCH (domain: domain) WHERE domain.hostname IS NOT NULL AND  domain.connectionLevel = \'' + connectionLvl + '\' AND toInteger(domain.connectionStrength) > ' + minConnectionStrength +
                ' RETURN domain.hostname as hostname, domain.connectionLevel as connectionLevel, domain.connectionStrength as connectionStrength' +
                ', domain.scanDate as scanDate, domain.validationDate as validationDate LIMIT 10';
        }
        console.log(cmd);
        if (cmd) {
            let readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(cmd);
            });

            return readTxResultPromise
                .then((results) => {
                    console.log('Done getting domains from DB');
                    session.close();
                    return resolve(results.records.map((record) => {
                        return {
                            hostname: record.get('hostname'),
                            connectionLevel: record.get('connectionLevel'),
                            connectionStrength: record.get('connectionStrength'),
                            scanDate: record.get('scanDate'),
                            validationDate: record.get('validationDate')
                        };
                    }));
                })
                .catch((error) => {
                    console.error('Error in getDomainsByConnectionLvlAndScanDate', error);
                    return reject();
                });
        }
        else {
            console.error('ERROR: No command was entered');
            return resolve();
        }
    });
};

const GetCompanyDomains = (cid, connectionLvl, minConnectionStrength) => {
    return new Promise(async (resolve, reject) => {
            const session = neo4j.session();
            let cmd = '';

            if (cid) {
                cmd = 'MATCH (company: COMPANY)-[:owner]->(domain:domain) WHERE domain.hostname <> "no_domain" AND company.cid = \'' + cid + '\'' +
                    ' AND domain.connectionLevel = ' + connectionLvl + ' AND  toInteger(domain.connectionStrength) >= ' + minConnectionStrength + ' AND domain.scanDate IS NULL ' +
                    ' OR company.cid = \'' + cid + '\' AND  domain.hostname <> "no_domain" AND domain.connectionLevel IS NULL AND domain.connectionStrength IS NULL'
                    + ' RETURN domain.hostname as hostname, domain.connectionLevel as connectionLevel, domain.connectionStrength as connectionStrength' +
                    ', domain.scanDate as scanDate, domain.validationDate as validationDate LIMIT 30';

                console.log(cmd);
                if (cmd) {
                    let readTxResultPromise = session.readTransaction((transaction) => {
                        return transaction.run(cmd);
                    });
                    try {
                        let results = await readTxResultPromise;

                        console.log('Done getting domains from DB');
                        session.close();
                        return resolve(results.records.map((record) => {
                            return {
                                hostname: record.get('hostname'),
                                connectionLevel: record.get('connectionLevel'),
                                connectionStrength: record.get('connectionStrength'),
                                scanDate: record.get('scanDate'),
                                validationDate: record.get('validationDate')
                            };
                        }));
                    } catch (e) {
                        console.error('Error in GetCompanyDomains', e);
                        return reject();
                    }
                }
                else {
                    console.error('ERROR: No command was entered');
                    return resolve();
                }
            }
        }
    );
};

const GetCompanyName = (cid) => {
    return new Promise(async (resolve, reject) => {
            const session = neo4j.session();
            let cmd = '';

            if (cid) {
                cmd = 'MATCH (company: COMPANY) WHERE company.cid = \'' + cid + '\'' +
                    ' RETURN company.companyName as companyName';

                console.log(cmd);
                if (cmd) {
                    let readTxResultPromise = session.readTransaction((transaction) => {
                        return transaction.run(cmd);
                    });
                    try {
                        let results = await readTxResultPromise;

                        console.log('Done getting domains from DB');
                        session.close();
                        if(results.records.length > 0) {
                            return resolve(results.records[0].get('companyName'))
                        }else{
                            return resolve();
                        }

                    } catch (e) {
                        console.error('Error in GetCompanyDomains', e);
                        return reject();
                    }
                }
                else {
                    console.error('ERROR: No command was entered');
                    return resolve();
                }
            }
        }
    );
};

const updateDomainConnectionStrengthAndLvl = (domainsArr, connectionStrength, connectionLvl) => {
    return new Promise((resolve, reject) => {
        const session = neo4j.session();
        console.log("Domains found for updating connection Strength: ", domainsArr);
        console.log("Searched by Connection Strength: ", connectionStrength, " and Connection Level: ", connectionLvl);
        if(domainsArr && domainsArr.length > 0 && connectionStrength && connectionLvl ) {
            const domainsArrStr = JSON.stringify(domainsArr);
            let cmd = 'MATCH (n:domain) where n.hostname IN ' + domainsArrStr +
                ' SET n += {connectionStrength :' + connectionStrength + ', connectionLevel:' + connectionLvl + '}' +
                ' RETURN n';
            if (cmd) {
                let writeTxResultPromise = session.writeTransaction((transaction) => {
                    return transaction.run(cmd);
                });

                return writeTxResultPromise
                    .then(() => {
                        console.log('Done updating connectionStrength');
                        session.close();
                        return resolve();
                    })
                    .catch((error) => {
                        console.error('Error in updateConnectionStrength', error);
                        return reject();
                    });
            }
            else {
                console.error('ERROR: No command was entered');
                return resolve();
            }
        } else{
            console.log('ERROR: missing parameters, not updating connection strength and/or connection level');
            return resolve();
        }
    });
};

const cleanNulls = (arr) => {
    return arr.filter(function(el) {
        return el != null;
    });
};
