const request = require('request-promise');
const helpers = require('./helpers.js');


let findAndSaveDomains = async (domainNode, date) => {
    let options = {
        uri: 'https://api.spyonweb.com/v1/domain/' + domainNode.hostname,
        qs: {
            access_token: "DaV5XkYH1lOy"
        },
        json: true // Automatically parses the JSON string in the response
    };

    try {
        let domainObjArr = [];
        let domainArr = [];
        let ipList = [];
        let adSenseList = [];
        let analyticsList = [];
        let connectionLvl = parseInt(domainNode.connectionLevel) + 1;

        let data = await request.get(options);
        if (data && data.result) {
            if (data.result.ip) {
                ipList = Object.keys(data.result.ip);
            }
            if (data.result.adsense) {
                adSenseList = Object.keys(data.result.adsense);
            }
            if (data.result.analytics) {
                analyticsList = Object.keys(data.result.analytics);
            }

            //Get domains from IP.
            for (let i = 0; i < ipList.length; i++) {
                domainObjArr.push(Object.keys(data.result.ip[ipList[i]].items));
            }
            for (let i = 0; i < adSenseList.length; i++) {
                domainObjArr.push(Object.keys(data.result.adsense[adSenseList[i]].items));
            }
            for (let i = 0; i < analyticsList.length; i++) {
                domainObjArr.push(Object.keys(data.result.analytics[analyticsList[i]].items));
            }
        }

        domainObjArr.forEach((domainObj) => {
            domainArr = domainArr.concat(domainObj);
        });


        for (let i = 0; i < ipList.length; i++) {
            domainObjArr.push(Object.keys(data.result.ip[ipList[i]].items))
        }
        for (let i = 0; i < adSenseList.length; i++) {
            domainObjArr.push(Object.keys(data.result.adsense[adSenseList[i]].items))
        }
        for (let i = 0; i < analyticsList.length; i++) {
            domainObjArr.push(Object.keys(data.result.analytics[analyticsList[i]].items))
        }

        domainArr = [...new Set(domainArr)];

        let relData = [];

        for (let i = 0; i < domainArr.length; i++) {
            if (typeof domainArr[i] === "string") {
                domainArr[i] = {
                    domain: domainArr[i],
                    connectionLevel: connectionLvl,
                    connectionStrength : 20,
                    scanDate: date,
                    insertDate: date,
                    validationDate: null,
                    expired: null
                };
            }

            relData.push({
                fromNodeLabelName: 'domain',
                fromNodeProps: {
                    hostname: domainNode.hostname,
                    connectionLevel: domainNode.connectionLevel,
                    scanDate: domainNode.scanDate,
                    insertDate: domainNode.insertDate,
                    connectionStrength: domainNode.connectionStrength,
                    validationDate: domainNode.validationDate,
                    expired: domainNode.expired
                },
                newNodeLabelName: "domain",
                relationshipLabel: 'webspy',
                data: date,
                toNodeLabel: 'domain',
                toNodeType: 'domain',
                toNodeProps: domainArr[i]
            });
        }
        console.log("Saving domains form webspy for: ", domainNode.hostname);
        helpers.saveToNeo(relData);
        return {domains: domainArr, source: "webspy"};

    } catch (e) {
        console.log(e);
    }
};


module.exports.findAndSaveDomains = findAndSaveDomains;
