const request = require('request-promise');
const apiKey = "d1059c84f17eba6accd3a78e706ca312d4a0251202dd5247bc502e948518ea8e";

const getIPReport = async (ip) => {
    await delay();
    let opts = {
        uri: "https://www.virustotal.com/vtapi/v2/ip-address/report?apikey=" + apiKey + "&ip=" + ip,
        json: true,
        method: 'GET'
    };
    try {
        console.log("Running getIPReport");
        let result = await request(opts);
        return result;
    } catch (e) {
        console.error("Error in getIPReport", e);
    }
};

const getDomainList = async (ipList) => {
    let report = [];
    let domains = [];
    await asyncForEach(ipList, async (ip) => {
        domains.push(await getIPReport(ip));
    });

    console.log(report);
};

function delay() {
    return new Promise(resolve => setTimeout(resolve, 15000));
}

asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

module.exports.getDomainList = getDomainList;
