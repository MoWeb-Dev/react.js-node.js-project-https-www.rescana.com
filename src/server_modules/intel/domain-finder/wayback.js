#!/usr/bin/env node
'use strict';
const moment = require('moment');
const requestP = require('request-promise');
const wayback = require('wayback-machine');
const htmlToText = require('html-to-text');
const getUrls = require('get-urls');
const request = require('request');
const helpers = require('./helpers.js');
const psl = require('psl');

const DaysToInvalidWebSite = 7;
let closest_url;
let weekAgo = moment.utc().subtract(1, 'week');

// get the closest page from wayback machine
const getClosests = function(url) {
    return new Promise(function(resolve, reject) {
        if (url.indexOf('http') === -1) {
            url = 'http://' + url;
        }

        wayback.getClosest(url, async (err, closest) => {
            if (err) {
                console.error(err);
                reject(err);
            }
            let parsedDate = moment(moment(closest.timestamp, 'YYYYMMDDhhmmss'));
            let oldness = moment().diff(parsedDate, 'days');
            console.log('Snapshot is ' + oldness + ' days old');
            console.log('Last wayback update is ' + parsedDate.toString());
            console.log('last week is ' + weekAgo.toString());
            if (parseInt(oldness) < DaysToInvalidWebSite) {
                closest_url = closest;
            }
            if (closest.url) {
                let response = await requestP(closest.url);
                console.log(response);
            }
            resolve(closest);
        });
    });
};

const getLatestOnTimeline = (url) => {
    return new Promise((resolve, reject) => {
        if (url.indexOf('http') === -1) {
            url = 'http://' + url;
        }
        wayback.getTimeline(url, async (err, timeline) => {
            if (err) {
                console.error(err);
                reject(err);
            }
            let latest = timeline.mementos[timeline.mementos.length - 1];
            console.log('Timeline for <%s>:', url);
            console.log(latest);
            if (latest) {
                try {
                    let response = await requestP(latest);
                    resolve(response);
                } catch(e){
                    console.log("Error contacting waybackpage: ", e);
                    reject(e);
                }
            } else {
                resolve();
                console.log('Could not find latest version in timeline.');
            }

        });
    });
};

const isRedirect = (domain) => {
    return new Promise((resolve) => {
        let isRedirect = false;
        let isError = false;
        if (domain) {
            const lowerDomain = domain.toLowerCase();
            const fixDomainToURL = (withSecure = true) => {
                let preURL = '';
                if (!lowerDomain.startsWith('http')) {
                    preURL = 'http';
                    if (withSecure) {
                        preURL += 's';
                    }
                    preURL += '://';
                    if (!lowerDomain.startsWith('www.')) {
                        preURL += 'www.';
                    }
                }
                return preURL + domain;
            };

            // First try URL with https.
            let domainToRequest = fixDomainToURL(true);
            request({url: domainToRequest, followRedirect: false, timeout: 1000}, function(err, res) {
                if (err) {
                    // If first try failed - try just with http.
                    domainToRequest = fixDomainToURL(false);
                    request({url: domainToRequest, followRedirect: false, timeout: 1000}, function(err, res) {
                        if (err) {
                            isError = true;
                        }
                        else if (res && res.headers && res.headers.location) {
                            isRedirect = true;
                        }
                        resolve({isError: isError, isRedirect: isRedirect, err: err});
                    });
                }
                else {
                    if (res && res.headers && res.headers.location) {
                        isRedirect = true;
                    }
                    resolve({isError: isError, isRedirect: isRedirect});
                }
            });
        }
        else {
            resolve({isError: isError, isRedirect: isRedirect});
        }
    });
};
module.exports.isRedirect = isRedirect;
module.exports.getLatestOnTimeline = getLatestOnTimeline;

module.exports.getUrlsFromPage = (domainNode, date) => {
    return new Promise(async (resolve) => {
        try {
            let cntLvl;
            if(!domainNode.connectionLevel){
                cntLvl = 0;
            }else{
                cntLvl = domainNode && domainNode.connectionLevel || domainNode.connectionLevel.low;
            }

            let connectionLvl = parseInt(cntLvl) + 1;
            let closest;
            let response;
            let res = await this.isRedirect(domainNode.hostname);
            const isRedirect = (res && res.isRedirect);
            const isError = (res && res.isError);
            if (isError) {
                console.log('Domain ', domainNode.hostname, ' failed to check if contains a redirect action: ', res.err);
            }
            else {
                console.log('Domain ', domainNode.hostname, ' found with' + ((isRedirect) ? '' : 'out'), ' a redirect action');
                if (!isRedirect) {
                    closest = await getClosests(domainNode.hostname);
                }
            }
            if (closest) {
                response = await requestP(closest.url);
            }

            if (response) {
                const urlText = htmlToText.fromString(response, {
                    wordwrap: 'null',
                    noLinkBrackets: false,
                    ignoreHref: false,
                    ignoreImage: false
                });

                if (urlText) {
                    let uniq = this.extractURL(urlText, domainNode.hostname);
                    uniq = helpers.removeSubdomains(uniq);
                    uniq = [...new Set(uniq)];
                    console.log("Removed subdomains and duplicates");
                    let relData = [];

                    for (let i = 0; i < uniq.length; i++) {
                        let foundDomain = uniq[i];
                        let obj;

                        if (typeof uniq[i] === 'string') {
                            obj = {
                                hostname: foundDomain,
                                connectionLevel: connectionLvl,
                                scanDate: date,
                                insertDate: date,
                                connectionStrength: 20,
                                validationDate: null,
                                expired: null
                            };
                        }

                        relData.push({
                            fromNodeLabelName: 'domain',
                            fromNodeProps: {
                                hostname: domainNode.hostname
                            },
                            fromMatchProp: 'hostname',
                            toMatchProp: 'hostname',
                            fromMatchPropVal: domainNode.hostname,
                            toMatchPropVal: foundDomain,
                            newNodeLabelName: 'domain',
                            relationshipLabel: 'found_url',
                            data: date,
                            toNodeLabel: 'domain',
                            toNodeType: 'domain',
                            toNodeProps: obj
                        });
                    }
                    console.log('Saving urls from page: ', domainNode.hostname);
                    for (let i = 0; i < relData.length; i++) {
                        await helpers.createNewNode(relData[i]);
                    }

                    resolve();
                }
                else {
                    const errMsg = 'Error in getUrlsFromPage: No text returned.';
                    console.error(errMsg);
                    resolve();
                }
            }
            else {
                resolve();
            }

        }
        catch (e) {
            console.error('Error in getUrlsFromPage: ', e);
            resolve();
        }
    });
};

module.exports.getRedirectDestination = async (domainNode) => {
    let closest = await getClosests(domainNode.hostname);
    if (closest) {
        return await requestP(closest.url);
    }
};

module.exports.extractURL = (text, domain) => {
    let urls = getUrls(text, {extractFromQueryString: true});
    let moreUrls = [];
    let re = /(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))/gm;
    for (let url of urls) {
        let results = url.match(re);
        for (let i = 0; i < results.length; i++) {
            if (results[i].indexOf('archive') !== -1) {
                results.splice(i, 1);
            } else {
                results[i] = psl.get(helpers.extractHostname(url));
            }
        }
        moreUrls = moreUrls.concat(results);
    }
    let uniq = [...new Set(moreUrls)];
    console.log(uniq);

    for (let i = 0; i < uniq.length; i++) {
        if (uniq[i] === domain) {
            uniq.splice(i, 1);
        }
    }

    return uniq;
};





