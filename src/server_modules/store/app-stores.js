'use strict';
let crudStoreCreator = require('./crud-store-creator');

// This module creates crud interface for stores.
module.exports = function appStores() {
    // We need to bind the function to "this" so we have the right context when running seneca.
    crudStoreCreator = crudStoreCreator.bind(this);
    // role, targetName, zone, base, name
    crudStoreCreator('publicstore', 'app', 'publicApps', 'public', 'app');
    crudStoreCreator('surveystore', 'survey', 'surveys', 'survey', 'map');
    crudStoreCreator('surveystore', 'surveyAnswers', 'surveys', 'surveyAnswers', 'map');
    crudStoreCreator('customerstore', 'app', 'tenants', 'customer', 'app');
    crudStoreCreator('processMap', 'process', 'tenants', 'process', 'map');
    crudStoreCreator('userdata', 'user', 'tenants', 'sys', 'user');
    crudStoreCreator('pastes', 'paste', 'tenants', 'bin', 'paste');
    crudStoreCreator('pasteKeywords', 'keywords', 'tenants', 'paste', 'keywords');
    crudStoreCreator('serviceConfig', 'settings', 'serviceConfig', 'services', 'settings');
    crudStoreCreator('projects', 'project', 'projects', 'project', 'map');
    crudStoreCreator('companies', 'company', 'companies', 'company', 'map');
    crudStoreCreator('scans', 'scan', 'scans', 'scan', 'map');
    crudStoreCreator('intelAlerts', 'intelAlert', 'intelAlerts', 'intelAlert', 'map');
    crudStoreCreator('surveyFilesStore', 'surveyFile', 'surveyFiles', 'surveyFile', 'map');
    crudStoreCreator('fileScans', 'fileScan', 'fileScans', 'fileScans', 'map');
    crudStoreCreator('botnetFeeds', 'botnetFeed', 'botnetFeeds', 'botnetFeeds', 'map');
    crudStoreCreator('sslCerts', 'sslCert', 'sslCerts', 'sslCerts', 'map');
    crudStoreCreator('gdprs', 'gdpr', 'gdpr', 'gdpr', 'map');
    crudStoreCreator('discoveredDomains', 'discoveredDomain', 'discoveredDomain', 'discoveredDomain', 'map');
};
