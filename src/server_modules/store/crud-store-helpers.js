module.exports.whoIsRelatedByKeyword = (companies, keyword) =>{
    const relatedCompanies = [];
    if (companies && keyword) {
        companies.map((company) => {
            if (company.keywords && company.keywords.indexOf(keyword) !== -1) {
                relatedCompanies.push(company.companyName);
            }
        });
    }
    return (relatedCompanies);
};

module.exports.countSurveyAnswers = (surveyAnswer) =>{
    const counts = {
        complyCount: 0,
        nonComplyCount: 0,
        otherCount: 0,
        NACount: 0
    };
    if (surveyAnswer && surveyAnswer.pages && Array.isArray(surveyAnswer.pages)) {
        surveyAnswer.pages.map((page) => {
            if (page && page.elements && Array.isArray(page.elements)) {
                page.elements.map((category) => {
                    if (category &&
                        (!category.followUpCategoryData || (category.followUpCategoryData && category.followUpCategoryData.isShown))) {
                        if (category && category.elements && Array.isArray(category.elements)) {
                            category.elements.map((question) => {
                                if (question) {
                                    if (!question.answer) {
                                        counts.NACount++;
                                    } else {
                                        switch (question.answer) {
                                            case 'Comply': {
                                                counts.complyCount++;
                                                break;
                                            }
                                            case 'Non Comply': {
                                                counts.nonComplyCount++;
                                                break;
                                            }
                                            default: {
                                                counts.otherCount++;
                                                break;
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }
    return (counts);
};
