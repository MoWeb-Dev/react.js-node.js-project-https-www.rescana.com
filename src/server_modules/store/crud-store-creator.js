'use strict';
const config = require('app-config');
const Promise = require('bluebird');
const consts = require('../../../config/consts');
const helpers = require('./crud-store-helpers');
const {calculateTotalScore} = require('../reports/common.js');
const moment = require('moment');
const mongo = require('mongodb');
const {WILDFIRE_URL} = require('../../../config/consts');
const {WILDFIRE_STATUS_TYPES} = require('../../server/api/api-helpers');
const createWildfireClient = require('wildfire-client').create;
const wildfireClient = createWildfireClient({
    apikey: config.consts.wildFireAPI, // config.consts.WF_API,
    baseUrl: WILDFIRE_URL, // https://wildfire.paloaltonetworks.com/publicapi/
    json: true // api is xml only, client library converts the xml to json using xml2js
});

module.exports = function crudStoreCreator(role, targetName, zone, base, name) {
    const act = Promise.promisify(this.act, {context: this});

    const INTEL_ALERTS_COLLECTION = 'intelAlert_map';
    const PROJECT_COLLECTION = 'project_map';
    const COMPANY_COLLECTION = 'company_map';
    const SURVEY_ANSWERS_COLLECTION = 'surveyAnswers_map';
    const USERS_COLLECTION = 'sys_user';
    const BLACKLIST_COLLECTION = 'blacklist_map';
    const FILE_SCANS_COLLECTION = 'fileScans_map';
    const BOTNET_RSS_FEEDS_COLLECTION = 'botnetFeeds_map';
    const SSL_CERTIFICATES_COLLECTION = 'sslCerts_map';
    const GDPR_COMPLIANCE_COLLECTION = 'gdpr_map';
    const PASTEBIN_COLLECTION = 'bin_paste';
    const DISCOVERED_DOMAINS_COLLECTION = 'discoveredDomains_map';

    // basic crud operations for public apps
    this.add('role:' + role + ', get:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        console.log('Getting: ', targetName);
        app_entity.load$(msg.id, function(err, app) {
            if (err) {
                console.log(err);
                return err;
            }
            console.log('Returning: ', app);
            respond(null, app);
        });
    });

    this.add('role:' + role + ', set:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        app_entity.data$(msg.data);
        app_entity.save$(function(err, app) {
            if (err) {
                respond(null, err);
            }
            respond(null, {id: app.id});
        });
    });

    // Role should be 'surveystore' and targetName should be 'survey': role:surveystore, getSurveyBySid:survey
    this.add('role:' + role + ', getSurveyBySid:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        console.log('Getting: ', targetName);
        app_entity.load$({sid: msg.sid}, function(err, app) {
            if (err) {
                console.log(err);
                return err;
            }
            console.log('Returning: ', app);
            respond(null, app);
        });
    });

    // Set as above - just for an array of objects.
    this.add('role:' + role + ', setIntelFromArray:' + targetName, (msg, respond) => {
        if (msg.data && Array.isArray(msg.data) && msg.data.length > 0) {
            const app_entity = this.make(zone, base, name);

            app_entity.native$((err, db) => {
                const collection = db.collection(INTEL_ALERTS_COLLECTION);

                msg.data.map((currEntity) => {
                    collection.findAndModify(
                        currEntity, /* query.*/
                        [['_id', 'asc']], /* sort.*/
                        currEntity, /* update.*/
                        {upsert: true}, /* options.*/
                        (err) => {/* callback.*/
                            if (err) {
                                respond(null, {err: err});
                            }
                        });
                });
                respond(null, {ok: true});
            });
        } else {
            respond(null, {err: 'Invalid params for setIntelFromArray.'});
        }
    });

    // Get all intel stored in DB by type and dates.
    this.add('role:' + role + ', getIntelFound:' + targetName, (msg, respond) => {
        if (msg.data && msg.data.intelType && msg.data.collectUntil && msg.data.collectFrom) {
            const app_entity = this.make(zone, base, name);

            app_entity.native$((err, db) => {
                const collection = db.collection(INTEL_ALERTS_COLLECTION);

                collection.find({
                    'intelType': msg.data.intelType,
                    'resolve_time': {
                        $lte: msg.data.collectUntil,
                        $gte: msg.data.collectFrom
                    }
                }, (err, res) => {
                    if (err) {
                        respond(null, {err: err});
                    }
                    if (res) {
                        res.toArray((err, docs) => {
                            if (docs && docs.length > 0) {
                                respond(null, {data: docs});
                            } else {
                                respond(null, {err: 'No intel results returned.'});
                            }
                        });
                    } else {
                        respond(null, {err: 'Unknown error while getting intel results.'});
                    }
                });
            });
        } else {
            respond(null, {err: 'Invalid params for getIntelFound.'});
        }
    });

    // Remove the intel findings that were processed already.
    this.add('role:' + role + ', deleteIntelFound:' + targetName, (msg, respond) => {
        if (msg.data && msg.data.intelType && msg.data.collectUntil && msg.data.collectFrom) {
            const app_entity = this.make(zone, base, name);

            app_entity.native$((err, db) => {
                const collection = db.collection(INTEL_ALERTS_COLLECTION);

                collection.deleteMany({
                    'intelType': msg.data.intelType,
                    'resolve_time': {
                        $lte: msg.data.collectUntil,
                        $gte: msg.data.collectFrom
                    }
                }, (err) => {
                    if (err) {
                        respond(null, {err: err});
                    }
                    respond(null, {ok: true});
                });
            });
        } else {
            respond(null, {err: 'Invalid params for deleteIntelFound.'});
        }
    });

    // Get the allowed users' emails if sendIntelAlert is selected.
    this.add('role:' + role + ', getUsersWithAlertsByDomains:' + targetName, (msg, respond) => {
        if (msg.data) {
            const app_entity = this.make(zone, base, name);

            app_entity.native$((err, db) => {
                let collection = db.collection(COMPANY_COLLECTION);

                const domains = msg.data;

                collection.find({
                    'selectedDomains': {
                        $in: domains
                    }
                }, (err, res) => {
                    if (err) {
                        respond(null, {err: err});
                    }
                    if (res) {
                        res.toArray((err, docs) => {
                            if (err) {
                                respond(null, {err: err});
                            }
                            if (docs && docs.length > 0) {
                                const usersMongoIDs = [];
                                const companies = docs;
                                companies.map((currCompany) => {
                                    const allowedUsers = currCompany.allowedUsers;
                                    if (allowedUsers) {
                                        allowedUsers.map((currCompanyUser) => {
                                            const currID = new mongo.ObjectID(currCompanyUser.id);
                                            let isExist = false;
                                            for (let i = 0; i < usersMongoIDs.length; i++) {
                                                if (usersMongoIDs[i].equals(currID)) {
                                                    isExist = true;
                                                    break;
                                                }
                                            }
                                            if (!isExist) {
                                                usersMongoIDs.push(currID);
                                            }
                                        });
                                    }
                                });

                                if (usersMongoIDs.length > 0) {
                                    collection = db.collection(USERS_COLLECTION);

                                    collection.find({

                                        '_id': {
                                            $in: usersMongoIDs
                                        },
                                        'alertSystemConfig.sendIntelAlerts': true
                                    }, (err, res) => {
                                        if (err) {
                                            respond(null, {err: err});
                                        }
                                        if (res) {
                                            res.toArray((err, docs) => {
                                                if (err) {
                                                    respond(null, {err: err});
                                                }
                                                if (docs && docs.length > 0) {
                                                    const users = [];
                                                    // Duplicate of IDs is saved to reduce iterating on users.
                                                    const duplicateUsersIDs = [];
                                                    docs.map((currUser) => {
                                                        const userObj = {};
                                                        userObj.id = currUser._id.toString();
                                                        if (duplicateUsersIDs.indexOf(userObj.id) === -1) {
                                                            userObj.email = (currUser.email === 'admin@admin.com') ? 'yuval@rescana.com' : currUser.email;
                                                            userObj.name = currUser.name;
                                                            const currUserCompanies = companies.filter((currCompany) =>
                                                                currCompany.allowedUsers.find((allowedUsr) => allowedUsr.id === userObj.id));
                                                            const currUserDomains = [];
                                                            currUserCompanies.map((currComp) => {
                                                                const currCompanyDomains = currComp.selectedDomains.filter((currDomain) =>
                                                                    domains.indexOf(currDomain) > -1);
                                                                currCompanyDomains.map((currDomain) => {
                                                                    const currCompanyName = currComp.companyName;
                                                                    let isNewDomain = true;
                                                                    for (let i = 0; i < currUserDomains.length; i++) {
                                                                        if (currUserDomains[i].domain === currDomain) {
                                                                            isNewDomain = false;
                                                                            // This 'if' is separated because it is related to a separate check - excludeCompaniesFromIntelAlerts.
                                                                            // This 'if' checks whether the user configured his settings to exclude this company's domains or not.
                                                                            if (isUserConfiguredToThisIntelAlert(currUser.alertSystemConfig, currComp._id)) {
                                                                                currUserDomains[i].relatedCompanies.push(currCompanyName);
                                                                            }
                                                                            break;
                                                                        }
                                                                    }
                                                                    if (isNewDomain) {
                                                                        // This 'if' is separated because it is related to a separate check - excludeCompaniesFromIntelAlerts.
                                                                        // This 'if' checks whether the user configured his settings to exclude this company's domains or not.
                                                                        if (isUserConfiguredToThisIntelAlert(currUser.alertSystemConfig, currComp._id)) {
                                                                            currUserDomains.push({
                                                                                domain: currDomain,
                                                                                relatedCompanies: [currCompanyName]
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                            });
                                                            userObj.domains = currUserDomains;

                                                            duplicateUsersIDs.push(userObj.id);
                                                            users.push(userObj);
                                                        }
                                                    });
                                                    respond(null, {data: users});
                                                } else {
                                                    respond(null, {err: 'No users asking for alerts found.'});
                                                }
                                            });
                                        } else {
                                            respond(null, {err: 'Unknown error while getting users while collecting intel.'});
                                        }
                                    });
                                } else {
                                    respond(null, {err: 'No matching users found.'});
                                }
                            } else {
                                respond(null, {err: 'No matching domains found.'});
                            }
                        });
                    } else {
                        respond(null, {err: 'Unknown error while getting domains while collecting intel.'});
                    }
                });
            });
        } else {
            respond(null, {err: 'Invalid params for getIntelFound.'});
        }
    });
    // This function checks whether the user configured his settings to exclude this company's domains or not.
    function isUserConfiguredToThisIntelAlert(alertSystemConfig, companyID) {
        return (!(alertSystemConfig && alertSystemConfig.excludedCompaniesIntelAlert &&
            alertSystemConfig.excludedCompaniesIntelAlert.length > 0 &&
            alertSystemConfig.excludedCompaniesIntelAlert.some((c) => {
                const currExcludedCompanyID = new mongo.ObjectID(c.id);
                return currExcludedCompanyID.equals(companyID);
            })));
    }

    // Update last login attempt by user's email and check if his lockout expired.
    this.add('role:user, cmd:updateUserLastLoginTry', (msg, respond) => {
        if (msg.data) {
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
            native$()
                .then((db) => {
                    const collection = db.collection(USERS_COLLECTION);

                    // First, check if the user exists in order to know his failedLoginCount.
                    return [collection.find({
                        'email': {
                            $eq: msg.data
                        }
                    }), collection, msg.data];
                })
                .spread((res, collection, userEmail) => {
                    if (res) {
                        return [res.toArray(), collection, userEmail];
                    } else {
                        return [[], collection, userEmail];
                    }
                })
                .spread((docs, collection, userEmail) => {
                    if (docs && docs.length > 0) {
                        const user = docs[0];
                        const MS_PER_MINUTE = 60000;
                        const LOCKOUT_MINUTES = 5;
                        const find = {
                            'email': {
                                $eq: userEmail
                            }
                        };
                        let update;
                        // Check if lockout time is over or isAdmin- if so, reset the failedLoginCount.
                        const lastLockoutMinutes = new Date(new Date() - LOCKOUT_MINUTES * MS_PER_MINUTE);
                        if (user && (
                            (user.userRole && user.userRole === 'admin') ||
                            (user.lastLoginTry && new Date(user.lastLoginTry) <= lastLockoutMinutes))) {
                            update = {'lastLoginTry': new Date().toISOString(), 'failedLoginCount': 0};
                        } else {
                            update = {'lastLoginTry': new Date().toISOString()};
                        }
                        return collection.updateOne(find, {
                            $set: update
                        });
                    } else {
                        respond(null, {ok: false, why: 'user-not-found'});
                        return Promise.resolve({stop: true});
                    }
                })
                .then((obj) => {
                    if (!obj.stop) {
                        respond(null, {ok: true});
                    }
                })
                .catch((err) => {
                    respond(null, {err: err});
                });
        } else {
            respond(null, {err: 'Invalid params for updateUserLastLoginTry.'});
        }
    });

    // or curl -d '{"role":"publicstore", "update":"app", "data": {"dataArr":[{"id":"xxxxxx", "url":"http://kaka.co.il"}, {"id":"xxxxxx", "url":"http://kaka2.co.il"}]}}' http://localhost:10904/act
    this.add('role:' + role + ', update:' + targetName, (msg, respond) => {
        const resultArr = [];
        const promiseArr = [];

        if (msg.data.dataArr && Array.isArray(msg.data.dataArr)) {
            for (let i = 0; i < msg.data.dataArr.length; i++) {
                const prom = act('role:' + role + ', get:' + targetName, {id: msg.data.dataArr[i].id}).then((app) => {
                    if (app) {
                        app.data$(msg.data.dataArr[i]);
                        const save$ = Promise.promisify(app.save$, {context: app});
                        return save$().then((app) => {
                            resultArr.push({id: app.id});
                        });
                    } else {
                        const app_entity = this.make(zone, base, name);
                        app_entity.data$(msg.data.dataArr[i]);
                        app_entity.save$(function(err, app) {
                            if (err) {
                                respond(null, err);
                            }
                            resultArr.push({id: app.id});
                        });
                    }
                });
                promiseArr.push(prom);
            }
            Promise.all(promiseArr).then(() => {
                respond(null, {idArr: resultArr});
            });
        } else {
            this.act('role:' + role + ', get:' + targetName, {id: msg.data.id}, (err, app) => {
                if (app) {
                    app.data$(msg.data);
                    app.save$((err, app) => {
                        if (err) {
                            respond(null, err);
                        }
                        respond(null, {id: app.id});
                    });
                } else {
                    const app_entity = this.make(zone, base, name);
                    app_entity.data$(msg.data);
                    app_entity.save$((err, app) => {
                        if (err) {
                            respond(null, err);
                        }
                        respond(null, {id: app.id});
                    });
                }
            });
        }
    });

    this.add('role:' + role + ', insertBotnetFeedsFindings:' + targetName, (msg, respond) => {
        if (msg && msg.data && Array.isArray(msg.data)) {
            const app_entity = this.make(zone, base, name);
            console.log('starting query');
            app_entity.native$((err, db) => {
                const collection = db.collection(BOTNET_RSS_FEEDS_COLLECTION);
                const allUpdates = [];
                msg.data.map((currData) => {
                    if (currData && currData.contentIdentifier && currData.feedName) {
                        // Update if contentIdentifier and feedName matches, otherwise insert new data.
                        allUpdates.push(collection.update({contentIdentifier: currData.contentIdentifier, feedName: currData.feedName}, currData, {upsert: true}));
                    }
                });

                return Promise.all(allUpdates)
                    .then(() => {
                        respond(null, {ok: true});
                    }).catch((e) => {
                        console.log('Error saving Botnet Feeds Findings: ', e);
                        respond(null, {ok: false});
                    });
            });
        } else {
            const logMsg = 'Invalid params for insertBotnetFeedsFindings.';
            console.log(logMsg);
            respond(null, {ok: false, err: logMsg});
        }
    });

    this.add('role:' + role + ', insertSSLCertificatesFindings:' + targetName, (msg, respond) => {
        if (msg && msg.data && Array.isArray(msg.data)) {
            const app_entity = this.make(zone, base, name);
            console.log('starting query');
            app_entity.native$((err, db) => {
                const collection = db.collection(SSL_CERTIFICATES_COLLECTION);
                const allUpdates = [];
                msg.data.map((currData) => {
                    if (currData && currData.domain) {
                        currData.lastUpdated = moment().format();
                        // Update if domain fields matches, otherwise insert new data.
                        allUpdates.push(collection.update({domain: currData.domain}, currData, {upsert: true}));
                    }
                });

                return Promise.all(allUpdates)
                    .then(() => {
                        respond(null, {ok: true});
                    }).catch((e) => {
                        console.log('Error saving SSL Certificates Findings: ', e);
                        respond(null, {ok: false});
                    });
            });
        } else {
            const logMsg = 'Invalid params for insertSSLCertificatesFindings.';
            console.log(logMsg);
            respond(null, {ok: false, err: logMsg});
        }
    });

    this.add('role:' + role + ', insertGDPRFindings:' + targetName, (msg, respond) => {
        if (msg && msg.data && Array.isArray(msg.data)) {
            const app_entity = this.make(zone, base, name);
            console.log('starting query');
            app_entity.native$((err, db) => {
                const collection = db.collection(GDPR_COMPLIANCE_COLLECTION);
                const allUpdates = [];
                msg.data.map((currData) => {
                    if (currData && currData.domain) {
                        currData.lastUpdated = moment().format();
                        // Update if domain fields matches, otherwise insert new data.
                        allUpdates.push(collection.update({domain: currData.domain}, currData, {upsert: true}));
                    }
                });

                return Promise.all(allUpdates)
                    .then(() => {
                        respond(null, {ok: true});
                    }).catch((e) => {
                        console.log('Error saving GDPR Findings: ', e);
                        respond(null, {ok: false});
                    });
            });
        } else {
            const logMsg = 'Invalid params for insertGDPRFindings.';
            console.log(logMsg);
            respond(null, {ok: false, err: logMsg});
        }
    });

    this.add('role:' + role + ', getBotnetFeedsFindings:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        console.log('starting query');
        app_entity.native$((err, db) => {
            const collection = db.collection(BOTNET_RSS_FEEDS_COLLECTION);
            const findBotnetsQuery = {};
            // Filter results by dates if exist.
            if (msg && msg.startDate && msg.endDate) {
                findBotnetsQuery['createDate'] = {
                    $gte: msg.startDate,
                    $lte: msg.endDate
                };
            }
            collection.find(findBotnetsQuery, (err, res) => {
                if (res) {
                    res.toArray((err, docs) => {
                        console.log('Got ', (docs && Array.isArray(docs)) ? docs.length: 'null', 'records');
                        respond(null, docs || []);
                    });
                } else {
                    console.log('Query returned an empty result');
                    respond(null, []);
                }
            });
        });
    });

    this.add('role:' + role + ', insertPhishtankSites:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        console.log('In insertPhishtankSites - starting query');
        app_entity.native$((err, db) => {
            const collection = db.collection('phishtank');
            // We need to get only online websites - we have no use in old websites.
            collection.drop(() => {
                collection.insertMany(msg.apps, (err, list) => {
                    const result = {ok: false};
                    if (list) {
                        result.ok = true;
                        let logMsg = 'Inserted Phishing sites records';
                        if (list && list.hasOwnProperty('insertedCount')) {
                            logMsg += ': ' + list.insertedCount;
                        } else {
                            logMsg += ' successfully';
                        }
                        console.log(logMsg);
                    } else if (err) {
                        result.err = err;
                        console.error('Error in insertPhishtankSites: ', err);
                    }
                    respond(result);
                });
            });
        });
    });

    this.add('role:' + role + ', softRemove:' + targetName, (msg, respond) => {
        console.log('softRemove app:', msg.aid);
        this.act('role:' + role + ', list:' + targetName, {query: {aid: msg.aid}}, (err, apps) => {
            console.log('apps are :', apps);
            for (const app of apps) {
                console.log('***** app is :', app);
                for (let i = 0; i < app.uid.length; i++) {
                    if (app.uid[i] === msg.uid) {
                        app.uid.splice(i, 1);
                    }
                }
                for (let i = 0; i < app.pid.length; i++) {
                    if (app.pid[i] === msg.pid) {
                        app.pid.splice(i, 1);
                    }
                }
                app.save$(() => {
                    console.log('Saved after delete');
                    respond(null, {id: app.uid});
                });
            }
        });
    });

    this.add('role:' + role + ', remove:' + targetName, (msg, respond) => {
        console.log('remove app:', msg.id);
        const app_entity = this.make(zone, base, name);
        app_entity.remove$(msg.id, (err, res) => {
            if (err) {
                respond(null, err);
            }

            console.log('Record removed: ', res);
            respond();
        });
    });

    // For deleteSurvey
    this.add('role:' + role + ', deleteBySid:' + targetName, (msg, respond) => {
        this.act('role:' + role + ', list:' + targetName, {query: {sid: msg.query.sid}}, (err, apps) => {
            if (apps[0]) {
                console.log(apps[0]);
                apps[0].remove$((err, res) => {
                    if (err) {
                        respond(null, err);
                    }
                    console.log('Record removed: ', res);
                    respond(null, res);
                });
            } else {
                respond();
            }
        });
    });

    // For deleteSurveyAnswers
    this.add('role:' + role + ', deleteBySaid:' + targetName, (msg, respond) => {
        this.act('role:' + role + ', list:' + targetName, {query: {said: msg.query.said}}, (err, apps) => {
            if (apps[0]) {
                console.log(apps[0]);
                apps[0].remove$((err, res) => {
                    if (err) {
                        respond(null, err);
                    }
                    console.log('Record removed: ', res);
                    respond(null, res);
                });
            } else {
                respond();
            }
        });
    });
    // list apps (msg should contain query - {} is all - more@ http://senecajs.org/tutorials/understanding-query-syntax.html)
    // Returns array.
    this.add('role:' + role + ', list:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        app_entity.list$(msg.query, function(err, res) {
            if (err) {
                respond(null, err);
            }
            respond(null, res);
        });
    });

    // list apps (msg should contain query - {ObjectIDs} to return matching apps)
    // Returns array.
    this.add('role:' + role + ', listByObjectID:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const query = {};
        console.log('In crud - listByObjectID: ');
        console.log(msg.query);
        if (msg.query && Array.isArray(msg.query)) {
            const allObjectIDs = msg.query.map((currID) => {
                return new mongo.ObjectID(currID);
            });
            query._id = {$in: allObjectIDs};
        }
        app_entity.list$(query, function(err, res) {
            if (err) {
                respond(null, err);
            }
            respond(null, res);
        });
    });

    // getStaleApps
    this.add('role:' + role + ', stalelist:' + targetName, (msg, respond) => {
        console.log('in stalelist');
        const app_entity = this.make(zone, base, name);
        let foundStaleResults = false;
        // Getting apps by last modified (ascending).
        app_entity.list$({}, function(err, res) {
            const staleResults = [];
            if (err) {
                respond(null, err);
            }

            for (let i = 0; i < res.length; i++) {
                const now = new Date().getTime();
                if (!res[i].lastModified || parseInt(res[i].lastModified) < now - parseInt(msg.timestamp)) {
                    staleResults.push(res[i]);
                    foundStaleResults = true;
                }
            }

            console.log('Found ' + staleResults.length + ' stale results.');
            respond(null, {data: staleResults});
        });
    });

    // getDnsTwistStaleApps
    this.add('role:' + role + ', getOldestDnsTwistCustomerApps:' + targetName, (msg, respond) => {
        console.log('in getOldestDnsTwistCustomerApp');
        const app_entity = this.make(zone, base, name);
        let oldestApp;
        app_entity.list$({sort$: {dnsTwistTimeStamp: 1}}, (err, res) => {
            if (err) {
                respond(null, err);
            }
            if (res) {
                console.log('Got a result..');
                // If no dnsTwistTimeStamp exists, initialize.
                for (let k = 0; k < res.length; k++) {
                    if (!res[k].markedForDnsTwist) {
                        console.log('Found  the oldest app that wasn\'t marked for dnsTwist');
                        oldestApp = res[k];
                        break;
                    }
                }

                console.log('Returning:', oldestApp);
                respond(null, {data: oldestApp});
            } else {
                respond();
            }
        });
    });

    // Return what companies this user is allowed to see.
    this.add('role:' + role + ', getAllowedDomains:' + targetName, (msg, respond) => {
        this.act('role:userdata, get:user', {id: msg.data.id}, (err, user) => {
            const getCompaniesByUserQuery = {allowedUsers: {$eq: user.email}};
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

            return native$()
                .then((db) => {
                    const collection = db.collection(COMPANY_COLLECTION);
                    const find = Promise.promisify(collection.find, {context: collection});
                    return find(getCompaniesByUserQuery);
                })
                .then((list) => {
                    console.log('Found records');
                    list.toArray((err, docs) => {
                        let domains = [];
                        for (let i = 0; i < docs.length; i++) {
                            domains = domains.concat(docs[i].selectedDomains);
                        }
                        respond(null, domains);
                    });
                })
                .catch((e) => {
                    console.log('Error getAllowedDomains: ', e);
                });
        });
    });

    // Return what companies this user is allowed to see.
    this.add('role:' + role + ', getAllowedCompanies:' + targetName, (msg, respond) => {
        this.act('role:userdata, get:user', {id: msg.data.id}, (err, user) => {
            const getCompaniesByUserQuery = {'allowedUsers': {$elemMatch: {id: {$eq: user.id}}}};
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

            return native$()
                .then((db) => {
                    const collection = db.collection(COMPANY_COLLECTION);
                    const find = Promise.promisify(collection.find, {context: collection});
                    return find(getCompaniesByUserQuery);
                })
                .then((list) => {
                    console.log('Found records');
                    list.toArray((err, docs) => {
                        respond(null, docs);
                    });
                })
                .catch((e) => {
                    console.log('Error getAllowedDomains: ', e);
                });
        });
    });

    // Return what companies this user is allowed to see.
    this.add('role:' + role + ', getAllowedCompaniesByProject:' + targetName, (msg, respond) => {
        this.act('role:userdata, get:user', {id: msg.data.id}, (err, user) => {
            const getCompaniesByProjectsQuery = {
                'allowedUsers': {$elemMatch: {id: {$eq: user.id}}},
                'projectName': {$eq: msg.data.projectName}
            };
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

            return native$()
                .then((db) => {
                    const collection = db.collection('project_map');
                    const find = Promise.promisify(collection.find, {context: collection});
                    return find(getCompaniesByProjectsQuery);
                })
                .then((list) => {
                    console.log('Found records');
                    list.toArray((err, docs) => {
                        const companies = [];
                        for (let i = 0; i < docs.length; i++) {
                            for (let j = 0; j < docs[i].selectedCompanies.length; j++) {
                                companies.push(docs[i].selectedCompanies[j].id);
                            }
                        }
                        respond(null, uniq(companies));
                    });
                })
                .catch((e) => {
                    console.log('Error getAllowedCompaniesByProject: ', e);
                });
        });
    });

    const uniq = (arrArg) => {
        return arrArg.filter((elem, pos, arr) => {
            return arr.indexOf(elem) === pos;
        });
    };

    // Return latest scans
    this.add('role:' + role + ', getLatestScans:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        const company = msg.company;

        const getLatestScanQuery = {company: {$eq: company}};

        return native$()
            .then((db) => {
                const collection = db.collection('scan_map');
                const find = Promise.promisify(collection.find, {context: collection});
                return find(getLatestScanQuery);
            })
            .then((list) => {
                list = list.sort({'date': -1}).limit(2);
                console.log('Found records');
                list.toArray((err, docs) => {
                    respond(null, docs);
                });
            })
            .catch((e) => {
                console.log('Error getLatestScans: ', e);
            });
    });

    /**
     * Find phished url by brand name
     */
    this.add('role:' + role + ', listFromPhish:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        const getDomainsByBrand = {'url': {$regex: '.*' + msg.query.brandName + '.*'}};

        native$()
            .then((db) => {
                const collection = db.collection('phishtank');
                const find = Promise.promisify(collection.find, {context: collection});
                return find(getDomainsByBrand);
            })
            .then((list) => {
                return listToArray(list);
            })
            .then((results) => {
                for (let i = 0; i < results.length; i++) {
                    results[i].aid = msg.query.aid;
                }
                respond(results);
            });
    });

    /**
     * Set survey language by sid.
     */
    this.add('role:' + role + ', UpdateSurveyLang:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        console.log(msg.sid + ' === ' + msg.lang);
        native$()
            .then((db) => {
                const collection = db.collection('survey_map');
                const update = Promise.promisify(collection.update, {context: collection});
                return update({sid: msg.sid},
                    {
                        $set: {lang: msg.lang}
                    });
            })
            .then(() => {
                respond(null, {});
            });
    });

    /**
     * Return what dataleaks these keywords have.
     */
    this.add('role:' + role + ', getDataleaks:' + targetName, (msg, respond) => {
        this.act('role:pastes, get:paste', {}, () => {
            const getPastebinsByKeywordsQuery = {
                keyword: {$in: msg.query.companiesKeywords},
                hideFrom: {$ne: msg.query.uid}
            };
            if (msg.query.lastMonth) {
                const lastMonth = moment().subtract(1, 'months');
                // turn lastMonth to moment since the dataleaks are saved in moment format.
                getPastebinsByKeywordsQuery.time = {$gte: lastMonth.format()};
            }
            const companies = msg.query.companies;
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

            return native$()
                .then((db) => {
                    const collection = db.collection(PASTEBIN_COLLECTION);
                    const find = Promise.promisify(collection.find, {context: collection});
                    return find(getPastebinsByKeywordsQuery);
                })
                .then((list) => {
                    console.log('Found records');
                    const result = [];
                    list.toArray((err, docs) => {
                        docs.map((entity) => {
                            entity.source = consts.DATALEAKS_PASTEBIN;
                            entity.relatedCompanies = helpers.whoIsRelatedByKeyword(companies, entity.keyword);
                            result.push(entity);
                        });
                        respond(null, result);
                    });
                })
                .catch((e) => {
                    console.log('Error getDataleaks: ', e);
                });
        });
    });

    /**
     * Return how many dataleaks this company have.
     */
    this.add('role:' + role + ', getDataleaksCountByCompanyID:' + targetName, (msg, respond) => {
        this.act('role:companies, get:company', {id: msg.companyID}, (err, company) => {
            if (err) {
                console.log('Error in getDataleaksCountByCompanyID: Failed to fetch company data by id', msg.companyID + ': ' + err);
                respond();
            } else {
                this.act('role:pastes, get:paste', {}, () => {
                    let getPastebinsByKeywordsQuery = {
                        keyword: {$in: company.keywords}
                    };
                    if(msg.uid) {
                        getPastebinsByKeywordsQuery.hideFrom = {$ne: msg.uid}
                    }
                    if (msg.lastMonth) {
                        const lastMonth = moment().subtract(1, 'months');
                        // turn lastMonth to moment since the dataleaks are saved in moment format.
                        getPastebinsByKeywordsQuery.time = {$gte: lastMonth.format()};
                    }
                    const app_entity = this.make(zone, base, name);
                    const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

                    return native$()
                        .then((db) => {
                            const collection = db.collection(PASTEBIN_COLLECTION);
                            const find = Promise.promisify(collection.find, {context: collection});
                            return find(getPastebinsByKeywordsQuery);
                        })
                        .then((list) => {
                            console.log('Found records');
                            list.toArray((err, docs) => {
                                if (docs) {
                                    respond(null, {countDataleaks: docs.length, dataleaksData: docs});
                                } else {
                                    respond(null, {countDataleaks: 0, dataleaksData: []});
                                }
                            });
                        })
                        .catch((e) => {
                            console.log('Error in getDataleaks: ', e);
                            respond();
                        });
                });
            }
        });
    });

    /**
     * Set a Pastebin finding as hidden from user.
     */
    this.add('role:' + role + ', DeletePastebinFromUser:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        native$()
            .then((db) => {
                const collection = db.collection(PASTEBIN_COLLECTION);
                const update = Promise.promisify(collection.update, {context: collection});
                return update({lid: msg.lid},
                    {
                        $push: {hideFrom: msg.uid}
                    });
            })
            .then(() => {
                respond(null, {});
            });
    });

    /**
     * Set a DiscoveredDomain finding as hidden from company.
     */
    this.add('role:' + role + ', DeleteDiscoveredDomainFromCompany:' + targetName, async (msg, respond) => {
        if(msg && msg.companyID && msg.originDomain && msg.originSource && msg.discoveredDomainToDelete) {
            console.log('In DeleteDiscoveredDomainFromCompany');
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
            let exceptionSource = 'native$ initialization';
            try {
                const db = await native$();
                const companyCollection = db.collection(COMPANY_COLLECTION);
                const updateCompany = Promise.promisify(companyCollection.update, {context: companyCollection});
                const comapnyObjectID = new mongo.ObjectID(msg.companyID);
                exceptionSource = 'update company collection';
                const updateCompanyResult = await updateCompany({_id: {$eq: comapnyObjectID}},
                    {
                        $pull: {selectedDomains: msg.discoveredDomainToDelete}
                    });
                if(updateCompanyResult && updateCompanyResult.result) {
                    console.log("In DeleteDiscoveredDomainFromCompany - Successfully updated company collection");
                    const DDcollection = db.collection(DISCOVERED_DOMAINS_COLLECTION);
                    const updateDD = Promise.promisify(DDcollection.update, {context: DDcollection});
                    exceptionSource = 'update discoveredDomains collection';
                    const updateDDResult = await updateDD({domain: msg.originDomain, source: msg.originSource},
                        {
                            $addToSet: {hideFrom: {companyID: msg.companyID, discoveredDomainToHide: msg.discoveredDomainToDelete}}
                        },
                        {
                            upsert: true
                        });
                    if(updateDDResult && updateDDResult.result) {
                        console.log("In DeleteDiscoveredDomainFromCompany - Successfully updated discoveredDomains collection");
                        respond(null, {ok: true});
                    }
                    else {
                        console.log("In DeleteDiscoveredDomainFromCompany - Failed to update discoveredDomains collection: returned object is ", updateDDResult);
                        respond(null, {ok: false});
                    }
                }
                else {
                    console.error('Error in DeleteDiscoveredDomainFromCompany-updateCompany: object returned is ', updateCompanyResult);
                    respond(null, {ok: false});
                }
            }
            catch (e) {
                console.error('Error in DeleteDiscoveredDomainFromCompany-' + exceptionSource + ': ', e);
                respond(null, {ok: false});
            }
        }
        else {
            console.log('Wrong parameters were inserted to DeleteDiscoveredDomainFromCompany.');
            respond(null, {ok: false});
        }
    });

    const listToArray = (list) => {
        return new Promise((resolve) => {
            list.toArray((err, docs) => {
                resolve(docs);
            });
        });
    };

    /**
     * Save uploaded files that being scanned against malicious purposes.
     */
    this.add('role:' + role + ', saveScanUploadedFiles:' + targetName, (msg, respond) => {
        if (msg && msg.files && Array.isArray(msg.files) && msg.files.length > 0) {
            console.log('In saveScanUploadedFiles');
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
            native$()
                .then((db) => {
                    const collection = db.collection(FILE_SCANS_COLLECTION);
                    collection.insertMany(msg.files, (err, list) => {
                        console.log('Inserted records:', list);
                        respond(null, {ok: true});
                    });
                });
        } else {
            respond(null, {ok: false, err: 'Wrong parameters for saveScanUploadedFiles'});
        }
    });

    /**
     * Updates the scans that haven't finished yet.
     */
    this.add('role:' + role + ', updateScanUploadedFiles:' + targetName, (msg, respond) => {
        console.log('In updateScanUploadedFiles');
        // Fetch only records with wildfireStatus="scanning" to see if the scan has ended.
        const getScansOfUploadedFiles = {
            wildfireStatus: WILDFIRE_STATUS_TYPES.SCANNING
        };
        if (msg.lastMonth) {
            const lastMonth = moment().subtract(1, 'months');
            // turn lastMonth to moment since the scanFiles are saved in moment format.
            getScansOfUploadedFiles.createDate = {$gte: lastMonth.format()};
        }
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

        return native$()
            .then((db) => {
                const collection = db.collection(FILE_SCANS_COLLECTION);
                const find = Promise.promisify(collection.find, {context: collection});
                return [find(getScansOfUploadedFiles), db];
            })
            .spread((list, db) => {
                list.toArray((err, docs) => {
                    if (docs && docs.length > 0) {
                        console.log('Found ', docs.length, ' records');
                        // Foreach record update its wildfireStatus if changed.
                        const promiseArr = [];
                        let updateIsNeeded = false;
                        docs.map((entity) => {
                            const currPromise = wildfireClient.getReport({hash: entity.sha256})
                                .then((result) => {
                                    if (result && result.wildfire && result.wildfire['file_info'] && result.wildfire['file_info'].malware) {
                                        if (result.wildfire['file_info'].malware === 'no') {
                                            entity.wildfireStatus = WILDFIRE_STATUS_TYPES.SAFE;
                                            console.log('entity wildfire status found as safe!');
                                        } else {
                                            entity.wildfireStatus = WILDFIRE_STATUS_TYPES.MALICIOUS;
                                            console.log('entity wildfire status found as malicious!');
                                        }
                                        updateIsNeeded = true;
                                    } else if (result.error && result.error["error-message"]) {
                                        entity.wildfireStatus = WILDFIRE_STATUS_TYPES.SAFE;
                                        console.error("wildfire error: " + result.error["error-message"]);
                                        updateIsNeeded = true;
                                    } else {
                                        console.log("No result found from wildfire.");
                                    }
                                    return entity;
                                })
                                .catch((e) => {
                                    console.error('Failed to getReport of wildfireClient: ', e);
                                    return entity;
                                });

                            promiseArr.push(currPromise);
                        });

                        return Promise.all(promiseArr)
                            .then((res) => {
                                if (updateIsNeeded) {
                                    // Save updated data back to DB.
                                    const collection = db.collection(FILE_SCANS_COLLECTION);
                                    const update = Promise.promisify(collection.update, {context: collection});
                                    const allUpdates = [];

                                    res.map((currEntityToUpdate) => {
                                        currEntityToUpdate.lastUpdated = moment().format();
                                        const currUpdate = update({_id: currEntityToUpdate._id}, currEntityToUpdate);
                                        allUpdates.push(currUpdate);
                                    });

                                    return Promise.all(allUpdates)
                                        .then(() => {
                                            console.log('Updated all records in uploaded files scans');
                                            respond(null, {ok: true, data: res});
                                        }).catch((e) => {
                                            console.error('Error in updateScanUploadedFiles: ', e);
                                            respond(null, {ok: false, err: e});
                                        });
                                } else {
                                    console.log("Update is not needed.");
                                    respond(null, {ok: true});
                                }
                            })
                            .catch((e) => {
                                console.error('Error in updateScanUploadedFiles: ', e);
                                respond(null, {ok: false, err: e});
                            });
                    } else {
                        console.log('No records to update');
                        respond(null, {ok: true});
                    }
                });
            })
            .catch((e) => {
                console.log('Error updateScanUploadedFiles: ', e);
                respond(null, {ok: false, err: e});
            });
    });

    /**
     * Return the scan results of given files (if exists).
     */
    this.add('role:' + role + ', getScanUploadedFiles:' + targetName, (msg, respond) => {
        if (msg && msg.files && Array.isArray(msg.files)) {
            console.log('In getScanUploadedFiles');

            const files = msg.files;

            if (files.length === 0) {
                respond(null, {ok: true, data: undefined});
            } else {
                const filesNames = files.map((currFile) => {
                    return currFile.path.substring(1, currFile.path.length);
                });

                const getScansOfUploadedFiles = {
                    filename: {$in: filesNames}
                };

                const app_entity = this.make(zone, base, name);
                const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

                return native$()
                    .then((db) => {
                        const collection = db.collection(FILE_SCANS_COLLECTION);
                        const find = Promise.promisify(collection.find, {context: collection});
                        return find(getScansOfUploadedFiles);
                    })
                    .then((list) => {
                        list.toArray((err, docs) => {
                            const filesResult = [];
                            for (let i = 0; i < files.length; i++) {
                                if (docs && docs.length > 0) {
                                    const currFinding = docs.find((currResult) => {
                                        return files[i].path === '/' + currResult.filename;
                                    });
                                    // If any result found - return the result, otherwise return SAFE status.
                                    const currScanStatus = (currFinding && currFinding.wildfireStatus) ? currFinding.wildfireStatus : WILDFIRE_STATUS_TYPES.SAFE;
                                    filesResult.push({
                                        path: files[i].path,
                                        scanStatus: currScanStatus
                                    });
                                } else {
                                    // In case the all the files has no appearance in DB - means all files were by types that are not scanned - mark as SAFE.
                                    filesResult.push({
                                        path: files[i].path,
                                        scanStatus: WILDFIRE_STATUS_TYPES.SAFE
                                    });
                                }
                            }
                            respond(null, {ok: true, data: filesResult});
                        });
                    })
                    .catch((e) => {
                        console.log('Error in getScanUploadedFiles: ', e);
                        respond(null, {ok: false, err: e});
                    });
            }
        } else {
            respond(null, {ok: false, err: 'Wrong parameters for getScanUploadedFiles'});
        }
    });

    /**
     * Return the files that were scanned as malicious (if exists).
     */
    this.add('role:' + role + ', getMaliciousUploadedFiles:' + targetName, (msg, respond) => {
        console.log('In getMaliciousUploadedFiles');
        // Fetch only records with wildfireStatus="scanning" to see if the scan has ended.
        const getScansOfUploadedFiles = {
            wildfireStatus: WILDFIRE_STATUS_TYPES.MALICIOUS
        };
        if (msg.lastMonth) {
            const lastMonth = moment().subtract(1, 'months');
            // turn lastMonth to moment since the scanFiles are saved in moment format.
            getScansOfUploadedFiles.createDate = {$gte: lastMonth.format()};
        }
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

        return native$()
            .then((db) => {
                const collection = db.collection(FILE_SCANS_COLLECTION);
                const find = Promise.promisify(collection.find, {context: collection});
                return find(getScansOfUploadedFiles);
            })
            .then((list) => {
                list.toArray((err, docs) => {
                    let result;
                    if (docs && docs.length > 0) {
                        console.log('Found ', docs.length, ' records with malicious scan findings');
                        result = docs;
                    } else {
                        console.log('No records with malicious scan findings were found');
                        result = [];
                    }
                    respond(null, {ok: true, data: result});
                });
            })
            .catch((e) => {
                console.log('Error in getMaliciousUploadedFiles: ', e);
                respond(null, {ok: false, err: e});
            });
    });

    // Return what surveys this user is allowed to see.
    this.add('role:' + role + ', getAllowedSurveys:' + targetName, (msg, respond) => {
        const getSurveysByUserQuery = {allowedUsers: {$eq: msg.data.id}};
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

        return native$()
            .then((db) => {
                const collection = db.collection('survey_map');
                const find = Promise.promisify(collection.find, {context: collection});
                return find(getSurveysByUserQuery)
                    .then((list) => {
                        console.log('Found records');
                        list.toArray((err, docs) => {
                            respond(null, docs);
                        });
                    });
            });
    });

    this.add('role:' + role + ', updateSurvey:' + targetName, (msg, respond) => {
        this.act('role:' + role + ', list:' + targetName, {query: {sid: msg.data.sid}}, (err, app) => {
            if (app && app.length > 0) {
                const survey = app[0];
                survey.data$(msg.data);
                survey.save$((err, survey) => {
                    if (err) {
                        respond(null, err);
                    }
                    respond(null, {sid: survey.sid});
                });
            } else {
                const survey_entity = this.make(zone, base, name);
                survey_entity.data$(msg.data);
                survey_entity.save$((err, survey) => {
                    if (err) {
                        respond(null, err);
                    }
                    respond(null, {sid: survey.sid});
                });
            }
        });
    });

    this.add('role:' + role + ', updateSurveyAnswer:' + targetName, (msg, respond) => {
        this.act('role:' + role + ', list:' + targetName, {query: {said: msg.data.said}}, (err, app) => {
            if (app && app.length > 0) {
                const survey = app[0];
                survey.data$(msg.data);
                survey.save$((err, survey) => {
                    if (err) {
                        respond(null, err);
                    }
                    respond(null, {said: survey.said});
                });
            } else {
                const survey_entity = this.make(zone, base, name);
                survey_entity.data$(msg.data);
                survey_entity.save$((err, survey) => {
                    if (err) {
                        respond(null, err);
                    }
                    respond(null, {said: survey.said});
                });
            }
        });
    });

    this.add('role:' + role + ', updateAnswers:' + targetName, (msg, respond) => {
        this.act('role:' + role + ', list:' + targetName, {query: {said: msg.data.said}}, (err, app) => {
            if (app && app.length > 0) {
                const survey = app[0];
                survey.data$(msg.data);
                survey.save$((err, survey) => {
                    if (err) {
                        respond(null, err);
                    }
                    respond(null, {said: survey.sid});
                });
            } else {
                const survey_entity = this.make(zone, base, name);
                survey_entity.data$(msg.data);
                survey_entity.save$((err, survey) => {
                    if (err) {
                        respond(null, err);
                    }
                    respond(null, {said: survey.said});
                });
            }
        });
    });

    this.add('role:' + role + ', listById:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        let query;

        if (msg.query.sid) {
            if (msg.query.uid) {
                query = {'sid': {$eq: msg.query.sid}, 'uid': {$elemMatch: {id: {$eq: msg.query.uid}}}};
            } else {
                query = {'sid': {$eq: msg.query.sid}};
            }
        } else {
            query = {'uid': {$elemMatch: {id: {$eq: msg.query.uid}}}};
        }

        native$()
            .then((db) => {
                const collection = db.collection('survey_map');
                const find = Promise.promisify(collection.find, {context: collection});
                return find(query);
            })
            .then((list) => {
                return listToArray(list);
            })
            .then((results) => {
                respond(null, results);
            });
    });

    this.add('role:' + role + ', listByUidAndPid:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        let query;

        query = {'uid': {$elemMatch: {$eq: msg.query.uid}}, 'pid': {$elemMatch: {$eq: msg.query.pid}}};

        native$()
            .then((db) => {
                const collection = db.collection('customer_app');
                const find = Promise.promisify(collection.find, {context: collection});
                return find(query);
            })
            .then((list) => {
                return listToArray(list);
            })
            .then((results) => {
                respond(null, results);
            });
    });

    this.add('role:' + role + ', listAnswersById:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        let getSurveyByUid;

        if (msg.query.said) {
            getSurveyByUid = {'said': {$eq: msg.query.sid}, 'uid': {$elemMatch: {id: {$eq: msg.query.uid}}}};
        } else {
            getSurveyByUid = {'uid': {$elemMatch: {$eq: msg.query.uid}}};
        }

        native$()
            .then((db) => {
                const collection = db.collection('surveyAnswers_map');
                const find = Promise.promisify(collection.find, {context: collection});
                return find(getSurveyByUid);
            })
            .then((list) => {
                return listToArray(list);
            })
            .then((results) => {
                respond(null, results);
            });
    });

    this.add('role:' + role + ', listAnswersByCompanyIdArr:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});

        native$()
            .then((db) => {
                const collection = db.collection('surveyAnswers_map');
                const find = Promise.promisify(collection.find, {context: collection});

                return find({'selectedCompanyId': {$in: msg.query.companyIdArr}});
            })
            .then((resArr) => {
                    return listToArray(resArr);
            })
            .then((results) => {
                let filteredResults = [];
                for (let i = 0; i < results.length; i++) {
                    if (!results[i].length !== 0) {
                        filteredResults = filteredResults.concat(results[i]);
                    }
                }
                respond(null, filteredResults);
            });
    });

    this.add('role:' + role + ', listProjectsById:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        let getProjectQuery;

        if (msg.query.projectName) {
            getProjectQuery = {
                'projectName': {$eq: msg.query.projectName},
                'allowedUsers': {$elemMatch: {id: {$eq: msg.query.uid}}}
            };
        } else {
            getProjectQuery = {'allowedUsers': {$elemMatch: {id: {$eq: msg.query.uid}}}};
        }

        native$()
            .then((db) => {
                const collection = db.collection('project_map');
                const find = Promise.promisify(collection.find, {context: collection});
                return find(getProjectQuery);
            })
            .then((list) => {
                return listToArray(list);
            })
            .then((results) => {
                respond(null, results);
            });
    });

    this.add('role:' + role + ', listCompaniesById:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        let getCompaniesByUid;

        getCompaniesByUid = {'allowedUsers': {$elemMatch: {id: {$eq: msg.query.uid}}}};

        native$()
            .then((db) => {
                const collection = db.collection(COMPANY_COLLECTION);
                const find = Promise.promisify(collection.find, {context: collection});
                return find(getCompaniesByUid);
            })
            .then((list) => {
                return listToArray(list);
            })
            .then((results) => {
                respond(null, results);
            });
    });

    this.add('role:' + role + ', listByMultipleIds:' + targetName, (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        const listAllCompanies = [];
        let query;
        for (let i = 0; i < msg.query.ids.length; i++) {
            const id = new mongo.ObjectID(msg.query.ids[i]);
            listAllCompanies.push({_id: {$eq: id}});
        }

        query = {$or: listAllCompanies};

        native$()
            .then((db) => {
                const collection = db.collection(COMPANY_COLLECTION);
                const find = Promise.promisify(collection.find, {context: collection});
                return find(query);
            })
            .then((list) => {
                return listToArray(list);
            })
            .then((results) => {
                respond(null, results);
            });
    });

    this.add('role:' + role + ', deleteCascadeCompanyInProjectsByID:' + targetName, (msg, respond) => {
        if (msg && msg.id) {
            console.log('In deleteCascadeCompanyInProjectsByID - deleting all related companies with id: ', msg.id);

            const app_entity = this.make(zone, base, name);

            app_entity.native$((err, db) => {
                const collection = db.collection(PROJECT_COLLECTION);

                // Update all project records that is signed to company with this ID and its name isn't updated.
                collection.update(
                    {},
                    {$pull: {'selectedCompanies': {id: {$eq: msg.id}}}},
                    {multi: true}
                    , (err, res) => {
                        if (err) {
                            respond(null, {err: err});
                        } else {
                            if (res && res.result && res.result.hasOwnProperty('nModified')) {
                                if (res.result.nModified === 0) {
                                    console.log('No project instances with companyID [', msg.id, '] found to update.');
                                } else {
                                    console.log('CompanyID [', msg.id, '] was deleted from ', res.result.nModified, ' related project instances.');
                                }
                            }
                            respond(null, {ok: true});
                        }
                    });
            });
        } else {
            respond(null, {err: 'Invalid params for updateCompanyNameInProjects.'});
        }
    });

    this.add('role:' + role + ', updateCompanyNameInProjects:' + targetName, (msg, respond) => {
        if (msg && msg.companyName) {
            if (msg.id) {
                console.log('In updateCompanyNameInProjects - updating companyName: ', msg.companyName);

                const app_entity = this.make(zone, base, name);

                app_entity.native$((err, db) => {
                    const collection = db.collection(PROJECT_COLLECTION);

                    // Update all project records that is signed to company with this ID and its name isn't updated.
                    collection.updateMany({
                        'selectedCompanies': {
                            $elemMatch: {
                                id: msg.id,
                                name: {$ne: msg.companyName}
                            }
                        }
                    }, {
                        $set: {'selectedCompanies.$.name': msg.companyName}
                    }, (err, res) => {
                        if (err) {
                            respond(null, {err: err});
                        } else {
                            if (res && res.hasOwnProperty('matchedCount')) {
                                if (res.matchedCount === 0) {
                                    console.log('All related project instances with this companyName doesn\'t need an update.');
                                } else {
                                    console.log('CompanyName was updated on ', res.matchedCount, ' related project instances.');
                                }
                            }
                            respond(null, {ok: true});
                        }
                    });
                });
            } else {
                // This means that this is a new company (that has no id yet)
                respond(null, {ok: true});
            }
        } else {
            respond(null, {err: 'Invalid params for updateCompanyNameInProjects.'});
        }
    });

    this.add('role:' + role + ', getCompanyByID:' + targetName, (msg, respond) => {
        if (msg && msg.uid && msg.companyID) {
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
            const companyID = new mongo.ObjectID(msg.companyID);

            const query = {
                _id: {$eq: companyID},
                allowedUsers: {
                    $elemMatch: {
                        id: {$eq: msg.uid}
                    }
                }
            };

            native$()
                .then((db) => {
                    const collection = db.collection(COMPANY_COLLECTION);
                    const find = Promise.promisify(collection.find, {context: collection});
                    return find(query);
                })
                .then((list) => {
                    return listToArray(list);
                })
                .then((results) => {
                    respond(null, results);
                });
        } else {
            respond(null, {err: 'Invalid params for getCompanyByID.'});
        }
    });

    // Get the latest survey of a company by its ID.
    this.add('role:' + role + ', getLatestSurveyByCompanyID:' + targetName, (msg, respond) => {
        if (msg && msg.companyID) {
            const app_entity = this.make(zone, base, name);

            app_entity.native$((err, db) => {
                const collection = db.collection(SURVEY_ANSWERS_COLLECTION);

                collection.find({
                    'selectedCompanyId': msg.companyID
                })
                    .sort({
                        'lastChangedDate': -1
                    })
                    .limit(1)
                    .toArray((err, res) => {
                        if (err) {
                            console.error(err);
                            respond(null, {err: err});
                        } else if (res && res.length) {
                            const companySurvey = res[0];
                            const resultFields = {
                                said: companySurvey.said,
                                progress: companySurvey.progress,
                                surveyScore: companySurvey.score,
                                counts: helpers.countSurveyAnswers(companySurvey)
                            };
                            respond(null, {ok: true, data: resultFields});
                        } else {
                            respond(null, {isEmpty: true});
                        }
                    });
            });
        } else {
            respond(null, {err: 'Invalid params for getLatestSurveyByCompanyID.'});
        }
    });

    // Update new blacklists findinds for domains, if the domain does not already exist in this collection - insert it.
    this.add('role:blacklists, cmd:updateDomainsFindings', (msg, respond) => {
        if (msg.findings) {
            const app_entity = this.make(zone, base, name);
            const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
            native$()
                .then((db) => {
                    const collection = db.collection(BLACKLIST_COLLECTION);

                    return Promise.each(msg.findings, (finding) => {
                        const find = {
                            'domain': {
                                $eq: finding.domain
                            }
                        };

                        const update = finding;
                        update['resolve_time'] = moment().format();

                        return collection.updateOne(find, {
                            $set: update
                        }, {upsert: true});
                    });
                })
                .then(() => {
                    respond(null, {ok: true});
                })
                .catch((err) => {
                    respond(null, {ok: false, err: err});
                });
        } else {
            respond(null, {ok: false, err: 'Invalid params for updateUserLastLoginTry.'});
        }
    });

    // Update new scoresData findinds for companyID.
    this.add('role:' + role + ', updateScoresDataByCompanyID:' + targetName, (msg, respond) => {
        if (msg && msg.companyData && msg.companyData.id && msg.companyData.scoresData && msg.companyData.hasOwnProperty("maxIntelScore")) {
            let app_entity = this.make(zone, base, name);
            let native$ = Promise.promisify(app_entity.native$, {context: app_entity});
            native$()
                .then((db) => {
                    let collection = db.collection(COMPANY_COLLECTION);

                    let companyID = new mongo.ObjectID(msg.companyData.id);

                    let find = {
                        "_id": {
                            $eq: companyID
                        }
                    };

                    let scoresDataToUpdate = msg.companyData.scoresData;

                    if(!scoresDataToUpdate.hasOwnProperty("intelScore")) {
                        scoresDataToUpdate.intelScore = msg.companyData.maxIntelScore;
                    }

                    // Save intelScore and totalScore.
                    let isSurveyNA = (msg.companyData.surveyScore == undefined || msg.companyData.isSurveyNA);

                    scoresDataToUpdate.totalScore = calculateTotalScore(msg.companyData.maxIntelScore, msg.companyData.surveyScore || 0, msg.companyData.ratios, isSurveyNA);

                    if(scoresDataToUpdate.hasOwnProperty("intelScoreRatios")) {
                        delete scoresDataToUpdate.intelScoreRatios;
                    }

                    scoresDataToUpdate["last_updated"] = moment().format();

                    let update = {
                        scoresData: scoresDataToUpdate
                    };

                    return collection.updateOne(find, {
                        $set: update
                    }, {upsert: true});
                })
                .then(() => {
                    respond(null, {ok: true});
                })
                .catch((err) => {
                    respond(null, {ok: false, err: err});
                });
        }
        else {
            respond(null, {ok: false, err: "Invalid params for updateScoresDataByCompanyID."});
        }
    });

    // Get all domains' blacklists findings.
    this.add('role:blacklists, cmd:getDomainsFindings', (msg, respond) => {
        if (msg.domains && Array.isArray(msg.domains)) {
            if (msg.domains.length === 0) {
                respond(null, {ok: true, findings: []});
            } else {
                const app_entity = this.make(zone, base, name);
                const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
                native$()
                    .then((db) => {
                        const collection = db.collection(BLACKLIST_COLLECTION);

                        const find = {
                            'domain': {
                                $in: msg.domains
                            }
                        };

                        return collection.find(find);
                    })
                    .then((res) => {
                        if (res) {
                            res.toArray((err, docs) => {
                                const result = {ok: true};

                                if (docs && docs.length > 0) {
                                    result.findings = docs;
                                } else {
                                    result.findings = [];
                                }
                                respond(null, result);
                            });
                        } else {
                            respond(null, {ok: false, err: 'Failed to fetch data'});
                        }
                    })
                    .catch((err) => {
                        respond(null, {ok: false, err: err});
                    });
            }
        } else {
            respond(null, {ok: false, err: 'Invalid params for getDomainsFindings.'});
        }
    });

    // Delete domains' blacklists findings that have no longer found for over a week.
    this.add('role:blacklists, cmd:deleteOutdatedDomainsFindings', (msg, respond) => {
        const app_entity = this.make(zone, base, name);
        const native$ = Promise.promisify(app_entity.native$, {context: app_entity});
        native$()
            .then((db) => {
                const collection = db.collection(BLACKLIST_COLLECTION);

                const oneMonthBack = moment().subtract(1, 'months').startOf('day').format();

                const find = {
                    'resolve_time': {
                        $lt: oneMonthBack
                    }
                };

                return collection.deleteMany(find);
            })
            .then((res) => {
                const result = {ok: true};
                if (res && res.hasOwnProperty('deletedCount')) {
                    result.deletedCount = res.deletedCount;
                }
                respond(null, result);
            })
            .catch((err) => {
                respond(null, {ok: false, err: err});
            });
    });

    this.add('role:basic, update:props', (msg, respond) => {
        if (msg && msg.companyID && msg.scanId && msg.date) {
            const app_entity = this.make(zone, base, name);

            app_entity.native$((err, db) => {
                const collection = db.collection(COMPANY_COLLECTION);

                collection.updateOne({_id: new mongo.ObjectID(msg.companyID)},
                    {$set: {'currentScanId': msg.scanId, 'currentScanDate': msg.date}}, (err, res) => {
                            if (err) {
                                console.error(err);
                                respond(null, {err: err});
                            } else if (res.result) {
                                respond(null, {ok: true});
                            } else {
                                respond(null, {isEmpty: true});
                            }

                    });
            });
        } else {
            respond(null, {err: 'Invalid params for getLatestSurveyByCompanyID.'});
        }
    });
};


