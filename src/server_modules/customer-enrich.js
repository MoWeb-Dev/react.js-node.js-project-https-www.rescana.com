'use strict';
const Promise = require('bluebird');

// You can run a curl to test e.g.
// curl -d '{"role":"customerenrich","enrich":"app","url":"http://www.bekaloot.co.il"}' http://localhost:11104/act
module.exports = function CustomerEnrichment() {
    let res;
    // Promisify the .act() method
    const act = Promise.promisify(this.act, {context: this});

    this.add('role:customerenrich, enrich:app', async (msg, respond) => {
        const url = msg.url;

        try {
            res = await act('role:customerstore, list:app', {query: {url: url}});

            act('role:whois, cmd:sendQuery', {app: res[0], customer: true});
            act('role:scraper, scrape:page', {app: res[0], customer: true});
            act('role:screenshot, capture:app', {app: res[0], customer: true});

            respond();
        } catch(e){
            console.error("Error in role:customerstore, list:app ", e);
            respond();
        }

    });

};
