'use strict';


const isExistAndArray = function(data) {
    return !!(data && Array.isArray(data));
};
// This function checks whether the input is an array or not.
module.exports.isExistAndArray = isExistAndArray;

// This function checks whether the input is an array with at least 1 item or not.
module.exports.isNotEmptyArray = function(data) {
    return (isExistAndArray(data) && data.length > 0);
};

module.exports.asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};
