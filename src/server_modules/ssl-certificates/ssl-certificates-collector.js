'use strict';
const PromiseB = require('bluebird');
const {getDistinctDomainsByCompanies} = require('../reports/common.js');
const {STATUS_SSL} = require('../../../config/server-consts.js');
const {isNotEmptyArray, asyncForEach} = require('./ssl-certificates-common.js');
const https = require('https');
const wayback = require('../gdpr-compliance/wayback-scraper.js');
const VALID_TO = 'valid_to', DAYS_REMAINING = 'days_remaining', DAYS_EXPIRED = 'days_expired';

module.exports = function sslCertificatesCollector() {
    const act = PromiseB.promisify(this.act, {context: this});
    let sslData;

    const collectSslCertificates = async (msg, respond) => {
        console.log('In collectSslCertificates - starting to collect data');
        try {
            let allSystemDomains = await getAllDomainsInSystem(msg.companyIDs);
            // Respond to scheduler that the collecting has started.
            respond({ok: true});

            // Search for relevant SSL certificates for found domains.
            sslData = await getAllSslCertificates(allSystemDomains);
        }catch(e){
            console.log('Error in collectSslCertificates-getAllDomainsInSystem: ', e);
            // Respond to scheduler that the collecting has failed to start.
            respond({ok: false});
        }
        let sslDataCount = 0;
        if (isNotEmptyArray(sslData)) {
            sslDataCount = sslData.length;
        }
        console.log('Finished collecting ', sslDataCount, ' SSL certificates data');

    };

    this.add('role:sslCertificatesCollect, cmd:collectSSLCerts', collectSslCertificates);

    // This function load all existing companies -> domains and returns all distinct domains.
    const getAllDomainsInSystem = async (inputCompanyIDs) => {
        console.log('In getAllDomainsInSystem - starting to collect domains');

        // Get all companies.
        const params = {query: {}};
        let allCompanies, allDomainsWithSubdomains;

        // Check if admin requested a specific companies - otherwise all companies are returned.
        if (isNotEmptyArray(inputCompanyIDs)) {
            console.log('Requested companyIDs from Manual APIs: ', inputCompanyIDs);
            params.query = inputCompanyIDs;
        }
        try {
            allCompanies = await act('role:companies, listByObjectID:company', params);
        }catch(e){
            console.log('Error getting all companies in getAllDomainsInSystem: ', e);
            return [];
        }

        // Get all domains.
        const allDomains = getDistinctDomainsByCompanies(allCompanies);

        if (isNotEmptyArray(allDomains)) {
            console.log('Fetched a total of ', allDomains.length, ' domains');
        } else {
            console.log('No domains were found on the system');
        }

        // Get all domains' subdomains.
        try {
            allDomainsWithSubdomains = await act('role:intelQuery,cmd:getAllSubdomainsByDomains', {domains: allDomains});
            return allDomainsWithSubdomains;
        }catch(e){
            console.log('Error getting all subdomains in getAllSubdomainsByDomains: ', e);
            return [];
        }
    };

    const saveFinishedDomainToDB = async (matchingDomain) => {
        // Save current domain's data to DB.
        if (matchingDomain && matchingDomain.domain) {
            try {
                let savedData = await saveRelevantSslData([matchingDomain]);
                if (savedData && savedData.ok) {
                    console.log('Finished saving SSL certificates data on domain ', matchingDomain.domain);
                }
            } catch (e) {
                console.error('Error in saveFinishedDomainToDB: ', e);
            }
        } else {
            console.log('Did not get matching domain');
        }
    };

    // This function iterates on all domains and searches the relevant https SSL certificates.
    const getAllSslCertificates = (domains, currentQueueID) => {
        return new Promise(async (resolve) => {
            console.log('In getAllSslCertificates - starting to collect all SSL certificates data - for Queue [', currentQueueID, ']');
            let resultWithSubdomainsData;

            if (isNotEmptyArray(domains)) {
                const resultData = [];

                await asyncForEach(domains, async (currDomain) => {
                    if (currDomain && currDomain.domain) {
                        const domain = currDomain.domain;
                        console.log('Starting to fetch SSL certificate data for domain: ', domain);
                        try {
                            let res = await getDomainSslCertificateData(domain);
                            if (res && res.domain) {
                                resultData.push(res);

                                let logMsg = 'Domain: ' + domain + ' discovered with';
                                if (!res.valid) {
                                    logMsg += 'out';
                                }
                                logMsg += ' a valid SSL certificate';
                                console.log(logMsg);

                                // Save current domain's data without all subdomains to DB.
                                await saveFinishedDomainToDB(res, resultData);
                            } else {
                                console.log('No SSL results');
                            }
                        } catch (e) {
                            console.log('Error in getDomainSslCertificateData of \'', domain, '\': ', e);
                        }

                        console.log('Starting to fetch all SSL certificates records for all subdomains');
                    }
                });

                try {
                    resultWithSubdomainsData = await getSslCertificatesForSubdomains(domains, resultData);
                } catch (e) {
                    console.error('Error in getSslCertificatesForSubdomains: ', e);
                }

                if (isNotEmptyArray(resultWithSubdomainsData)) {
                    resolve(resultWithSubdomainsData);
                } else {
                    resolve(resultData);
                }
            } else {
                console.log('No domains were found while running Queue [', currentQueueID, '].');
                resolve([]);
            }
        });
    };

    const getSslCertificatesForSubdomains = (domains, resultDomainsData) => {
        return new Promise(async (resolve) => {
            console.log('In getSslCertificatesForSubdomains - starting to collect all subdomains SSL certificates data');

            if (isNotEmptyArray(domains)) {
                const resultData = resultDomainsData || [];
                let subdomainResultsCount = 0;
                await asyncForEach(domains, async (currDomain) => {
                    if (currDomain && currDomain.domain && isNotEmptyArray(currDomain.subdomains)) {
                        // Find the matching domain to add each subdomain result to.
                        const matchingDomain = resultData.find((currEntity) => {
                            return currEntity && currEntity.domain && currEntity.domain === currDomain.domain;
                        });

                        // Foreach subdomain - fetch its SSL data.
                        await asyncForEach(currDomain.subdomains, async (currSubdomain) => {
                            if (currSubdomain && !currSubdomain.startsWith('www.')) {
                                console.log('Starting to fetch SSL certificate data for subdomain: ', currSubdomain);
                                let res;
                                try {
                                    res = await getDomainSslCertificateData(currSubdomain);
                                } catch (e) {
                                    console.log('Error in getDomainSslCertificateData of subdomain \'', currSubdomain, '\': ', e);
                                }
                                if (res && res.domain) {
                                    if (matchingDomain) {
                                        subdomainResultsCount++;
                                        if (matchingDomain.subdomains) {
                                            matchingDomain.subdomains.push(res);
                                        } else {
                                            matchingDomain.subdomains = [res];
                                        }
                                    }

                                    let logMsg = 'Subdomain: ' + currSubdomain + ' discovered with';
                                    if (!res.valid) {
                                        logMsg += 'out';
                                    }
                                    logMsg += ' a valid SSL certificate';
                                    console.log(logMsg);
                                }
                                try {
                                    // Get current domain's data with all subdomains and save it to DB.
                                    await saveFinishedDomainToDB(matchingDomain);
                                } catch (e) {
                                    console.log('Error in checkIfSSLCollectHasEnded at subdomain \'', currSubdomain, '\': ', e);
                                }
                            }
                        });
                    }
                });

                // This fix is for domains without any registered subdomains.
                if (subdomainResultsCount === 0) {
                    console.log('No subdomains were found while running Queue.');
                    resolve(resultData);
                }
            } else {
                resolve(resultDomainsData);
            }
        });
    };

    const getSslStatus = (url, domain) => {
        return new Promise(async (resolve) => {
            const sslData = {
                domain: domain,
                statusSSL: STATUS_SSL.HTTPS,
                valid: false
            };
            let timeout = 3000;

            const options = {
                host: url || domain,
                port: 443,
                method: 'GET',
                rejectUnauthorized: false,
                family: 4
            };

            let finalUrl = 'https://' + options.host;
            console.log('waiting ' + timeout / 1000 + ' seconds');
            await new Promise(r => setTimeout(r, timeout));
            const req = https.request(options, function(res) {
                if (res && res.statusCode && (res.statusCode === 404 || res.statusCode === 403)) {
                    sslData.statusSSL = STATUS_SSL.OFFLINE;
                    sslData.url = finalUrl;
                } else if (res && res.connection) {

                    const certificateInfo = res.connection.getPeerCertificate();

                    if (certificateInfo && certificateInfo[VALID_TO]) {
                        try {
                            const today = new Date();
                            const dateTo = new Date(certificateInfo[VALID_TO]);
                            const difference = dateTo - today;
                            if (difference) {
                                const days = Math.round(difference / (24 * 60 * 60 * 1000));
                                if (days && days > 0) {
                                    sslData.valid = true;
                                    sslData[DAYS_REMAINING] = days;
                                } else {
                                    sslData[DAYS_EXPIRED] = days * -1;
                                }
                            }
                        } catch (e) {
                            console.log('Error reading SSL certificate expiration dates for \'', domain, '\':', e);
                        }

                        // Save all certificate data on result object.
                        Object.keys(certificateInfo).map((currKey) => {
                            // Don't override existing data properties.
                            if (currKey && !sslData.hasOwnProperty(currKey)) {
                                sslData[currKey] = certificateInfo[currKey];
                            }
                        });
                    }
                } else {
                    console.error('In getSslStatus - Could not connect to: ' + domain);
                }
                sslData.url = finalUrl;
                resolve(sslData);
            }).on('error', (e) => {
                if (e && e.code) {
                    sslData.url = finalUrl;
                    if (e.code === 'ENOTFOUND' || e.code === 'EPROTO') {
                        sslData.statusSSL = STATUS_SSL.OFFLINE;
                    } else if (e.code === 'ECONNREFUSED') {
                        sslData.statusSSL = STATUS_SSL.NO_HTTPS;
                        sslData.url = 'http://' + domain;
                    } else if (e.code === 'ETIMEDOUT' || e.code === 'ECONNRESET') {
                        sslData.statusSSL = STATUS_SSL.TIMEOUT;
                    }
                }
                if (e && e.code) {
                    console.log('domain ', domain, ' resulted with https request error code: ', e.code, ' which means: ', sslData.statusSSL);
                } else {
                    console.log('domain ', domain, ' resulted with https request error: ', e);
                }
                resolve(sslData);
            });

            req.end();
        });
    };

    // This function returns the SSL certificate data of a given domain if exists.
    const getDomainSslCertificateData = (domain) => {
        return new Promise(async (resolve) => {
            const sslData = {
                domain: domain,
                statusSSL: STATUS_SSL.HTTPS,
                valid: false
            };

            try {
                let res = await wayback.isRedirect(domain);
                const isAnswered = (res && res.isAnswered);
                if (isAnswered) {
                    //in case http request found and the redirect url didnt redirect to https with the same domain name
                    if (res.isHttpRedirectUrl && res.url) {
                        sslData.statusSSL = STATUS_SSL.REDIRECT;
                        sslData.url = 'http://' + res.url;
                        resolve(sslData);
                        console.log('Domain ', domain, ' resulted with a redirect response');
                        //in case request found with parking url
                    } else if (res.isParking) {
                        sslData.statusSSL = STATUS_SSL.OFFLINE;
                        sslData.url = 'https://' + res.url;
                        resolve(sslData);
                        console.log('Domain ', domain, ' resulted with a parking url');
                        //in case https request found and need to be checked for certifications
                    } else if (res.url) {
                        resolve(await getSslStatus(res.url, domain));
                    }
                } else {
                    console.log('Domain ', domain, ' resulted with no answer');
                    sslData.statusSSL = STATUS_SSL.OFFLINE;
                    sslData.url = 'http://' + domain;
                    resolve(sslData);
                }
            } catch (e) {
                console.error('Error in getDomainSslCertificateData-isRedirect: ', e);
                resolve(await getSslStatus(domain));
            }
        });
    };

    // This function saves the results - sslData to DB.
    const saveRelevantSslData = (sslData) => {
        return new Promise((resolve) => {
            if (isNotEmptyArray(sslData)) {
                console.log('Starting to save ', sslData.length, ' SSL certificates findings on DB');
                // Save sslData to DB.
                return act('role:sslCerts, insertSSLCertificatesFindings:sslCert', {data: sslData})
                    .then((result) => {
                        if (result && result.ok) {
                            console.log('Successfully saved ', sslData.length, ' SSL certificates findings on DB.');
                            resolve({ok: true});
                        } else {
                            console.log('Failed saving SSL certificates findings on DB');
                            resolve({ok: false});
                        }
                    }).catch((e) => {
                        console.log('Error in saveRelevantSslData - from finished Queue: ', e);
                        resolve({ok: false});
                    });
            } else {
                resolve({ok: false});
            }
        });
    };
};
