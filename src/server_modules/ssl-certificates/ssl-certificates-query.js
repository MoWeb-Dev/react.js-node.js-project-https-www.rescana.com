'use strict';
const PromiseB = require('bluebird');
const {getDistinctDomainsByCompanies} = require('../reports/common.js');
const {whoIsRelatedByDomain} = require('../../server/api/api-helpers.js');
const {isNotEmptyArray} = require('./ssl-certificates-common.js');


module.exports = function sslCertificatesQuery() {
    const act = PromiseB.promisify(this.act, {context: this});

    // This function gets SSL certificates data results from mongo and adds whoIsRelatedByDomain for connecting domain -> relatedCompanies.
    const getSslCertificatesData = (msg, respond) => {
        console.log('In getSslCertificatesData');

        const params = {};
        // If companyIDs were given - only results related to these companies are returned. Otherwise, returned data related to all companies in the system.
        if (msg && isNotEmptyArray(msg.companyIDs)) {
            params.query = msg.companyIDs;
        }
        // Get all companies from DB.
        return act('role:companies, listByObjectID:company', params)
            .then((allCompanies) => {
                const query = {query: {}};

                // Get all companies domains.
                const allDomains = getDistinctDomainsByCompanies(allCompanies);

                // get SSL Certificates data only for relevant domains.
                if (isNotEmptyArray(allDomains)) {
                    query.query.domain = {$in: allDomains};
                }

                // Get sslData from DB.
                return [act('role:sslCerts, list:sslCert', query), allCompanies];
            }).catch((e) => {
                console.log('Error in getSslCertificatesData-list:sslCert: ', e);

                respond({ok: false});
            }).spread((sslData, allCompanies) => {
                if (isNotEmptyArray(sslData)) {
                    if (isNotEmptyArray(allCompanies)) {
                        const dataToReturn = [];

                        sslData.map((currEntity) => {
                            // Make sure this feed result is relevant.
                            if (currEntity && currEntity.domain) {
                                // Add related companies to the object.
                                currEntity.relatedCompanies = whoIsRelatedByDomain(allCompanies, currEntity.domain);

                                // Insert the entity to the returned array only if related to any given company.
                                if (isNotEmptyArray(currEntity.relatedCompanies)) {
                                    dataToReturn.push(currEntity);
                                }
                            }
                        });

                        if (isNotEmptyArray(dataToReturn)) {
                            console.log('Found relevant results for given companies');
                            respond({ok: true, data: dataToReturn});
                        } else {
                            console.log('No relevant results were found for given companies');
                            respond({ok: false});
                        }
                    } else {
                        console.log('getSslCertificatesData didn\'t return any companies data');
                        respond({ok: false});
                    }
                } else {
                    console.log('getSslCertificatesData didn\'t return any ssl certificates records');
                    respond({ok: false});
                }
            }).catch((e) => {
                console.error('Error in getSslCertificatesData-getAllCompanies: ', e);

                respond({ok: false});
            });
    };

    this.add('role:sslCertificatesQuery, cmd:getSSLCerts', getSslCertificatesData);
};
