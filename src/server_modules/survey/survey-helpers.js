// Aggragates all pages into one page.
module.exports.concatPages = (survey) => {
    const singlePage = [{}];
    let elements = [];

    if (survey.pages) {
        for (let i = 0; i < survey.pages.length; i++) {
            elements = elements.concat(survey.pages[i].elements);
        }
    }

    singlePage[0].elements = elements;
    singlePage[0].name = 'Survey';
    return (singlePage);
};
