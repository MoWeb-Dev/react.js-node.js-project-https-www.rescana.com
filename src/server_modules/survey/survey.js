const Promise = require('bluebird');
const ExcelJS = require('exceljs-rtl');
const moment = require('moment');
const shortid = require('shortid');
const consts = require('../../../config/consts');
const t = require('../../../config/i18n');
const xl = require('excel4node');
const config = require('app-config');
const helpers = require('./survey-helpers');
const reportHelpers = require('../reports/common');
const excelHelpers = require('../reports/excel-reports/excel-report.js');
const serverHelpers = require('../../server/api/api-helpers');

module.exports = function Survey() {
    const promiseAct = Promise.promisify(this.act, {context: this});

    // consts to read data properly from excel.
    const PAGE_TITLE_ROW = 2, PAGE_TITLE_COL = 2, CATEGORY_COL = 2, DATA_START_ROW = 4, ANSWER_COL = 5;
    // The column in position [CATEGORY_COL] can be a category or a question.
    const COL_TYPES = {Category: 1, Question: 2};

    const getsurveyWithAnswers = (msg, respond) => {
        return listSurveyWithAnswer({uid: msg.query.uid, said: msg.query.said}).then((data) => {
            return respond(null, data);
        });
    };

    const listSurveyWithAnswer = (obj) => {
        const act = Promise.promisify(this.act, {context: this});
        console.log('Querying surveyAnswers uid : ', obj.uid);
        console.log('Querying surveyAnswers said : ', obj.said);
        let query;

        query = !obj.uid ? {query: {said: obj.said}} : {uid: obj.uid, query: {said: obj.said}};

        return act('role:surveystore, list:surveyAnswers', query)
            .then((data) => {
                console.log('Received data : ', data);
                if (data && data.length > 0 && data[0]) {
                    data = data[0];
                    // Check if surveyAnswers object has questions from survey.
                    if (data.pages) {
                        return Promise.resolve(data);
                    } else {
                        query = !obj.uid ? {query: {sid: data.sid}} : {uid: obj.uid, query: {sid: data.sid}};
                        // If not, get the questions from template and add to surveyAnswers Object.
                        return act('role:surveystore, listById:survey', query)
                            .then((templateArr) => {
                                console.log('Templates found: ', templateArr);
                                if (templateArr && templateArr[0]) {
                                    data.pages = templateArr[0].pages;
                                    data.lang = templateArr[0].lang;
                                } else {
                                    console.log('Could not find templates.');
                                }
                                return Promise.resolve(data);
                            })
                            .catch((e) => {
                                console.log('Error in \'role:surveystore, listById:survey\' : ', e);
                                return Promise.reject(e);
                            });
                    }
                } else {
                    return Promise.resolve([]);
                }
            })
            .catch((e) => {
                console.log(e);
                return Promise.reject(e);
            });
    };

    const initQuest = (msg) => {
        const user = msg.query.user;
        const templateName = msg.query.templateName || 'New Template';
        const templateLang = msg.query.lang || consts.DEFAULT_LANG;
        const sid = shortid.generate();

        // create a questionnaire from the excel's data.
        const questionnaire = {};
        questionnaire.name = templateName;
        questionnaire.sid = sid;
        questionnaire.pages = [];
        questionnaire.createDate = moment().format();
        questionnaire.uid = [user];
        questionnaire.lang = templateLang;
        return questionnaire;
    };

    const initQuestAnswer = (msg, sid, templatePages, selectedCompanyName) => {
        const user = msg.query.user.id;
        const templateLang = msg.query.lang || consts.DEFAULT_LANG;
        const said = shortid.generate();
        const pages = (templatePages && Array.isArray(templatePages)) ? templatePages : [];

        // create a questionnaire from the excel's data.
        const questionnaire = {};
        questionnaire.lastChangedDate = moment().format();
        questionnaire.sid = sid;
        questionnaire.selectedCompanyId = msg.query.surveyCompanyId;
        questionnaire.selectedCompanyName = selectedCompanyName;
        questionnaire.respondent = msg.query.surveyRespondent;
        questionnaire.name = msg.query.surveyName;
        questionnaire.said = said;
        questionnaire.uid = [user];
        questionnaire.progress = 0;
        questionnaire.pages = pages;
        questionnaire.lang = templateLang;
        return questionnaire;
    };

    const setLang = (langFound, worksheet, questionnaire) => {
        if (!langFound) {
            if (worksheet.views && worksheet.views[0]) {
                if (worksheet.views[0].rightToLeft && worksheet.views[0].rightToLeft === 1) {
                    // set workbook as RTL.
                    questionnaire.lang = consts.LANG_HE;
                } else {
                    // set workbook as LTR.
                    questionnaire.lang = consts.LANG_EN;
                }
            }
        }
        return true;
    };

    const saveTemplateToDB = (questionnaire) => {
        const act = Promise.promisify(this.act, {context: this});

        return act('role:surveystore, updateSurvey:survey', {sid: questionnaire.sid, data: questionnaire})
            .then(() => {
                console.log('Saved survey');
                return questionnaire.sid;
            })
            .catch((e) => {
                console.log('Error in saveSurvey: ', e);
                return '';
            });
    };

    const saveSurveyAnswerToDB = (questionnaire) => {
        return promiseAct('role:surveystore, updateSurveyAnswer:surveyAnswers', {
            said: questionnaire.said,
            data: questionnaire
        })
            .then(() => {
                console.log('Saved survey answer');

                // Update the surveyScore also on company.
                if (questionnaire.selectedCompanyId && questionnaire.score) {
                    const data = {id: questionnaire.selectedCompanyId, surveyScore: questionnaire.score};

                    return promiseAct('role:companies, update:company', {data: data})
                        .then(() => {
                            console.log('Updated Company Score');
                            return questionnaire.said;
                        })
                        .catch((e) => {
                            console.log('Error while updating companySurveyScore: ', e);
                            return questionnaire.said;
                        });
                } else {
                    console.log('No surveyScore to update on company');

                    return questionnaire.said;
                }
            })
            .catch((e) => {
                console.log('Error in saveSurvey: ', e);
                return '';
            });
    };

    const setExcelDir = (lang) => {
        if (lang) {
            if (lang === consts.LANG_EN) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    };

    const convertRichTextToString = (richTextObj) => {
        let result = '';
        richTextObj.forEach((textObj) => {
            if (textObj.text) {
                result += textObj.text;
            }
        });
        return result;
    };

    const getSurveyTemplateFromExcel = (msg, respond) => {
        // Used to insert default paging number if no page name is written in excel.
        let pagesCounter = 0;

        // Read the message query.
        const filePath = msg.query.filePath;
        console.log('Getting excel data of : ', filePath);

        if (filePath) {
            const workbook = new ExcelJS.Workbook();
            workbook.xlsx.readFile(filePath)
                .then(() => {
                    // Check the first worksheet language or set to default.
                    let langFound = false;
                    const questionnaire = initQuest(msg);

                    // Iterate on excel and add data as pages.
                    workbook.eachSheet((worksheet) => {
                        if (worksheet) {
                            let categoriesCounter = 0;

                            // If it's the first worksheet - Check the language of the excel.
                            langFound = setLang(langFound, worksheet, questionnaire);

                            const currentPage = {};
                            const currentPageElements = [];
                            let currentCategory = {};
                            let currentQuestions = [];

                            pagesCounter++;
                            let pageName = worksheet.getRow(PAGE_TITLE_ROW).getCell(PAGE_TITLE_COL).value;

                            if (!pageName) {
                                pageName = t[questionnaire.lang]['SurveyApi_Page_Title'] + pagesCounter;
                            }
                            const questionChoices = [t[questionnaire.lang]['Template_Quest_Choice1'],
                                t[questionnaire.lang]['Template_Quest_Choice2'],
                                t[questionnaire.lang]['Template_Quest_Choice3']];

                            currentPage.name = pageName;

                            // Iterate over all rows (including empty rows) in a worksheet
                            worksheet.eachRow({includeEmpty: true}, function (row, rowNumber) {
                                let whatColAmI;
                                if (rowNumber >= DATA_START_ROW) {
                                    // Iterate over all non-null cells in a row.
                                    row.eachCell(function (cell, colNumber) {
                                        // Check column of Category or Question.
                                        if (colNumber === CATEGORY_COL) {
                                            const cellData = cell.value;
                                            // If the number in column 2 is a natural number, this row is category.
                                            if (isNaturalNumber(cellData)) {
                                                whatColAmI = COL_TYPES.Category;
                                            } else {
                                                whatColAmI = COL_TYPES.Question;
                                            }
                                        }
                                        // category name or question data.
                                        else if (colNumber === CATEGORY_COL + 1) {
                                            switch (whatColAmI) {
                                                case (COL_TYPES.Category): {
                                                    if(currentCategory && currentCategory.name)
                                                    {
                                                        currentCategory.elements = currentQuestions;
                                                        currentPageElements.push(currentCategory);
                                                    }
                                                    currentQuestions = [];
                                                    currentCategory = {};
                                                    categoriesCounter++;
                                                    let catgName = cell.value;
                                                    if (!catgName) {
                                                        catgName = t[questionnaire.lang]['SurveyApi_Category_Title'] + categoriesCounter;
                                                    }
                                                    currentCategory.type = 'panel';
                                                    currentCategory.name = catgName;
                                                    break;
                                                }
                                                case (COL_TYPES.Question): {
                                                    let val = '';

                                                    if(typeof cell.value === 'object') {
                                                        if(cell.value.text) {
                                                            val = cell.value.text
                                                        }
                                                        if(cell.value.richText){
                                                            val = convertRichTextToString(cell.value.richText)
                                                        }
                                                    } else {
                                                        val = cell.value;
                                                    }

                                                    currentQuestions.push({
                                                        type: 'radiogroup',
                                                        name: val,
                                                        choices: questionChoices,
                                                        withText: {name: t[questionnaire.lang]['Template_Quest_Comments']},
                                                        weight: 5,
                                                        markedAsImportant: false
                                                    });
                                                    break;
                                                }
                                            }
                                        }
                                        // question weight.
                                        else if (colNumber === CATEGORY_COL + 2 && whatColAmI === COL_TYPES.Question) {
                                            let val;
                                            let questWeight;
                                            if (typeof cell.value === 'object') {
                                                val = cell.value.result;
                                            } else {
                                                val = cell.value;
                                            }
                                            // make sure if it's not a number between 0-10, set default to 5.
                                            if (!val.toString().trim() || !isNaturalNumber(val) || val < 0 || val > 10) {
                                                questWeight = 5;
                                            } else {
                                                questWeight = val;
                                            }
                                            currentQuestions[currentQuestions.length - 1].weight = questWeight;
                                        }
                                    });
                                }
                            });

                            // Add the last questions to the last category.
                            currentCategory.elements = currentQuestions;
                            currentPageElements.push(currentCategory);

                            // Add the page to the questionnaire.
                            currentPage.elements = currentPageElements;
                            questionnaire.pages.push(currentPage);
                        }
                    });

                    return questionnaire;
                })
                .then((questionnaire) => {
                    // In case that a questionnaire is created successfully, save it to DB.
                    if (questionnaire && questionnaire.pages && questionnaire.pages.length > 0) {
                        return saveTemplateToDB(questionnaire);
                    }
                })
                .then((sid) => {
                    // Return the sid to the user.
                    return respond(null, {sid: sid});
                })
                .catch((e) => {
                    console.log(e);
                    return respond();
                });
        } else {
            return respond(null, []);
        }
    };

    const getSurveyWithAnswersTemplateFromExcel = (msg, respond) => {
        // First create the template from the Excel and then add the answer from the same Excel.
        return getSurveyTemplateFromExcel(msg, (err, res) => {
            if (res && res.sid) {
                // Fetch newly created template' pages by sid.
                return promiseAct('role:surveystore, getSurveyBySid:survey', {sid: res.sid})
                    .then((template) => {
                        if (template && template.pages) {
                            // Fetch newly created template' companyName by companyID.
                            return promiseAct('role:companies, get:company', {id: msg.query.surveyCompanyId})
                                .then((cmp) => {
                                    if (cmp && cmp.companyName) {
                                        // Used to insert default paging number if no page name is written in excel.
                                        let pagesCounter = 0;

                                        // Read the message query.
                                        const filePath = msg.query.filePath;
                                        console.log('Getting excel data of : ', filePath);

                                        if (filePath) {
                                            const workbook = new ExcelJS.Workbook();
                                            workbook.xlsx.readFile(filePath)
                                                .then(() => {
                                                    // Check the first worksheet language or set to default.
                                                    let langFound = false;

                                                    const questionnaire = initQuestAnswer(msg, res.sid, template.pages, cmp.companyName);

                                                    // Iterate on excel and add data as pages.
                                                    workbook.eachSheet((worksheet) => {
                                                        if (worksheet) {
                                                            let categoriesCounter = 0;
                                                            let questionsInCategoryCounter = 0;

                                                            pagesCounter++;

                                                            // If it's the first worksheet - Check the language of the excel.
                                                            langFound = setLang(langFound, worksheet, questionnaire);

                                                            const questionChoices = [t[questionnaire.lang]['Template_Quest_Choice1'],
                                                                t[questionnaire.lang]['Template_Quest_Choice2'],
                                                                t[questionnaire.lang]['Template_Quest_Choice3']];

                                                            // Iterate over all rows (including empty rows) in a worksheet
                                                            worksheet.eachRow({includeEmpty: true}, function (row, rowNumber) {
                                                                let whatColAmI;
                                                                if (rowNumber >= DATA_START_ROW) {
                                                                    // Check column of Category or Question.
                                                                    let cellData = worksheet.getRow(rowNumber).getCell(CATEGORY_COL).value;

                                                                    if (cellData) {
                                                                        // If the number in column 2 is a natural number, this row is category.
                                                                        if (isNaturalNumber(cellData)) {
                                                                            whatColAmI = COL_TYPES.Category;
                                                                            categoriesCounter++;
                                                                            questionsInCategoryCounter = 0;
                                                                        } else {
                                                                            whatColAmI = COL_TYPES.Question;
                                                                            questionsInCategoryCounter++;
                                                                        }

                                                                        if (whatColAmI === COL_TYPES.Question &&
                                                                            isQuestionnaireExistingOnSpecificIndexes(questionnaire, pagesCounter, categoriesCounter, questionsInCategoryCounter)) {
                                                                            // Check if any answer exists.
                                                                            cellData = worksheet.getRow(rowNumber).getCell(ANSWER_COL).value;
                                                                            if (cellData) {
                                                                                let answer;
                                                                                // Check if this answer is valid.
                                                                                for (let choice = 1; choice <= questionChoices.length; choice++) {
                                                                                    if (questionChoices[choice - 1].toLowerCase() === cellData.toLowerCase()) {
                                                                                        // A match was found - save the english choice of the answer.
                                                                                        const i18nKey = 'Template_Quest_Choice' + choice;
                                                                                        answer = t[consts.LANG_EN][i18nKey];
                                                                                        break;
                                                                                    }
                                                                                }
                                                                                if (answer) {
                                                                                    questionnaire.pages[pagesCounter - 1].elements[categoriesCounter - 1].elements[questionsInCategoryCounter - 1].answer = answer;
                                                                                }
                                                                            }
                                                                            // Check if any comment exists.
                                                                            cellData = worksheet.getRow(rowNumber).getCell(ANSWER_COL + 1).value;
                                                                            if (cellData) {
                                                                                questionnaire.pages[pagesCounter - 1].elements[categoriesCounter - 1].elements[questionsInCategoryCounter - 1].comment = cellData;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });

                                                    return questionnaire;
                                                })
                                                .then((questionnaire) => {
                                                    // In case that a questionnaire is created successfully, save it to DB.
                                                    if (questionnaire && questionnaire.pages && questionnaire.pages.length > 0) {
                                                        // Firstly, calc the score and count the answers.
                                                        const scoreObj = calcScore(questionnaire.pages);
                                                        questionnaire.score = scoreObj.score;
                                                        questionnaire.counts = scoreObj.countObj;
                                                        questionnaire.noGoAnsweredWrongArr = scoreObj.noGoAnsweredWrongArr || [];

                                                        // Secondly, update the survey's progress.
                                                        questionnaire.progress = serverHelpers.getProgress(questionnaire);

                                                        // Then, save the results to DB.
                                                        return saveSurveyAnswerToDB(questionnaire);
                                                    }
                                                })
                                                .then((said) => {
                                                    // Return the said to the user.
                                                    return respond(null, {said: said});
                                                })
                                                .catch((e) => {
                                                    console.log(e);
                                                    return respond();
                                                });
                                        } else {
                                            return respond(null, []);
                                        }
                                    }
                                })
                                .catch((e) => {
                                    console.log('Error in get company while getting survey template by sid:', e);
                                });
                        }
                    })
                    .catch((e) => {
                        console.log('Error in getSurveyBySid while getting survey template by sid:', e);
                    });
            } else {
                console.log('Error in getSurveyWithAnswersTemplateFromExcel - failed to create the template from uploaded Excel.');
            }
        });
    };

    const calcScore = (pages) => {
        let ComplyScore = 0;
        let weightSum = 0;
        let noGoAnsweredWrongArr = [];
        let countObj = {
            NACount: 0,
            complyCount: 0,
            nonComplyCount: 0,
            questionsCount: 0,
            noGoQuestionsCount: 0,
            noGoAnsweredWrongCount: 0,
            noGoAnsweredRightCount: 0
        };
        let NAscore = 0;
        // Loop through pages.
        for (let k = 0; k < pages.length; k++) {
            // Loop through categories.
            for (let i = 0; i < pages[k].elements.length; i++) {
                if (pages[k].elements && pages[k].elements[i] &&
                    (!pages[k].elements[i].followUpCategoryData ||
                        (pages[k].elements[i].followUpCategoryData && pages[k].elements[i].followUpCategoryData.isShown))) {
                    // Loop through questions in each category.
                    for (let j = 0; j < pages[k].elements[i].elements.length; j++) {
                        let question = pages[k].elements[i].elements[j];
                        weightSum += question.weight;
                        countObj.questionsCount++;

                        if (question.answer === 'Comply') {
                            countObj.complyCount++;
                            ComplyScore += question.weight;
                        } else if (question.answer === 'Other') {
                            countObj.NACount++;
                            NAscore += question.weight;
                        } else if (!question.answer) {
                            countObj.NACount++;
                            NAscore += question.weight;
                        } else {
                            countObj.nonComplyCount++;
                        }

                        if (question.noGoQuestionData
                            && question.noGoQuestionData.isNoGoQuestion
                            && question.noGoQuestionData.noGoCorrectAnswer) {
                            let correctAnswer = this.getCorrectAnswerInText(question.noGoQuestionData.noGoCorrectAnswer);
                            if (correctAnswer && typeof correctAnswer === 'string' && correctAnswer !== '') {
                                countObj.noGoQuestionsCount++;
                                if (!question.answer || (question.answer !== correctAnswer)) {
                                    countObj.noGoAnsweredWrongCount++;
                                    let questionObj = {
                                        questionText: question.name,
                                        vendorAnswer: question.answer || 'Not Answered',
                                        correctAnswer: correctAnswer,
                                        vendorComment: question.comment || ''
                                    };
                                    noGoAnsweredWrongArr.push(questionObj);
                                } else {
                                    countObj.noGoAnsweredRightCount++;
                                }
                            }
                        }
                    }
                }
            }
        }
        return {score: 1 - ((ComplyScore + NAscore) / weightSum), countObj: countObj, noGoAnsweredWrongArr: noGoAnsweredWrongArr};
    };


    const isQuestionnaireExistingOnSpecificIndexes = (questionnaire, pageIndex, categoryIndex, questionInCategoryIndex) => {
        return (questionnaire.pages && questionnaire.pages.length > pageIndex - 1 &&
            questionnaire.pages[pageIndex - 1] && questionnaire.pages[pageIndex - 1].elements &&
            questionnaire.pages[pageIndex - 1].elements.length > categoryIndex - 1 &&
            questionnaire.pages[pageIndex - 1].elements[categoryIndex - 1] &&
            questionnaire.pages[pageIndex - 1].elements[categoryIndex - 1].elements &&
            questionnaire.pages[pageIndex - 1].elements[categoryIndex - 1].elements.length > questionInCategoryIndex - 1 &&
            questionnaire.pages[pageIndex - 1].elements[categoryIndex - 1].elements[questionInCategoryIndex - 1]);
    };

    const isNaturalNumber = (cellValue) => {
        const n = Math.floor(Number(cellValue));
        return !isNaN(n) && String(n) === String(cellValue) && n > 0;
    };

    const createFrontPage = (wb, data, rtl) => {
        let frontPage = ['INTRO', 'Name', 'Company Surveyed', 'Respondent', 'Date of Last Change (in the system)'];
        if(data && data.isUnsafeCompany){
            frontPage.push('Company is unsafe!')
        }
        if (rtl) {
            frontPage = ['מבוא', 'שם הסקר', 'חברה נסקרת', 'שם ממלא השאלון', 'תאריך שינוי אחרון (במערכת)'];
            if(data && data.isUnsafeCompany){
                frontPage.push('הספק אינו מוגן!')
            }
        }

        const opts = {
            sheetView: {
                rightToLeft: rtl
            }
        };

        return promiseAct('role:companies, get:company', {id: data.selectedCompanyId})
            .then((company) => {
                return new Promise((resolve, reject) => {
                    const ws = wb.addWorksheet(frontPage[0], opts);
                    let companyName = '';
                    ws.column(2).setWidth(25);
                    ws.column(3).setWidth(50);

                    if (company && company.companyName) {
                        companyName = company.companyName;
                    }

                    console.log('Creating front page with data:');
                    console.log('Survey Name: ', data.name);
                    console.log('Company name: ', companyName);
                    console.log('Respondent: ', data.respondent);
                    console.log('lastChangedDate: ', data.lastChangedDate);
                    const introDataArr = ['', data.name, companyName, data.respondent, data.lastChangedDate];
                    if(data && data.isUnsafeCompany){
                        introDataArr.push('Company didn\'t answer obligatory questions according to ' + companyName + '\'s requirements.')
                }
                    for (let i = 1; i < frontPage.length; i++) {
                        ws.cell(i + 1, 2).string(frontPage[i]);
                        ws.cell(i + 1, 3).string(introDataArr[i]);
                    }
                    console.log('Done Creating front page.');
                    resolve();
                });
            })
            .catch((e) => {
                console.log('Error when creating front page: ', e);
                Promise.reject(e);
            });
    };

    const exportSurveyToExcel = (msg, respond) => {
        let wb = new xl.Workbook();
        listSurveyWithAnswer({uid: msg.query.uid, said: msg.query.said})
            .then((data) => {

                let curOrgId = msg && msg.query && msg.query.orgId ? msg.query.orgId : '';
                const categoryStyle = wb.createStyle({
                    font: {
                        bold: true
                    },
                    alignment: {
                        wrapText: true,
                        horizontal: 'center'
                    }
                });
                const generalStyle = wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true,
                        size: 50
                    }
                });
                const headerStyle = wb.createStyle({
                    alignment: {
                        wrapText: true,
                        horizontal: 'center',
                        shrinkToFit: true
                    },
                    font: {
                        color: 'white',
                        bold: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#1E88E5'
                    }
                });


                // Add Worksheets to the workbook
                if (data) {
                    const rtl = setExcelDir(data.lang);
                    let columnHeaders = ['Section', 'Category name / Question', 'Answer', 'Comments', 'Evidence A', 'Evidence B', 'Evidence C'];

                    if (rtl) {
                        columnHeaders = ['סעיף', 'שם קטגוריה / שאלה', 'תשובה', 'הערות', 'הוכחה 1', 'הוכחה 2', 'הוכחה 3'];
                    }

                    const opts = {
                        sheetView: {
                            rightToLeft: rtl
                        }
                    };
                    console.log('Data is: ', data);
                    createFrontPage(wb, data, rtl)
                        .then(() => {
                            if (data && data.pages && data.pages.length) {
                                let pages = data.pages;
                                if (msg.query.excelFormat === 'one_sheet') {
                                    pages = helpers.concatPages(data);
                                }

                                // Add Executive Summary
                                const importantQuestions = reportHelpers.getSurveyImportantQuestions(data);
                                if (importantQuestions && Array.isArray(importantQuestions) && importantQuestions.length) {
                                    let executiveSummaryTitle, questionHeaders;
                                    if (rtl) {
                                        executiveSummaryTitle = 'שאלות שסומנו כחשובות';
                                        questionHeaders = ['שאלה', 'סטטוס', 'הערה', 'הוכחה 1', 'הוכחה 2', 'הוכחה 3'];
                                    } else {
                                        executiveSummaryTitle = 'Important Questions';
                                        questionHeaders = ['Question', 'Status', 'Comment', 'Evidence A', 'Evidence B', 'Evidence C'];
                                    }

                                    const ws = wb.addWorksheet(executiveSummaryTitle.toUpperCase(), opts);
                                    ws.column(1).setWidth(30);
                                    ws.column(3).setWidth(30);
                                    ws.column(4).setWidth(30);
                                    ws.column(5).setWidth(30);
                                    ws.column(6).setWidth(30);

                                    // Headers
                                    for (let i = 0; i < questionHeaders.length; i++) {
                                        ws.cell(2, i + 1).string(questionHeaders[i]).style(headerStyle);
                                    }

                                    // Start positioning important questions
                                    for (let j = 0, line = 3; j < importantQuestions.length; j++, line++) {
                                        const question = importantQuestions[j];
                                        if (question && question.name) {
                                            ws.cell(line, 1).string(question.name).style(generalStyle);
                                            if (question.answer) {
                                                ws.cell(line, 2).string(question.answer).style(generalStyle);
                                            }

                                            if (question.comment) {
                                                ws.cell(line, 3).string(question.comment).style(generalStyle);
                                            }

                                            if (question.files) {
                                                for (let i = 0, column = 4; i < question.files.length; i++, column++) {
                                                    const fileName = question.files[0].path.replace('/', '');
                                                    ws.cell(line, column).style(generalStyle).link(config.addresses.website + '/evidence' + question.files[0].path, fileName);
                                                }
                                            }
                                        }
                                    }
                                }

                                //Create No Go questions tab only if company related to organization and is unsafe
                                if (curOrgId && curOrgId !== 'No Id' && data.orgId && curOrgId === data.orgId && data.isUnsafeCompany) {
                                    wb = excelHelpers.addNoGoQuestionsToExcel([data], wb, {
                                        headerStyle: headerStyle,
                                        generalStyle: generalStyle
                                    }, false, rtl, opts);
                                }

                                for (let i = 0; i < pages.length; i++) {
                                    const ws = wb.addWorksheet(pages[i].name.toUpperCase(), opts);
                                    ws.column(2).setWidth(30);
                                    ws.column(4).setWidth(30);
                                    ws.column(5).setWidth(30);
                                    ws.column(6).setWidth(30);
                                    ws.column(7).setWidth(30);

                                    const page = pages[i];
                                    let sectionNum = 1;

                                    // Headers
                                    for (let i = 0; i < columnHeaders.length; i++) {
                                        ws.cell(2, i + 1).string(columnHeaders[i]).style(headerStyle);
                                    }

                                    // Categories
                                    for (let k = 0, line = 3; k < page.elements.length; k++, line++) {
                                        const category = page.elements[k];
                                        //check if category exists and if the category is a follow up/nested type and if its shown(activated on the survey)
                                        if (category.elements.length > 0 && (!category.followUpCategoryData || (category.followUpCategoryData && category.followUpCategoryData.isShown))) {
                                            // Set Category name
                                            ws.cell(line, 1).number(sectionNum).style(categoryStyle);
                                            ws.cell(line, 2).string(category.name).style(categoryStyle);

                                            // Start positioning questions
                                            line++;
                                            let sectionSubNum = sectionNum;
                                            for (let j = 0; j < category.elements.length; j++, line++) {
                                                const question = category.elements[j];
                                                if (question && question.name) {
                                                    sectionSubNum += 0.1;
                                                    ws.cell(line, 1).number(sectionSubNum).style(generalStyle);
                                                    ws.cell(line, 2).string(question.name).style(generalStyle);
                                                    if (question.answer) {
                                                        ws.cell(line, 3).string(question.answer).style(generalStyle);
                                                    }

                                                    if (question.comment) {
                                                        ws.cell(line, 4).string(question.comment).style(generalStyle);
                                                    }

                                                    if (question.files) {
                                                        for (let i = 0, column = 5; i < question.files.length; i++, column++) {
                                                            const fileName = question.files[0].path.replace('/', '');
                                                            ws.cell(line, column).style(generalStyle).link(config.addresses.website + '/evidence' + question.files[0].path, fileName);
                                                        }
                                                    }
                                                }
                                            }

                                            // Inc category section number
                                            sectionNum++;
                                        }
                                    }
                                }
                                return [wb, data.name + '.xlsx'];
                            } else {
                                console.log('createFrontPage Error: No data received');
                                Promise.reject();
                            }
                        })
                        .catch((e) => {
                            console.log('createFrontPage Error:', e);
                            respond(null, e);
                        })
                        .spread((wb, fileName) => {
                            return [wb.writeToBuffer(), fileName];
                        })
                        .catch((e) => {
                            console.log('exportSurveyToExcel - Save buffer to DB error ', e);
                            respond(null, e);
                        })
                        .spread((buffer, fileName) => {
                            const app_entity = this.make$('surveyFiles', 'surveyFile', 'map');
                            const save$ = Promise.promisify(app_entity.save$, {context: app_entity});
                            return save$({
                                said: msg.query.said,
                                fileName: fileName,
                                lastChangedDate: data.lastChangedDate,
                                buffer: buffer
                            });
                        })
                        .then((survey) => {
                            respond(null, {id: survey.id});
                        })
                        .catch((e) => {
                            console.log('exportSurveyToExcel - Write to buffer error ', e);
                            respond(null, e);
                        });
                } else {
                    respond(null, {});
                }
            });
    };

    this.ready(() => {
        this.add('role:survey, get:surveyWithAnswers', getsurveyWithAnswers);
        this.add('role:survey, get:getSurveyTemplateFromExcel', getSurveyTemplateFromExcel);
        this.add('role:survey, get:getSurveyWithAnswersTemplateFromExcel', getSurveyWithAnswersTemplateFromExcel);
        this.add('role:survey, export:surveyToExcel', exportSurveyToExcel);
    });
};
