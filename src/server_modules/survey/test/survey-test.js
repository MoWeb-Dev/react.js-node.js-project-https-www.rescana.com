const Seneca = require('seneca');
const chai = require('chai');
const expect = chai.expect;
const helpers = require('../survey-helpers');
const surveyObj = require('./survey-object');

describe('get survey answers', function() {
    describe('role:survey, get:surveyWithAnswers', function(fin) {
        const seneca = test_seneca(fin);
        it('when survey answers ("said") exists without questions from template - should add template data', function(fin) {
            setTimeout(() => {
                // Create a Seneca instance for testing.
                seneca
                    .gate()
                    // Send an action, and validate the response.
                    .act({
                        role: 'survey',
                        get: 'surveyWithAnswers',
                        query: {
                            uid: 1,
                            said: 'abcd'
                        }
                    }, function(ignore, result) {
                        return expect(result).to.be.an('object');
                    })

                    // Under gating, `ready` will wait until all actions have completed.
                    .ready(fin);
            }, 300);
        });
    });
});

describe('role:survey, export:surveyAnswersToExcel', function() {
    it('Should receive said and create file', function(fin) {
        const seneca = test_seneca(fin);
        this.timeout(5000);
        setTimeout(() => {
            // Create a Seneca instance for testing.
            seneca
                .gate()
                // Send an action, and validate the response.
                .act({
                    role: 'survey',
                    export: 'surveyToExcel',
                    query: {
                        uid: 1,
                        said: 'abcd'
                    },
                    pin: 'role:survey'
                }, function(ignore, result) {
                    return expect(result.id).to.be.string;
                })
                // Under gating, `ready` will wait until all actions have completed.
                .ready(fin);
        }, 300);
    });

    it('Should receive said and create excel file rtl', function(fin) {
        const seneca = test_seneca(fin);
        this.timeout(5000);
        setTimeout(() => {
            seneca
                .gate()
                // Send an action, and validate the response.
                .act({
                    role: 'survey',
                    export: 'surveyToExcel',
                    query: {
                        uid: 1,
                        said: 'abcdee'
                    },
                    pin: 'role:survey'
                }, function(ignore, result) {
                    return expect(result.id).to.be.string;
                })
                // Under gating, `ready` will wait until all actions have completed.
                .ready(fin);
        }, 300);
    });
});

describe('concatPages() - Should concat all pages to one page', function() {
    it('Should concat all pages to one page', function() {
        const pages = helpers.concatPages(surveyObj.surveyObj2[0]);
        expect(pages).to.be.an('array');
        return expect(pages.length).to.equal(1);
    });

    it('Should handle a survey without pages', function() {
        const pages = helpers.concatPages(surveyObj.surveyObj1[0]);
        expect(pages[0].elements.length).to.equal(0);
        return expect(pages[0].elements).to.be.an('array').that.is.empty;
    });
});

// Construct a Seneca instance suitable for unit testing
function test_seneca(fin) {
    return Seneca({log: 'test'})

    // activate unit test mode. Errors provide additional stack tracing context.
    // The fin callback is called when an error occurs anywhere.
        .test(fin)

        .use('basic')

        .use('entity')

        // Load the microservice business logic
        .use(require('../survey.js'))

        // Define mock messages that the business logic needs
        .add('role:surveystore, list:surveyAnswers', function(msg, reply) {
            reply(null, surveyObj.surveyObj2);
        })

        .add('role:surveystore, listById:survey', function(msg, reply) {
            reply(null, surveyObj.surveyObj2);
        })

        .add('role:companies, get:company', function(msg, reply) {
            reply(null, surveyObj.elCamino);
        });
}


