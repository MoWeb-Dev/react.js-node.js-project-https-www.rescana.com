'use strict';

const Cronjob = require('cron').CronJob;
const Promise = require('bluebird');
const config = require('app-config');
const mongoDbQueue = require('mongodb-queue');
const mongodb = require('mongodb');
const timeConfig = require('../../config/time-config.json');
const moment = require('moment');
const async = require('async');
const neo4jDriver = require('neo4j-driver').v1;
const neo4jRetried = require('@ambassify/neo4j-retried');
const companyCollectionName = 'company_map';
const scansCollectionName = 'scan_map';

const sampleJobInterval = 1200000; // 20 minutes
const neo4j = neo4jRetried(neo4jDriver.driver(config.addresses.neo4jUrl, neo4jDriver.auth.basic(config.consts.neo4jUser, config.consts.neo4JPass),
    {
        maxConnectionLifetime: 20 * 60 * 1000,
        connectionTimeout: 1000 * 45,
        connectionAcquisitionTimeout: 600000,
        maxTransactionRetryTime: 10000,
        connectionPoolSize: 1000
    }), {
    shouldRetry: [neo4jRetried.errors.Transaction.DeadlockDetected]
});
const pForever = require('p-forever');

module.exports = function Schedule() {
    let emailHarvestQueue;
    let companyCollection;
    let scansCollection;

    mongodb.MongoClient.connect(config.addresses.jobConnectionString, (err, db) => {
        emailHarvestQueue = mongoDbQueue(db, 'email_harvest_queue');
    });

    mongodb.MongoClient.connect(config.addresses.mongoConnectionString, async (err, client) => {
        const darkvisionDb = client.db('darkvision');
        scansCollection = darkvisionDb.collection(scansCollectionName);
        companyCollection = darkvisionDb.collection(companyCollectionName);
    });

    const promiseAct = Promise.promisify(this.act, {context: this});
    let STALE_TIME = timeConfig.STALE_TIME;

    if (!STALE_TIME) {
        STALE_TIME = 86400000;
    }

    let staleCustomerApps = [], stalePublicApps = [];

    this.ready(() => {
        console.log('Starting Cronjob timers');
        initTimers();

        // harvest emails forever.
        pForever(() => {
            return getAllPotentialHostnames()
                .then((domains) => {
                    return harvestEmails(domains);
                });
        });

        samplingScanJobs();

        // This is a function that runs in a never ending loop.
        console.log('Running runHibp()');
        runHibp();
        this.add('role:scheduler, hibp:notifyDone', (msg, respond) => {
            const runResult = (msg && msg.noData) ? 'No emails were found' : 'runHibp() is done';
            console.log(runResult, ', trying again in 5 minutes.');
            respond();
            setTimeout(() => {
                console.log('Running runHibp()');
                runHibp();
            }, 300000);
        });

        this.add('role:scheduler, collect:notifyDone', (msg, respond) => {
            console.log('IntelJob done');
            respond();
        });
    });

    // hourly get the apps, and run everything on them
    const appCompareJob = () => {
        let compareAppPid = 0, runCompare = true;
        console.log('appCompareJob run');

        // Check what is the compare apps process id.
        this.act('role:compareApps, return:process', (err, res) => {
            if (res) {
                compareAppPid = res.pid;
            }

            console.log('compareAppPid is: ', compareAppPid);

            // Check if the compare apps process id is in the process map.
            this.act('role:processMap, list:process', {query: compareAppPid}, (err, res) => {
                if (res && res.length > 0) {
                    console.log('process map returned: ', res);
                    runCompare = false;
                } else {
                    runCompare = true;
                }

                if (runCompare) {
                    this.act('role:customerstore,stalelist:app', {timestamp: STALE_TIME}, function(err, res) {
                        console.log('Pulling stale customer apps.');
                        if (res) {
                            console.log('Found stale customer apps.');
                            staleCustomerApps = res.data;
                            // runReenrich(staleCustomerApps);
                            this.act('role:publicstore, stalelist:app', {timestamp: STALE_TIME}, function(err, res) {
                                console.log('Pulling stale public apps.');
                                stalePublicApps = res.data;
                                compareApps(staleCustomerApps, stalePublicApps);
                                /*
                                 recollectTrackedApps(StalePublicApps);
                                 activeSearches(StaleCustomerApps); */
                            });
                        }
                    });
                }
            });
        });
    };

    const collectSslCertificatesJob = () => {
        console.log('in collectSslCertificatesJob');
        // Get all domains related SSL Certificates.
        return promiseAct('role:sslCertificatesCollect, cmd:collectSSLCerts', {})
            .then((res) => {
                if (res && res.ok) {
                    console.log('Successfully started to collect SSL certificates data for all domains');
                } else {
                    console.log('Failed to start collecting SSL certificates data for all domains');
                }
            })
            .catch((e) => {
                console.log('Error in collectSslCertificatesJob: ', e);
            });
    };

    const collectGDPRComplianceJob = () => {
        console.log('in collectGDPRComplianceJob');
        // Get all domains related SSL Certificates.
        return promiseAct('role:gdprComplianceCollect, cmd:collectGDPR', {})
            .then((res) => {
                if (res && res.ok) {
                    console.log('Successfully started to collect GDPR results for all domains');
                } else {
                    console.log('Failed to start collecting GDPR results for all domains');
                }
            })
            .catch((e) => {
                console.log('Error in collectGDPRComplianceJob: ', e);
            });
    };

    const collectRssFeedsJob = () => {
        console.log('in collectRssFeedsJob');
        // Get all companies related feeds.
        return promiseAct('role:rssFeeds, cmd:collectFeeds', {})
            .then((res) => {
                if (res && res.ok) {
                    console.log('Successfully started to collect data from RSS feeds');
                } else {
                    console.log('Failed to start collecting data from RSS feeds');
                }
            })
            .catch((e) => {
                console.log('Error in collectRssFeedsJob: ', e);
            });
    };

    const collectBlacklistsJob = () => {
        console.log('in collectBlacklistsJob');
        // Get all companies.
        return promiseAct('role:companies, list:company', {query: {}})
            .then((res) => {
                // Get all domains.
                const allDistinctDomains = [];
                if (res && Array.isArray(res)) {
                    res.map((currCompany) => {
                        if (currCompany && currCompany.selectedDomains) {
                            currCompany.selectedDomains.map((currDomain) => {
                                if (currDomain && !allDistinctDomains.includes(currDomain)) {
                                    allDistinctDomains.push(currDomain);
                                }
                            });
                        }
                    });
                }
                console.log('Getting blacklists for all ', allDistinctDomains.length, ' domains');

                return promiseAct('role:blacklistsSearcher, cmd:collectAllBlacklists', {domains: allDistinctDomains});
            })
            .then((res) => {
                if (res && res.ok) {
                    console.log('Blacklists collect in progress at blacklists-service.');
                } else {
                    console.log('Error in collectBlacklistsJob: Check blacklists-service logs for more information.');
                }
            })
            .catch((e) => {
                console.log('Error in collectBlacklistsJob: ', e);
            });
    };

    const deleteOutdatedBlacklistsJob = () => {
        console.log('in deleteOutdatedBlacklistsJob');
        // Delete findings older than a week.
        return promiseAct('role:blacklists, cmd:deleteOutdatedDomainsFindings', {})
            .then((res) => {
                if (res && res.ok) {
                    if (res.deletedCount) {
                        console.log('Successfully deleted ', res.deletedCount, ' outdated domains\' blacklists findings');
                    } else {
                        console.log('No outdated domains\' blacklists findings were found');
                    }
                } else {
                    let errMsg = 'Failed to delete outdated blacklists findings';
                    if (res && res.err) {
                        errMsg += ': ' + res.err;
                    }
                    console.log(errMsg);
                }
            })
            .catch((e) => {
                console.log('Error in deleteOutdatedBlacklistsJob: ', e);
            });
    };

    const samplingUploadedFilesScansJob = () => {
        console.log('in samplingUploadedFilesScansJob');
        // Sample all unfinished scans files.
        return promiseAct('role:fileScans, updateScanUploadedFiles:fileScan', {lastMonth: true})
            .then((res) => {
                if (res && res.ok) {
                    console.log('Successfully sampled the uploaded files scans.');
                } else {
                    let logMsg = 'Error has occurred while sampling the uploaded files scans';
                    if (res && res.err) {
                        logMsg += ': ' + res.err;
                    }
                    console.log(logMsg);
                }
            })
            .catch((e) => {
                console.log('Error in samplingUploadedFilesScansJob: ', e);
            });
    };

    const samplingMaliciousScansUploadedFilesJob = () => {
        console.log('in samplingMaliciousScansUploadedFilesJob');
        // Sample all scanned uploaded files from last month for malicious findings.
        return promiseAct('role:fileScans, getMaliciousUploadedFiles:fileScan', {lastMonth: true})
            .then((res) => {
                if (res && res.ok && res.data) {
                    console.log('Successfully sampled the uploaded files scans');

                    if (res.data.length === 0) {
                        console.log('No malicious findings were found on last month uploaded files');
                    } else {
                        // Send to admin the malicious Findings.
                        return promiseAct('role:alertSystem, alert:sendToAdminMaliciousUploadedFiles', {files: res.data})
                            .then((res) => {
                                if (res && res.ok) {
                                    console.log('Successfully notified admin that malicious files were uploaded to surveys');
                                } else {
                                    let logMsg = 'Error notifying admin that malicious files were uploaded to surveys';
                                    if (res && res.err) {
                                        logMsg += ': ' + res.err;
                                    }
                                    console.log(logMsg);
                                }
                            })
                            .catch((e) => {
                                console.log('Error in sendToAdminMaliciousUploadedFiles: ', e);
                            });
                    }
                } else {
                    let logMsg = 'Error has occurred while sampling the uploaded files scans';
                    if (res && res.err) {
                        logMsg += ': ' + res.err;
                    }
                    console.log(logMsg);
                }
            })
            .catch((e) => {
                console.log('Error in samplingMaliciousScansUploadedFilesJob: ', e);
            });
    };

    const samplingScanJobs = () => {
        setInterval(() => {
            console.log('Sampling scans...');
            //Get all jobs that are not done.

            scansCollection.find(
                {percentageDone: {$ne: 100}}
            ).toArray((err, docs) => {
                console.log('Found the following jobs');
                docs.forEach((job) => {
                    let jobKeys = Object.keys(job);
                    let domainStartCounter = 0;
                    let domainEndCounter = 0;
                    let shodanStartCounter = 0;
                    let shodanEndCounter = 0;
                    let d2sStartCounter = 0;
                    let d2sEndCounter = 0;

                    // Count all counters
                    if (jobKeys && jobKeys.length > 0) {
                        for (let i = 0; i < jobKeys.length; i++) {
                            if (jobKeys[i].indexOf('domain-collect-topic') > -1 && jobKeys[i].indexOf('StartCounter') > -1) {
                                domainStartCounter += job[jobKeys[i]];
                            }
                            if (jobKeys[i].indexOf('domain-collect-topic') > -1 && jobKeys[i].indexOf('EndCounter') > -1) {
                                domainEndCounter += job[jobKeys[i]];
                            }
                            if (jobKeys[i].indexOf('shodan-get-host-topic') > -1 && jobKeys[i].indexOf('StartCounter') > -1) {
                                shodanStartCounter += job[jobKeys[i]];
                            }
                            if (jobKeys[i].indexOf('shodan-get-host-topic') > -1 && jobKeys[i].indexOf('EndCounter') > -1) {
                                shodanEndCounter += job[jobKeys[i]];
                            }
                            if (jobKeys[i].indexOf('domain-to-subdomain-topic') > -1 && jobKeys[i].indexOf('StartCounter') > -1) {
                                d2sStartCounter += job[jobKeys[i]];
                            }
                            if (jobKeys[i].indexOf('domain-to-subdomain-topic') > -1 && jobKeys[i].indexOf('EndCounter') > -1) {
                                d2sEndCounter += job[jobKeys[i]];
                            }
                        }

                        //TODO: Need to add a cool off timer in DB for companies recently checked.
                        //Check that each start counter equals the end counter.
                        if (domainStartCounter === domainEndCounter - 1 && shodanStartCounter === shodanEndCounter) {
                            console.log('scan for:', job.companyName, 'is done!');
                            // Calculate company scores and save to DB.
                            promiseAct('role:intelQuery,cmd:getMaxCVSScore', {
                                companyID: job.companyId,
                                saveScoresOnDB: true
                            })
                                .then(() => {
                                    console.log('Successfully updated company score to companyID: ', job.companyId);
                                })
                                .catch(() => {
                                    console.error('Error updating company score to companyID: ', job.companyId);
                                });
                        } else {
                            console.log('scan for:', job.companyName, 'is at ', job.percentageDone, '%');
                        }

                        let percentageDone = Math.floor((domainStartCounter + shodanStartCounter) / ((domainEndCounter - 1) + shodanEndCounter)) * 100;
                        //Save to scan job collection.
                        scansCollection.updateOne({scanId: job.scanId}, {$set: {percentageDone: percentageDone}});
                        //Save to company for display.
                        companyCollection.updateOne({_id: new mongodb.ObjectID(job.companyId)}, {
                            $set: {
                                percentageDone: percentageDone,
                                lastScanDate: job.date
                            }
                        });
                    }
                });
            });
        }, sampleJobInterval);
    };

    let intelCollectionJob = () => {
        this.ready(async () => {

            console.log('in IntelCollectionJob');
            let res = await promiseAct('role:companies, list:company', {query: {}});

            for (let i = 0; i < res.length; i++) {
                if (res[i] && res[i].companyName) {
                    console.log('Getting intel for: ', res[i].companyName);
                }
            }

            try {
                await promiseAct('role:intel, collectAll:projectFindings', {companies: res, scheduled: true});
                console.log('done sending companies to collection.');
            } catch (e) {
                console.error('Error in intelCollection job: ', e);
            }
        });
    };

    let calculateScoreJob = () => {
        this.ready(async () => {
            let res;
            let data = {};
            data.companyIDs = [];

            console.log('in calculateScoreJob');
            try {
                res = await promiseAct('role:companies, list:company', {query: {}});
                res.map((currComp) => {
                    if (currComp && currComp.id && !data.companyIDs.includes(currComp.id)) {
                        data.companyIDs.push(currComp.id);
                    }
                });
                console.log('Calculating score for all companies');
                await promiseAct('role:intelQuery,cmd:getMaxCVSScore', data);
                console.log('Done calculating scores');
            } catch (e) {
                console.log('Error in calculateScoreJob: ', e);
            }
        });
    };

    const collectNewIntelAlerts = () => {
        console.log('in collectNewIntelAlerts');
        const collect = 'cve';
        const msgObj = {intelType: collect};
        this.act('role:alertSystem, alert:newIntelFound', {data: msgObj}, (err, res) => {
            if (err) {
                console.log(err);
            } else if (res && res.ok && res.data) {
                console.log('Found new ' + collect + ' intel.');

                this.act('role:intelAlerts, setIntelFromArray:intelAlert', {
                    data: res.data
                }, (err, res) => {
                    if (res && res.ok) {
                        console.log('Saved new ' + collect + ' intel to DB');
                    } else if (res && res.err) {
                        console.log('Error saving new ' + collect + ' intel to DB: ' + res.err);
                    } else {
                        console.log('Failed to save new ' + collect + ' intel to DB');
                    }
                });
            } else if (res && !res.ok && res.err) {
                console.log(res.err);
            } else {
                console.log('Unknown error occurred in collectNewIntelAlerts while receiving result.');
            }
        });
    };

    // This function runs once a day and notifies user about new intel that were found.
    const sendNewIntelAlerts = () => {
        console.log('in sendIntelAlerts');
        const collect = 'cve';
        const now = moment().format('YYYY-MM-DD');
        const from = moment(now).subtract(7, 'd').format('YYYY-MM-DD');
        const msgObj = {intelType: collect, collectFrom: from, collectUntil: now};

        // Get all intel of last week.
        this.act('role:intelAlerts, getIntelFound:intelAlert', {data: msgObj}, (err, res) => {
            if (err) {
                console.log('In sendIntelAlerts/getIntelFound - ' + err);
            } else if (res && res.err) {
                console.log('In sendIntelAlerts/getIntelFound - ' + res.err);
            } else if (res && res.data) {
                // Get the intel's domains.
                const domains = [];
                const intelByDomains = [];
                const intelResults = res.data;
                intelResults.map((currIntel) => {
                    const currDomain = currIntel.hostname;
                    if (currDomain) {
                        const domainIndex = domains.indexOf(currDomain);
                        if (domainIndex === -1) {
                            domains.push(currDomain);
                            intelByDomains.push({domain: currDomain, intelFindings: [currIntel]});
                        } else {
                            intelByDomains[domainIndex].intelFindings.push(currIntel);
                        }
                    }
                });
                if (domains.length > 0) {
                    this.act('role:intelAlerts, getUsersWithAlertsByDomains:intelAlert', {data: domains}, (er, re) => {
                        if (er) {
                            console.log('In sendIntelAlerts/getUsersWithAlertsByDomains - ' + er);
                        } else if (re && re.err) {
                            console.log('In sendIntelAlerts/getUsersWithAlertsByDomains - ' + re.err);
                        } else if (re && re.data) {
                            this.act('role:alertSystem, alert:sendToUsersIntelFound', {
                                data: {
                                    intelType: collect,
                                    intelByDomains: intelByDomains,
                                    usersObj: re.data
                                }
                            }, (e, r) => {
                                if (e) {
                                    console.log(e);
                                } else if (r && r.ok) {
                                    console.log('Intel alerts have been successfully sent to related users.');
                                    // Remove processed intel with msgObj.
                                    this.act('role:intelAlerts, deleteIntelFound:intelAlert', {data: msgObj}, (err, res) => {
                                        if (err) {
                                            console.log('In sendIntelAlerts/deleteIntelFound - ' + err);
                                        } else if (res && res.err) {
                                            console.log('In sendIntelAlerts/deleteIntelFound - ' + res.err);
                                        } else if (res && res.ok) {
                                            console.log('Processed intel alerts were successfully deleted.');
                                        } else {
                                            console.log('Failed to run deleteIntelFound in sendNewIntelAlerts');
                                        }
                                    });
                                } else if (r && r.err) {
                                    console.log(r.err);
                                } else {
                                    console.log('Failed to run sendToUsersIntelFound in sendNewIntelAlerts');
                                }
                            });
                        } else {
                            console.log('Failed to run getUsersWithAlertsByDomains in sendNewIntelAlerts');
                        }
                    });
                } else {
                    console.log('No domains found in sendNewIntelAlerts');
                }
            } else {
                console.log('Failed to load ' + collect + ' intel from DB');
            }
        });
    };

    const phishTankCollection = () => {
        console.log('In phishtankJob');
        promiseAct('role:phishtank, get:phishingSites');
    };

    const startCollectBuckets = () => {
        console.log('In startCollectBuckets');
        promiseAct('role:bucketCollector,cmd:findBuckets');
    };

    const createPublicAppFromPhishtank = () => {
        console.log('Finding customer apps in phishtank DB');
        promiseAct('role:phishtank, set:customerAppFromPT');
    };

    const initTimers = () => {
        const compareJob = new Cronjob({
            cronTime: '*/120 * * * * *',
            onTick: function() {
                // console.log('compareJob starting now.');
                // appCompareJob();
            },
            start: false
        });
        compareJob.start();

        const intelJob = new Cronjob({
            cronTime: config.consts.intelScheduler.cron,
            onTick: function() {
                if (config.consts.intelScheduler.run) {
                    console.log('intel Collection starting now.');
                    intelCollectionJob();
                } else {
                    console.log('intel Collection cron flag is on false - not running intel collection scheduler.');
                }
            },
            start: false
        });

        intelJob.start();

        /*        const collectIntelAlertJob = new Cronjob({
                    cronTime: '40 8 * * *', // run every 12 hours.
                    onTick: function() {
                        console.log('Collect Intel Alerts system starting now.');
                        collectNewIntelAlerts();
                    },
                    start: false
                });
                collectIntelAlertJob.start();*/


        // const sendIntelAlertJob = new Cronjob({
        //     cronTime: '7 9 * * *', // run at 21:15 every day.
        //     onTick: function() {
        //         console.log('Send Intel Alerts system starting now.');
        //         sendNewIntelAlerts();
        //     },
        //     start: false
        // });
        // sendIntelAlertJob.start();

        const collectBuckets = new Cronjob({
            cronTime: '0 */4 * * *', // run every 4 hours.
            onTick: function() {
                console.log('Bucket Collection starting now.');
                startCollectBuckets();
            },
            start: false
        });
        collectBuckets.start();

        const fetchPhishtankDB = new Cronjob({
            cronTime: '0 1 * * *',
            onTick: function() {
                console.log('Getting live phishing sites from phishtank');
                phishTankCollection();
            },
            start: false
        });
        fetchPhishtankDB.start();

        const createPublicAppFromPhishtankJob = new Cronjob({
            cronTime: '10 * * * *',
            onTick: function() {
                console.log('Getting live phishing sites from phishtank');
                createPublicAppFromPhishtank();
            },
            start: false
        });
        createPublicAppFromPhishtankJob.start();

        // Try to read config - if failed set default time to weekly.
        let collectBlacklistsCronTime = config.consts.blacklistsCollectCronTime;
        if (!collectBlacklistsCronTime) {
            console.log('Failed to read config value called blacklistsCollectCronTime');
            collectBlacklistsCronTime = '0 2 * * 0'; // default - run every week on Sunday at 2:00.
        }

        const collectBlacklists = new Cronjob({
            cronTime: collectBlacklistsCronTime, // run according to config.
            onTick: function() {
                console.log('Collect Blacklists for all domains starting now.');
                collectBlacklistsJob();
            },
            start: false
        });
        collectBlacklists.start();

        const calculateScores = new Cronjob({
            cronTime: '0 * * * *', // run according to config.
            onTick: function() {
                console.log('Calculate scores.');
                calculateScoreJob();
            },
            start: false
        });
        calculateScores.start();

        const collectBotnets = new Cronjob({
            cronTime: '15 */3 * * *', // run at minute 15 past every 3rd hour (once every 3 hours).
            onTick: function() {
                console.log('Collect Botnets RSS feeds for all IPs starting now.');
                collectRssFeedsJob();
            },
            start: false
        });
        collectBotnets.start();

        const collectSSLCerts = new Cronjob({
            cronTime: '35 6 * * *', // run at 06:35 (once a day).
            onTick: function() {
                console.log('Collect SSL Certificates for all IPs starting now.');
                collectSslCertificatesJob();
            },
            start: false
        });
        collectSSLCerts.start();

        const collectGDPR = new Cronjob({
            cronTime: '55 6 * * *', // run at 06:55 (once a day).
            onTick: function() {
                console.log('Collect GDPR results for all domains starting now.');
                collectGDPRComplianceJob();
            },
            start: false
        });
        collectGDPR.start();

        const removeOutdatedBlacklists = new Cronjob({
            cronTime: '5 8 1 * *', // run 8:05 every month 1st (once a month).
            onTick: function() {
                console.log('Deletion of outdated Blacklists for all domains starting now.');
                deleteOutdatedBlacklistsJob();
            },
            start: false
        });
        removeOutdatedBlacklists.start();

        const samplingUploadedFilesScans = new Cronjob({
            cronTime: '45 * * * *', // run at minute 45. (once an hour).
            onTick: function() {
                console.log('Sampling of unfinished scans for uploaded files at surveys starting now.');
                samplingUploadedFilesScansJob();
            },
            start: false
        });
        samplingUploadedFilesScans.start();

        const samplingMaliciousScansUploadedFiles = new Cronjob({
            cronTime: '0 9 * * 0', // run at 09:00 on Sunday. (once a week).
            onTick: function() {
                console.log('Sampling of malicious scanned files uploaded at surveys starting now.');
                samplingMaliciousScansUploadedFilesJob();
            },
            start: false
        });
        samplingMaliciousScansUploadedFiles.start();

    };

    const compareApps = (staleCustomerApps, stalePublicApps) => {
        if (staleCustomerApps.length > 0 && stalePublicApps.length > 0) {
            console.log('in compareApps function');
            for (let i = 0; i < staleCustomerApps.length; i++) {
                for (let j = 0; j < stalePublicApps.length; j++) {
                    if (staleCustomerApps[i].aid === stalePublicApps[j].aid) {
                        console.log('Comparing! aid:', staleCustomerApps[i].url, ' with ', stalePublicApps[j].url);
                        this.act('role:compareApps,compare:app', {
                            'customerAppId': staleCustomerApps[i].id,
                            'publicAppId': stalePublicApps[j].id
                        }, function() {
                            console.log('done comparing');
                        });
                        return;
                    }
                }
            }
        }
    };

    const removeDuplicatesBy = (keyFn, array) => {
        const mySet = new Set();
        return array.filter(function(x) {
            let key = keyFn(x), isNew = !mySet.has(key);
            if (isNew) mySet.add(key);
            return isNew;
        });
    };

    const getAllPotentialHostnames = () => {
        return new Promise((resolve, reject) => {
            // Delay harvester for 5 minutes to allow scheduler to perform other tasks if necessary.
            console.log('Waiting 5 minutes before next call to getAllPotentialHostnames() for harvester service');
            setTimeout(() => {
                console.log('Starting getAllPotentialHostnames for harvester service');
                const session = neo4j.session();
                const query = 'MATCH path = ((n:domain)) WHERE n.hostname <> "null" RETURN n.hostname as hostname';
                const readTxResultPromise = session.readTransaction((transaction) => {
                    return transaction.run(query);
                });

                return readTxResultPromise
                    .then((result) => {
                        session.close();

                        const data = result.records.map((record) => {
                            const dataObj = {};

                            for (let i = 0; i < record._fields.length; i++) {
                                dataObj[record.keys[i]] = record._fields[i];
                            }

                            return dataObj;
                        });

                        const dataLength = (data && data.length) ? data.length : 'no';
                        console.log('In getAllPotentialHostnames for harvester service - ', dataLength, ' hostnames were returned');

                        return resolve(data);
                    })
                    .catch((error) => {
                        console.error('Error in getAllPotentialHostnames for harvester service: ', error);
                        return reject();
                    });
            }, 300000);
        });
    };

    const harvestEmails = (domains) => {
        return new Promise((resolve, reject) => {
            domains = removeDuplicatesBy((domain) => domain.hostname, domains);
            console.log('Adding ', domains.length, ' distinct domains to email harvest queue');
            for (let i = 0; i < domains.length; i++) {
                emailHarvestQueue.add('emailHarvest_' + domains[i].hostname, (err, id) => {
                    // console.log(id, " is now added to queue");
                });
            }

            console.log('Running on emailHarvestQueue');
            async.eachSeries(domains, (domain, cb) => {
                emailHarvestQueue.get((err, msg) => {
                    const ack = msg.ack;
                    this.act('role:harvester, cmd:run-harvest', {query: {domain: domain.hostname}});

                    this.add('role:scheduler, harvester:notifyDone', (msg, respond) => {
                        // console.log("Deleting: ", ack, " of domain: ", domain.hostname);
                        emailHarvestQueue.ack(ack, (err, id) => {
                            respond();
                            cb();
                        });
                    });
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }

                console.log('All done - harvested all emails.');
                emailHarvestQueue.clean((err) => {
                    if (err) {
                        console.error('Error cleaning emailHarvestQueue: ', err);
                    } else {
                        // All processed (ie. acked) messages have been deleted
                        console.log('Successfully cleaned emailHarvestQueue logs from DB.');
                    }
                    return resolve();
                });
            });
        });
    };

    const getAllEmails = () => {
        return new Promise((resolve, reject) => {
            console.log('Starting getAllEmails for hibp service');
            const session = neo4j.session();
            const query = 'MATCH (n:email) RETURN n.email as email';
            const readTxResultPromise = session.readTransaction((transaction) => {
                return transaction.run(query);
            });

            return readTxResultPromise
                .then((result) => {
                    session.close();

                    const data = result.records.map((record) => {
                        const dataObj = {};

                        for (let i = 0; i < record._fields.length; i++) {
                            dataObj[record.keys[i]] = record._fields[i];
                        }

                        return dataObj;
                    });

                    const dataLength = (data && data.length) ? data.length : 'no';
                    console.log('In getAllEmails for hibp service - ', dataLength, ' emails were returned');

                    return resolve(data);
                })
                .catch((error) => {
                    console.error('Error in getAllEmails for hibp service: ', error);
                    return reject();
                });
        });
    };

    const runHibp = () => {
        return getAllEmails()
            .then((emails) => {
                if (emails.length) {
                    console.log('Collected all emails for hibp service.');
                    return promiseAct('role:hibp,cmd:runMultiHibp', {query: {emails: emails}});
                } else {
                    return promiseAct('role:scheduler, hibp:notifyDone', {noData: true});
                }
            }).catch((e) => {
                console.log('Error in runHibp: ', e);
                return Promise.reject();
            });
    };

};
