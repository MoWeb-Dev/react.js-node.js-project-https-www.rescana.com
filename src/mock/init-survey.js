export default
{
    'name': 'New Survey Template',
    'sid': 'a3f5gh6',
    'pages': [
        {
            'name': 'New Page',
            'elements': [
                {
                    'type': 'panel',
                    'name': 'New Category',
                    'elements': [
                        {
                            'type': 'radiogroup',
                            'name': 'Enter a question here',
                            'choices': [
                                'Non Comply',
                                'Comply',
                                'Other'
                            ],
                            'withText': {
                                'name': 'Comments'
                            },
                            'weight': 5,
                            'markedAsImportant': false
                        }
                    ]
                }
            ]
        }
    ],
    'lang': 'en'
};
