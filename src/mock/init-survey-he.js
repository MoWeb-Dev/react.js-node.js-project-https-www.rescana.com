export default
{
    'name': 'תבנית סקר חדשה',
    'sid': 'a3f5gh6',
    'pages': [
        {
            'name': 'דף 1',
            'elements': [
                {
                    'type': 'panel',
                    'name': 'קטגוריה 1',
                    'elements': [
                        {
                            'type': 'radiogroup',
                            'name': 'הכנס שאלה כאן',
                            'choices': [
                                'לא קיים',
                                'קיים',
                                'אחר'
                            ],
                            'withText': {
                                'name': 'הערות'
                            },
                            'weight': 5,
                            'markedAsImportant': false
                        }
                    ]
                }
            ]
        }
    ],
    'lang': 'he'
};
