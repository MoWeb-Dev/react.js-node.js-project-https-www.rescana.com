'use strict';
const config = require('app-config');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const ObjectID = mongo.ObjectID;
const PromiseB = require('bluebird');
const sha1 = require('sha1');
const moment = require('moment');
const parseDomain = require('parse-domain');
const shortid = require('shortid');
const jwt = require('jsonwebtoken');
const authCfg = require('../server/middleware/auth-config');
const {API_KEY_STATUS, COMPANY_INFO_CLASSIFICATION_TYPES, COMPANY_INFO_SECTOR_TYPES, DEFAULT_INTEL_SCORE_RATIOS, DEFAULT_LANG} = require('../../config/consts.js');
const {extractHostname} = require('../server_modules/utils/utils-helpers.js');
const {getAdminUsers, OnUserCompanyCreateMergeOrgAndCompOnNeo, addNewUserCreatedCompanyToProject, sendNotificationToAdmin} = require('../server/api/api-helpers.js');
const {getSurveyAnswersEmailText} = require('../server/api/helpers.js');
const {fixTimezoneRange} = require('../modules/common/CommonHelpers.js');

const seneca = require('seneca')({timeout: 99999999999})
    .use('basic')
    .use('entity')
    .client({port: 22228, host: 'localhost', pin: 'role:intelQuery', timeout: 999999})
    .client({port: 11112, host: 'localhost', pin: 'role:mailer', timeout: 999999})
    .client({port: 10904, host: config.addresses.storeServiceAddress, timeout: 999999});
const promiseAct = PromiseB.promisify(seneca.act, {context: seneca});

const DB_NAME = 'darkvision';
const COMPANY_COLLECTION = 'company_map';
const SURVEY_ANSWERS_COLLECTION = 'surveyAnswers_map';
const API_KEYS_COLLECTION = 'apiKeys_map';
const ORGANIZATIONS_COLLECTION = 'organizations_map';
const PROJECTS_COLLECTION = 'project_map';
const SURVEY_COLLECTION = 'survey_map';
const TOKENS_COLLECTION = 'tokens_map';
const MAX_FIELD_LENGTH = 200;
const defaultProjectName = 'TPRA';

const listToArray = (list) => {
    return new Promise((resolve) => {
        try {
            list.toArray((err, docs) => {
                resolve(docs);
            });
        }
        catch (e) {
            console.error('Error in listToArray: ', e);
            resolve([]);
        }
    });
};

// This function returns true when input is a number.
const isNumeric = (n) => {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

// This function returns true when input contains only numeric characters.
const isOnlyDigits = (ftxID) => {
    let isDigits = true;
    if (ftxID) {
        for (let i = 0; i < ftxID.length; i++) {
            if(!isNumeric(ftxID.charAt(i))) {
                isDigits = false;
                break;
            }
        }
    }
    return isDigits;
};
module.exports.isOnlyDigits = isOnlyDigits;

const isMongoObjectID = (id) => {
    let isObjectID = false;
    if(id && ObjectID.isValid(id)) {
        try {
            // Another check is required because 12 digits string sometimes can falsely pass ObjectID.isValid.
            const anotherCheck = new ObjectID(id);
            if (anotherCheck.valueOf().toString() === id) {
                isObjectID = true;
            }
        }
        catch (e) {
            console.error('Error in isMongoObjectID with id ', id, ' : ', e);
        }
    }
    return isObjectID;
};

// This function returns true when input is a valid federalTaxID
const isFederalTaxID = (ftxID) => {
    return !!(ftxID && ftxID.length && ftxID.length >= 9 && ftxID.length < MAX_FIELD_LENGTH && !isMongoObjectID(ftxID));
};

// This function returns the matching companyID to given federalTaxID, otherwise empty string is returned.
const getCompanyIDByFederalTax = async (ftxID, db) => {
    let companyIDResult = '';

    if(ftxID && db) {
        try {
            const companiesCollection = db.collection(COMPANY_COLLECTION);

            const pFindCompany = PromiseB.promisify(companiesCollection.findOne, {context: companiesCollection});

            const compDBResult = await pFindCompany({
                'federalTaxID': ftxID
            });

            if (compDBResult && compDBResult['_id']) {
                companyIDResult = compDBResult['_id'].toString();
            }
        }
        catch (e) {
            console.error("Error in getCompanyIDByFederalTax: ", e);
        }
    }

    return companyIDResult;
};

const getHashedKey = (key) => {
    let result;
    if(key) {
        const keySplitted = key.split('.');
        if(keySplitted && keySplitted.length === 2) {
            const prefix = keySplitted[0];
            const hashedKey = sha1(key);
            if(prefix && hashedKey) {
                result = prefix + '.' + hashedKey;
            }
            else {
                result = key;
            }
        }
        else {
            result = key;
        }
    }
    else {
        result = key;
    }
    return result;
};

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !!(email && re.test(email));
};

const extractHostnameForApi = (domain) => {
    let hostname = '';
    if(domain) {
        hostname = extractHostname(domain);
        const WWW = 'www.';
        if(hostname && hostname.startsWith(WWW)) {
            hostname = hostname.substring(WWW.length)
        }
    }
    return hostname;
};

const validatePhoneNumber = (phone) => {
    let isPhoneValid = false;
    if(phone) {
        if(/^(02|03|04|08|09|050|052|053|054|055|056|057|058|059|072|073|074|075|076|077|078|079)[\-\s{1}]?\d{1}[\-\s{1}]?\d{6}$/.test(phone)) {
            // Test if Israel phone.
            isPhoneValid = true;
        }
        else if(/^\+(?:[0-9] ?){6,14}[0-9]$/.test(phone)) {
            // Test if international phone.
            isPhoneValid = true;
        }
        else if(phone.length <= 25) {
            // General validation - Up to 25 numbers seperated by - or + or ( or ).
            const regexCharsToIgnore = /[-\+\(\)]/g;

            const phoneWithoutCharsToIgnore = phone.replace(regexCharsToIgnore, '');

            if(phoneWithoutCharsToIgnore && isOnlyDigits(phoneWithoutCharsToIgnore)) {
                isPhoneValid = true;
            }
        }
    }
    return isPhoneValid;
};

const validateCompanyName = (name) => {
    let isCompanyNameValid = false;
    if(name && typeof name === "string") {
        isCompanyNameValid = true;
    }
    return isCompanyNameValid;
};

const validateCompanySensitivity = (sensitivity) => {
    let isSensitivityValid = false;
    if(sensitivity && isOnlyDigits(sensitivity) && 1 <= Number(sensitivity) && 3 >= Number(sensitivity)) {
        isSensitivityValid = true;
    }
    return isSensitivityValid;
};

// These functions are exported to be tested in unit tests.
module.exports.testFunctions = {
    isNumeric: isNumeric,
    isOnlyDigits: isOnlyDigits,
    isFederalTaxID: isFederalTaxID,
    getHashedKey: getHashedKey,
    validateEmail: validateEmail,
    extractHostnameForApi: extractHostnameForApi,
    validatePhoneNumber: validatePhoneNumber,
    validateCompanySensitivity: validateCompanySensitivity,
    validateCompanyName: validateCompanyName
};

/**
 * This function validates that given API Key exists and active + validates that companyID is allowed by this key.
 * params:  req - The API request header.
 *          db - The open connection to DB.
 * returned: True if API Key is valid, otherwise False.
 * */
const authApiKey = async (req, db) => {
    let isApiKeyValid = false;
    let isScoreBackwardsEnabled = false;

    const apiKeysCollection = db.collection(API_KEYS_COLLECTION);

    const pFindApiKey = PromiseB.promisify(apiKeysCollection.findOne, {context: apiKeysCollection});

    const hashedKey = getHashedKey(req.headers['x-api-key']);

    const apiKeyDBResult = await pFindApiKey({
        'key': hashedKey,
        'status': API_KEY_STATUS.ACTIVE
    });

    // Meaning this key is valid.
    if(apiKeyDBResult && apiKeyDBResult.key) {
        isScoreBackwardsEnabled = apiKeyDBResult.isScoreBackwardsEnabled || false;
        // In case companyID is given - validate that this key is connected to allowed organization.
        if(req.params && req.params.hasOwnProperty('companyID') && apiKeyDBResult.organizations && Array.isArray(apiKeyDBResult.organizations) && apiKeyDBResult.organizations.length > 0) {
            const organizationsObjectIDs = [];
            apiKeyDBResult.organizations.map((currOrg) => {
                if(currOrg && currOrg.id) {
                    const currOrgID = new mongo.ObjectID(currOrg.id);
                    if(!organizationsObjectIDs.includes(currOrgID)) {
                        organizationsObjectIDs.push(currOrgID);
                    }
                }
            });

            const organizationsCollection = db.collection(ORGANIZATIONS_COLLECTION);

            const pFindOrganizations = PromiseB.promisify(organizationsCollection.find, {context: organizationsCollection});

            const list = await pFindOrganizations({'_id': { $in: organizationsObjectIDs}});

            if (list) {
                const resultOrgs = await listToArray(list);
                if (resultOrgs && Array.isArray(resultOrgs) && resultOrgs.length > 0) {
                    const projectsObjectIDs = [];
                    resultOrgs.map((org) => {
                        if(org && org.selectedProjects && Array.isArray(org.selectedProjects)) {
                            org.selectedProjects.map((currProj) => {
                                if(currProj && currProj.id) {
                                    const currProjID = new mongo.ObjectID(currProj.id);
                                    if(!projectsObjectIDs.includes(currProjID)) {
                                        projectsObjectIDs.push(currProjID);
                                    }
                                }
                            });
                        }
                    });

                    const projectsCollection = db.collection(PROJECTS_COLLECTION);

                    const pFindProjects = PromiseB.promisify(projectsCollection.findOne, {context: projectsCollection});

                    const resultProj = await pFindProjects({
                        '_id': { $in: projectsObjectIDs},
                        'selectedCompanies.id': req.params.companyID
                    });

                    if (resultProj && resultProj['_id']) {
                        // Meaning that this companyID exists in hierarchy of organizations -> projects -> companies.
                        isApiKeyValid = true;
                    }
                }
            }
        }
        else {
            // If there is no companyID - this key works as valid for all related companies.
            isApiKeyValid = true;
        }
    }

    return {isApiKeyValid: isApiKeyValid, isScoreBackwardsEnabled: isScoreBackwardsEnabled};
};

// Returns all companies data by drilling down API Key -> organization -> projects -> companies.
const getCompaniesDataByApiKey = async (key, db) => {
    let companiesResult = [];

    if(key) {
        const apiKeysCollection = db.collection(API_KEYS_COLLECTION);

        const pFindApiKey = PromiseB.promisify(apiKeysCollection.findOne, {context: apiKeysCollection});

        const apiKeyDBResult = await pFindApiKey({
            'key': key
        });

        if(apiKeyDBResult && apiKeyDBResult.key && apiKeyDBResult.organizations && Array.isArray(apiKeyDBResult.organizations) && apiKeyDBResult.organizations.length > 0) {
            const organizationsObjectIDs = [];
            apiKeyDBResult.organizations.map((currOrg) => {
                if(currOrg && currOrg.id) {
                    const currOrgID = new mongo.ObjectID(currOrg.id);
                    if(!organizationsObjectIDs.includes(currOrgID)) {
                        organizationsObjectIDs.push(currOrgID);
                    }
                }
            });

            const organizationsCollection = db.collection(ORGANIZATIONS_COLLECTION);

            const pFindOrganizations = PromiseB.promisify(organizationsCollection.find, {context: organizationsCollection});

            const listOrgs = await pFindOrganizations({'_id': { $in: organizationsObjectIDs}});

            if (listOrgs) {
                const resultOrgs = await listToArray(listOrgs);
                if (resultOrgs && Array.isArray(resultOrgs) && resultOrgs.length > 0) {
                    const projectsObjectIDs = [];
                    resultOrgs.map((org) => {
                        if(org && org.selectedProjects && Array.isArray(org.selectedProjects)) {
                            org.selectedProjects.map((currProj) => {
                                if(currProj && currProj.id) {
                                    const currProjID = new mongo.ObjectID(currProj.id);
                                    if(!projectsObjectIDs.includes(currProjID)) {
                                        projectsObjectIDs.push(currProjID);
                                    }
                                }
                            });
                        }
                    });

                    const projectsCollection = db.collection(PROJECTS_COLLECTION);

                    const pFindProjects = PromiseB.promisify(projectsCollection.find, {context: projectsCollection});

                    const listProjs = await pFindProjects({
                        '_id': { $in: projectsObjectIDs}
                    });

                    if(listProjs) {
                        const resultProjs = await listToArray(listProjs);

                        if (resultProjs && Array.isArray(resultProjs) && resultProjs.length > 0) {
                            const companiesObjectIDs = [];

                            resultProjs.map((currProj) => {
                                if(currProj && currProj.selectedCompanies && Array.isArray(currProj.selectedCompanies)) {
                                    currProj.selectedCompanies.map((currProjComp) => {
                                        if(currProjComp && currProjComp.id) {
                                            const currCompID = new mongo.ObjectID(currProjComp.id);
                                            if(!companiesObjectIDs.includes(currCompID)) {
                                                companiesObjectIDs.push(currCompID);
                                            }
                                        }
                                    });
                                }
                            });

                            const companiesCollection = db.collection(COMPANY_COLLECTION);

                            const pFindCompanies = PromiseB.promisify(companiesCollection.find, {context: companiesCollection});

                            const listComps = await pFindCompanies({
                                '_id': { $in: companiesObjectIDs}
                            });

                            if(listComps) {
                                const resultComps = await listToArray(listComps);

                                if (resultComps && Array.isArray(resultComps) && resultComps.length > 0) {
                                    // Update the returned result.
                                    companiesResult = resultComps;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return companiesResult;
};

// Returns organization data by drilling down API Key -> organization.
const getOrganizationDataByApiKey = async (key, db) => {
    const organizationResult = {};

    if(key) {
        const apiKeysCollection = db.collection(API_KEYS_COLLECTION);

        const pFindApiKey = PromiseB.promisify(apiKeysCollection.findOne, {context: apiKeysCollection});

        const apiKeyDBResult = await pFindApiKey({
            'key': key
        });

        if(apiKeyDBResult && apiKeyDBResult.key
            && apiKeyDBResult.organizations && Array.isArray(apiKeyDBResult.organizations)
            && apiKeyDBResult.organizations.length > 0 && apiKeyDBResult.organizations[0].organizationName
            && apiKeyDBResult.organizations[0].id) {
            organizationResult.organizationName = apiKeyDBResult.organizations[0].organizationName;
            organizationResult.id = apiKeyDBResult.organizations[0].id;
        }
    }

    return organizationResult;
};

// Returns default project id after creating it if not exists.
const getDefaultProjectDataByOrganization = async (orgData, newCompanyData, db) => {
    let defaultProjectData = '';

    if(orgData && orgData.id) {
        try {
            const organizationsCollection = db.collection(ORGANIZATIONS_COLLECTION);

            const pFindOrgWithDefaultProj = PromiseB.promisify(organizationsCollection.findOne, {context: organizationsCollection});

            const orgDataID = new mongo.ObjectID(orgData.id);

            const orgWithDefaultProjResult = await pFindOrgWithDefaultProj({
                '_id': orgDataID,
                'selectedProjects.name': defaultProjectName
            });

            if (orgWithDefaultProjResult && orgWithDefaultProjResult.selectedProjects
                && Array.isArray(orgWithDefaultProjResult.selectedProjects)) {
                // If default project exists already - return its ID.
                const matchedProj = orgWithDefaultProjResult.selectedProjects.find((proj) => {
                    return (proj && proj.name && proj.name === defaultProjectName);
                });
                if (matchedProj && matchedProj.id) {
                    // Fetch existing project's data.
                    const projectsCollection = db.collection(PROJECTS_COLLECTION);

                    const pFindDefaultProj = PromiseB.promisify(projectsCollection.findOne, {context: projectsCollection});

                    const defaultProjID = new mongo.ObjectID(matchedProj.id);

                    const defaultProjResult = await pFindDefaultProj({
                        '_id': defaultProjID
                    });

                    if(defaultProjResult && (defaultProjResult._id || defaultProjResult.id)) {
                        let idVal = (defaultProjResult._id || defaultProjResult.id);
                        if (typeof idVal !== "string") {
                            idVal = idVal.valueOf().toString();
                        }
                        defaultProjectData = defaultProjResult;
                        defaultProjectData.id = idVal;
                    }
                }
            }
            else if (newCompanyData && (newCompanyData._id || newCompanyData.id) && newCompanyData.companyName && newCompanyData.allowedUsers && Array.isArray(newCompanyData.allowedUsers)) {
                // If default project does not exist - create it, connect to organization and return its ID.
                let idVal = (newCompanyData._id || newCompanyData.id);
                if (typeof idVal !== "string") {
                    idVal = idVal.valueOf().toString();
                }
                const projectData = {
                    projectName: defaultProjectName,
                    allowedUsers: newCompanyData.allowedUsers,
                    selectedCompanies: [{
                        name: newCompanyData.companyName,
                        id: idVal
                    }],
                    createDate: moment().format()
                };

                const projectsCollection = db.collection(PROJECTS_COLLECTION);

                const pInsertDefaultProj = PromiseB.promisify(projectsCollection.insertOne, {context: projectsCollection});

                const defaultProjResult = await pInsertDefaultProj(projectData);

                if(defaultProjResult && defaultProjResult.insertedId) {
                    idVal = defaultProjResult.insertedId;
                    if (typeof idVal !== "string") {
                        idVal = idVal.valueOf().toString();
                    }
                    const pUpdateDefaultProjInOrg = PromiseB.promisify(organizationsCollection.update, {context: organizationsCollection});

                    await pUpdateDefaultProjInOrg({'_id': orgDataID}, {$addToSet: {'selectedProjects': {
                                name: projectData.projectName,
                                id: idVal
                            }}}, {upsert: true});

                    defaultProjectData = projectData;
                    defaultProjectData.id = idVal;
                }
            }
        }
        catch (e) {
            console.error('Error in getDefaultProjectDataByOrganization: ', e);
        }
    }

    return defaultProjectData;
};

const sendSurveyAnswersEmailFromApi = (surveyAnswers, companyContacts, organizationName, db) => {
    return new Promise(async (resolve) => {
        if(surveyAnswers && companyContacts && companyContacts[0] && companyContacts[0].email && db) {
            try {
                const uid = (surveyAnswers.uid && Array.isArray(surveyAnswers.uid) && surveyAnswers.uid.length > 0) ? surveyAnswers.uid[0] : '';

                const payload = {
                    iat: Math.floor(Date.now() / 1000),
                    uid: uid,
                    senderEmail: surveyAnswers.respondent,
                    surveyName: surveyAnswers.name,
                    said: surveyAnswers.said
                };

                const daysToExpire = 60;

                const expiry = daysToExpire + 'd';

                // Create token.
                const token = jwt.sign(payload, authCfg.jwtSecret, {expiresIn: expiry});

                const tokenCreationTime = moment().format('LL');

                const tokenExpirationTime = moment().add(daysToExpire - 1, 'days').format('LL');

                const tokenData = {
                    uid: uid,
                    userEmail: surveyAnswers.respondent,
                    tokenCreationTime: tokenCreationTime,
                    tokenExpirationTime: tokenExpirationTime,
                    said: surveyAnswers.said,
                    senderEmail: surveyAnswers.respondent,
                    surveyName: surveyAnswers.name,
                    companyName: surveyAnswers.selectedCompanyName,
                    companyId: surveyAnswers.selectedCompanyId,
                    isSentWithEmail: true,
                    receiverEmail: companyContacts[0].email,
                    token: token,
                    isTokenRevoked: false
                };

                const Tcollection = db.collection(TOKENS_COLLECTION);

                const tokenResult = await Tcollection.insert(tokenData, {upsert: true});

                if (tokenResult) {
                    // Create mail to send.
                    const url = config.addresses.websiteForEmail + '/survey-continue?said=' + surveyAnswers.said + '&token=' + token;

                    const text = getSurveyAnswersEmailText(url, tokenExpirationTime, tokenData.companyName, organizationName, null);

                    const msgObj = {
                        to: tokenData.receiverEmail,
                        subject: 'Security survey from ' + tokenData.senderEmail,
                        text: text
                    };

                    await promiseAct('role:mailer, cmd:send', msgObj);

                    console.log('In sendSurveyAnswersEmailFromApi - Auto Survey Email has been successfully sent');

                    resolve({ok: true});
                }
                else {
                    console.log('In sendSurveyAnswersEmailFromApi - failed to create new token');
                    resolve({ok: false});
                }
            }
            catch (e) {
                console.error('Error in sendSurveyAnswersEmailFromApi: ', e);
                resolve({ok: false});
            }
        }
        else {
            console.log('Missing parameters in sendSurveyAnswersEmailFromApi');
            resolve({ok: false});
        }
    });
};

const isVerySensitiveCompany = (companyData) => {
    // TODO: Tzach need to change to organization's sensitivity when published.
    return !!(companyData && companyData.sensitivity && companyData.sensitivity === 3);
};

const sendAutoSurveyFromApi = (companyData, organizationID, companyContacts, db) => {
    return new Promise(async (resolve) => {
        if(companyData && organizationID && companyContacts && Array.isArray(companyContacts) && companyContacts.length > 0 && db) {
            if(isVerySensitiveCompany(companyData)) {
                try {
                    console.log('In sendAutoSurveyFromApi');

                    const organizationsCollection = db.collection(ORGANIZATIONS_COLLECTION);

                    const pFindOrgWithDefaultProj = PromiseB.promisify(organizationsCollection.findOne, {context: organizationsCollection});

                    const orgDataID = new mongo.ObjectID(organizationID);

                    const orgWithNewCompResult = await pFindOrgWithDefaultProj({
                        '_id': orgDataID
                    });

                    if (orgWithNewCompResult && orgWithNewCompResult.isSendAutoSurveyFromApi && orgWithNewCompResult.apiAuditorEmail
                        && orgWithNewCompResult.apiSelectedTemplate && Array.isArray(orgWithNewCompResult.apiSelectedTemplate) &&
                        orgWithNewCompResult.apiSelectedTemplate.length > 0 && orgWithNewCompResult.apiSelectedTemplate[0].sid) {

                        const surveyTemplatesCollection = db.collection(SURVEY_COLLECTION);

                        const pFindSurveyTemplateBySid = PromiseB.promisify(surveyTemplatesCollection.findOne, {context: surveyTemplatesCollection});

                        const surveyTemplateBySidResult = await pFindSurveyTemplateBySid({
                            'sid': orgWithNewCompResult.apiSelectedTemplate[0].sid
                        });

                        if (surveyTemplateBySidResult && surveyTemplateBySidResult.pages) {
                            let selectedCompanyId = (companyData._id || companyData.id);
                            if (typeof selectedCompanyId !== "string") {
                                selectedCompanyId = selectedCompanyId.valueOf().toString();
                            }

                            const allowedUsers = [];
                            if (orgWithNewCompResult.allowedUsers && Array.isArray(orgWithNewCompResult.allowedUsers)) {
                                orgWithNewCompResult.allowedUsers.map((currUser) => {
                                    if (currUser) {
                                        let idVal = (currUser._id || currUser.id);
                                        if (typeof idVal !== "string") {
                                            idVal = idVal.valueOf().toString();
                                        }
                                        allowedUsers.push(idVal);
                                    }
                                });
                            }

                            const today = moment().format();

                            const newSurveyAnswers = {
                                lastChangedDate: moment().format(),
                                sid: orgWithNewCompResult.apiSelectedTemplate[0].sid,
                                selectedCompanyId: selectedCompanyId,
                                selectedCompanyName: companyData.companyName,
                                respondent: orgWithNewCompResult.apiAuditorEmail,
                                name: defaultProjectName + ' - ' + today,
                                said: shortid.generate(),
                                uid: allowedUsers,
                                progress: 0,
                                pages: surveyTemplateBySidResult.pages,
                                lang: surveyTemplateBySidResult.lang || DEFAULT_LANG,
                                score: 0
                            };

                            // Create new surveyAnswer.
                            const newSurveyAnswersResult = await promiseAct('role:surveystore, updateAnswers:surveyAnswers', {
                                said: newSurveyAnswers.said,
                                data: newSurveyAnswers
                            });

                            if (newSurveyAnswersResult && newSurveyAnswersResult.said) {
                                console.log('In sendAutoSurveyFromApi - New surveyAnswers successfully created');

                                // Create token and send email.
                                const tokenResult = await sendSurveyAnswersEmailFromApi(newSurveyAnswers, companyContacts, orgWithNewCompResult.organizationName, db);

                                resolve(tokenResult);
                            }
                            else {
                                console.log('In sendAutoSurveyFromApi - Failed to create new surveyAnswers');
                                resolve({ok: false});
                            }
                        }
                        else {
                            console.log('In sendAutoSurveyFromApi - pFindSurveyTemplateBySid did not fetch any existing result');
                            resolve({ok: false});
                        }
                    }
                    else {
                        console.log('In sendAutoSurveyFromApi - organization did not prefer to send auto survey');
                        resolve({ok: false});
                    }
                }
                catch (e) {
                    console.error('Error in sendAutoSurveyFromApi: ', e);
                    resolve({ok: false});
                }
            }
            else {
                console.log('In sendAutoSurveyFromApi - company is not very sensitive');
                resolve({ok: false});
            }
        }
        else {
            console.log('Missing parameters in sendAutoSurveyFromApi');
            resolve({ok: false});
        }
    });
};

module.exports.getSurveyData = async (req, res) => {
    console.log("In getSurveyData");
    const returnedObj = {message: ''};
    let responseCode = 200;

    // This function only checks that the key exists - validation will happen after logging to DB.
    if (!(req && req.headers && req.headers['x-api-key'])) {
        returnedObj.message = 'No valid API key was inserted';
        // 401 - Unauthorized.
        responseCode = 401;
    }
    else if (!(config && config.addresses && config.addresses.mongoConnectionString)) {
        returnedObj.message = 'No config was found. Maybe NODE_ENV is missing';
        // 500 - Internal Server Error.
        responseCode = 500;
    }
    else {
        const mongoUrl = config.addresses.mongoConnectionString;
        let recentlyExecuted = 'MongoClient.connect';

        try {
            const pConnect = PromiseB.promisify(MongoClient.connect, {context: MongoClient});
            const client = await pConnect(mongoUrl, {useNewUrlParser: true});

            const db = client.db(DB_NAME);

            recentlyExecuted = 'getCompanyIDByFederalTax';

            if (!(req && req.params && req.params.companyID)) {
                returnedObj.message = 'No company identifier was inserted';
                // 400 - Bad Request.
                responseCode = 400;
            }
            else {
                if (isFederalTaxID(req.params.companyID)) {
                    // Change federalTaxId back to companyID.
                    req.params.companyID = await getCompanyIDByFederalTax(req.params.companyID, db);
                }

                if (!req.params.companyID || !mongo.ObjectID.isValid(req.params.companyID)) {
                    returnedObj.message = 'This is not a valid company identifier';
                    // 400 - Bad Request.
                    responseCode = 400;
                }
                else {

                    recentlyExecuted = 'authApiKey';

                    let authApiKeyRes = await authApiKey(req, db);

                    // Check if this key is valid.
                    if (authApiKeyRes && authApiKeyRes.isApiKeyValid) {

                        // Get latest survey scores.
                        const companyCollection = db.collection(COMPANY_COLLECTION);

                        recentlyExecuted = 'companyCollection.findOne';
                        const pFindCompany = PromiseB.promisify(companyCollection.findOne, {context: companyCollection});

                        const result = await pFindCompany({'_id': new mongo.ObjectID(req.params.companyID)});


                        const surveyAnswersCollection = db.collection(SURVEY_ANSWERS_COLLECTION);
                        recentlyExecuted = 'surveyAnswersCollection.findOne';
                        const pFindSurveyAnswers = PromiseB.promisify(surveyAnswersCollection.findOne, {context: surveyAnswersCollection});

                        const latestSurveyResult = await pFindSurveyAnswers(
                            {'selectedCompanyId': req.params.companyID},
                            {sort: {lastChangedDate: -1}});


                        if (result && result.companyName && latestSurveyResult && latestSurveyResult.said) {
                            returnedObj.message = 'Successfully fetched data';

                            // Get the most updated surveyScore.
                            let surveyScore;
                            if (result.scoresData && result.scoresData.surveyData && result.scoresData.surveyData.hasOwnProperty('surveyScore')) {
                                surveyScore = result.scoresData.surveyData.surveyScore;
                            }
                            else if (latestSurveyResult.hasOwnProperty('score')) {
                                surveyScore = Math.round((latestSurveyResult.score || 0) * 100);
                            }
                            else {
                                surveyScore = 0;
                            }

                            const surveyUrl = config.addresses.website + '/survey?said=' + latestSurveyResult.said;

                            returnedObj.surveySummary = {
                                companyName: result.companyName,
                                said: latestSurveyResult.said,
                                url: surveyUrl || '',
                                submitDate: latestSurveyResult.lastChangedDate || '',
                                progress: Math.floor(latestSurveyResult.progress || 0),
                                surveyScore: surveyScore,
                                counts: latestSurveyResult.counts || {}
                            };

                            // 200 - OK.
                            responseCode = 200;
                        }
                        else {
                            returnedObj.message = 'No existing survey was found for this identifier';
                            // 200 - OK.
                            responseCode = 200;
                        }

                        // Close DB connection.
                        client.close();
                    }
                    else {
                        returnedObj.message = 'The inserted API key is not valid for this request';
                        // 401 - Unauthorized.
                        responseCode = 401;

                        // Close DB connection.
                        client.close();
                    }
                }
            }
        }
        catch (e) {
            console.error('Error in getSurveyData-' + recentlyExecuted + ': ', e);
            returnedObj.message = 'Failed to fetch data from ' + recentlyExecuted;
            returnedObj.err = e;
            // 500 - Internal Server Error.
            responseCode = 500;
        }
    }

    console.log("Returning: ", returnedObj);
    // Return the result with status code.
    res
        .status(responseCode)
        .json(returnedObj)
        .end();
};

// Ignored from next parameter because it is not used here.
const getScore = async (req, res, ignore, isGetAllData = false) => {
    if(!isGetAllData) {
        console.log("In getScore");
    }
    const returnedObj = {message: ''};
    let responseCode = 200;

    // This function only checks that the key exists - validation will happen after logging to DB.
    if (!(req && req.headers && req.headers['x-api-key'])) {
        returnedObj.message = 'No valid API key was inserted';
        // 401 - Unauthorized.
        responseCode = 401;
    }
    else if (!(config && config.addresses && config.addresses.mongoConnectionString)) {
        returnedObj.message = 'No config was found. Maybe NODE_ENV is missing';
        // 500 - Internal Server Error.
        responseCode = 500;
    }
    else {
        const mongoUrl = config.addresses.mongoConnectionString;
        let recentlyExecuted = 'MongoClient.connect';

        try {
            const pConnect = PromiseB.promisify(MongoClient.connect, {context: MongoClient});
            const client = await pConnect(mongoUrl, {useNewUrlParser: true});

            const db = client.db(DB_NAME);

            recentlyExecuted = 'getCompanyIDByFederalTax';

            if (!(req && req.params && req.params.companyID) && !isGetAllData) {
                returnedObj.message = 'No company identifier was inserted';
                // 400 - Bad Request.
                responseCode = 400;
            }
            else {
                if (isFederalTaxID(req.params.companyID)) {
                    // Change federalTaxId back to companyID.
                    req.params.companyID = await getCompanyIDByFederalTax(req.params.companyID, db);
                }

                if ((!req.params.companyID || !mongo.ObjectID.isValid(req.params.companyID)) && !isGetAllData) {
                    returnedObj.message = 'This is not a valid company identifier';
                    // 400 - Bad Request.
                    responseCode = 400;
                }
                else {

                    recentlyExecuted = 'authApiKey';

                    let authApiKeyRes = await authApiKey(req, db);

                    // Check if this key is valid.
                    if (authApiKeyRes && authApiKeyRes.isApiKeyValid) {
                        if (isGetAllData) {
                            // Get all scores for this API Key.
                            const resultComps = await getCompaniesDataByApiKey(getHashedKey(req.headers['x-api-key']), db);

                            if (resultComps && Array.isArray(resultComps) && resultComps.length > 0) {
                                returnedObj.companies = [];

                                resultComps.map((currComp) => {
                                    if (currComp && currComp.companyName) {
                                        // Load score from cache. Otherwise return default score which is 0.
                                        let companyScore = (currComp.scoresData && currComp.scoresData.hasOwnProperty('totalScore'))? currComp.scoresData.totalScore : 0;

                                        if(authApiKeyRes.isScoreBackwardsEnabled){
                                            companyScore = 100 - companyScore;
                                        }

                                        let companyID;
                                        if (currComp.federalTaxID) {
                                            companyID = currComp.federalTaxID;
                                        }
                                        else if (currComp._id || currComp.id) {
                                            companyID = (currComp._id || currComp.id);
                                            if (typeof companyID !== "string") {
                                                companyID = companyID.valueOf().toString();
                                            }
                                        }
                                        else {
                                            companyID = '';
                                        }

                                        // Add to result.
                                        returnedObj.companies.push({
                                            companyName: currComp.companyName,
                                            score: companyScore,
                                            companyID: companyID
                                        });
                                    }
                                });

                                returnedObj.message = 'Successfully fetched data';
                                // 200 - OK.
                                responseCode = 200;
                            }
                            else {
                                returnedObj.message = 'No companies were found for your organization';
                                // 200 - OK.
                                responseCode = 200;
                            }
                        }
                        else {
                            // Get score for requested company.
                            const companyCollection = db.collection(COMPANY_COLLECTION);

                            recentlyExecuted = 'companyCollection.findOne';
                            const pFindCompany = PromiseB.promisify(companyCollection.findOne, {context: companyCollection});

                            const companyResult = await pFindCompany({'_id': new mongo.ObjectID(req.params.companyID)});

                            if (companyResult && companyResult.companyName) {
                                returnedObj.message = 'Successfully fetched data';

                                returnedObj.companies = [];

                                // Load score from cache. Otherwise return default score which is 0.
                                let companyScore = (companyResult.scoresData && companyResult.scoresData.hasOwnProperty('totalScore')) ? companyResult.scoresData.totalScore : 0;

                                if(authApiKeyRes.isScoreBackwardsEnabled){
                                    companyScore = 100 - companyScore;
                                }
                                let companyID;
                                if (companyResult.federalTaxID) {
                                    companyID = companyResult.federalTaxID;
                                }
                                else if (companyResult._id || companyResult.id) {
                                    companyID = (companyResult._id || companyResult.id);
                                    if (typeof companyID !== "string") {
                                        companyID = companyID.valueOf().toString();
                                    }
                                }
                                else {
                                    companyID = '';
                                }

                                // Add to result.
                                returnedObj.companies.push({
                                    companyName: companyResult.companyName,
                                    score: companyScore,
                                    companyID: companyID
                                });

                                // 200 - OK.
                                responseCode = 200;
                            }
                            else {
                                returnedObj.message = 'No data was found for this identifier';
                                // 200 - OK.
                                responseCode = 200;
                            }
                        }

                        // Close DB connection.
                        client.close();
                    }
                    else {
                        returnedObj.message = 'The inserted API key is not valid for this request';
                        // 401 - Unauthorized.
                        responseCode = 401;

                        // Close DB connection.
                        client.close();
                    }
                }
            }
        }
        catch (e) {
            console.error('Error in getSurveyData-' + recentlyExecuted + ': ', e);
            returnedObj.message = 'Failed to fetch data from ' + recentlyExecuted;
            returnedObj.err = e;
            // 500 - Internal Server Error.
            responseCode = 500;
        }
    }

    console.log("Returning: ", returnedObj);
    // Return the result with status code.
    res
        .status(responseCode)
        .json(returnedObj)
        .end();
};
module.exports.getScore = getScore;

module.exports.getAllScores = async (req, res) => {
    console.log('In getAllScores');
    // isGetAllData parameter is set to true.
    getScore(req, res, null, true);
};

module.exports.addCompany = async (req, res) => {
    console.log('In addCompany');

    const returnedObj = {message: ''};
    let responseCode = 200;

    // This function only checks that the key exists - validation will happen after logging to DB.
    if (!(req && req.headers && req.headers['x-api-key'])) {
        returnedObj.message = 'No valid API key was inserted';
        // 401 - Unauthorized.
        responseCode = 401;
    }
    else if (!(config && config.addresses && config.addresses.mongoConnectionString)) {
        returnedObj.message = 'No config was found. Maybe NODE_ENV is missing';
        // 500 - Internal Server Error.
        responseCode = 500;
    }
    else {
        const mongoUrl = config.addresses.mongoConnectionString;
        let recentlyExecuted = 'MongoClient.connect';

        try {
            const pConnect = PromiseB.promisify(MongoClient.connect, {context: MongoClient});
            const client = await pConnect(mongoUrl, {useNewUrlParser: true});

            const db = client.db(DB_NAME);

            // Check for mandatory fields.
            if (!(req && req.body && validateCompanyName(req.body.name) && validateCompanySensitivity(req.body.sensitivity)
                && req.body.companyInfo && req.body.companyInfo.companyContacts
                && Array.isArray(req.body.companyInfo.companyContacts) && req.body.companyInfo.companyContacts.length > 0
                && validateEmail(req.body.companyInfo.companyContacts[0].email)
                && req.body.companyInfo.companyContacts[0].name && validatePhoneNumber(req.body.companyInfo.companyContacts[0].phone))) {
                returnedObj.message = 'Some mandatory parameters are missing:';

                // Add failure reasons.
                if(!(req && req.body)){
                    returnedObj.message += ' -Request type is wrong.';
                }
                else {
                    if(!validateCompanyName(req.body.name)) {
                        returnedObj.message += ' -Name is missing or not valid.';
                    }
                    if(!validateCompanySensitivity(req.body.sensitivity)) {
                        returnedObj.message += ' -Sensitivity is not valid.';
                    }
                    if(!(req.body.companyInfo && req.body.companyInfo.companyContacts
                        && Array.isArray(req.body.companyInfo.companyContacts) && req.body.companyInfo.companyContacts.length > 0)) {
                        returnedObj.message += ' -companyInfo.companyContacts array is missing.';
                    }
                    else {
                        if(!validateEmail(req.body.companyInfo.companyContacts[0].email)) {
                            returnedObj.message += ' -One or more companyInfo.companyContacts emails are not valid.';
                        }
                        if(!req.body.companyInfo.companyContacts[0].name) {
                            returnedObj.message += ' -One or more companyInfo.companyContacts names are missing.';
                        }
                        if(!validatePhoneNumber(req.body.companyInfo.companyContacts[0].phone)) {
                            returnedObj.message += ' -One or more companyInfo.companyContacts phones are not valid.';
                        }
                    }
                }

                // 400 - Bad Request.
                responseCode = 400;
            }
            else {
                const newVendorData = {
                    companyName: req.body.name,
                    selectedDomains: [],
                    keywords: [],
                    sensitivity: Number(req.body.sensitivity),
                    ratios: {
                        surveyWeight: 0,
                        intelWeight: 100
                    },
                    intelScoreRatios: DEFAULT_INTEL_SCORE_RATIOS,
                    companyInfo: {
                        companyContacts: [{
                            email: req.body.companyInfo.companyContacts[0].email,
                            name: req.body.companyInfo.companyContacts[0].name,
                            phone: req.body.companyInfo.companyContacts[0].phone
                        }]
                    },
                    createDate: moment().format()
                };

                let shouldStopBecauseOfInvalidFields = false;
                let invalidFieldsReasons = '';

                // Check for mandatory fields length.
                if(newVendorData.companyName.length > MAX_FIELD_LENGTH) {
                    shouldStopBecauseOfInvalidFields = true;
                    invalidFieldsReasons += ' -Name is too long.';
                }
                if(newVendorData.companyInfo.companyContacts[0].email.length > MAX_FIELD_LENGTH) {
                    shouldStopBecauseOfInvalidFields = true;
                    invalidFieldsReasons += ' -One or more companyInfo.companyContacts emails are too long.';
                }
                if(newVendorData.companyInfo.companyContacts[0].name.length > MAX_FIELD_LENGTH) {
                    shouldStopBecauseOfInvalidFields = true;
                    invalidFieldsReasons += ' -One or more companyInfo.companyContacts names are too long.';
                }
                if(newVendorData.companyInfo.companyContacts[0].phone.length > MAX_FIELD_LENGTH) {
                    shouldStopBecauseOfInvalidFields = true;
                    invalidFieldsReasons += ' -One or more companyInfo.companyContacts phones are too long.';
                }

                // Check for not-mandatory fields.
                if(req.body.companyInfo.companyContacts[0].position) {
                    if(req.body.companyInfo.companyContacts[0].position.length > MAX_FIELD_LENGTH) {
                        shouldStopBecauseOfInvalidFields = true;
                        invalidFieldsReasons += ' -One or more companyInfo.companyContacts positions are too long.';
                    }
                    else {
                        newVendorData.companyInfo.companyContacts[0].position = req.body.companyInfo.companyContacts[0].position;
                    }
                }
                if(req.body.companyInfo.companyContacts.length > 1) {
                    for(let i=1; i<req.body.companyInfo.companyContacts.length;i++) {
                        const currContact = req.body.companyInfo.companyContacts[i];
                        if(currContact) {
                            if(validateEmail(currContact.email)) {
                                if(currContact.email.length > MAX_FIELD_LENGTH) {
                                    shouldStopBecauseOfInvalidFields = true;
                                    invalidFieldsReasons += ' -One or more companyInfo.companyContacts emails are too long.';
                                }
                                else {
                                    if (currContact.name) {
                                        if(currContact.name.length > MAX_FIELD_LENGTH) {
                                            shouldStopBecauseOfInvalidFields = true;
                                            invalidFieldsReasons += ' -One or more companyInfo.companyContacts names are too long.';
                                        }
                                        else {
                                            if(validatePhoneNumber(currContact.phone)) {
                                                if(currContact.phone.length > MAX_FIELD_LENGTH) {
                                                    shouldStopBecauseOfInvalidFields = true;
                                                    invalidFieldsReasons += ' -One or more companyInfo.companyContacts phones are too long.';
                                                }
                                                else {
                                                    const newContact = {
                                                        email: currContact.email,
                                                        name: currContact.name,
                                                        phone: currContact.phone
                                                    };
                                                    if(currContact.position) {
                                                        if(currContact.position.length > MAX_FIELD_LENGTH) {
                                                            shouldStopBecauseOfInvalidFields = true;
                                                            invalidFieldsReasons += ' -One or more companyInfo.companyContacts positions are too long.';
                                                        }
                                                        else {
                                                            newContact.position = currContact.position;
                                                        }
                                                    }
                                                    newVendorData.companyInfo.companyContacts.push(newContact);
                                                }
                                            }
                                            else {
                                                shouldStopBecauseOfInvalidFields = true;
                                                invalidFieldsReasons += ' -One or more companyInfo.companyContacts phones are not valid.';
                                            }
                                        }
                                    }
                                    else {
                                        shouldStopBecauseOfInvalidFields = true;
                                        invalidFieldsReasons += ' -One or more companyInfo.companyContacts names are not valid.';
                                    }
                                }
                            }
                            else {
                                shouldStopBecauseOfInvalidFields = true;
                                invalidFieldsReasons += ' -One or more companyInfo.companyContacts emails are not valid.';
                            }
                        }
                    }
                }
                if (isFederalTaxID(req.body.federalTaxID)) {
                    // Check if federalTaxID is unique or not.
                    if(await getCompanyIDByFederalTax(req.body.federalTaxID, db)) {
                        shouldStopBecauseOfInvalidFields = true;
                        invalidFieldsReasons += ' -This federalTaxID is already in use.';
                    }
                    else {
                        newVendorData.federalTaxID = req.body.federalTaxID.toString();
                    }
                }
                else if(req.body.federalTaxID != null) {
                    shouldStopBecauseOfInvalidFields = true;
                    invalidFieldsReasons += ' -This federalTaxID is not valid.';
                }
                if(req.body.domains && Array.isArray(req.body.domains)) {
                    req.body.domains.map((currDomain) => {
                        const extractdDomainToAdd = extractHostnameForApi(currDomain);
                        // Check domain validation.
                        if(extractdDomainToAdd) {
                            const parsedDomainToAdd = parseDomain(extractdDomainToAdd);
                            if(parsedDomainToAdd && parsedDomainToAdd.domain && parsedDomainToAdd.tld) {
                                const domainToAdd = parsedDomainToAdd.domain + '.' + parsedDomainToAdd.tld;
                                if(domainToAdd.length > MAX_FIELD_LENGTH) {
                                    shouldStopBecauseOfInvalidFields = true;
                                    invalidFieldsReasons += ' -One or more domains are too long.';
                                }
                                else if (!newVendorData.selectedDomains.includes(domainToAdd)) {
                                    // Add new domain.
                                    newVendorData.selectedDomains.push(domainToAdd);

                                    // Add keywords automatically.
                                    newVendorData.keywords.push(domainToAdd);
                                }
                            }
                        }
                    });
                }
                if(req.body.templateSurveyID) {
                    newVendorData.templateSurveyID = req.body.templateSurveyID;
                }
                if(req.body.companyInfo.description) {
                    if(req.body.companyInfo.description.length > MAX_FIELD_LENGTH) {
                        shouldStopBecauseOfInvalidFields = true;
                        invalidFieldsReasons += ' -companyInfo.description is too long.';
                    }
                    else {
                        newVendorData.companyInfo.companyDescription = req.body.companyInfo.description;
                    }
                }
                if (req.body.companyInfo.classifications && Array.isArray(req.body.companyInfo.classifications)
                    && req.body.companyInfo.classifications.length > 0) {
                    newVendorData.companyInfo.companyClassifications = [];
                    // Check each classification.
                    req.body.companyInfo.classifications.map((currUserClass) => {
                        if(currUserClass) {
                            const lowerUserClass = currUserClass.toString().toLowerCase();
                            COMPANY_INFO_CLASSIFICATION_TYPES.map((originalClass) => {
                                if(originalClass && originalClass.toLowerCase() === lowerUserClass) {
                                    newVendorData.companyInfo.companyClassifications.push(originalClass);
                                }
                            })
                        }
                    });

                    // If some classification were not matched, notify the user.
                    if(newVendorData.companyInfo.companyClassifications.length !== req.body.companyInfo.classifications.length) {
                        shouldStopBecauseOfInvalidFields = true;
                        invalidFieldsReasons += ' -One or more companyInfo.classifications are not valid.';
                    }
                }
                if (req.body.companyInfo.sectors && Array.isArray(req.body.companyInfo.sectors)
                    && req.body.companyInfo.sectors.length > 0) {
                    newVendorData.companyInfo.companySectors = [];
                    // Check each sector.
                    req.body.companyInfo.sectors.map((currUserSector) => {
                        if(currUserSector) {
                            const lowerUserSector = currUserSector.toString().toLowerCase();
                            COMPANY_INFO_SECTOR_TYPES.map((originalSector) => {
                                if(originalSector && originalSector.toLowerCase() === lowerUserSector) {
                                    newVendorData.companyInfo.companySectors.push(originalSector);
                                }
                            })
                        }
                    });

                    // If some sectors were not matched, notify the user.
                    if(newVendorData.companyInfo.companySectors.length !== req.body.companyInfo.sectors.length) {
                        shouldStopBecauseOfInvalidFields = true;
                        invalidFieldsReasons += ' -One or more companyInfo.sectors are not valid.';
                    }
                }
                if (req.body.companyInfo.dateOfContact && req.body.companyInfo.dateOfContact.startDate
                    && req.body.companyInfo.dateOfContact.endDate) {
                    try {
                        const startDate = moment(req.body.companyInfo.dateOfContact.startDate);
                        const endDate = moment(req.body.companyInfo.dateOfContact.endDate);
                        // Check if dates.
                        if (startDate && endDate && moment.isMoment(startDate) && moment.isMoment(endDate)) {
                            const formattedStartDate = startDate.format();
                            const formattedEndDate = endDate.format();
                            // Check if valid dates.
                            if(formattedStartDate !== 'Invalid date' && formattedEndDate !== 'Invalid date') {
                                // Fix timezone range issues.
                                const fixedStartDate = fixTimezoneRange(formattedStartDate, true);
                                const fixedEndDate = fixTimezoneRange(formattedEndDate, false);
                                newVendorData.companyInfo['companyDateOfContact'] = {
                                    startDate: fixedStartDate.format(),
                                    endDate: fixedEndDate.format()
                                };
                            }
                            else {
                                shouldStopBecauseOfInvalidFields = true;
                                invalidFieldsReasons += ' -companyInfo.dateOfContact dates are invalid.';
                            }
                        }
                        else {
                            shouldStopBecauseOfInvalidFields = true;
                            invalidFieldsReasons += ' -companyInfo.dateOfContact dates are invalid.';
                        }
                    }
                    catch (e) {
                        console.error("Error parsing dateOfContact for API-addCompany: ", e);
                    }
                }

                if(shouldStopBecauseOfInvalidFields) {
                    returnedObj.message = 'Some fields are not valid: ' + invalidFieldsReasons;

                    // 400 - Bad Request.
                    responseCode = 400;

                    // Close DB connection.
                    client.close();
                }
                else {

                    recentlyExecuted = 'authApiKey';
                    let authApiKeyRes = await authApiKey(req, db);

                    // Check if this key is valid.
                    if (authApiKeyRes && authApiKeyRes.isApiKeyValid) {

                        // Add allowed users to be all organization's users and all admins.
                        const uniqueUsersIDs = [];
                        newVendorData.allowedUsers = [];
                        newVendorData.allowedEditUsers = [];

                        recentlyExecuted = 'getAdminUsers';

                        // Get all admins.
                        const adminUsers = await getAdminUsers();
                        if (adminUsers && Array.isArray(adminUsers)) {
                            adminUsers.map((currAdmin) => {
                                if (currAdmin) {
                                    let idVal = (currAdmin._id || currAdmin.id);
                                    if (idVal && (typeof idVal !== "string")) {
                                        idVal = idVal.valueOf().toString();
                                    }
                                    if (idVal && currAdmin.email && !uniqueUsersIDs.includes(idVal)) {
                                        uniqueUsersIDs.push(idVal);
                                        const objToAdd = {email: currAdmin.email, id: idVal};
                                        newVendorData.allowedUsers.push(objToAdd);
                                        newVendorData.allowedEditUsers.push(objToAdd);
                                    }
                                }
                            });
                        }

                        const apiKey = getHashedKey(req.headers['x-api-key']);

                        recentlyExecuted = 'getCompaniesDataByApiKey';

                        // Get all users for this API Key.
                        const resultComps = await getCompaniesDataByApiKey(apiKey, db);

                        if (resultComps && Array.isArray(resultComps) && resultComps.length > 0) {
                            resultComps.map((currComp) => {
                                if (currComp && currComp.allowedUsers && Array.isArray(currComp.allowedUsers)) {
                                    currComp.allowedUsers.map((currUser) => {
                                        if (currUser && currUser.email && currUser.id && !uniqueUsersIDs.includes(currUser.id)) {
                                            uniqueUsersIDs.push(currUser.id);
                                            newVendorData.allowedUsers.push(currUser);
                                            newVendorData.allowedEditUsers.push(currUser);
                                        }
                                    });
                                }
                            });
                        }

                        recentlyExecuted = 'intelQuery-addCompany';

                        // Create the new company.
                        const resAddComp = await promiseAct('role:intelQuery,add:company', {data: newVendorData});

                        if (resAddComp && resAddComp.ok && resAddComp.newCompany) {
                            recentlyExecuted = 'getOrganizationDataByApiKey';

                            // Get organization by API Key.
                            const resultOrg = await getOrganizationDataByApiKey(apiKey, db);
                            if (resultOrg && resultOrg.id && resultOrg.organizationName && resAddComp.newCompany.companyName && (resAddComp.newCompany._id || resAddComp.newCompany.id)) {
                                // Merge organization and related company on GraphDB.
                                await OnUserCompanyCreateMergeOrgAndCompOnNeo(
                                    resultOrg.id,
                                    resultOrg.organizationName,
                                    resAddComp.newCompany.companyName,
                                    resAddComp.newCompany._id || resAddComp.newCompany.id);

                                // Create default project for this new company.
                                const projectData = await getDefaultProjectDataByOrganization(resultOrg, resAddComp.newCompany, db);

                                if (projectData && projectData.id) {
                                    // Add new company to project on mongo.
                                    await addNewUserCreatedCompanyToProject(projectData.id, resAddComp.newCompany);

                                    // Send only first part of api_key identifier.
                                    let apiKeyToSend;
                                    if(apiKey.includes('.')) {
                                        apiKeyToSend = apiKey.split('.')[0];
                                    }
                                    else {
                                        apiKeyToSend = apiKey;
                                    }
                                    // Send notification to admin.
                                    const sendEmailData = {
                                        newProjectsCreatedByUser: [],
                                        userWhoCreateNewCompany: {email: {'API_Key_Prefix': apiKeyToSend}},
                                        chosenOrg: resultOrg,
                                        chosenProj: projectData,
                                        newCompanyData: resAddComp.newCompany
                                    };
                                    const result = await sendNotificationToAdmin(sendEmailData, promiseAct);
                                    if (result && result.ok) {
                                        console.log('In API addCompany - sendNotificationToAdmin() - Notification sent to admins.');
                                    } else {
                                        console.log('In API addCompany - sendNotificationToAdmin() - Failed to send Notification to admins.');
                                    }

                                    // Create surveyAnswer from sid. Then send to company contact.
                                    const autoSurveyResult = await sendAutoSurveyFromApi(resAddComp.newCompany, resultOrg.id, newVendorData.companyInfo.companyContacts, db);

                                    if(autoSurveyResult && autoSurveyResult.ok) {
                                        console.log('In API addCompany - sendAutoSurveyFromApi() - survey successfully sent.');
                                    }
                                }
                            }

                            returnedObj.message = 'Successfully added data';
                            returnedObj.companyID = (resAddComp.newCompany.hasOwnProperty('federalTaxID')) ? resAddComp.newCompany.federalTaxID : resAddComp.newCompany._id || resAddComp.newCompany.id;
                            // 200 - OK.
                            responseCode = 200;
                        }
                        else {
                            returnedObj.message = 'Failed to add data';
                            // 500 - Internal Server Error.
                            responseCode = 500;
                        }

                        // Close DB connection.
                        client.close();
                    }
                    else {
                        returnedObj.message = 'The inserted API key is not valid for this request';
                        // 401 - Unauthorized.
                        responseCode = 401;

                        // Close DB connection.
                        client.close();
                    }
                }
            }
        }
        catch (e) {
            console.error('Error in addCompany-' + recentlyExecuted + ': ', e);
            returnedObj.message = 'Failed to add data from ' + recentlyExecuted;
            // 500 - Internal Server Error.
            responseCode = 500;
        }
    }

    console.log("Returning: ", returnedObj);
    // Return the result with status code.
    res
        .status(responseCode)
        .json(returnedObj)
        .end();
};
