const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const apiHelpers = require('../api-helpers.js');

describe('ApiHelpers', function() {
    describe('isNumeric() - should return true when input is a number', function() {
        const isNumeric = apiHelpers.testFunctions.isNumeric;

        it('Should identify null input as not valid', function() {
            expect(isNumeric()).to.equal(false);
        });
        it('Should identify empty input as not valid', function() {
            expect(isNumeric('')).to.equal(false);
        });
        it('Should identify numeric input as valid', function() {
            expect(isNumeric('123')).to.equal(true);
        });
        it('Should identify not only numeric input as not valid', function() {
            expect(isNumeric('1asd')).to.equal(false);
        });
        it('Should identify float numeric input as valid', function() {
            expect(isNumeric('12.53')).to.equal(true);
        });
    });

    describe('isOnlyDigits() - should return true when input contains only numbers', function() {
        const isOnlyDigits = apiHelpers.testFunctions.isOnlyDigits;

        it('Should identify null input as valid', function() {
            expect(isOnlyDigits()).to.equal(true);
        });
        it('Should identify empty input as valid', function() {
            expect(isOnlyDigits('')).to.equal(true);
        });
        it('Should identify numeric input as valid', function() {
            expect(isOnlyDigits('123')).to.equal(true);
        });
        it('Should identify not only numeric input as not valid', function() {
            expect(isOnlyDigits('1asd')).to.equal(false);
        });
        it('Should identify float numeric input as not valid', function() {
            expect(isOnlyDigits('12.53')).to.equal(false);
        });
    });

    describe('isFederalTaxID() - should return true when input is a valid federalTaxID', function() {
        const isFederalTaxID = apiHelpers.testFunctions.isFederalTaxID;

        it('Should identify null input as not valid', function() {
            expect(isFederalTaxID()).to.equal(false);
        });
        it('Should identify empty input as not valid', function() {
            expect(isFederalTaxID('')).to.equal(false);
        });
        it('Should identify a set of 9 numeric digits as valid federalTaxID', function() {
            expect(isFederalTaxID('123456789')).to.equal(true);
        });
        it('Should identify not only numeric input as not valid', function() {
            expect(isFederalTaxID('1b3d5f7h9')).to.equal(false);
        });
    });

    describe('getHashedKey() - should return a hashed API key', function() {
        const getHashedKey = apiHelpers.testFunctions.getHashedKey;

        it('Should identify null input and return null', function() {
            expect(getHashedKey()).to.equal(undefined);
        });
        it('Should identify empty input and return empty string', function() {
            expect(getHashedKey('')).to.equal('');
        });
        it('Should identify a set of 8 characters followed by a dot and another 35 characters and hash it', function() {
            expect(getHashedKey('qL406FRC.Tw0ReOudZnmo6bgoNtuIS3lQjKsv2LJE46N')).to.equal('qL406FRC.e0704b283fe64eada1080ece4a9be28abae38952');
        });
    });

    describe('validateEmail() - should return if an email is valid or not', function() {
        const validateEmail = apiHelpers.testFunctions.validateEmail;

        it('Should identify null input and return false', function() {
            expect(validateEmail()).to.equal(false);
        });
        it('Should identify empty input and return false', function() {
            expect(validateEmail('')).to.equal(false);
        });
        it('Should identify an valid email and return true', function() {
            expect(validateEmail('email@rescana.com')).to.equal(true);
        });
        it('Should identify an email without @ and return false', function() {
            expect(validateEmail('email.rescana.com')).to.equal(false);
        });
        it('Should identify an email without valid domain and return false', function() {
            expect(validateEmail('email@rescana')).to.equal(false);
        });
    });

    describe('extractHostnameForApi() - should return an extracted hostname for a domain', function() {
        const extractHostnameForApi = apiHelpers.testFunctions.extractHostnameForApi;

        it('Should identify null input and return empty string', function() {
            expect(extractHostnameForApi()).to.equal('');
        });
        it('Should identify empty input and return empty string', function() {
            expect(extractHostnameForApi('')).to.equal('');
        });
        it('Should identify an valid hostname and return it', function() {
            expect(extractHostnameForApi('rescana.com')).to.equal('rescana.com');
        });
        it('Should extract hostname from domain', function() {
            expect(extractHostnameForApi('www.rescana.com')).to.equal('rescana.com');
        });
        it('Should extract hostname from domain with https', function() {
            expect(extractHostnameForApi('https://www.rescana.com')).to.equal('rescana.com');
        });
    });

    describe('validatePhoneNumber() - should return true if phone is valid', function() {
        const validatePhoneNumber = apiHelpers.testFunctions.validatePhoneNumber;

        it('Should identify null input and return false', function() {
            expect(validatePhoneNumber()).to.equal(false);
        });
        it('Should identify empty input and return false', function() {
            expect(validatePhoneNumber('')).to.equal(false);
        });
        it('Should identify a not valid phone with special characters', function() {
            expect(validatePhoneNumber('031234567a')).to.equal(false);
        });
        it('Should identify a not valid phone', function() {
            expect(validatePhoneNumber('03@12345')).to.equal(false);
        });
        it('Should identify a valid phone with \"-\"', function() {
            expect(validatePhoneNumber('03-1234567')).to.equal(true);
        });
        it('Should identify a valid phone without \"-\"', function() {
            expect(validatePhoneNumber('021234567')).to.equal(true);
        });
        it('Should identify a valid cell phone', function() {
            expect(validatePhoneNumber('+(514)-050-1234')).to.equal(true);
        });
        it('Should identify a valid international cell phone', function() {
            expect(validatePhoneNumber('+972501234567')).to.equal(true);
        });
    });

    describe('validateCompanySensitivity() - should return true if sensitivity is valid', function() {
        const validateCompanySensitivity = apiHelpers.testFunctions.validateCompanySensitivity;

        it('Should identify null input and return false', function() {
            expect(validateCompanySensitivity()).to.equal(false);
        });
        it('Should identify empty input and return false', function() {
            expect(validateCompanySensitivity('')).to.equal(false);
        });
        it('Should identify a not valid sensitivity special characters', function() {
            expect(validateCompanySensitivity('03a')).to.equal(false);
        });
        it('Should identify a not valid sensitivity', function() {
            expect(validateCompanySensitivity('1.5')).to.equal(false);
        });
        it('Should identify a valid sensitivity as string', function() {
            expect(validateCompanySensitivity('1')).to.equal(true);
        });
        it('Should identify a valid sensitivity as number', function() {
            expect(validateCompanySensitivity(2)).to.equal(true);
        });
        it('Should identify a valid sensitivity as string while converted to number', function() {
            expect(validateCompanySensitivity('03')).to.equal(true);
        });
    });

    describe('validateCompanyName() - should return true if companyName is valid', function() {
        const validateCompanyName = apiHelpers.testFunctions.validateCompanyName;

        it('Should identify null input and return false', function() {
            expect(validateCompanyName()).to.equal(false);
        });
        it('Should identify empty input and return false', function() {
            expect(validateCompanyName('')).to.equal(false);
        });
        it('Should identify a not valid input type', function() {
            expect(validateCompanyName({})).to.equal(false);
        });
        it('Should identify a not valid name', function() {
            expect(validateCompanyName(1.5)).to.equal(false);
        });
        it('Should identify a valid name as string', function() {
            expect(validateCompanyName('company 1.5')).to.equal(true);
        });
    });
});