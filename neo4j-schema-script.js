
// The script build indexes and constraints in the neo4j database
// The constraints also prevents duplicate nodes
// the script should be run whenever we deploy new version
// Remark - in neo4j, when trying to create an index that is already exist, it is ok. Means it will not take
// time due to rebuild.
// added constraints

const config = require('app-config');

const neoAddress = config.addresses.neo4jUrl;
const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver(neoAddress + ':7687', neo4j.auth.basic('neo4j', 'Yuval4j'));
const session = driver.session();

const queriesToRun='CREATE CONSTRAINT ON (p:ports) ASSERT p.portsIP IS UNIQUE\n' +
    'CREATE CONSTRAINT ON (s:subdomain) ASSERT s.hostname IS UNIQUE\n' +
    'CREATE CONSTRAINT ON (s:scan) ASSERT s.scanId IS UNIQUE\n' +
    'CREATE CONSTRAINT ON (s:domain) ASSERT s.hostname IS UNIQUE\n' +
    'CREATE CONSTRAINT ON (s:cpe) ASSERT s.cpe IS UNIQUE\n' +
    'CREATE CONSTRAINT ON (s:cve) ASSERT s.cve IS UNIQUE\n' +
    'CREATE INDEX ON :mechanisms(prefix, prefixdesc, type, description, related, cluster, name )\n' +
    'CREATE INDEX ON :IP(address)\n' +
    'CREATE INDEX ON :spf(spf)\n' +
    'CREATE INDEX ON :dmarc(dmarc)\n' +
    'CREATE INDEX ON :dmarc(related)\n' +
    'CREATE INDEX ON :range(name, finding)\n' +
    'CREATE INDEX ON :ASN(name, finding)\n' +
    'CREATE INDEX ON :countryCode(name, finding)\n' +
    'CREATE INDEX ON :hash(name, finding)\n' +
    'CREATE INDEX ON :registrar(name, finding)\n' +
    'CREATE INDEX ON :description(name, finding)\n' +
    'CREATE INDEX ON :ports(ports, IP)\n' +
    'CREATE INDEX ON :isp(name, finding)\n' +
    'CREATE INDEX ON :transport(name, finding)\n' +
    'CREATE INDEX ON :bucket(bucket)\n' +
    'CREATE INDEX ON :cve(cve)\n' +
    'CREATE INDEX ON :domain(hostname)\n' +
    'CREATE INDEX ON :subdomain(hostname)\n' +
    'CREATE INDEX ON :COMPANY(cid)\n' +
    'CREATE INDEX ON :country_code(finding)\n' +
    'CREATE INDEX ON :longitude(finding)\n' +
    'CREATE INDEX ON :latitude(finding)\n' +
    'CREATE INDEX ON :country_name(finding)\n' +
    'CREATE INDEX ON :country_code3(finding)\n' +
    'CREATE INDEX ON :last_update(finding)\n' +
    'CREATE INDEX ON :cpe(cpe)\n' +
    'CREATE INDEX ON :ip(finding)\n' +
    'CREATE INDEX ON :org(finding)\n' +
    'CREATE INDEX ON :vulns(vulns)\n' +
    'CREATE INDEX ON :version(finding)\n' +
    'CREATE INDEX ON :version(name)\n' +
    'CREATE INDEX ON :product(finding)\n' +
    'CREATE INDEX ON :dateString(finding)\n' +
    'CREATE INDEX ON :tags(tags)\n' +
    'CREATE INDEX ON :city(finding)\n' +
    'CREATE INDEX ON :region_code(finding)\n' +
    'CREATE INDEX ON :info(finding)\n' +
    'CREATE INDEX ON :asn(finding)\n' +
    'CREATE INDEX ON :data(finding)\n' +
    'CREATE INDEX ON :data(name)\n' +
    'CREATE INDEX ON :timestamp(name)\n' +
    'CREATE INDEX ON :timestamp(finding)\n' +
    'CREATE INDEX ON :product(finding)\n' +
    'CREATE INDEX ON :scan(scanId)\n' +
    'CREATE INDEX ON :location(longitude, country_code3, latitude, country_code, country_name, name)\n' +
    'CREATE INDEX ON :hostnames(name, hostnames)';

asyncForEach = async (array, callback) =>{
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

let func = async () => {
    await asyncForEach(queriesToRun.split('\n'), async (val) =>{
        await session.run(val);
        console.log(val);
    });
    process.exit();
};

func();

/* const queriesArray = queriesToRun.split('\n').map((val) => {
    session.run(
        val);
    console.log(val);
}); */
