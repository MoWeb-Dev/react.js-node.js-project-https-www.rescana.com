const dateobj = new Date();

module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
        '}',
    vtAPI: '535476c1cd97a6d5b8c494fdf8f0456edd1705ea4cb5c280870532b781fef345',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    neo4jUser: process.env.NEO4JUSER,
    neo4JPass: process.env.NEO4JPASS,
    enableReportSend : false,
    domainValidationScoreLimit: 76,
    blacklistsCollectCronTime: '0 0 * * 1', /* Run every Monday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA',
    vt_apikeys: [
        { name: 'kikouyh@walla.co.il', key:'c2a070d700537be125ad3c2a63afaffdc3076ceb403d29126131f11e5501e662', timeStamp:dateobj.toISOString()},
        { name: 'fadlonmirit@yahoo.com', key:'07bc9ac0c9abcb38260b5caf4b4574d680a2079df12f53088f176b974b1ed514', timeStamp:dateobj.toISOString()},
        { name: 'mirit.f@yahoo.com', key:'c6597e51523c9d085d19af1a1a169ee68e39c4d0636af358ec2b1a9966865e55', timeStamp:dateobj.toISOString()},
        { name: 'fadlon.mirit123@yahoo.com', key:'5ea062f17538ec445e6b6080ab843a6b173398a46145f27d1c073d1de71f4ecd', timeStamp:dateobj.toISOString()},
        { name: 'miritfadlon88@yahoo.com', key:'f599d981c6a959f31ad3ecd4c413505b0910840b3dd1975a60b573bfb3f4faf0', timeStamp:dateobj.toISOString()}
    ],
    shodan_apikeys: [
        { name: 'miki1469@walla.co.il', key:'EdBLfuz6GfUrIg0Oniv4xPSIacKZQzAj', timeStamp:dateobj.toISOString()},
        { name: 'kikouyh@walla.co.il', key:'kfFp3Si89Bh4zPWUujcCNGjiXYERyrSn', timeStamp:dateobj.toISOString()},
        { name: 'fadlonmirit@yahoo.com', key:'5oZ9ZJ5nCnLBHxsyvlZ1ZkHNsPQV7RJa', timeStamp:dateobj.toISOString()},
        { name: 'mirit.f@yahoo.com', key:'d9CJaC0fVBfW9pHfzcjyIisv8GfzsqbH', timeStamp:dateobj.toISOString()},
        { name: 'fadlon.mirit123@yahoo.com', key:'trfZeObetyaHA4uPUiVjfKuAE5BGr2e5', timeStamp:dateobj.toISOString()}
    ],
    intelScheduler : {
        run : false,
        cron : "0 0 * * *"
    }
};
