module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/darkvision?authMechanism=DEFAULT&authSource=admin',
    jobConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/jobs?authMechanism=DEFAULT&authSource=admin',
    website : 'https://demo.rescana.com',
    websiteForEmail : 'survey.rescana.com',
    storeServiceAddress: 'localhost',//'10.0.0.21',
    schedulerAddress: 'localhost',//'10.0.0.21',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    hibpServiceAddress: 'localhost',//'10.0.0.42',
    harvesterServiceAddress: 'localhost',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.20.0.2',
    circllu: 'http://cve.rescana.com/api/', //'http://10.0.0.18:5000/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddresses: ['localhost']//['10.0.0.42']
};
