const path = require('path');
module.exports = {
    evidence: path.join(__dirname, '../..', 'evidence/'),
    orgLogo: path.join(__dirname, '../..', 'orgLogo/'),
    vendorAssessmentFiles: path.join(__dirname, '../..', 'vendorAssessmentFiles/'),
    excelTemplates: path.join(__dirname, '../..', 'excelTemplates/'),
    excelTemplatesWithAnswers: path.join(__dirname, '../..', 'excelTemplatesWithAnswers/'),
    cveReports: path.join(__dirname, '../..', 'cveReports/'),
    intelligenceReports: path.join(__dirname, '../..', 'intelReports/'),
    surveyReports: path.join(__dirname, '../..', 'surveyReports/'),
    intelligenceManualFindings: path.join(__dirname, '../..', 'intelManuals'),
    googlePubSubCredentials: path.join(__dirname, '../..', 'rescana-qa-19d99de072c3.json'),
    gcloudKeyFile: path.join(__dirname, '../..', 'rescana-qa-19d99de072c3.json')
};
