module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@mongo:27017/darkvision?authMechanism=DEFAULT&authSource=admin',
    jobConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@mongo:27017/jobs?authMechanism=DEFAULT&authSource=admin',
    storeServiceAddress: 'localhost',//'10.0.0.14,
    schedulerAddress: 'localhost',//'10.0.0.14',
    mailerServiceAddress: 'localhost',
    harvesterServiceAddress: 'harvester',
    cveImporterServiceAddress: 'localhost',
    website : 'https://qa.rescana.com',
    websiteForEmail : 'qa.rescana.com',
    alertServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.130.0.2',
    circllu: 'http://cve.rescana.com/api/', //http://cve.circl.lu/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddresses: ['hibp'],
    hibpServiceAddress: 'hibp'
};
