const shodanExcludes = ["verbose_msg", "undetected_downloaded_samples", "_shodan"];
const shodanCompleteObjectExcludes = [
    [{name: "undetected_downloaded_samples"}, "date", "sha256"],
    [{name: "detected_downloaded_samples"}, "date", "sha256"],
    [{name: "undetected_communicating_samples"}, "date", "sha256"],
    [{name: "detected_communicating_samples"}, "date", "sha256"],
    "undetected_urls"
];
const removeExcludesFromObject = (obj) => {

    for (let item in obj) {
        if (obj.hasOwnProperty(item)) {
            if (!obj[item]) {
                console.log("Removing: ", obj[item]);
                delete obj[item];
            }
        }
    }

    for (let i = 0; i < shodanExcludes.length; i++) {
        if (obj.hasOwnProperty(shodanExcludes[i])) {
            console.log("Removing: ", obj[shodanExcludes[i]]);
            delete obj[shodanExcludes[i]];
        }
    }

    if(typeof obj === "object" && !Array.isArray(obj) && isObjectShouldBeIgnored(obj)) {
        obj = {isEmpty: true};
    }

    return obj;
};

// This function checks whether obj contains any pattern of shodanCompleteObjectExcludes, if so - returns an empty object.
// shodanCompleteObjectExcludes holds the patterns of the object to ignore.
const isObjectShouldBeIgnored = (obj) => {
    let shouldIgnoreObj = false;
    for(let i=0; i< shodanCompleteObjectExcludes.length; i++) {
        if(Array.isArray(shodanCompleteObjectExcludes[i])) {
            let isExcludeArrayExist = false;
            for(let j=0;j<shodanCompleteObjectExcludes[i].length; j++) {
                if(typeof shodanCompleteObjectExcludes[i][j] === "string") {
                    isExcludeArrayExist = obj.hasOwnProperty(shodanCompleteObjectExcludes[i][j]);
                }
                else if(typeof shodanCompleteObjectExcludes[i][j] === "object") {
                    let currExcludeObjKey = Object.keys(shodanCompleteObjectExcludes[i][j])[0];
                    let currExcludeObjVal = shodanCompleteObjectExcludes[i][j][currExcludeObjKey];
                    isExcludeArrayExist = obj.hasOwnProperty(currExcludeObjKey) && obj[currExcludeObjKey] === currExcludeObjVal;
                }

                if(!isExcludeArrayExist) {
                    break;
                }
            }
            if(isExcludeArrayExist) {
                shouldIgnoreObj = true;
                break;
            }
        }
        else if(typeof shodanCompleteObjectExcludes[i] === "string") {
            if(obj.hasOwnProperty(shodanCompleteObjectExcludes[i])) {
                shouldIgnoreObj = true;
                break;
            }
        }
    }
    return shouldIgnoreObj;
};

const inExcludeList = (item) => {
    for (let i = 0; i < shodanExcludes.length; i++) {
        if (item == shodanExcludes[i]) {
            return true;
        }
    }
};

module.exports = {shodanExcludes, removeExcludesFromObject, inExcludeList};
