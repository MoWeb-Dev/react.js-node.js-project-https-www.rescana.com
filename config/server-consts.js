const RSS_FEEDS_INFO = [
    {
        feedName: "Feodo",
        feedURL: "https://feodotracker.abuse.ch/feodotracker.rss",
        feedProps: ["Host", "Version", "Status", "FirstSeen", "LastSeen"],
        feedIPIdentifierProp: "Host"
    },
    {
        feedName: "ZeuS",
        feedURL: "https://zeustracker.abuse.ch/rss.php",
        feedProps: ["Host", "IP Address", "Status", "Level", "Malware", "AS", "Country", "SBL"],
        feedIPIdentifierProp: "IP Address"
    },
    {
        feedName: "malc0de",
        feedURL: "http://malc0de.com/rss/",
        feedProps: ["IP Address", "URL", "Country", "ASN", "MD5"],
        feedIPIdentifierProp: "IP Address"
    },
    {
        feedName: "Malwarebytes hpHosts",
        feedURL: "https://hosts-file.net/rss.asp",
        feedProps: ["Website", "IP", "Classification", "Added", "Added By"],
        feedIPIdentifierProp: "IP"
    },
    {
        feedName: "Malware Domain List",
        feedURL: "http://www.malwaredomainlist.com/hostslist/mdl.xml",
        feedProps: ["Host", "IP Address", "ASN", "Country", "Description"],
        feedIPIdentifierProp: "IP Address"
    }
];
const TXT_FEEDS_INFO = [
    {
        feedName: "c2-IP-MasterList",
        feedURLHost: "osint.bambenekconsulting.com",
        feedURLPath: "/feeds/c2-ipmasterlist.txt",
        feedIPPropName: "IP Address",
        feedProps: [{
            column: 2,
            label: "Command & Control"
        },{
            column: 3,
            label: "createDate"
        }],
        feedSeparators: {
            row: "\n",
            column: ","
        },
        feedIgnoreRowsStartsWith: ["#"],
        feedShouldInsertManualCreateDate: false
    }
];
const DEFAULT_INTEL_SCORE_RATIOS = {
    cveWeight: 30,
    dnsWeight: 10,
    bucketsWeight: 10,
    breachesWeight: 10,
    dataleaksWeight: 10,
    blacklistsWeight: 10,
    botnetsWeight: 10,
    sslCertsWeight: 10
};

const STATUS_SSL = {
    HTTPS: "https",
    NO_HTTPS: "no https",
    OFFLINE: "offline",
    REDIRECT: "redirect",
    TIMEOUT: "timeout"
};

const DISCOVERED_DOMAINS_SOURCES = {
    REVERSE_NS: "reverseNS"
};

const API_KEY_STATUS = {
    ACTIVE: "Active",
    SUSPENDED: "Suspended"
};

module.exports = {
    RSS_FEEDS_INFO,
    TXT_FEEDS_INFO,
    DEFAULT_INTEL_SCORE_RATIOS,
    STATUS_SSL,
    DISCOVERED_DOMAINS_SOURCES,
    API_KEY_STATUS
};
