const dateobj = new Date();

module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: '6040f9e981718a72f8680dfb044cd08971beb1f0702fc3cffe37c5773d9fbe02',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    neo4jUser: process.env.NEO4JUSER,
    neo4JPass: process.env.NEO4JPASS,
    domainValidationScoreLimit: 76,
    enableReportSend : false,
    blacklistsCollectCronTime: '0 0 * * 2', /* Run every Tuesday (weekly)*/
    sessionMaxAge: null,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA',
    vt_apikeys: [
        { name: 'talfeigel194@protonmail.com', key:'53c62299dde78f101178b7b6d603f6b4d837718bb195feeca3d9efbbb12b3478', timeStamp:dateobj.toISOString()},
        { name: 'dinnafr@walla.co.il', key:'f5f21a85d7a617c5108388b9fee2de0a3027ae19cbf4fa89702c80f646e50443', timeStamp:dateobj.toISOString()},
        { name: 'gadiford132@walla.co.il', key:'b09c25fc65d0f963ed5a8c34e9d5ab8a0dca72cadc898a4c726b83cbe89602c7', timeStamp:dateobj.toISOString()},
        { name: 'noaford135@walla.co.il', key:'846e691bb3ade56ec3364b1c7e108815fbcdeb14f2123e4a543e7fa00b10e13f', timeStamp:dateobj.toISOString()},
        { name: 'sariford135@walla.co.il', key:'418dfc66d7e2a02e071796c8bfa754eb8442dcb2e1474578732149e63c2ffb5', timeStamp:dateobj.toISOString()},
        { name: 'mirit.f@yahoo.com', key:'c6597e51523c9d085d19af1a1a169ee68e39c4d0636af358ec2b1a9966865e55', timeStamp:dateobj.toISOString()},
        { name: 'fadlon.mirit123@yahoo.com', key:'5ea062f17538ec445e6b6080ab843a6b173398a46145f27d1c073d1de71f4ecd', timeStamp:dateobj.toISOString()},
        { name: 'miritfadlon88@yahoo.com', key:'f599d981c6a959f31ad3ecd4c413505b0910840b3dd1975a60b573bfb3f4faf0', timeStamp:dateobj.toISOString()}
    ],
    shodan_apikeys: [
        { name: 'talfeigel194@protonmail.com', key:'0vHKtyXB62oxJTDmD2rqMcAke79tRDEk', timeStamp:dateobj.toISOString()},
        { name: 'danyfr101@walla.co.il', key:'jIZmU0LVlXmcfZc9sLGAVR7q8FPScgSi', timeStamp:dateobj.toISOString()},
        { name: 'dinnafr@walla.co.il', key:'jjiMcUUUzYCQP74tODV7lbh0vCskf45a', timeStamp:dateobj.toISOString()},
        { name: 'gadiford132@walla.co.il', key:'cYKq2Q2OdMttcezY0W9ClfTo0uaGBa8U', timeStamp:dateobj.toISOString()},
        { name: 'noaford135@walla.co.il', key:'cbF6KHpWz4zPLhmUj3M4HhpInPSkhfth', timeStamp:dateobj.toISOString()},
        { name: 'sariford135@walla.co.il', key:'HtekgTMjkWGKwOrxxKAohyuzEgvo3dKM', timeStamp:dateobj.toISOString()},
        { name: 'fadlonmirit@yahoo.com', key:'5oZ9ZJ5nCnLBHxsyvlZ1ZkHNsPQV7RJa', timeStamp:dateobj.toISOString()},
        { name: 'mirit.f@yahoo.com', key:'d9CJaC0fVBfW9pHfzcjyIisv8GfzsqbH', timeStamp:dateobj.toISOString()},
        { name: 'fadlon.mirit123@yahoo.com', key:'trfZeObetyaHA4uPUiVjfKuAE5BGr2e5', timeStamp:dateobj.toISOString()}
    ],
    intelScheduler : {
        run : false,
        cron : "0 21 * * *"
    }
};
