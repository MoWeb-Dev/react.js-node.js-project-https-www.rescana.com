const dateobj = new Date();

module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: 'ede4eeb6d377d4c33718d99ac004797d2e3ab5bc89d8d0f1e8c24b4af8971fd0',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    neo4jUser: process.env.NEO4JUSER,
    neo4JPass: process.env.NEO4JPASS,
    enableReportSend : false,
    domainValidationScoreLimit: 76,
    blacklistsCollectCronTime: '0 0 * * 4', /* Run every Wednesday (weekly)*/
    sessionMaxAge: null,
    vt_apikeys: [
        { name: 'mirit.fadlon8765@yahoo.com', key:'0c51fc0c6dcd439340f5a628f275ba3fafd50b87d5a82ccc6fbd1ba5bb74a6a8', timeStamp:dateobj.toISOString()},
        { name: 'sariford135@walla.co.il', key:'418dfc66d7e2a02e071796c8bfa754eb8442dcb2e1474578732149e63c2ffb5', timeStamp:dateobj.toISOString()},
        { name: 'mirit.f@yahoo.com', key:'c6597e51523c9d085d19af1a1a169ee68e39c4d0636af358ec2b1a9966865e55', timeStamp:dateobj.toISOString()},
        { name: 'fadlon.mirit123@yahoo.com', key:'5ea062f17538ec445e6b6080ab843a6b173398a46145f27d1c073d1de71f4ecd', timeStamp:dateobj.toISOString()},
        { name: 'miritfadlon88@yahoo.com', key:'f599d981c6a959f31ad3ecd4c413505b0910840b3dd1975a60b573bfb3f4faf0', timeStamp:dateobj.toISOString()}
    ],
    shodan_apikeys: [
        { name: 'miritfadlon88@yahoo.com', key:'REYxAkAckfin5bA9jdJyDc0febkAnlfu', timeStamp:dateobj.toISOString()},
        { name: 'sariford135@walla.co.il', key:'HtekgTMjkWGKwOrxxKAohyuzEgvo3dKM', timeStamp:dateobj.toISOString()},
        { name: 'fadlonmirit@yahoo.com', key:'5oZ9ZJ5nCnLBHxsyvlZ1ZkHNsPQV7RJa', timeStamp:dateobj.toISOString()},
        { name: 'mirit.f@yahoo.com', key:'d9CJaC0fVBfW9pHfzcjyIisv8GfzsqbH', timeStamp:dateobj.toISOString()},
        { name: 'fadlon.mirit123@yahoo.com', key:'trfZeObetyaHA4uPUiVjfKuAE5BGr2e5', timeStamp:dateobj.toISOString()}
    ],
    intelScheduler : {
        run : false,
        cron : "0 21 * * *"
    }
};
