const dateobj = new Date();

module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: process.env.VT_KEY,
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    domainValidationScoreLimit: 76,
    enableReportSend : false,
    blacklistsCollectCronTime: '0 0 * * 3', /* Run every Wednesday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7,
    vt_apikeys: [
        { name: 'nukigs456@protonmail.com', key:'a95be069eecbf28d95ba9add8c3ba52a4bef89eb9ec8c64c2b19ade0b2af6637', timeStamp:dateobj.toISOString()},
        { name: 'kikouyh@walla.co.il', key:'c2a070d700537be125ad3c2a63afaffdc3076ceb403d29126131f11e5501e662', timeStamp:dateobj.toISOString()},
        { name: 'fadlonmirit@yahoo.com', key:'07bc9ac0c9abcb38260b5caf4b4574d680a2079df12f53088f176b974b1ed514', timeStamp:dateobj.toISOString()},
        { name: 'mirit.f@yahoo.com', key:'c6597e51523c9d085d19af1a1a169ee68e39c4d0636af358ec2b1a9966865e55', timeStamp:dateobj.toISOString()},
        { name: 'fadlon.mirit123@yahoo.com', key:'5ea062f17538ec445e6b6080ab843a6b173398a46145f27d1c073d1de71f4ecd', timeStamp:dateobj.toISOString()},
        { name: 'miritfadlon88@yahoo.com', key:'f599d981c6a959f31ad3ecd4c413505b0910840b3dd1975a60b573bfb3f4faf0', timeStamp:dateobj.toISOString()},
        { name: 'talfeigel194@yahoo.com', key:'b6e7aecb27dbd621c1f2753fc6270e92161c901e9d1ced5729fe3a4647a696fb', timeStamp:dateobj.toISOString()},
        { name: 'bakbok85@walla.co.il', key:'0a522d019f90a4e2a105e1597be8efd456218043f4d8d183288251ba8c51416d', timeStamp:dateobj.toISOString()},
        { name: 'bakbok85@yahoo.com', key:'202ddd15a44dd96653f0f4f3a442f97b10690c4ff14c62d17b717b2ae5f3dab9', timeStamp:dateobj.toISOString()},
        { name: 'lynndnzgr@gmail.com', key:'465a2eab74efdf402f29a2819118fcffb796beb238145cca3456e07cc1eb5fbe', timeStamp:dateobj.toISOString()},
        { name: 'miritfadlon29@walla.co.il', key:'9732c0a551f5e82a473c5bb9360b1ac2d11acc70183fe72359a27ca8005da6f0', timeStamp:dateobj.toISOString()},
        { name: 'mirmirmi@walla.co.il', key:'3bdc3e2106a02d0144ea8dbffe2d4b446c5307468a85aea6af21bc61c1d4d6c7', timeStamp:dateobj.toISOString()},
        { name: 'pizza12355@walla.co.il', key:'6ee455a02de97b54317de7d0306ba444a67f290e05ac7a7fd800fad8df79e280', timeStamp:dateobj.toISOString()},
        { name: 'miki1469@walla.co.il', key:'6cc8c27429d841eb9943b370a8fc3aca8bb64d588dcc7b7a269dcde8e8d64ba4', timeStamp:dateobj.toISOString()}
    ],
    shodan_apikeys: [
        { name: 'mirit.fadlon8765@yahoo.com', key:'uQBls8coPHcP5NQHvh54e58ok18Ujno4', timeStamp:dateobj.toISOString()},
        { name: 'miki1469@walla.co.il', key:'EdBLfuz6GfUrIg0Oniv4xPSIacKZQzAj', timeStamp:dateobj.toISOString()},
        { name: 'kikouyh@walla.co.il', key:'kfFp3Si89Bh4zPWUujcCNGjiXYERyrSn', timeStamp:dateobj.toISOString()},
        { name: 'fadlonmirit@yahoo.com', key:'5oZ9ZJ5nCnLBHxsyvlZ1ZkHNsPQV7RJa', timeStamp:dateobj.toISOString()},
        { name: 'mirit.f@yahoo.com', key:'d9CJaC0fVBfW9pHfzcjyIisv8GfzsqbH', timeStamp:dateobj.toISOString()},
        { name: 'fadlon.mirit123@yahoo.com', key:'trfZeObetyaHA4uPUiVjfKuAE5BGr2e5', timeStamp:dateobj.toISOString()},
        { name: 'talfeigel194@yahoo.com', key:'TstdmF8PXTySqoQzhvzaiu9EsM36zksm', timeStamp:dateobj.toISOString()},
        { name: 'bakbok85@walla.co.il', key:'e7tPY3CHNAXnchTN3JRkAAYd3HIet7nV', timeStamp:dateobj.toISOString()},
        { name: 'bakbok85@yahoo.com', key:'lwRSzAu5Ye9YF10pnyoj2Yfk3Y1ffhxW', timeStamp:dateobj.toISOString()},
        { name: 'lynndnzgr@gmail.com', key:'XFxEdUklBp4Y2jtXYbaFuT2jrCoF3LwF', timeStamp:dateobj.toISOString()},
        { name: 'kobile123_123@protonmail.com', key:'WzjS4nI8zCMcE4XyJvuHCCoSDXKacctN', timeStamp:dateobj.toISOString()},
        { name: 'miritfadlon29@walla.co.il', key:'RFv1UFNYi41hbUexE49JeMGWnTJ3Y4ap', timeStamp:dateobj.toISOString()},
        { name: 'mirmirmi@walla.co.il', key:'UU1OXkuLaAqcBRGnadjWJLE7GyzXIybR', timeStamp:dateobj.toISOString()},
        { name: 'pizza12355@walla.co.il', key:'3lNz5GjfjFK9V2gOcvfEoPSTACwzCZGo', timeStamp:dateobj.toISOString()}

    ],
    neo4jUser: process.env.NEO4JUSER,
    neo4JPass: process.env.NEO4JPASS,
    intelScheduler : {
        run : false,
        cron : "0 0 * * *"
    }
};
