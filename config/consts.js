const LANG_EN = 'en';
const LANG_HE = 'he';
const RTL = 'rtl';
const LTR = 'ltr';
const MY_LANG = 'myLang';
const MY_DIR = 'myDir';
const AUTH_USER = 'authUser';
const PROJECT_NAME = 'pn';
const PROJECT_ID = 'pId';
const PROJECT_ISSELFASSESSMENT = 'false';
const DASHBOARD_SORT_TYPE = 'dst';
const ORGANIZATION_ID = 'orgId';
const ORGANIZATION_NAME = 'orgName';
const SESSION_COMPANIES = 'SessionCompanies';
const SESSION_CVE_SEVERITY = 'SessionCveSeverity';
const SESSION_CVE_SEVERITY_PANEL = 'SessionCveSeverityPanel';
const SESSION_FIRST_CVE_TABLE = 'SessionCveFirstTable';
const SESSION_ADD_NEW_COMP_BY_USER = 'SessionAddNewCompanyByUser';
const DEFAULT_LANG = LANG_EN;
const DEFAULT_DIR = LTR;
const DATALEAKS_PASTEBIN = 'Pastebin';
const VIEWDNS_KEY = 'fbe70be12f33d354e4846d90c2d7a5aadfc971ae'; /*'5b972fc7263a10adcc83a9d9bc0b0bac4f061072';*/
const HUNTER_KEY = 'fe28ec7730dabf18261e44a69ce5fff0decd566a';
const WIFI_WIGLE_API_KEY = 'Basic QUlEODRlN2FmYjI0ZmVmZDUxZGM3OTQ4NjhhOTYxNjI1ODE6YWU2M2RmMWE0ZjdmZThmMzZlOWQ4OTc5NzRkNGVlY2Y=';
const WILDFIRE_URL = "https://wildfire.paloaltonetworks.com/publicapi/";
const EMAIL_BREACHES_TYPE = 'emailBreaches';
const BLACKLISTS_TYPE = 'blacklistedDomains';
const DATALEAKS_TYPE = 'dataleaks';
const BOTNETS_TYPE = 'botnets';
const SSL_CERTS_TYPE = 'sslCerts';
const WIFI_NETWORKS_TYPE = 'wifiNetworks';
const GDPR_NOT_COMPLY_TYPE = 'gdprNotComply';
const CONTACTS_BOX_TYPE = 'contactsBox';
const INFORMATION_BOX_TYPE = 'infoBox';
const DISCOVERED_DOMAINS_TYPE = 'discoveredDomains';
const OVERVIEW_TYPE = 'overview';
const GRAPH_COMPANIES_FILTER_LIMIT = 3;
const CVE_LOW_MAX = 3, CVE_MEDIUM_MIN = CVE_LOW_MAX, CVE_MEDIUM_MAX = 7.5, CVE_HIGH_MIN = CVE_MEDIUM_MAX, CVE_HIGH_MAX = 9, CVE_CRITICAL_MIN = CVE_HIGH_MAX, CVE_CRITICAL_MAX = 10;
const OVERVIEW_COLUMN_TYPES = {
    NUMBER_LIMIT_100: "number_limit_100",
    STRING: "string",
    DATES_RANGE: "dates_range",
    SINGLE_DATE: "single_date",
    CLOSED_OPTIONS: "closed_options",
    YES_NO_ANY_ARRAY: "yes_no_any_array"
};
const OVERVIEW_DATE_FORMAT = 'LL';
const EMPTY_CONTACT = {
    email: "",
    name: "",
    phone: "",
    position: ""
};
const EMPTY_DATE_RANGE_FIELD = {
    startDate: "",
    endDate: ""
};
const SCORE_TYPES = {
    COMPANY: "company",
    INTEL: "intel",
    SURVEY: "survey"
};
const WILDFIRE_STATUS_TYPES = {
    SAFE: "safe",
    SCANNING: "scanning",
    MALICIOUS: "malicious"
};
const DEFAULT_INTEL_SCORE_RATIOS = {
    cveWeight: 30,
    dnsWeight: 10,
    bucketsWeight: 10,
    breachesWeight: 10,
    dataleaksWeight: 10,
    blacklistsWeight: 10,
    botnetsWeight: 10,
    sslCertsWeight: 10
};
const COMPANY_CONTACT_TYPES = {
    RESPONSIBLE: "responsible",
    CONTACT: "contact"
};
const YES_NO_MODAL_TYPES = {
    OK: "ok",
    DISCARD: "discard",
    CANCEL: "cancel"
};
const COMPANY_INFO_CLASSIFICATION_TYPES = [
    "Cloud Services",
    "Software Development",
    "More Than 100 Employees",
    "Have Access to Sensitive Data",
    "Holds Sensitive Data"
];
const COMPANY_INFO_SECTOR_TYPES = [
    "Law Office",
    "IT",
    "Financial",
    "Marketing and Advertising",
    "Deliveries",
    "Printing Press",
    "Maintenance",
    "Physical Security",
    "Other"
];
const RSS_FEEDS_EXTRA_INFO = [
    {
        feedName: "Feodo",
        feedDescription: "Feodo (also known as Cridex or Bugat) is a Trojan used to commit ebanking fraud and steal sensitive information from the victims computer, such as credit card details or credentials.\nThe following are four versions of Feodo, labeled as version A, version B, version C and version D:",
        feedData: [{
            name: "Version A",
            finding: "Hosted on compromised webservers running an nginx proxy on port 8080 TCP forwarding all botnet traffic to a tier 2 proxy node. Botnet traffic usually directly hits these hosts on port 8080 TCP without using a domain name."
        }, {
            name: "Version B",
            finding: "Hosted on servers rented and operated by cybercriminals for the exclusive purpose of hosting a Feodo botnet controller. Usually taking advantage of a domain name within ccTLD .ru. Botnet traffic usually hits these domain names using port 80 TCP."
        }, {
            name: "Version C",
            finding: "Successor of Feodo, completely different code. Hosted on the same botnet infrastructure as Version A (compromised webservers, nginx on port 8080 TCP or port 7779 TCP, no domain names) but using a different URL structure. This Version is also known as Geodo and Emotet."
        }, {
            name: "Version D",
            finding: "Successor of Cridex. This version is also known as Dridex."
        }, {
            name: "Version E",
            finding: "Successor of Geodo / Emotet (Version C) called Heodo. First appeared in March 2017."
        }]
    }, {
        feedName: "ZeuS",
        feedDescription: "ZeuS (also known as Zbot / WSNPoem) is a crimeware kit, which steals credentials from various online services like social networks, online banking accounts, ftp accounts, email accounts and other (phishing).",
        feedData: [{}]
    }
];
const API_KEY_STATUS = {
    ACTIVE: "Active",
    SUSPENDED: "Suspended"
};
const DASHBOARD_COMPANIES_SORT_TYPES = [
    "Default Display" ,
    "Highest Risk First",
    "Lowest Risk First",
    "Flagged Companies First",
    "Flagged Companies Last",
    "Alphabetic Ascending",
    "Alphabetic Descending"
];

module.exports = {
    HUNTER_KEY,
    VIEWDNS_KEY,
    WIFI_WIGLE_API_KEY,
    LANG_EN,
    LANG_HE,
    RTL,
    LTR,
    MY_LANG,
    MY_DIR,
    AUTH_USER,
    PROJECT_NAME,
    PROJECT_ID,
    PROJECT_ISSELFASSESSMENT,
    DASHBOARD_SORT_TYPE,
    ORGANIZATION_ID,
    ORGANIZATION_NAME,
    SESSION_COMPANIES,
    SESSION_CVE_SEVERITY,
    SESSION_CVE_SEVERITY_PANEL,
    SESSION_FIRST_CVE_TABLE,
    SESSION_ADD_NEW_COMP_BY_USER,
    DEFAULT_LANG,
    DEFAULT_DIR,
    DATALEAKS_PASTEBIN,
    EMAIL_BREACHES_TYPE,
    BLACKLISTS_TYPE,
    DATALEAKS_TYPE,
    BOTNETS_TYPE,
    OVERVIEW_TYPE,
    OVERVIEW_COLUMN_TYPES,
    OVERVIEW_DATE_FORMAT,
    SSL_CERTS_TYPE,
    WIFI_NETWORKS_TYPE,
    GDPR_NOT_COMPLY_TYPE,
    CONTACTS_BOX_TYPE,
    INFORMATION_BOX_TYPE,
    DISCOVERED_DOMAINS_TYPE,
    EMPTY_CONTACT,
    EMPTY_DATE_RANGE_FIELD,
    GRAPH_COMPANIES_FILTER_LIMIT,
    CVE_LOW_MAX,
    CVE_MEDIUM_MIN,
    CVE_MEDIUM_MAX,
    CVE_HIGH_MIN,
    CVE_HIGH_MAX,
    CVE_CRITICAL_MIN,
    CVE_CRITICAL_MAX,
    SCORE_TYPES,
    WILDFIRE_URL,
    WILDFIRE_STATUS_TYPES,
    DEFAULT_INTEL_SCORE_RATIOS,
    COMPANY_CONTACT_TYPES,
    YES_NO_MODAL_TYPES,
    COMPANY_INFO_CLASSIFICATION_TYPES,
    COMPANY_INFO_SECTOR_TYPES,
    RSS_FEEDS_EXTRA_INFO,
    API_KEY_STATUS,
    DASHBOARD_COMPANIES_SORT_TYPES
};
