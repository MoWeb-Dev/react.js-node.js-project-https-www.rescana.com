module.exports = {
    hostname: 'mongo',
    port: 27017,
    username: process.env.MONGODBUSER,
    password: process.env.MONGODBPASS
};
