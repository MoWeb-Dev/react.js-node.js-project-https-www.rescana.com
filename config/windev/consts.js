const dateobj = new Date();

module.exports = {
    html: '',
    vtAPI: '6040f9e981718a72f8680dfb044cd08971beb1f0702fc3cffe37c5773d9fbe02',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    blacklistsCollectCronTime: '0 0 * * 4', /* Run every Thursday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7,
    neo4jUser: process.env.NEO4JUSER,
    neo4JPass: process.env.NEO4JPASS,
    domainValidationScoreLimit: 76,
    enableReportSend : false,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA',
    vt_apikeys: [
        { name: 'yuvlynn@walla.co.il', key:'d1059c84f17eba6accd3a78e706ca312d4a0251202dd5247bc502e948518ea8e', timeStamp:dateobj.toISOString()},
    ],
    shodan_apikeys: [
        { name: 'talfeigel194@walla.co.il', key:'dV8lksjRZIwEPNsaNQp589dfG227Y4RJ', timeStamp:dateobj.toISOString()}
    ],
    intelScheduler : {
        run : false,
        cron : "0 21 * * *"
    }
};
