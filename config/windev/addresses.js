module.exports = {
    circllu: 'http://cve.rescana.com/api/', //'http://10.0.0.18:5000/api/',
    mongoConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/darkvision?authMechanism=DEFAULT&authSource=admin',
    jobConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/jobs?authMechanism=DEFAULT&authSource=admin',
    website : 'http://localhost:3002',
    websiteForEmail : 'localhost:3002',
    dnsTwistAddress: 'localhost',
    storeServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://localhost',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
