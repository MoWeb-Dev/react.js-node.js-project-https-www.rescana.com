module.exports = {
    compareQueue: {
        type: 'amqp',
        pin: 'role:compareApps,compareNow:app',
        hostname: 'localhost',
        port: 5672,
        vhost: '/',
        username: 'guest',
        password: process.env.RABBITMQ,
        timeout: 999999
    }
};
