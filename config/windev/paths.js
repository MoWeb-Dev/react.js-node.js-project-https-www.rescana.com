const path = require('path');
module.exports = {
    evidence: './evidence',
    orgLogo: './orgLogo',
    vendorAssessmentFiles: './vendorAssessmentFiles',
    excelTemplates: './excelTemplates',
    excelTemplatesWithAnswers: './excelTemplatesWithAnswers',
    cveReports: './cveReports',
    intelligenceReports: './intelReports',
    surveyReports: './surveyReports',
    intelligenceManualFindings: './intelManuals',
    gcloudKeyFile: '/opt/darkvision/rescana-qa-19d99de072c3.json',
    googlePubSubCredentials: path.join(__dirname, '../..', 'rescana-qa-19d99de072c3.json'),
};
