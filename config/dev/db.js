module.exports = {
    hostname: 'localhost',
    port: 27017,
    username: process.env.MONGODBUSER,
    password: process.env.MONGODBPASS
};
