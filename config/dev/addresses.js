module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/darkvision?authMechanism=DEFAULT&authSource=admin',
    jobConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/jobs?authMechanism=DEFAULT&authSource=admin',
    storeServiceAddress: 'localhost',//'10.0.0.14,
    schedulerAddress: 'localhost',//'10.0.0.14',
    mailerServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',
    cveImporterServiceAddress: 'localhost',
    website : 'localhost:3003',
    websiteForEmail : 'localhost:3003',
    alertServiceAddress: 'localhost',
    neo4jUrl: 'bolt://localhost',
    circllu: 'http://cve.rescana.com/api/', //'http://cve.circl.lu/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
