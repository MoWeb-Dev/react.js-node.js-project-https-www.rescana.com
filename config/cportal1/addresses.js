module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/darkvision?authMechanism=DEFAULT&authSource=admin',
    jobConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/jobs?authMechanism=DEFAULT&authSource=admin',
    website : 'https://cportal.rescana.com',
    websiteForEmail : 'cportal.rescana.com',
    storeServiceAddress: 'localhost', //'10.0.0.40',
    schedulerAddress: 'localhost', //'10.0.0.40',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost', //'10.0.0.30',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://localhost',
    circllu: 'http://cve.rescana.com/api/',
    intelServiceAddresses: ['0.0.0.0'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
