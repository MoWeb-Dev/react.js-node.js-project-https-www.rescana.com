const dateobj = new Date();

module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: 'ede4eeb6d377d4c33718d99ac004797d2e3ab5bc89d8d0f1e8c24b4af8971fd0',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    neo4jUser: process.env.NEO4JUSER,
    neo4JPass: process.env.NEO4JPASS,
    domainValidationScoreLimit: 76,
    wildFireAPI: process.env.WF_API,
    enableReportSend : false,
    blacklistsCollectCronTime: '0 0 * * 0', /* Run every Sunday (weekly)*/
    sessionMaxAge: 1000 * 60 * 60 * 24 * 7,
    vt_apikeys: [
        { name: 'yuvlynn@protonmail.com', key:'b3479ad070b1564097005c979fd30fc5a935bf6194633c4dc8c17b86baed8ec4', timeStamp:dateobj.toISOString()},
        { name: 'yuvlynn@yahoo.com', key:'c5d2d743d81727cb19da69c516fbd6f6c2e980ae8d5bb43aae191798b3373159', timeStamp:dateobj.toISOString()},
        { name: 'tommyfeigel@walla.co.il', key:'ef562fb0eaa5a36a2de6d3fa8dc8ea4b177f830d7330e13ce17b590d54f0046f', timeStamp:dateobj.toISOString()},
        { name: 'tommyfeigel@protonmail.com', key:'5b2774311c08686de5b1d49acc9553050642ad33c813aa29999ad18899c04fd2', timeStamp:dateobj.toISOString()},
        { name: 'tommyfeigel@yahoo.com', key:'6d84aac173e2e2b4ba27f909df0d4915ee0bc0a42904a874f0e1f9078fc0e8b8', timeStamp:dateobj.toISOString()},
        { name: 'lynndan@walla.co.il', key:'0466f756f4d86571dc0421733aa0c63483b45203511c55ecef77e34dba4c9b3e', timeStamp:dateobj.toISOString()},
        { name: 'lynndan@protonmail.com', key:'4395401783b4f7779b7ce4bea392190d89eaf5c4af0592b8bdc2bdb192c013f7', timeStamp:dateobj.toISOString()},
        { name: 'lynndan58@yahoo.com', key:'144fc9d9e51928135d5cd42e5348ac933d40d75359c242afc2dd887c92b9252e', timeStamp:dateobj.toISOString()},
        { name: 'talfeigel194@walla.co.il', key:'cfe72aa9404cb739cdc8035a3fef1006ac3fef146cce4eee1abc13b4fda12dee', timeStamp:dateobj.toISOString()}
    ],
    shodan_apikeys: [
        { name: 'qa@gmail.com', key:'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA', timeStamp:dateobj.toISOString()},
        { name: 'yuvlynn@walla.co.il', key:'8zb9Qa0S9Rb0KsWxzN9UJ7Qy0A1giuZa', timeStamp:dateobj.toISOString()},
        { name: 'yuvlynn@protonmail.com', key:'8zb9Qa0S9Rb0KsWxzN9UJ7Qy0A1giuZa', timeStamp:dateobj.toISOString()},
        { name: 'yuvlynn@yahoo.com', key:'pEbP1CZcmd70W1UW3kFG1U0G8B9eX2iR', timeStamp:dateobj.toISOString()},
        { name: 'tommyfeigel@walla.co.il', key:'hrmq7DMbMDiejC4bwzzEHi2azh8TRtUl', timeStamp:dateobj.toISOString()},
        { name: 'tommyfeigel@protonmail.com', key:'Bk3LTfX5hKEl5Vg0AnYAvru3yXl8cWiz', timeStamp:dateobj.toISOString()},
        { name: 'tommyfeigel@yahoo.com', key:'pcGIIZuP0Eoz98wf06qeL15atN0LK8eX', timeStamp:dateobj.toISOString()},
        { name: 'lynndan@walla.co.il', key:'sZnPWMtXim1Iwv9DbOvfLyXSY6MxGPoV', timeStamp:dateobj.toISOString()},
        { name: 'lynndan@protonmail.com', key:'dXixcJOyEAbX9Z8u219u91Qxl1ga8XJ3', timeStamp:dateobj.toISOString()},
        { name: 'lynndan58@yahoo.com', key:'jFBIvlNkyw1E30Kr69Sk7FQSgWGSZaXI', timeStamp:dateobj.toISOString()}
    ],
    intelScheduler : {
        run : false,
        cron : "0 0 * * *"
    }
};
