module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/darkvision?authMechanism=DEFAULT&authSource=admin',
    jobConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/jobs?authMechanism=DEFAULT&authSource=admin',
    website : 'https://poc.rescana.com',
    websiteForEmail : 'poc.rescana.com',
    storeServiceAddress: 'localhost',//'10.0.0.39',
    schedulerAddress: 'localhost',//'10.0.0.39',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',//'10.0.0.30',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.240.0.2', //'bolt://10.0.0.39',
    circllu: 'http://cve.rescana.com/api/', //'http://10.0.0.18:5000/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
