module.exports = {
    dnsTwistAddress: 'localhost',
    mongoConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/darkvision?authMechanism=DEFAULT&authSource=admin',
    jobConnectionString: 'mongodb://' + process.env.MONGODBUSER + ':' + process.env.MONGODBPASS + '@localhost:27017/jobs?authMechanism=DEFAULT&authSource=admin',
    website : 'https://vcportal.rescana.com',
    websiteForEmail : 'vcportal.rescana.com',
    storeServiceAddress: 'localhost', //'10.0.0.41',
    schedulerAddress: 'localhost', //'10.0.0.41',
    mailerServiceAddress: 'localhost',
    alertServiceAddress: 'localhost',
    harvesterServiceAddress: 'localhost',
    cveImporterServiceAddress: 'localhost',
    neo4jUrl: 'bolt://10.240.0.2',
    circllu: 'http://cve.rescana.com/api/',
    intelServiceAddresses: ['localhost'],
    hibpServiceAddress: 'localhost',
    hibpServiceAddresses: ['localhost']
};
