const dateobj = new Date();

module.exports = {
    html: 'html {' +
        'zoom: 0.75;' +
    '}',
    vtAPI: '6040f9e981718a72f8680dfb044cd08971beb1f0702fc3cffe37c5773d9fbe02',
    apilityAPI: '50665e85-6eab-453c-9743-88dc5d224175',
    wildFireAPI: process.env.WF_API,
    neo4jUser: process.env.NEO4JUSER,
    neo4JPass: process.env.NEO4JPASS,
    domainValidationScoreLimit: 76,
    enableReportSend : false,
    blacklistsCollectCronTime: '0 0 * * 4', /* Run every Wednesday (weekly)*/
    sessionMaxAge: null,
    shodanKey: 'tLsTo0YJGGtomZ4cIqspRxyZtlDgTIkA',
    vt_apikeys: [
        { name: 'danyfr101@walla.co.il', key:'5a991a1068fc4a7bb149111a9c57aafe27e70a79bc30cda600de81f9c1913b07', timeStamp:dateobj.toISOString()}
    ],
    shodan_apikeys: [
        { name: 'nukigs456@protonmail.com', key:'4tJEe1dLraJIAiGlyCdIXTnaMAMLWOML', timeStamp:dateobj.toISOString()}
    ],
    intelScheduler : {
        run : false,
        cron : "0 21 * * *"
    }
};
