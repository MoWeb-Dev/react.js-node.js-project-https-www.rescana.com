FROM node:10-jessie
WORKDIR /opt/darkvision
RUN wget -qO - https://www.mongodb.org/static/pgp/server-3.4.asc | apt-key add -
RUN echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.4 main" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list
RUN apt-get update && \
  apt-get install -y --no-install-recommends git \
  python3-dnspython \
  libcairo2-dev \
  libjpeg-dev \
  libpango1.0-dev \
  libgif-dev \
  build-essential \
  g++ \
  python3-geoip \
  python3-whois \
  python3-requests \
  python3-ssdeep \
  telnet \
  vim \
  tcsh \
  mongodb-org-tools \
  cron \
  locales && \
  locale-gen en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
COPY ./package.json /opt/darkvision
RUN npm install pm2 -g
RUN pm2 install pm2-logrotate
RUN npm install canvas@2.5.0
RUN npm install --quiet
COPY . /opt/darkvision 
RUN cd /opt/darkvision && npm run build
RUN crontab /opt/darkvision/docker/cronjobs
RUN service cron start
CMD pm2-runtime process.json
